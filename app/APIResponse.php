<?php

namespace App;

use Illuminate\Http\JsonResponse;
use App\Exceptions\AGException;
use App\K;

/**
 * This class represents the response object to be returned from API calls. A response object
 * can have two different structures depending on whether the response is for an error or
 * for a successfull call containing the results, according to the below format. For some
 * successfull calls the body may be missing (null), for example if there are no expected
 * results attached to a response.
 *
 *  {
 *      "status":   "ok"|"error",
 *
 *    IF STATUS IS OK
 *
 *      "body": { <the response's body> } | null
 *
 *    ELSE
 *
 *      "error_code": n,
 *      "error_description": "A short description of the error",
 *      "error_details": 
 *       [ 
 *             {  
 *                 "message" : "A detailed specific error message",
 *                 "culprit" : "The name of the entity/field that caused the error"
 *             }
 *             
 *             ...
 *                 
 *       ] | null
 *  }
 *
 */
class APIResponse {
	
	protected  $m_status = null;
	protected  $m_body = null;
	protected  $m_error_code = null;
	protected  $m_error_description = null;
	protected  $m_error_details = null;
	protected  $m_exception = null;
	
	protected function __construct(array $body = null, AGException $errors = null) {
        $this->m_body = $body;
        $this->m_exception = $errors;
	}
	
	/**
	 *  Construct a new API response object with an empty body. If the response
	 *  is supposed to carry results data, then call the with() method passing
	 *  an array with the results data.
	 *  
	 *  @return \App\APIResponse
	 */
	public static function ok() {
		$r = new APIResponse([]);
		return $r;
	}

	/**
	 *  Construct a new API response object with a default generic AGException.
	 *  More specific exceptions may be set by calling the with() method after
	 *  this method passing an AGException object.
	 *  
	 *  @return \App\APIResponse
	 */
	public static function error() {
		$r = new APIResponse(null, new AGException);
		return $r;
	}
	
	/**
	 *  This method sets the response's body, if called after the ok() method, or
	 *  the response errors, if called after the error() method.
	 *  
	 *  @param array|AGException $what If the response has been constructed using
	 *  the ok() factory method, then passing an array will set the response body.
	 *  If the response has been constructed using the error() method, then pass a
	 *  AGException to set response errors.
	 *  @throws AGException
	 *  @return \App\APIResponse
	 */
	public function with($what) {
		if($what instanceof AGException) {
		   if(!$this->is_error())
		   	  throw new AGException
		      ('Cannot make response: status should be <error>.');
		   $this->m_exception = $what;
		} else
		if(is_array($what)) {
           if(!$this->is_ok())
           	  throw new AGException
           	  ('Cannot make response: status should be <ok>.');
           $this->m_body = $what;
		}
		return $this;
	}
	
	/**
	 *  Encodes this API response to JSON.
	 *  
	 *  @return string
	 */
	public function to_json() {
        return json_encode($this->to_array());
	}

	/**
	 *  Convert this API reponse to an array.
	 *  
	 *  @return array
	 */
	public function to_array() {
		if($this->m_status === 'ok')
			return [
				'status' => $this->m_status,
				'body'   => $this->m_body
			];
		else
			return [
				'status' => $this->m_status,
				'error_code'   => $this->m_error_code,
				'error_description' => $this->m_error_description,
				'error_details' => $this->m_error_details
			];
	}
	
	/**
	 *  Determines whether the current response represents an error.
	 */
	public function is_error() {
		return $this->m_body === null && $this->m_exception !== null;
	}
	
	/**
	 *  Determines whether the current response represents a success.
	 */
	public function is_ok() {
		return $this->m_body !== null && $this->m_exception === null;
	}
	
	/**
	 * Lets an API response go. This method must be called as the last method in the
	 * call chain in order to set the response free. If this method is not called, the
	 * response will be trapped in the server (maybe forever).
	 * 
	 * NOTE: This method is very much tied to the framework as it is the bridge between
	 * the app's notion of 'response' and the framework's implementation at the transport
	 * level.
	 * 
	 * @return object The framework implementation of a JSON response.
	 */
	public function go() {
		$rcode = $this->make_response();
		return new JsonResponse($this->to_array(), $rcode);
	}
	
	/**
	 * This method builds the response. Must be called before letting the response go.
	 *
	 * @return int The response error code.
	 * @throws AGException
	 */
    private function make_response(){
		if(($this->m_body !== null && $this->m_exception !== null) ||
           ($this->m_body === null && $this->m_exception === null))
			throw new AGException('Cannot make response: ambiguous parameters.');
		$this->m_status = $this->m_body!==null ? 'ok' : 'error';
		if($this->m_status === 'ok') {
            if(empty($this->m_body)) $this->m_body = null;
            return K::ERROR_NONE;
		}
		else if($this->m_status === 'error') {
			$code = $this->m_exception->getCode();
			$description = $this->m_exception->getMessage();
			$details = $this->m_exception->get_context();
			if(empty($code) || !is_numeric($code) || empty($description))
				throw new AGException('Invalid response error parameters.');
			$this->m_error_code = (int) $code;
			$this->m_error_description = $description;
			// Only include details for non-internal errors
			if($code < 500){
				$this->m_error_details = !empty($details) ? $details : null;
			}
			return $this->m_error_code;
		}
	}
	
		
}