<?php

namespace App;

use App\Interfaces\IAccountManager;
use App\Models\User;
use App\Models\UserVerification;
use App\Models\Customer;
use App\Models\CustomerSettings;
use App\Models\Provider;
use App\Models\ProviderSettings;
use App\Models\ProviderIdentity;
use App\Models\Task;
use App\Exceptions\AGException;
use App\Exceptions\AGAccountException;
use App\Exceptions\AGValidationException;
use App\Exceptions\AGInvalidStateException;
use App\Events\EventUserSignup;
use App\Events\EventUserCancelled;
use App\Utils\Validate;
use App\Alert;
use App\K;
use DB, Hash, Event;


/**
 */
class AccountManager {
    
    /**
     * This manager factories.
     * 
     * @var array
     */
    protected $managers;

    
    public function __construct() {
        
        $this->managers = [
            
            K::ROLE_CUSTOMER => function(User $user) {
                                    $customer = $user instanceof Customer ?
                                                $user : $user->as_a('customer');
                                    return new CustomerAccountManager($customer);
                                },
            K::ROLE_PROVIDER => function(User $user) {
                                    $provider = $user instanceof Provider ?
                                                $user : $user->as_a('provider');
                                    return new ProviderAccountManager($provider);
                                },
        ];
    }
    
    
    /**
     * Create the specialized account manager for a given user.
     * 
     * @param User $user
     * @return IAccountManager
     */
    public function of(User $user) {
        assert(!is_null($user));
        return $this->managers[$user->role] ($user);
    }
}


// -----------------------------------------------------------------------------


/**
 * This is the base class for all account managers.
 */
abstract class BaseAccountManager implements IAccountManager {
    
    /**
     * The account holder.
     * 
     * @var User
     */
    protected $user;
    
    /**
     * Additional parameters used by managers.
     *
     * @var array
     */
    protected $params = null;

    
    /**
     * Construct a new account manager for the given user.
     * 
     * @param User $user
     */
    public function __construct(User $user) {
        $this->user = $user;
    }

    
    /**
     * Add parameters that may be used when performing account management.
     *
     * @param array $params
     * @return  This account manager instance.
     */
    public function using(array $params) {
        
        $this->params = $params ? ($this->params ?
                                   $this->params += $params :
                                   $this->params = $params) : 
                                   null;
        return $this;
    }
    
    
    /**
     * Register this user with the platform.
     *  
     * This method is the lowest common denominator for registering a user,
     * providing basic functionality used by any type of user account.
     * Specialized managers should override it to implement user-specific
     * registration.
     */
    public function register() {
        
        if(empty($this->user->password) || empty($this->user->username))
           throw new AGException('Cannot register user: missing credentials');

        $this->user->ref = User::make_ref($this->user);
        $this->user->status = $this->user->status ?: K::USER_REGISTERED;
        $this->user->password = Hash::make($this->user->password);
        $this->user->email = $this->user->username;
    	$this->user->country = 'Australia';
        $this->user->save();
    }
    
    
    /**
     * Unregister this user from the platform.
     * 
     * This method performs unregistration of both customers and providers.
     * If overridden, must be called from the overriding methods.
     * @throws AGInvalidStateException
     */
    public function unregister() {

        // Accounts with open tasks cannot be cancelled.

        $tasks = Task::where(function($q) {
                          $q->where('id_users_provider', $this->user->id)
                            ->orWhere('id_users_customer', $this->user->id);
                       })->
    	               where('status','!=',K::TASK_CANCELLED)->
    	               where('status','!=',K::TASK_COMPLETED)->
                       get();
        
        if(!$tasks->isEmpty()) {
           throw new AGInvalidStateException(
                trans('exceptions.account-delete-has-open-tasks')
           );
        }
        
        // Mark the account as scheduled for cancellation.
        $this->user->cancelled_at = Hey::now_to_db_datetime();
        $this->user->save();
    }
    
    
    /**
     * Verify this user's email. The email verification process is currently
     * the same for all users, so this is the right place to be.
     * 
     * @return boolean
     * @throws AGAccountException
     */
    public function verify_email($token = null) {
        
        // If the user email is not verified and no verification token has been
        // issued then issue one. This is the case for new users and the verification
        // email will be sent in the user sign up event handler.
        if(!$this->user->email_verified && !$this->user->verification->ev_token) {
            $this->issue_new_ev_token();
            return false;
        }
        // If the user email is not verified and a verification token has been issued
        // then we have to check for a user provided token to verify it, or a user
        // request to resend a new one.
        else 
        if(!$this->user->email_verified && $this->user->verification->ev_token) {
            
            assert(!empty($token));
            
            if($token !== 'resend') {
                // If the issued token is expired, stop here.
                if($this->is_ev_token_expired()) {
                   throw new AGAccountException(trans('exceptions.account-evlink-expired', [
                       'link-resend' => Alert::make_link('Resend email', url('user/email/verify/resend')),
                   ]));
                }
                // Verify the token.
                if($token !== $this->user->verification->ev_token) {
                   throw new AGAccountException(trans('exceptions.account-evlink-invalid'));
                }
                // Verify the email and invalidate the token.
                $this->user->email_verified = true;
                $this->user->verification->ev_token = null;
                $this->user->verification->ev_token_ts = null;
            }
            else {
                // If the current token is not yet expired, don't issue a new one.
                if(!$this->is_ev_token_expired()) {
                   throw new AGAccountException(trans('exceptions.account-evlink-not-expired'));
                }
                $this->issue_new_ev_token(true);
                return false;
            }
        }
        return true;
    }
    
    
    /**
     * Reset this user's account password.
     * 
     * @param string $password
     * @param sring $token
     * @throws AGAccountException
     * @throws AGValidationException
     */
    public function reset_password($password = null, $token = null) {
        
        if(!$this->user) return;
        
        // If there is no issued token, issue a new one.
        if(!$this->user->verification->pr_token) {
            $this->issue_new_pr_token(true);
        }
        // If there is an issued token and we get a token and a password,
        // do the verification and reset the password.
        else if($token && $password) {
            // If the issued token is expired, stop here.
            if($this->is_pr_token_expired()) {
               throw new AGAccountException(trans('exceptions.account-prlink-expired', [
                   'link-resend' => Alert::make_link('Go to login page', url('user/login')),
               ]));
            }
            // Verify the token.
            if($token !== $this->user->verification->pr_token) {
               throw new AGAccountException(trans('exceptions.account-link-invalid'));
            }
            // Reset the password and invalidate the token.
            Validate::password($password);
            $this->user->password = Hash::make($password);
            $this->user->verification->pr_token = null;
            $this->user->verification->pr_token_ts = null;
        }
        // If there is an issued token and we don't get any parameter, then this is
        // a request for a new password reset link. Check if it can be re-issued.
        else if(!$token && !$password) {
                
            if(!$this->is_pr_token_expired()) {
               throw new AGAccountException(trans('exceptions.account-prlink-not-expired'));
            }
            $this->issue_new_pr_token(true);
        }
        // Any other case should be an invalid request.
        else {
            throw new AGValidationException;
        }
        
        // Save changes, if any.
        DB::transaction(function() {
            $this->user->push();
        });
    }
    
    
    // HELPERS

    
    /**
     * Generate a verification token.
     * 
     * @return string
     */
    protected function get_token() {
        
        return Hey::MAC( $this->user->username.'_'.str_random(32) );
    }
    
    
    /**
     * Issue a new email verification token for this user.
     * 
     * @param bool $send_email
     */
    protected function issue_new_ev_token($send_email = false) {
        
        $this->user->verification->ev_token = $this->get_token();
        $this->user->verification->ev_token_ts = Hey::now_to_db_datetime();
        
        if($send_email)
           $this->send_verification_email();
    }
    
    
    /**
     * Check whether the user's email verification token is expired.
     * 
     * @return bool
     */
    protected function is_ev_token_expired() {
        
        return (new \DateTime) > 
               (new \DateTime($this->user->verification->ev_token_ts))->add
               (new \DateInterval(Hey::get_app('intervals.account.ev_token_expired')));
    }
    
    
    /**
     * Send the verification email to this user.
     */
    protected function send_verification_email() {
        
        // To send the verification/activation email we just fire the
        // EventUserSignup event, which will push a WelcomeUser job in the queue.
        Event::fire(new EventUserSignup($this->user->id,
                                        $this->user->verification->ev_token));
        
        app('session')->flash('alerts', trans('messages.email-verification-sent', [
            'user-email' => $this->user->email,
        ]));
    }
    
    
    /**
     * Issue a new email verification token for this user.
     * 
     * @param bool $send_email
     */
    protected function issue_new_pr_token($send_email = false) {
        
        $this->user->verification->pr_token = $this->get_token();
        $this->user->verification->pr_token_ts = Hey::now_to_db_datetime();
        
        if($send_email)
           $this->send_password_reset_email();
    }
    
    
    /**
     * Check whether the user's email verification token is expired.
     * 
     * @return bool
     */
    protected function is_pr_token_expired() {
        
        return (new \DateTime) > 
               (new \DateTime($this->user->verification->pr_token_ts))->add
               (new \DateInterval(Hey::get_app('intervals.account.pr_token_expired')));
    }
    
    
    /**
     * Send the verification email to this user.
     */
    protected function send_password_reset_email() {
        
        dispatch( (new \App\Jobs\ResetPassword ($this->user->id,
                                                $this->user->verification->pr_token))->
                   onConnection('system')->
                   onQueue('account') );
    }
    
}


// -----------------------------------------------------------------------------


/**
 * This class implements an account manager for customers.
 */
class CustomerAccountManager extends BaseAccountManager {
    
    
    public function __construct(Customer $customer) {
        parent::__construct($customer);
    }
    
    
    public function register() {
        
        $customer = $this->user;
        
    	DB::transaction(function () use ($customer) {
            parent::register();
            $customer->add('settings', new CustomerSettings);
            $customer->add('verification', new UserVerification);
            app('account')->of($customer)->create_balance();
            $this->verify_email();
            $customer->push();
            Event::fire(new EventUserSignup($customer->id,
                                            $customer->verification->ev_token));
    	});
    }
    
    
    public function verify() {
        
        $customer = $this->user;
        
        // If the provider has not verified its email and there is a token in the
        // parameters then verify the email.
        if($customer->is_registered() && !$customer->email_verified && 
           isset($this->params['ev_token']))
        {
//           $customer->status = K::USER_ONBOARDING;
           try {
               if($this->verify_email( $this->params['ev_token'] )) {
                  $customer->status = K::USER_ACTIVE;
                  app('session')->flash('alerts', trans('messages.customer-active'));
               }
           }
           finally { DB::transaction(function() { $this->user->push(); }); }
        }
        // If the customer is active there is currently nothing to verify ...
        else if($customer->is_active()) {
           //
        }
    }
    
    
    public function get_alerts() {
        
        $customer = $this->user;
        $session = app('session');
        
        $alerts = [];

        // Check if there are any alerts in the session (alerts in sessions
        // are flashed text messages).
        if($session && $session->has('alerts')) {
           $alerts[] = new Alert( $session->get('alerts') );
        }
        
        if($customer->is_registered() || $customer->is_onboarding()) {
           $alerts[] = new Alert( trans('messages.email-unverified', [
               'user-email'        => $customer->email,
               'link-email-resend' => Alert::make_link('Resend email',
                                                       url('user/email/verify/resend')),
           ]) );
        }
        else if(!$customer->is_active()) {
            //
        }
        
        return $alerts;
    }
    
    
    public function unregister() {
    }
    
}


// -----------------------------------------------------------------------------


/**
 * This class implements an account manager for providers.
 */
class ProviderAccountManager extends BaseAccountManager {
    
    
    public function __construct(Provider $provider) {
        parent::__construct($provider);
    }

    
    public function register() {
        
        $provider = $this->user;
        
        // Map the given services to database records.
        $services = array_map(function ($categories) {
           return ['categories' => $categories ?: null];
        }, $this->params['services']);

        // Register the new provider and create related resources.
        DB::transaction(function () use ($provider, $services) {
           parent::register();
           $provider->add('settings', new ProviderSettings);
           $provider->add('verification', new UserVerification);
           $provider->add('identity', new ProviderIdentity);
           $provider->services()->attach($services);
           app('account')->of($provider)->create_balance();
           $provider->get_identity_manager()->require_info();
           $this->verify_email();
           $provider->push();
           Event::fire(new EventUserSignup($provider->id,
                                           $provider->verification->ev_token));
        });
    }
    
    
    public function verify() {
        
        $provider = $this->user;
        
        // If the provider has not verified its email and there is a token in the
        // parameters then verify the email.
        if($provider->is_registered() && !$provider->email_verified && 
           isset($this->params['ev_token']))
        {
           try {
               if($this->verify_email( $this->params['ev_token'] )) {
                  //$this->open_payment_account();
                  app('session')->flash('alerts', trans('messages.email-verified'));
               }
           }
           finally { DB::transaction(function() { $this->user->push(); }); }
        }
        // If the provider is active there is currently nothing to verify ...
        else if($provider->is_active()) {
           //
        }
    }
    
    
    public function get_alerts() {
        
        $provider = $this->user;
        $session = app('session');
        
        $alerts = [];

        // Check if there are any alerts in the session (alerts in sessions
        // are flashed text messages).
        if($session && $session->has('alerts')) {
           $alerts[] = new Alert( $session->get('alerts') );
        }
        
        // Rise alerts based on user account status.
        if($provider->is_registered() && !$provider->email_verified) {
           $alerts[] = new Alert( trans('messages.email-unverified', [
               'user-email'    => $provider->email,
               'link-email-resend' => Alert::make_link('here', url('user/email/verify/resend')),
           ]) );
        }
        else if(!$provider->is_active()) {

            if($provider->is_verifying()) {
               $alerts[] = new Alert( trans('messages.provider-verifying') );
            }
            else if($provider->is_inquired()) {
               $alerts[] = new Alert( trans('messages.provider-inquired', [
                   'link-provider-settings' => Alert::make_link('here',
                                               url('provider/settings/?view=account-settings')),
               ]) );
            }
            else if($provider->is_unverified()) {
               $alerts[] = new Alert( trans('messages.provider-unverified') );
            }
            else {
               $alerts[] = new Alert( trans('messages.provider-not-active', [
                   'link-provider-settings' => Alert::make_link('here',
                                               url('provider/settings/?view=account-settings')),
               ]) );
            }
        }
        else if($provider->is_active()) {
            //
        }
        return $alerts;
    }
    
    
    public function unregister() {
        
        $provider = $this->user;
        
        DB::transaction(function () use ($provider) {
            
            parent::unregister();
            
            // Provider-specific unregistration here...
            
            // TODO Check the consistency of data after unregistration.
            
            Event::fire(new EventUserCancelled($provider->id));
        });
    }
    
    
    // HELPERS
    
    /*
    protected function open_payment_account() {
        
        $provider = $this->user->as_a('provider');
        
        assert(isset($this->params['ip']));
        
        $account = app('account')->of($provider);
        
        DB::transaction(function () use ($provider, $account) {

            $provider->set_data('request_ip', $this->params['ip']);

        });
    }
    */
    
}
