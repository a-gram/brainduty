<?php

namespace App;

use Illuminate\Support\Collection;
use App\Interfaces\ITransaction;
use App\Interfaces\IAccount;
use App\Interfaces\IMarketplaceProvider;
use App\Models\User;
use App\Models\Provider;
use App\Models\Customer;
use App\Models\Task;
use App\Models\Transaction;
use App\Models\UserBalance;
use App\Models\ProviderIdentity;
use App\Models\IdentityDocument;
use App\Hey;
use App\K;
use App\Exceptions\AGException;
use App\Exceptions\AGPaymentException;
use App\Exceptions\AGTransferException;
use App\Exceptions\AGInvalidStateException;
use App,DB;


/**
 * This is the base class from which all transaction types must derive.
 */
abstract class BaseTransaction extends Transaction implements ITransaction {
    
    /**
     * The debited user.
     * 
     * @var User
     */
    protected $sender;

    /**
     * The credited user.
     * 
     * @var User
     */
    protected $recipient;

    /**
     * The subject of the transaction. This may be the amount itself or
     * something else from which the amount can be computed.
     * 
     * @var number|Task|Collection|mixed
     */
    protected $subject;

    /**
     * The marketplace provider instance.
     * 
     * @var IMarketplaceProvider
     */
    protected $market;

    /**
     *
     * @var IAccount
     */
    protected $sender_account;
    
    /**
     *
     * @var IAccount
     */
    protected $recipient_account;
    
    /**
     * Additional parameters necessary to execute the transaction.
     * 
     * @var array
     */
    protected $params = [];
    
    
    /**
     * This flag indicates whether the transaction is just an entry that does not
     * actually executes movements of funds (for example to fix errors or record 
     * transactions executed asynchronously).
     * 
     * @var boolean
     */
    protected $executes = true;


    /**
     * Construct a new transaction.
     *
     * @param User $sender
     * @param User $recipient
     * @param mixed $subject
     * @param IAccount $sender_account
     * @param IAccount $recipient_account
     * @param array $params
     * @param IMarketplaceProvider $market
     */
    public function __construct(User $sender,
                                User $recipient,
                                $subject,
                                $sender_account,
                                $recipient_account,
                                array $params,
                                IMarketplaceProvider $market) 
    {
        parent::__construct();
        $this->sender = $sender;
        $this->recipient = $recipient;
        $this->subject = $subject;
        $this->sender_account = $sender_account;
        $this->recipient_account = $recipient_account;
        $this->market = $market;
        $this->using($params);
    }


    /**
     * Set the subject of the transaction.
     * 
     * @param mixed $subject This may be anything that is 'chargeable',
     * typically a Task but can be something else, or that gives rise to
     * a fund transfer, such as a withdrawal.
     * @return Transaction This transaction instance.
     */
    public function for_this($subject) {
        $this->subject = $subject;
        return $this;
    }


    /**
     * Add parameters that are used when executing the transaction.
     * Existing parameters with the same name will be overwritten.
     * 
     * @param array $params The parameters to be used when executing the charge.
     * @return Transaction This Charge instance.
     */
    public function using(array $params) {
        $this->params = array_merge($this->params, $params);
        return $this;
    }
    
    
    /**
     * Calling this method causes the transaction not to be executed but just
     * recorded in the database.
     * 
     * @return Transaction
     */
    public function log_only() {
        $this->executes = false;
        return $this;
    }
    
    
    /**
     * Get the parameters for this transaction.
     * 
     * @return array
     */
    public function params() {
        return $this->params;
    }
    

    /**
     * Execute the transaction.
     * 
     * @throws AGException
     */
    public function execute() {
        // Two additional parameters must always be provided for security reasons:
        // 1 - IP address of the client requesting the charge.
        // 2 - Whether the client request was authenticated.
        // If this parameters are missing then they will default to the IP of the
        // server and an authentication value of TRUE.
        if(!isset($this->params['request_id'], $this->params['request_auth'])) {
           $this->using([
               'request_id' => env('APP_IP'),
               'request_auth' => true
           ]);
        }
    }
    
}


// -----------------------------------------------------------------------------


class Charge extends BaseTransaction {

    /**
     * The payment method to be used for this charge.
     * 
     * @var Payment
     */
    protected $payment = null;

    /**
     * Flag indicating whether the charge should 'capture' the funds or just
     * putting them on hold (authorization).
     * 
     * @var boolean
     */
    protected $captured = true;
    

    /**
     * Construct a new charge.
     * 
     * @param User $sender
     * @param User $recipient
     * @param mixed $subject
     * @param IAccount $sender_account
     * @param IAccount $recipient_account
     * @param array $params
     * @param IMarketplaceProvider $market
     */
    public function __construct(User $sender,
                                User $recipient,
                                $subject,
                                $sender_account,
                                $recipient_account,
                                array $params,
                                IMarketplaceProvider $market)
    {
        parent::__construct($sender,
                            $recipient, 
                            $subject, 
                            $sender_account,
                            $recipient_account,
                            $params, 
                            $market);
        
        $this->type = K::TXN_TYPE_CHARGE;
    }


    /**
     * Charge the customer using the specified payment method.
     * 
     * @param App\Models\Payment $payment The payment method to be used for  the charge.
     * If not specified, the customer's default payment method will be used.
     * @return Charge This Charge instance.
     */
    public function pay_by(App\Models\Payment $payment) {
        $this->payment = $payment;
        return $this;
    }
    
    
    /**
     * Set the charge not to capture the funds but to just put them on hold.
     * (authorization).
     * 
     * @return Charge This Charge instance.
     */
    public function hold() {
        $this->captured = false;
        return $this;
    }


    /**
     * Same as hold(), for those who love credit card jargon.
     *
     * @return Charge This Charge instance.
     */
    public function authorize() {
        $this->hold();
    }
    
    
    /**
     * Capture the charge if it was previously put on hold (authorized).
     */
    public function capture() {
        // TODO
    }
    
    
    /**
     * In case of failure, determine whether the charge can be reattempted.
     * 
     * @return boolean
     */
    public function can_retry() {
        $error = Hey::getval($this->params, 'error');
        return $this->market->should_retry_charge($error);
    }
    
    
    /**
     * Execute the charge.
     */
    public function execute() {
        
        parent::execute();
        
        // Provider charges customer.
        if($this->sender->is_customer() && $this->recipient->is_provider()) {
           return $this->execute_charge_customer();
        }
        else {
           Hey::bug_here();
        }
    }
    

    /**
     * Charge a customer's payment method.
     * 
     * @return Transaction
     * @throws AGException
     */
    protected function execute_charge_customer() {
        
        // NOTE: The default payment and payout method records are currently not
        //       used as customers and providers are charged and paid using their
        //       ids (customer and provider account ids).
        
        // If a payment method is not specified then use the payer's default.
        if(!$this->payment) {
            $this->payment = $this->sender->default_payment_method();
        }
        
        // Get the provider's default payout method.
        $provider_payout = $this->recipient->default_payout_method();

        assert($this->payment);
        //assert($provider_payout);
        

        // -----------------------------------------------------------------
        //    Here is where the accountant determines the price to charge.
        // -----------------------------------------------------------------
        

        // The amount debited to the customer for the services.
        $amount = 0;

        // If the charge is for a task, then we need to compute the charge 
        // amount from the services associated with the task.
        if($this->subject instanceof Task) {
           
           $description = 'Payment for duty';
           $services = $this->subject->get_services();
           
           foreach ($services as $service) {
                    $amount += $service->amount;
           }
        }
        // If the charge is for some other services, then the subject should
        // be a collection of Service records.
        else if($this->subject instanceof Collection) {
           
           $description = '';
           $services = $this->subject;
           
           foreach ($services as $i=>$service) {
                   $amount += $service->price;
                   $description .= ($i?', ':'').$service->description;
           }
        }
        // This should never happen ...
        else {
           Hey::bug_here($this->subject);
        }
        
        // TODO Me may want to assign an individual commission rate to
        // each provider based on its circumnstances
        // $this->recipient->fee

        // Compute all fees to deduct from the payment. These include the
        // platform commission and other costs to pass on to the provider and
        // will form the 'application fee' in Stripe.
        // If there are fees specified for this payment then use them,
        // otherwise use the default platform fees.
        $fees_var = Hey::getval($this->params, 'fees.var') ?:
                    array_sum(Hey::get_app('fees.service.var'));
        
        $fees_fix = Hey::getval($this->params, 'fees.fix') ?:
                    array_sum(Hey::get_app('fees.service.fix'));

        $fees = (int) round( $amount * $fees_var ) + $fees_fix;
        
        // Compute all taxes to withhold from the payment, if any.
        $taxes_var = array_sum( Hey::get_app('taxes.payments.var') );
        $taxes_fix = array_sum( Hey::get_app('taxes.payments.fix') );
        
        $taxes = (int) round( $amount * $taxes_var ) + $taxes_fix;

        // The amount credited to the provider.
        $amount_bal = $amount - $fees - $taxes;
        
        
        // -----------------------------------------------------------------

        
        // If for some reason the credited amount cannot cover all the costs
        // then we should abort the transaction.
        if($amount_bal < 0)
           throw new AGPaymentException('Cannot execute payment : insufficient amount');
        

        $this->amount_gross   =  $amount;
        $this->amount_fee     =  $fees;
        $this->amount_tax     =  $taxes;
        $this->amount_bal     =  $amount_bal;
        $this->status         =  K::TXN_PENDING;
        $this->is_captured    =  $this->captured;
        $this->currency       =  Hey::get_app('currency');
        $this->description    =  $description;
        $this->request_id     =  $this->params['request_id'];
        $this->request_auth   =  $this->params['request_auth'];
        $this->settle_method  =  $this->payment->method;
        $this->id_sender      =  $this->sender->id;
        $this->id_recipient   =  $this->recipient->id;
        $this->ref            =  Transaction::make_ref($this);
        $this->clear_datetime =  $this->set_clear_datetime();

        $this->validate();

        $customer_n_payment = $this->sender->attributesToArray();
        $customer_n_payment['payment'] = $this->payment->attributesToArray();
        $provider_n_payout = $this->recipient->attributesToArray();
        //$provider_n_payout['payout'] =  $provider_payout->attributesToArray();

        // Charge details.
        $charge = [
            'transaction'  =>  $this->attributesToArray(),
            'customer'     =>  $customer_n_payment,
            'provider'     =>  $provider_n_payout,
            'params'       =>  $this->params,
        ];
        
        if($this->executes) {
           $payment = $this->market->bill($charge);
           $this->fill($payment);
        }
        
        // NOTE: what is returned here is not the charge but the payment
        //       made to the provider for this charge.
        return $this;
    }

}


// -----------------------------------------------------------------------------


class Transfer extends BaseTransaction {

    /**
     * The payout method to be used for this transfer. This is only set if the
     * transfer is to an external payout account (bank, e-wallet, etc.).
     *
     * @var App\Models\Payout
     */
    protected $payout = null;
    
    
    /**
     * Create a new transfer.
     * 
     * @param User $sender
     * @param User $recipient
     * @param type $subject
     * @param IAccount $sender_account
     * @param IAccount $recipient_account
     * @param array $params
     * @param IMarketplaceProvider $market
     */
    public function __construct(User $sender,
                                User $recipient,
                                $subject,
                                $sender_account,
                                $recipient_account,
                                array $params,
                                IMarketplaceProvider $market) 
    {
        parent::__construct($sender,
                            $recipient, 
                            $subject, 
                            $sender_account,
                            $recipient_account,
                            $params, 
                            $market);
        
        $this->type = K::TXN_TYPE_TRANSFER;
    }


    /**
     * Transfer the funds using the specified payout method.
     *
     * @param App\Models\Payout $payout The payout method to be used for the transfer. 
     * If not specified, the recipient's default payout method will be used.
     * @return Transfer This Transfer instance.
     */
    public function send_by(App\Models\Payout $payout) {
        $this->payout = $payout;
        return $this;
    }
    
    
    /**
     * Execute the transfer.
     *
     * @return Transaction
     * @throws AGException
     */
    public function execute() {
        
        parent::execute();

        // Provider-to-Self transfer.
        // This is the case for 'withdrawals' where funds are transferred from a provider's
        // platform account to a provider's external account (bank, e-wallet, etc.).
        if($this->sender === $this->recipient ) {
           return $this->execute_withdrawal();
        }
        // More transfer cases here ...
        else {
           Hey::bug_here();
        }
    }


    /**
     * Provider withdraw funds.
     * 
     * @return $this
     */
    protected function execute_withdrawal() {

        // Only providers can withdraw funds.
        assert($this->recipient instanceof Provider);

        if(!$this->payout) {
            $this->payout = $this->recipient->default_payout_method();
        }

        assert($this->payout);
        assert(is_numeric($this->subject));
        
        $balance = $this->recipient_account->get_balance(true);

        // We expect the subject be the transfer amount, otherwise use all the available funds.
        $amount = !empty($this->subject) ? $this->subject : $balance->available;
        // Cap to the available amount, just in case ...
        $amount = $amount > $balance->available ? $balance->available : $amount;
        
        $fees_var = array_sum( Hey::get_app('fees.withdraw.var') );
        $fees_fix = array_sum( Hey::get_app('fees.withdraw.fix') );
        $fees = (int) round( $amount * $fees_var ) + $fees_fix;
        
        $taxes_var = array_sum( Hey::get_app('taxes.payouts.var') );
        $taxes_fix = array_sum( Hey::get_app('taxes.payouts.fix') );
        $taxes = (int) round( $amount * $taxes_var ) + $taxes_fix;

        // The amount to be transferred to the provider bank account.
        $amount_bal = $amount - $fees - $taxes;

        
//        if($amount <= 0)
//           throw new AGTransferException('Cannot execute transfer : No available funds.');
//
//        if($amount_net <= 0)
//           throw new AGTransferException('Cannot execute transfer : Insufficient funds.');
        

        $this->amount_gross   =  $amount;
        $this->amount_fee     =  $fees;
        $this->amount_tax     =  $taxes;
        $this->amount_bal     =  $amount_bal;
        $this->currency       =  Hey::get_app('currency');
        $this->status         =  K::TXN_PENDING;
        $this->description    =  'Funds withdrawal';
        $this->request_id     =  $this->params['request_id'];
        $this->request_auth   =  $this->params['request_auth'];
        $this->settle_method  =  $this->payout->method;
        $this->id_sender      =  $this->sender->id;
        $this->id_recipient   =  $this->recipient->id;
        $this->ref            =  Transaction::make_ref($this);
        //$this->clear_datetime =  $this->set_clear_datetime();

        $this->validate();

        $provider_n_payout = $this->recipient->attributesToArray();
        $provider_n_payout['payout'] =  $this->payout->attributesToArray();

        // transfer details.
        $funds = [
            'transaction'   =>  $this->attributesToArray(),
            'provider'      =>  $provider_n_payout,
            'params'        =>  $this->params,
            'transfer_type' =>  'withdrawal',
        ];

        if ($this->executes) {
            $transfer = $this->market->transfer($funds);
            $this->fill($transfer);
        }
        
        return $this;		
    }
    
}


// -----------------------------------------------------------------------------


class Refund extends BaseTransaction {
    
    /**
     * The portion of the original charge to be refunded.
     * 
     * @var number
     */
    protected $amount = 0;
    
    
    /**
     * The reason for refunding.
     * 
     * @var string|null
     */
    protected $reason = null;
    
    
    /**
     * Construct a new refund.
     * 
     * @param User $sender
     * @param User $recipient
     * @param type $subject
     * @param IAccount $sender_account
     * @param IAccount $recipient_account
     * @param array $params
     * @param IMarketplaceProvider $market
     */
    public function __construct(User $sender,
                                User $recipient,
                                $subject,
                                $sender_account,
                                $recipient_account,
                                array $params,
                                IMarketplaceProvider $market) 
    {
        parent::__construct($sender,
                            $recipient, 
                            $subject, 
                            $sender_account,
                            $recipient_account,
                            $params, 
                            $market);
        
        $this->type = K::TXN_TYPE_REFUND;
    }
    
    
    /**
     * 
     * @param number $amount
     * @return Refund
     * @throws AGException
     */
    public function amount($amount) {
        $this->amount = $amount;
        return $this;
    }
    
    
    /**
     * 
     * @param string $reason
     * @return Refund
     */
    public function reason($reason) {
        $this->reason = $reason;
        return $this;
    }
    
    
    /**
     * Execute the refund based on the type of sender and recipient.
     */
    public function execute() {
        
        parent::execute();
        
        // Provider refunds customer.
        if($this->sender->is_provider() && $this->recipient->is_customer()) {
           return $this->execute_provider_refund_customer();
        }
        else {
           Hey::bug_here();
        }
    }

    
    /**
     * Provider refund a customer.
     * 
     * Customers are refunded by the platform by returning the amount of a charge
     * to the customer through the used payment method.
     * 
     * @return $this
     * @throws AGPaymentException
     */
    protected function execute_provider_refund_customer() {
        
        assert($this->subject instanceof Transaction);
        assert($this->subject->type === K::TXN_TYPE_CHARGE || 
               $this->subject->type === K::TXN_TYPE_PAYMENT);
        
        // If no amount is specified, we assume the whole charge amount refunded.
        $amount = $this->amount ?: $this->subject->amount_gross;
        
        // If for some reason the refunded amount is invalid we should abort the transaction.
        if($amount <=0 || $amount > $this->subject->amount_gross)
           throw new AGPaymentException('Cannot execute refund : invalid amount');
        
        // Refunds will come out of the platform since charges are created there
        // and we don't want (for now) to bear the cost, so we get the refund amount 
        // back from the provider payment and refund the app fee.
        $this->using([
            'reverse_payment' => true,
            'refund_app_fee'  => true,
        ]);
        
        // No fees or taxes on refunds.
        $fees = 0;
        $taxes = 0;
        
        $description = 'Refund for '. $this->subject->description;
        
        $this->amount_gross   =  $amount;
        $this->amount_fee     =  $fees;
        $this->amount_tax     =  $taxes;
        $this->status         =  K::TXN_PENDING;
        $this->currency       =  Hey::get_app('currency');
        $this->description    =  $description;
        $this->settle_method  =  $this->subject->settle_method;
        $this->refunded_txn   =  $this->subject->id;
        $this->request_id     =  $this->params['request_id'];
        $this->request_auth   =  $this->params['request_auth'];
        $this->id_sender      =  $this->sender->id;
        $this->id_recipient   =  $this->recipient->id;
        $this->ref            =  Transaction::make_ref($this);

        $this->validate();

        $customer = $this->recipient->attributesToArray();
        $provider = $this->sender->attributesToArray();

        // Charge details.
        $charge = [
            'refund'       =>  $this->attributesToArray(),
            'transaction'  =>  $this->subject->attributesToArray(),
            'customer'     =>  $customer,
            'provider'     =>  $provider,
            'params'       =>  $this->params,
        ];
        
        if ($this->executes) {
            $refund = $this->market->refund($charge);
            $this->fill($refund);
        }
        
        return $this;
    }
    
}


// -----------------------------------------------------------------------------


class Payment extends BaseTransaction {

    
    /**
     * Create a new payment.
     * 
     * @param User $sender
     * @param User $recipient
     * @param type $subject
     * @param IAccount $sender_account
     * @param IAccount $recipient_account
     * @param array $params
     * @param IMarketplaceProvider $market
     */
    public function __construct(User $sender,
                                User $recipient,
                                $subject,
                                $sender_account,
                                $recipient_account,
                                array $params,
                                IMarketplaceProvider $market) 
    {
        parent::__construct($sender,
                            $recipient, 
                            $subject, 
                            $sender_account,
                            $recipient_account,
                            $params, 
                            $market);
        
        $this->type = K::TXN_TYPE_PAYMENT;
    }

    
    /**
     * Execute the payment.
     *
     * @return Transaction
     * @throws AGException
     */
    public function execute() {
        
        parent::execute();

        // Platform pays provider.
        if($this->sender->is_admin() && $this->recipient->is_provider()) {
           return $this->execute_market_pay_provider();
        }
        // More payment cases here ...
        else {
           Hey::bug_here();
        }
    }
    
    
    /**
     * Platform pay a provider.
     * 
     * Providers are paid by the platform by making a transfer from the platform's
     * account to the provider's account (credit).
     * 
     * @return $this
     * @throws AGPaymentException
     */
    protected function execute_market_pay_provider() {
        
        assert(is_numeric($this->subject) && $this->subject > 0);
        
        $amount = $this->subject;
        
        // If we are executing the transaction make sure there is money for the payment ...
        if($this->executes) {
            
           $balance = $this->sender_account->get_balance(true);
        
           if($amount > $balance->available)
              throw new AGPaymentException('Cannot execute payment: no available funds.');
        }

        $fees_var = Hey::getval($this->params, 'fees.var') ?: 0;
        $fees_fix = Hey::getval($this->params, 'fees.fix') ?: 0;
        $fees = (int) round( $amount * $fees_var ) + $fees_fix;

        $taxes_var = Hey::getval($this->params, 'taxes.var') ?: 0;
        $taxes_fix = Hey::getval($this->params, 'taxes.fix') ?: 0;
        $taxes = (int) round( $amount * $taxes_var ) + $taxes_fix;

        // The amount credited to the provider.
        $amount_bal = $amount - $fees - $taxes;
        
        $description = Hey::getval($this->params, 'description') ?: 'Credit';
        
        $this->amount_gross   =  $amount;
        $this->amount_fee     =  $fees;
        $this->amount_tax     =  $taxes;
        $this->amount_bal     =  $amount_bal;
        $this->currency       =  Hey::get_app('currency');
        $this->status         =  K::TXN_PENDING;
        $this->description    =  $description;
        $this->request_id     =  $this->params['request_id'];
        $this->request_auth   =  $this->params['request_auth'];
        $this->settle_method  =  'balance';
        $this->id_sender      =  $this->sender->id;
        $this->id_recipient   =  $this->recipient->id;
        $this->ref            =  Transaction::make_ref($this);
        $this->clear_datetime =  $this->set_clear_datetime();

        $this->validate();

        $provider = $this->recipient->attributesToArray();

        // transfer details.
        $funds = [
            'transaction'   =>  $this->attributesToArray(),
            'provider'      =>  $provider,
            'params'        =>  $this->params,
            'transfer_type' =>  'credit',
        ];

        if ($this->executes) {
            $transfer = $this->market->transfer($funds);
            $this->fill($transfer);
        }
        
        return $this;
    }
}


// -----------------------------------------------------------------------------


/**
 * This is a factory class that constructs transaction accounts for specified
 * users.
 */
class Account {

    public function __construct() {
    }

    /**
     * A 'factory' method that initializes and returns an Account object for the specified
     * user. Must be called before performing any accounting operations.
     *
     * @param User $user The user to return the account for.
     */
    public function of(User $user) {
        
        if (is_null($user))
           throw new AGException('Invalid entity : null.');
        if ($user instanceof Provider)
           return new ProviderAccount($user);
        if ($user instanceof Customer)
           return new CustomerAccount($user);
        // Handle the case where a generic user instance is given.
        if ($user->is_provider())
           return new ProviderAccount($user->as_a('provider'));
        if ($user->is_customer())
           return new CustomerAccount($user->as_a('customer'));
        if ($user->is_admin())
           return new MarketAccount($user);
        throw new AGException('Unrecognized account entity.');
    }
	
}


// -----------------------------------------------------------------------------


/**
 *  This class represents a transaction account (also referred to as the "payment
 *  account" or the "marketplace account"), not to be confused with the user account.
 *  It deals with anything related to financial transactions, such as payments, 
 *  transfers, refunds, balances, statements, etc.
 *
 */
abstract class BaseAccount implements IAccount {

    /**
     *
     * @var Marketplace
     */
    protected $market;
    
    /**
     *
     * @var User
     */
    protected $holder  = null;
    
    /**
     *
     * @var UserBalance
     */
    protected $balance = null;

    
    public function __construct() {
        $this->market = App::make('marketplace');
    }
    
    
    /**
     * Returns the account holder's instance.
     */
    public function holder() {
        return $this->holder;
    }

    /**
     * Create a new balance record for this account holder's account.
     * This method must be called only once, generally when the user
     * is registered or approved to the platform.
     *
     * @return UserBalance
     */
    public function create_balance() {
        
        $balance = new UserBalance([
                        'id_users'  => $this->holder->id,
                        'total'     => 0,
                        'available' => 0
        ]);
        $balance->save();
        return $balance;
    }


    /**
     * Returns the account holder's balance.
     * 
     * IMPORTANT! This method must be called within a database transaction.
     * 
     * @param bool $poll
     * @return UserBalance
     */
    public function get_balance($poll = false) {
        
        if($this->balance && !$poll)
           return $this->balance;
        
        if(!$this->balance)
            $this->balance = new UserBalance;
        
        if($this->holder->is_customer()) {
           $bal = $this->market->retrieve_customer_balance($this->holder->toArray());
        } else
        if($this->holder->is_provider()) {
           $bal = $this->market->retrieve_merchant_balance($this->holder->toArray());
        } else
        if($this->holder->is_admin()) {
           $bal = $this->market->retrieve_market_balance();
        } else {
           Hey::bug_here();
        }
        return $this->balance->fill($bal);
    }
    
    
    /**
     * Store the given documents based on their type/category and on whether
     * they should be hosted on the platform or uploaded to external services.
     * 
     * @param array $documents An array of document records.
     * @return array An array of Document models.
     */
    public function store_documents(array $documents) {
        
        $stored_docs = [];
        
        foreach ($documents as $document) {
            
            assert(isset($document['is_hosted']));
            assert(isset($document['category']));
            
            if($document['category'] === K::DOCUMENT_CATEGORY_IDENTITY) {
               if($document['is_hosted']) {
                  assert(isset($document['file']));
                  $stored = $this->holder->store_files($document['file'], User::DOCUMENTS_DIR, '');
                  $stored_docs[] = new IdentityDocument( array_merge($document, $stored[0]) );
               }
               else {
                  // Non-hosted identity documents for providers are stored on the
                  // marketplace service provider.
                  if($this->holder->is_provider()) {
                     $document['provider']['ref_ext'] = $this->holder->ref_ext;
                     $stored = $this->market->store_merchant_document($document);
                     $stored_docs[] = new IdentityDocument( array_merge($document, $stored) );
                  } else {
                     // Non-hosted identity documents for non providers ?
                  }
               }
            }
            else {
            }
        }
        return $stored_docs;
    }
    
    
    /**
     * Update this account holder's balance.
     * 
     * IMPORTANT! This method must be called within a database transaction.
     *
     * @param number $total
     * @param number $available
     * @return UserBalance
     */
    public function update_balance($total = 0, $available = 0, $pending = 0) {}
    
    
    /**
     * 
     * @param User $user
     * @return null
     */
    public function charge(User $user) {}
    
    
    /**
     * 
     * @param User $user
     * @return null
     */
    public function refund(User $user) {}
    
    
    /**
     * 
     * @param number $amount
     * @return null
     */
    public function withdraw($amount = 0) {}

    
    /**
     * 
     * @param User $user
     * @param number $amount
     * @return null
     */
    public function pay(User $user, $amount) {}

    
    /**
     * Get transactions.
     * 
     * @return array
     */
    public function get_transactions() {}
    
    
    /**
     * Get a transaction.
     * 
     * @param string $txn_id
     * @return null
     */
    public function get_transaction($txn_id) {}
    
}


// -----------------------------------------------------------------------------



/**
 *  An account for providers. 
 */
class ProviderAccount extends BaseAccount {
		
	
    public function __construct(Provider $provider) {
        parent::__construct();
        $this->holder = $provider;
    }


    /**
     * This method opens a provider/merchant account on the platform, enabling the provider
     * to receive payments on their account and withdraw money. Generally, opening a provider
     * account involves creating at least one payout method (usually a bank account) and
     * the necessary resources on the platform and on the marketplace provider.
     * The resources (records, files, etc.) to be created depend on how the account is
     * managed. It may be entirely managed by a third party service (payment processor),
     * partially managed by the platform and partially by the third party service or entirely
     * managed by the platform (third party service only provides money transfer facilities).
     * This class method takes care of creating the necessary resources to connect the
     * user's account on the platform to the matketplace provider platform.
     * Note, users may be able to perform limited transactions on their accounts but in
     * order to be fully operational the account must be opened. For example, providers
     * may be able to accept payments but will not be able to withdraw the funds until the
     * account is opened.
     * 
     * @param App\Models\Payout $method Payout method to be attached when opening the account.
     * @return array The opened account.
     */
    public function open(/*Payout*/ $method = null) {

        assert($method instanceof App\Models\Payout || $method === null);

        $provider = $this->holder;

        if($provider->ref_ext) {
            throw new AGException
            ('Cannot open payout account : the account is already open.');
        }
        
        // Get the provider's info array and add the payout method.
        $merchant = $provider->attributesToArray();
        $merchant['id'] = $provider->id;
        $merchant['ip'] = $provider->get_data('request_ip');
        $merchant['payout'] = $method ? $method->attributesToArray() : null;

        // Create the payout account on the marketplace provider
        $account = $this->market->create_merchant_account ($merchant);

        // Update the payout account settings with the received results.
        if($method) {
           $method->fill($account['payout']);
           $method->is_primary = true;
           $method->is_open = true;
           
           $provider->payout()->save($method);
        }
        
        $provider->ref_ext = $account['id'];
        $provider->save();

        return $account;
    }

    /**
     * Update the account.
     * 
     * @param string $action The update action to be performed. This is a string in
     * the format <resource>.<operation>.
     * @param array $params Array of parameters used for updating.
     * 
     *        Supported actions:
     * 
     *        1) payout.add:     Add a new payout method to an existing one (e.g. a new bank account).
     *        2) payout.replace: Replace the primary payout method of the specified type with
     *                           a new one (e.g. a primary bank account with another one).
     *        3) payout.remove:  Remove the specified payout method (e.g. remove a bank account).
     *        4) *.*             Update any account information.
     * 
     * @return mixed The updated resource.
     * @throws AGException
     */
    public function update($action, array $params = []) {

        $provider = $this->holder;
        
        if(!$provider->ref_ext) {
            throw new AGException
            ('Cannot update payout account : the account is not open.');
        }
        
        $what = explode('.', $action);
        
        if(count($what) !== 2)
           throw new AGException;
        
        list($target, $operation) = $what;
        
        
        // Update payout info
        if($target === 'payout') {
           
           $payout = Hey::getval($params, 'payout');
           
           assert($payout instanceof App\Models\Payout);
           
           $payout_method = null;
           
           if($operation === 'add') {
               // Add the given method to an already open one. If there is no other
               // method then make this the primary one.
               $payout_method = $payout;
               $payout_method->is_primary = $provider->payout->isEmpty();
               $payout_method->is_open = true;
           }
           else if($operation === 'replace') {

               // The payout method to be replaced is the primary (open) one.
               $payout_method = $provider->payout()->where('method', $payout->method)
                                                   ->where('is_primary', true)
                                                   ->where('is_open', true)
                                                   ->first();

               if(!$payout_method) {
                   throw new AGException
                   ('Cannot update payout account : no open primary method for '.$payout->method);
               }

               $payout_method->fill ($payout->attributesToArray());
               $payout_method->is_primary = true;
               $payout_method->is_open = true;
           }
           else if($operation === 'remove') {
               // Remove the specified payment method. Must be open and not primary.
           }
           else {
               Hey::bug_here($operation);
           }

           // Get the provider's info array and add the payout method.
           $merchant = $provider->attributesToArray();
           $merchant['payout'] = $payout_method->attributesToArray();

           // Update the payout account on the marketplace provider.
           $results = $this->market->update_merchant_account($merchant, $action);

           // Update the payout with the received results.
           $payout_method->fill($results);

           $provider->payout()->save($payout_method);

           return $payout_method;
        }
        else
        // Update any given info
        if($target === '*') {
            
            if($operation === '*') {
               
               $updated_fields = Hey::getval($params, 'fields');
               
               $merchant = $provider->attributesToArray();
               
               if(in_array('bank', $updated_fields)) {
                  $payout = Hey::getval($params, 'payout');
                  $payout->is_primary = true;
                  $payout->is_open = true;
                  $merchant['payout'] = $payout->attributesToArray();
               }
               
               $results = $this->market->update_merchant_account($merchant, $action);
               
               if(isset($results['payout'])) {
                  $payout->fill($results['payout']);
                  $provider->payout()->save($payout);
               }
               
               return $results;
            }
        }
        else {
           Hey::bug_here($target);
        }
        
    }


    /**
     *  Close the account.
     * 
     */
    public function close() {
        
        $provider = $this->holder;
        
        if(!$provider->ref_ext) {
            return;
        }
        
        // Check for any pending balance amounts
        $balance = $this->get_balance(true);
        
        if($balance->available || $balance->pending) {
           throw new AGInvalidStateException('Cannot close the account: balance is not zero.');
        }
        
        $account = $this->market->delete_merchant_account($provider->toArray());
        
        $provider->payout()->delete();
        
        $provider->settings()->update([
            'default_payout_method' => null,
        ]);
        
        $provider->setAttribute('ref_ext', null)->save();
                
        return $account['deleted'];
    }
    
    
    /**
     *  Construct a charge object that can be used to charge the specified
     *  customer.
     *  
     *  @param User $customer
     *  @return Charge
     */
    public function charge(User $customer) {
        
        assert($customer instanceof Customer);
        
        return new Charge($customer,
                          $this->holder,
                          null,
                          new CustomerAccount($customer),
                          $this,
                          [],
                          $this->market);
    }


    /**
     *  Construct a transfer object that can be used to withdraw funds from 
     *  this provider's account.
     *  
     *  @param number $amount The amount to be withdrawn. If not specified,
     *  then the whole available balance will be withdrawn (default behaviour).
     *  @return Transfer
     */
    public function withdraw($amount = 0) {
        
        return new Transfer($this->holder,
                            $this->holder,
                            $amount,
                            null,
                            $this,
                            [],
                            $this->market);
    }


    /**
     *  Construct a refund object that can be used to return funds to a customer.
     *
     *  @param User
     *  @return Refund
     */
    public function refund(User $customer) {
        
        assert($customer instanceof Customer);
        
        return new Refund($this->holder,
                          $customer,
                          null,
                          $this,
                          new CustomerAccount($customer),
                          [], 
                          $this->market);
    }
    
    
    public function get_transactions() {
        return $this->market->retrieve_merchant_transactions($this->holder->toArray());
    }
    
    
    public function get_transaction($txn_id) {
        return null;
    }

}


// -----------------------------------------------------------------------------


/**
 *  An account for customers. This account represents and manages all the sources of
 *  funds used by a customer to make payments, such as credit cards, virtual wallets
 *  (PayPal et al.), bank accounts, etc.
 */
class CustomerAccount extends BaseAccount {


    public function __construct(Customer $customer) {
        parent::__construct();
        $this->holder = $customer;
    }

    /**
     *  This method opens a customer payment account with a specified method that enables
     *  customer to make payments for the services received. A payment account may have
     *  several payment methods (sources of funds). This method 'opens' a specified payment
     *  source by creating all the resources needed to use that specific source of funds
     *  (card, paypal, etc.).
     *  Due to the sensitive nature of these information, they will not usualy be kept
     *  on the platform but securely vaulted on a marketplave provider.
     *  
     *  @param App\Models\Payment $method The payment method to be attached when openig the account.
     *  @return array The opened account.
     */
    public function open(/*Payment*/ $method) {

        assert($method instanceof App\Models\Payment);

        $customer = $this->holder;

        if($customer->ref_ext) {
            throw new AGException
            ('Cannot open payment account : the account is already open.');
        }

        // Get the customer's info array and add the payment method.
        $mp_customer = $customer->attributesToArray();
        $mp_customer['id'] = $customer->id;
        $mp_customer['ip'] = $customer->get_data('request_ip');
        $mp_customer['payment'] = $method->attributesToArray();

        // Create the payment account on the marketplace provider
        $account = $this->market->create_customer_account ($mp_customer);

        // TODO Here we may also verify the payment method by checking that it
        // actually works (for example by charging a small amount) ?
        //

        // Update the payment settings with the received results.
        $method->fill($account['payment']);
        $method->requestor = $customer->get_data('request_ip');
        $method->cycle = K::PAYMENT_CYCLE_RECURRING;
        $method->is_primary = true;
        $method->is_open = true;
        
        $customer->ref_ext = $account['id'];

        $customer->save();
        $customer->payment()->save($method);

        return $account;
    }


    /**
     * Update the account.
     * 
     * @param string $action The action to be performed when updating the account.
     * This is a string in the format <resource>.<operation>.
     * @param array $params Array of parameters to be used when updating.
     * 
     *        Supported actions:
     * 
     *        1) payment.add:     Add a new payment method to an existing one (e.g. a new card).
     *        2) payment.replace: Replace the primary payment method of the specified type with
     *                            a new one (e.g. a primary card with another one).
     *        3) payment.remove:  Remove the specified payment method (e.g. remove a card). Note
     *                            this doesn't remove the whole payment method type (see close()).
     * 
     * @return mixed The updated resource.
     * @throws AGException
     */
    public function update($action, array $params = []) {

        $customer = $this->holder;
        
        if(!$customer->ref_ext) {
            throw new AGException
            ('Cannot update payment account : the account is not open.');
        }
        
        $what = explode('.', $action);
        
        if(count($what) !== 2)
           throw new AGException;
        
        list($target, $operation) = $what;
        
        // Update payout info
        if($target === 'payment') {
            
           $payment = Hey::getval($params, 'payment');

           assert($payment instanceof App\Models\Payment);

           $payment_method = null;
           
           if($operation === 'add') {
               // Add the given method to an already open one.
               // If specified so, the new method will become the primary one within
               // this method type and all others will be made non primary.
           }
           else if($operation === 'replace') {

               // The payment method to be replaced is the primary (open) one.
               $payment_method = $customer->payment()->where('method', $payment->method)
                                          ->where('is_primary', true)
                                          ->where('is_open', true)
                                          ->first();

               if(!$payment_method) {
                   throw new AGException
                   ('Cannot update payment : no open primary method for '.$payment->method);
               }

               $payment_method->fill ($payment->attributesToArray());
               $payment_method->requestor = $customer->get_data('request_ip');
               $payment_method->cycle = K::PAYMENT_CYCLE_RECURRING;
               $payment_method->is_primary = true;
               $payment_method->is_open = true;
           }
           else if($operation === 'remove') {
               // Remove the specified payment method. Must be open.
               // If the method is primary then rise an exception.
           }
           else {
               Hey::bug_here($operation);
           }

           // Get the customer's info array and add the payment method.
           $details = $customer->attributesToArray();
           $details['payment'] = $payment_method->attributesToArray();

           // Update the payment account on the marketplace provider.
           $results = $this->market->update_customer_account($details, $action);

           // Update the payment with the received results.
           $payment_method->fill($results);

           $customer->payment()->save($payment_method);

           return $payment_method;
        }
        else {
           Hey::bug_here($target);
        }

    }


    /**
     *  Close the account.
     */
    public function close() {
        
        $customer = $this->holder;
        
        if(!$customer->ref_ext) {
            return;
        }

        // Check for any pending balance amounts
        $balance = $this->get_balance(true);
        
        if($balance->available || $balance->pending) {
           throw new AGInvalidStateException('Cannot close the account: balance is not zero.');
        }
        
        $account = $this->market->delete_customer_account($customer->toArray());
        
        $customer->payment()->delete();
        
        $customer->settings()->update([
            'default_payment_method' => null,
        ]);
        
        $customer->setAttribute('ref_ext', null)->save();
                
        return $account['deleted'];
    }
    
}


// -----------------------------------------------------------------------------


/**
 *  This class represents the platform's account.
 */
class MarketAccount extends BaseAccount {


    public function __construct(User $admin) {
        parent::__construct();
        $this->holder = $admin;
    }

    /**
     *  
     */
    public function open($method) {

    }


    /**
     * 
     * @return Payment The updated payment method.
     * @throws AGException
     */
    public function update($action, array $params = []) {

    }


    /**
     *  Close the account.
     */
    public function close() {
        
    }
    
    
    /**
     * Construct a payment object that can be used to pay money to a user using
     * the platform account.
     * 
     * @param User $user
     * @param type $amount
     * @return \App\Payment
     */
    public function pay(User $user, $amount) {
        
        assert($this->holder->is_admin());
        
        return new Payment($this->holder,
                           $user,
                           $amount,
                           $this,
                           null,
                           [],
                           $this->market);
    }
    
}

