<?php

namespace App;

use App\Exceptions\AGException;
use App\Hey;


/**
 * This class implements a system alert. A system alert is a message that is
 * prominently shown on the user client.
 */
class Alert {


    /**
     * The alert type.
     * 
     * @var string
     */
    protected $type;
    
    /**
     * The alert message.
     * 
     * @var string
     */
    protected $message;
    
    /**
     * The html links associated with the alert, if any.
     * 
     * @var array
     */
    protected $links;
    
    const INFO = 'info';
    const WARNING = 'warning';
    const CRITICAL = 'critical';
    
    /**
     * The link placeholder pattern that will be replaced with the provided
     * html links. The # character indicates the link number and must be
     * sequential starting from 1.
     */
    const LINK_TAG = '{{???}}';
    
    /**
     * CSS classes for the links.
     */
    const CLASSES = '';
    
    
    /**
     * Construct a new alert.
     * 
     * @param string $type
     * @param string $message
     * @param array $links
     */
    public function __construct($message,
                                array $links = [],
                                $type = Alert::INFO) {
        $this->type = $type;
        $this->links = $links;
        $this->message = $message;
    }

    
    /**
     * Add a link to this alert.
     * 
     * @param string $tag The name of the link's tag/placeholder.
     * @param array $link The link attributes, as follows:
     * [
     *    'text'    => 'The link text',
     *    'url'     => 'http://some.cool.app',
     *    'classes' => 'some css classes',
     * ]
     * @return type
     */
    public function add_link($tag, array $link) {
        
        if(!isset($link['text'], $link['url']))
            throw new AGException('Invalid parameters');
        
        $link = self::make_link($link['text'],
                                $link['url'],
                                Hey::getval($link, 'classes') ?: '');
        
        $this->links[$tag] = $link;
        return $link;
    }


    /**
     * Create the alert message with all associated links.
     * 
     * @return string The built alert message.
     * @throws AGException
     */
    public function get() {
        
        $alert = $this->message;
        foreach ($this->links as $name => $link){
            $tag = str_replace('???', $name, Alert::LINK_TAG);
            if(Hey::contains($tag, $this->message)) {
               $alert = str_replace($tag, $link, $alert);
            } else {
              throw new AGException('Alert link tag not found : '.$name);
            }
        }
        return $alert;
    }
    
    
    /**
     * Make an html link.
     * 
     * @param type $text
     * @param type $url
     * @param type $classes
     * @return type
     */
    public static function make_link($text, $url, $classes='') {
        return '<a href="'.$url.'" class="'.Alert::CLASSES.$classes.'">'.$text.'</a>';
    }
	
}
