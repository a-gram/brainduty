<?php

namespace App;

use App\Interfaces\IArchive;
use App\Models\User;
use App\Models\Provider;
use App\Models\Customer;
use App\Models\Task;
use App\Models\AGEntity;
use App\K;
use App\Hey;
use App\Exceptions\AGException;
use Storage, File, Config;
use Illuminate\Http\UploadedFile;


/**
 * This library class deals with all the 'archiving' in the system. It stores files in
 * the appropriate way and locations based on entity types and contexts. A clean way to
 * a good separation of concerns preventing to bloat controllers with code that should
 * not be there.
 *
 *  Archive file info record:
 *  [
 *     filename      : the stored file name
 *     path          : the stored file path
 *     size          : the size in bytes
 *     mime          : MIME type
 *     filename_orig : the original name when posted by clients
 *     filename_old  : the temp file name before storing
 *     path_old      : the temp file path before storing
 *  ]
 */
class Archive {

    public function __construct() {
    }

    /**
     * A 'factory' method that initializes and returns an archive object for the
     * specified entity. Must be called before performing any archiving operations.
     *
     * @param Entity $entity The entity to return the archive for.
     */
    public function of(AGEntity $entity) {
        
        assert(!is_null($entity));
        
        if ($entity instanceof Provider)
           return new ProviderArchive($entity);
        if ($entity instanceof Customer)
           return new CustomerArchive($entity);
        // Handle the case where a generic user instance is given.
        if ($entity instanceof User && $entity->is_provider())
           return new ProviderArchive($entity->as_a('provider'));
        if ($entity instanceof User && $entity->is_customer())
           return new CustomerArchive($entity->as_a('customer'));
        
        if ($entity instanceof Task)
           return new TaskArchive($entity);
        
        return new EntityArchive($entity);
    }

    /**
     * Returns the app's local private or public base files directory. These are the 
     * directories where all files for the app are stored.
     *
     * @param boolean $normalized [optional] If false returns a non normalized path.
     * @param boolean $public [optional] Flag indicating whether to get the public dir.
     * @return string The full absolute path to the local base files directory.
     */
    public static function get_local_files_dir($normalized = true, $public = false) {
        $root_dir = Config::get('filesystems.disks.local'.($public?'_public':'').'.root');
        return $normalized ? $root_dir : $root_dir.DIRECTORY_SEPARATOR;
    }

    /**
     * Returns the app's cloud base files directory.
     *
     * @param bool $normalized (Optional) If false returns a non normalized path.
     * @return string The full absolute path to the cloud base files directory.
     */
    public static function get_cloud_files_dir($normalized = true, $public = false) {
        // TODO
        throw new AGException('Not implemented.');
    }
}

/**
 *  This class represents an entity's archive, that is a collection of files that
 *  are related/associated with the entity. Specialized classes know how to organize,
 *  process and retrieve all of the files for a specific entity. Low level filesystem
 *  operations are handled by this base class. Currently using Laravel's Storage::
 *  which does all the dirty filesystem work, but any other component may be used,
 *  even plain raw filesystem calls.
 * 
 *  An entity's archive may be private or public and has the following format:
 * 
 *  <private/public app files root dir>/<archive base dir>/<archive name/id>
 *
 *  NOTE: ALL PATHS ARE RELATIVE TO THE APP'S PRIVATE/PUBLIC FILES DIRECTORY.
 */
abstract class BaseArchive implements IArchive {

    protected $entity = null;
    protected $storage = null;
    protected $is_local = null;
    protected $is_public = false;
    protected $encrypt = false;
    protected $compress = false;
    protected $basedir = null;
    protected $dirprefix = null;


    public function __construct() {
        $this->local();
    }


    /**
     * Set the archive for local storage operations.
     */
    public function local() {
        $this->storage = $this->is_public ? Storage::disk('local_public') : 
                                            Storage::disk('local');
        $this->is_local = true;
        return $this;
    }
    
    
    /**
     * Switch to public archive.
     * 
     * @return $this
     */
    public function pub() {
        $this->storage = $this->is_local ? Storage::disk('local_public') : 
                                           Storage::disk('cloud_public');
        $this->is_public = true;
        return $this;
    }

    
    /**
     * Switch to private archive.
     * 
     * @return $this
     */
    public function pvt() {
        $this->storage = $this->is_local ? Storage::disk('local') : 
                                           Storage::disk('cloud');
        $this->is_public = false;
        return $this;
    }
    

    /**
     * Set the archive for cloud storage operations.
     */
    public function cloud() {
        // TODO
        //$this->storage = $this->is_public ? Storage::disk('cloud_public') : 
        //                                    Storage::disk('cloud');
        //$this->is_local = false;
        //return $this;
        throw new AGException('Not implemented.');
    }


    /**
     * Set the archive for file encryption.
     * 
     * @return IArchive This archive instance.
     */
    public function with_encryption() {
        $this->encrypt = true;
        return $this;
    }
    
    
    /**
     * Set the archive for file compression.
     * 
     * @return IArchive This archive instance.
     */
    public function with_compression() {
        $this->compress = true;
        return $this;
    }

    
    /**
     * This method moves all the given files from their current storage directory to
     * the new specified location, optionally renaming them with unique random names
     * and encrypting them.
     * 
     * @param array $files The files to be moved (instances of Symfony's File class).
     * @param string $to_dir The (relative) directory to move the files to.
     * @param string $with_new_names (optional) Rename the files with unique, random
     * names.
     * @return array An array with the moved files.
     */
    protected function move_all(array $files, $to_dir, $with_new_names = true) {

        assert(!is_null($this->storage));

        $moved_files = [];

        $this->storage->makeDirectory($to_dir);

        $root_files_dir = $this->get_root_dir(false);
        
        // Make the destination dir an absolute path
        $to_dir = $root_files_dir.$to_dir.DIRECTORY_SEPARATOR;

        foreach ($files as $file) {
            
            $moved_file = [];
            // Give the file a new unique random name if so specified, otherwise
            // use the current file name. If a new name is required then for uploaded
            // files the extension must be inferred from the mime type (the server
            // creates .tmp files), otherwise use the file current extension.
            $with_new_name = $with_new_names ?
                             ( $file instanceof UploadedFile ?
                               md5_file($file->getRealPath()).'.'.$file->guessExtension() :
                               md5_file($file->getRealPath()).'.'.$file->getExtension() ) :
                             $file->getFilename();
            
            // Store the old file info that will be returned to the caller.
            $moved_file['filename_old'] = $file->getFilename();
            $moved_file['path_old'] = $file->getRealPath();

            // Determine if the file was an uploaded file, in which case we can get
            // the original name as posted by the client and also the MIME type.
            if ($file instanceof UploadedFile) {
                $moved_file['filename_orig'] = $file->getClientOriginalName();
                $moved_file['mime'] = $file->getMimeType();
            }

            // Using the File class to move the files as Laravel's storage/filesystem
            // class only works relative to the app's files dir and with uploaded
            // files we have to work outside of that dir.
            $new_file = $file->move($to_dir, $with_new_name);
            //$this->storage->move($file->getFilename(), $to_my_dir.$with_new_name);

            $moved_file['filename'] = $new_file->getFilename();
            $moved_file['path'] = $new_file->getRealPath();
            $moved_file['size'] = $new_file->getSize();

            if($this->encrypt) {
               $this->encrypt_file($moved_file['path']);
               $moved_file['is_encrypted'] = true;
            }
            
            if($this->compress) {
               $this->compress_file($moved_file['path']);
               $moved_file['is_compressed'] = true;
            }

            $moved_files[] = $moved_file;
        }
        return $moved_files;
    }


    /**
     * Delete the given files.
     * 
     * @param mixed $files The files to be deleted. This can be an array of
     * path strings, an array of Archive file info records or a path string. 
     * @return array The deletes files.
     * @throws AGException
     */
    protected function delete_files($files) {
        
        // Paths are relative to the app's files dir, so we check that they
        // don't actually are absolute (in which case file operations will fail)
        // and remove the root prefix should it be present.
        $prefix = $this->get_root_dir();

        if(!is_array($files) || (is_array($files) && isset($files['path'])))
           $files = [$files];

        $dead = [];
        foreach ($files as $file) {
            $fileo = $this->get_filepath_from($file);
            $fileo = Hey::norm_path( Hey::if_starts_with($prefix, $fileo, '') );
            $path = $prefix.DIRECTORY_SEPARATOR.$fileo;
            $record = [
                'filename'      => Hey::getval($file,'filename') ?: File::basename($fileo),
                'path'          => $path,
                'size'          => Hey::getval($file,'size') ?: File::size($path),
                'mime'          => Hey::getval($file,'mime') ?: File::mimeType($path),
                'filename_orig' => Hey::getval($file,'filename_orig') ?: '',
                'filename_old'  => '',
                'path_old'      => '',
            ];

            if($this->storage->delete($fileo))
               $dead[] = $record;
        }
        return $dead;
    }


    /**
     * Delete directories from the storage.
     *
     * @param string|array $folders
     * @return array The deleted directories.
     */
    protected function delete_folders($folders) {
        
        // Paths are relative to the app's files dir, so we check that they
        // don't actually are absolute (in which case file operations will fail)
        // and remove the root prefix should it be present.
        $prefix = $this->get_root_dir();

        if(!is_array($folders)) $folders = [$folders];

        $dead = [];
        foreach ($folders as $folder) {
            // We only expect the array elements to be strings (paths).
            if(!is_string($folder))
                throw new AGException('Cannot remove folder : wrong type.');

            $folder = Hey::norm_path( Hey::if_starts_with($prefix, $folder, '') );

            if($this->storage->deleteDirectory($folder))
               $dead[] = $folder;
        }
        return $dead;
    }
    
    
    /**
     * Remove the contents of directories.
     *
     * @param string|array $folders
     * @return array The directories to be emptied.
     */
    protected function empty_folders($folders) {
        
        // Paths are relative to the app's files dir, so we check that they
        // don't actually are absolute (in which case file operations will fail)
        // and remove the root prefix should it be present.
        $prefix = $this->get_root_dir();

        if(!is_array($folders)) $folders = [$folders];

        $emptied = [];
        foreach ($folders as $folder) {
            // We only expect the array elements to be strings (paths).
            if(!is_string($folder))
                throw new AGException('Cannot remove folder : wrong type.');

            $folder = Hey::norm_path( Hey::if_starts_with($prefix, $folder, '') );

            if($this->storage->cleanDirectory($folder))
               $emptied[] = $folder;
        }
        return $emptied;
    }


    /**
     * Get all files within a directory.
     * 
     * @param string $folder The directory to get the list of files from, relative
     * to the app's files directory.
     * @param bool $recursive If set to true then all files in the folder's sub
     * directories are included.
     * @return array A list of Archive file info records for the files in the
     * specified directory.
     */
    protected function list_folder($folder, $recursive) {
        
        $prefix = $this->get_root_dir();

        $files = $recursive ? $this->storage->allFiles($folder) :
                              $this->storage->files($folder);
        $records = [];

        foreach ($files as $file) {
            $file = $prefix.DIRECTORY_SEPARATOR.$file;
            $records[] = [
                'filename'      => File::basename($file),
                'path'          => $file,
                'size'          => File::size($file),
                'mime'          => File::mimeType($file),
                'filename_orig' => '',
                'filename_old'  => '',
                'path_old'      => '',
            ];
        }
        return $records;
    }


    /**
     * Get a file info.
     *
     * @param string $file The file's path, relative to the app's files directory.
     * @return array An Archive file info record for the specified file.
     */
    protected function file_info($file) {
        
        $prefix = $this->get_root_dir();

        $file = $prefix.DIRECTORY_SEPARATOR.$file;
        return [
            'filename'      => File::basename($file),
            'path'          => $file,
            'size'          => File::size($file),
            'mime'          => File::mimeType($file),
            'filename_orig' => '',
            'filename_old'  => '',
            'path_old'      => '',
        ];
    }


    /**
     * Copy a folder to a new destination.
     * 
     * @param unknown $folder The original folder's path, relative to the app's
     * files directory.
     * @param unknown $folder The destination folder's path, relative to the app's
     * files directory.
     * @return array The copied files.
     */
    protected function copy_folder($folder, $to) {
        
        $prefix = $this->get_root_dir();

        $folder = $prefix.DIRECTORY_SEPARATOR.$folder;
        $to = $prefix.DIRECTORY_SEPARATOR.$to;

        if(!File::exists($folder)) return [];

        //return $this->storage->copyDirectory($folder, $to);
        File::copyDirectory($folder, $to);

        $records = [];

        foreach (File::allFiles($to) as $file) {
                $file = Hey::norm_path( Hey::if_starts_with($prefix, $file, '') );
                $records[] = $this->file_info($file);
        }
        return $records;
    }


    /**
     * Copy the given files to a new location.
     * 
     * @param mixed $files The files to be copied. This can be an array of
     * path strings, an array of Archive file info records or a path string. 
     * @param string $to The destination folder into which to copy the files.
     * The new files will have the same names as the old ones.
     * @return array The copied files.
     * @throws AGException
     */
    protected function copy_files($files, $to) {
        
        $prefix = $this->get_root_dir();

        if(!is_array($files) || (is_array($files) && isset($files['path'])))
           $files = [$files];

        $copied = [];
        foreach ($files as $file) {
            $fileo = $this->get_filepath_from($file);
            $fileo = Hey::norm_path( Hey::if_starts_with($prefix, $fileo, '') );
            $newfile = $to.DIRECTORY_SEPARATOR.File::basename($fileo);
            $path = $prefix.DIRECTORY_SEPARATOR.$fileo;
            $newpath = $prefix.DIRECTORY_SEPARATOR.$newfile;
            $record = [
                'filename'      => Hey::getval($file,'filename') ?: File::basename($fileo),
                'path'          => $newpath,
                'size'          => Hey::getval($file,'size') ?: File::size($path),
                'mime'          => Hey::getval($file,'mime') ?: File::mimeType($path),
                'filename_orig' => Hey::getval($file,'filename_orig') ?: '',
                'filename_old'  => '',
                'path_old'      => '',
            ];
            if($this->storage->copy($fileo, $newfile))
                    $copied[] = $record;
        }
        return $copied;
    }


    /**
     * Read the contents of a file.
     * 
     * TODO SEE encrypt_file()
     * 
     * @param string $file The file path, relative to the app's files directory
     * @param bool $decrypt Specifies whether the contents should be decrypted.
     * @param bool $decompress Specifies whether the contents should be decompressed.
     * @return string The file contents.
     */
    protected function read($file, $decrypt, $decompress) {

        $fileo = $this->get_filepath_from($file);
        assert($this->storage->exists($fileo));
        
        if(!$decrypt && !$decompress) {
            return $this->storage->get($fileo);
        }
        else if($decrypt && $decompress) {
            return decrypt( gzinflate( $this->storage->get($fileo) ) );
        }
        else if($decrypt && !$decompress) {
            return decrypt( $this->storage->get($fileo) );
        }
        else if(!$decrypt && $decompress) {
            return gzinflate( $this->storage->get($fileo) );
        }
        else {
            return decrypt($this->storage->get($fileo));
        }
    }
    
    
    /**
     * Encrypt a file.
     * 
     * TODO THIS METHOD IS NOT SUITABLE FOR LARGE FILES AS THE CONTENTS TO BE
     *      ENCRYPTED ARE FULLY LOADED IN MEMORY. A STREAMED APPROACH SHOULD
     *      BE USED. OK FOR FILE SIZES OF A FEW MB.
     * 
     * @param string $file The path to the file to be encrypted
     * @param type $to_file (Optional) If specified, the encrypted contents are
     * written to the given file, otherwise the original file is overwritten.
     */
    protected function encrypt_file($file, $to_file = null) {
        
        $prefix = $this->get_root_dir();
        
        $fileo = Hey::norm_path( Hey::if_starts_with($prefix, $file, '') );
        
        assert($this->storage->exists($fileo));
        
        $content = $this->storage->get($fileo);
        $econtent = encrypt($content);
        
        $to_file = $to_file ?: $fileo;
        $this->storage->put($to_file, $econtent);
    }
    
    
    /**
     * Compress a file.
     * 
     * TODO SEE encrypt_file()
     * 
     * @param string $file The path to the file to be compressed.
     * @param type $to_file (Optional) If specified, the compressed contents are
     * written to the given file, otherwise the original file is overwritten.
     */
    protected function compress_file($file, $to_file = null) {
        
        $prefix = $this->get_root_dir();
        
        $fileo = Hey::norm_path( Hey::if_starts_with($prefix, $file, '') );
        
        assert($this->storage->exists($fileo));
        
        $content = $this->storage->get($fileo);
        $ccontent = gzdeflate($content, 9);
        
        if($ccontent === false) throw AGException;
        
        $to_file = $to_file ?: $fileo;
        $this->storage->put($to_file, $ccontent);
    }

    
    // PRIVATE HELPERS
    
    
    /**
     * This method extracts the file path from the given parameter. Many methods
     * in the Archive accept a 'file' parameter to refer to an actual file in the
     * storage and which may be a path string or an Archive file info record.
     * 
     * @param type $param The parameter/variable to check for the file path.
     * @return string The file path.
     */
    private function get_filepath_from($param) {
        // For Archive file info records, use the path field.
        if(is_array($param) && isset($param['path'])) {
           return $param['path'];
        // For single strings, it should be the file's path.
        } else if(is_string($param)) {
           return $param;
        // Anything else is unexpected.
        } else {
           assert(false);
        }
    }

    /**
     * This method gets the root directory of the file storage used by this
     * archive.
     * 
     * @param boolean $normalized
     * @return string This archive root directory.
     */
    private function get_root_dir($normalized = true) {
        return $this->is_local ?
               Archive::get_local_files_dir($normalized, $this->is_public) :
               Archive::get_cloud_files_dir($normalized, $this->is_public);
    }
    
}


/////////////////////////////////////////////////////////////////////////////////////////


/**
 *  This class implements an archive to store files related to an entity.
 */
class EntityArchive extends BaseArchive {

	
    public function __construct(AGEntity $entity) {
        parent::__construct();
        $this->entity = $entity;
        $this->basedir = 'files/';
        $this->dirprefix = (new \ReflectionClass($this->entity))->getShortName();
    }


    /**
     * Stores an array of files in this entity's archive.
     * 
     * @param array $files The file(s) to be stored. These must be instances of
     * Symfony's File class.
     * @param string $context The context in which to store the files. A context is
     * generally a directive indicating how to store the files, the simplest action
     * being storing all the files in a subdirectory represented by the context name,
     * but it may be something more complex than a simple directory, such as a group
     * of directories or other resources.
     * @return array An array with the stored files.
     */
    public function store($files, $context = '') {

        // Nothing to store.
        if(empty($files)) return [];

        if(!is_array($files)) $files = [$files];

        // The default method moves the files to a subdirectory of the
        // entity's base files directory (specified by the 'context'
        // parameter). For more specific storage methods catch the relevant
        // cases in the switch statement and route to the appropriate helper.
        switch ($context) {
            // Specific storage cases here ...
            default:
               return $this->move_all($files, $this->get_context($context));
        }
    }


    /**
     * Removes the specified files from this entity's archive or the whole
     * archive altogether.
     *
     * @param string|array $files The file(s) to be removed. This may be an array
     * of strings (paths) or of Archive file info records, or a string path. If it
     * is null, the whole archive is removed.
     */
    public function remove($files = null) {

        if(!is_null($files)) {
           return $this->delete_files($files);
        } else {
           return $this->delete_folders($this->get_context());
        }
    }


    /**
     * Removes contexts from this entity's archive.
     *
     * @param string|array $contexts The context(s) names to be removed.
     * @return array The removed context(s).
     */
    public function remove_contexts($contexts) {

        if(is_null($contexts))
           throw new AGException('Invalid parameter $contexts : null.');

        if(!is_array($contexts)) $contexts = [$contexts];

        // The default method treats contexts as simple directories to be removed.
        // If an entity context models something more elaborate then take spcific 
        // actions based on the type of context.
        foreach($contexts as &$context) {
            $context = $this->get_context().DIRECTORY_SEPARATOR.$context;
        }
        return $this->delete_folders($contexts);
    }
    
    
    /**
     * Empty contexts of this entity's archive.
     *
     * @param string|array $contexts The context(s) names to be cleared.
     * @return array The removed context(s).
     */
    public function clear_contexts($contexts) {

        if(is_null($contexts))
           throw new AGException('Invalid parameter $contexts : null.');

        if(!is_array($contexts)) $contexts = [$contexts];

        // The default method treats contexts as simple directories to be emptied.
        // If an entity context models something more elaborate then take spcific 
        // actions based on the type of context.
        foreach($contexts as &$context) {
            $context = $this->get_context().DIRECTORY_SEPARATOR.$context;
        }
        return $this->empty_folders($contexts);
    }


    /**
     * Returns the path to the specified context.
     *
     * @param string $ctx The context name for which to return the path. If none is
     * specified, then by default the archive's base context is returned.
     * @return string The path to the context, relative to the app's files directory.
     */
    public function get_context($ctx = '') {

        switch ($ctx) {
           // Specific cases here ...
           default:
              return $this->get_files_dir($ctx);
        }
    }


    /**
     * Get the contents of a file.
     *
     * @param string $file The file name for which to get the contents.
     * @param string $context (Optional) The context the file is in.
     * @param bool $decrypt Specifies whether the contents should be decrypted.
     * @param bool $decompress Specifies whether the contents should be decompressed.
     * @return string The file contents.
     */
    public function get($file, $context = '', $decrypt = false, $decompress = false) {

        if(is_null($file))
           throw new AGException('Cannot read file : null.');

        switch ($context) {
           // Specific cases here ...
           default:
              return $this->read($this->get_context($context).DIRECTORY_SEPARATOR.$file, $decrypt, $decompress);
        }
    }


    /**
     * Get info about a file.
     *
     * @param string $file The file name for which to get the info.
     * @param string $context (Optional) The context the file is in.
     * @return array An Archive file info record.
     */
    public function get_file_info($file, $context = '') {

        if(is_null($file))
           throw new AGException('Cannot read file : null');

        switch ($context) {
           // Specific cases here ...
           default:
              return $this->file_info($this->get_context($context).DIRECTORY_SEPARATOR.$file);
        }
    }


    /**
     * Returns a list of all files in a context.
     *
     * @param string $ctx The context name for which to return the list. If none is
     * specified, then by default a list of files in the entity archive is returned.
     * @param bool $with_sub Flag indicating whether to include files from all sub
     * context of the specified context.
     * @return array A list of Archive file info records for all files in the specified
     * context.
     */
    public function lista($ctx = '', $with_sub = true) {

        switch ($ctx) {
           // Specific cases here ...
           default:
              return $this->list_folder($this->get_context($ctx), $with_sub);
        }
    }


    /**
     * Clone this entity archive into a new archive.
     *
     * @param string $new Name of the new archive.
     * @return bool
     */
    public function clone_to($new, $files = null) {

        if(!$files) {
           return $this->copy_folder($this->get_context(),
                                     $this->basedir.$this->make_archive_dir($new));
        } else {
           return $this->copy_files($files, $this->basedir.$this->make_archive_dir($new));
        }
    }

    // Private helpers

    /**
     * Build the name of the folder storing this entity archive.
     *
     * @param string $from (optional) The base string from which to build the name.
     * If not given, this entity's id will be used.
     */
    private function make_archive_dir($from = null) {
        if(!$this->is_public) {
            assert(!empty($this->entity->id));
            return $this->dirprefix.($from ?: $this->entity->id);
        } else {
            assert(!empty($this->entity->ref));
            return $from ?: $this->entity->ref;
        }
    }

    /**
     * This method creates the path for this archive's directory.
     *
     * @param string $ctx (optional) A context name that may be passed when creating
     * the directory. This string will be appended to the directory path effectively
     * creating sub-directories within the provider's base dir.
     * @return string The entity files directory relative to the base directory.
     */
    private function get_files_dir($ctx = '') {
        $entitydir = $this->make_archive_dir();
        $ctx = $ctx ? DIRECTORY_SEPARATOR.$ctx : '';
        return $this->basedir.$entitydir.$ctx;
    }

}


/**
 *  Provider Archive implementation. Archives provider-related files.
 */
class ProviderArchive extends EntityArchive {
	
    public function __construct(Provider $provider) {
        parent::__construct($provider);
        $this->basedir = 'users/';
        $this->dirprefix = 'PID';
    }
}


/**
 *  Customer Archive implementation. Archives customer-related files.
 */
class CustomerArchive extends EntityArchive {
	
    public function __construct(Customer $customer) {
        parent::__construct($customer);
        $this->basedir = 'users/';
        $this->dirprefix = 'CID';
    }
}


/**
 *  Task Archive implementation. Archives task-related files.
 */
class TaskArchive extends EntityArchive {

    public function __construct(Task $task) {
        parent::__construct($task);
        $this->basedir = 'tasks/';
        $this->dirprefix = 'TID';
    }
}

