<?php

namespace App\Console\Commands;

use App\Console\Commands\ScheduledCommand;
use App\Models\Provider;
use App\Models\Transaction;
use App\Exceptions\AGException;
use App\Hey;
use App\K;
use App, DB, Log;

/**
 * This command clears all pending payment transactions created for tasks
 * making the associated funds available for withdrawal.
 * After a payment is made in the context of a task, a wait/security period
 * applies, after which the associated funds are available for the recipient
 * to use. This command is scheduled and executed as a task periodically.
 *
 * @throws AGException
 */

class ClearTransactions extends ScheduledCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cleartxns';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears all pending payment transactions.';

    /**
     * Execute the job.
     *
     * @return mixed
     */
    public function do_job()
    {
    }
	
}
