<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App;

/**
 *  Base abstract class for all scheduled jobs. All commands/jobs
 *  that are meant to be run by the scheduler should derive it.
 */

abstract class ScheduledCommand extends Command
{
    private $signal_start;
    private $signal_end;
    
    public function __construct() {
        parent::__construct();
        
        $signals_dir = App::basePath().'/../signals/';
        $this->signal_start = $signals_dir.$this->signature.'.job';
        $this->signal_end = null;
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->signal_start_job();
        $this->do_job();
        $this->signal_end_job();
    }

    abstract public function do_job();

    protected function signal_start_job() {
        touch($this->signal_start);
    }
    
    protected function signal_end_job() {
        @unlink($this->signal_start);
    }

}
