<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Events\Dispatcher;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
    	Commands\ClearTransactions::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->command('cleartxns')->everyMinute();
    }
    
    
    /**
     * Create a new console kernel instance.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function __construct(Application $app, Dispatcher $events)
    {
        parent::__construct($app, $events);
        
        // Override the framework's logging config and install a custome one.
        array_walk($this->bootstrappers, function(&$bootstrapper) {
            if($bootstrapper === 'Illuminate\Foundation\Bootstrap\ConfigureLogging') {
               $bootstrapper = 'App\Bootstrap\ConfigureLogging';
            }
        });
    }

}
