<?php

namespace App\Events;

abstract class Event
{

    /**
     * This method implements necessary event-specific processing
     * that should be done after it has been successfully handled
     * by the queue worker.
     */
    abstract public function on_after_event();
}
