<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;


class EventMarketplace extends Event
{
    use SerializesModels;

    /**
     * The id of the event as sent by the marketplace provider.
     * 
     * @var string $event_id
     */
    public $event_id;
    
    /**
     * The provider's account id the event refers to.
     * 
     * @var string $account_id
     */
    public $account_id;
    
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($event_id, $account_id) {
        $this->event_id = $event_id;
        $this->account_id = $account_id;
    }
    
    
    /**
     */
    public function on_after_event(){
    }

}
