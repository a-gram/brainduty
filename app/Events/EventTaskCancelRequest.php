<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class EventTaskCancelRequest extends Event
{
    use SerializesModels;

    /**
     * The id of the task to be cancelled.
     *  
     * @var unknown
     */
    public $task_id;
    
    /**
     * The id of the transactional message for the request.
     * 
     * @var unknown
     */
    public $msg_id;
    
    /**
     * The user who requested the cancellation.
     *
     * @var unknown
     */
    public $requestor_id;
    
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($task_id, $msg_id, $requestor_id) {
        $this->task_id = $task_id;
        $this->msg_id = $msg_id;
        $this->requestor_id = $requestor_id;
    }
    
    
    /**
     * Perform any required processing needed after the event
     * has been successfully handled by the queue worker.
     */
    public function on_after_event(){
    }
}
