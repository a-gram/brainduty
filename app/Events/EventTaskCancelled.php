<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class EventTaskCancelled extends Event
{
    use SerializesModels;

    /**
     * The id of the cancelled task.
     *  
     * @var unknown
     */
    public $task_id;
    
    /**
     * The status of the task at the time of cancellation.
     * 
     * @var unknown
     */
    public $task_status;
    
    /**
     * The user who requested the cancellation.
     *
     * @var unknown
     */
    public $requestor_id;
    
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($task_id, $task_status, $requestor_id) {
        $this->task_id = $task_id;
        $this->task_status = $task_status;
        $this->requestor_id = $requestor_id;
    }
    
    
    /**
     * Perform any required processing needed after the event
     * has been successfully handled by the queue worker.
     */
    public function on_after_event(){
    }
}
