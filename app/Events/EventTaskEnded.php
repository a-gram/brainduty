<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;


class EventTaskEnded extends Event
{
    use SerializesModels;

    public $task_id;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($task_id) {
        $this->task_id = $task_id;
    }
    
    
    /**
     * Perform any required processing needed after the event
     * has been successfully handled by the queue worker.
     */
    public function on_after_event(){
    }

}
