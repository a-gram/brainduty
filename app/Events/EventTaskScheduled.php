<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;


class EventTaskScheduled extends Event
{
    use SerializesModels;

    /**
     * The id of the scheduled task.
     * 
     * @var type $task_id
     */
    public $task_id;
        
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($task_id) {
        $this->task_id = $task_id;
    }
    
    
    /**
     */
    public function on_after_event(){
    }

}
