<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;


class EventUserIdentityVerification extends Event
{
    use SerializesModels;

    /**
     * The id of the user being verified.
     * 
     * @var type $user_id
     */
    public $user_id;
    
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user_id) {
        $this->user_id = $user_id;
    }
    
    
    /**
     */
    public function on_after_event(){
    }

}
