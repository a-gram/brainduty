<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;


class EventUserRating extends Event
{
    use SerializesModels;

    /**
     * The id of the given rating.
     * 
     * @var type $rating_id
     */
    public $rating_id;
    
    /**
     * The id of the user receiving the rating.
     * 
     * @var type $recipient_id
     */
    public $recipient_id;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($rating_id, $recipient_id) {
        $this->rating_id = $rating_id;
        $this->recipient_id = $recipient_id;
    }
    
    
    /**
     */
    public function on_after_event(){
    }

}
