<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;


class EventUserSignup extends Event
{
    use SerializesModels;

    /**
     * The id of the user who signed up.
     * 
     * @var type $user_id
     */
    public $user_id;
    
    /**
     * The user email verification token.
     * 
     * @var string 
     */
    public $user_ev_token;
    
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user_id, $user_ev_token) {
        $this->user_id = $user_id;
        $this->user_ev_token = $user_ev_token;
    }
    
    
    /**
     */
    public function on_after_event(){
    }

}
