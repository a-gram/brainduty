<?php

namespace App\Exceptions;

use App\Exceptions\AGException;
use App\K;

/**
 *  Account Exception Class.
 *  Throw this exception whenever there are issues with a user account.
 */
class AGAccountException extends AGException {
    public function __construct($message = 'Account issues.', $ctx = null, $previous = null) {
        parent::__construct($message, $ctx, $previous, K::ERROR_CONFLICTS);
    }
}
