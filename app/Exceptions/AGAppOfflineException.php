<?php

namespace App\Exceptions;

use App\Exceptions\AGException;
use App\K;

/**
 *  Payment Exception Class
 */
class AGAppOfflineException extends AGException {
    public function __construct($message = 'Service unavailable.', $ctx = null, $previous = null) {
        parent::__construct($message, $ctx, $previous, K::ERROR_SERVICE_UNAVAILABLE);
    }
}

