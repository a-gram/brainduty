<?php

namespace App\Exceptions;

use App\Exceptions\AGException;
use App\K;

/**
 *  Unauthenticaed Exception Class
 *  Throw this exception whenever the entity trying to access a resource
 *  has not been authenticated.
 */
class AGAuthenticationException extends AGException {
	public function __construct($message = 'Access denied.', $ctx = null, $previous = null) {
		parent::__construct($message, $ctx, $previous, K::ERROR_UNAUTHENTICATED);
	}
}
