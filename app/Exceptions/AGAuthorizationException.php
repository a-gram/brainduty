<?php

namespace App\Exceptions;

use App\Exceptions\AGException;
use App\K;

/**
 *  Unauthenticaed Exception Class
 *  Throw this exception whenever an entity is trying to access a resource
 *  without authorization or performing some illegal action.
 */
class AGAuthorizationException extends AGException {
	public function __construct($message = 'The action is not allowed.', $ctx = null, $previous = null) {
		parent::__construct($message, $ctx, $previous, K::ERROR_UNAUTHORIZED);
	}
}
