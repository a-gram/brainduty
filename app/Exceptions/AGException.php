<?php

namespace App\Exceptions;

use Exception;
use App\K;
use Log;

/**
 *  Base exception class for all of the app's exceptions.
 *  Throw this exception for anything that is not covered by derived
 *  classes, usually pertaining to unexpected errors (500 et al.).
 */
class AGException extends Exception {
	
	/**
	 *  Contains exception-specific data. This is any object that
	 *  can be serialized in JSON format.
	 */
	protected $ctx = null;
	
        /**
         * 
         * @param string $message
         * @param int $code
         * @param mixed $ctx
         * @param Throwable $previous
         */
	public function __construct($message = 'We have got issues',
                                    $ctx = null,
                                    $previous = null,
                                    $code = K::ERROR_INTERNAL) 
        {
		 parent::__construct($message, $code, $previous);
		 $this->ctx = $ctx;
	}
	
        /**
         * 
         * @return mixed
         */
	public function get_context() {
		return $this->ctx;
	}
	
        /**
         * 
         * @param mixed $data
         * @return $this
         */
	public function set_context($data) {
		$this->ctx = $data;
		return $this;
	}
	
        /**
         * 
         * @return boolean
         */
	public function has_context() {
		return $this->ctx !== null;
	}
        
        /**
         * 
         * @return string
         */
        public function context_to_string() {
            $ctx = is_array($this->ctx) ? $this->ctx : 
                   [ 'context' =>  print_r($this->ctx, true)];
            return json_encode($ctx);
        }
        
        /**
         * 
         * @return string A Json string with the following format:
         * {
         *    "exception" : <the caught excpetion>,
         *    "context"   : <context of the caught exception>,
         *    "chain"     : [ <array of previously thrown excpetions> ]
         * }
         */
        public function to_string() {
            $exc = [
                'exception' => $this->getCode().' '.
                               $this->getMessage().'\n'.
                               $this->getFile().':'.$this->getLine().'\n\n'.
                               $this->getTraceAsString(),
                'context'   => $this->context_to_string(),
                'chain'     => []
            ];
            $curr = $this;
            while(($prev = $curr->getPrevious()) !== null) {
                  $exc['chain'][] = $prev->getCode().' '.
                                    $prev->getMessage().'\n'.
                                    $prev->getFile().':'.$prev->getLine().'\n\n'.
                                    $prev->getTraceAsString();
                  $curr = $prev;
            }
            return json_encode($exc);
        }
        
        /**
         * 
         * @return $this
         */
        public function log() {
            Log::error($this->to_string());
            return $this;
        }
        
}

