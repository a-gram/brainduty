<?php

namespace App\Exceptions;

use App\Exceptions\AGException;


/**
 *  Just an alias for the generic 500-ish AGException with a more classy name.
 */
class AGInternalException extends AGException {
	public function __construct($message = 'Internal issues.', $ctx = null, $previous = null) {
		parent::__construct($message, $ctx, $previous, K::ERROR_INTERNAL);
	}
}
