<?php

namespace App\Exceptions;

use App\Exceptions\AGException;
use App\K;

/**
 *  Invalid Session Exception Class.
 *  Throw this exception whenever the client session is no longer valid (expired,
 *  invalidated, etc.).
 */
class AGInvalidSessionException extends AGException {
	public function __construct($message = 'Invalid session.', $ctx = null, $previous = null) {
		parent::__construct($message, $ctx, $previous, K::ERROR_CONFLICTS);
	}
}
