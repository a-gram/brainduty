<?php

namespace App\Exceptions;

use App\Exceptions\AGException;
use App\K;

/**
 *  Invalid State Exception Class.
 *  Throw this exception whenever the input from the user is valid but
 *  cannot be processed because it conflicts with the state of some
 *  resources, with enforced policies, with business rules, etc.
 */
class AGInvalidStateException extends AGException {
	public function __construct($message = 'Invalid action.', $ctx = null, $previous = null) {
		parent::__construct($message, $ctx, $previous, K::ERROR_CONFLICTS);
	}
}
