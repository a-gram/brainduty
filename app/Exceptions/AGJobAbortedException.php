<?php

namespace App\Exceptions;

use App\Exceptions\AGException;


/**
 *  Throw this exception to signal that a queued job has aborted and must not
 *  be tried again.
 */
class AGJobAbortedException extends AGException {
	public function __construct($message = 'Job aborted.', $ctx = null, $previous = null) {
		parent::__construct($message, $ctx, $previous, K::ERROR_INTERNAL);
	}
}
