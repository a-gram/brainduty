<?php

namespace App\Exceptions;

use App\Exceptions\AGException;
use App\K;

/**
 *  Marketplace Exception Class.
 *  Throw this exception for any generic issue in marketplace operations.
 */
class AGMarketplaceException extends AGException {
	public function __construct($message = 'Marketplace operation issues.', $ctx = null, $previous = null) {
		parent::__construct($message, $ctx, $previous, K::ERROR_PAYMENT);
	}
}

