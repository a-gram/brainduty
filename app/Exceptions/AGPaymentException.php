<?php

namespace App\Exceptions;

use App\Exceptions\AGException;
use App\K;

/**
 *  Payment Exception Class.
 *  Throw this exception whenever a payment issue occurs. For example
 *  a credit card charge fails, or some other payment source cannot be
 *  charged for some reason.
 */
class AGPaymentException extends AGException {
	public function __construct($message = 'Payment issues.', $ctx = null, $previous = null) {
		parent::__construct($message, $ctx, $previous, K::ERROR_PAYMENT);
	}
}

