<?php

namespace App\Exceptions;

use App\Exceptions\AGException;
use App\K;

/**
 *  Payment Exception Class
 */
class AGResourceNotFoundException extends AGException {
	public function __construct($message = 'Resource not found.', $ctx = null, $previous = null) {
		parent::__construct($message, $ctx, $previous, K::ERROR_NOT_FOUND);
	}
}

