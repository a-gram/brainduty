<?php

namespace App\Exceptions;

use App\Exceptions\AGException;
use App\K;

/**
 *  Transfer Exception Class.
 *  Throw this exception whenever a funds transfer issue occurs. For example
 *  a withdrawal fails, or some payout account cannot be credited for some reason.
 */
class AGTransferException extends AGException {
	public function __construct($message = 'Transfer issues.', $ctx = null, $previous = null) {
		parent::__construct($message, $ctx, $previous, K::ERROR_PAYMENT);
	}
}

