<?php

namespace App\Exceptions;

use App\Exceptions\AGException;
use App\K;

/**
 *  Validation Exception Class.
 *  Throw this exception whenever the input from the user does not
 *  conform to predefined rules.
 */
class AGValidationException extends AGException {
	public function __construct($message = 'Invalid request.', $ctx = null, $previous = null) {
		parent::__construct($message, $ctx, $previous, K::ERROR_BAD_REQUEST);
	}
}
