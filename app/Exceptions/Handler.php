<?php

namespace App\Exceptions;

use Exception, Log, View, Redirect, URL;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Session\TokenMismatchException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use App\APIResponse;
use App\Hey;
use App\K;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    	
        //AGException::class,
        //AGInternalException::class,
    	AGAuthenticationException::class,
    	AGAuthorizationException::class,
    	AGValidationException::class,
        AGAccountException::class,
        AGInvalidSessionException::class,
        AGInvalidStateException::class,
        AGMarketplaceException::class,
        AGPaymentException::class,
        AGResourceNotFoundException::class,
        AGTransferException::class,
        
    ];

    
    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
    	// Log context for internal AGExceptions
    	if($e instanceof AGException &&
           $e->getCode() === K::ERROR_INTERNAL &&
           $e->get_context()) {
           Log::error($e->context_to_string());
    	}
    	
        parent::report($e);
    }
    
    
    /**
     * Render an exception into an HTTP response according to the app format.
     * Depending on the request type that generated the exception, the response
     * is prepared accordingly. If the request was for a 'page' (html or anything
     * visual) then the response will be set with the appropriate view (generally
     * an error page). If the request was for an API call (REST, RPC, or anything
     * returning structured data) then the response will be set with the error
     * information using the appropriate format (JSON or XML).
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
    	$exc = $e;
    	
    	// Process non-app exceptions to map and/or change the information exposed
        // to the clients.
    	if(!($e instanceof AGException)) {
           // Token mismatches are a sign of invalid (usually expired) sessions.
           if($e instanceof TokenMismatchException) {
              $exc = new AGInvalidSessionException(trans('exceptions.session-invalid'));
           }
           // Other mapped exceptions here
           // ...
           else {
              // When in production/live mode, suppress the exception message returned
              // to clients, which may expose too much details and give a generic one.
              $msg = Hey::is_app_live() ? K::MSG_ERROR_GENERIC : $e->getMessage();
              $exc = new AGException ($msg);
           }
    	}
    	
    	$ctx = $exc->get_context();
    	
    	// NOTE: to verify whether the request is for an API call the Request::ajax()
    	//       method is used, but not all API calls are necessarily made via AJAX...
    	
    	// The request is for a web page. Return a view or redirect to another page.
        // Here we handle specific error cases or return the catch-all error page.
    	if ($request->acceptsHtml() && !$request->ajax())
    	{
            $page = $to_page = null;
            
            if($exc instanceof AGAuthenticationException) {
                $to_page = $ctx && $ctx['is_admin_login'] ?
                           URL::to(Hey::get_app('admin_root').'/login') :
                           URL::to('user/login');
            }
            else if($exc instanceof AGInvalidSessionException) {
                $to_page = URL::to('/');
            }
            else if($exc instanceof AGAppOfflineException) {
                $page = 'errors.maintenance';
            }
            else {
                $page = 'errors.all';
            }
            
            if($page) {
               $view = View::make($page, [
                    'error_code'    => $exc->getCode(),
                    'error_message' => $exc->getMessage()
               ]);
               return new Response($view, $exc->getCode());
            } 
            else {
               return new RedirectResponse($to_page);
            }
    	}
    	else
        // The request is for JSON data (API call). Return an API error response.
    	if ($request->acceptsJson())
    	{
            try{ // The following throws, so catch bugs here before messing up ...
                return APIResponse::error()->with($exc)->go();
            } 
            catch (Exception $e) {
                $this->report($e);
                return new Response('Unexpected error.', K::ERROR_INTERNAL);
            }
    	}
    	// We have no idea of what they accept. Just send our best wishes.
    	else {
            return new Response('Best wishes.', K::ERROR_INTERNAL);
    	}
    	 
    }
    
}
