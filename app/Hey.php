<?php

namespace App;

use App\Exceptions\AGException;
use App\Models\User;
use App\K;
use Config;

/**
 *  This class is the handyman of the application. Just say Hey:: and it
 *  will do many useful jobs for you.
 *
 */
class Hey {	
	
    /**
     * Convert a date string from DD/MM/YYYY to database format.
     *
     * @param string A date string in the format DD/MM/YYYY.
     * @return string The date string in database format.
     */
    public static function dmy_to_db_date($date){
            if(!isset($date)) return;
            $d = \DateTime::createFromFormat('d/m/Y', $date);
            return $d->format(k::DB_DATE_FORMAT);
    }

    /**
     * Convert a date string from database format to DD/MM/YYYY.
     *
     * @param string A date string in database format.
     * @return string The date string in DD/MM/YYYY format.
     */
    public static function db_date_to_dmy($date){
            if(!isset($date)) return;
            $d = \DateTime::createFromFormat(k::DB_DATE_FORMAT, $date);
            return $d->format('d/m/Y');
    }

    /**
     * Creates a datetime string from current date/time into the database
     * specific format.
     *
     * @return string The current date and time in database format.
     */
    public static function now_to_db_datetime(){
            return \date(k::DB_DATETIME_FORMAT);
    }

    /**
     * Get the current time in UNIX timestamp format.
     *
     * @param boolean $localized Whether to get the local or UTC timestamp.
     * @return int The current timestamp.
     */
    public static function now_to_timestamp($localized = false){
            $now = (new \DateTime);
            return $localized ? $now->getTimestamp() + $now->getOffset() :
                                $now->getTimestamp();
    }
    
    /**
     * Convert a timestamp into the db format.
     * 
     * @param int $timestamp
     * @return string
     */
    public static function timestamp_to_db_datetime($timestamp) {
        return self::to_db_datetime( new \DateTime('@'.$timestamp) );
    }

    /**
     * Convert a date-time object into the db format.
     * 
     * @param DateTime $datetime The date-time to be converted.
     * @return string The given date-time in db format.
     */
    public static function to_db_datetime($datetime) {
            return $datetime->format(k::DB_DATETIME_FORMAT);
    }

    /**
     * Convert a db date-time string into a more human-friendly date (e.g. September 6, 2016).
     * 
     * @param string $datetime The db date-time string to be converted.
     * @return string The given date-time into human-friendly date format.
     */
    public static function date_to_human($datetime) {
            if(!isset($datetime)) return;
            return \DateTime::createFromFormat(k::DB_DATETIME_FORMAT, $datetime)
                            ->format('F j, Y');
    }
    
    /**
     * Convert a time interval in ISO 8601 format to a human readable thing.
     * 
     * @param string $dt The time interval in ISO 8601 format.
     * @return string The time interval in a human readable string.
     * @throws AGException
     */
    public static function dt_to_human($dt) {
        if(!isset($dt)) return '';
        $interval = new \DateInterval($dt);
        if(!$interval) throw new AGException('ERROR', $dt);
        $human = $interval->y ? $interval->y.' years ' : '';
        $human .= $interval->m ? $interval->m.' months ' : '';
        $human .= $interval->d ? $interval->d.' days ' : '';
        $human .= $interval->h ? $interval->h.' hours ' : '';
        $human .= $interval->i ? $interval->i.' minutes ' : '';
        $human .= $interval->s ? $interval->s.' seconds ' : '';
        return $human;
    }
    
    /**
     * Convert a time interval in ISO 8601 format to seconds.
     * 
     * @param string $dt The time interval in ISO 8601 format.
     * @return int The duration of the interval in seconds.
     * @throws AGException
     */
    public static function dt_to_sec($dt) {
        if(!isset($dt)) return 0;
        $interval = new \DateInterval($dt);
        if(!$interval) throw new AGException('ERROR', $dt);
        return date_create('@0')->add($interval)->getTimestamp();
    }

    /**
     * This method generates a random string.
     *
     * @param int $length (OPTIONAL = 10) The length of the generated string.
     * @return string Returns the randomly generated string.
     * @link http://stackoverflow.com/a/4356295/1718162
     */
    public static function random_string($length = 10) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $random_string = '';
            for ($i = 0; $i < $length; $i++) {
                    $random_string .= $characters[rand(0, strlen($characters) - 1)];
            }
            return $random_string;
    }


    /**
     * This method trims the string fields of a record and its subrecords, if any.
     *
     * @param array $record A reference to the record to be trimmed.
     */
    public static function trim_record(&$record) {
            if(!is_array($record)) return;
            foreach ($record as $key => &$value) {
                    if(is_string($value)) $value = trim($value);
                    else if(is_array($value))  self::trim_record($value);
            }
    }


    /**
     *  Convenience function to check if a given field in a record is set and get its value
     *  should it be. A field is considered 'set' if it is present in the record and is set
     *  to anything that's not null or '' (empty string).
     *
     * @param array $record The record for which the existence of the field must
     * be checked.
     * @param string $path_to_field A string containing the field to be checked. The function
     * supports checking for fields at any level within the record, so this parameter
     * in general holds the 'path' to the field in 'dot notation' to check for nested fields.
     * For example to check for the existence of a field $record['tree']['fruit'] = 'apple'
     * the $field parameter would be 'tree.fruit', which would return 'apple' or null
     * if 'fruit' does not exist.
     * @param bool $throw_on_fail If set to true, an exception will be thrown if the field
     * is not set.
     * @return mixed|null Returns the field's value if it exists in the record, null otherwise.
     */
    public static function getval($record, $path_to_field, $throw_on_fail=false) {
    if (empty($record) || !isset($path_to_field) || !is_array($record))
        return null;
    $fields = explode('.', $path_to_field);
    $ret_val = $record;
    foreach ($fields as $field) {
        $ret_val = (array_key_exists($field, is_array($ret_val) ? $ret_val : []) &&
                   $ret_val[$field] !== null &&
                   $ret_val[$field] !== '') ? 
                   $ret_val[$field] : null;
        if($ret_val === null){
            if($throw_on_fail)
               throw new AGException('Field "'.$path_to_field.'" not set.');
            break;
        }
    }
    return $ret_val;
    }

    
    /**
     * Replaces the specified portion of the target string starting at the first character
     * with another string. In other words, check if the given string starts with the given
     * prefix and if so replace it with another string.
     * 
     * @param string $prefix The prefix the string should start with.
     * @param string $string The target string.
     * @param string $replace_with The replacement string for the prefix.
     * @return string A new string with the replaced prefix, if found, or the original string
     * if not found.
     */
    public static function if_starts_with($prefix, $string, $replace_with) {
    	if(is_null($prefix) || is_null($string) || is_null($replace_with))
    	   throw new AGException('Invalid parameters.');
        return strpos ($string, $prefix) === 0 ? 
               substr_replace ($string , $replace_with , 0, strlen($prefix)) :
                   $string;
    }
    
    
    /**
     * Check whether the given string starts with the given prefix using case
     * sensitive or insensitive search.
     * 
     * @param string $prefix The prefix to be found.
     * @param string $string The target string.
     * @param boolean $cs Flag indicating case-sensitiveness.
     * @return boolean
     */
    public static function starts_with($prefix, $string, $cs = true) {
    	if(is_null($prefix) || is_null($string))
    		throw new AGException('Invalid parameters.');
    	return $cs ? strpos ($string, $prefix) === 0 :
    	             stripos ($string, $prefix) === 0;
    }
    
    
    /**
     * Check whether a string contains a specified substring.
     * 
     * @param string $substring
     * @param string $string
     * @param string $cs
     * @throws AGException
     * @return boolean
     */
    public static function contains($substring, $string, $cs = true) {
    	if(is_null($substring) || is_null($string))
    		throw new AGException('Invalid parameters.');
    	return $cs ? strpos ($string, $substring) !== false :
    	             stripos ($string, $substring) !== false;
    }
    
    
    /**
     * Normalize a path (remove leading and trailing path separators).
     * 
     * @param string $path The path to be normalized.
     * @param bool $full If true, normalize both sides of the path,
     * else normalize the trailing separators only.
     * @return string
     */
    public static function norm_path($path, $full = true){
        return $full ? trim($path, " \t\n\r\0\x0B\\/") : 
                       rtrim($path, " \t\n\r\0\x0B\\/");
    }
    
    
    /**
     *  Generate Universally Unique IDentifiers.
     * 
     *  http://php.net/manual/en/function.com-create-guid.php
     * 
     *  @param int $len The number of random bytes used to generate the UUID. 
     *  @return string A string representing a UUID.
     */
    public static function generate_uuid($len = 16) {
        
        if (function_exists('openssl_random_pseudo_bytes') === false)
            throw new AGException('Cannot find OpenSSL function.');
/*
        $data = openssl_random_pseudo_bytes($len);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
        return vsprintf('%s%s%s%s%s%s%s%s', str_split(bin2hex($data), 4));
*/
        $h = '';
        $a = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $na = strlen($a);
        
        $r = openssl_random_pseudo_bytes($len);

        for($i=0; $i<$len; $i++) {
            $k = ord($r[$i]);
            do {
               $h = $a[$k % $na] . $h ;
               $k = (int) ($k / $na);
            } while ($k);
        }
        return $h;
    }
    
    
    /**
     *  Get app's settings. 
     *  App settings are defined in the files config/app-settings.php
     *  and config/app.php. If not found there, the method checks the environment.
     *  
     *  @param string $setting The name of the setting to be returned.
     *  @param mixed $default A default value to be returned if the setting is 
     *  not found.
     */
    public static function get_app($setting, $default = null) {
    	$value = Config::get('app-settings.'.$setting);
    	$value = $value ?: Config::get('app.'.$setting);
        $value = $value ?: env($setting, $default);
    	return $value;
    }
    
    
    /**
     * Check whether the given field(s) are present in the specified record,
     * optionally checking whether they also contain a valid value.
     * 
     * @param string|array $fields The field(s) whose presence must be checked.
     * @param array $record An associative array to be checked for the presence
     * of the field(s).
     * @param string $presence A string flag indicating how the presence of the
     * field(s) must be considered. If it's 'any' then the record is checked for
     * the presence of any of the given fields. If it's 'all' then the record is
     * checked for the presence of all of the given field(s).
     * @param boolean $with_value Indicates whether the present fields must also
     * contain a value (that is anything but null and the empty string ''). 
     * @return boolean True if the field(s) are present according to the specified
     * presence parameter, false otherwise.
     * NOTE: If $fields is an empty array (or string) then if $record is also empty
     *       the method returns true, otherwise it returns false.
     */
    public static function has_field($fields, array $record, $presence = 'any', $with_value=true) {
    	if($presence !== 'any' && $presence !== 'all')
           throw new AGException('Invalid paramater $presence : '.$presence);
    	if(!is_array($fields)) $fields = [$fields];
    	if(count($fields) === 0 || $fields[0] ==='')
           return count($record) === 0;
    	$i = array_intersect($fields, array_keys($record));
		$set = true;
		if($with_value) {
		   if($presence === 'any') $set = false;
           foreach($i as $field)
			 $set = $presence === 'any' ? 
			        $set |= $record[$field]!==null && $record[$field]!=='' :
			        $set &= $record[$field]!==null && $record[$field]!=='';
		}
    	return $presence === 'any' ?
		       count($i) > 0 && $set :
			   count($i) === count($fields) && $set;
    }
    
    
    /**
     * Strip off html tags from the given text.
     * 
     * @param string $text The text to be stripped of tags.
     * @param array $exclude A list of tags that should be not stripped.
     */
    public static function strip_html_tags($text, array $exclude = []) {
    	$allowed = implode($exclude);
    	return strip_tags($text, $allowed);
    }
    
    
    /**
     * Format the given numeric value into a currency string.
     * 
     * @param number $value The value to be formatted.
     * @param boolean $in_cents Whether to consider the value in cents.
     * @param string $currency The leading currency symbol.
     * @return string The currency string.
     */
    public static function to_currency($value, $in_cents = true, $currency = null) {
        $currency = $currency ?: Hey::get_app('currency');
    	return $currency.' '.number_format($in_cents ? $value/100 : $value, 2);
    }
    
    
    /**
     * Get the fingerprint of a file.
     * 
     * @param string $file Full path to the file to be fingerprinted.
     * @param bool $make_name If true, make the fingerprint string a filename.
     * @return string The file's fingerprint, optionally made into a filename
     * by appending the file extension, if any.
     */
    public static function hash_file($file, $make_name = true) {
        $fingerprint = md5_file($file);
        if(!$fingerprint)
            throw new AGException('Could not hash file '.$file);
        return $make_name ? $fingerprint.'.'.pathinfo($file, PATHINFO_EXTENSION) :
                            $fingerprint;
    }
    
    
    /**
     * Get the MAC of the given data.
     * 
     * @param string $data The message to be hashed.
     * @param string $key (optional) The secret key.
     * @return string
     */
    public static function MAC($data, $key=null) {
        return hash_hmac ( self::get_app('hash_algo'),
                           $data ,
                           $key ?: self::get_app('key') );
    }
    
    
    /**
     * Turn http URLs in a text into html links.
     * 
     * @param string $text The text to search for URLs.
     * @return string The text with linkfied URLs.
     */
    public static function linkfy($text) {
        $reg = '/(https?)\:\/\/(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])/i';
        return preg_replace($reg, '<a href="$1://$2" target="_blank">$0</a>', $text);
    }
    
    
    /**
     * Check whether the app is currently in development or production/live
     * mode.
     * 
     * @return boolean
     */
    public static function is_app_live() {
        return self::get_app('env') === 'production';
    }
    
    
    /**
     * Check that the given conditions are true and throw if any is not.
     * 
     * @param array $conditions
     * @throws Exceptions\AGValidationException
     */
    public static function check($conditions) {
        if(!is_array($conditions)) {
           $conditions = [$conditions];
        }
        foreach ($conditions as $i=>$condition) {
             if($condition !== "0" && !$condition) {
                 throw new Exceptions\AGValidationException("Condition [$i] is false.");
             }
        }
    }
    
    
    /**
     * Convert a country name to its ISO 3166 code.
     * 
     * @param string $country
     * @return string
     */
    public static function country_to_iso3166($country) {
        return self::get_app('countries')[$country];
    }
    
    
    /**
     * Convenience method to catch runtime bugs. Currently throws an exception
     * but may do something else.
     * 
     * @param mixed $ctx
     * @throws AGException
     */
    public static function bug_here($ctx = null) {
        throw new AGException('BUG!', $ctx);
    }
    
    
    /**
     * Truncate a text to the given number of characters.
     * 
     * @param string $text
     * @param int $nchar
     * @return string
     */
    public static function truncate_text($text, $nchar = 50) {
        $ttext = trim($text);
        $stext = substr($ttext, 0, $nchar);
        return $stext ? $stext . (strlen($stext) < strlen($ttext) ? '...' : '') : '';
    }
        
}

