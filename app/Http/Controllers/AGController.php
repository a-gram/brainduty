<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Exceptions\AGValidationException;
use Validator;

/**
 * The Godfather of all app's controllers from which all others descend.
 * Abstract, doesn't do anything special, aside from implementing some
 * typical functionalities shared by many controllers.
 */
abstract class AGController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    protected $request;
    
    public function __construct(Request $request) {
    	$this->request = $request;
    }
    
    /**
     * Many POST-oriented controllers require specific data in the requests' to
     * be present before any processing can proceed. This convenient method does
     * the verification job.
     * 
     * @param array $fields The request's fields that are required.
     */
    protected function is_required(array $fields) {
    	
    	$required_fields = [];
    	$error_messages = [];
    	foreach ($fields as $field=>$emsg){
    		$required_fields[$field] = 'required';
    		$error_messages[$field.'.required'] = $emsg;
    	}
    	//$this->validate($this->request, $required_fields, $error_messages);
    	
        $validator = Validator::make($this->request->all(),
        		                     $required_fields,
        		                     $error_messages);
        
        if ($validator->fails()) {
        	$errors['failed_params'] = $validator->errors();
        	throw (new AGValidationException
        		  ('There are missing parameters in the request.'))
        	      ->set_context($errors);
        }
    }
    
    protected function is_not_accepted(array $fields) {

//     	$rejected_fields = array_filter($fields, function ($field){
//     		if($this->request->has($field)){
//     			return true;
//     		}
//         }, \ARRAY_FILTER_USE_KEY);

    	$rejected_fields = [];
    	foreach ($fields as $field=>$emsg){
    		if($this->request->has($field)){
    		   $rejected_fields[$field] = $emsg;
    		}
    	}
    
    	if (count($rejected_fields)) {
    		$errors['failed_params'] = $rejected_fields;
    		throw (new AGValidationException
    				('There are unaccepted parameters in the request.'))
    				->set_context($errors);
    	}
    }
    
}
