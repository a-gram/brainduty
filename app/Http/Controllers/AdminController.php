<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\APIResponse;
use App\Models\User;
use App\Models\Provider;
use App\Models\ProviderIdentity;
use App\Models\Task;
use App\Models\TaskDispute;
use App\Models\Transaction;
use App\Models\Document;
use App\Exceptions\AGException;
use App\Exceptions\AGAuthenticationException;
use App\Exceptions\AGAuthorizationException;
use App\Exceptions\AGInvalidStateException;
use App\Exceptions\AGValidationException;
use App\Exceptions\AGResourceNotFoundException;
use App\Notifications\IdentityInquiryNotification;
use App\Notifications\IdentityUnverifiedNotification;
use App\Notifications\IdentityVerifiedNotification;
use App\Notifications\TransactionNotification;
use App\Notifications\TaskDisputeResolvedNotification;
use App\Events\EventUserIdentityVerification;
use App\Hey;
use App\Utils\Validate;
use App\K;
use App, Auth, DB, View, URL, Redirect, Event;

/**
 *  This is the admin controller for accessing the admin section of the app.
 */
class AdminController extends AGController {

    private $notifier;

    public function __construct(Request $request) {
            parent::__construct($request);
            $this->notifier = app('notifier');
    }


    /**
     *  Admin login page.
     */
    public function login_page() {

            if(Auth::check()) {
                    return Redirect::to(URL::to(Hey::get_app('admin_root').'/home'));
            }

            $data = [
                'vw_company_name' => Hey::get_app('company_name'),
                'vw_admin_root' => Hey::get_app('admin_root'),
            ];

            return View::make('admin.login', $data);
    }


    /**
     *  The admin home page.
     */
    public function home_page() {

            $this->authorize('admin', Auth::user());

            $view_data = [
                    'vw_company_name' => Hey::get_app('company_name'),
                'vw_admin_root' => Hey::get_app('admin_root'),
            ];

            return View::make('admin.home', $view_data);
    }


    /**
     *  The admin provider onboarding page.
     */
    public function provider_onboarding_page() {

        $this->authorize('admin', Auth::user());

        $archive = app('archive');

        // Get all providers who need to be verified.
        $identities = ProviderIdentity::with('provider')->
                      where('status', K::PROVIDER_IDENTITY_VERIFYING)->
                      where(function($q){
                          $q->whereNull('int_verified')
                            ->orWhereNull('ext_verified')
                            ->orWhere('int_verified',0)
                            ->orWhere('ext_verified',0);
                      })->
                      get();

        $providers = [];

        foreach($identities as $identity) {
                $provider = $identity->provider->toArray();
                $provider['id'] = $identity->provider->id;
                $provider['identity'] = $identity->attributesToArray();
                $provider['identity']['status'] = $identity->status_to_string();
                $providers[] = $provider;
        }

        $view_data = [
            'vw_company_name' => Hey::get_app('company_name'),
            'vw_admin_root' => Hey::get_app('admin_root'),
            'vw_providers'    => $providers,
        ];

        return View::make('admin.provider-onboarding', $view_data);
    }
    
    
    /**
     *  Task disputes page
     */
    public function disputes_page() {
        
        $this->authorize('admin', Auth::user());
        
    	$disputes = TaskDispute::with('task')->
                                 where('status', '!=', K::TASK_DISPUTE_RESOLVED)->
                                 orderBy('created_at','desc')->
                                 get();
        
        $view_data = [
            'vw_company_name' => Hey::get_app('company_name'),
            'vw_admin_root' => Hey::get_app('admin_root'),
            'vw_disputes'    => $disputes,
        ];

        return View::make('admin.task-disputes', $view_data);
    }
    
    

    /**
     *  [API] Performs admin logins.
     *
     *  @param string $username POST[username] The username to be logged in.
     *  @param string $password POST[password] The user's password.
     *  @return A JSON response object.
     */
    public function login() {

            if(Auth::check()) {
               throw new AGInvalidStateException('You\'re already logged in');
            }

            $this->is_required([
                    'username'  => 'The username is missing.',
                    'password'  => 'The password is missing.',
            ]);

            $posted = [
                    'username' => $this->request->input('username'),
                    'password' => $this->request->input('password')
            ];

            // If the input doesn't pass the validation checks just throw up here.
            try {
                    Validate::username($posted['username']);
                    Validate::password($posted['password']);
            } catch (AGValidationException $exc) {
                    throw new AGAuthenticationException
                    ('Login failed: invalid credentials.');
            }

    // Verify credentials
    if (Auth::attempt($posted)) {

            if(!Auth::user()->is_admin()) {
           throw new AGAuthorizationException;
            }

            $redirect_url = URL::to(Hey::get_app('admin_root'));

            $results = [
               'hello'    => Auth::user()->first_name.' '.Auth::user()->last_name,
               'redirect' => $redirect_url,
            ];
    }
    else {
            throw new AGAuthenticationException
            ('Login failed: invalid credentials.');
    }

            return APIResponse::ok()->with($results)->go();
    }


    /**
     *  [API] Performs admin logouts.
     *
     *  @return A JSON response object (with a nice message).
     */
    public function logout() {

            $this->authorize('admin', Auth::user());

            if(Auth::check()) {
       Auth::logout();
       $results = ['message'=>'Bye :)'];
            } else {
       $results = ['message'=>'You are not logged in.'];
            }

            return APIResponse::ok()->with($results)->go();
    }


    /**
     *  [API] Get full info about a user account.
     *  
     *  The information returned by this method depend on the type of user.
     *  
     * @param unknown $user_id
     * @return A JSON response object with the user account info.
     */
    public function get_user_account($user_id) {

        $this->authorize('admin', Auth::user());

        $user = User::where('id', $user_id)->first();

        if($user->is_provider()) {
            
           $provider = $user->as_a('provider');
           $provider->load(['identity']);
           $provider->identity->load('documents');
           $user_ = $provider->toArray();
           $user_['id'] = $provider->id;
           $user_['services'] = $provider->services_list(false);
        }
        else
        if($user->is_customer()) {

        }
        else {
           throw new AGException('Invalid user.');
        }

        return APIResponse::ok()->with($user_)->go();
    }


    /**
     *  [API] Get the document files attached to the user identity.
     *  
     *  @param unknown $user_id
     *  @param unknown $doc_id
     *  @throws AGAuthorizationException
     */
    public function get_identity_doc($user_id, $doc_id) {

        $this->authorize('admin', Auth::user());

        $archive = app('archive');

        $user = User::where('id', $user_id)->first();
        $document = Document::where('id', $doc_id)->first();

        // Identity docs can only be fetched for providers.
        if(!$user->is_provider())
            throw new AGAuthorizationException;
        
        if(!$document->is_hosted)
            throw new AGResourceNotFoundException;

        $provider = $user->as_a('provider');
        
        // We know the context where id documents are stored, so just need the filename.
        $file = $archive->of($provider)->get($document->filename,
                                             User::DOCUMENTS_DIR,
                                             $document->is_encrypted,
                                             $document->is_compressed);
        
	// If the file is currently not in its original format (e.g. it has been encrypted
	// and/or compressed) then we cannot get the original file info from the current
	// file, so we need to check the contents buffer after it has been restored.
        //$fileinfo = $archive->of($provider)->get_file_info($doc_id, 'activation');
	$fileinfo['mime'] = (new \finfo(FILEINFO_MIME_TYPE))->buffer($file);
	$fileinfo['size'] = strlen($file);
	$fileinfo['filename_orig'] = $document->filename_orig;

        $download = new Response($file);
        $download->header('Content-Type', $fileinfo['mime'])
                 ->header('Content-Disposition', 'attachment; filename="'.$fileinfo['filename_orig'].'"')
                 ->header('Content-Length', $fileinfo['size']);

        return $download;
    }


    /**
     *  [API] Change identity verification states.
     *  
     *  @param unknown $provider_id
     *  @param unknown $action
     *  @throws AGValidationException
     *  @throws AGResourceNotFoundException
     *  @throws AGInvalidStateException
     */
    public function identity($provider_id = null, $action = null) {

        $this->authorize('admin', Auth::user());

        if(empty($provider_id) || empty($action))
           throw new AGValidationException('Missing request paramater: $provider_id='.
                                           $provider_id.', $action='.$action);

        DB::transaction(function () use ($provider_id, $action) {
            
            // Get a managed/locked provider identity record (some identity fields
            // are concurrently set by both the platform and the external service).
            $provider = app('identity.manager')->of($provider_id)->get_provider();
            
            if($provider->identity->is_closed())
               throw new AGInvalidStateException('Identity verification closed.');
            
            // NOTE: None of the actions will change the identity status, which
            //       is managed by a system job (VerifyIdentity).
            switch($action) {
                
                // Flag the provider as accepted/verified by the platform. If the
                // identity also needs external verification then it will be fully
                // verified only if the external service set its flag as well.
                case 'accept':
                     $provider->identity->int_verified = true;
                     break;
                
                // Notify the provider that further info must be provided or
                // corrected by setting the relevant field in the identity record.
                case 'inquiry':
                     $info = $this->request->input('info');
                     $reason = $this->request->input('reason');
                     if(!$info || !$reason)
                         throw new AGValidationException('Missing parameters.');
                     $required_info = $info ? explode(',', $info) : [];
                     $provider->identity->add_required_info($required_info);
                     $provider->identity->add_notes($reason);
                     break;
                 
                // Flag the provider as rejected/unverified by the platform. If the
                // identity also needs external verification then it will be fully
                // rejected regardless of the service outcome.
                case 'reject':
                     $provider->identity->int_verified = false;
                     break;
                 
                default :
                     throw new AGAuthorizationException;
            }
            
            $provider->identity->save();
            
            Event::fire(new EventUserIdentityVerification($provider->id));
        });

        return APIResponse::ok()->go();
    }
    
    
    /**
     * Resolve a dispute for a task in favour of the specified user.
     * 
     * @param type $task_ref
     * @param type $user_ref
     * @throws AGResourceNotFoundException
     * @throws AGAuthorizationException
     */
    public function dispute_resolve($task_ref, $user_ref) {        
        
        DB::transaction(function() use ($task_ref, $user_ref) {
            
            $task = app('task.manager')->of($task_ref)->get_task();
            
            $task->load(['dispute','customer','provider']);
            
            if(!$task->dispute)
               throw new AGInvalidStateException('The task is not disputed');

            if($task->dispute->status === K::TASK_DISPUTE_RESOLVED)
               throw new AGInvalidStateException('This dispute has been resolved');

            if($task->customer->ref !== $user_ref && 
               $task->provider->ref !== $user_ref)
               throw new AGAuthorizationException;

            $tlm = $task->get_manager();
            $tlm->set_requestor(Auth::user());
            
            $winner = $task->customer->ref === $user_ref ? 
                      $task->customer : 
                      $task->provider;

            $filter = [
                ['type','=',K::TXN_TYPE_PAYMENT],
                //['status','=',K::TXN_PENDING],
                ['id_tasks','=',$task->id],
                ['id_sender','=',$winner->id],
            ];
            
            $security = [
                'request_id' => $this->request->ip(),
                'request_auth' => Auth::check()
            ];
            
            
            if($winner->is_customer()) {
                              
               $payments = Transaction::where($filter)->get();
               
               // If there are pending (not yet cleared) payments for the task
               // then refund them to the customer.
               if(!$payments->isEmpty()) {
               
                  try {
                  // - Refund all payments to customer
                  foreach($payments as $payment) {

                     $refund = app('account')->of($task->provider)
                                             ->refund($task->customer)
                                             ->for_this($payment)
                                             ->using($security);

                     $txn_ref = $refund->execute();
                     $task->transactions()->save($txn_ref);

                     // If the refund amount is taken back from the provider's account 
                     // then pay back the app fee. If the flag is set, the app fee is
                     // automatically paid back by Stripe when the refund is executed,
                     // so we just need to log the transaction. Currently it seems there
                     // is no way to get the transaction for the payment made for the app
                     // fee refund, so here we just log a Payment without making a transfer.
                     if(Hey::getval($txn_ref->params(), 'refund_app_fee')) {

                        $txn_fee = app('account')->of(Auth::user())
                                                 ->pay($task->provider,
                                                       abs( $payment->amount_fee) )
                                                 ->log_only()
                                                 ->using($security)
                                                 ->execute();
                        // Mark as settled
                        $txn_fee->settle_status = 'succeeded';

                        $task->transactions()->save($txn_fee);
                     }
                     
                    // Notify customer.
                    $notification = new TransactionNotification($txn_ref);
                    $notification->message = trans('messages.txn-refund-made', [
                        'refund-amount' => Hey::to_currency(abs($txn_ref->amount_gross)),
                        'task-subject' => $task->subject,
                    ]);
                    $notification->to($task->id_users_customer);
                    
                    app('notifier')->push($notification);
                    
                  }
                  // If the refund did not went through propagate the exception
                  // otherwise proceed and log the transaction.
                  } catch(\Exception $exc) {
                    if(!isset($txn_ref))
                        throw new AGException('ERROR', $txn_ref, $exc);
                  }
               }
               // - Cancel task
               $tlm->cancel();
            }
            else
            if($winner->is_provider()) {
               // - Close task
               $tlm->close();
            }
            
            $task->dispute->status = K::TASK_DISPUTE_RESOLVED;
            $task->dispute->winner = $winner->id;
            $task->dispute->results = "Resolved in ".$winner->role_to_string()."'s favour.";
            $task->dispute->results_datetime = Hey::now_to_db_datetime();
            $task->dispute->save();

            $customer_notification = new TaskDisputeResolvedNotification($task);
            $provider_notification = new TaskDisputeResolvedNotification($task);
            $customer_notification->to($task->id_users_customer);
            $provider_notification->to($task->id_users_provider);
            
            app('notifier')->push([$customer_notification, $provider_notification]);
        });
        
        return APIResponse::ok()->go();
    }
    

    public function deploy() {
        
        $params = $this->request->input('args');
        $args = is_array($params) ? $params : [$params];
        
        $script_dir = Hey::get_app('deploy_script_dir');
        $script_name = Hey::get_app('deploy_script_name');
        $script_args = implode(' ', $args);
        
        $cmd = 'bash '.$script_dir.'/'.$script_name.' '.$script_args;
        
        $output = []; $return = 0;
        
        exec($cmd, $output, $return);
        // Windows
        //pclose(popen("start /B ". $cmd, "r"));
        
        return implode('<br>', $output);
    }
    
    
    public function app_online() {
        
    }
    
    public function app_offline() {
        
    }

}

