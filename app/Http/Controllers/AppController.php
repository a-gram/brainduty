<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\APIResponse;
use App\Models\User;
use App\Models\Message;
use App\Models\Notification;
use App\Exceptions\AGException;
use App\Exceptions\AGValidationException;
use App\Hey;
use App\K;
use App, Auth, DB, View, URL;

/**
 *  This controller is the boss of controllers. It manages stuffs related
 *  to system administration, jobs dispatching, general web pages management,
 *  etc.
 */
class AppController extends AGController {

    public function __construct(Request $request) {
        parent::__construct($request);
    }


    /**
     *  The app's home page.
     */
    public function home_page() {

        $view_data = [
                'vw_company_name' => Hey::get_app('company_name'),
        ];

        return View::make('portal.home', $view_data);
    }


    /**
     *
     * @throws AGValidationException
     */
    public function get_updates() {

        // Only customers and providers authorized to access this method.
        $this->authorize('provider_or_customer', Auth::user());

        $requestor = Auth::user();

        $chans = $this->request->input('ch');

        if(empty($chans))
           throw new AGValidationException('Missing channel/topic parameter.');
        
        $channels = [];

        // NOTE: Notifications are currently 'acked' at the source.
        $nacks = [];
        // NOTE: If the following parameter is present in the request don't ack.
        $noack = $this->request->has('noack');
        

        foreach ($chans as $channel) {

            if(!$channel) $channel = 'no/channel';

            // Split the channel command into its <channel>/<topic> components
            $channel = explode('/', $channel);

            if(count($channel) != 2)
               throw new AGValidationException('Invalid channel/topic parameter.');

            switch($channel[0]) {

                // Task messages channel.
                case 'messages':

                    $topic = $channel[1];

                    // Get new messages
                    if ($topic === 'new') {

                        $relations = ['task','attachments'];

                        $filter = [ ['is_deliverable',true],
                                    ['is_delivered', false],
                                    ['is_seen', false],
                                    ['type', '!=', K::MESSAGE_TASK_REQUEST],
                                    ['id_users_recipient', $requestor->id] ];

                        $query = Message::where($filter)->with($relations);
                    } else
                    // Get unseen messages
                    if ($topic === 'unseen') {

                        $relations = ['task','attachments'];

                        $filter = [ ['is_deliverable',true],
                                    ['is_delivered', true],
                                    ['is_seen', false],
                                    ['type', '!=', K::MESSAGE_TASK_REQUEST],
                                    ['id_users_recipient', $requestor->id] ];

                        $query = Message::where($filter)->with($relations)->
                                          orderBy('created_at','desc');
                    } else
                    // Get latest delivered and seen messages
                    if ($topic === 'latest') {

                        $relations = ['task','attachments'];

                        $filter = [ ['is_deliverable',true],
                                    ['is_delivered', true],
                                    ['is_seen', true],
                                    ['type', '!=', K::MESSAGE_TASK_REQUEST],
                                    ['id_users_recipient', $requestor->id] ];

                        $query = Message::where($filter)->with($relations)->
                                          orderBy('created_at','desc')->
                                          take(6);
                    } else {
                        throw new AGValidationException
                        ('Unsupported topic for channel "'.$channel[0].'"');
                    }

                    $messages  = $query->get();
                    
                    $messages_ = [];

                    foreach ($messages as $message) {
                             $message_ = $message->toArray();
                             $message_['task'] = $message->task->ref;
                             $message_['sender'] = $message->get_sender_role();
                             $messages_[] = $message_;
                    }

                    $channels[$channel[0]][$topic] = $messages_;

                    break;

                // Notifications channel.
                case 'notifications':

                    $topic = $channel[1];

                    // Get new notifications
                    if ($topic === 'new') {

                        $filter = [ ['is_delivered', false],
                                    ['is_seen', false],
                                    ['id_users_recipient', $requestor->id] ];

                        $query = Notification::where($filter)->
                                               orderBy('created_at','desc');
                        $nack = true;
                    } else
                    // Get latest n delivered and seen notifications
                    if ($topic === 'latest') {

                        $filter = [ ['is_delivered', true],
                                    ['id_users_recipient', $requestor->id] ];

                        $query = Notification::where($filter)->
                                               orderBy('created_at','desc')->
                                               take(6);
                        $nack = false;
                    } else {
                        throw new AGValidationException
                        ('Unsupported topic for channel "'.$channel[0].'"');
                    }

                    $notifications = $query->get();
                    $notifications_ = $notifications->toArray();

                    // *** See NOTE above.
                    if($nack)
                       $nacks = array_merge($nacks, $notifications->pluck('id')->all());

                    $channels[$channel[0]][$topic] = $notifications_;

                    break;

                default:
                    //
            }

        }

        // *** See NOTE above
        if(count($nacks) && !$noack)
           Notification::whereIn('id', $nacks)->update(['is_delivered' => true]);
        
        // Here we acknowledge the user as being online and mark its last seen time.
        // NOTE: This signalling is currently done as part of the poll performed
        // by the client. In a full bi-directional communication (websocket et al.)
        // we need a sort of PING call from the client (or some other method to mark
        // the user as being online).
        User::where('id', $requestor->id)->update(['lastseen_at' => Hey::now_to_db_datetime()]);

        return APIResponse::ok()->with($channels)->go();
    }
    
    
    // ========================== TESTING STUFFS================================
    
    public function test() {
        
        $msg = 'ok';
        
        //$this->test_withdraw();
        
        return $msg;
    }
    
    private function test_withdraw() {
        
        $this->authorize('provider', Auth::user());
        $provider = Auth::user()->as_a('provider');
        $account = app('account')->of($provider);
        $balance = $account->get_balance(true);
        $transfer = \Stripe\Transfer::create([
             'amount'      => $balance->available,
             'currency'    => Hey::get_app('currency'),
             'destination' => 'default_for_currency',
          ],
          [  'stripe_account' => $provider->ref_ext  ]
        );

    }
    
    // =========================================================================
    
}
