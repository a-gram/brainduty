<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\Customer;
use App\Models\User;
use App\Models\ProviderIdentity;
use App\Models\Payment;
use App\Models\CustomerSettings;
use App\Events\EventUserSignup;
use App\Hey;
use App\Utils\Validate;
use App\Archive;
use App\K;
use App\APIResponse;
use Exception;
use App\Exceptions\AGException;
use App\Exceptions\AGAuthorizationException;
use App\Exceptions\AGInvalidStateException;
use App\Exceptions\AGValidationException;
use App\Exceptions\AGMarketplaceException;
use URL, View, DB, Auth, Session, Event, Hash;



class CustomerController extends UserController
{
    /**
     * Create a new customer controller instance.
     * Always call the parent/base class to init stuffs.
     *
     * @return void
     */
    public function __construct(Request $request) {
    	parent::__construct($request);
    }

    
    /**
     * Customer welcome page
     *
     */
    public function welcome_page() {
    
    	// Only customers authorized to access this method.
    	$this->authorize('customer', Auth::user());
    	 
    	$customer = Auth::user()->as_a('customer');
        
    	$view_data = [
            'vw_company_name'  => Hey::get_app('company_name'),
            'vw_customer'      => $customer,
    	];
    	     	
    	return View::make('dashboard.customer.welcome', $view_data);
    }
    
    
    /**
     * Customer home page
     *
     */
    public function home_page() {
    
    	// Only customers authorized to access this method.
    	$this->authorize('customer', Auth::user());
    	
    	$customer = Auth::user()->as_a('customer');
    	
    	// Get the list of the customer's open tasks.
    	$tasks = Task::where('id_users_customer', $customer->id)->
    	               where('status','!=',k::TASK_CANCELLED)->
    	               where('status','!=',k::TASK_COMPLETED)->
                       orderBy('schedule_datetime','desc')->
                       paginate(K::MAX_ITEMS_PER_PAGE);
    	
        // Get account alerts, if any.
        $alerts = app('account.manager')->of($customer)->get_alerts();
        
    	$view_data = [
            'vw_company_name'  => Hey::get_app('company_name'),
            'vw_alerts'        => $alerts,
            'vw_open_tasks'    => $tasks,
            'vw_customer'      => $customer,
            'vw_chat_enabled'  => true,
    	];
    	
    	return View::make('dashboard.customer.home', $view_data);
    }
    

    /**
     * Display the Customer Closed Tasks page.
     */
    public function closed_tasks_page() {
    
    	// Only customers authorized to access this method.
    	$this->authorize('customer', Auth::user());
    	 
    	$customer = Auth::user()->as_a('customer');
    	 
    	// Get the list of the customer's closed tasks.
    	// Note that there may be only a few closed tasks per customer (probably a
    	// fixed number) so there shouldn't be the need for pagination here.
    	$tasks = Task::where('id_users_customer', $customer->id)->
    	               where(function ($query) {
    	                     $query->where('status','=',k::TASK_CANCELLED)->
    	                           orWhere('status','=',k::TASK_COMPLETED);
    	               })->
                       orderBy('schedule_datetime','desc')->
                       paginate(K::MAX_ITEMS_PER_PAGE);
    	
    	$view_data = [
    		'vw_company_name'  => Hey::get_app('company_name'),
    		//'vw_user_fullname' => $customer->first_name.' '.$customer->last_name, 
    		'vw_closed_tasks'  => $tasks,
    	];
        
    	return View::make('dashboard.customer.closed_tasks', $view_data);
    }
    
    
    /**
     * Display the Customer Settings page.
     */
    public function settings_page() {
    
    	// Only customers authorized to access this method.
    	$this->authorize('customer', Auth::user());
    	
    	$customer = Auth::user()->as_a('customer');
    	
    	$customer->default_payment_method();
    	$customer->load('settings');
    	
    	$view_data = [
            'vw_company_name' => Hey::get_app('company_name'),
            'vw_customer' => $customer,
            'vw_market_pkey' => env('MARKET_API_PKEY'),
    	];
    	
    	return View::make('dashboard.customer.settings', $view_data);
    }
    
    
    /**
     *  Customer registration page.
     */
    public function signup_page() {

    	$view_data = [
    		'vw_company_name' => Hey::get_app('company_name'),
    	];
    	 
    	return View::make('portal.customer.signup', $view_data);
    }
    
    
    /**
     *  [API] Register a new customer with the platform.
     *
     *  @param array POST[customer] Contains the customer data.
     *  @return A JSON response object.
     */
    public function signup() {

    	// Authenticated users have not much to do here.
    	if(Auth::check())
    	   throw new AGInvalidStateException("You're already registered.");
    	
    	// Validate required fields.
    	$this->is_required([
            'customer'            => 'No customer data received.',
            'customer.first_name' => 'Missing first name.',
            'customer.last_name'  => 'Missing last name.',
            'customer.username'   => 'Missing username.',
            'customer.password'   => 'Missing password.',
    	]);
    	
    	// If validation passed get the customer's info
    	$customer_wannabe = $this->request->input('customer');
    	
    	// Trim the request parameters to avoid unwanted effects
    	Hey::trim_record($customer_wannabe);
    	
    	$customer = new Customer ($customer_wannabe);
    	$customer->password = $customer_wannabe['password'];
    	$customer->validate();
        
        app('account.manager')->of($customer)->register();
        
        // Log the new customer in.
        Auth::loginUsingId($customer->id);
        
        $results = ['redirect' => URL::to('customer/welcome')];

        return APIResponse::ok()->with($results)->go();
    }
    
    
    /**
     * [API] Saves a customer record into database.
     *
     * @param array PUT[customer] An array with the customer's data. All fields
     * are optional but an error will be returned if no data is provided.
     * @return A JSON response object with the updated fields.
     */
    public function update() {
    	
    	// Only customers authorized to access this method.
    	$this->authorize('customer', Auth::user());
    	
    	$this->is_required([
            'customer' => 'No customer data received.',
    	]);
    	
    	// Currently, the following info cannot be changed.
    	$this->is_not_accepted([
            'customer.username'   => 'Usernames cannot be changed.',
            'customer.first_name' => 'Account holder names cannot be changed.',
            'customer.last_name'  => 'Account holder names cannot be changed.',
    	]);
    	
    	// If validation passed get the provider's updated fields
    	$updated = $this->request->input('customer');
        $updated['avatar'] = $this->request->allFiles();
    	
    	// Trim the request parameters to avoid unwanted effects
    	Hey::trim_record($updated);
    	
    	$customer = Auth::user()->as_a('customer');
    	//$customer->forget('*', ['id','updated_at']);
    	
    	$user_info = [/*'first_name','last_name','dob',*/'mobile','about',
                        'address','city','state','zip_code','country'];
    	
    	// Update profile and billing info.
    	if(Hey::has_field($user_info, $updated)) {
            $customer->fill($updated);
            $customer->validate();
    	}
    	else
        // Update password.
        if(Hey::has_field('password', $updated)) {
            // Check for the current password
            $this->is_required([
                'customer.old_password' => 'No current password received.',
            ]);
            
            if($updated['password'] === $updated['old_password'])
               throw new AGValidationException('The new password is the same as the old one.');
            
            // Verify the provided current password.
            if(!Auth::validate([
                'id'       => $customer->id,
                'password' => $updated['old_password']
            ])) {
                throw new AGAuthorizationException('The old password is invalid.');
            }
            Validate::password($updated['password']);
            $customer->password = Hash::make($updated['password']);
        }
        
        // Update avatar
        if(Hey::has_field('files', $updated['avatar'])){
           Validate::attachments($updated['avatar']['files'], 'avatar');
           $customer->store_files($updated['avatar']['files'], 
                                  'avatar', 'avatar',false, false);
        }

    	$customer->save();
    	
    	return APIResponse::ok()->go();
    }
    
    
    /**
     *  [API] This method is called if the customer has no payment method on file.
     *  
     *  POST /customer/payment
     *
     *  @param array POST[] Contains the following details:
     *  [
     *      payment   => <payment method details or token>
     *     (customer) => <customer info, tipically billing info>
     *  ]
     *  
     *  @return A JSON response object.
     */
    public function register_payment() {
    
    	$this->authorize('customer', Auth::user());
        
        if(Auth::user()->ref_ext) {
           throw new AGInvalidStateException('Already registered');
        }
    	
        // If no payment method is on file, we should open an account on the 
        // marketplace provider and register the payment method. This is actually 
        // done automatically when a payment method is updated.
    	return $this->update_payment('register');
    }

    
    /**
     *  [API] Updates a payment method for the customer.
     *
     *  PUT /customer/payment/{action}
     *  
     *  @param array POST[] Contains the following details:
     *  [
     *      payment   => <payment method details or token>
     *     (customer) => <customer info, tipically billing info>
     *  ]
     *  @param string $action The operation to be performed on the payment.
     *
     *  @return A JSON response object.
     */
    public function update_payment($action) {
    
    	$this->authorize('customer', Auth::user());
    	
    	if (!$action)
            throw new AGInvalidStateException('Update action is missing.');
    	
    	// Validate required fields.
    	$this->is_required([
            'payment'           => 'No payment data.',
            'payment.method'    => 'Missing payment method type.',
            'payment.source_id' => 'Missing payment source id.',
            // Note: the following info can still be retrieved when registering
            // this method with the marketplace provider, so they're not required.
            //'payment.source_number' => 'Missing payment method number.',
            //'payment.source_type'   => 'Missing payment method type.',
            //'payment.source_name'   => 'Missing payment method holder.',
            //'payment.source_expiry' => 'Missing payment method expiry date.',
    	]);
    	
    	$posted = [
            'payment'  => $this->request->input('payment'),
            'customer' => $this->request->input('customer'),
    	];
    	 
    	// Trim the request parameters to avoid unwanted effects
    	Hey::trim_record($posted);
    	
    	$customer = Auth::user()->as_a('customer');
    	
    	$payment = new Payment ($posted['payment']);
    	$payment->validate();
    	
    	// If customer details are posted then update the current customer.
    	if($posted['customer']) {
    	   // Currently, the following info cannot be changed.
    	   $this->is_not_accepted([
                'customer.username'   => 'Usernames cannot be changed.',
                'customer.first_name' => 'Account holder names cannot be changed.',
                'customer.last_name'  => 'Account holder names cannot be changed.',
    	   ]);
    	   $customer->fill($posted['customer']);
    	   $customer->validate();
    	}
    	
    	// We should always check that required identity details are present
    	// at this point before making requests to the marketplace provider.
    	// Required details vary depending on the service provider.
    	if(!$customer->has_set(['first_name','last_name','address',
                                'zip_code','city','country']))
           throw new AGInvalidStateException('Identity info are missing.');
    		 
        $customer->set_data('request_ip', $this->request->ip());
        
        $account = app('account')->of($customer);
        $action = 'payment.'.$action;
        $params = ['payment' => $payment];
        $results = [];

        DB::transaction(function () 
            use ($customer, $account, $action, $params, &$results)
        {
            try {
                // If the customer has no account on the marketplace provider
                // open one now with the given payment method.
                if(!$customer->ref_ext) {
                    $mp_account = $account->open($params['payment']);
                    $results = $mp_account['payment'];
                }
                else {
                    $new_payment = $account->update($action, $params);
                    $results = $new_payment->attributesToArray();
                }
                
                // Set the default payment method if none is set
                if(empty($customer->settings->default_payment_method))
                   $customer->settings->default_payment_method = $params['payment']->method;

                $customer->save();
                $customer->settings->save();
            }
            // Propagate recoverable exceptions.
            catch (AGMarketplaceException $exc) {
                throw $exc;
            }
            // If the customer account was opened and an exception occurs afterwards
            // it will not be linked to the customer and will remain dangling on the
            // marketplace provider since it is not rolled back, so we catch errors
            // here and close/delete the dangling account.
            catch(Exception $exc) {

               if(isset($mp_account)) {
                  $account->close();
               }
               throw $exc;
            }
        });

        return APIResponse::ok()->with($results)->go();
    }
    
}
