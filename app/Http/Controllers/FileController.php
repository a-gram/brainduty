<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use App\Http\Controllers\AGController;
use App\Models\AGEntity;
use App\Models\User;
use App\Models\Task;
use App\Models\Customer;
use App\Models\Service;
use App\Models\Message;
use App\Models\Attachment;
use App\Utils\Validate;
use App\Hey;
use App\Archive;
use App\Exceptions\AGException;
use App\Exceptions\AGValidationException;
use App\Exceptions\AGInvalidStateException;
use App\Exceptions\AGAuthorizationException;
use App\Exceptions\AGResourceNotFoundException;
use App\APIResponse;
use App\K;
use App, URL, View, DB, Auth, Redirect;
use Exception;
use Throwable;

/**
 *  This controller handles file-related operations. 
 */
class FileController extends AGController
{
    /**
     * Create a new file controller instance.
     * Always call the parent/base class to init stuffs.
     *
     * @return void
     */
    public function __construct(Request $request) {
        parent::__construct($request);

    }


    /**
     * [API] Get a task file/attachment.
     *
     * GET \task\{ref}\file\{fid}
     *
     * @param string $task_ref The reference of the task for which to get the file.
     * @param string $fid The identifier of the file to be retrieved.
     * @return A JSON response object with the retrieved file.
     */
    public function get_one($task_ref, $fid) {

        // Only customers and providers authorized to access this method.
        $this->authorize('provider_or_customer', Auth::user());

        if(!isset($task_ref))
            throw new AGValidationException('No task reference given.');

        if(!isset($fid))
            throw new AGValidationException('No file identifier given.');

        $task = Task::where('ref', $task_ref)->first();

        if(!$task)
            throw new AGResourceNotFoundException('Task not found : '.$task_ref);

        $attach = Attachment::where('id', $fid)->first();

        if(!$attach)
            throw new AGResourceNotFoundException('Attachment not found : '.$fid);

        // Make sure the user is authorized to act on this task.
        // If the task is not yet assigned then providers can access it for bidding.
        // TODO This should probably be a policy on tasks.
        if(!$task->has_actor(Auth::user()->id) &&
           ($task->is_assigned() ||
            !Auth::user()->is_provider())) {
           throw new AGAuthorizationException(trans('messages.task-unavailable'));
        }

        $file = app('archive')->of($task)->get($attach->filename, '', 
                                               $attach->is_encrypted, 
                                               $attach->is_compressed);

        $download = new Response($file);
        $download->header('Content-Type', $attach->mime)
                 ->header('Content-Disposition', 'attachment; filename="'.$attach->filename_orig.'"')
                 ->header('Content-Length', $attach->size);

        return $download;
    }
    
    
    public function get_avatar($user_ref) {
        
        $this->authorize('provider_or_customer', Auth::user());
        
        if(!$user_ref)
            throw new AGValidationException;
        
        return "ok";
    }


}
