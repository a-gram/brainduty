<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\AGController;
use App\Models\AGEntity;
use App\Models\User;
use App\Models\Task;
use App\Models\Customer;
use App\Models\Service;
use App\Models\Message;
use App\Models\Attachment;
use App\Utils\Validate;
use App\Hey;
use App\Archive;
use App\Exceptions\AGException;
use App\Exceptions\AGValidationException;
use App\Exceptions\AGInvalidStateException;
use App\Exceptions\AGAuthorizationException;
use App\Exceptions\AGResourceNotFoundException;
use App\APIResponse;
use App\K;
use App, URL, View, DB, Auth, Redirect;
use Exception;
use Throwable;

/**
 *  This controller 
 */
class MessageController extends AGController
{
	/**
	 * Create a new message controller instance.
	 * Always call the parent/base class to init stuffs.
	 *
	 * @return void
	 */
	public function __construct(Request $request) {
		parent::__construct($request);

		//$this->middleware('auth', ['except' => ['login']);
	}
	
		
	/**
	 * [API] Get message records.
	 *
	 * GET \message
	 * GET \message?with=attachments&where=message.new
	 *
	 * @param array|string $with If present in the query string, this field indicates
	 * the related entities that must be included with the returned message.
	 * @param string $where A filter applied when retrieving the message. Note that this is
	 * not an SQL clause (although it usually translates to one or more) but a filter name
	 * in the form {target}.{filter} that must be mapped to some implementation.
	 * 
	 * Currently implemented filters:
	 * 
	 *        1) message.new = retrieve all new received messages for the current user
	 *        2) message.unseen = retrieve all unseen messages for the current user.
	 *           
	 * @throws AGValidationException
	 * @throws AGResourceNotFoundException
	 * @return A JSON response object with the retrieved message.
	 */
	public function get() {
	
		// Only customers and providers authorized to access this method.
		$this->authorize('provider_or_customer', Auth::user());
		
		$relations = $this->request->get('with');
		$relations = $relations ? (!is_array($relations) ? [$relations] : $relations)
                                : null;
		$relations[] = 'task';
		
		$where = $this->request->input('where', 'no.filter');
		$filter_type = explode('.', $where);
		
		$messages = null;
		
		switch ($filter_type[0]) {
			// Message filters
			case 'message':
				// New messages filter.
				if(count($filter_type)===2 && $filter_type[1]==='new') {
					
					$filter = [ ['is_deliverable',true],
							    ['is_delivered', false],
		                        ['is_seen', false],
							    ['type', '!=', K::MESSAGE_TASK_REQUEST],
		                        ['id_users_recipient', Auth::user()->id] ];
		            
					$messages = Message::where($filter)->with($relations)->get();
				} else
				// Unseen messages filter.
				if(count($filter_type)===2 && $filter_type[1]==='unseen') {

					$filter = [ ['is_deliverable',true],
							    ['is_delivered', true],
							    ['is_seen', false],
							    ['type', '!=', K::MESSAGE_TASK_REQUEST],
							    ['id_users_recipient', Auth::user()->id] ];
					
					$messages = Message::where($filter)->with($relations)->get();						
				} else {
					throw new AGValidationException('Unrecognized message filter.');
				}			
				break;
			// Attachment filters
			case 'attachments':
				break;
			// No filter.
			default:
				// Retrieve all messages for this user.
				$filter = [ ['is_deliverable',true],
				            ['id_users_recipient', Auth::user()->id],
                            ['id_users_sender', Auth::user()->id] ];
				
		        $messages = Message::where([$filter[0]])->orWhere([$filter[1]])
		                           ->with($relations)
		                           ->get();
		}
		
		if(!$messages)
			throw new AGResourceNotFoundException('Message not found : '.$msg_ref);
		
		$messages_ = [];
		
		foreach ($messages as $message) {
			if(!$message->has_actor(Auth::user()->id))
				throw new AGAuthorizationException;
			$message_ = $message->toArray();
			$message_['task'] = $message->task->ref;
			$message_['sender'] = $message->get_sender_role();
			$messages_[] = $message_;
		}
		
		return APIResponse::ok()->with(['messages'=>$messages_])->go();
	}
	
	
	
	/**
	 * [API] Get a message.
	 *  
	 * GET \message\{ref}[?with=attachments]
	 *  
	 * @param string $msg_ref
	 */
	public function get_one($msg_ref) {
	
	}
	
	
	/**
	 * [API] Create a new message for the specified task.
	 *
	 * POST \task\{ref}\message  =>  message[]
	 *
	 * @param array message[] An array with the message data.
	 * @param string $task_ref The reference of the task for which to create
	 * the new message. For task request messages, it is 'new', indicating
	 * that a new task is gonna be created.
	 */
	public function create($task_ref) {
	
            // Only customers and providers authorized to access this method.
            $this->authorize('provider_or_customer', Auth::user());

            if(!isset($task_ref))
               throw new AGValidationException('No task reference given.');

            // Note that a task may not yet exist when a message is posted (example:
            // a transactional message that requests a task), so we may get null.
            $task = null;

            // If the message is related to an existing task then fetch and verify it.
            if($task_ref != 'new') {
               
               if(!($task = Task::where('ref', $task_ref)->first()))
                  throw new AGResourceNotFoundException('Task not found : '.$task_ref);
               
               if(!$task->has_actor(Auth::user()->id))
                  throw new AGAuthorizationException;
               
               if($task->is_ending)
                  throw new AGInvalidStateException(trans('exceptions.task-ending'));
            }
		
            // The following fields are always present in a message, so we can
            // check them here. Other fields presence depends on the type of message.
	    $this->is_required([
                'message'       =>  'No message data received.',
                'message.type'  =>  'No message type specified.',
                'message.body'  =>  'No message body specified.',
	    ]);
		   
            // Collect posted data and other info.
            $posted = [
                'message' => $this->request->input('message'),
                'sender'  => Auth::user(),
                'task'    => $task,
            ];
		
            Hey::trim_record($posted['message']);
                
	    // Get attachments, if any.
	    $attachs = $this->request->allFiles();
	    
            $posted['message']['attachments'] = !empty($attachs) ? $attachs['files'] : [];

            Validate::attachments( $posted['message']['attachments'] );
                                              
            $message = App::make('message')->from($posted);
	    
            // For messages posted to existing tasks, return the created message,
            // otherwise for new tasks just return a success message.
            if($task) {
               $message_ = $message->toArray();
               $message_['sender'] = $message->get_sender_role($task);
    // 	       $message_['sender_name'] = Auth::user()->short_name();
    // 	       $message_['recipient_name'] = User::find($message->id_users_recipient)->short_name();
               $message_['task'] = $task->ref;
            } else {
               $message_ = ['body' => trans('messages.task-scheduled')];
            }
	     
	    return APIResponse::ok()->with($message_)->go();
	}
	
	
	/**
	 * [API] Updates a message record in the database.
	 * 
	 * PUT \message\{ref}  =>  message[]
	 *
	 * @param array message[] An array that contains the message data to be saved.
	 * All updatable fields are optional and it will be a no-op if no data is provided.
	 * The following are updatable fields:
	 *   1) is_delivered
	 *   2) is_seen
	 * These fields can only be updated by the receiver of the message.
	 * @return A JSON response with the updated fields, if any.
	 */
	public function update($msg_ref) {
		 
		// Only customers and providers authorized to access this method.
		$this->authorize('provider_or_customer', Auth::user());
		
		if(!isset($msg_ref))
			throw new AGValidationException('No message reference given.');
		
		if(!$this->request->has('message'))
			return APIResponse::ok()->go();
		
		// Message bodies cannot be changed.
		$this->is_not_accepted([
			'message.body' => 'The message body cannot be changed.',
		]);

		$posted = $this->request->input('message');
		
		// Trim the request parameters to avoid unwanted effects
		//Hey::trim_record($posted);
		
		// Make sure the requesting client is the legit recipient.
		$recipient_id = Auth::user()->id;
		
		$updated = [];
		
		if (Hey::getval($posted,'is_delivered') == true) {
			$updated['is_delivered'] = true;
			$updated['delivery_datetime'] = Hey::now_to_db_datetime();
		}
		if (Hey::getval($posted,'is_seen') == true) {
			$updated['is_seen'] = true;
			$updated['seen_datetime'] = Hey::now_to_db_datetime();
		}
		
		if($updated && !Message::where('ref', $msg_ref)
		                       ->where('id_users_recipient', $recipient_id)
                               ->update($updated))
		{
			throw new AGException('Could not update message');
        }
		
		return APIResponse::ok()->with( $updated )->go();
	}
	
	
	/**
	 * [API] Ack messages.
	 * 
	 * PUT /message/ack  =>  messages[]
	 * 
	 * @param array messages The messages to be acked, as follows:
	 * 
	 *     messages[i][ref]          = <message reference>
	 *     messages[i][is_delivered] = true
	 *     messages[i][is_seen]      = true|false
	 * 
	 * @return A JSON response with two arrays: delivered[] and seen[] containing
	 * the message references of those acked as delivered and seen.
	 * @throws AGValidationException
	 */
	public function ack() {
		
		// Only customers and providers authorized to access this method.
		$this->authorize('provider_or_customer', Auth::user());
		
		$this->is_required([
			'messages' => 'No messages to ack received.',
		]);
		
		$messages = $this->request->input('messages');
		
		if (!is_array($messages))
			throw new AGValidationException('Paramater $message must be an array');
		
	    $delivered = [];
	    $seen = [];
		
		foreach ($messages as $message) {
			if(!isset($message['ref']))
				throw new AGValidationException('Missing message reference.');
			// These fields cannot be unset by clients, so if set to false ignore them.
			if (Hey::getval($message,'is_delivered') == true)
				$delivered[] = $message['ref'];
			if (Hey::getval($message,'is_seen') == true)
				$seen[] = $message['ref'];
		}
		
		$recipient_id = Auth::user()->id;
		
		if(count($delivered))
		   Message::where('id_users_recipient', $recipient_id)
                  ->whereIn('ref', $delivered)
                  ->update(['is_delivered' => true,
                            'delivery_datetime' => Hey::now_to_db_datetime()]);
		
        if(count($seen))
           Message::where('id_users_recipient', $recipient_id)
                  ->where('is_delivered', true)
                  ->whereIn('ref', $seen)
                  ->update(['is_seen' => true,
                            'seen_datetime' => Hey::now_to_db_datetime()]);
        
        $acked = [
        	'delivered' => $delivered,
        	'seen'      => $seen
        ];
        
        return APIResponse::ok()->with($acked)->go();
	}
	
	
	/**
	 * [API] Transactional Messages action.
	 * 
	 * POST /message/{msg_ref}/action/{action}[?{params}]
	 * 
	 * The action params, if any, are passed in the query string.
	 */
	public function action($msg_ref, $action) {
	
            // Only customers and providers authorized to access this method.
            $this->authorize('provider_or_customer', Auth::user());

            if(!isset($msg_ref))
                throw new AGValidationException('No message reference given.');

            if(!isset($action))
                throw new AGValidationException('No action given.');

            $message = Message::with('task')->where('ref', $msg_ref)->first();

            if($message->task->status != K::TASK_ONGOING)
               throw new AGInvalidStateException('This task has ended.');

            if(!$message)
                throw new AGResourceNotFoundException('Message not found : '.$msg_ref);

            if(!$message->is_transactional())
                throw new AGAuthorizationException;

            if($message->is_txn_completed)
                throw new AGInvalidStateException('This transaction has been completed.');

            if(!$message->has_actor(Auth::user()->id))
                throw new AGAuthorizationException;

            $posted = [
                'action' => $action,
                'params' => $this->request->query(),
            ];

            $action = App::make('message.action')->for_this($message);
            $results = $action->by(Auth::user())->execute($posted['action'],
                                                          $posted['params']);

            return APIResponse::ok()->with($results)->go();
	}
	
	
}
