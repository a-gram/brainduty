<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\AGController;
use App\Models\AGEntity;
use App\Models\User;
use App\Models\Task;
use App\Models\Customer;
use App\Models\Service;
use App\Models\Notification;
use App\Utils\Validate;
use App\Hey;
use App\Archive;
use App\Exceptions\AGException;
use App\Exceptions\AGValidationException;
use App\Exceptions\AGInvalidStateException;
use App\Exceptions\AGAuthorizationException;
use App\Exceptions\AGResourceNotFoundException;
use App\APIResponse;
use App\K;
use App, URL, View, DB, Auth, Redirect;
use Exception;
use Throwable;

/**
 *  This controller
 */
class NotificationController extends AGController
{
	/**
	 * Create a new notification controller instance.
	 * Always call the parent/base class to init stuffs.
	 *
	 * @return void
	 */
	public function __construct(Request $request) {
		parent::__construct($request);

		//$this->middleware('auth', ['except' => ['login']);
	}

	
	/**
	 * [API] Get notification records.
	 *
	 * GET \notification
	 *
	 * @return A JSON response object with the retrieved notifications.
	 */
	public function get() {

	}


	/**
	 * [API] Get a notification record.
	 *
	 * GET \notification\{ref}
	 *
	 * @param string $not_ref
	 */
	public function get_one($not_ref) {

	}

	/**
	 * [API] Ack notifications.
	 *
	 * PUT /notification/ack  =>  notification[]
	 *
	 * @param array messages The messages to be acked, as follows:
	 *
	 *     messages[i][ref]          = <message reference>
	 *     messages[i][is_delivered] = true
	 *     messages[i][is_seen]      = true|false
	 *
	 * @return A JSON response with two arrays: delivered[] and seen[] containing
	 * the message references of those acked as delivered and seen.
	 * @throws AGValidationException
	 */
	public function ack() {
	
		// Only customers and providers authorized to access this method.
		$this->authorize('provider_or_customer', Auth::user());
	
		$this->is_required([
			'notifications' => 'No notifications to ack received.',
		]);
	
		$notifications = $this->request->input('notifications');
	
		if (!is_array($notifications))
			throw new AGValidationException('Paramater $notifications must be an array');
	
		$delivered = [];
		$seen = [];
	
		foreach ($notifications as $notification) {
			if(!isset($notifications['ref']))
				throw new AGValidationException('Missing notification reference.');
			// These fields cannot be unset by clients, so if set to false ignore them.
			if (Hey::getval($notification,'is_delivered') == true)
				$delivered[] = $notification['ref'];
			if (Hey::getval($notification,'is_seen') == true)
				$seen[] = $notification['ref'];
		}
	
		$recipient_id = Auth::user()->id;
	
		if(count($delivered))
			Notification::where('id_users_recipient', $recipient_id)->
                          whereIn('ref', $delivered)->
                          update(['is_delivered' => true]);
	
		if(count($seen))
			Notification::where('id_users_recipient', $recipient_id)->
                          where('is_delivered', true)->
                          whereIn('ref', $seen)->
                          update(['is_seen' => true]);
	
		$acked = [
			'delivered' => $delivered,
			'seen'      => $seen
		];
	
		return APIResponse::ok()->with($acked)->go();
	}
	
}
