<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Provider;
use App\Models\ProviderIdentity;
use App\Models\Payout;
use App\Models\ProviderSettings;
use App\Models\Service;
use App\Models\Task;
use App\Models\Transaction;
use App\Hey;
use App\Utils\Validate;
use App\K;
use App\APIResponse;
use App\Account;
use App\Exceptions\AGInvalidStateException;
use App\Exceptions\AGValidationException;
use App\Exceptions\AGAuthorizationException;
use App\Exceptions\AGResourceNotFoundException;
use App, URL, View, DB, Auth, Session, Redirect, Hash;



class ProviderController extends UserController
{
    /**
     * Create a new provider controller instance.
     * Always call the parent/base class to init stuffs.
     *
     * @return void
     */
    public function __construct(Request $request) {
    	parent::__construct($request);
    }
    

    /**
     * Customer welcome page
     *
     */
    public function welcome_page() {
    
    	// Only providers authorized to access this method.
    	$this->authorize('provider', Auth::user());
    	 
    	$provider = Auth::user()->as_a('provider');
        
        // Get account alerts, if any.
        $alerts = app('account.manager')->of($provider)->get_alerts();

    	$view_data = [
            'vw_company_name'  => Hey::get_app('company_name'),
            'vw_alerts'        => $alerts,
            'vw_provider'      => $provider,
    	];
    	     	
    	return View::make('dashboard.provider.welcome', $view_data);
    }
    

    /**
     * Provider home page
     *
     */
    public function home_page() {
    
    	// Only providers authorized to access this method.
    	$this->authorize('provider', Auth::user());
    	
    	$provider = Auth::user()->as_a('provider');
    	
    	// Get the list of the provider's open tasks.
    	$tasks = Task::where('id_users_provider', $provider->id)->
    	               where('status','!=',k::TASK_CANCELLED)->
    	               where('status','!=',k::TASK_COMPLETED)->
                       orderBy('schedule_datetime','desc')->
                       paginate(K::MAX_ITEMS_PER_PAGE);

        // Get the list of new tasks that may be of interest to the provider
        $new_tasks = [];
        
        // Get account alerts, if any.
        $alerts = app('account.manager')->of($provider)->get_alerts();
        
    	$view_data = [
            'vw_company_name' => Hey::get_app('company_name'),
            'vw_alerts'       => $alerts,
            'vw_provider'     => $provider,
            'vw_open_tasks'   => $tasks,
            'vw_new_tasks'    => $new_tasks,
            'vw_chat_enabled' => true,
    	];
    	 
    	return View::make('dashboard.provider.home', $view_data);
    }
    
    
    /**
     * Display the Provider Settings page.
     */
    public function settings_page() {
    
    	// Only providers authorized to access this method.
    	$this->authorize('provider', Auth::user());
    	
        $provider = Auth::user()->as_a('provider');
        
        // Eager-load default payment method and identity.
        $provider->default_payout_method();
        $provider->load('identity');
        $provider->load('settings');
        
        $vw_id_docs_required = $provider->identity->get_required_documents();
        
    	// Get all platform services and service categories.
    	$services = app('services')->get();
    	
    	$view_data = [
            'vw_company_name' => Hey::get_app('company_name'),
            'vw_provider' => $provider,
            'vw_services' => $services,
            'vw_id_docs' => Hey::get_app('identity.documents'),
            'vw_id_docs_required' => $vw_id_docs_required,
            'vw_market_pkey' => env('MARKET_API_PKEY'),
    	];
    	
    	return View::make('dashboard.provider.settings', $view_data);
    }
    
    
    /**
     * Display the Provider Closed Tasks page.
     */
    public function closed_tasks_page() {
    
    	// Only providers authorized to access this method.
    	$this->authorize('provider', Auth::user());
    
    	$provider = Auth::user()->as_a('provider');
    
    	// Get the list of the provider's closed tasks.
    	$tasks = Task::where('id_users_provider', $provider->id)->
                       where(function ($query) {
                             $query->where('status','=',K::TASK_CANCELLED)->
                                   orWhere('status','=',K::TASK_COMPLETED);
                       })->
                       orderBy('schedule_datetime','desc')->
                       paginate(K::MAX_ITEMS_PER_PAGE);
    	 
    	$view_data = [
            'vw_company_name'  => Hey::get_app('company_name'),
            //'vw_user_fullname' => $customer->first_name.' '.$customer->last_name,
            'vw_closed_tasks'  => $tasks,
    	];
    
    	return View::make('dashboard.provider.closed_tasks', $view_data);
    }
    
    
    /**
     * Display the Provider Transactions Activity page.
     */
    public function activity_page() {
    
    	// Only providers authorized to access this method.
    	$this->authorize('provider', Auth::user());
    	
    	$provider = Auth::user()->as_a('provider');
    	$account = app('account')->of($provider);
    	$balance = $account->get_balance();
    	
    	$vw_balance = [
            //'total'     => $balance->total,
            'available' => $balance->available,
            'pending'   => $balance->pending
    	];
        
        //$transactions = $account->get_transactions();
        
    	$transactions = Transaction::select('transactions.ref',
                                            'transactions.amount_bal',
                                            'transactions.description',
                                            'transactions.status',
                                            'transactions.settle_status',
                                            'transactions.id_sender',
                                            'transactions.id_recipient',
                                            'transactions.created_at')->
                                      where('id_sender', Auth::user()->id)->
                                      orWhere('id_recipient', Auth::user()->id)->
                                      orderBy('created_at', 'desc')->
                                      paginate(K::MAX_ITEMS_PER_PAGE);
        
    	$view_data = [
            'vw_company_name' => Hey::get_app('company_name'),
            'vw_currency' => Hey::get_app('currency'),
            'vw_transactions' => $transactions,
            'vw_balance' => $vw_balance,
    	];
    	
    	return View::make('dashboard.provider.activity', $view_data);
    }

    
    /**
     *  Provider help page.
     */
    public function help_page() {

    	$view_data = [
            'vw_company_name'     => Hey::get_app('company_name'),
            'vw_service_fee_var'  => array_sum(Hey::get_app('fees.service.var'))*100,
            'vw_service_fee_fix'  => array_sum(Hey::get_app('fees.service.fix')),
            'vw_withdraw_fee_var' => array_sum(Hey::get_app('fees.withdraw.var'))*100,
            'vw_withdraw_fee_fix' => array_sum(Hey::get_app('fees.withdraw.fix')),
            'vw_task_cancel_fee'  => Hey::to_currency(Hey::get_app('fees.task.fix.cancellation')),
            'vw_dispute_period'   => Hey::dt_to_human(Hey::get_app('intervals.task.disputable')),
            'vw_cancel_req_exp'   => Hey::dt_to_human(Hey::get_app('intervals.task.cancel_request_expired')),
            'vw_payout_delay'     => Hey::get_app('payout.delay'),
            'vw_payout_interval'  => Hey::get_app('payout.interval'),
    	];
    	 
    	return View::make('portal.provider.help', $view_data);
    }
    
    
    /**
     *  Provider signup page.
     */
    public function signup_page() {

        // Signed up users should not go further
        if(Auth::check())
           return Redirect::to(  Auth::user()->home() );
        
    	// Get all platform services and service categories.
    	$services = app('services')->get();
    	 
    	$view_data = [
            'vw_company_name' => Hey::get_app('company_name'),
            'vw_services' => $services,
    	];
    	 
    	return View::make('portal.provider.signup', $view_data);
    }
    
    
    /**
     *  [API] Register a new provider with the platform.
     *
     *  @param array POST[provider] Contains the provider data.
     *  @return A JSON response object.
     */
    public function signup() {

    	// Authenticated users have not much to do here.
    	if(Auth::check())
    	   throw new AGInvalidStateException("You're already registered.");
    	
    	// Validate required fields.
        $this->is_required([
            'provider'               => 'No provider data received.',
            'provider.username'      => 'Missing username.',
            'provider.password'      => 'Missing password.',
            'provider.first_name'    => 'The first name is required.',
            'provider.last_name'     => 'The last name is required.',
            'provider.dob'           => 'The date of birth is required.',
            'provider.mobile'        => 'The mobile number is required.',
            'provider.entity_type'   => 'The entity type is required.',
            'provider.services'      => 'No provided services selected.',
        ]);

        $posted = $this->request->input('provider');

        // Trim the request parameters to avoid unwanted effects
        Hey::trim_record($posted);

        // If provider is a business check for business fields, else remove them.
        if (User::is_business_entity( $posted['entity_type']) ) {
            $this->is_required([
                'provider.business_name' => 'The business name is required.',
                'provider.business_number' => 'The business number is required.',
            ]);
        }
        else {
            unset($posted['business_name'],
                  $posted['business_number']);
        }

        // Note: passwords are never mass-filled with posted data so we
        // need to set it explicitly.
        $provider = new Provider ($posted);
        $provider->password = $posted['password'];
        $provider->validate();

        Validate::services($posted['services']);
        
        app('account.manager')->of($provider)
                              ->using(['services' => $posted['services']])
                              ->register();
        
        // Log the new provider in. The provider will be redirected to
        // the welcome page in the frontend.
        Auth::loginUsingId($provider->id);

        $results = ['redirect' => URL::to('provider/welcome')];

        return APIResponse::ok()->with($results)->go();
    }
    
    
    /**
     * [API] Provider identity verification method.
     *
     * This method receives a request with all of the information needed to verify
     * the provider's identity. The data is submitted as multipart/form-data that 
     * includes the payout form fields and the document files.
     *
     * @param array POST 
     * 
     * [
     *     entity    => <provider entity type>
     *     dob       => <provider date of birth>
     *     names     => [
     *           first_name => <provider first name>
     *           last_name  => <provider last name>
     *     ]
     *     business  => [
     *           business_name => <provider business name>
     *           business_number => <provider business number>
     *     ]
     *     payout    => [
     *           method     => <payout account method>
     *           acc_holder => <payout account holder>
     *           acc_id     => <payout account id/number>
     *     ]
     *     billing   => [
     *           address    => <provider address>
     *           city       => <provider city>
     *           state      => <provider state>
     *           zip_code   => <provider post code>
     *           country    => <provider country>
     *     ]
     *     documents => [
     *           0 => [
     *                  type     => <type of document>
     *                  filename => <original file name>
     *                  mime     => <mme type>
     *                  (token    => <document file token>)
     *           ]
     *           1 => [...]
     *           2 => [...]
     *               ...
     *     ]
     * ]
     * 
     * @param array FILES['files'] Holds the uploaded document files.
     *
     * @return A JSON response object.
     */
    public function identity() {
    
    	// Only providers authorized to access this method.
    	$this->authorize('provider', Auth::user());
    	
    	// Get the posted info.
        // NOTE: The names of the posted info must match the expected names of
        //       the info required for identification (see K::IDENTITY_INFO_*).
    	$posted = [
            'entity'    => $this->request->input('entity'),
            'dob'       => $this->request->input('dob'),
            'names'     => $this->request->input('names'),
            'business'  => $this->request->input('business'),
            'bank'      => $this->request->input('payout'),
            'address'   => $this->request->input('billing'),
            'documents' => $this->request->input('documents') ?: [],
            'docs'      => $this->request->allFiles()
    	];
    	 
    	Hey::trim_record($posted);
    	
        $provider = Auth::user()->as_a('Provider');
                
        DB::transaction(function () use($provider, $posted) {
            
            $identity = app('identity.manager')->of($provider->id);
            $identity->get_provider()->set_data('request_ip', $this->request->ip());
            $identity->submit($posted);
        });
        
        $results = [
            'message' => trans('messages.identity-info-received'),
        ];
		
    	return APIResponse::ok()->with($results)->go();
    }
    
   
    /**
     * [API] Updates a provider record.
     *
     * @param array PUT[provider] An array that contains the provider data. All fields
     * are optional but an error will be returned if none is provided.
     * @return A JSON response object with the updated fields, if any.
     */
    public function update() {
        
    	// Only providers authorized to access this method.
    	$this->authorize('provider', Auth::user());
    	
        $this->is_required([
             'provider' => 'No provider data received.',
        ]);

        // Currently, the following info cannot be changed.
        $this->is_not_accepted([
             'provider.username' => 'Usernames cannot be changed.',
             'provider.first_name' => 'Account holder names cannot be changed.',
             'provider.last_name' => 'Account holder names cannot be changed.',
             'provider.dob' => 'Account holder date of birth cannot be changed.',
             'provider.entity_type' => 'The entity type cannot be changed.',
             'provider.business_name' => 'The business name cannot be changed.',
             'provider.business_number' => 'The business number cannot be changed.',
        ]);
        
        $updated = $this->request->input('provider');
        $updated['avatar'] = $this->request->allFiles();

        // Trim the request parameters to avoid unwanted effects
        Hey::trim_record($updated);

        $provider = Auth::user()->as_a('provider');
        $provider->forget('*', ['id','ref','updated_at']);

        $user_info = [/*'first_name','last_name','dob',*/'mobile','about',
                        'address','city','state','zip_code','country',
                      /*'entity_type','business_name','business_number'*/];
		
	// Update profile and billing info.
	if(Hey::has_field($user_info, $updated)) {
           $provider->fill($updated);
           $provider->validate();
	}
	else
	// Update password.
	if(Hey::has_field('password', $updated)) {
	   // Check for the current password
	   $this->is_required([
		'provider.old_password' => 'No current password received.',
	   ]);
           
           if($updated['password'] === $updated['old_password'])
              throw new AGValidationException('The new password is the same as the old one.');

	   // Verify the provided current password.
	   if(!Auth::validate([
	   	'id' => $provider->id,
	   	'password' => $updated['old_password']
	   ])) {
	   	  throw new AGAuthorizationException('The old password is invalid.');
	   }
	   Validate::password($updated['password']);
	   $provider->password = Hash::make($updated['password']);
	}

	// Update services.
	if(Hey::has_field('services', $updated)){
		
           Validate::services($updated['services']);
           
           // TODO If a provider adds new expert services we should verify
           //      its qualifications again. For now we will just issue a notice
           //      to contact our support service. A solution would be to disable
           //      the ability to change the services altogether.
           
           if(isset($updated['services'][Service::EXPERT])) {
              $provided = $provider->services_list();
              $c = explode(',', $updated['services'][Service::EXPERT]);
              $new = 0;
              if(isset($provided[Service::EXPERT])) {
                 $cp = array_keys($provided[Service::EXPERT]['categories']);
                 $new = count(array_diff($c, array_intersect($c, $cp)));
              }
              if(!isset($provided[Service::EXPERT]) ||
                 (isset($provided[Service::EXPERT]) && $new)){
                  throw new AGAuthorizationException
                 ('Please contact us if you want to add new expert services.');
              }
           }
           
           // Map the given services to database records.
	   $services = array_map(function ($categories) {
		return ['categories' => $categories ?: null];
	   }, $updated['services']);
					
	   $provider->services()->detach();
	   $provider->services()->attach($services);
	}
        
        // Update avatar
        if(Hey::has_field('files', $updated['avatar'])) {
           Validate::attachments($updated['avatar']['files'], 'avatar');
           $provider->store_files($updated['avatar']['files'], 
                                  'avatar', 'avatar',false, false);
        }
        
	$provider->save();
		
	// Get the list of updated fields to include in the response
	$new = $provider->attributesToArray();
		
	// Include fields that may be filtered out.
	if(isset($updated['password'])) $new['password'] = '********';
	if(isset($updated['services'])) $new['services'] = $updated['services'];
		
	return APIResponse::ok()->with( $new )->go();
    }
    

   /**
    *  [API] Updates a payout method for the provider.
    *
    *  PUT /provider/payout/{action}
    *
    *  @param array POST[] Contains the following info:
    *      [
    *          payout_method     => <the payout method ('bank', etc.)>
    *          payout_acc_holder => <the payout account holder's full name>
    *          payout_acc_id     => <the payout account id (tokenized )>
    *      ]
    *  @param string $action The operation to be performed on the payout.
    *
    *  @return A JSON response object.
    */
   public function update_payout($action) {
   
        // Only providers authorized to access this method.
        $this->authorize('provider', Auth::user());

        if (!$action)
            throw new AGInvalidStateException('Update action is missing.');

        // Validate required fields.
        $this->is_required([
            'payout_method'     => 'Missing payment method.',
            'payout_acc_holder' => 'Missing payment account holder.',
            'payout_acc_id'     => 'Missing payment account id.',
        ]);
	   	
    	// Get the posted info
    	$posted = [
            'payout' => [
                'method'       => $this->request->input('payout_method'),
                'account_name' => $this->request->input('payout_acc_holder'),
                'account_id'   => $this->request->input('payout_acc_id'),
            ]
    	];
   	
        // Trim the request parameters to avoid unwanted effects
        Hey::trim_record($posted);

        $provider = Auth::user()->as_a('provider');

        $payout = new Payout ($posted['payout']);
        $payout->validate();

        // We should always check that required identity details are present
        // at this point before making requests to the marketplace provider.
        // Required details vary depending on the service provider.
        if(!$provider->has_set(['first_name','last_name','address',
                                'zip_code','city','country']))
            throw new AGInvalidStateException('Identity info are missing.');

        $provider->match_payment_names($payout);

        $account = app('account')->of($provider);
        $action = 'payout.'.$action;
        $params  = ['payout' => $payout];
        $results = [];
        
        DB::transaction(function ()
            use ($provider, $account, $action, $params, &$results)
        {
            $results = $account->update($action, $params);
            
            // Set the default payout method if none is set
            if(empty($provider->settings->default_payout_method))
                $provider->settings->default_payout_method = $payout->method;
            
            $provider->save();
            $provider->settings->save();
        });

        $results = [
            'message' => 'Payout method successfully updated.',
            'method'  => $results['method'],
            'holder'  => $results['account_name'],
            'number'  => $results['account_number'],
            'host'    => $results['account_host']
        ];

        return APIResponse::ok()->with($results)->go();
   }
      
   
   /**
    * [API] Updates a payout method for the provider.
    * 
    * PUT /provider/settings
    * 
    * @return type
    */
   public function update_settings() {
       
        // Only providers authorized to access this method.
        $this->authorize('provider', Auth::user());
        
        $this->is_required([
            'settings' => 'No provider settings received.',
        ]);

        // Currently, the following info cannot be changed.
        $this->is_not_accepted([
            'settings.default_payout_method' => 'Default payout method cannot be changed.',
        ]);
        
        $updated = $this->request->input('settings');

        Hey::trim_record($updated);

        $provider = Auth::user()->as_a('provider');
        // Clear all fields so we only set what's required.
        $provider->settings->forget('*', ['id','updated_at']);
        
        $provider->settings->fill($updated);
        $provider->settings->validate();
	$provider->settings->save();
		
	// Get the list of updated fields to include in the response
	$new = $provider->settings->toArray();
		
	return APIResponse::ok()->with( $new )->go();
   }
   
}
