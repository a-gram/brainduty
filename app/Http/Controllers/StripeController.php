<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\APIResponse;
use App\Events\EventMarketplaceAccountUpdated;
use App\Events\EventMarketplaceBalanceAvailable;
use App\Events\EventMarketplaceWithdrawalCreated;
use App\Events\EventMarketplaceWithdrawalDeposited;
use App\Events\EventMarketplaceWithdrawalFailed;
use App\Hey;
use App\K;
use Event, Log;


/**
 *  This controller receives and handles events from Stripe.
 */
class StripeController extends AGController {
    
    /**
     * Event map for firing the internal events associated with Stripe events.
     * 
     * @var array
     */
    protected $fire;
    
    
    /**
     * Constructor
     * 
     * @param Request $request
     */
    public function __construct(Request $request) {
        parent::__construct($request);
        
        $this->fire = [
            
            'account.updated' => function($event) { 
            
                Event::fire(new EventMarketplaceAccountUpdated(
                    $event['id'],
                    $event['data']['object']['id']
                ));
            },
            
            'balance.available' => function($event) { 
            
                Event::fire(new EventMarketplaceBalanceAvailable(
                    $event['id'],
                    $event['user_id']
                ));
            },
            
            'transfer.created' => function($event) { 
            
                Event::fire(new EventMarketplaceWithdrawalCreated(
                    $event['id'],
                    $event['user_id']
                ));
            },
            
            'transfer.paid' => function($event) { 
            
                Event::fire(new EventMarketplaceWithdrawalDeposited(
                    $event['id'],
                    $event['user_id']
                ));
            },
            
            'transfer.failed' => function($event) { 
            
                Event::fire(new EventMarketplaceWithdrawalFailed(
                    $event['id'],
                    $event['user_id']
                ));
            },

        ];
            
        Log::useFiles('c:\temp\stripe-events.log');
    }
    
    
    /**
     * This method processes the events sent by Stripe to our webhook. It must
     * return a response quickly, so all it does is firing the internal event
     * associated with the Stripe event. The Stripe event is then processed by
     * the event handler asynchronously (by pushing a job in the system queue).
     * 
     * @return APIResponse
     */
    public function on_event() {
        
        $event = $this->request->all();
        
        Log::debug($event);
        
        if(isset($this->fire[ $event['type'] ])) {
            
           $this->fire[$event['type']] ($event);
        }
        else {
           // This Stripe event is not handled
        }
        
        return APIResponse::ok()->go();
    }
    
}
