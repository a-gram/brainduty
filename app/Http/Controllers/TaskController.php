<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\AGController;
use App\Models\AGEntity;
use App\Models\User;
use App\Models\Task;
use App\Models\Bid;
use App\Models\Customer;
use App\Models\Service;
use App\Models\Message;
use App\Models\Attachment;
use App\Models\Notification;
use App\Models\TaskDispute;
use App\Models\UserRating;
use App\Utils\Validate;
use App\Hey;
use App\Archive;
use App\Account;
use App\Exceptions\AGException;
use App\Exceptions\AGValidationException;
use App\Exceptions\AGInvalidStateException;
use App\Exceptions\AGAuthorizationException;
use App\Exceptions\AGResourceNotFoundException;
use App\APIResponse;
use App\K;
use App, URL, View, DB, Auth, Redirect, Exception, Event;

use App\Jobs\TaskManager;

/**
 *  This controller 
 */
class TaskController extends AGController
{
    /**
     * Create a new task controller instance.
     * Always call the parent/base class to init stuffs.
     *
     * @return void
     */
    public function __construct(Request $request) {
        parent::__construct($request);
    }


    /**
     *  Task details page.
     */
    public function task_page($task_ref) {

        // Only customers and providers authorized to access this method.
        $this->authorize('provider_or_customer', Auth::user());

        if(!isset($task_ref))
           throw new AGValidationException('No task reference given.');
        
        $task_ = [];
        
        $this->load_task_info($task_ref, $task_);
        
        foreach ($task_['messages'] as $message) {
            if($message['type'] === K::MESSAGE_TASK_REQUEST) {
               $req_msg = $message; break;
            }
        }
        
        $task_['messages'] = [$req_msg];
        
        $view_data = [
            'vw_company_name'  => Hey::get_app('company_name'),
            'vw_task'          => json_encode($task_),
            'vw_no_chat'       => true,
        ];

        return View::make('dashboard.task', $view_data);
    }


    /**
     *  Task request/scheduling page.
     */
    public function scheduling_page() {

        // Only customers authorized to access this method.
        $this->authorize('customer', Auth::user());

        $customer = Auth::user()->as_a('customer');
        
        // Only active customers can request tasks
        if(!$customer->is_active())
            throw new AGAuthorizationException
            ('Your account must be activated in order to use this feature.');
        
        $customer->default_payment_method();

        // Get all platform services and service categories.
        $services = app('services')->get();
        
        // NOTE: If providers are allowed to modify the platform base rates
        //       then the shown rates must be an estimate. For example the
        //       average for a specific service or a range using the minimum
        //       and maximum provider rates.
        //Service::estimate_rates($services);
        
        $favourited = $customer->favourited()
                                ->select(['id','ref','first_name','last_name'])
                                ->get();

        $view_data = [
            'vw_company_name'  => Hey::get_app('company_name'),
            'vw_customer' => $customer,
            'vw_services' => $services,
            'vw_favourites' => $favourited->toArray(),
            'vw_market_pkey' => env('MARKET_API_PKEY'),
        ];

        return View::make('dashboard.customer.task.request', $view_data);
    }


    /**
     *  Task dispute page.
     */
    public function dispute_page($task_ref) {

        // Only customers authorized to access this method.
        $this->authorize('customer', Auth::user());

        if(!isset($task_ref))
            throw new AGValidationException('No task reference given.');

        $task = Task::where('ref', $task_ref)->first();

        if(!$task)
            throw new AGResourceNotFoundException('Task not found : '.$task_ref);

        $customer = Auth::user()->as_a('customer');

        $dispute_reasons = Hey::get_app('task_dispute_reasons');

        $view_data = [
            'vw_company_name'     => Hey::get_app('company_name'),
            'vw_task'             => $task,
            'vw_customer'         => $customer,
            'vw_dispute_reasons'  => $dispute_reasons,
        ];

        return View::make('dashboard.customer.task.dispute', $view_data);
    }


    /**
     *  Task rating page.
     */
    public function rating_page($task_ref) {

        // Only customers authorized to access this method.
        $this->authorize('customer', Auth::user());

        if(!isset($task_ref))
            throw new AGValidationException('No task reference given.');

        $task = Task::where('ref', $task_ref)->first();

        if(!$task)
            throw new AGResourceNotFoundException('Task not found : '.$task_ref);

        $customer = Auth::user()->as_a('customer');

        $rating_scores = Hey::get_app('task_rating_scores');

        $view_data = [
            'vw_company_name'     => Hey::get_app('company_name'),
            'vw_task'             => $task,
            'vw_customer'         => $customer,
            'vw_rating_scores'    => $rating_scores,
        ];

        return View::make('dashboard.customer.task.rating', $view_data);
    }
    

    /**
     *   [API] Schedule a task and stores it into the database. This function also
     *   executes the customer payment by charging either a supplied card (using tokens)
     *   or a stored payment method.
     *
     *   @return A JSON response object.
     */
    public function schedule() {
        // NOTE: Task scheduling is currently done through transactional
        //       message (see TaskRequestMessage).
    }


    /**
     *  [API] Reschedule a closed task.
     *  
     *  @param unknown $task_ref
     *  @throws AGValidationException
     */
    public function reschedule($task_ref = null) {

        // Only customers authorized to access this method.
        $this->authorize('customer', Auth::user());

        if(!isset($task_ref))
                throw new AGValidationException('No task reference given.');

        $task = Task::with(['messages'=> function($q) {
                           $q->with('attachments')->
                               where('type', K::MESSAGE_TASK_REQUEST);
                      }])->
                      where('ref', $task_ref)->first();

        if(!$task)
            throw new AGResourceNotFoundException('Task not found : '.$task_ref);

        if(!$task->has_actor(Auth::user()->id))
            throw new AGAuthorizationException;

        if($task->status != K::TASK_CANCELLED)
            throw new AGInvalidStateException('Cannot reschedule the task.');

        if($task->is_rescheduled)
            throw new AGInvalidStateException('This task has already been rescheduled.');

        // Tasks are scheduled thru a transactional message so we need
        // to create one.
        $request = [
            'message' => [
                'type' => K::MESSAGE_TASK_REQUEST,
                'body' => $task->messages->first()->body,
                'data' => [
                    'type' => $task->type,
                    'catags' => $task->catags,
                    'subject'  => $task->subject,
                    'assignee' => $task->assignee,
                 ],
                'attachments' => null,
             ],
            'task'   => $task,
            'sender' => Auth::user(),
        ];

        App::make('message')->from($request);

        return APIResponse::ok()->go();
    }


    /**
     * [API] Get task records.
     *
     * GET \task[?page=<number>&with=<relation>&where=<filter>]
     *
     * @param number $page If present, indicates that the results are paginated and
     * only the specified page must be returned, otherwise all tasks are returned.
     * @param array|string $with If present in the query string, this field indicates
     * the related entities that must be included with the returned tasks.
     * @param string $where A filter applied when retrieving the tasks. Note that this is
     * not an SQL clause (although it usually translates to one or more) but just a name
     * that must be mapped to some implementation.
     * 
     * Currently implemented filters:
     * 
     *        1) open   = retrieve all open tasks for the current user
     *        2) closed = retrieve all closed tasks for the current user
     *           
     * @throws AGValidationException
     * @throws AGResourceNotFoundException
     * @return A JSON response object with the retrieved tasks.
     */
    public function get() {

        // Only customers and providers authorized to access this method.
        $this->authorize('provider_or_customer', Auth::user());

        $page      = $this->request->input('page');
        $filter    = $this->request->input('where', 'no.filter');
        $relations = $this->request->get('with');
        $relations = $relations && !is_array($relations) ? [$relations] : $relations;

    	$query = Task::where(function ($query) {
                             $query->where('id_users_provider','=',Auth::user()->id)->
                                   orWhere('id_users_customer','=',Auth::user()->id);
                       })->
                       orderBy('schedule_datetime','desc');
        
        // Retrieve open tasks
        if($filter == 'open') {
           $query->where(function ($query) {
                      $query->where('status','!=',k::TASK_CANCELLED)->
                              where('status','!=',k::TASK_COMPLETED);
            });
        }
        else
        // Retrieve closed tasks
        if($filter == 'closed') {
           $query->where(function ($query) {
                      $query->where('status','=',k::TASK_CANCELLED)->
                            orWhere('status','=',k::TASK_COMPLETED);
            });
        }
        // More filters here
        // ...
        else {
            throw new AGValidationException;
        }
        
        if($relations)
           $query->with($relations);
        
        $results =[];
        
        if($page) {
           $res = $query->paginate(K::MAX_ITEMS_PER_PAGE);
           $results['tasks'] = $res->toArray()['data'];
           $results['page'] = $res->currentPage();
           $results['pages'] = $res->lastPage();
        }
        else {
           $res = $query->get();
           $results['tasks'] = $res->toArray();
        }
        
        return APIResponse::ok()->with($results)->go();
    }


    /**
     * [API] Get a task record.
     * 
     * GET \task\{ref}
     *           
     * @throws AGValidationException
     * @throws AGResourceNotFoundException
     * @return A JSON response object with the retrieved task.
     */
    public function get_one($task_ref) {

        // Only customers and providers authorized to access this method.
        $this->authorize('provider_or_customer', Auth::user());

        if(!isset($task_ref))
           throw new AGValidationException;
        
        $task_ = [];
        
        $this->load_task_info($task_ref, $task_);
        
        return APIResponse::ok()->with($task_)->go();
    }


    /**
     * [API] Create a new bid for the specified task.
     * 
     * POST /task/{ref}/bid
     * 
     * @param unknown $task_ref
     * @throws AGValidationException
     * @throws AGResourceNotFoundException
     * @throws AGInvalidStateException
     * @throws AGException
     */
    public function create_bid($task_ref) {

        // Only providers authorized to access this method.
        $this->authorize('provider', Auth::user());

        $provider = Auth::user()->as_a('provider');

        // Only active or onboarding providers can bid on tasks.
        if(!($provider->is_active() || $provider->is_onboarding()))
           throw new AGAuthorizationException;

        if(!isset($task_ref))
           throw new AGValidationException('No task reference given.');

        $task = Task::with('bids')->where('ref', $task_ref)
                                  ->lockForUpdate()
                                  ->first();

        if(!$task)
           throw new AGResourceNotFoundException('Task not found : '.$task_ref);

        if(!$task->accepts_bids())
           throw new AGInvalidStateException(trans('exceptions.task-bids-closed'));
        
        // ---
        
        // Get the offer notification for this provider and check if it's expired
        $notification = Notification::where('type', K::NOTIFICATION_TASK_EVENT)->
                                      where('event', K::TASK_BID_REQUEST_EVENT)->
                                      where('subject_id', $task->ref)->
                                      where('id_users_recipient', $provider->id)->
                                      first();
        
        // Check that the provider was sent an offer to bid
        if(!$notification) {
            throw new AGAuthorizationException;
        }
	
        // If the provider is the specified assignee for the task then it can
        // bid until the task expires, else check if the offer expired.
        if($task->assignee !== $provider->id) {
            
           $bid_time = (new \DateTime)->sub
                       (new \DateInterval(Hey::get_app('intervals.task.bid_request_expired')));

           if($bid_time > $notification->created_at ) {
               throw new AGInvalidStateException(trans('exceptions.task-bid-expired'));
           }
        }

        // ---
        
        $bid = new Bid;
        $bid->bidder = $provider->id;
        $bid->bid_datetime = Hey::now_to_db_datetime();
        $bid->is_excluded = true;

        // Catch multiple bid attempts here.
        try {
            $task->bids()->save($bid);
        } catch (Exception $exc) {
            if($exc->getCode() === '23000') //<- ANSI SQL Error: duplicate key
               throw new AGInvalidStateException(trans('exceptions.task-bid-already'));
            else
               throw new $exc;
        }

        $results = [
           'message' => trans('messages.task-bid-received', [
                 'position' => $task->bids->count() + 1
           ])
        ];
        return APIResponse::ok()->with($results)->go();
    }


    // Task states can be updated concurrently (usually by the provider and the
    // customer) so access synchronization to the transition methods is required
    // in order to maintain consistency. Alternatively, a queued event-based approach
    // may be used ...
    // http://dev.mysql.com/doc/refman/5.0/en/innodb-transaction-model.html


    /**
     * [API] Cancel the specified task.
     * 
     * POST /task/{task_ref}/cancel
     * 
     * Tasks are cancelled according to the following policy:
     * 
     * Cancelling a task causes a disruption in the engagement of both the customer and
     * the provider, which may have already invested money and time (in payments and work).
     * For this reasons, the rationale of cancelling a task is that of penalizing who
     * causes the disruption. 
     *  
     * If the task is in the scheduled state then the customer can cancel it and
     * it will be closed. No other actions will occur.
     * 
     * If the task is in the ongoing state then both customer and provider can cancel
     * it and the following will occur:
     * 
     * Tasks with pending payments
     * 
     * if the provider cancels the task then all (captured) payments associated with
     * the task will be refunded to the customer;
     * if the customer cancels the task then all (captured) payments associated with
     * the task will be released to the provider.
     * 
     * Tasks without pending payments (current implementation)
     * 
     * if the provider cancels the task then no service will be paid for the task and
     * a ranking penalty may be applied or a cancellation fee may be charged if there
     * are available funds in its balance;
     * if the customer cancels the task then all services used so far for the task or
     * a cancellation fee may be charged.
     * if both customer and provider mutually agree to cancel the task by approving a
     * task cancel request then the task will be closed and no other actions will occur.
     * 
     */
    public function cancel($task_ref = null) {

        $tlm = App::make('task.manager');

        // Only customers and providers authorized to access this method.
        $this->authorize('provider_or_customer', Auth::user());

        if(!isset($task_ref))
           throw new AGValidationException('No task reference given.');

        DB::transaction(function() use ($task_ref, $tlm) {

            $tlm->set_requestor( Auth::user() );
            $tlm->cancel($task_ref);           
        });

        $results = ['message' => 'The task has been cancelled.'];

        return APIResponse::ok()->with($results)->go();
    }


    /**
     * [API] End the specified task
     * 
     * POST /task/{task_ref}/end
     * 
     * Ending a task is the act of declaring it as fulfilled according to the
     * task's description, therefore only providers shall end tasks. Tasks can
     * only be ended if they are in the ongoing state.
     * 
     * Upon ending the following will occur:
     * 
     * if there are chargeable services associated with the task, these will
     * be charged to the customer;
     * if there are 'uncaptured' charges for services associated with the task,
     * these will be captured;
     * once payments have been made, any deliverable posted by the provider, if
     * any, will be released/unlocked;
     * a 'followup job/event' will be created in order to check whether the funds
     * associated with payments for the task can be cleared to the provider and
     * the task closed.
     * 
     */
    public function end($task_ref = null) {
            // NOTE: Tasks are currently ended by the system upon acceptance by the
            //       customer of an "end duty request" sent by the provider.
            //       (see Jobs/TaskEndRequest).
    }	


    /**
     * [API] Dispute the specified task.
     * 
     * POST /task/{task_ref}/dispute  =>  dispute[]
     * 
     * @param array dispute[] An array of data specifing information about the disputed
     * task, as follows:
     * 
     *    dispute[reason]  => Reason for disputing
     *    dispute[message] => Details about the dispute
     * 
     * Tasks can be disputed by customers once they are ended either by the provider
     * or by the system. Disputing a task may incur an 'arbitration fee' to cover the
     * cost of mediating.
     * 
     * Upon disputing the following will occur:
     * 
     * a dispute record will be created and a system representative will examine the case;
     * if the dispute is successfull then the task is cancelled and any charged amounts for
     * the disputed task, if any, refunded to the customer. A ranking penalty may be applied
     * to the provider;
     * if the dispute is unsuccessfull then the task is closed as completed and any charged
     * amounts released to the provider.
     * 
     */
    public function dispute($task_ref = null) {

        // Only customers authorized to access this method.
        $this->authorize('customer', Auth::user());

        if(!isset($task_ref))
           throw new AGValidationException('No task reference given.');

        $this->is_required([
            'dispute'          =>  'No dispute data received.',
            'dispute.reason'   =>  'No dispute reason specified',
            'dispute.details'  =>  'No dispute details specified',
        ]);
        
        // Get the posted data.
        $posted = $this->request->input('dispute');

        Hey::trim_record($posted);

        DB::transaction(function() use ($task_ref, $posted) {
            
            $task = app('task.manager')->of($task_ref)->get_task();
            
            // If there is no dispute record then this is a customer-initiated
            // dispute and a new record is created.
            if(!$task->dispute) {
                
                $task->get_manager()
                     ->set_requestor( Auth::user() )
                     ->dispute();

                $dispute = new TaskDispute($posted);
                $dispute->id_tasks = $task->id;
                $dispute->status = K::TASK_DISPUTE_SUBMITTED;
                $dispute->validate();
                $dispute->save();
            }
            else
            // If there is a dispute record with no set reason then it is a
            // system-initiated dispute and we let the customer complete it.
            if($task->dispute && $task->dispute->reason === 0) {
               $task->dispute->reason = $posted['reason'];
               $task->dispute->details = $posted['details'];
               $task->dispute->validate();
               $task->dispute->save();
            }
            else {
               throw new AGInvalidStateException;
            }
        });

        $results = [
            'message'  => trans('messages.task-dispute-received'),
            'redirect' => URL::to('/customer/home')
        ];

        return APIResponse::ok()->with($results)->go();
    }


    /**
     * Rate the service provider for the given task.
     * 
     * @param string $task_ref
     */
    public function rate($task_ref = null){

        // Only customers authorized to access this method.
        $this->authorize('customer', Auth::user());

        if(!isset($task_ref))
           throw new AGValidationException;

        $task = Task::where('ref', $task_ref)->first();

        if(!$task)
           throw new AGResourceNotFoundException('Task not found : '.$task_ref);

        if(!$task->has_actor(Auth::user()->id))
           throw new AGAuthorizationException;

        if($task->status != K::TASK_COMPLETED)
           throw new AGInvalidStateException('Can\'t rate this task');

        if(($rating = UserRating::where('id_sender', Auth::user()->id)->
                                  where('id_tasks', $task->id)->first()))
           throw new AGInvalidStateException('Rating already given for this task.');

        $this->is_required([
            'rating'              =>  'No rating data received.',
            'rating.score'        =>  'No rating score specified',
            'rating.description'  =>  'No rating description specified',
        ]);

        // Get the posted data.
        $posted = $this->request->input('rating');

        Hey::trim_record($posted);

        $rating = new UserRating ($posted);
        $rating->id_sender = Auth::user()->id;
        $rating->id_recipient = Auth::user()->is_customer() ? $task->id_users_provider : 
                                                              $task->id_users_customer;
        $rating->id_tasks = $task->id;

        $rating->validate();
        $rating->save();
        
        Event::fire(new \App\Events\EventUserRating($rating->id, $rating->id_recipient));

        $results = [
            'message'  => trans('messages.task-feedback-received'),
            'redirect' => URL::to('customer/tasks/closed'),
        ];

        return APIResponse::ok()->with($results)->go();
    }
    
    
    /**
     * [API] Get some information about a party in a task.
     * 
     * GET /task/{ref}/party/{user_ref}
     * 
     * @param type $task_ref
     * @param type $user_ref
     * @return APIResponse with the following info about the user
     * 
     *         [
     *            ref        => <the user reference>,
     *            first_name => <user first name>
     *            last_name  => <user last name>
     *            rating     => <user feedback rating>
     *         ]
     */
    public function get_party($task_ref, $user_ref) {
        
        // Only customers and providers authorized to access this method.
        $this->authorize('provider_or_customer', Auth::user());
        
        Hey::check([ isset($task_ref), isset($user_ref) ]);
        
        $task = Task::where('ref', $task_ref)->first();
        
        Hey::check([ $task, $task->has_actor(Auth::user()->id) ]);
        
        $filter = ['id','ref','first_name','last_name','rating'];
        
        $party = User::where('ref', $user_ref)->select($filter)->first();
        
        Hey::check( $party );
        Hey::check( $task->has_actor($party->id) );
        
        $party_ = $party->toArray();
        
        return APIResponse::ok()->with($party_)->go();
    }




    // HELPERS
    
    
    /**
     * Load a task with all related info and resources.
     * 
     * @param type $task_ref
     * @param type $task_
     * @throws AGResourceNotFoundException
     * @throws AGAuthorizationException
     */
    protected function load_task_info($task_ref, &$task_) {

        $query = Task::where('ref', $task_ref);

        // Get all deliverable messages.
        $filter = ['is_deliverable' => true];
        
        // TODO There may be many messages per task so it's better to paginate them...
        $query->with(['messages' => function($q) use ($filter) {
            $q->with('attachments')->where($filter);
        }]);

        // Get all services.
        $filter = []; // Currently not filtered

        $query->with(['services' => function($q) use ($filter) {
            $q->where($filter);
        }]);

        // Get some provider and customer info.
        // NOTE: Provider/Customer are 1-to-1 relationships, so filtering is on the fields.
        $filter = ['id','ref','first_name','last_name','rating'];

        $query->with(['provider' => function($q) use ($filter) {
            $q->select($filter);
        }]);

        $query->with(['customer' => function($q) use ($filter) {
            $q->select($filter);
        }]);

        $task = $query->first();

        if(!$task)
           throw new AGResourceNotFoundException('Task not found : '.$task_ref);

        // Make sure the user is authorized to act on this task.
        // If the task is not yet assigned then providers can access it for bidding.
        // TODO This should probably be a policy on tasks.
        if(!$task->has_actor(Auth::user()->id) &&
           ($task->is_assigned() ||
            !Auth::user()->is_provider())) {
           throw new AGAuthorizationException(trans('messages.task-unavailable'));
        }
                        
        $task_ = $task->attributesToArray();
        
        $task_['provider'] = $task->provider->attributesToArray();
        $task_['provider']['avatar'] = $task->provider->get_shared_file('avatar','url');
        $task_['customer'] = $task->customer->attributesToArray();
        $task_['customer']['avatar'] = $task->customer->get_shared_file('avatar','url');
        
        $task_['services'] = [];
        $fields = ['id_tasks','id_messages','id_services'];
        foreach ($task->services as $service) {
                 $service_ = $service->pivot->setHidden($fields)->toArray();
                 $service_['ref'] = $service->pivot->id_services;
                 $task_['services'][] = $service_;
        }

        $task_['messages'] = [];
        foreach ($task->messages as $message) {
                 $message_ = $message->toArray();
                 $message_['task'] = $task->ref;
                 $message_['sender'] = $message->get_sender_role($task);
                 $task_['messages'][] = $message_;
        }
        
        // Load the dispute record if the task is disputed.
        if($task->dispute) {
           $task_['dispute'] = $task->dispute->attributesToArray();
           $task_['dispute']['reason'] = $task->dispute->reason_to_string() ?:
                                         (Auth::user()->is_customer() ? 
                                          trans('messages.task-dispute-no-reason',[
                                             'url' => url('task').'/'.$task->ref.'/dispute',
                                          ]) : 'N/A');
        }

    }
    
}
