<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use App\Http\Controllers\AGController;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Task;
use App\Models\Customer;
use App\Models\Service;
use App\Models\Message;
use App\Models\Attachment;
use App\Utils\Validate;
use App\Hey;
use App\Account;
use App\Exceptions\AGException;
use App\Exceptions\AGValidationException;
use App\Exceptions\AGInvalidStateException;
use App\Exceptions\AGAuthorizationException;
use App\Exceptions\AGResourceNotFoundException;
use App\Exceptions\AGTransferException;
use App\Exceptions\AGMarketplaceException;
use App\APIResponse;
use App\K;
use App, URL, View, DB, Auth, Redirect;
use Exception;
use Throwable;

/**
 *  This controller handles transaction-related operations.
 */
class TransactionController extends AGController
{
	/**
	 * Create a new transaction controller instance.
	 * Always call the parent/base class to init stuffs.
	 *
	 * @return void
	 */
	public function __construct(Request $request) {
		parent::__construct($request);
	}

        
    /**
     * [API] Get transaction records.
     *
     * GET \transaction[?page=<number>&with=<relation>&where=<filter>]
     *
     * @param number $page If present, indicates that the results are paginated and
     * only the specified page must be returned, otherwise all transactions are returned.
     * @param array|string $with If present in the query string, this field indicates
     * the related entities that must be included with the returned transactions.
     * @param string $where A filter applied when retrieving the transactions. Note that this is
     * not an SQL clause (although it usually translates to one or more) but just a name
     * that must be mapped to some implementation.
     * 
     * Currently implemented filters:
     * 
     *           
     * @throws AGValidationException
     * @throws AGResourceNotFoundException
     * @return A JSON response object with the retrieved transactions.
     */
    public function get() {

        // Only customers and providers authorized to access this method.
        $this->authorize('provider_or_customer', Auth::user());

        $page      = $this->request->input('page');
        $filter    = $this->request->input('where', 'no.filter');
        $relations = $this->request->get('with');
        $relations = $relations && !is_array($relations) ? [$relations] : $relations;

        //$transactions = $account->get_transactions();
        
    	$query = Transaction::select('transactions.ref',
                                     'transactions.amount_bal',
                                     'transactions.description',
                                     'transactions.status',
                                     'transactions.settle_status',
                                     'transactions.id_sender',
                                     'transactions.id_recipient',
                                     'transactions.created_at')->
                                      where('id_sender', Auth::user()->id)->
                                      orWhere('id_recipient', Auth::user()->id)->
                                      orderBy('created_at', 'desc');
        
        // More filters here
        // ...
        
        if($relations)
           $query->with($relations);
        
        $results =[];
        
        if($page) {
           $res = $query->paginate(K::MAX_ITEMS_PER_PAGE);
           $results['page'] = $res->currentPage();
           $results['pages'] = $res->lastPage();
           $transactions = $res->items();
        }
        else {
           $transactions = $query->get()->all();
        }
        
        $results['transactions'] = array_map(function ($txn){
            return $txn->attributesToArray();
        },
        $transactions);
        
        return APIResponse::ok()->with($results)->go();
    }


    /**
     * [API] Get a transaction.
     *
     * GET \transaction\{txn_ref}
     *
     * @param string $txn_ref The reference of the transaction to be retrieved.
     * @return A JSON response object with the retrieved transaction record.
     */
    public function get_one($txn_ref) {

        // Only customers and providers authorized to access this method.
        $this->authorize('provider_or_customer', Auth::user());

        if(!isset($txn_ref))
            throw new AGValidationException('No transaction reference given.');

        $txn = Transaction::with('task')->where('ref', $txn_ref)->first();

        if(!$txn)
            throw new AGResourceNotFoundException('Transaction not found : '.$txn_ref);

        // Make sure the user is authorized to act on this transaction and the
        // relative task, if any.
        if(!$txn->has_actor(Auth::user()->id) ||
           ($txn->task && !$txn->task->has_actor(Auth::user()->id)))
            throw new AGAuthorizationException;

        $txn_ = $txn->attributesToArray();
        $txn_['task'] = $txn->task ? $txn->task->ref : null;

        return APIResponse::ok()->with($txn_)->go();
    }
	
	
    /**
     * [API] Withdraw funds.
     * 
     * POST \transaction\withdraw => data[]
     * 
     * @param array data[] An array with the transaction data, as follows:
     * 
     * data => [
     *    'password' => 'account password'
     * ]
     * 
     * @return JSON A JSON object containing the updated balance and the
     * transfer trasnaction, as follows:
     * {
     *    'balance' : {
     *         'total'    : <total amount>,
     *         'available': <available amount>
     *    },
     *    'txn' : { <transfer transaction details> }
     * }
     */
    public function withdraw() {

        // NOTE: We are currently using automatic transfers.
        throw new AGAuthorizationException;
        
        // Only providers authorized to access this method.
        $this->authorize('provider', Auth::user());

        $provider = Auth::user()->as_a('provider');
        

        if(!$provider->is_active() && !$provider->is_inactive())
            throw new AGAuthorizationException
            (trans('exceptions.account-no-withdraw'));

        $this->is_required([
            'provider'          => 'No provider data received.',
            'provider.password' => 'No password received.',
        ]);

        $posted = $this->request->input('provider');

        $credentials = [
            'id'       => $provider->id,
            'password' => $posted['password']
        ];

        if(!Auth::validate($credentials)) {
                throw new AGAuthorizationException('Invalid password.');
        }

        $security = [
            'request_id'   => $this->request->ip(),
            'request_auth' => Auth::check()
        ];

        $account = app('account')->of($provider);

        $results = [];

        DB::transaction(function() use ($provider, $account, $security, &$results) {
            
            try {
                $transfer = $account->withdraw()->using($security);
                $txn = $transfer->execute();
                $txn->save();

                $results['balance'] = $account->get_balance()->toArray();
                $results['txn'] = $txn->toArray();
            }
            catch (AGTransferException $exc) {

                // ...
                throw $exc;
            }
            catch (\Exception $exc) {

                throw $exc;
            }
        });

        return APIResponse::ok()->with($results)->go();
    }


}
