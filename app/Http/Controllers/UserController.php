<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\AGController;
use App\Models\User;
use App\Models\Provider;
use App\Models\UserVerification;
use App\Utils\Validate;
use App\Hey;
use App\Exceptions\AGException;
use App\Exceptions\AGAuthenticationException;
use App\Exceptions\AGAuthorizationException;
use App\Exceptions\AGValidationException;
use App\Exceptions\AGResourceNotFoundException;
use App\Exceptions\AGAccountVerificationException;
use App\Exceptions\AGInvalidStateException;
use App\APIResponse;
use App\K;
use Exception, URL, View, Hash, Auth, Session, Redirect;


/**
 *  This controller takes care of users registration. Depending on the
 *  user's personality, it knows the right processes to welcome the user
 *  onboard.  
 */
class UserController extends AGController
{
    /**
     * Create a new user controller instance.
     * Always call the parent/base class to init stuffs.
     *
     * @return void
     */
    public function __construct(Request $request) {
        parent::__construct($request);
        //$this->middleware('auth', ['except' => ['login']);
    }

	
	/**
	 *  Login page.
	 */
    public function login_page() {
    	
        if(Auth::check())
          return Redirect::to( Auth::user()->home() );
        
    	$data = [
            'vw_company_name' => Hey::get_app('company_name'),
    	];
        
    	return View::make('auth.login', $data);
    }
    
    
    /**
     * Password reset page.
     */
    public function reset_password_page() {
        
        $token = $this->request->input('t');
        
        if(!$token) {
            throw new AGValidationException;
        }
        
    	$data = [
            'vw_company_name' => Hey::get_app('company_name'),
            'vw_pr_token' => $token,
    	];
        
    	return View::make('auth.password-reset', $data);        
    }

        /**
     *  [API] Performs users logins.
     *  
     *  @param string $username POST[username] The username to be logged in.
     *  @param string $password POST[password] The user's password.
     *  @return A JSON response object.
     */
    public function login() {
    	
    	$this->is_required([
            'username'  => 'The username is missing.',
            'password'  => 'The password is missing.',
        ]);
    	
    	$posted = [
    	    'username'     => $this->request->input('username'),
    	    'password'     => $this->request->input('password'),
            'cancelled_at' => null
    	];
    	 
    	// If the input doesn't pass the validation checks just throw up here.
    	try {
    	    Validate::username($posted['username']);
    	    Validate::password($posted['password']);
    	} catch (AGValidationException $exc) {
            throw new AGAuthenticationException
            ('Login failed: invalid credentials.');
     	}
    	
        // Is the user already logged in?
        if(Auth::check()) {
           throw new AGInvalidStateException('You are already logged in.');
        }
        
     	// Verify credentials
    	if (Auth::attempt($posted)) {
            
            $user = Auth::user();
    		
            // Admins must authenticate thru the admin controller.
            if($user->is_admin()) {
               Auth::logout();
               throw new AGAuthenticationException;
            }
            
            $redirect_url = Session::pull('url.intended') ?: $user->home();
            
            $results = [
               'hello'  => $user->first_name.' '.$user->last_name,
               'redirect' => $redirect_url,
            ];
    	}
    	else {
            throw new AGAuthenticationException
            ('Login failed: invalid credentials.');
    	}
    	
    	return APIResponse::ok()->with($results)->go();
    }
    
    
    /**
     *  [API] Performs users logouts.
     *
     *  @return A JSON response object (with a nice message).
     */
    public function logout() {
    	
    	if(Auth::check()) {
    	   Auth::logout();
    	   $results = ['message'=>'Bye :)'];
    	} else {
    	   $results = ['message'=>'You are not logged in.'];
    	}
    	return Redirect::to(URL::to('/'));
    }
    
    
    /**
     * Check the validity of the user email verification token.
     * 
     * @param string $token This can be a verification token or 'resend'
     * to indicate the request for a new verification link.
     */
    public function verify_email($token) {
        
        // Only customers and providers authorized to access this method.
        $this->authorize('provider_or_customer', Auth::user());
        
        $user = Auth::user();

        if(empty($token))
           throw new AGValidationException;
        
        if($user->email_verified)
           throw new AGValidationException;
        
        $params = [
            'ev_token' => $token,
            'ip'       => $this->request->ip()
        ];
        
        app('account.manager')->of($user)->using($params)->verify();
        
        // If everything is ok, redirect the user to the home page
        return Redirect::to( $user->home() );
     }
    
    
    /**
     * [API] Reset a user account password.
     * 
     * @return type
     * @throws AGValidationException
     */
    public function reset_password() {

        $username = $this->request->input('username');
        $token    = $this->request->input('token');
        $password = $this->request->input('password');
        
        // If there is a username posted, then it's assumed we are in the
        // password reset request step.
        if($username) {
            
           $user = User::where('username', $username)->first();
           $message = trans('messages.account-password-resetting');
        }
        // If there is a token and a password, then it's assumed we are in the
        // password reset step (the user clicked on the link sent by email).
        else if($token && $password) {
            
           $verification = UserVerification::with('user')->
                                             where('pr_token', $token)->
                                             first();
           if(!$verification)
               throw new AGValidationException;
           
           $user = $verification->user;
           $user->add('verification', $verification);
           $verification->setRelations([]);
           $message = trans('messages.account-password-reset');
        }
        // Any other case should not be a valid request.
        else {
           throw new AGValidationException;
        }
        
        if(!$user)
            throw new AGValidationException;
        
        if($user->cancelled_at)
            throw new AGValidationException;
        
        app('account.manager')->of($user)->reset_password($password, $token);
        
        $results = [ 'message' => $message ];
        
        return APIResponse::ok()->with($results)->go();
    }
    
    
    /**
     * Add a user to the requestor's favourites list.
     * 
     * @param string $user_ref
     * @return APIResponse
     * @throws AGValidationException
     * @throws AGInvalidStateException
     * @throws AGException
     */
    public function favourite($user_ref) {
        
        // Currently, only customers can favourite users (providers).
        $this->authorize('customer', Auth::user());
        
        if(empty($user_ref))
           throw new AGValidationException;
        
        $favourite = User::where('ref', $user_ref)->first();
        
        if(!$favourite)
            throw new AGValidationException;
                
        if(!$favourite->is_provider())
            throw new AGValidationException;
        
        if($favourite->ref == Auth::user()->ref)
            throw new AGValidationException;
        
        if($favourite->is_cancelled())
            throw new AGValidationException('User not available');
        
        try {
            Auth::user()->favourited()->attach($favourite->id);
            $message = trans('messages.user-favourited');
        }
        catch (Exception $exc) {
            if($exc->getCode() === '23000') //<- ANSI SQL Error: duplicate key
               throw new AGInvalidStateException(trans('exceptions.user-already-favourite'));
            else
               throw new $exc;
        }
        
        // If the favourited user is currently in the ignore list, we suppose the
        // requestor no longer whishes to ignore it, so we will remove it from ignore.
        if(Auth::user()->ignored()->where('id', $favourite->id)->exists()) {
           Auth::user()->ignored()->detach($favourite->id);
           $message = trans('messages.user-favourited-was-ignored');
        }

        $results = [ 'message' => $message ];
        
        return APIResponse::ok()->with($results)->go();
    }
    
    
    /**
     * Add a user to the requestor's ignore list.
     * 
     * @param string $user_ref
     * @return APIResponse
     * @throws AGValidationException
     * @throws AGInvalidStateException
     * @throws AGException
     */
    public function ignore($user_ref) {
        
        // Currently, only customers can ignore users (providers).
        $this->authorize('customer', Auth::user());
        
        if(empty($user_ref))
           throw new AGValidationException;
        
        $ignored = User::where('ref', $user_ref)->first();
        
        if(!$ignored)
            throw new AGValidationException;
                
        if(!$ignored->is_provider())
            throw new AGValidationException;
        
        if($ignored->ref == Auth::user()->ref)
            throw new AGValidationException;
        
        if($ignored->is_cancelled())
            throw new AGValidationException('User not available');

        try {
            Auth::user()->ignored()->attach($ignored->id);
            $message = trans('messages.user-ignored');
        }
        catch (Exception $exc) {
            if($exc->getCode() === '23000') //<- ANSI SQL Error: duplicate key
               throw new AGInvalidStateException(trans('exceptions.user-already-ignored'));
            else
               throw new $exc;
        }
        
        // If the ignored user is currently in the favourites list, we suppose the
        // requestor no longer whishes to keep it there.
        if(Auth::user()->favourited()->where('id', $ignored->id)->exists()) {
           Auth::user()->favourited()->detach($ignored->id);
           $message = trans('messages.user-ignored-was-favourited');
        }

        $results = [ 'message' => $message ];
        
        return APIResponse::ok()->with($results)->go();
    }
    
    
    /**
     * Delete a user from the platform.
     * 
     * @return type
     */
    public function delete() {
        
        $this->authorize('provider_or_customer', Auth::user());
        
        app('account.manager')->of( Auth::user() )->unregister();
        
        Auth::logout();
        
        $results = [ 'message' => trans('messages.account-deleted') ];
        
        return APIResponse::ok()->with($results)->go();
    }

}
