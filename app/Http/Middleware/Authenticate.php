<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\AGAuthenticationException;
use App\Hey;
use URL;


class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
    	//$user = Auth::user();
    	// Unauthenticated user events are treated like exceptions and are
    	// dealt with in the exception handler.
        if (Auth::guard($guard)->guest()) {
            // !!! WARNING !!! If any exception is rised here the request chain
            // will be interrupted and a response will not be made available, so
            // MAKE SURE that anywhere these exceptions are being caught a response
            // will be created and returned (using response(), new Response, et al.).
            // https://laracasts.com/discuss/channels/general-discussion/disable-global-verifycsrftoken-and-use-as-middleware-doesnt-work-setcookie-on-null?page=1
            
            if(!$request->ajax())
                $request->session()->put('url.intended', $request->url());
            
            // Admins redirected to the admin login.
            $admin = Hey::get_app('admin_root');
            $ctx = [ 'is_admin_login' => $request->is($admin.'/*') ];
            
            throw (new AGAuthenticationException)->set_context($ctx);
        }
        else {
            // The authenticated user info loaded in the session includes some
            // sensitive info. Remove it here before doing anything stupid with it.
            Auth::user()->forget('password');
            Auth::user()->forget('remember_token');
            Auth::user()->syncOriginal();
        }

        return $next($request);
    }
}
