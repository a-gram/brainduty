<?php

namespace App\Http\Middleware;

use Closure, Request;
use App\Exceptions\AGAppOfflineException;
use Illuminate\Contracts\Foundation\Application;


class Maintenance
{
    /**
     * The application implementation.
     *
     * @var \Illuminate\Contracts\Foundation\Application
     */
    protected $app;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @return void
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function handle($request, Closure $next)
    {
        if($this->app->isDownForMaintenance() && !$this->isIpWhiteListed()) {
           throw new AGAppOfflineException(trans('exceptions.app-offline'));
        }

        return $next($request);
    }
    

    private function isIpWhiteListed()
    {
        $ip = Request::getClientIp();
        $allowed = explode(',', env('APP_IP_WHITELIST'));

        return in_array($ip, $allowed);
    }
}
