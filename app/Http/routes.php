<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::post('/marketplace/event', 'StripeController@on_event');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {

    Route::get('/', 'AppController@home_page');//->name('home');
    Route::get('/learn', function() { return View::make('portal.learn'); });
    Route::get('/faq', function() { return View::make('portal.faq'); });
    Route::get('/work', function() { return View::make('portal.work'); });
    Route::get('/about', function() { return View::make('portal.about'); });
    Route::get('/terms', function() { return View::make('portal.terms'); });
    Route::get('/privacy', function() { return View::make('portal.privacy'); });
    Route::get('/updates/push', 'AppController@get_updates')->middleware(['auth']);
Route::get('/test', 'AppController@test');
	
    Route::get('/user/login', 'UserController@login_page');
    Route::post('/user/login', 'UserController@login');
Route::get('/user/logout', 'UserController@logout');
    Route::post('/user/logout', 'UserController@logout');
    Route::get('/user/email/verify/{token}', 'UserController@verify_email')->middleware(['auth']);
    Route::get('/user/password/reset', 'UserController@reset_password_page');
    Route::post('/user/password/reset', 'UserController@reset_password');
    Route::post('/user/{user_ref}/favourite', 'UserController@favourite');
    Route::post('/user/{user_ref}/ignore', 'UserController@ignore');
    Route::delete('/user/delete', 'UserController@delete');

    Route::get('/provider', 'ProviderController@home_page')->middleware(['auth']);
    Route::get('/provider/welcome', 'ProviderController@welcome_page')->middleware(['auth']);
    Route::get('/provider/home', 'ProviderController@home_page')->middleware(['auth']);
    Route::get('/provider/settings', 'ProviderController@settings_page')->middleware(['auth']);
    Route::get('/provider/tasks/closed', 'ProviderController@closed_tasks_page')->middleware(['auth']);
    Route::get('/provider/activity', 'ProviderController@activity_page')->middleware(['auth']);
    Route::get('/provider/help', 'ProviderController@help_page')->middleware(['auth']);
    Route::get('/provider/signup', 'ProviderController@signup_page');
    Route::post('/provider', 'ProviderController@signup');
    Route::put('/provider', 'ProviderController@update')->middleware(['auth']);
    Route::post('/provider/identity', 'ProviderController@identity')->middleware(['auth']);
    Route::put('/provider/payout/{action}', 'ProviderController@update_payout')->middleware(['auth']);
    Route::put('/provider/settings', 'ProviderController@update_settings')->middleware(['auth']);

    Route::get('/customer/signup', 'CustomerController@signup_page');
    Route::post('/customer', 'CustomerController@signup');
    Route::put('/customer', 'CustomerController@update')->middleware(['auth']);
    Route::get('/customer/welcome', 'CustomerController@welcome_page')->middleware(['auth']);
    Route::get('/customer/home', 'CustomerController@home_page')->middleware(['auth']);
    Route::get('/customer/settings', 'CustomerController@settings_page')->middleware(['auth']);
    Route::get('/customer/tasks/closed', 'CustomerController@closed_tasks_page')->middleware(['auth']);
    Route::post('/customer/payment', 'CustomerController@register_payment')->middleware(['auth']);
    Route::put('/customer/payment/{action}', 'CustomerController@update_payment')->middleware(['auth']);

    Route::get('/task/{ref}/view', 'TaskController@task_page')->middleware(['auth']);
    Route::get('/task/request', 'TaskController@scheduling_page')->middleware(['auth']);
    Route::get('/task/{ref}/dispute', 'TaskController@dispute_page')->middleware(['auth']);
    Route::get('/task/{ref}/rate', 'TaskController@rating_page')->middleware(['auth']);
    Route::get('/task/{ref}/party/{user_ref}', 'TaskController@get_party')->middleware(['auth']);
    //Route::post('/task', 'TaskController@schedule')->middleware(['auth']);
    Route::get('/task', 'TaskController@get')->middleware(['auth']);
    Route::post('/task/{ref}/reschedule', 'TaskController@reschedule')->middleware(['auth']);
    Route::get('/task/{ref}', 'TaskController@get_one')->middleware(['auth']);
    Route::post('/task/{ref}/bid', 'TaskController@create_bid')->middleware(['auth']);
    Route::post('/task/{ref}/cancel', 'TaskController@cancel')->middleware(['auth']);
    //Route::post('/task/{ref}/end', 'TaskController@end')->middleware(['auth']);
    Route::post('/task/{ref}/dispute', 'TaskController@dispute')->middleware(['auth']);
    Route::post('/task/{ref}/rate', 'TaskController@rate')->middleware(['auth']);

    Route::get('/message', 'MessageController@get')->middleware(['auth']);
//    Route::get('/message/{ref}', 'MessageController@get_one')->middleware(['auth']);
    Route::post('/task/{ref}/message', 'MessageController@create')->middleware(['auth']);
    Route::put('/message/ack', 'MessageController@ack')->middleware(['auth']);
    Route::post('/message/{msg_ref}/action/{action}', 'MessageController@action')->middleware(['auth']);
    
    Route::get('/task/{ref}/file/{fid}', 'FileController@get_one')->middleware(['auth']);
    Route::get('/user/{ref}/avatar', 'FileController@get_avatar')->middleware(['auth']);

    Route::get('/transaction/{txn_ref}', 'TransactionController@get_one')->middleware(['auth']);
    Route::get('/transaction', 'TransactionController@get')->middleware(['auth']);
    Route::post('/transaction/withdraw', 'TransactionController@withdraw')->middleware(['auth']);
    
    Route::put('/notification/ack', 'NotificationController@ack')->middleware(['auth']);
    
    // Admins 
    // TODO Should be in a different routing/middleware group.

    Route::get('/'.env('APP_ADMIN_ROOT', 'admin').'/app/deploy', 'AdminController@deploy');
    Route::get('/'.env('APP_ADMIN_ROOT', 'admin').'/app/online', 'AdminController@app_online');
    Route::get('/'.env('APP_ADMIN_ROOT', 'admin').'/app/offline', 'AdminController@app_offline');

    Route::get('/'.env('APP_ADMIN_ROOT', 'admin').'/login', 'AdminController@login_page');
    Route::get('/'.env('APP_ADMIN_ROOT', 'admin').'/home', 'AdminController@home_page')->middleware(['auth']);
    Route::get('/'.env('APP_ADMIN_ROOT', 'admin'), 'AdminController@home_page')->middleware(['auth']);
    Route::get('/'.env('APP_ADMIN_ROOT', 'admin').'/provider/onboarding', 'AdminController@provider_onboarding_page')->middleware(['auth']);
    Route::get('/'.env('APP_ADMIN_ROOT', 'admin').'/disputes', 'AdminController@disputes_page')->middleware(['auth']);
    Route::get('/'.env('APP_ADMIN_ROOT', 'admin').'/user/{user_id}/account', 'AdminController@get_user_account')->middleware(['auth']);
    Route::post('/'.env('APP_ADMIN_ROOT', 'admin').'/provider/{user_id}/identity/{action}', 'AdminController@identity')->middleware(['auth']);
    Route::get('/'.env('APP_ADMIN_ROOT', 'admin').'/provider/{user_id}/identity/doc/{doc_id}', 'AdminController@get_identity_doc')->middleware(['auth']);
    Route::post('/'.env('APP_ADMIN_ROOT', 'admin').'/dispute/{task_ref}/resolve/{user_ref}', 'AdminController@dispute_resolve')->middleware(['auth']);
    Route::post('/'.env('APP_ADMIN_ROOT', 'admin').'/login', 'AdminController@login');
    Route::get('/'.env('APP_ADMIN_ROOT', 'admin').'/logout', 'AdminController@logout');
    Route::post('/'.env('APP_ADMIN_ROOT', 'admin').'/logout', 'AdminController@logout');
    
});

