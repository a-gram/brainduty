<?php

namespace App;

use App\Interfaces\IIdentityLifecycle;
use App\Models\Task;
use App\Models\Payout;
use App\Models\Provider;
use App\Models\ProviderIdentity;
use App\Models\IdentityDocument;
use App\Models\Service;
use App\Models\Attachment;
use App\Events\EventTaskCancelled;
use App\Events\EventTaskEnded;
use App\Events\EventTaskScheduled;
use App\Events\EventUserIdentityVerification;
use App\Notifications\IdentityInquiryNotification;
use App\Notifications\IdentityUnverifiedNotification;
use App\Notifications\IdentityVerifiedNotification;
use App\Exceptions\AGException;
use App\Exceptions\AGAuthorizationException;
use App\Exceptions\AGInvalidStateException;
use App\Exceptions\AGValidationException;
use App\Hey;
use App\Utils\Validate;
use App\K;
use App,Event,DB,Exception;


/**
 * An implementation of the IIdentityLifecycle interface.
 * 
 * @author Albert
 *
 */
class IdentityManager implements IIdentityLifecycle {
    
    /**
     * The instance of the provider being verified.
     * 
     * IMPORTANT! THE PROVIDER->IDENTITY RECORD MUST BE ALREADY LOCKED WITHIN A
     *            DATABASE TRANSACTION BEFORE USING THIS CLASS METHODS.
     * 
     * @var Provider
     */
    protected $provider;
    
    /**
     * The event to be fired when changing state.
     * 
     * @var Event (optional)
     */
    protected $event = null;

    /**
     * The notification to be pushed when changing state.
     * 
     * @var Notification (optional)
     */
    protected $notification = null;

    /**
     * The notifier instance
     * 
     * @var Notifier
     */
    protected $notifier;


    public function __construct($provider = null) {
        $this->provider = $provider;
        $this->notifier = app('notifier');
    }

    // SETTERS

    public function set_provider($provider) {
        $this->provider = $provider;
        return $this;
    }
    
    public function set_event($event) {
        $this->event = $event;
        return $this;
    }

    public function set_notification($notification) {
        $this->notification = $notification;
        return $this;
    }
    
    // GETTERS
    
    public function get_provider() {
        return $this->provider;
    }
    
    public function get_event() {
        return $this->event;
    }
    
    public function get_notification() {
        return $this->notification;
    }

    
    /**
     * Get a managed identity verification.
     * 
     * @param Provider|int $provider
     */
    public function of($provider) {
        
        $this->provider = $provider instanceof Provider ? $provider : 
                          $this->get_provider_identity($provider);
        return $this;
    }
    

    // INTERFACE
    
    
    /**
     * Submit identity info.
     * 
     * @param array $posted The identity information posted by the user.
     * NOTE: The names of the posted info (the array keys) must match the expected
     * names of the info required for identification (see K::IDENTITY_INFO_*).
     * @param type $provider_id
     * @return Provider
     * @throws AGInvalidStateException
     * @throws AGValidationException
     * @throws Exception
     */
    public function submit(array $posted, $provider_id = null) {
        
        $provider = $this->get_provider_identity($provider_id);
        
        // A verified email address is required
        if (!$provider->email_verified)
            throw new AGInvalidStateException(trans('exceptions.account-email-unverified'));

        // Stop here if provider is already verified and active.
        if ($provider->is_verified())
            throw new AGInvalidStateException('This account is already active.');

        // Stop here if provider is being verified and not asked for further identity info.
        if ($provider->is_verifying())
            throw new AGInvalidStateException('This account is under verification.');
        
        // If provider has failed identity verification we are blocking any further
        // submission. Alternatively, we can allow to repeat the verification process.
        if ($provider->is_unverified())
            throw new AGInvalidStateException('This account is unverified.');
	    
        
        Validate::activation_info($posted);
        
        $payout = null;
        $mp_account = null; 
        $verification_started = !$provider->has_verification();
        
        // Remove null fields and empty arrays.
        $info = array_filter($posted, function($field){ 
            return !(is_null($field) || (is_array($field) && empty($field)));
        });
        
        // Get the expected given info.
        $given = array_merge(
            array_diff(array_keys($info), ['documents','docs']),
            isset($info['documents']) ? array_column($info['documents'], 'type') : []
        );

        // Check that all required info have been provided.
        if (!$provider->identity->has_required_info($given))
            throw new AGValidationException('Please provide all the required information.');
        
        $account = app('account')->of($provider);
        $archive = app('archive')->of($provider);
        
        // If provider has just started the verification process, mark it as onboarding
        // and fire the relevant event to create the verifier job.
        if ($verification_started) {
            $provider->status = K::USER_ONBOARDING;
            //Event::fire(new EventUserIdentityVerification($provider->id));
        }

        $provider->identity->submission_datetime = Hey::now_to_db_datetime();
        $provider->identity->notes = null;

        // Change verification status
        if (!$provider->identity->status ||
             $provider->identity->status == K::PROVIDER_IDENTITY_INQUIRED)
             $provider->identity->status = K::PROVIDER_IDENTITY_SUBMITTED;
        else
            Hey::bug_here ($provider->identity->status);
        
        
        if(isset($posted['entity'])) {
           $provider->entity_type = $posted['entity'];
        }
        
        if(isset($posted['dob'])) {
           $provider->dob = $posted['dob'];
        }

        if(!empty($posted['names'])) {
           $provider->fill($posted['names']);
        }

        if(!empty($posted['business'])) {
           $provider->fill($posted['business']);
        }

        if(!empty($posted['address'])) {
           $provider->fill($posted['address']);
        }
        
        if(!empty($posted['bank'])) {
           $payout = $provider->payout->first() ?: new Payout;
           $payout->fill($posted['bank']);
           $provider->match_payment_names($payout);
           $provider->settings()->update([
               'default_payout_method' => $payout->method
           ]);
        }

        try {
            // Open the payment account or update it.
            if(!$provider->ref_ext) {
                $mp_account = $account->open($payout);
            }
            else {
                // Do not update if the only given info is the ID document as
                // it needs to be attached/stored differently (see below).
                // TODO Maybe we can include documents storing in account->update()
                if(!(count($given)===1 && $given[0]===K::IDENTITY_INFO_PHOTOID)) {
                    $mp_account = $account->update('*.*', [
                        'fields' => $given,
                        'payout' => $payout,
                    ]);
                }
            }
            
            $provider->identity->ext_verified = $mp_account['is_verified'];

            // Get, process and store posted documents, if any are required.
            $posted_documents = $this->get_provider_documents($provider, $posted);
            $documents = $account->store_documents($posted_documents);
            
            // At this point all the required info are assumed provided, so clear them.
            $provider->identity->set_required_info([]);
            
            // If the marketplace account was opened or updated, we may get a list 
            // of further required info in the response, so add them here.
            $provider->identity->add_required_info(
                Hey::getval($mp_account, 'info_required') ?: []
            );
            
            // Add required info for expert providers on first submission.
            if($verification_started && $provider->provides(Service::EXPERT)) {
               $provider->identity->add_required_info([K::IDENTITY_INFO_QUALIFICATIONS]);
            }
            
            // Check whether there are reported issues with the provider's account.
            $this->check_account($provider, $mp_account);
            
            // Save verification info
            $provider->identity->save();
            $provider->identity->documents()->saveMany($documents);
            $provider->save();
            
            Event::fire(new EventUserIdentityVerification($provider->id));
        }
        // If the payout account was opened and an exception occurs afterwards
        // it will not be linked to the provider and will remain dangling on the
        // marketplace provider since it is not rolled back, so we catch errors
        // here and close/delete the dangling account.
        catch(Exception $exc) {
            
           if(isset($mp_account)) {
              $account->close();
           }
           throw $exc;
        }
        
        if($this->event)
           Event::fire($this->event);

        if($this->notification) {
           $this->notifier->push($this->notification);
        }
        
        return $provider;
    }
    
    
    /**
     * Verify identity info.
     * 
     * @param type $provider_id
     * @return Provider
     * @throws AGInvalidStateException
     */
    public function verify($provider_id = null) {
        
        $provider = $this->get_provider_identity($provider_id);
        
        if ($provider->identity->status == K::PROVIDER_IDENTITY_SUBMITTED) {
            $provider->identity->status = K::PROVIDER_IDENTITY_VERIFYING;
        }
        else {
            throw new AGInvalidStateException('Cannot verify provider.');
        }
        
        $provider->identity->save();

        if($this->event)
           Event::fire($this->event);

        if($this->notification) {
           $this->notifier->push($this->notification);
        }
        
        return $provider;
    }
    
    
    /**
     * Inquiry user for further info.
     * 
     * @param type $provider_id
     * @param type $reason
     * @return Provider
     * @throws AGValidationException
     * @throws AGInvalidStateException
     */
    public function inquiry($reason = null, $provider_id = null) {
        
        $provider = $this->get_provider_identity($provider_id);
        
        if ($provider->identity->status == K::PROVIDER_IDENTITY_VERIFYING) {
            $provider->identity->status = K::PROVIDER_IDENTITY_INQUIRED;
            if(empty($reason))
               throw new AGValidationException('A reason for inquirying must be given.');
            $provider->identity->add_notes( $reason );
        } else {
            throw new AGInvalidStateException
            ('The provider account is not under verification.');
        }
        
        $provider->identity->save();

        if($this->event)
           Event::fire($this->event);

        if($this->notification) {
           $this->notifier->push($this->notification);
        } else {
           $notification = new IdentityInquiryNotification;
           $notification->to($provider->id);
           $this->notifier->push($notification);
        }
        
        return $provider;
    }
    
    
    /**
     * Identity verified.
     * 
     * @param type $provider_id
     * @return Provider
     * @throws AGInvalidStateException
     */    
    public function verified($provider_id = null) {
        
        $provider = $this->get_provider_identity($provider_id);
        
        if($provider->identity->status != K::PROVIDER_IDENTITY_VERIFYING) {
           throw new AGInvalidStateException;
        }
        
        // Provider is verified at this point so we can set its new status.
        // To activate it, however, this method must complete successfully.
        $provider->identity->status = K::PROVIDER_IDENTITY_VERIFIED;
        $provider->identity->results_datetime = Hey::now_to_db_datetime();
        $provider->identity->notes = null;

        // Provider is now active.
        $provider->status = K::USER_ACTIVE;
        
        $provider->save();
        $provider->identity->save();
        
        if($this->event)
           Event::fire($this->event);

        if($this->notification) {
           $this->notifier->push($this->notification);
        } else {
           $notification = new IdentityVerifiedNotification;
           $notification->to($provider->id);
           $this->notifier->push($notification);
        }
        
        return $provider;
    }
    
    
    /**
     * Identity unverified.
     * 
     * @param type $provider_id
     * @param type $reason
     * @return Provider
     * @throws AGInvalidStateException
     */
    public function unverified($provider_id = null, $reason = null) {
        
        $provider = $this->get_provider_identity($provider_id);
        
        if ($provider->identity->status != K::PROVIDER_IDENTITY_VERIFYING)
            throw new AGInvalidStateException
            ('The provider account is not under verification.');

        $provider->identity->status = K::PROVIDER_IDENTITY_UNVERIFIED;
        $provider->identity->results_datetime = Hey::now_to_db_datetime();
        $provider->identity->notes = $reason;

        // Provider is susoended for now.
        $provider->status = K::USER_SUSPENDED;

        $provider->save();
        $provider->identity->save();

        if($this->event)
           Event::fire($this->event);

        if($this->notification) {
           $this->notifier->push($this->notification);
        } else {
           $notification = new IdentityUnverifiedNotification;
           $notification->to($provider->id);
           $this->notifier->push($notification);
        }
        
        return $provider;
    }
    
    
    /**
     * Require the identity info needed for this provider, according to business
     * rules.
     * 
     * @param type $provider_id
     */
    public function require_info($provider_id = null) {
        
        $provider = $this->get_provider_identity($provider_id);
        
        // Info common to all providers
        $required_info = [
            //K::IDENTITY_INFO_ENTITY,        // provided at registration
            //K::IDENTITY_INFO_NAMES,         // provided at registration
            //K::IDENTITY_INFO_DOB,           // provided at registration
            //K::IDENTITY_INFO_BANK,
            //K::IDENTITY_INFO_ADDRESS,
            //K::IDENTITY_INFO_PHOTOID        // may be required later
        ];
        
        // Info required for 'expert' providers
        if($provider->provides(Service::EXPERT)) {
//            $required_info[] = K::IDENTITY_INFO_QUALIFICATIONS;
        }
        
        $provider->identity->set_required_info($required_info);
    }
    

    // HELPERS
    
    
    /**
     * Get the provider identity records.
     * 
     * @param int $provider_id
     * @return Provider
     * @throws AGResourceNotFoundException
     */
    protected function get_provider_identity($provider_id) {
        
        $provider = isset($provider_id) ?
                    
                    Provider::with(['identity' => function ($q){
                        $q->lockForUpdate();
                    }])->
                    where('id', $provider_id)->first() :
                    
                    $this->provider;

        if(!$provider)
            throw new AGException('Provider not found', [
                $provider, $provider_id
            ]);
        
        return $provider;
    }
    
    
    /**
     * Process the documents data and files posted by the provider, if any, and 
     * returns a set of document records compatible with the Document entity for
     * storage.
     * 
     * @param Provider $provider
     * @param array $posted The posted data including the document fields.
     * @return array An array of document records compatible with the Document
     * entity. These records may include extra fields used in successive steps
     * but they must include at least a subset of relevant fields of Document
     * so that they can be stored.
     */
    protected function get_provider_documents($provider, &$posted) {
        
        if (!$provider->identity->requires_documents()) {
            return [];
        }
        
        $documents = array_map(function($posted_doc) use ($posted) {

            $posted_doc['category'] = K::DOCUMENT_CATEGORY_IDENTITY;

            // Documents verified by the platform are associated with their
            // respective uploaded files, and documents verified by external 
            // services are associated with the respective tokens.
            if(ProviderIdentity::is_platform_verified ($posted_doc['type'])) {
               $posted_doc['verification'] = K::IDENTITY_DOC_VERIFIER_PLATFORM;
               $posted_doc['is_hosted'] = true;
               foreach($posted['docs']['files'] as $file) {
                   if($file->getClientOriginalName() == $posted_doc['filename']) {
                      $posted_doc['file'] = $file;
                      return $posted_doc;
                   }
               }
               // If this point is reached, then there is no associated file
               // with the posted document and it's a bug.
               Hey::bug_here();
            } else
            if(ProviderIdentity::is_external_verified ($posted_doc['type'])) {
               if(!isset($posted_doc['token']))
                  throw new AGException;
               $posted_doc['verification'] = K::IDENTITY_DOC_VERIFIER_EXTERNAL;
               $posted_doc['is_hosted'] = false;
               $posted_doc['ref_ext'] = $posted_doc['token'];
               return $posted_doc;
            }
            else {
               Hey::bug_here();
            }
        },
        $posted['documents']);
        
        return $documents;
    }
    
    
    /**
     * Check the payment account for any issues, such as restrictions (e.g. charges 
     * and/or transfers disabled) and notify the provider.
     * 
     * @param Provider $provider
     * @param array $account
     * @return type
     */
    protected function check_account($provider, $account) {
        
        if(!$account) return;
        
        $issues = [];
        
        if(!$account['charges_enabled']) {
            $issue = new \App\Notifications\AlertNotification;
            $issue->message = trans('messages.account-charges-disabled');
            $issues[] = $issue;
            $provider->status = K::USER_SUSPENDED;
            //$provider->save();
        }
        
        if(!$account['transfers_enabled']) {
            $issue = new \App\Notifications\AlertNotification;
            $issue->message = trans('messages.account-transfers-disabled');
            $issues[] = $issue;
        }
        
        if($issues) {
           $this->notifier->push($issues, $provider->id);
        }
    }
    
}
