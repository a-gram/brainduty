<?php

namespace App\Interfaces;


/**
 * Account managers deal with all the beaurocracy concerning the management
 * of the platform's user accounts.
 * 
 * @author Albert
 *
 */
interface IAccountManager {
    
    /**
     * Register a user with the platform.
     */
    public function register();
     
    /**
     * Verify a user account. This method performs several checks on the
     * user account depending on current user status and throws if there
     * is something wrong. It also verifies email addresses by calling
     * verif_email() if necessary.
     * 
     * @throws AGAccountException
     */
    public function verify();

    /**
     * Verify a user email.
     * 
     * @param string $token The email verification token.
     * @return boolean True if the user email is verified, false otherwise.
     * On errors (expired verification emails, mismatching tokens, invalid links, etc.)
     * throws.
     * @throws AGAccountException
     */
    public function verify_email($token = null);
    
    /**
     * Reset the user account password.
     * 
     * @param type $password The new password
     * @param type $token The reset password verification token.
     * @throws AGAccountException
     */
    public function reset_password($password = null, $token = null);

    /**
     * Get user account-related alerts.
     * 
     * @return array An array of Alert objects.
     */
    public function get_alerts();
    
    /**
     * Unregister a user from the platform.
     */
    public function unregister();
    
}
