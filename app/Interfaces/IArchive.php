<?php

namespace App\Interfaces;


interface IArchive {
    
    /**
     * Store files in this entity's archive.
     * 
     * @param array $files The file(s) to be stored. These must be instances of
     * Symfony's File class.
     * @param string $context The context in which to store the files. A context is
     * generally a directive indicating how to store the files, the simplest action
     * being storing all the files in a subdirectory represented by the context name,
     * but it may be something more elaborate.
     * @return array An array with the stored files.
     */
    public function store($files, $context = '');
    
    /**
     * Remove files from this entity's archive, or the whole archive altogether.
     *
     * @param string|array $files The file(s) to be removed. This may be an array
     * of strings (paths) or of Archive file info records, or a path string. If it
     * is null, the whole archive is removed.
     */
    public function remove($files = null);
    
    /**
     * Remove contexts from this entity's archive.
     *
     * @param string|array $contexts The context(s) names to be removed.
     * @return array The removed context(s).
     */
    public function remove_contexts($contexts);

    /**
     * Empty contexts of this entity's archive.
     *
     * @param string|array $contexts The context(s) names to be cleared.
     * @return array The removed context(s).
     */
    public function clear_contexts($contexts);
    
    /**
     * Return the path to the specified context.
     *
     * @param string $context The context name for which to return the path. If none is
     * specified, then by default the entity's root context is returned.
     * @return string The path to the context, relative to the app's files directory.
     */
    public function get_context($context = '');
    
    /**
     * Returns a list of all files in a context.
     *
     * @param string $context The context name for which to return the list. If none is
     * specified, then by default a list of files in the entity archive is returned.
     * @param bool $with_sub Flag indicating whether to include files from all sub
     * context of the specified context.
     * @return array A list of Archive file info records for all files in the specified
     * context.
     */
    public function lista($context = '', $with_sub = true);
    
    /**
     * Get the contents of a file.
     *
     * @param string $file The file name for which to get the contents.
     * @param string $context (Optional) The context the file is in.
     * @param bool $decrypt Flag indicating whether the contents should be decrypted.
     * Note that this method does not check whether the file is actually encrypted,
     * so it's up to the caller to make sure it is if this flag is set to true.
     * @return string The file contents.
     */
    public function get($file, $context = '', $decrypt = false);
    
    /**
     * Get info about a file.
     *
     * @param string $file The file name for which to get the info.
     * @param string $context (Optional) The context the file is in.
     * @return array An Archive file info record.
     */
    public function get_file_info($file, $context = '');
    
    /**
     * Clone this entity's archive into a new archive.
     *
     * @param string $new Name of the new archive.
     * @param mixed $files If provided, only the files specified in this parameter
     * will be cloned. This can be an array of path strings, an array of Archive 
     * file info records or a path string. 
     * @return bool
     */
    public function clone_to($new, $files = null);

}
