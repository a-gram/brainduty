<?php

namespace App\Interfaces;


/**
 * This interface defines the lifecycle of the provider identity verification.
 * 
 * @author Albert
 *
 */
interface IIdentityLifecycle {
    
    /**
     * Submit identity verification info.
     * 
     * @param array $posted The submitted identity verification info.
     * @param type $provider_id The provider id.
     * If not supplied, then the provider instance in the $provider property will
     * be used.
     * @return Provider
     */
    public function submit(array $posted, $provider_id = null);
    
    
    /**
     * Verify the submitted identity information.
     *  
     * IMPORTANT! This method must be called within a database transaction.
     *  
     * @param 
     * @return 
     * 
     */
    public function verify($provider_id = null);
    

    /**
     * Inquiry the user for additional identity info.
     *  
     * IMPORTANT! This method must be called within a database transaction.
     *  
     * @param 
     * @return 
     * 
     */
    public function inquiry($reason = null, $provider_id = null);
    

    /**
     * Verify the user identity.
     *  
     * IMPORTANT! This method must be called within a database transaction.
     *  
     * @param 
     * @return 
     * 
     */
    public function verified($provider_id = null);
    

    /**
     * Reject the user identity.
     * 
     * @param 
     * @return 
     * 
     */
    public function unverified($provider_id = null, $reason = null);
    
}
