<?php

namespace App\Interfaces;

/**
 * 
 * Interface with the minimum set of functionality that a marketplace
 * service provider must provide.
 *
 */
interface IMarketplaceProvider {

	/**
	 * Create a merchant account on the marketplace.
	 * 
	 * @param array $merchant Array holding merchant details and other
	 * info needed to create the account.
	 */
	public function create_merchant_account(array $merchant);
	
	/**
	 * Update a merchant account on the marketplace.
	 *  
	 * @param array $merchant Array holding merchant details and other
	 * info needed to update the account.
	 * @param unknown $action The type of update.
	 */
	public function update_merchant_account(array $merchant, $action);
        
        
        /**
         * Retrieve the account balance for the specified merchant.
         * 
         * @param array $merchant Array holding merchant info for which to
         * retrieve the balance.
         * @return array
         */
        public function retrieve_merchant_balance(array $merchant);
        
        
        /**
         * Retrieve a list of transactions from the merchant account.
         * 
         * @param array $merchant
         * @param array $params
         * @return array
         */
        public function retrieve_merchant_transactions(array $merchant,
                                                       array $params = []);
        
        
        /**
         * Retrieve a single transaction from the merchant account.
         * 
         * @param array $merchant
         * @param string $txn_id
         */
        public function retrieve_merchant_transaction(array $merchant, $txn_id);
        
        
        /**
         * Store a document in the merchant account.
         * 
         * @param array $document
         */
        public function store_merchant_document(array $document);

        
        /**
	 * Delete a merchant account from the marketplace.
	 * 
	 * @param array $merchant Array holding merchant details and other
	 * info needed to delete the account.
	 */
	public function delete_merchant_account(array $merchant);
	
	/**
	 * Create a customer account on the marketplace.
	 * 
	 * @param array $customer Array holding customer details and other
	 * info needed to delete the account.
	 */
	public function create_customer_account(array $customer);
	
	/**
	 * Update a customer account on the marketplace.
	 * 
	 * @param array $customer Array holding customer details and other
	 * info needed to delete the account.
	 * @param unknown $action The type of update.
	 */
	public function update_customer_account(array $customer, $action);
        
        /**
         * Retrieve the account balance for the specified customer.
         * 
         * @param array $customer Array holding customer info for which to
         * retrieve the balance.
         * @return array
         */
        public function retrieve_customer_balance(array $customer);

	
	/**
	 * Delete a customer account from the marketplace.
	 *
	 * @param array $customer Array holding customer details and other
	 * info needed to delete the account.
	 */
	public function delete_customer_account(array $customer);
	
	/**
	 * Transfer an amount of money from/to an account.
	 * 
	 * @param array $funds
	 */
	public function transfer(array $funds);
	
	/**
	 * Execute a charge on a customer payment method.
	 * 
	 * @param array $charge An array with the charge parameters.
	 */
	public function bill(array $charge);
	
	/**
	 * Reverese a charge on a customer payment method (refund).
         * 
	 * @param array $charge An array with the refund parameters.
	 */
	public function refund(array $charge);
        
        /**
         * Retrieve the marketplace platform balance.
         * 
         * @return array
         */
        public function retrieve_market_balance();

        /**
         * Determine whether a failed charge can be reattempted.
         * 
         * @param array $reason Details for the charge failure.
         */
        public function should_retry_charge(array $reason);

}

