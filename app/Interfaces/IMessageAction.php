<?php

namespace App\Interfaces;

use App\Models\User;


/**
 * A message action is executed as part of a transactional message, usually
 * as a result of a user-triggered command.
 * 
 * @author Albert
 *
 */
interface IMessageAction {
	
	/**
	 * Execute all the required business logic in order to perform the
	 * transaction(s) represented by the message.
	 * 
	 * @param string $action The action to be execute for the specified
	 * transactional message.
	 * @param array $params Optional additional parameters passed to
	 * the action.
	 */
	 public function execute($action, array $params = null);
	
	 /**
	  * Message actions may require verifications on the requesting user in
	  * order to proceed with the execution. This method takes a User instance
	  * (supposedly the authenticated user who made the request) and may
	  * perform the needed verifications on it or store the instance for later
	  * use).
	  * 
	  * @param User $user The user who made the request.
	  * @return IMessageAction For chainability, $this instance should be returned.
	  * @throws AGAuthorizationException.
	  */
	 public function by(User $user);
	 
}
