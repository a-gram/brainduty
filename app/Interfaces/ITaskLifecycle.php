<?php

namespace App\Interfaces;


/**
 * This interface defines the lifecycle of a task by taking the appropriate
 * actions based on the task's current status. It is basically the
 * implementation of the state machine modeling a task's lifecycle.
 * 
 * @author Albert
 *
 */
interface ITaskLifecycle {
    
    /**
     * Schedule a task from a client request.
     * 
     * @param array $request An array with the request data needed to schedule
     * the task having the following format:
     * [
     *    task => [
     *        type =>
     *        catags =>
     *        subject =>
     *        attachs =>
     *    ],
     *    sender => <the customer sending the request>,
     *    rescheduled => <the reschedule task, if applicable>,
     *    message => <the transactional message for the task request>
     * ]
     * 
     * @return Task
     */
    public function schedule(&$request);
    
    
    /**
     * Start a task by assigning it to a service provider.
     * 
     * @param unknown $task_ref The reference of the task to be started.
     * @return Task
     */
    public function start($task_ref = null);


    /**
     * End the specified task and take/schedule any consequent actions.
     * Note that any action associated with the ending may be taken
     * either synchronously in this method or scheduled as an event/job.
     * This method is called by providers.
     *  
     * IMPORTANT! This method must be called within a database transaction.
     *  
     * @param unknown $task_ref The reference of the task to be ended.
     * @return Task
     * 
     */
    public function end($task_ref = null);
    

    /**
     * Cancel the specified task and take/schedule any consequent actions.
     * Note that any action associated with the cancellation may be taken
     * either synchronously in this method or scheduled as an event/job.
     * This method is called by providers, customers and the system user.
     *  
     * IMPORTANT! This method must be called within a database transaction.
     *  
     * @param unknown $task_ref The reference of the task to be cancelled.
     * @return Task
     * 
     */
    public function cancel($task_ref = null);
    

    /**
     * Dispute the specified task.
     * This method is called by customers.
     *  
     * IMPORTANT! This method must be called within a database transaction.
     *  
     * @param unknown $task_ref The reference of the task to be disputed.
     * @return Task
     * 
     */
    public function dispute($task_ref = null);
    

    /**
     * This method checks whether the given task can be set as completed (closed)
     * by verifying that the dispute period has elapsed and completing the payment,
     * if necessary (e.g. capturing uncaptured funds).
     * 
     * @param unknown $task_ref The reference of the task to be closed.
     * @return Task|null
     * 
     */
    public function close($task_ref = null);
    
}
