<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Task;
use App\Models\User;
use App\Models\Bid;
use App\Models\CustomerSettings;
use App\Models\ProviderSettings;
use App\Models\Notification;
use App\Models\Service;
use App\Exceptions\AGException;
use App\Notifications\TaskStartedNotification;
use App\Notifications\TaskCancelledNotification;
use App\Notifications\TaskBidRequestNotification;
use App\Notifications\TaskBidRequestExpiredNotification;
use App\Hey;
use App\K;
use App,DB,Mail;


class AssignTask extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * The id of the scheduled task.
     * 
     * @var type $task_id
     */
    public $task_id;

	
    /**
     * Create the job.
     *
     * @return void
     */
    public function __construct($task_id) {
        $this->task_id = $task_id;
    }
    

    /**
     * Handle the job.
     *
     * @return void
     */
    public function handle()
    {
        parent::handle();
        
    	DB::transaction(function() {
           
    	   $task = Task::with('customer')->
                         where('id', $this->task_id)->
                         lockForUpdate()->
                         first();

    	   if($task->status != K::TASK_SCHEDULED) {
              $this->reset_bids($task);
              return;
           }
           
           // Get the task's bids list with bidders info.
           $bids = Bid::where('id_tasks', $task->id)->
                        join('users',
                             'users.id', '=', 'tasks_bids.bidder')->
                        select('tasks_bids.*',
                               'users.ref',
                               'users.first_name',
                               'users.last_name',
                               'users.email')->
                        get();

           // Try assigning the task until it expires and there are no bids.
           if(!$task->is_expired() ||
              ($task->is_expired() && !$bids->isEmpty())) {
               $this->assign($task, $bids);
           }
           // Task expired. Cancel it and notify the customer.
           else {
               
               $task->status = K::TASK_CANCELLED;

               $this->reset_bids($task);

               $notification = new TaskCancelledNotification($task);
               $notification->message = trans('messages.task-expired', [
                   'task-subject'=>$task->subject
               ]);
               $notification->to($task->customer->id);

               app('notifier')->push($notification);

               $task->notes = 'Duty expired on '.Hey::now_to_db_datetime();
           }
    		
           $task->save();
    	});
    		 
    }
    
    
    /**
     * Assign the task.
     * 
     * @param Task $task
     * @param Collection $bids
     */
    public function assign($task, $bids) {
                

        if(!$bids->isEmpty()) {
            
            if($bids->count() !== 1) {
               Hey::bug_here();
            }
            
            $matched = $bids->first();
            $matched->is_excluded = false;
            
            $this->reset_bids($task);
            
            // Assign task to matched provider.
            $task->id_users_provider = $matched->bidder;

            // NOTE: if providers are allowed to override the base prices
            // then the prices of the base services attached to the task
            // must be updated here according to the matched provider's settings.
            //$this->set_prices($task, $matched_provider);

            // Start the task.
            $task->status = K::TASK_ONGOING;
            $task->start_datetime = Hey::now_to_db_datetime();

            // Notify provider and customer.
            $notifications = [ new TaskStartedNotification($task),
                               new TaskStartedNotification($task), ];
            
            $notifications[0]->data = json_encode(['provider' => $matched->ref]);
            $notifications[1]->data = null;
            $notifications[0]->to($task->id_users_customer);
            $notifications[1]->to($task->id_users_provider);

            app('notifier')->push($notifications);
            
            $this->send_assignment_email($task, $matched->bidder);

        } else {
            // There are no available providers for this job.
            // Find other suitable ones.
            // TODO We may do the matching just once by finding N providers and
            //      save this list. Then send the offer to each one in the list
            //      in turn until someone accepts.

            $this->match($task);

            // Push a new job like this to follow up the task assignment later.
            // NOTE Must be periods.task.reassign > intervals.task.bid_request_expired
            dispatch( (new self($task->id))->delay( Hey::get_app('periods.task.reassign')) );
        }
    }
    

     /**
    	MATCHING ALGO:
        
        The philosophy behind the task assignment is to have the platform
        pick a short list of providers using a "fair policy", that is a policy
        which does not privilege anyone, and customer preferences.
    	
    	The matching algo is quite simple: it selects providers who offer the
    	specified task services and are	readly available to work, that is
    	have no ongoing tasks. Among these providers with no ongoing tasks there
        will be those who have never worked (have no assigned tasks) and those who
        have worked (have had assigned tasks).
    	To give those who have never worked on tasks more chances we may choose 
        a*N of them and b*N of the ones who have already worked on tasks, with 
        a>b (a+b=1). The N providers may be selected using a fair criterion, for
        example one of the following:
    	
    	     1) Pick N at random. This is probably the fairest.
    	     2) Sort by proximity to customer and pick the nearest N
                (this will only do if proximity to customer is important)
             3) ... nothing else comes to mind ...
    	
    	Currently, the algorithm works in an "exclusive offer" mode, that is only
        one provider is selected randomly from the list of candidates and notified
        with a "job offer". If the offer is accepted within the specified period
        of time then the provider is assigned to the task, otherwise the match
        is repeated until someone accepts or the task expires.
    	
        @param Task $task
     */
    protected function match($task) {
    	    	
    	// The number of available providers to be notified about the new task
    	$N = 1;
    	
        $prefs = $task->customer->settings;
        
        // The offline/inactive time after which the provider is filtered out.
        $offline_time = (new \DateTime)->sub
                        (new \DateInterval( Hey::get_app('intervals.provider.offline') ))->
                        format(K::DB_DATETIME_FORMAT);
						
        // Get the list of ignored providers for this customer, if any.
        $ignored = $task->customer->ignored()->select(['id'])->get()->pluck('id')->toArray();

        // Get all providers who have already been notified about this task.
        $notified = Notification::where('type', K::NOTIFICATION_TASK_EVENT)->
                                  where('event', K::TASK_BID_REQUEST_EVENT)->
                                  where('subject_id', $task->ref)->
                                  select('id_users_recipient')->
                                  get()->
                                  pluck('id_users_recipient')->toArray();

        // Get all providers who have pending bids (those who have responded to
        // the offers in time but haven't been assigned the tasks yet).
        $bidded = Bid::where('is_excluded', true)->select('bidder')->
                       get()->pluck('bidder')->toArray();

        // Form an exclusion list with all those providers that must not be considered.
        $excluded = array_merge($ignored, $notified, $bidded);
        
        // If there is a specified provider to be assigned then no need to do
        // the matching: just notify it (only once) and return.
        if($task->assignee) {
           if(!in_array($task->assignee, $notified)) {
              $n = new TaskBidRequestNotification($task);
              $n->to($task->assignee);
              $this->send_offer_email($task, $task->assignee);
              app('notifier')->push($n);
           }
           return;
        }
        

        $q = User::where('users.role', K::ROLE_PROVIDER);
        
                   // If a specific provider is given for this task, then
                   // filter out any other provider.
                   if($task->assignee)
                      $q->where('users.id', $task->assignee);

               $q->where(function ($q2){
                        $q2->where('users.status', K::USER_ACTIVE)
                           ->orWhere('users.status', K::USER_ONBOARDING);
                   })->
                   where('users.lastseen_at','>=', $offline_time)->
                   //where('users.rating','>=', $prefs->ta_rating)->
                   whereNotIn('users.id', $excluded)->

                   leftJoin('tasks', 'users.id', '=', 'tasks.id_users_provider')->
                   leftJoin('providers_services', 'users.id', '=', 'providers_services.id_users')->
                   leftJoin('users_ratings', 'users.id', '=', 'users_ratings.id_recipient')->
                   select(DB::raw(
                          'users.id,
                           users.role,
                           users.status,
                           users.rating,
                           users.lastseen_at,
                           COUNT(tasks.id) AS tasks,
                           COUNT(IF(tasks.status='.K::TASK_ONGOING.',1,NULL)) AS tasks_ongoing,
                           COUNT(IF(providers_services.id_services='.Service::GENERIC.',1,NULL)) AS generic,
                           COUNT(IF(providers_services.id_services='.Service::EXPERT.',1,NULL)) AS expert,
                           COUNT(users_ratings.id) AS ratings,
                           GROUP_CONCAT(providers_services.categories) AS catags'))->
                   groupBy('id');

        if($task->type == Service::GENERIC){
           $q->having('generic','>',0);
        }
        else
        if($task->type == Service::EXPERT) {
           $catags = explode(',',$task->catags);
           $categories = '(';
           foreach ($catags as $i=>$tag) {
              if(!empty($tag))
                 $categories .= ($i?'OR':'').' catags LIKE \'%'.$tag.'%\' ';
           }
           $categories .= ')';
           $q->having('expert','>',0)
             ->havingRaw($categories);
        }
        else {
           Hey::bug_here($task->type);
        }

        // If rating preferences are given, also include providers with no ratings
        // otherwise they will never be selected.
        if($prefs->ta_rating) {
           $q->havingRaw('(users.rating >= '.$prefs->ta_rating.' OR (users.rating = 0 AND ratings = 0))');
        }
        
        // TODO The following method of selecting N random rows is not efficient.
        // For alternative solutions see the following SO:
        // http://stackoverflow.com/questions/4329396/mysql-select-10-random-rows-from-600k-rows-fast

        $q->havingRaw('(tasks = 0 OR (tasks > 0 AND tasks_ongoing = 0))')->
            orderBy(DB::raw('RAND()'))->
            take($N);

        $results = $q->get();
        
        // Get the ids of the matched providers.
        $providers = $results->pluck('id')->all();
        
        // Notify providers.
        $notifications = array_map(function ($provider) use ($task) {
            $n = new TaskBidRequestNotification($task);
            $n->to($provider);
            $this->send_offer_email($task, $provider);
            return $n;
        }, $providers);
         
        app('notifier')->push($notifications);
    }
    
    
    /**
     * This method resets the "bid pending" flag which excludes the providers 
     * from bidding on other tasks.
     * 
     * @param Task $task
     */
    protected function reset_bids($task) {
        Bid::where('id_tasks', $task->id)->update(['is_excluded'=>false]);
    }
    
    
    /**
     * Set the task prices based on business rules and matched provider's settings.
     * 
     * @param Bidder $provider The provider to which the task has been assigned.
     * This is a record from the Bid joining table.
     * @param type $task
     */
    protected function set_prices($task, $provider) {
        
        // Get the task's base services (the only services at scheduling time).
        $base_services = $task->get_services();

        foreach ($base_services as $base_service) {
            // Uncomment this if providers can set prices by setting service rates.
            //$base_service->amount = $provider->rate;
            // Uncomment this if providers can set prices by setting a markup.
            $base_service->amount *= 1 + $provider->markup / 100.0;
            $base_service->save();
        }
    }
    
    
    /**
     * Email a notice to the provider for the task offer.
     * 
     * @param Task $task
     * @param int $provider_id
     */
    protected function send_offer_email($task, $provider_id) {
        
        $provider = User::where('role', K::ROLE_PROVIDER)->
                          where('id', $provider_id)->first();
        
        $vw_data = [
            'vw_provider_name'  => $provider->first_name,
            'vw_offer_url'      => url('provider/home?task='.$task->ref),
            'vw_offer_expiry'   => Hey::dt_to_human( Hey::get_app('intervals.task.bid_request_expired') ),
        ];
        
        $company = Hey::get_app('company_name');
        $subject = 'You have been offered a new duty';
        $from = Hey::get_app('email.from');
        $email = ['text' => 'emails.provider.task-offer-text'];
        
        Mail::send($email, $vw_data, function ($message)
                  use ($provider, $subject, $from, $company)
        {
            $message->from($from, $company);
            $message->to($provider->email, $provider->full_name());
            $message->subject($subject);
        });
    }
    
    
    /**
     * Email a notice to the provider for the task assignment.
     * 
     * @param Task $task
     * @param int $provider_id
     */
    protected function send_assignment_email($task, $provider_id) {
        
        $provider = User::where('role', K::ROLE_PROVIDER)->
                          where('id', $provider_id)->first();
        
        $vw_data = [
            'vw_provider_name'  => $provider->first_name,
            'vw_offer_url'      => url('provider/home?task='.$task->ref),
        ];
        
        $company = Hey::get_app('company_name');
        $subject = 'You have been assigned a new duty';
        $from = Hey::get_app('email.from');
        $email = ['text' => 'emails.provider.task-assigned-text'];
        
        Mail::send($email, $vw_data, function ($message)
                  use ($provider, $subject, $from, $company)
        {
            $message->from($from, $company);
            $message->to($provider->email, $provider->full_name());
            $message->subject($subject);
        });
    }


}
