<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Task;
use App\Models\User;
use App\Models\Service;
use App\Exceptions\AGException;
use App\Exceptions\AGPaymentException;
use App\Exceptions\AGMarketplaceException;
use App\Exceptions\AGInvalidStateException;
use App\Notifications\TransactionNotification;
use App\Notifications\AlertNotification;
use App\Hey;
use App\K;
use App,DB,Log;


class CancelTask extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * The id of the cancelled task.
     * 
     * @var type $task_id
     */
    public $task_id;
    
    /**
     * The status of the task at the time of cancellation.
     * 
     * @var unknown
     */
    public $task_status;
    
    /**
     * The user who requested the cancellation.
     *
     * @var unknown
     */
    public $requestor_id;
    
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($task_id, $task_status, $requestor_id) {
        $this->task_id = $task_id;
        $this->task_status = $task_status;
        $this->requestor_id = $requestor_id;
    }
    

    /**
     * Handle the job.
     *
     * @return void
     */
    public function handle()
    {
        parent::handle();
        
    	DB::transaction(function() {
    		 
    	    $task = Task::with(['provider','customer'])
    	                ->where('id', $this->task_id)
                        ->lockForUpdate()
                        ->first();
    		
            assert($task && $this->task_id);
    		
            if($task->status != K::TASK_CANCELLED)
               return;

            $task_status_on_cancel = $this->task_status;
            
            $mutual = $task->customer->id.'&'.$task->provider->id;

            $requestor = $this->requestor_id === $task->customer->id ? $task->customer :
                        ($this->requestor_id === $task->provider->id ? $task->provider : 
                        ($this->requestor_id === $mutual ? $this->requestor_id : null));

            // If a charge is made, we want to avoid some exception rising
            // afterwards to fail and retry the job risking to charge the customer 
            // multiple times. So catch everything, log the issue and finish.
            try {

            // Cancelled ongoing task.
            if($task_status_on_cancel == K::TASK_ONGOING) {

                // If customers cancel a task, then the provider's cancellation
                // policy applies, which may or may not lead to a charge.
                if($requestor instanceof User && $requestor->is_customer()) {
                    
                   $txn = $this->apply_cancellation_policy($task);

                   if(is_null($txn)) return;
                   
                   if($txn === K::TASK_CANCEL_POLICY_NO_CHARGE) {}
                   else {
                      // Notify customer.
                      $notification = new TransactionNotification;
                      $notification->message = trans('messages.txn-charge-made', [
                          'charge-amount' => Hey::to_currency($txn->amount_gross),
                          'task-subject' => Hey::truncate_text( $task->subject, 30 ),
                      ]);
                      $notification->to($task->id_users_customer);

                      app('notifier')->push($notification);
                      
                      $this->send_charge_notice_email($task, $txn);
                   }
                }
                // If providers cancel a task, then they will not be paid any 
                // services and may incur a penalty (bad ranking, etc.).
                else if($requestor instanceof User && $requestor->is_provider()) {
                    // TODO
                }
                // If admins cancel ongoing tasks, no further actions are taken.
                else if($requestor instanceof User && $requestor->is_admin()) {
                }
                // For mutual cancellations, no further actions are taken.
                else if($requestor === $mutual) {
                }
                else {
                   assert(false);
                }
                
                $task->notes = 'Cancelled by '.($requestor instanceof User?
                                                $requestor->role_to_string():
                                                'agreement').
                               ' on '.Hey::now_to_db_datetime();
            }
            // Cancelled disputed task.
            else
            if($task_status_on_cancel == K::TASK_DISPUTED) {
                // TODO:
            }
            else {
                throw new AGException;
            }

            $task->save();
            
            }
            catch(\Exception $exc) { Log::error($exc); }
            catch(\Throwable $thr) { Log::error($thr); }
    	});
    		 
    }
    
    
    /**
     * Apply the provider's cancellation policy to the task. This may lead to
     * a charge or not.
     * 
     * @param Task $task
     * @return Transaction|K::TASK_CANCEL_NO_CHARGE|null If a charge is made,
     * then a Transaction is returned. If no charge is made, then K::TASK_CANCEL_NO_CHARGE
     * is returned. If there are any issues with the charge, null is returned.
     */
    protected function apply_cancellation_policy($task) {
        
        $cancel_policy = $task->provider->settings->task_cancel_policy;
        
        // To give the customer a chance of cancelling the task in case the
        // provider is unresponsive check whether it has posted a request for
        // a mutual cancellation to which the other party has not responded.
        // If so, do not charge anything.
        $msg = $task->get_pending_cancel_request();
        
        if($msg && $msg->is_request_expired() && 
           $msg->id_users_sender === $task->customer->id)
        {
           $cancel_policy = K::TASK_CANCEL_POLICY_NO_CHARGE;
        }

        if($cancel_policy === K::TASK_CANCEL_POLICY_CHARGE_SERVICES) {
           return $this->do_charge($task->customer, $task->provider, $task);
        }
        else
        if($cancel_policy === K::TASK_CANCEL_POLICY_CHARGE_FEE) {
           $service = new Service;
           $service->type = Service::EXTRA;
           $service->price = Hey::get_app('fees.task.fix.cancellation');
           $service->description = 'Task cancellation fee';
           return $this->do_charge($task->customer, $task->provider, 
                                   $task, collect([$service]));
        }
        else
        if($cancel_policy === K::TASK_CANCEL_POLICY_NO_CHARGE) {
           return K::TASK_CANCEL_POLICY_NO_CHARGE;
        }
        else {
           Hey::bug_here();
        }
    }
        
}
