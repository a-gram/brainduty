<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use App\Models\Payment;
use App\Models\Payout;
use App\Exceptions\AGResourceNotFoundException;
use App\Hey;
use App\K;
use DB,Hash,Log;

/**
 * This job unregister a user from the platform. Currently, the user record is not
 * deleted from the database, as it is related to many information that  we want 
 * to retain and that would be lost due to enforced data integrity.
 * Instead, all personally identifiable information are erased so that the user
 * can no longer be identified and the user record is kept. We'll be left with a
 * zombie/ghost record but the related data that we want to retain is much more
 * than this ghost data.
 */
class CancelUser extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * The id of the user.
     * 
     * @var type $user_id
     */
    public $user_id;
    
    /**
     * Create the job.
     *
     * @return void
     */
    public function __construct($user_id) {
        $this->user_id = $user_id;
    }
    

    /**
     * Handle the job.
     *
     * @return void
     */
    public function handle() {
        
        parent::handle();
        
        DB::transaction(function () {
            
            $user = User::where('id', $this->user_id)->first();

            if(!$user)
               throw new AGResourceNotFoundException('User not found : '.$this->user_id);

            $account = app('account')->of($user);
            $archive = app('archive')->of($user);

            $user->password = Hash::make(Hey::get_app('key'));
            $user->first_name = null;
            $user->last_name = null;
            $user->dob = null;
            $user->mobile = null;
            //$user->email = null; //< May still be needed by pending jobs ...
            $user->address = null;
            $user->city = null;
            $user->state = null;
            $user->zip_code = null;
            $user->country = null;
            $user->tax_id = null;
            $user->entity_type = null;
            $user->business_name = null;
            $user->business_number = null;
            $user->about = null;
            
            $user->status = K::USER_CANCELLED;
            
            $user->save();

            // Documents.
            // There is no PII in the documents table and the document files referenced
            // therein are deleted from the storage, so we're fine.

            // Messages and Notifications.
            // There is no PII in there. We want to keep these.

            // Identity Verification record.
            // There is no PII in there.

            // Tasks and Transactions.
            // There is no PII in there. We want to keep these.

            // Payment/Payout.
            // There is some PII in there. Delete them.
            Payment::where('id_users_customer', $user->id)->delete();
            Payout::where('id_users_provider', $user->id)->delete();

            // Remove the user from favourite and ignore lists.
            DB::table('users_fav_list')->where('id_users_fav', $user->id)->delete();
            DB::table('users_ignore_list')->where('id_users_ignored', $user->id)->delete();
            
            // Close the marketplace account (currently the Stripe account).
            // NOTE: This may fail. In such case log the error and retry.
            try {
                $account->close();
            }
            catch (\Exception $exc) {
                Log::error('Account '.$user->ref_ext.': '.$exc->getMessage());
                throw $exc;
            }
            
            // Remove private and public storage.
            $archive->remove();
            $archive->pub()->remove();

        });
        
    }
        
}
