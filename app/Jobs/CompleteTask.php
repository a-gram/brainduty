<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\CompleteTask;
use App\Models\Task;
use App\Models\Message;
use App\Models\User;
use App\Exceptions\AGException;
use App\Exceptions\AGPaymentException;
use App\Exceptions\AGMarketplaceException;
use App\Notifications\TransactionNotification;
use App\Notifications\AlertNotification;
use App\Hey;
use App\K;
use App,DB,Mail;


class CompleteTask extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * The id of the ended task.
     * 
     * @var type $task_id
     */
    public $task_id;

	
    /**
     * Create the job.
     *
     * @return void
     */
    public function __construct($task_id) {
        $this->task_id = $task_id;
    }
    

    /**
     * Handle the job.
     *
     * @return void
     */
    public function handle()
    {
        parent::handle();
        
    	DB::transaction(function() {
    		 
    	    $task = Task::with(['provider','customer'])->
    	                  where('id', $this->task_id)->
                          lockForUpdate()->
                          first();
    	              
    	    assert($task && $this->task_id);
    		
    	    if($task->status != K::TASK_ENDED)
               return;
            
            $tlm = $task->get_manager();
            $tlm->set_requestor(User::sys());

            // The task can't be closed yet. Re-enqueue a new job to check later.
            // If there is a disputable period then we check again just after this
            // period elapsed, otherwise use the set close reattempts period.
            if(!$tlm->close()) {
               $later = Hey::get_app('intervals.task.disputable') ?
                        Hey::dt_to_sec(Hey::get_app('intervals.task.disputable')) * 1.1 :
                        Hey::get_app('periods.task.close_reattempts');
               
               dispatch( (new self($task->id))->delay( $later ));
               return;
            }
            
            // The task has been completed. Do any task-completion-related jobs.

            if($task->is_funded) {
               
               $payment = $task->transactions->where('type', K::TXN_TYPE_PAYMENT)->
                                               first();
               
               // If there is no payment for the task, charge the customer now.
               if(!$payment) {
                   // $payment = $this->do_charge(...)
               }
               // If there is a pending uncaptured payment, capture it.
               else if($payment && $payment->is_pending() && !$payment->is_captured) {
                   // app('account')->of($provider)->capture($payment)
               }
               // If there is a pending captured payment just close the task.
               else if($payment && $payment->is_pending() && $payment->is_captured) {}
               // There should not be a completed payment in the normal flow of the
               // program but it may happen if this job gets stuck in the queue for
               // long (cause of crashes, etc.) and the payment completes in the
               // meanwhile. In such a case just close the task.
               else if($payment && $payment->is_completed()) {}
               else {
                   Hey::bug_here();
               }

               // At this point for what concerns our platform the transaction
               // would be considered completed and the amount available for the
               // provider to withdraw. However, there may be other waiting time
               // enforced by external services. For example, payment processors
               // may have a security 'hold period' before they transfer the funds
               // to bank accounts. So we need to get the (pending) payment for
               // this task and check if the funds can be released to the provider.
               // If funds can't be released, then there must be a scheduled job
               // that periodically checks and clear pending payments to providers.

               if($payment->is_clear()) {
                  $payment->status = K::TXN_COMPLETED;
                  $payment->save();
               }

               $this->make_invoice($task->customer, $task, $payment);
               $this->send_receipt($task->customer, $task, $payment);
            }
            else {
               // TODO Handle the case for not funded tasks
            }
            
    	});
    }
    
    
    /**
     * Make an invoice for the given task.
     * 
     * @param Customer $customer
     * @param Task $task
     * @param Transaction $payment
     */
    protected function make_invoice($customer, $task, $payment) {
        
    }

    
    /**
     * Email a receipt to the customer for the payment of the given task.
     * 
     * @param Customer $customer
     * @param Task $task
     * @param Transaction $payment
     */
    protected function send_receipt($customer, $task, $payment) {
        
        $vw_data = [
            'vw_customer_name'        => $customer->first_name,
            'vw_receipt_ref'          => $payment->ref,
            'vw_receipt_date'         => Hey::date_to_human($payment->created_at),
            'vw_receipt_description'  => $payment->description,
            'vw_receipt_pay_method'   => $payment->settle_method,
            'vw_receipt_amount_net'   => Hey::to_currency(
                                              $payment->amount_gross -
                                              $payment->amount_tax
                                         ),
            'vw_receipt_amount_tax'   => Hey::to_currency($payment->amount_tax),
            'vw_receipt_amount_total' => Hey::to_currency($payment->amount_gross),
            'vw_task_ref'             => $task->ref,
        ];

        $company = Hey::get_app('company_name');
        $subject = $company.' Receipt';
        $from = Hey::get_app('email.from');
        $email = ['text' => 'emails.customer.receipt-text'];
        
        Mail::send($email, $vw_data, function ($message) 
                   use ($customer, $subject, $from, $company)
        {
            $message->from($from, $company);
            $message->to($customer->email, $customer->full_name());
            $message->subject($subject);
        });
    }
    
}
