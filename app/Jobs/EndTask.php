<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Task;
use App\Models\Message;
use App\Models\User;
use Exception;
use App\Exceptions\AGException;
use App\Exceptions\AGPaymentException;
use App\Exceptions\AGMarketplaceException;
use App\Notifications\TransactionNotification;
use App\Notifications\AlertNotification;
use App\Hey;
use App\K;
use App,DB,Mail,Log;


class EndTask extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * The id of the ended task.
     * 
     * @var type $task_id
     */
    public $task_id;

	
    /**
     * Create the job.
     *
     * @return void
     */
    public function __construct($task_id) {
        $this->task_id = $task_id;
    }
    

    /**
     * Handle the job.
     *
     * @return void
     */
    public function handle()
    {
        parent::handle();
        
    	DB::transaction(function() {
    		 
    	    $task = Task::with(['provider','customer'])->
    	                  where('id', $this->task_id)->
                          lockForUpdate()->
                          first();
    	              
    	    assert($task && $this->task_id);
    		
    	    if($task->status != K::TASK_ENDED)
               return;
           
            $filter = ['type' => K::TXN_TYPE_PAYMENT];

            $payment = $task->transactions()->where($filter)->first();

            // If no payment has been made, charge the custmer now.
            // TODO Charge the customer once the dispute period is over?
            if(!$payment) {

                $payment = $this->do_charge($task->customer, $task->provider, $task);

                if(is_null($payment)) return;

                // We don't want some exception rising in the following code to 
                // screw up and risk multiple charges, so just catch everything, 
                // log the issue and continue. At worst nobody will be notified.
                try {
                    // Charge succeeded. Here we can release/unlock the deliverables
                    // (if any) associated with the transactional message that ended the task.
                    $this->release_deliverables($task);

                    // Notify customer.
                    $notification1 = new TransactionNotification($payment);
                    $notification1->message = trans('messages.txn-charge-made', [
                        'charge-amount' => Hey::to_currency($payment->amount_gross),
                        'task-subject' => Hey::truncate_text( $task->subject, 30 ),
                    ]);
                    $notification1->to($task->id_users_customer);
					
                    // Notify provider.
                    $notification2 = new TransactionNotification($payment);
                    $notification2->message = trans('messages.txn-payment-received', [
                        'charge-amount' => Hey::to_currency($payment->amount_gross),
                        'task-subject' => Hey::truncate_text( $task->subject, 30 )
                    ]);
                    $notification2->to($task->id_users_provider);

                    app('notifier')->push([$notification1, $notification2]);

                    $this->send_charge_notice_email($task, $payment);
                }
                catch(\Exception $exc) { Log::error($exc); }
                catch(\Throwable $thr) { Log::error($thr); }
            }

            dispatch( new \App\Jobs\CompleteTask($task->id) );
    	});
    }
    
    
    /**
     * This method releases/unlocks the deliverables associated with the task
     * upon payment.
     * 
     * @param unknown $task
     */
    protected function release_deliverables($task) {
    	
    	Message::where('id_tasks', $task->id)->
    	         where('type', K::MESSAGE_TASK_DELIVERABLES)->
    	         update(['is_deliverable' => true]);
    }
    
        
}
