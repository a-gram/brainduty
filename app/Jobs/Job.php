<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Collection;
use App\Models\User;
use App\Models\Task;
use App\Exceptions\AGPaymentException;
use App\Exceptions\AGJobAbortedException;
use App\Notifications\AlertNotification;
use App\Hey;
use App\K;
use Log, Mail;


abstract class Job
{
    /*
    |--------------------------------------------------------------------------
    | Queueable Jobs
    |--------------------------------------------------------------------------
    |
    | This job base class provides a central location to place any logic that
    | is shared across all of your jobs. The trait included with the class
    | provides access to the "onQueue" and "delay" queue helper methods.
    |
    */

    use Queueable;
    
        
    /**
     * This method must be called by any derived class in order to properly
     * handle job failures.
     * 
     * @throws AGJobAbortedException
     */
    public function handle() {
        // TODO Check for a signal somewhere indicating that this job is aborted
        //      and do not execute it if it is.
//        if($is_aborted) {
//           throw new AGJobAbortedException;
//        }
    }

    
    /**
     * This method executes a charge for a task or other services.
     * 
     * @param User $payer The user being charged (typically a customer).
     * @param User $payee The user being paid (typically a provider).
     * @param Task $task The task for which the charge is made.
     * @param Collection|null $services The services for which the charge is made.
     * @return Charge|null If the charge is successfull, a Charge object with the
     * transaction details is returned, otherwise null is returned. The charge may
     * be reattempted if unsuccessfull.
     * @throws AGPaymentException Throws if the charge fails and cannot be reattempted.
     */
    protected function do_charge(User $payer,
                                 User $payee,
                                 Task $task,
                                 Collection $services = null) {
        
        $charge = app('account')->of($payee)
                                ->charge($payer)
                                ->for_this($services ?: $task);
        
    	try {            
            $txn = $charge->execute();
            $task->transactions()->save($txn);
            return $txn;
    	}
    	// For unsuccessfull payments, such as card failures (declined, expired, etc.),
    	// retry a few times before aborting.
        // NOTE: For the following code to work, must be MAX_CHARGE_REATTEMPTS <= --tries
        //       where --tries is the flag passed to the job worker at stratup.
    	catch (AGPaymentException $exc) {
            
            $error = $exc->get_context();
    	
            if($this->attempts() <= K::MAX_CHARGE_REATTEMPTS) {

               $last_attempt = $this->attempts() == K::MAX_CHARGE_REATTEMPTS;

               $subject = $services ? implode(', ', $services->pluck('description')->all())
                                    : 'duty '.$task->subject;

               $notification = new AlertNotification;
               $notification->message = !$last_attempt ?
                  trans('messages.txn-charge-fail-reattempt', ['subject'=>$subject]) :
                  trans('messages.txn-charge-fail', ['subject'=>$subject]);
               $notification->to($task->id_users_customer);
               
               app('notifier')->push($notification);
               
               if($charge->using(['error'=>$error])->can_retry())
                  $this->release(Hey::get_app('periods.txn.charge_reattempts'));
               else
                  Log::error($error);
            }
            else {
               // TODO Couldn't charge the customer.
               // Propagate to fail and store the job.
               throw $exc;
            }
            return null;
    	}
        // For any other issues, since we don't know whether the charge has been
        // made and to avoid multiple charges in case it has, remove the job from
        // the queue and log the issue.
    	catch (\Exception $exc) {
            
            // TODO This failed job will be deleted and not stored in the failed
            //      jobs table. Find a way to fail and store the job.
            //$failer = App::make('queue.failer');
            //$failer->log($this->connection, $this->queue, $this->getRawBody());

            Log::error($exc);
            return null;
    	}
    }
    
    
    /**
     * Email a charge notice to the customer for the payment.
     * 
     * @param Task $task
     * @param Charge $payment
     */
    protected function send_charge_notice_email($task, $payment) {
        
        $vw_data = [
            'vw_customer_name'         => $task->customer->first_name,
            'vw_payment_method'        => $task->customer->default_payment_method()->method,
            'vw_payment_method_type'   => $task->customer->default_payment_method()->source_type,
            'vw_payment_method_number' => $task->customer->default_payment_method()->source_number,
            'vw_task_ref'              => $task->ref,
            'vw_task_subject'          => $task->subject,
            'vw_charge_amount_total'   => Hey::to_currency($payment->amount_gross),
            'vw_charge_dispute_period' => Hey::dt_to_human(Hey::get_app('intervals.task.disputable')),
        ];
        
        $company = Hey::get_app('company_name');
        $subject = $company.' Service Charge';
        $from = Hey::get_app('email.from');
        $email = ['text' => 'emails.customer.charge-text'];
        
        Mail::send($email, $vw_data, function ($message)
                  use ($task, $subject, $from, $company)
        {
            $message->from($from, $company);
            $message->to($task->customer->email, $task->customer->full_name());
            $message->subject($subject);
        });
    }
    
}
