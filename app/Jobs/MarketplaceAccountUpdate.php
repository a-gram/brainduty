<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Provider;
use App\Models\ProviderIdentity;
use App\Exceptions\AGResourceNotFoundException;
use App\Notifications\IdentityInquiryNotification;
use App\Exceptions\AGException;
use App\Hey;
use App\Events\EventUserIdentityVerification;
use App\K;
use Log, DB, Event;


class MarketplaceAccountUpdate extends MarketplaceJob implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    

    /**
     * Handle the job.
     *
     * @return void
     */
    public function do_handle($event) {
        
        //$account = $event->data->object;
        $account = \Stripe\Account::retrieve($this->account_id);
        
        $account_previous = $event->data->previous_attributes;

        $provider_id = $account->metadata->user_id;

        assert($provider_id);

        $provider = app('identity.manager')->of($provider_id)->get_provider();

        assert($provider);

        // HANDLE ACCOUNT VERIFICATION

        // Handle events of unverified accounts that need further verification.
        if($account->legal_entity->verification->status === 'unverified') {

           $needed = $account->verification->fields_needed;
           $details = $account->legal_entity->verification->details;

           $verify_names = in_array('legal_entity.first_name', $needed) ||
                           in_array('legal_entity.last_name', $needed);

           $verify_address = in_array('legal_entity.address.line1', $needed) ||
                             in_array('legal_entity.address.city', $needed) ||
                             in_array('legal_entity.address.postal_code', $needed) ||
                             in_array('legal_entity.address.state', $needed);

           $verify_dob = in_array('legal_entity.dob.day', $needed) ||
                         in_array('legal_entity.dob.month', $needed) ||
                         in_array('legal_entity.dob.year', $needed);

           $verify_bank = in_array('external_account', $needed);

           $verify_business = in_array('legal_entity.business_name', $needed) ||
                              in_array('legal_entity.business_tax_id', $needed);

           $verify_type = in_array('legal_entity.type', $needed);

           $verify_document = in_array('legal_entity.verification.document', $needed);
           
           $tos_acceptance = in_array('tos_acceptance.date', $needed) ||
                             in_array('tos_acceptance.ip', $needed);
           
           $missing = [];
           
           // 
           if($verify_names) {
              $requires = 'first and last name';
              $missing[] = K::IDENTITY_INFO_NAMES;
           }
           elseif($verify_address) {
              $requires = 'address';
              $missing[] = K::IDENTITY_INFO_ADDRESS;
           }
           elseif($verify_dob) {
              $requires = 'date of birth';
              $missing[] = K::IDENTITY_INFO_DOB;
           }
           elseif($verify_bank) {
              $requires = 'bank account details';
              $missing[] = K::IDENTITY_INFO_BANK;
           }
           elseif($verify_business) {
              $requires = 'business details';
              $missing[] = K::IDENTITY_INFO_BUSINESS;
           }
           elseif($verify_type) {
              $requires = 'entity type';
              $missing[] = K::IDENTITY_INFO_ENTITY;
           }
           elseif($verify_document) {
              $requires = 'scan of an identifying document (passport, driver license)';
              $missing[] = K::IDENTITY_INFO_PHOTOID;
           }
           elseif($tos_acceptance) {
              $requires = 'tos acceptance';
           }
           else {
              // Account unverified and no needed fields ?
              throw new \UnexpectedValueException;
           }
           
           if(!$provider->identity->is_closed()) {
               $provider->identity->add_required_info($missing);
               $provider->identity->add_notes( $details ?: '' /*$requires*/ );
               $provider->identity->save();
           }
        }
        // Handle accounts under verification.
        else
        if($account->legal_entity->verification->status === 'pending') {
            // What to do here ?
        }
        // Handle accounts that have become verified.
        else
        if($account->legal_entity->verification->status === 'verified') {

            if($account_previous && isset($account_previous['legal_entity']['verification'])) {
                
                $was = $account_previous->legal_entity->verification->status;
                // If the account becomes verified then signal it by setting
                // the relevant flag in the provider's verification record.
                if($was === 'unverified' || !$provider->identity->ext_verified) {
                   $provider->identity->ext_verified = true;
                   $provider->identity->save();
                }
            }
        }

        // HANDLE OTHER ACCOUNT STUFFS
        // ...
        
        $this->check_account($provider, [
            'charges_enabled'   => $account->charges_enabled,
            'transfers_enabled' => $account->transfers_enabled
        ]);
        
        Event::fire(new EventUserIdentityVerification($provider->id));
		
        return true;
    }
    
    
    /**
     * Check the payment account for any issues such as charges and/or transfers
     * and notify the provider.
     * 
     * @param Provider $provider
     * @param array $account
     * @return type
     */
    protected function check_account($provider, $account) {
        
        $issues = [];
        
        if(!$account['charges_enabled']) {
            $issue = new \App\Notifications\AlertNotification;
            $issue->message = trans('messages.account-charges-disabled');
            $issues[] = $issue;
            $provider->status = K::USER_SUSPENDED;
            $provider->save();
        }
        
        if(!$account['transfers_enabled']) {
            $issue = new \App\Notifications\AlertNotification;
            $issue->message = trans('messages.account-transfers-disabled');
            $issues[] = $issue;
        }
        
        if($issues) {
           app('notifier')->push($issues, $provider->id);
        }
    }

}
