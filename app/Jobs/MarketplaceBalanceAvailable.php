<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Provider;
use App\Models\Transaction;
use App\Exceptions\AGResourceNotFoundException;
use App\Notifications\IdentityInquiryNotification;
use App\Exceptions\AGException;
use App\Hey;
use App\K;
use Log, DB;

/**
 * This job is triggered by balance.update events sent by Stripe whenever cleared
 * transactions are added to the available balance. Since there is no event when a
 * single transaction is cleared by Stripe, to change the status we would have to
 * poll the user's balance. Instead, we use this event to clear all pending transactions 
 * in the user's account based on the clear date-time (which is set with the 'available_on'
 * field provided by Stripe when the transaction is created).
 */
class MarketplaceBalanceAvailable extends MarketplaceJob implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    

    /**
     * Handle the job.
     *
     * @return void
     */
    public function do_handle($event) {
        
        $provider = Provider::where('ref_ext', $this->account_id)
                             ->select(['id'])
                             ->first();
        
        assert($provider);
        
        $now = (new \DateTime)->format(K::DB_DATETIME_FORMAT);
        
        // Clear all pending payment transactions for the provider.
        $filter = [
            [function($q) use ($provider) {
                $q->where('id_recipient',$provider->id)
                  ->orWhere('id_sender',$provider->id);
            }],
            ['type','=',K::TXN_TYPE_PAYMENT],
            ['status','=',K::TXN_PENDING],
            //[function($q){$q->whereNotNull('id_tasks');}],
            [function($q) use ($now) {
                $q->whereNotNull('clear_datetime')
                  ->where('clear_datetime','<=', $now);
            }],
        ];
        
        Transaction::where($filter)->update(['status' => K::TXN_COMPLETED]);
        
        return true;
    }
        
}
