<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use App\Exceptions\AGException;
use App\Hey;
use Log, DB, Config;


/**
 *  Abstract class to handle asynchronous marketplace events (webhooks).
 *  All marketplace webhook event handlers must derive from this and implement
 *  the do_handle() method.
 */
abstract class MarketplaceJob extends Job
{
    /**
     * The event id.
     * 
     * @var string $event_id
     */
    public $event_id;
    
    
    /**
     * The provider's account id the event refers to.
     * 
     * @var string
     */
    protected $account_id;
    
    
    /**
     * Create the job.
     *
     * @return void
     */
    public function __construct($event_id, $account_id) {
        $this->event_id = $event_id;
        $this->account_id = $account_id;
    }
    

    /**
     * Handle the job.
     *
     * @return void
     */
    public function handle() {
        
        parent::handle();
        
        \Stripe\Stripe::setApiKey( Config::get('services.stripe.skey') );
        
        if($this->is_event_processed()) return;
        
        $event = \Stripe\Event::retrieve($this->event_id, [
            'stripe_account' => $this->account_id,
        ]);
        
        DB::transaction(function () use ($event) {
            try {
                if($this->do_handle($event))
                   $this->log_event();
            }
            catch (\Exception $exc) {
                throw new AGException('ERROR', $event, $exc);
            }
        });

    }
    
    
    /**
     * Handle the event.
     * 
     * @param object event
     * @return boolean This method must return true if the event has been processed,
     * false otherwise (e.g. it needs to be handled again at a later stage). If false
     * is returned then the event will not be logged.
     */
    abstract public function do_handle($event);
    
    
    /**
     * Check for duplicates.
     * https://stripe.com/docs/webhooks#best-practices
     * 
     * @return boolean
     * @throws type
     */
    public function is_event_processed() {
        
        return DB::table('x_events')->where('id', $this->event_id)->first() !== null;
    }
    
    
    /**
     * Log marketplace events.
     * https://stripe.com/docs/webhooks#best-practices
     */
    public function log_event() {
        
        DB::table('x_events')->insert(['id' => $this->event_id]);
    }
    
}
