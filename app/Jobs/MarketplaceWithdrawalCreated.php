<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Transaction;
use App\Models\Provider;
use App\Exceptions\AGResourceNotFoundException;
use App\Notifications\IdentityInquiryNotification;
use App\Exceptions\AGException;
use App\Hey;
use App\K;
use Log, DB;


class MarketplaceWithdrawalCreated extends MarketplaceJob implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    

    /**
     * Handle the job.
     *
     * @return boolean
     */
    public function do_handle($event) {
        
        $transfer = $event->data->object;
        
        $txn_ref = $transfer->metadata->txn_ref;
        
        // NOTE: This event should not be handled if payout.interval is set to manual.
        
        // If there is no transaction reference in the transfer then the withdrawal
        // was automatically made by the marketplace provider and we must insert 
        // the corresponding record in the database.
        if(!$txn_ref) {
            
            $provider = Provider::where('ref_ext', $this->account_id)
                                ->join('users_providers_settings',
                                       'users.id', '=', 'users_providers_settings.id_users')
                                ->select('users.id',
                                         'users_providers_settings.default_payout_method')
                                ->first();
            
            $txn = \Stripe\BalanceTransaction::retrieve($transfer->balance_transaction, [
                   'stripe_account' => $this->account_id,
            ]);
            
            assert($txn);
            
            $fees = 0; $taxes = 0;

            foreach ($txn->fee_details as $fee) {
                if($fee->type == 'tax') $taxes += $fee->amount;
                else                    $fees += $fee->amount;
            }

            $txn_record = new Transaction;
            
            $txn_record->amount_gross   =  $txn->amount;
            $txn_record->amount_fee     =  $fees;
            $txn_record->amount_tax     =  $taxes;
            $txn_record->amount_bal     =  $txn->net;
            $txn_record->currency       =  Hey::get_app('currency');
            $txn_record->status         =  $txn->status;
            $txn_record->description    =  'Funds withdrawal';
            $txn_record->type           =  $txn->type;
            $txn_record->status         =  $txn->status;
            $txn_record->settle_status  =  $transfer->status;
            //$txn_record->request_id     =  $this->params['request_id'];
            //$txn_record->request_auth   =  $this->params['request_auth'];
            $txn_record->settle_method  =  $provider->default_payout_method;
            $txn_record->id_sender      =  $provider->id;
            $txn_record->id_recipient   =  $provider->id;
            $txn_record->ref            =  Transaction::make_ref($txn_record);
            $txn_record->ref_ext        =  $txn->id;
            $txn_record->ref_ext_source =  $transfer->id;
            $txn_record->clear_datetime =  Hey::timestamp_to_db_datetime($txn->available_on);

            $txn_record->validate();
            
            $txn_record->save();
            
            $transfer = \Stripe\Transfer::update($transfer->id, 
                ['metadata' => ['txn_ref' => $txn_record->ref ] ],
                ['stripe_account' => $this->account_id ]
            );

        }
        
        return true;
    }
    
}

