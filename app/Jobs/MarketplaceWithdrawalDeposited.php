<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Transaction;
use App\Models\ProviderIdentity;
use App\Exceptions\AGResourceNotFoundException;
use App\Notifications\IdentityInquiryNotification;
use App\Exceptions\AGException;
use App\Hey;
use App\K;
use Log, DB;


class MarketplaceWithdrawalDeposited extends MarketplaceJob implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    

    /**
     * Handle the job.
     *
     * @return boolean
     */
    public function do_handle($event) {
        
        $transfer = $event->data->object;
        
        // Retrieve the original Transfer shouldn't the one included in the event
        // be updated with the txn_ref in the metadata.
        if(!$transfer->metadata->txn_ref) {
            $transfer = \Stripe\Transfer::retrieve($transfer->id, [
                'stripe_account' => $this->account_id,
            ]);
        }
        
        // Abort if for whatever reason we receive a failed transfer.
        if($transfer->failure_code) return true;

        $txn_ref = $transfer->metadata->txn_ref;
        
        // At this point we should always have a transaction reference attached
        // to the transfer, whether it was manually or automatically created.
        // However, for automatic transfers if the transfer.created event has not 
        // yet been received (apparently it's possible that the transfer events are 
        // sent out of order and not in the created->paid/failed order) the txn_ref
        // will be null and there won't be a record in the db. In such a case we may
        // release the job back into the queue  and retry later (hoping in the 
        // meanwhile we receive the transfer.created).
        if(!$txn_ref && Hey::get_app('payout.interval') !== 'manual') {
            // To prevent the job from being redispatched forever in case we never
            // get the txn_ref, abort it after 24 hours from the event.
            if(Hey::now_to_timestamp() - $event->created > 86400)
               throw new AGException('Job timeout');
            
            dispatch( (new self ($this->event_id, $this->account_id))
                    ->onConnection('system')
                    ->onQueue('account')
                    ->delay( Hey::get_app('periods.1h') ));
            
            return false;
        }
        
        // Update the transfer status (paid/completed).
        $txn = Transaction::with('recipient')->where('ref', $txn_ref)->first();
        //$txn->status = K::TXN_COMPLETED; //< Txn was completed when funds have been taken.
        $txn->settle_status = $transfer->status;
        $txn->save();
        
        return true;
    }
    
}

