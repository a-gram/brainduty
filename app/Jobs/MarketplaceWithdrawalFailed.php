<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Transaction;
use App\Models\ProviderIdentity;
use App\Exceptions\AGResourceNotFoundException;
use App\Notifications\TransactionNotification;
use App\Exceptions\AGException;
use App\Hey;
use App\K;
use Log, DB, Mail;


class MarketplaceWithdrawalFailed extends MarketplaceJob implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    

    /**
     * Handle the job.
     *
     * @return boolean
     */
    public function do_handle($event) {
        
        $transfer = $event->data->object;
        
        // NOTE Transfers may fail immediately when created, in which case we won't
        //      get the updated Transfer object with the transaction reference in
        //      the metadata, so in such a case retrieve the original Transfer.
        if(!$transfer->metadata->txn_ref) {
            $transfer = \Stripe\Transfer::retrieve($transfer->id, [
               'stripe_account' => $this->account_id,
            ]);
        }
        
        $txn_ref = $transfer->metadata->txn_ref;
        
        // At this point we should always have a transaction reference attached
        // to the transfer, whether it was manually or automatically created.
        // However, for automatic transfers if the transfer.created event has not 
        // yet been received (apparently it's possible that the transfer events are 
        // sent out of order and not in the created->paid/failed order) the txn_ref
        // will be null and there won't be a record in the db. In such a case we may
        // release the job back into the queue  and retry later (hoping in the 
        // meanwhile we receive the transfer.created).
        if(!$txn_ref && Hey::get_app('payout.interval') !== 'manual') {
            // To prevent the job from being redispatched forever in case we never
            // get the txn_ref abort it after 24 hours from the event.
            if(Hey::now_to_timestamp() - $event->created > 86400)
               throw new AGException('Job timeout');
            
            dispatch( (new self ($this->event_id, $this->account_id))
                    ->onConnection('system')
                    ->onQueue('account')
                    ->delay( Hey::get_app('periods.1h') ));
            
            return false;
        }
        
        // Update the transfer status (paid/completed).
        $txn = Transaction::with('recipient')->where('ref', $txn_ref)->first();
        //$txn->status = K::TXN_CANCELLED;
        $txn->settle_status = $transfer->status;
        $txn->save();
        
        // Notify provider
        $notification = new TransactionNotification($txn);
        $notification->message = trans('messages.txn-withdrawal-failed', [
            'txn-amount' => Hey::to_currency($txn->amount_gross),
            'fail-reason' => $transfer->failure_message,
        ]);
        $notification->to($txn->recipient->id);

        app('notifier')->push($notification);
        
        $this->send_failure_notice_email($txn, $transfer->failure_message);
        
        return true;
    }
    
    
    /**
     * Email a failure notice to the provider.
     * 
     * @param Transaction $txn
     * @param string $failure
     */
    protected function send_failure_notice_email($txn, $failure) {
        
        $vw_data = [
            'vw_provider_name'          => $txn->recipient->first_name,
            'vw_withdrawal_date'        => Hey::date_to_human($txn->created_at),
            'vw_withdrawal_amount'      => Hey::to_currency(abs($txn->amount_gross)),
            'vw_withdrawal_fail_reason' => $failure,
        ];
        
        $company = Hey::get_app('company_name');
        $subject = $company.' Withdrawal Failure';
        $from = Hey::get_app('email.from');
        $email = ['text' => 'emails.provider.withdrawal-fail-text'];
        
        Mail::send($email, $vw_data, function ($message)
                  use ($txn, $subject, $from, $company)
        {
            $message->from($from, $company);
            $message->to($txn->recipient->email, $txn->recipient->full_name());
            $message->subject($subject);
        });
    }
    
}

