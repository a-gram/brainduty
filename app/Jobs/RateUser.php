<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use App\Models\UserRating;


class RateUser extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * The id of the given rating.
     * 
     * @var type $rating_id
     */
    public $rating_id;
    
    /**
     * The id of the user receiving the rating.
     * 
     * @var type $recipient_id
     */
    public $recipient_id;

    
    /**
     * Create the job.
     *
     * @return void
     */
    public function __construct($rating_id, $recipient_id) {
        $this->rating_id = $rating_id;
        $this->recipient_id = $recipient_id;
    }
    

    /**
     * Handle the job.
     *
     * @return void
     */
    public function handle() {
        
        parent::handle();
        
        // Update the aggregate value of the rating for the recipient user.
        
        $avg = UserRating::where('id_recipient', $this->recipient_id)->avg('score');
        User::where('id', $this->recipient_id)->update(['rating' => $avg ?: 0]);
        
    }
        
}
