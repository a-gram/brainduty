<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use App\Exceptions\AGResourceNotFoundException;
use App\Hey;
use Mail;


class ResetPassword extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * The id of the user.
     * 
     * @var type $user_id
     */
    public $user_id;
    
    /**
     * The user's password reset verification token.
     * 
     * @var string 
     */
    public $user_pr_token;

    /**
     * Create the job.
     *
     * @return void
     */
    public function __construct($user_id, $user_pr_token) {
        $this->user_id = $user_id;
        $this->user_pr_token = $user_pr_token;
    }
    

    /**
     * Handle the job.
     *
     * @return void
     */
    public function handle() {
        
        parent::handle();
        
        $user = User::where('id', $this->user_id)->first();
        
        assert($user);
        
        $vw_data = [
            'vw_user_pr_token'   => $this->user_pr_token,
        ];
        
        $company = Hey::get_app('company_name');
        $subject = $company.' Password Reset';
        $from = Hey::get_app('email.from');
        $email = ['text' => 'emails.password-reset-text'];
        
        Mail::send($email, $vw_data, function ($message) 
                   use ($user, $subject, $from, $company)
        {
            $message->from($from, $company);
            $message->to($user->email);
            $message->subject($subject);
        });
    }
        
}
