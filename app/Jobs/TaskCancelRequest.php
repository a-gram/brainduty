<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Message;
use App\Notifications\TaskCancelExpiredNotification;
use App\Hey;
use App\K;
use DB;


class TaskCancelRequest extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * The id of the task to be cancelled.
     * 
     * @var type $task_id
     */
    public $task_id;
    
    /**
     * The id of the transactional message for the request.
     * 
     * @var unknown
     */
    public $msg_id;
    
    /**
     * The user who requested the cancellation.
     *
     * @var unknown
     */
    public $requestor_id;
    
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($task_id, $msg_id, $requestor_id) {
        $this->task_id = $task_id;
        $this->msg_id = $msg_id;
        $this->requestor_id = $requestor_id;
    }
    

    /**
     * Handle the job.
     *
     * @return void
     */
    public function handle()
    {
        parent::handle();
        
    	//DB::transaction(function() {
    		 
    	    $msg = Message::with('task')->
                            where('id', $this->msg_id)->
                            where('id_users_sender', $this->requestor_id)->
                            first();
            
            assert($msg && $msg->task->id === $this->task_id);
    		
            if($msg->task->status != K::TASK_ONGOING)
               return;
            
            // If the request has expired without a response then notify the
            // requestor, otherwise we'll check just after it expires.
            if($msg->is_request_expired() && !$msg->action_user)
            {
               $notification = new TaskCancelExpiredNotification($msg->task);
               $notification->to($this->requestor_id);
               app('notifier')->push($notification);
            }
            else {
               $later = Hey::dt_to_sec(Hey::get_app('intervals.task.cancel_request_expired'));
               $this->release($later * 1.1);
            }
    	//});
    		 
    }
        
}
