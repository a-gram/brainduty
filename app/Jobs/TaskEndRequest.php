<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Message;
use App\Models\TaskDispute;
use App\Notifications\TaskEndedNotification;
use App\Notifications\TaskDisputedNotification;
use App\Hey;
use App\K;
use DB,Mail;


/**
 * This job checks whether the task end request has expired with no response from
 * the customer and ends the task if so.
 */
class TaskEndRequest extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * The id of the task to be cancelled.
     * 
     * @var type $task_id
     */
    public $task_id;
    
    /**
     * The id of the transactional message for the request.
     * 
     * @var unknown
     */
    public $msg_id;
    
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($task_id, $msg_id) {
        $this->task_id = $task_id;
        $this->msg_id = $msg_id;
    }
    

    /**
     * Handle the job.
     *
     * @return void
     */
    public function handle()
    {
        parent::handle();
        
    	DB::transaction(function() {
    		 
    	    $msg = Message::with(['task' => function ($q){ $q->lockForUpdate(); }])->
                            with(['sender','recipient'])->
                            where('id', $this->msg_id)->
                            first();
            
            assert($msg && $msg->task->id === $this->task_id);
            
            if($msg->task->status != K::TASK_ONGOING)
               return;
            
            // End the task if the request expired unanswered.
            if($msg->is_request_expired() && !$msg->action_user)
            {
               $msg->task->get_manager()
                         ->set_requestor( $msg->recipient )
                         ->end();
               
               $notification = new TaskEndedNotification($msg->task);
               $notification->message = trans('messages.task-end-request-expired',[
                    'task-subject' => $msg->task->subject,
               ]);
               $notification->to($msg->task->id_users_customer);
               app('notifier')->push($notification);
            }
            // If the customer has not taken action and the request is not expired
            // then release the job back into the queue and check just after it expires.
            else {
               $later = Hey::dt_to_sec(Hey::get_app('intervals.task.end_request_expired'));
               $this->release($later * 1.1);
            }
            
            // Send the customer a notice email if this is the first job attempt.
            if($this->attempts() === 1) {
               $this->send_end_notice_email($msg);
            }
            
    	});
    		 
    }
    
    
    /**
     * Email a notice to the customer about the pending request.
     * 
     * @param Message $msg
     */
    protected function send_end_notice_email($msg) {
        
        $vw_data = [
            'vw_customer_name'         => $msg->recipient->first_name,
            'vw_msg_ref'               => $msg->ref,
            'vw_task_ref'              => $msg->task->ref,
            'vw_task_subject'          => $msg->task->subject,
            'vw_request_datetime'      => $msg->created_at,
            'vw_request_expiry_time'   => Hey::dt_to_human(
                                             Hey::get_app('intervals.task.end_request_expired')
                                          ),
        ];
        
        $company = Hey::get_app('company_name');
        $subject = $company.' Notice';
        $from = Hey::get_app('email.from');
        $email = ['text' => 'emails.customer.task-ended-text'];

        Mail::send($email, $vw_data, function ($message)
                   use ($msg, $subject, $from, $company)
        {
            $message->from($from, $company);
            $message->to($msg->recipient->email, $msg->recipient->full_name());
            $message->subject($subject);
        });
    }
    
}
