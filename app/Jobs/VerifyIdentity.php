<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\ProviderIdentity;
use App\Models\Provider;
use App\Exceptions\AGInvalidStateException;
use App\Hey;
use App\K;
use DB,Mail;


class VerifyIdentity extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * The id of the user being verified.
     * 
     * @var type $user_id
     */
    public $user_id;
    

    /**
     * Create the job.
     *
     * @return void
     */
    public function __construct($user_id) {
        $this->user_id = $user_id;
    }
    

    /**
     * Handle the job.
     *
     * @return void
     */
    public function handle() {
        
        parent::handle();
        
        DB::transaction(function() {
            
            $identity = app('identity.manager')->of($this->user_id);
            
            $provider = $identity->get_provider();
            
            // Abort if the provider is cancelled or scheduled for cancellation.
            if($provider->is_cancelled() || $provider->cancelled_at)
               return;
            
            if($provider->identity->is_closed())
               return;
            
            // TODO This job will never be deleted from the queue if (1) the identity
            // verification status is 'verifying' and the user is never verified
            // (2) the status is 'inquired' and the user will abandon the verification.
            // To avoid this, we may set a maximum time allowed for the verification
            // (e.g. 30 days) after which the user will be considered 'unverified'.
            // ...
            
            switch ($provider->identity->status) {
                
                case K::PROVIDER_IDENTITY_SUBMITTED :
                    
                     $identity->verify();
                    
                case K::PROVIDER_IDENTITY_VERIFYING :
                    
                    // If both the plaform and the external service have verified
                    // the provider's identity set it as fully verified.
                    if ($provider->identity->int_verified &&
                        $provider->identity->ext_verified) {
                        
                        $identity->verified();
                        
                        try { $this->send_notice_email($provider, 'success'); }
                        catch (Exception $ex) {}
                        
                        return;
                    }
                    // If the platform has rejected the provider's identity then
                    // set it as fully rejected regardless of the external service.
                    else
                    if ($provider->identity->int_verified === 0) {
                        
                        $identity->unverified();
                        
                        try { $this->send_notice_email($provider, 'fail'); }
                        catch (Exception $ex) {}

                        return;
                    }
                    // If there are required information, inquiry the provider.
                    else
                    if ($provider->identity->info_required) {
                        $reason = 'Provide or correct the following info.'; //$this->get_inquiry_reasons($provider);
                        $identity->inquiry($reason);
                    }
                    break;
                     
            }
            
        });
    }
    
    
    /**
     * Helper method to get the reason for inquirying.
     * 
     * @param type $provider
     * @return string
     */
    protected function get_inquiry_reasons($provider) {
        
        $documents = $provider->identity->get_required_documents();
        
        $reasons = [];
        
        foreach($documents as $document) {
            $reasons[] = $document['name'];
        }
        
        if ($provider->identity->is_info_required(K::IDENTITY_INFO_NAMES)) {
            $reasons[] = 'First/Last name';
        }
        if ($provider->identity->is_info_required(K::IDENTITY_INFO_DOB)) {
            $reasons[] = 'Date of birth';
        }
        if ($provider->identity->is_info_required(K::IDENTITY_INFO_ENTITY)) {
            $reasons[] = 'Entity type';
        }
        if ($provider->identity->is_info_required(K::IDENTITY_INFO_BANK)) {
            $reasons[] = 'Bank account details';
        }
        if ($provider->identity->is_info_required(K::IDENTITY_INFO_ADDRESS)) {
            $reasons[] = 'Address';
        }
        
        return 'Please provide the following: ' . implode(', ', $reasons);
    }
    
    
    /**
     * Email a notice to the provider about the verification outcome.
     * 
     * @param Provider $provider
     * @param string $outcome The outcome of the verification (success|fail)
     */
    protected function send_notice_email($provider, $outcome) {
        
        $vw_data = [
            'vw_provider_name'  => $provider->first_name,
            'vw_failure_reason' => 'NOT AVAILABLE'
        ];
        
        $company = Hey::get_app('company_name');
        $subject = $company.' Account Activation';
        $from = Hey::get_app('email.from');
        
        if($outcome === 'success')
           $email = ['text' => 'emails.provider.identity-verified-text'];
        else
        if($outcome === 'fail')
           $email = ['text' => 'emails.provider.identity-unverified-text'];
        else
           Hey::bug_here ($outcome);

        Mail::send($email, $vw_data, function ($message)
                   use ($provider, $subject, $from, $company)
        {
            $message->from($from, $company);
            $message->to($provider->email, $provider->full_name());
            $message->subject($subject);
        });
    }
    

}
