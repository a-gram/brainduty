<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use App\Exceptions\AGResourceNotFoundException;
use App\Hey;
use Mail;


class WelcomeUser extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * The id of the user.
     * 
     * @var type $user_id
     */
    public $user_id;
    
    /**
     * The user email verification token.
     * 
     * @var string 
     */
    public $user_ev_token;

    /**
     * Create the job.
     *
     * @return void
     */
    public function __construct($user_id, $user_ev_token) {
        $this->user_id = $user_id;
        $this->user_ev_token = $user_ev_token;
    }
    

    /**
     * Handle the job.
     *
     * @return void
     */
    public function handle() {
        
        parent::handle();
        
        $user = User::where('id', $this->user_id)->first();
        
        if(!$user)
           throw new AGResourceNotFoundException('User not found : '.$this->user_id);
        
        $vw_data = [
            'vw_user_first_name' => $user->first_name,
            'vw_user_last_name'  => $user->last_name,
            'vw_user_ev_token'   => $this->user_ev_token,
        ];
                
        $company = Hey::get_app('company_name');
        $subject = $company.' Account Verification';
        $from = Hey::get_app('email.from');
        
        if($user->is_customer())
           $email = 'emails.customer.welcome';
        else if($user->is_provider())
           $email = 'emails.provider.welcome';
        else return;

        Mail::send($email, $vw_data, function ($message)
                   use ($user, $subject, $from, $company)
        {
            $message->from($from, $company);
            $message->to($user->email, $user->full_name());
            $message->subject($subject);
        });
    }
        
}
