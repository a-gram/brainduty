<?php

namespace App;

// I love scope-resolution notation...
abstract class K {

    // Standard HTTP error codes are used in the app.
    const ERROR_NONE = 200;
    const ERROR_BAD_REQUEST = 400;
    const ERROR_UNAUTHENTICATED = 401;
    const ERROR_PAYMENT = 402;
    const ERROR_UNAUTHORIZED = 403;
    const ERROR_NOT_FOUND = 404;
    const ERROR_CONFLICTS = 409;
    const ERROR_UNPROCESSABLE_ENTITY = 422;
    const ERROR_INTERNAL = 500;
    const ERROR_SERVICE_UNAVAILABLE = 503;

    // App users roles
    const ROLE_SYSTEM = 1;
    const ROLE_ADMIN = 2;
    const ROLE_PROVIDER = 4;
    const ROLE_CUSTOMER = 8;
    const ROLE_GUEST = 16;

    // Task status
    const TASK_SCHEDULED = 1;
    const TASK_ONGOING = 2;
    const TASK_ENDED = 3;
    const TASK_CANCELLED = 4;
    const TASK_COMPLETED = 5;
    const TASK_DISPUTED = 6;
    
    // Task assignment policy.
    // The task assignment policy indicates how tasks for this customer are
    // assigned to providers. This can be one of the following:
    // - MANAGED: the task assignment is managed entirely by the platform
    //            according to the customer's preferences. The platform will
    //            match the tasks to suitable providers using some algorithm
    //            and will keep following up trying to assign until the task
    //            expires. Matched providers will receive an 'invitation to
    //            bid' notification.
    // - UNMANAGED: the task assignment is done by the customers who directly
    //            choose the provider and assign the task.
    const TASK_ASSIGNMENT_MANAGED = 1;
    const TASK_ASSIGNMENT_UNMANAGED = 2;

    // Task assignment criteria for managed assignment.
    const TASK_ASSIGN_FIRST_BIDDER = 1;
    const TASK_ASSIGN_CHEAPEST_BIDDER = 2;
    const TASK_ASSIGN_BEST_BIDDER = 4;
    
    // Task cancellation policy.
    // These are the policies applied to unilaterally cancelled tasks and set 
    // by providers.
    // TASK_CANCEL_NO_CHARGE:       No charges are incurred.
    // TASK_CANCEL_CHARGE_SERVICES: All services associated with the task up 
    //                              to the cancellation time are charged.
    // TASK_CANCEL_CHARGE_FEE:      A fixed fee is charged.
    //
    const TASK_CANCEL_POLICY_NO_CHARGE = 1;
    const TASK_CANCEL_POLICY_CHARGE_SERVICES = 2;
    const TASK_CANCEL_POLICY_CHARGE_FEE = 3;

    /*const*/ static $TASK_CANCEL_POLICY = [self::TASK_CANCEL_POLICY_NO_CHARGE,
                                            self::TASK_CANCEL_POLICY_CHARGE_SERVICES,
                                            self::TASK_CANCEL_POLICY_CHARGE_FEE];

    const TASK_DISPUTE_SUBMITTED = 1;
    const TASK_DISPUTE_PROCESSING = 2;
    const TASK_DISPUTE_RESOLVED = 4;

    // Transaction types.
    // Charges are transactions wherein a customer's source of fund (card, bank account,
    // e-wallet, etc.) is debited for services purchased on the platform.
    const TXN_TYPE_CHARGE = 'charge';
    // Payments are transactions wherein funds are moved from the platform's account 
    // to a recipient's account on the platform (typically a provider).
    const TXN_TYPE_PAYMENT = 'payment';
    // Refunds are transactions wherein a charged amount is returned (in full or
    // in part) to the customer.
    const TXN_TYPE_REFUND = 'refund';
    // Transfers are transactions wherein funds are moved from a sender's account
    // on the platform to an external account (bank, e-wallet, etc.) or from/to
    // the platform's account and a user's account to credit/debit.
    const TXN_TYPE_TRANSFER = 'transfer';

    // Transaction statuses.
    // These statuses indicate whether the funds associated with the transaction have
    // affected the account balance (i.e. have been either credited or debited):
    // 
    // PENDING = The funds for the transaction are not cleared and not available
    //           in the balance.
    // COMPLETED = The funds for the transaction are cleared and available in the
    //             balance.
    // CANCELLED = The funds for the transaction will never be available cause the
    //             transaction failed.
    //
    const TXN_PENDING = 'pending';
    const TXN_COMPLETED = 'available';
    const TXN_CANCELLED = 'cancelled';
    
    // Map different statuses wording into our 3 transaction statuses.
    /*const*/ static $TXN_STATUS_MAP = [
        
        'completed'  => self::TXN_COMPLETED,
        'succeeded'  => self::TXN_COMPLETED,
        'available'  => self::TXN_COMPLETED,
        'paid'       => self::TXN_COMPLETED,
        
        'cancelled'  => self::TXN_CANCELLED,
        'failed'     => self::TXN_CANCELLED,
        'aborted'    => self::TXN_CANCELLED,
        
        'pending'    => self::TXN_PENDING,
        'ongoing'    => self::TXN_PENDING,
        'in_transit' => self::TXN_PENDING,
        'processing' => self::TXN_PENDING,
    ];
    
    // Payment cycle types
    const PAYMENT_CYCLE_ONETIME = 1;
    const PAYMENT_CYCLE_RECURRING = 2;

    /*const*/ static $PAYMENT_CYCLE = [
        
         self::PAYMENT_CYCLE_ONETIME,
         self::PAYMENT_CYCLE_RECURRING
    ];
    
    // User status
    // The user is registered with the platform but is not able to
    // use core features or the whole account altogether until the
    // system enables it. This is the state for new users and cannot
    // be taken again once it has changed to one of the below.
    const USER_REGISTERED = 1;
    // The user is going thru a process required to become active. The process
    // depends on the type of user. Account use may be limited.
    // For providers, this is the identity verification process.
    // For customers, this may be a simpler verification process, such as email 
    // verification.
    const USER_ONBOARDING = 2;
    // The user is fully active and able to use all the account functionalities.
    // it is authorized for.
    const USER_ACTIVE = 3;
    // The user willingly deactivates its account (offline mode) and is
    // no longer visible on the platform.
    const USER_INACTIVE = 4;
    // The user account has been cancelled and is no longer accessible.
    const USER_CANCELLED = 5;
    // The user has been put 'on hold' by the system and is no longer able
    // to use core features of the account or the whole account altogether
    // until it takes some required action. Users transiting to this state
    // may incur some form of penalty.
    const USER_SUSPENDED = 6;
    
    const USER_TYPE_INDIVIDUAL = 'individual';
    const USER_TYPE_SOLETRADER = 'soletrader';
    const USER_TYPE_COMPANY = 'company';
    
    /*const*/ static $USER_TYPE = [
        
        self::USER_TYPE_INDIVIDUAL,
        self::USER_TYPE_SOLETRADER,
        self::USER_TYPE_COMPANY
    ];
    
    // Provider identity verification status
    // The provider has submitted all the required information.
    const PROVIDER_IDENTITY_SUBMITTED = 1;
    // The information submitted by the provider is being verified.
    const PROVIDER_IDENTITY_VERIFYING = 2;
    // The submitted information is insufficient and the provider is being
    // asked for further evidence.
    const PROVIDER_IDENTITY_INQUIRED = 3;
    // The provider's identity has been successfully verified.
    const PROVIDER_IDENTITY_VERIFIED = 4;
    // The provider's identity could not be verified.
    const PROVIDER_IDENTITY_UNVERIFIED = 5;
        
    // Task-related message types.
    const MESSAGE_TASK_REQUEST = 1;
    const MESSAGE_TASK_CONVERSATION = 2;
    const MESSAGE_TASK_CHARGE_REQUEST = 3;
    const MESSAGE_TASK_DELIVERABLES = 4;
    const MESSAGE_TASK_CANCEL_REQUEST = 5;
    const MESSAGE_TASK_END_REQUEST = 6;
    
    const DOCUMENT_CATEGORY_IDENTITY = 'identity';

    // Identity document types.
    const IDENTITY_INFO_PHOTOID = 'photoid';
    const IDENTITY_INFO_QUALIFICATIONS = 'qualifications';
    const IDENTITY_INFO_NAMES = 'names';
    const IDENTITY_INFO_BANK = 'bank';
    const IDENTITY_INFO_ADDRESS = 'address';
    const IDENTITY_INFO_DOB = 'dob';
    const IDENTITY_INFO_ENTITY = 'entity';
    const IDENTITY_INFO_BUSINESS = 'business';
    
    // Identity document verifiers.
    const IDENTITY_DOC_VERIFIER_PLATFORM = 'platform';
    const IDENTITY_DOC_VERIFIER_EXTERNAL = 'external';
    

    // Notification types.
    const NOTIFICATION_PLATFORM_EVENT = 100;

    const NOTIFICATION_TASK_EVENT = 200;
    const              TASK_SCHEDULED_EVENT = 201;
    const              TASK_STARTED_EVENT = 202;
    const              TASK_ENDED_EVENT = 203;
    const              TASK_CANCELLED_EVENT = 204;
    const              TASK_COMPLETED_EVENT = 205;
    const              TASK_DISPUTED_EVENT = 206;
    const              TASK_BID_REQUEST_EVENT = 207;
    const              TASK_RESCHEDULED_EVENT = 208;
    const              TASK_CHARGE_ADDED_EVENT = 209;
    const              TASK_CANCEL_REQUEST_ACCEPTED_EVENT = 210;
    const              TASK_CANCEL_REQUEST_REFUSED_EVENT = 211;
    const              TASK_CANCEL_REQUEST_EXPIRED_EVENT = 212;
    const              TASK_END_REQUEST_ACCEPTED_EVENT = 213;
    const              TASK_END_REQUEST_REFUSED_EVENT = 214;
    const              TASK_END_REQUEST_EXPIRED_EVENT = 215;
    const              TASK_DISPUTE_RESOLVED_EVENT = 216;
    const              TASK_BID_REQUEST_EXPIRED_EVENT = 217;
    

    const NOTIFICATION_TRANSACTION_EVENT = 300;
//	const              CHARGE_EVENT = 301;
//	const              CHARGE_ISSUE_EVENT = 302;

    const NOTIFICATION_ACCOUNT_EVENT = 400;
    const              IDENTITY_INQUIRY_EVENT = 401;
    const              IDENTITY_VERIFIED_EVENT = 402;
    const              IDENTITY_UNVERIFIED_EVENT = 403;


    const NOTIFICATION_ALERT_EVENT = 500;

    // Limits
    const MIN_PASSWORD_LENGTH = 8;
    const MAX_CHAT_MESSAGE_LENGTH = 1000;
    const MAX_ATTACHS_PER_MESSAGE = 5;
    const MAX_ATTACHS_SIZE_IN_MB = 3;
    const MAX_PROVIDER_SERVICE_CATEGORIES = 10;
    const MAX_CHARGE_REATTEMPTS = 3;
    const MAX_CHARGE_REQUEST_AMOUNT = 200;
    const MAX_ITEMS_PER_PAGE = 10;
    const MAX_THUMBNAIL_WIDTH = 125;
    const MAX_THUMBNAIL_HEIGHT = 125;
    
    // The date-time formats used by the database.
    const DB_DATETIME_FORMAT = 'Y-m-d H:i:s';
    const DB_DATE_FORMAT = 'Y-m-d';
    const DB_TIME_FORMAT = 'H:i:s';
    
    // Other
    const RESERVED_USER_IDS = 1000;
    const MASK_PLACEHOLDER = '*';
    const DEFAULT_AVATAR_IMAGE_URI = 'img/def-avatar.png';
    const MSG_ERROR_GENERIC = 'Unexpected issues occurred.';
    
    // Some useful mime type mappings used in the app.
    /* const */ static $FILE_TYPE_TO_MIME_MAP = [
         'png'   => 'image/png',
         'jpg'   => 'image/jpeg',
         'jpeg'  => 'image/jpeg',
         'gif'   => 'image/gif',
         'pdf'   => 'application/pdf',
         'xml'   => 'application/xml',
         'json'  => 'application/json',
         'js'    => 'application/javascript',
         'doc'   => 'application/msword',
         'csv'   => 'text/csv',
         'txt'   => 'text/plain',
         'htm'   => 'text/html',
         'html'  => 'text/html',
         'bin'   => 'application/octet-stream',
         'zip'   => 'application/zip',
    ];

    
    
    ///////////////////////// VIEWS STUFFS ////////////////////////////
    
    
    /*const*/ static $TXN_STATUS_ICON = [
	'pending'   => 'fa-minus-circle',
	'available' => 'fa-check-circle',
        'cancelled' => 'fa-times-circle',
    ];
        
}
