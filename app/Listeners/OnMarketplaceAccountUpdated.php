<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\EventMarketplaceAccountUpdated;


class OnMarketplaceAccountUpdated //implements ShouldQueue
{
    //use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
    }

    /**
     * Handle the event.
     *
     * @param  EventMarketplaceAccountUpdated  $event
     * @return void
     */
    public function handle(EventMarketplaceAccountUpdated $event) {
        
        dispatch( (new \App\Jobs\MarketplaceAccountUpdate ($event->event_id, 
                                                           $event->account_id))->
                   onConnection('system')->
                   onQueue('account') );
    }
    
}
