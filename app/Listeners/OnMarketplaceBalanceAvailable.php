<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\EventMarketplaceBalanceAvailable;


class OnMarketplaceBalanceAvailable //implements ShouldQueue
{
    //use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
    }

    /**
     * Handle the event.
     *
     * @param  EventMarketplaceBalanceAvailable  $event
     * @return void
     */
    public function handle(EventMarketplaceBalanceAvailable $event) {
        
        dispatch( (new \App\Jobs\MarketplaceBalanceAvailable ($event->event_id, 
                                                              $event->account_id))->
                   onConnection('system')->
                   onQueue('account') );
    }
    
}
