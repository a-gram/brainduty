<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\EventMarketplaceWithdrawalCreated;


class OnMarketplaceWithdrawalCreated //implements ShouldQueue
{
    //use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
    }

    /**
     * Handle the event.
     *
     * @param  EventMarketplaceWithdrawalCreated  $event
     * @return void
     */
    public function handle(EventMarketplaceWithdrawalCreated $event) {
        
        dispatch( (new \App\Jobs\MarketplaceWithdrawalCreated ($event->event_id, 
                                                               $event->account_id))->
                   onConnection('system')->
                   onQueue('account') );
    }
    
}
