<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\EventMarketplaceWithdrawalDeposited;


class OnMarketplaceWithdrawalDeposited //implements ShouldQueue
{
    //use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
    }

    /**
     * Handle the event.
     *
     * @param  EventMarketplaceWithdrawalDeposited  $event
     * @return void
     */
    public function handle(EventMarketplaceWithdrawalDeposited $event) {
        
        dispatch( (new \App\Jobs\MarketplaceWithdrawalDeposited ($event->event_id, 
                                                                 $event->account_id))->
                   onConnection('system')->
                   onQueue('account') );
    }
    
}
