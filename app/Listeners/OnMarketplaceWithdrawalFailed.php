<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\EventMarketplaceWithdrawalFailed;


class OnMarketplaceWithdrawalFailed //implements ShouldQueue
{
    //use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
    }

    /**
     * Handle the event.
     *
     * @param  EventMarketplaceWithdrawalFailed  $event
     * @return void
     */
    public function handle(EventMarketplaceWithdrawalFailed $event) {
        
        dispatch( (new \App\Jobs\MarketplaceWithdrawalFailed ($event->event_id, 
                                                              $event->account_id))->
                   onConnection('system')->
                   onQueue('account') );
    }
    
}
