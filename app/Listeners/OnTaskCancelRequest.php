<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\EventTaskCancelRequest;
use App\Hey;


/**
 * Event handler called whenever a task cancel request is sent. 
 * 
 * @author Albert
 *
 */
class OnTaskCancelRequest //implements ShouldQueue
{
    //use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
    }

    /**
     * Handle the event.
     *
     * @param  EventTaskCancelRequest  $event
     * @return void
     */
    public function handle(EventTaskCancelRequest $event) {
        
        dispatch(new \App\Jobs\TaskCancelRequest($event->task_id,
                                                 $event->msg_id,
                                                 $event->requestor_id));
        
    }
    
}
