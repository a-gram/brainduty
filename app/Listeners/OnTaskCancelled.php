<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\EventTaskCancelled;


/**
 * Event handler called whenever a task is cancelled. Takes care of any operation
 * that must be performed upon task cancellation, such as charging cancellation
 * fees if any, notifying parties, etc.
 * 
 * @author Albert
 *
 */
class OnTaskCancelled //implements ShouldQueue
{
    //use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
    }

    /**
     * Handle the event.
     *
     * @param  EventTaskCancelled  $event
     * @return void
     */
    public function handle(EventTaskCancelled $event) {
        
        dispatch(new \App\Jobs\CancelTask($event->task_id,
                                          $event->task_status,
                                          $event->requestor_id));
        
    }
    
}
