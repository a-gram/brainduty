<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\EventTaskCompleted;


class OnTaskCompleted //implements ShouldQueue
{
    //use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
    }

    /**
     * Handle the event.
     *
     * @param  EventTaskCompleted  $event
     * @return void
     */
    public function handle(EventTaskCompleted $event) {
        
        
    }
        
}
