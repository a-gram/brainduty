<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\EventTaskEndRequest;
use App\Hey;


/**
 * Event handler called whenever a task end request is sent. 
 * 
 * @author Albert
 *
 */
class OnTaskEndRequest //implements ShouldQueue
{
    //use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
    }

    /**
     * Handle the event.
     *
     * @param  EventTaskEndRequest  $event
     * @return void
     */
    public function handle(EventTaskEndRequest $event) {
        
        dispatch(new \App\Jobs\TaskEndRequest($event->task_id,
                                              $event->msg_id));
        
    }
    
}
