<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\EventTaskEnded;


class OnTaskEnded //implements ShouldQueue
{
    //use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
    }

    /**
     * Handle the event.
     *
     * @param  EventTaskEnded  $event
     * @return void
     */
    public function handle(EventTaskEnded $event) {
        
        dispatch(new \App\Jobs\EndTask($event->task_id));
    }
        
}
