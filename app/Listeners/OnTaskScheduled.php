<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\EventTaskScheduled;


class OnTaskScheduled //implements ShouldQueue
{
    //use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
    }

    /**
     * Handle the event.
     *
     * @param  EventTaskScheduled  $event
     * @return void
     */
    public function handle(EventTaskScheduled $event) {
        
        dispatch(new \App\Jobs\AssignTask ($event->task_id));
    }
    
}
