<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\EventUserCancelled;


class OnUserCancelled //implements ShouldQueue
{
    //use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
    }

    /**
     * Handle the event.
     *
     * @param  EventUserCancelled  $event
     * @return void
     */
    public function handle(EventUserCancelled $event) {
        
        dispatch( (new \App\Jobs\CancelUser ($event->user_id))->
                   onConnection('system')->
                   onQueue('account') );
    }
    
}
