<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\EventUserIdentityVerification;


class OnUserIdentityVerification //implements ShouldQueue
{
    //use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
    }

    /**
     * Handle the event.
     *
     * @param  EventUserIdentityVerification  $event
     * @return void
     */
    public function handle(EventUserIdentityVerification $event) {
        
        dispatch( (new \App\Jobs\VerifyIdentity ($event->user_id))->
                   onConnection('system')->
                   onQueue('account') );
    }
    
}
