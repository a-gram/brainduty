<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\EventUserRating;


class OnUserRating //implements ShouldQueue
{
    //use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
    }

    /**
     * Handle the event.
     *
     * @param  EventUserRating  $event
     * @return void
     */
    public function handle(EventUserRating $event) {
        
        dispatch( (new \App\Jobs\RateUser ($event->rating_id, $event->recipient_id))->
                   onConnection('system') );
    }
    
}
