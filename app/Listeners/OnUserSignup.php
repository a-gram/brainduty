<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\EventUserSignup;


class OnUserSignup //implements ShouldQueue
{
    //use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
    }

    /**
     * Handle the event.
     *
     * @param  EventUserSignup  $event
     * @return void
     */
    public function handle(EventUserSignup $event) {
        
        dispatch( (new \App\Jobs\WelcomeUser ($event->user_id,
                                              $event->user_ev_token))->
                   onConnection('system')->
                   onQueue('account') );
    }
    
}
