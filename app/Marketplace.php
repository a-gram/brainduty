<?php

namespace App;

use App\Interfaces\IMarketplaceProvider;
use App\Exceptions\AGPaymentException;
use App\Exceptions\AGTransferException;
use App\Exceptions\AGMarketplaceException;
use App\Exceptions\AGException;
use App\Models\Transaction;
use App\Hey;
use App\K;
use Config,Log;
use Stripe;

/**
 *  This class is an implementation of the IMarketplaceProvider interface. It
 *  takes care of all typical operations in a marketplace such as creating,
 *  deleting and updating merchant accounts, making transfers, charging and
 *  refunding customers, etc. Generally, most (if not all) of these functionality
 *  are provided by a marketplace provider (Stripe, Braintree, Paypal, etc.) but
 *  in some cases it may be necessary to integrate additional functionality not
 *  provided by a single service, so this class may use different third party
 *  services to accomplish its tasks.
 *
 */
class Marketplace implements IMarketplaceProvider {
    
	
    // Maps stripe's document category names to internal names.
    public static $DOCUMENT_CATEGORIES_MAP = [
        
        'business_logo'             => '',
        'dispute_evidence'          => 'dispute',
        'identity_document'         => 'identity',
        'incorporation_article'     => '',
        'incorporation_document'    => '',
        'invoice_statement'         => '',
        'payment_provider_transfer' => '',
        'product_feed'              => '',
    ];
        
    // Maps platform entity types to stripe's names.
    public static $ENTITY_TYPE_MAP = [
        
        'individual' => 'individual',
        'soletrader' => 'individual',
        'company'    => 'company',
    ];
    
    //
    public static $REQUIRED_INFO_MAP = [
        
        'legal_entity.first_name'            => K::IDENTITY_INFO_NAMES,
        'legal_entity.last_name'             => K::IDENTITY_INFO_NAMES,
        'legal_entity.address.line1'         => K::IDENTITY_INFO_ADDRESS,
        'legal_entity.address.postal_code'   => K::IDENTITY_INFO_ADDRESS,
        'legal_entity.address.city'          => K::IDENTITY_INFO_ADDRESS,
        'legal_entity.address.state'         => K::IDENTITY_INFO_ADDRESS,
        'legal_entity.dob.day'               => K::IDENTITY_INFO_DOB,
        'legal_entity.dob.month'             => K::IDENTITY_INFO_DOB,
        'legal_entity.dob.year'              => K::IDENTITY_INFO_DOB,
        'legal_entity.type'                  => K::IDENTITY_INFO_ENTITY,
        'legal_entity.verification.document' => K::IDENTITY_INFO_PHOTOID,
        'external_account'                   => K::IDENTITY_INFO_BANK,
    ];
    

    public function __construct() {
        Stripe\Stripe::setApiKey( Config::get('services.stripe.skey') );
    }

    /**
     * Creates a payout account for the specified provider.
     * 
     * This method calls the marketplace provider API to create a payout 'Recipient'
     * or 'Account' or whatever it's called by the service provider. Currently it's
     * calling the Pin Payment create recipient API.
     *
     * @param array $merchant Contains the provider's info with payout details.
     * @return array A record with recipient/account details, as for provider's response.
     * This record is used to extract information to be stored in the database.
     * @throws AGMarketplaceException on error.
     */
    public function create_merchant_account(array $merchant) {
        
        try {

            $params = [
                'managed' => true,
                'country' => Hey::country_to_iso3166($merchant['country']),
                'email'   => $merchant['email'],
                'legal_entity' => [
                    'type' => self::$ENTITY_TYPE_MAP[ $merchant['entity_type'] ],
                    'first_name' => $merchant['first_name'],
                    'last_name' => $merchant['last_name'],
                    'address' => [
                       'line1'       => $merchant['address'],
                       'city'        => $merchant['city'],
                       'postal_code' => $merchant['zip_code'],
                       'state'       => $merchant['state'],
                       'country'     => Hey::country_to_iso3166($merchant['country'])
                    ],
                    'dob' => [
                       'day'   => \date_create($merchant['dob'])->format('j'),
                       'month' => \date_create($merchant['dob'])->format('n'),
                       'year'  => \date_create($merchant['dob'])->format('Y'),
                    ],
                    'business_name' => $merchant['business_name'],
                    'business_tax_id' => $merchant['business_number'],
                ],
                'external_account' => Hey::getval($merchant, 'payout.account_id'), //<-token
                'transfer_schedule' => [
                    'delay_days' => Hey::get_app('payout.delay'),
                    'interval'   => Hey::get_app('payout.interval'),
                ],
                'tos_acceptance' => [
                    'date' => Hey::now_to_timestamp(),
                    'ip'   => $merchant['ip'],
                ],
                'metadata' => [
                    'user_id' => $merchant['id'],
                ],
            ];

            // Create a merchant account on stripe.
            $account = Stripe\Account::create($params);

            // Determine if the account needs more info for verification.
            $fields_needed = array_map(function($info) {
                return self::$REQUIRED_INFO_MAP[$info];
            },
            $account->verification->fields_needed);
            $info_required = array_unique($fields_needed);

            $account_record = [
                'id'                => $account->id,
                'charges_enabled'   => $account->charges_enabled,
                'transfers_enabled' => $account->transfers_enabled,
                'is_verified'       => $account->legal_entity->verification->status == 'verified',
                'info_required'     => $info_required,
            ];
            
            if($params['external_account']) {
               $payout = $account->external_accounts->data[0];
               $account_record['payout'] = [
                    'account_name'   => $payout->account_holder_name,
                    'account_id'     => $payout->id,
                    'account_number' => $payout->routing_number.' *****'.
                                        $payout->last4,
                    'account_host'   => $payout->bank_name,
               ];
            }
            return $account_record;
        }
        catch (\Stripe\Error\InvalidRequest $e) {
            throw new AGMarketplaceException($e->getMessage());
        } catch (\Exception $e) {
			Log::error($merchant);
            throw $e;
        }
    }

    /**
     * Updates a payout account for the specified provider.
     *
     * @param array $merchant Contains the provider's account info to be updated.
     * @return array A record with recipient/account details, as for provider's response.
     * This record is used to extract information to be stored in the database.
     * @throws AGMarketplaceException on error.
     */
    public function update_merchant_account(array $merchant, $action) {

        try {
			
            if($action === 'payout.replace' || $action === 'payout.add') {
                
               if($action === 'payout.replace') {
                  $account = Stripe\Account::update($merchant['ref_ext'], [
                     'external_account' => $merchant['payout']['account_id'], //<-token
                  ]);
                  $payout = $account->external_accounts->data[0];
               } else {
                  $account = Stripe\Account::retrieve($merchant['ref_ext']);
                  $payout = $account->external_accounts->create([
                     'external_account' => $merchant['payout']['account_id'], //<-token
                  ]);
               }
               
               $payout_record = [
                    'account_name'   => $payout->account_holder_name,
                    'account_id'     => $payout->id,
                    'account_number' => $payout->routing_number.' *****'.
                                        $payout->last4,
                    'account_host'   => $payout->bank_name,
               ];
            }
            else
            if($action === '*.*') {
               
                $params = [
                    'email'   => $merchant['email'],
                    'legal_entity' => [
                        'type' => self::$ENTITY_TYPE_MAP[ $merchant['entity_type'] ],
                        'first_name' => $merchant['first_name'],
                        'last_name' => $merchant['last_name'],
                        'address' => [
                           'line1'       => $merchant['address'],
                           'city'        => $merchant['city'],
                           'postal_code' => $merchant['zip_code'],
                           'state'       => $merchant['state'],
                           'country'     => Hey::country_to_iso3166($merchant['country'])
                        ],
                        'dob' => [
                           'day'   => \date_create($merchant['dob'])->format('j'),
                           'month' => \date_create($merchant['dob'])->format('n'),
                           'year'  => \date_create($merchant['dob'])->format('Y'),
                        ],
                        'business_name' => $merchant['business_name'],
                        'business_tax_id' => $merchant['business_number'],
                    ],
                    //'external_account' => Hey::getval($merchant, 'payout.account_id'), //<-token
                    'transfer_schedule' => [
                        'delay_days' => Hey::get_app('payout.delay'),
                        'interval'   => Hey::get_app('payout.interval'),
                    ],
                ];
                
                if(isset($merchant['payout'])) {
                   $params['external_account'] = $merchant['payout']['account_id']; //<-token
                }
                
                // Create a merchant account on stripe.
                $account = Stripe\Account::update($merchant['ref_ext'], $params);
                
                // Determine if the account needs more info for verification.
                $fields_needed = array_map(function($info) {
                    return self::$REQUIRED_INFO_MAP[$info];
                },
                $account->verification->fields_needed);
                $info_required = array_unique($fields_needed);

                $account_record = [
                    'id'                => $account->id,
                    'charges_enabled'   => $account->charges_enabled,
                    'transfers_enabled' => $account->transfers_enabled,
                    'is_verified'       => $account->legal_entity->verification->status == 'verified',
                    'info_required'     => $info_required,
                ];

                if(isset($params['external_account'])) {
                   $payout = $account->external_accounts->data[0];
                   $account_record['payout'] = [
                        'account_name'   => $payout->account_holder_name,
                        'account_id'     => $payout->id,
                        'account_number' => $payout->routing_number.' *****'.
                                            $payout->last4,
                        'account_host'   => $payout->bank_name,
                   ];
                }
                return $account_record;
            }
            else {
               Hey::bug_here($action);
            }

            return $payout_record;
        }
        catch (\Stripe\Error\InvalidRequest $e) {
            throw new AGMarketplaceException($e->getMessage());
        }
        catch (\Exception $e) {
            throw $e;
        }

    }
    
    
    /**
     * 
     * @param array $merchant
     * @return array
     */
    public function retrieve_merchant_balance(array $merchant) {
        
        return $this->retrieve_balance($merchant['ref_ext']);
    }
    
    
    /**
     * 
     * @param array $merchant
     * @param array $params
     * @return array
     */
    public function retrieve_merchant_transactions(array $merchant,
                                                   array $params = null) {
        /*
        
        $txns = Stripe\BalanceTransaction::all(
             ['limit' => 2],
             ['stripe_account' => $merchant['ref_ext'] ]
        );
        
        $txn_list = [];
        
        return $txn_list;
         */
    }
    
    
    /**
     * 
     * @param array $merchant
     * @param string $txn_id
     * @return type
     */
    public function retrieve_merchant_transaction(array $merchant, $txn_id) {
        /*        
        $txn = Stripe\BalanceTransaction::retrieve ($txn_id, [
                 'stripe_account' => $merchant['ref_ext'],
                 'expand' => ['source'],
             ]
        );
        
        $fees = 0; $taxes = 0;
        
        foreach ($txn->fee_details as $fee) {
            if($fee->type == 'tax') $taxes += $fee->amount;
            else                    $fees += $fee->amount;
        }
        
        $txn_record = [
            'amount_gross'   => $txn->amount,
            'amount_fee'     => $fees,
            'amount_tax'     => $taxes,
            'amount_bal'     => $txn->net,
            'type'           => $txn->type,
            'status'         => $txn->status,
            'settle_status'  => $txn->source->status,
            'is_captured'    => $txn->source->captured,
            'clear_datetime' => Hey::timestamp_to_db_datetime($txn->available_on),
        ];
        return $txn_record;
         */
    }
    

    /**
     * Deletes a payout account for the specified provider.
     *
     * @param array $merchant Contains the provider's account info to be deleted.
     * @return array A record with recipient/account details, as for provider's response.
     * This record is used to extract information to be stored in the database.
     * @throws AGMarketplaceException on error.
     */
    public function delete_merchant_account(array $merchant) {
        
        $account = Stripe\Account::retrieve($merchant['ref_ext'])->delete();
        
        assert($account->id === $merchant['ref_ext']);
        
        $deleted_record = [
            'id'      => $account->id,
            'deleted' => $account->deleted,
        ];
        return $deleted_record;
    }


    /**
     * Creates a payment account for the specified customer.
     *
     * This method calls the marketplace provider API to create a 'Customer' or whatever
     * it's called by the marketplace provider, with associated vaulted cards.
     *
     * @param array $customer Contains the customer and payment method details.
     * @return array A record with customer/payment details, as for provider's response.
     * This record is used to extract information to be stored in the database.
     * @throws AGMarketplaceException on error.
     */
    public function create_customer_account(array $customer) {

        $params = [
            'email'   => $customer['email'],
            'source'  => $customer['payment']['source_id'], //<-token
            'metadata' => [
                'user_id' => $customer['id'],
                'user_ip' => $customer['ip'],
            ],
        ];
        
        try {
            // Create a customer account on stripe.
            $account = Stripe\Customer::create($params);

            $payment = $account->sources->data[0];

            $account_record = [
                'id'                => $account->id,
                'payment' => [
                    'source_name'    => $payment->name,
                    'source_id'      => $payment->id,
                    'source_number'  => '*******'.$payment->last4,
                    'source_expiry'  => $payment->exp_month.'/'.$payment->exp_year,
                    'source_type'    => $payment->brand,
                ],
            ];
            return $account_record;
        }
        catch (\Stripe\Error\Card $e) {
            throw new AGMarketplaceException($e->getMessage());
        }
        catch (\Stripe\Error\InvalidRequest $e) {
            throw new AGMarketplaceException($e->getMessage());
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Updates a payment account for the specified customer.
     *
     * This method calls the marketplace provider API to update a 'Customer', or whatever
     * it's called, to update the account.
     *
     * @param array $customer Contains the customer and payment method details.
     * @param string $action  The updating action to be performed on the account.
     * @return array A record with customer/payment details, as for provider's response.
     * This record is used to extract information to be stored in the database.
     * @throws AGMarketplaceException on error.
     */
    public function update_customer_account(array $customer, $action) {

        try {
			
            if($action === 'payment.replace') {
               
               $account = Stripe\Customer::update($customer['ref_ext'], [
                  'source' => $customer['payment']['source_id'], //<-token
               ]);

               $payment = $account->sources->data[0];

               $payment_record = [
                  'source_name'    => $payment->name,
                  'source_id'      => $payment->id,
                  'source_number'  => '*******'.$payment->last4,
                  'source_expiry'  => $payment->exp_month.'/'.$payment->exp_year,
                  'source_type'    => $payment->brand,
               ];
            }
            else {
                Hey::bug_here($action);
            }

            return $payment_record;
        }
        catch (\Stripe\Error\Card $e) {
            throw new AGMarketplaceException($e->getMessage());
        }
        catch (\Stripe\Error\InvalidRequest $e) {
            throw new AGMarketplaceException($e->getMessage());
        }
        catch (\Exception $e) {
            throw $e;
        }
    }
    
    
    /**
     * 
     * @param array $customer
     */
    public function retrieve_customer_balance(array $customer) {
        
        // TODO There is a Customer.account_balance property ...
        
        return [
            'pending'   => 0,
            'available' => 0,
        ];
    }
    

    /**
     * Deletes a payment account for the specified customer.
     *
     * @param string $action  The updating action to be performed on the account.
     * @param array $customer Contains the customer and payment method details.
     * @throws AGMarketplaceException on error.
     */
    public function delete_customer_account(array $customer) {
        
        $account = Stripe\Customer::retrieve($customer['ref_ext'])->delete();
        
        assert($account->id === $customer['ref_ext']);
        
        $deleted_record = [
            'id'      => $account->id,
            'deleted' => $account->deleted,
        ];
        return $deleted_record;
    }
    
    
    /**
     * Transfer an amount of money from/to an account.
     * 
     * @param array $funds Details about the funds to be transferred.
     */
    public function transfer(array $funds) {

        $transfer_type = $funds['transfer_type'];

        // Transfer funds from the recipient's account to an external account.
        if($transfer_type === 'withdrawal') {

            try {
                $transfer = Stripe\Transfer::create([
                     'amount'      => $funds['transaction']['amount_gross'],
                     'currency'    => $funds['transaction']['currency'],
                     'destination' => 'default_for_currency',
                     'metadata'    => [
                                        //'txn_ref' => $funds['transaction']['ref'],
                                      ],

                     'expand' => ['balance_transaction'],
                  ],
                  [  'stripe_account' => $funds['provider']['ref_ext']  ]
                );
                
                $txn = $transfer->balance_transaction;
                
                $fees = 0; $taxes = 0;

                foreach ($txn->fee_details as $fee) {
                    if($fee->type == 'tax') $taxes += $fee->amount;
                    else                    $fees += $fee->amount;
                }

                $txn_record = [
                    'amount_gross'   => $txn->amount,
                    'amount_fee'     => $fees,
                    'amount_tax'     => $taxes,
                    'amount_bal'     => $txn->net,
                    'type'           => $txn->type,
                    'status'         => $txn->status,
                    'settle_status'  => $transfer->status,
                    'clear_datetime' => Hey::timestamp_to_db_datetime($txn->available_on),
                    'ref_ext'        => $txn->id,
                    'ref_ext_source' => $transfer->id,
                    'error'          => $transfer->failure_message,
                ];
                return $txn_record;
            }
            // Catch and propagate transfer errors as internal transfer exceptions.
            catch(\Stripe\Error\Base $e) {
                $response = $e->getJsonBody();
                throw new AGTransferException( $response['error']['message'],
                                               $response['error'],
                                               $e );
            }
            // For anything else, log the transfer if it went through, then propagate
            catch(\Exception $e) {
                throw new AGException('ERROR', isset($transfer) ? $transfer : null, $e);
            }
        }
        else
        // Transfer funds from the platform's account to the recipient's account.
        if($transfer_type === 'credit') {
            // TODO ?
        }
        else {
           Hey::bug_here($transfer_type);
        }
    }


    /**
     * Execute payments. Make money.
     *
     * @param array $charge An array containing the details necessary to execute the
     *        payment. This array has the following structure:
     *        
     *        [
     *           'transaction' =>  [ <transaction details> ]
     *           'customer'    =>  [ <customer's details>
     *                               'payment' => [ <payment method details> ]
     *                             ]
     *           'provider'    =>  [ <provider's details>
     *                               'payout' => [ <payout account details> ]
     *                             ]
     *        ]
     *
     *         NOTE: CHARGE DETAILS MUST BE ALREADY VALIDATED BY
     *               THE MODELS. NO CHECKS ARE DONE HERE.
     *               
     * @return array A payment record with details about the transaction.
     * @throws Payment exceptions on error.
     */
    public function bill(array $charge) {

        $payment_method = $charge['customer']['payment']['method'];

        // Charge a card. This is currently done by Pin Payment.
        if($payment_method === 'card') {
            
            try {
                $payment = Stripe\Charge::create([
                    'amount'          => $charge['transaction']['amount_gross'],
                    'application_fee' => $charge['transaction']['amount_fee'],
                    'currency'        => $charge['transaction']['currency'],
                    'capture'         => $charge['transaction']['is_captured'],
                    'customer'        => $charge['customer']['ref_ext'],
                    'destination'     => $charge['provider']['ref_ext'],
                    'metadata'        => [
                                           'txn_ref' => $charge['transaction']['ref'],
                                         ],
                    // NOTE: The transaction of interest is the one associated with
                    //       the payment to the connected account.
                    'expand' => ['transfer.destination_payment.balance_transaction'],
                ]);
            
                assert($payment->customer == $charge['customer']['ref_ext']);
                assert($payment->amount == $charge['transaction']['amount_gross']);
                assert($payment->destination == $charge['provider']['ref_ext']);
                assert($payment->source->id == $charge['customer']['payment']['source_id']);

                $txn = $payment->transfer->destination_payment->balance_transaction;
                
                $fees = 0; $taxes = 0;
                
                foreach ($txn->fee_details as $fee) {
                    if($fee->type == 'tax') $taxes += $fee->amount;
                    else                    $fees += $fee->amount;
                }

                $txn_record = [
                    'amount_gross'   => $txn->amount,
                    'amount_fee'     => $fees>0 ? -$fees : $fees,
                    'amount_tax'     => $taxes>0 ? -$taxes : $taxes,
                    'amount_bal'     => $txn->net,
                    'type'           => $txn->type,
                    'status'         => $txn->status,
                    'settle_status'  => $payment->status,
                    'is_captured'    => $payment->captured,
                    'clear_datetime' => Hey::timestamp_to_db_datetime($txn->available_on),
                    'ref_ext'        => $txn->id,
                    'ref_ext_source' => $payment->id,
                    'error'          => $payment->failure_message,
                ];

                return $txn_record;
            }
            // Catch and propagate card errors as internal payment exceptions.
            catch(\Stripe\Error\Card $e) {
                $response = $e->getJsonBody();
                throw new AGPaymentException( $response['error']['message'],
                                              $response['error'],
                                              $e );
            }
            // For anything else, log the payment if it went through, then propagate
            catch(\Exception $e) {
                throw new AGException('ERROR', isset($payment) ? $payment : null, $e);
            }
        }
        else
        // Charge a paypal account.
        if($payment_method === 'paypal') {
            // Here we may use a PayPal interface to charge PayPal accounts.
            // ...
        }
        else {
           Hey::bug_here($payment_method);
        }
    }

    
    /**
     * Refund a charged amount.
     * 
     * @param array $charge
     * @return array A refund record with details about the transaction.
     * @throws AGPaymentException
     * @throws AGMarketplaceException
     */
    public function refund(array $charge) {
        
        $payment_method = $charge['transaction']['settle_method'];

        // Refund a charged card.
        if($payment_method === 'card') {
            
            try {
                $refund = Stripe\Refund::create([
                    'charge'                 => $charge['transaction']['ref_ext_source'],
                    'amount'                 => $charge['refund']['amount_gross'],
                    'refund_application_fee' => $charge['params']['refund_app_fee'],
                    'reverse_transfer'       => $charge['params']['reverse_payment'],
                    'reason'                 => Hey::getval($charge, 'params.reason'),
                    'metadata'               => [
                                                  'txn_ref' => $charge['refund']['ref'],
                                                ],
                    // NOTE: The transaction of interest is the one associated with
                    //       the refund in the connected account.
                    'expand' => ['charge.transfer.destination_payment.application_fee'],
                ]);
            
                assert($refund->amount == $charge['refund']['amount_gross']);
                assert($refund->charge->id == $charge['transaction']['ref_ext_source']);
                assert($refund->reason == Hey::getval($charge, 'params.reason'));
                
                // The refund object returned refers to the original charge made
                // on the platform. We are interested in the refund transaction in
                // the provider account (the refund on the provider payment we made).
                $provider_payment = $refund->charge->transfer->destination_payment;
                
                // Get the provider payment refund and the app fee refund (if any).
                if(count($provider_payment->refunds->data) === 1) {
                   
                   $provider_refund = $provider_payment->refunds->data[0];
                   
                   if(count($provider_payment->application_fee->refunds->data) === 1) {
                      // Apparently, there is no way to get the refund the provider receives 
                      // for the app fee as a payment from here (webhooks?).
                   }
                   
                   $txn = \Stripe\BalanceTransaction::retrieve (
                             $provider_refund->balance_transaction, [
                             'stripe_account' => $charge['provider']['ref_ext']
                          ]);
                   
                   $fees = 0; $taxes = 0;
                   
                   foreach ($txn->fee_details as $fee) {
                      if($fee->type == 'tax') $taxes += $fee->amount;
                      else                    $fees += $fee->amount;
                   }
                }
                else
                if(count($provider_payment->refunds->data) > 1) {
                   Hey::bug_here('Multiple refunds not supported.');
                } else {
                   Hey::bug_here('No refund record');
                }

                $txn_record = [
                    'amount_gross'   => $txn->amount,
                    'amount_fee'     => $fees,
                    'amount_tax'     => $taxes,
                    'amount_bal'     => $txn->net,
                    //'type'           => $txn->type,
                    'status'         => $txn->status,
                    'settle_status'  => $provider_refund->status,
                    'clear_datetime' => Hey::timestamp_to_db_datetime($txn->available_on),
                    'ref_ext'        => $txn->id,
                    'ref_ext_source' => $provider_refund->id,
                    'error'          => null,
                ];
                
                return $txn_record;
            }
            // Catch and propagate refund errors as internal payment exceptions.
            catch(\Stripe\Error\Base $e) {
                $response = $e->getJsonBody();
                throw new AGPaymentException( $response['error']['message'],
                                              $response['error'],
                                              $e );
            }
            // For anything else, log the refund if it went through, then propagate
            catch(\Exception $e) {
                throw new AGException('ERROR', isset($refund) ? $refund : null, $e);
            }
        }
        else
        // Refund a paypal account.
        if($payment_method === 'paypal') {
            // Here we may use a PayPal interface to refund PayPal accounts.
            // ...
        }
        else {
           Hey::bug_here($payment_method);
        }
    }
    
    
    /**
     * 
     * @param array $document
     */
    public function store_merchant_document(array $document) {
                
        // Upload the document files.
        // NOTE: This is actually done client side so we already have
        //       the tokens for the documents.
        
        // Link/associate the uploaded document with the merchant account.
        //$account = Stripe\Account::retrieve($document['provider']['ref_ext']);
        //$account->legal_entity->verification->document = $document['ref_ext'];
        //$account_updated = $account->save();
        
        $account = Stripe\Account::update($document['provider']['ref_ext'], [
             'legal_entity' => [
                 'verification' => [
                      'document' => $document['ref_ext']
                 ]
             ]
        ]);
        
        assert($account->legal_entity->verification->document == $document['ref_ext']);
        
        // Retrieve the stored document info
        $stored_doc = Stripe\FileUpload::retrieve($document['ref_ext']);
        
        assert($stored_doc->id == $document['ref_ext']);
        
        $stored_doc_record = [
            'category'      => self::$DOCUMENT_CATEGORIES_MAP[$stored_doc->purpose],
            'filename'      => Hey::getval($stored_doc, 'filename'),
            'filename_orig' => Hey::getval($document, 'filename'),
            'mime'          => K::$FILE_TYPE_TO_MIME_MAP[$stored_doc->type],
            'size'          => $stored_doc->size,
            'ref_ext'       => $stored_doc->id,
            'created_at'    => $stored_doc->created,
        ];
        
        return $stored_doc_record;
    }
    
    
    /**
     * This method determines if a failed charge can be reattempted.
     * (see https://stripe.com/docs/declines )
     * 
     * @param array $reason
     */
    public function should_retry_charge(array $reason) {
        
        if($reason['decline_code'] === 'fraudulent')
           return false;
        else
           return true;
    }
    
    
    /**
     * Retrieve the marketplace platform's balance.
     * 
     * @return type
     */
    public function retrieve_market_balance() {
        return $this->retrieve_balance();
    }


    // HELPERS
    
    
    /**
     * Retrieve a balance.
     * 
     * @param string $account If supplied, this is a string indicating the id of
     * the marketplace account for which to retrieve the balance, otherwise the 
     * marketplace platform's account balance will be retrieved.
     * @return array
     */
    protected function retrieve_balance($account = null) {
        
        $params = $account ? ['stripe_account' => $account] : null;
        
        $balance = Stripe\Balance::retrieve($params);
        
        $balance_record = [
            'available' => 0,
            'pending'   => 0,
        ];
        
        $currency = strtolower( Hey::get_app('currency') );
        
        foreach($balance->available as $available ) {
            if($available->currency == $currency) {
               $balance_record['available'] += $available->amount;
               break;
            }
        }
        foreach($balance->pending as $pending ) {
            if($pending->currency == $currency) {
               $balance_record['pending'] += $pending->amount;
               break;
            }
        }
        
        return $balance_record;
    }
    
}

