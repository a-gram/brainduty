<?php

namespace App\Messages\Actions;

use App\Interfaces\IMessageAction;
use App\Models\Message;
use App\Models\User;
use App\Exceptions\AGValidationException;
use App;


/**
 * The base class for all transactional message actions. A message action
 * is like a 'call to action' that a user receiving the message is supposed
 * to take.
 * 
 * @author Albert
 *
 */
abstract class MessageAction implements IMessageAction {

	/**
	 * The transactional message to which this action(s) is associated.
	 * 
	 * @var Message
	 */
	protected $message;
	
	/**
	 * The actions associated with this transactional message type.
	 * Currently an associative array action => Closure, but may be
	 * something else.
	 * 
	 * @var array
	 */
	protected $actions = null;
	
	/**
	 * The user executing the action.
	 * 
	 * @var unknown
	 */
	protected $user = null;
	
	/**
	 * The notifier instance.
	 * 
	 * @var Notifier
	 */
	protected $notifier;
	
	
	/**
	 * Construct a new message action.
	 * 
	 * @param Message $message The transactional message to
	 * which this action(s) is associated.
	 */
	public function __construct(Message $message) {
		$this->message = $message;
		$this->notifier = app('notifier');
		$this->set_actions();
	}
	
	
	/**
	 * Save the requesting user for execution-time verifications.
	 *
	 * @param User $user The user executing the action.
	 * @return MessageAction This instance, for chainability.
	 * @throws AGAuthorizationException
	 */
	public function by(User $user) {
		$this->user = $user;
		return $this;
	}
	
	
	/**
	 * Execute action. Does pretty much little here. Just checks if the given
	 * action is actually supported. Concrete classes will extend its behaviour.
	 *
	 * @param string $action The action to be execute for the specified
	 * transactional message.
	 * @param array $params Optional additional parameters passed to
	 * the action.
	 * @return array A results set specific for this action, if any.
	 */
	public function execute($action, array $params = null) {
		$this->is_action_supported($action);
		return [];
	}
	
	
	/**
	 * The supported message actions. Concrete classes will implement the
	 * specific actions for a specific message by defining them in the $actions
	 * property.
	 */
	abstract protected function set_actions();
	
	
	/**
	 * Check whether the given action is supported. Assumes the $action property
	 * is an associative array [action => Closure]. If that's not the case, then
	 * must be reimplemented in concrete classes.
	 * 
	 * @throws AGValidationException
	 */
	protected function is_action_supported($action) {
		if(is_array($this->actions) && !array_key_exists($action, $this->actions))
			throw new AGValidationException('Invalid action : '.$action);
	}
	
}
