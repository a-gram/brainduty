<?php

namespace App\Messages\Actions;

use App\Models\Message;
use App\Messages\Actions\TaskChargeRequestMessageAction;
use App\Messages\Actions\TaskCancelRequestMessageAction;
use App\Messages\Actions\TaskEndRequestMessageAction;
use App\K;

/**
 * This class maps the given (transactional) message to its corresponding action(s).
 * It is currently bound to the service container as a singleton.
 */
class MessageActionMap {

    /**
     * The message => action map.
     * 
     * @var unknown
     */
    protected $map;


    public function __construct() {

        $this->map = [

            K::MESSAGE_TASK_CHARGE_REQUEST => function ($message) { 
                return new TaskChargeRequestMessageAction($message); 
            },

            K::MESSAGE_TASK_CANCEL_REQUEST => function ($message) {
                return new TaskCancelRequestMessageAction($message);
            },

            K::MESSAGE_TASK_END_REQUEST => function ($message) {
                return new TaskEndRequestMessageAction($message);
            },

            // More actions here...
        ];
    }

    /**
     * Maps the given transactional message to its associated action(s).
     * 
     * @param Message $message
     * @param MessageAction
     */
    public function for_this(Message $message) {
         return $this->map[$message->type]($message);
    }
	
}
