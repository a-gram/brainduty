<?php

namespace App\Messages\Actions;

use App\Messages\Actions\MessageAction;
use App\Events\EventTaskCancelled;
use App\Exceptions\AGException;
use App\Exceptions\AGValidationException;
use App\Exceptions\AGAuthorizationException;
use App\Notifications\TaskCancelAcceptedNotification;
use App\Notifications\TaskCancelRefusedNotification;
use App\Notifier;
use App\Models\User;
use App\Hey;
use App\K;
use DB, Event;


/**
 * The actions that are associated with a task cancel request transactional
 * message.
 *
 * @author Albert
 *
 */
class TaskCancelRequestMessageAction extends MessageAction {
	
    /**
     * Execute action.
     * 
     * @param string $action = accept
     * @param array $params = null
     * @return array No results.
     */
    public function execute($action, array $params = null) {

        parent::execute($action);

        // Verify that the requesting user is authorized to act on the task.
        if(!$this->message->task->has_actor($this->user->id))
            throw new AGAuthorizationException;

        // Verify that the requesting user is the legit recipient
        if($this->user->id != $this->message->id_users_recipient)
            throw new AGAuthorizationException;

        return $this->actions[$action]($params);
    }


    /**
     * The supported message actions.
     */
    protected function set_actions() {

        $this->actions = [

            'accept' => function (array $params) {

                 $message = $this->message;

                 $requestor = $message->task->id_users_customer.'&'.
                              $message->task->id_users_provider;

                 // This event will be fired by the task lifecycle manager
                 $event = new EventTaskCancelled($message->task->id,
                                                 $message->task->status,
                                                 $requestor);

                 $message->task->get_manager()
                               ->set_requestor( $this->user )
                               ->set_event( $event );
                 
                 $message->action_user = 'accept';
                 $message->action_datetime = Hey::now_to_db_datetime();
                 $message->is_txn_completed = true;

                 // Cancel the task and close the transaction
                 DB::transaction(function() use ($message) {
                    
                    $message->task->get_manager()
                            ->cancel($message->task->ref);
                    
                    $message->save();
                 });

                     // Notify the other party
                 $notification = new TaskCancelAcceptedNotification($message->task);
                 $notification->to( $this->user->id == $message->id_users_sender ?
                                                       $message->id_users_recipient :
                                                       $message->id_users_sender);
                 
                 $this->notifier->push($notification);

                 return [];
            },
            
            
            'refuse' => function (array $params) {
                
                 $message = $this->message;
                 
                 $message->action_user = 'refuse';
                 $message->action_datetime = Hey::now_to_db_datetime();
                 $message->is_txn_completed = true;

                 // Mark the request as refused and close the transaction.
//                 DB::transaction(function() use ($message) {
                    $message->save();
//                 });

                 // Notify the other party
                 $notification = new TaskCancelRefusedNotification($message->task);
                 $notification->to( $this->user->id == $message->id_users_sender ?
                                                       $message->id_users_recipient :
                                                       $message->id_users_sender);
                 
                 $this->notifier->push($notification);

                 return [];
            },

        ];
    }

}
