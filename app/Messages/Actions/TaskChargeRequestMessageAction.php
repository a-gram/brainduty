<?php

namespace App\Messages\Actions;

use App\Messages\Actions\MessageAction;
use App\Exceptions\AGException;
use App\Exceptions\AGValidationException;
use App\Exceptions\AGAuthorizationException;
use App\Notifications\TaskChargeApprovedNotification;
use App\Notifications\TaskChargeRefusedNotification;
use App\Notifier;
use App\Models\User;
use App\Hey;
use App\K;
use DB;


/**
 * The actions that are associated with a task charge request transactional
 * message.
 *
 * @author Albert
 *
 */
class TaskChargeRequestMessageAction extends MessageAction {

	/**
	 * Execute action.
	 * 
	 * @param string $action = approve|refuse
	 * @param array $params = [
	 *                           'token'  => security_token
	 *                        ]
	 * @return array Approve returns the added extra service,
	 * Refuse returns no results;
	 */
	public function execute($action, array $params = null) {
		
		parent::execute($action);
		
		if(empty($params) || !isset($params['token']))
			throw new AGException('Missing parameter : token');
				
		// Verify that the received security token is valid.
		$bite = $this->message->data.Hey::get_app('key');
		$token = hash(Hey::get_app('hash_algo'), $bite);
				
		if($params['token'] != $token)
			throw new AGAuthorizationException;
					
		// Verify that the requesting user is the legit recipient customer
		if($this->user->id != $this->message->id_users_recipient ||
          !$this->user->is_customer())
           throw new AGAuthorizationException;
						
		return $this->actions[$action]($params);
	}
	
	
	/**
	 * The supported message actions.
	 */
	protected function set_actions() {

            $this->actions = [
			
                'approve' => function (array $params) {

                    $message = $this->message;

                    // Attach the extra service charge to the task.
                    // NOTE: the following data has already been validated when the
                    //       message was created.
                    $charge = json_decode($message->data);

                    $service[$charge->service] = [
                        'amount'      => $charge->amount,
                        'description' => $charge->description,
                        'id_messages' => $message->id,
                    ];

                    $message->action_user = 'approve';
                    $message->action_datetime = Hey::now_to_db_datetime();
                    $message->is_txn_completed = true;

                    // Attach the service and mark the transaction as completed.
                    DB::transaction(function() use ($message, $service) {
                        $message->task->services()->attach($service);
                        $message->save();
                    });

                    unset($service[$charge->service]['id_messages']);

                             // Notify provider
                    $notification = new TaskChargeApprovedNotification($message->task);
                    $notification->data = json_encode($service[$charge->service]);
                    $notification->to($message->id_users_sender);

                    $this->notifier->push($notification);

                    return $service[$charge->service];
		},
			
			
                'refuse'  => function (array $params) {

                    $message = $this->message;

                        // Mark the transaction as completed and notify the provider.
                    $message->action_user = 'refuse';
                    $message->action_datetime = Hey::now_to_db_datetime();
                    $message->is_txn_completed = true;
                    $message->save();

                    // Notify provider
                    $notification = new TaskChargeRefusedNotification($message->task);
                    $notification->to($message->id_users_sender);

                    $this->notifier->push($notification);

                    return [];
                },
			
	    ];
	}

}
