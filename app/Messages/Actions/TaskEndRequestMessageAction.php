<?php

namespace App\Messages\Actions;

use App\Messages\Actions\MessageAction;
use App\Events\EventTaskEnded;
use App\Exceptions\AGException;
use App\Exceptions\AGValidationException;
use App\Exceptions\AGAuthorizationException;
use App\Notifications\TaskEndAcceptedNotification;
use App\Notifications\TaskEndRefusedNotification;
use App\Notifier;
use App\Models\TaskDispute;
use App\Hey;
use App\K;
use DB, Event;


/**
 * The actions that are associated with a task end request transactional
 * message.
 *
 * @author Albert
 *
 */
class TaskEndRequestMessageAction extends MessageAction {
	
    /**
     * Execute action.
     * 
     * @param string $action = accept
     * @param array $params = null
     * @return array No results.
     */
    public function execute($action, array $params = null) {

        parent::execute($action);

        // Verify that the requesting user is authorized to act on the task.
        if(!$this->message->task->has_actor($this->user->id))
            throw new AGAuthorizationException;

        // Verify that the requesting user is the legit recipient.
        if($this->user->id != $this->message->id_users_recipient ||
          !$this->user->is_customer())
           throw new AGAuthorizationException;

        return $this->actions[$action]($params);
    }


    /**
     * The supported message actions.
     */
    protected function set_actions() {

        $this->actions = [

            'accept' => function (array $params) {

                 $message = $this->message;
                 
                 $message->action_user = 'accept';
                 $message->action_datetime = Hey::now_to_db_datetime();
                 $message->is_txn_completed = true;

                 // End the task and close the transaction
                 DB::transaction(function() use ($message) {
                     
                    $message->task->get_manager()
                                  ->set_requestor( $this->user )
                                  ->end( $message->task->ref );
                    
                    $message->save();
                 });
                 
                 // Notify the provider
                 $notification = new TaskEndAcceptedNotification($message->task);
                 $notification->to($message->task->id_users_provider);
                 
                 $this->notifier->push($notification);
                 
                 return [];
            },
            
            
            'dispute' => function (array $params) {
                
                 $message = $this->message;
                 
                 $message->action_user = 'dispute';
                 $message->action_datetime = Hey::now_to_db_datetime();
                 $message->is_txn_completed = true;
                 
                 $dispute = new TaskDispute;
                 $dispute->reason = 0;
                 $dispute->details = trans('messages.task-dispute-on-end');
                 $dispute->status = K::TASK_DISPUTE_SUBMITTED;

                 // Dispute the task and close the transaction.
                 DB::transaction(function() use ($message, $dispute) {
                    
                    $task = app('task.manager')->of($message->task->ref)->get_task();
                    
                    // Task must transit to the ended state in order to be disputed.
                    $task->status = K::TASK_ENDED;
                    
                    $task->get_manager()
                         ->set_requestor( $this->user )
                         ->dispute();

                    $message->save();
                    $message->task->dispute()->save($dispute);
                 });
                 
                 // Notify the provider
                 $notification = new TaskEndRefusedNotification($message->task);
                 $notification->to($message->task->id_users_provider);
                 
                 $this->notifier->push($notification);

                 return [];
            },

        ];
    }

}
