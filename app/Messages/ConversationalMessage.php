<?php

namespace App\Messages;

use App\Models\Message;
use App\K;
use App\Exceptions\AGException;
use App\Exceptions\AGInvalidStateException;
use Exception, Throwable;
use DB;

/**
 * A standard chat message.
 *
 * @author Albert
 *
 */
class ConversationalMessage extends Message {

    public function __construct(array $request = null) {
        parent::__construct();
        
        $this->type = K::MESSAGE_TASK_CONVERSATION;

        if($request) {
           $this->process($request);
        }
    }


    /*
     *   Request data:
     *
     *   [
     *      message => [
     *           type => <message_type>,
     *           body => <message_body>,
     *      ],
     *
     *      sender => <message_sender>,
     *      task   => <task_instance>
     *   ]
     *
     */
    public function process(array $request) {

        // A task is required to post this task.
        if(!isset($request['task']))
           throw new AGException('Field task is not set.');

        // This message type can only be posted to ongoing tasks.
        if($request['task']->status != K::TASK_ONGOING)
           throw new AGInvalidStateException('Cannot post messages for this task.');

        // The recipient is any actor of this task that is not the sender :)
        $recipient_id = $request['sender']->id === $request['task']->id_users_customer ?
                                                   $request['task']->id_users_provider :
                                                   $request['task']->id_users_customer;					

        $this->ref = Message::make_ref($this);
        $this->body = $request['message']['body'];
        $this->id_users_sender = $request['sender']->id;
        $this->id_users_recipient = $recipient_id;

        $this->validate();

        $attachments = [];

        try {
            DB::beginTransaction();

            $attachments = $request['task']->store_files($request['message']['attachments']);

            $request['task']->messages()->save($this);
            $this->attachments()->saveMany($attachments);

            $this->add('attachments', $attachments);

            DB::commit();
        }
        catch (Exception $exc) {
            DB::rollBack();
            $request['task']->rollback_archive($attachments);
            throw $exc;
        }
        catch (Throwable $exc) {
            DB::rollBack();
            $request['task']->rollback_archive($attachments);
            throw $exc;
        }

    }
	
}
