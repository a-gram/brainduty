<?php

namespace App\Messages;

use App\Messages\TaskRequestMessage;
use App\Messages\ConversationalMessage;
use App\Messages\TaskChargeRequestMessage;
use App\Messages\TaskCancelRequestMessage;
use App\Messages\TaskDeliverablesMessage;
use App\Messages\TaskEndRequestMessage;
use App\K;

/**
 * This class implements factory methods to appropriately create messages
 * based on request parameters. It will be managed by the service container
 * as a singleton and injiected wherever needed using the app service provider.
 * 
 * @author Albert
 *
 */
class MessageFactory {

    /**
     * The factory makers.
     * 
     * @var unknown
     */
    protected $make;


    public function __construct() {

        $this->make = [

            K::MESSAGE_TASK_REQUEST => function ($request) {
                return new TaskRequestMessage($request);
            },

            K::MESSAGE_TASK_CONVERSATION => function ($request) {
                return new ConversationalMessage($request);
            },

            K::MESSAGE_TASK_CHARGE_REQUEST => function ($request) {
                return new TaskChargeRequestMessage($request);
            },

            K::MESSAGE_TASK_CANCEL_REQUEST => function ($request) {
                return new TaskCancelRequestMessage($request);
            },

            K::MESSAGE_TASK_DELIVERABLES => function ($request) {
                return new TaskDeliverablesMessage($request);
            },

            K::MESSAGE_TASK_END_REQUEST => function ($request) {
                return new TaskEndRequestMessage($request);
            },

            // More types here...
         ];
    }


    /**
     * Make a new message from the given request parameters.
     * 
     * @param array $request
     * @return Message
     */
    public function from(&$request) {
        return $this->make[$request['message']['type']]($request);
    }
    
}
