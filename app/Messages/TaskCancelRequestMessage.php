<?php

namespace App\Messages;

use App\Models\Message;
use App\Models\Service;
use App\K;
use App\Hey;
use App\Exceptions\AGException;
use App\Exceptions\AGValidationException;
use App\Exceptions\AGInvalidStateException;
use App\Events\EventTaskCancelRequest;
use DB,Event;

/**
 * Transactional message.
 * 
 * This class models a transactional message that creates a request from a user
 * (provider or customer) to cancel a task in mutual agreement without incurring
 * any task cancellation fees/charges.
 * 
 * @author Albert
 *
 */
class TaskCancelRequestMessage extends TransactionalMessage {

	
    public function __construct(array $request = null) {
    	parent::__construct(K::MESSAGE_TASK_CANCEL_REQUEST);
    	
    	if($request) {
    	   $this->process($request);
    	}
    }
    
    /*
     *   Request data:
     *   
     *   [
     *      message => [
     *           type => <message_type>,
     *           body => <message_body>,
     *      ],
     *      
     *      sender => <message_sender>,
     *      task   => <task_instance>
     *   ]
     *   
     *   Available actions:
     *   
     *   accept | POST /message/{msg_ref}/action/accept => []
     *   refuse | POST /message/{msg_ref}/action/refuse => []
     */
    public function process(array $request) {
    	
    	assert($request['task']);
    	
        // This message type can only be posted to ongoing tasks.
        if($request['task']->status != K::TASK_ONGOING)
           throw new AGInvalidStateException('Cannot post messages for this task.');
   		
        // Check whether there is already a cancel request for the task. If so, block
        // any further request until action for the pending one is taken. If the cancel
        // request was sent by this user and it expired (the other part has not responded)
        // then advise this user that the task can be cancelled with no penalties, 
        // so long as the other party does not respond.
        $msg = $request['task']->get_pending_cancel_request();
        
        if($msg && $msg->is_request_expired() &&
           $msg->id_users_sender === $request['sender']->id )
           throw new AGInvalidStateException(trans('exceptions.task-cancel-request-expired'));
        
        if($msg)
           throw new AGInvalidStateException(trans('exceptions.task-cancel-request-pending'));

        // The recipient is any actor of this task that is not the sender :)
        $recipient_id = $request['sender']->id === $request['task']->id_users_customer ?
                                                   $request['task']->id_users_provider :
                                                   $request['task']->id_users_customer;
            
    	$msg_ref = Message::make_ref($this);
    	$msg_action = '/message/'.$msg_ref.'/action/';
    	$msg_body = trans('messages.chat-cancel-request', [
            'message' =>  $request['message']['body']
        ]);
    	
    	$this->ref = $msg_ref; 
    	$this->body = $msg_body;
    	$this->action_names = 'accept|refuse';
    	$this->action_method = $msg_action;
    	$this->id_users_sender = $request['sender']->id;
    	$this->id_users_recipient = $recipient_id;
    	
    	$this->validate();
    	
    	DB::transaction(function() use ($request) {
            $request['task']->messages()->save($this);
            Event::fire(new EventTaskCancelRequest($this->id_tasks, 
                                                   $this->id, 
                                                   $this->id_users_sender));
    	});
        
    }
    
}
