<?php

namespace App\Messages;

use App\Models\Message;
use App\Models\Service;
use App\K;
use App\Hey;
use App\Exceptions\AGException;
use App\Exceptions\AGValidationException;
use App\Exceptions\AGInvalidStateException;
use App\Exceptions\AGResourceNotFoundException;
use DB,URL;

/**
 * Transactional message.
 * 
 * This class models a transactional message that creates a request from a
 * provider to charge a customer with an extra service for the specified task.
 * 
 * @author Albert
 *
 */
class TaskChargeRequestMessage extends TransactionalMessage {

	
    public function __construct(array $request = null) {
    	parent::__construct(K::MESSAGE_TASK_CHARGE_REQUEST);
    	
    	if($request) {
    	   $this->process($request);
    	}
    }
    
    /*
     *   Request data:
     *   
     *   [
     *      message => [
     *           type => <message_type>,
     *           body => <message_body>,
     *           data => [
     *               service     => <service_id>,
     *               description => <service_description>,
     *               amount      => <amount_to_be_charged>
     *           ]
     *      ],
     *      
     *      sender => <message_sender>,
     *      task   => <task_instance>
     *   ]
     *   
     *   Available actions:
     *   
     *   approve | POST /message/{msg_ref}/action/approve => message.data
     *   refuse  | POST /message/{msg_ref}/action/refuse => message.data
     */
    public function process(array $request) {
    	
    	assert($request['task']);
    	
        // This message type can only be posted to ongoing tasks.
        if($request['task']->status != K::TASK_ONGOING)
           throw new AGInvalidStateException('Cannot post messages for this task.');

        // This message can only be posted by providers
        if(!$request['sender']->is_provider())
           throw new AGAuthorizationException;
                
        // Check whether there is already a charge request for the task. If so, block
        // any further request until action for the pending one is taken.
        $msg = $request['task']->get_pending_charge_request();
        
        if($msg)
           throw new AGInvalidStateException(trans('exceptions.task-charge-request-pending'));
    	 
    	// Message data must be present.
    	if(!isset($request['message']['data']))
    	   throw new AGValidationException('Field message.data is not set.');
    	
    	$charge = $request['message']['data'];
    	
    	// Validate message data
    	if(!Hey::has_field(['service','description','amount'], $charge, 'all'))
           throw new AGValidationException('Missing fields in message data');
    	
        $service = Service::find($charge['service']);
        
        if(!$service)
           throw new AGResourceNotFoundException('Service not found.', $charge['service']);
        
    	if(strlen($charge['description']) > 50)
           throw new AGValidationException('Charge description max length is 50 characters.');
    	
        if(!is_numeric($charge['amount']) || $charge['amount'] <= 0)
           throw new AGValidationException('Invalid charge amount.');
        
        // Set a limit on the amount that may be charged
        if($charge['amount'] > K::MAX_CHARGE_REQUEST_AMOUNT*100)
           throw new AGValidationException('The charge amount limit is $'.K::MAX_CHARGE_REQUEST_AMOUNT);
        
        // The recipient is the customer
        $recipient_id = $request['task']->id_users_customer;
            
        $data_json = json_encode($request['message']['data']);
        
    	// Create a security token/fingerprint for this request
    	// f = hash(data,key);
    	$bite = $data_json.Hey::get_app('key');
    	
    	$token = hash(Hey::get_app('hash_algo'), $bite);
    	
    	$msg_ref = Message::make_ref($this);
    	$msg_action = '/message/'.$msg_ref.'/action/';
    	$msg_action_params = '?token='.$token;
    	//$msg_action_url = $msg_action.$msg_action_params;
    	$msg_body = trans('messages.chat-charge-request',[
            'amount'      => Hey::to_currency($charge['amount']),
            'description' => $charge['description'],
            'message'     => $request['message']['body'],
        ]);
        
    	$this->ref = $msg_ref; 
    	$this->body = $msg_body;
    	$this->action_names = 'approve|refuse';
    	$this->action_method = $msg_action;
    	$this->action_params = $msg_action_params;
    	$this->data = $data_json;
    	$this->id_users_sender = $request['sender']->id;
    	$this->id_users_recipient = $recipient_id;
    	
    	$this->validate();
    	
    	//DB::transaction(function() use ($request) {
            $request['task']->messages()->save($this);
    	//});
        
    }
    
}
