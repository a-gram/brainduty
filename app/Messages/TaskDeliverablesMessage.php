<?php

namespace App\Messages;

use App\Models\Message;
use App\Models\Service;
use App\K;
use App\Hey;
use App\Exceptions\AGException;
use App\Exceptions\AGValidationException;
use App\Exceptions\AGInvalidStateException;
use Exception, Throwable;
use App,DB,URL;

/**
 * Transactional message.
 * 
 * This class models a transactional message that ends a task and
 * makes deliverables available to the customer, if any. It does not
 * require any action from the recipient (the customer).
 * 
 * @author Albert
 *
 */
class TaskDeliverablesMessage extends TransactionalMessage {

	
    public function __construct(array $request = null) {
    	parent::__construct(K::MESSAGE_TASK_DELIVERABLES);
    	
    	if($request) {
    	   $this->process($request);
    	}
    }
    
    /*
     *   Request data:
     *   
     *   [
     *      message => [
     *           type => <message_type>,
     *           body => <message_body>,
     *      ],
     *      
     *      sender => <message_sender>,
     *      task   => <task_instance>
     *   ]
     *   
     *   Available actions:
     *   
     *   None
     */
    public function process(array $request) {
    	
        assert($request['task']);

        // This message type can only be posted to ongoing tasks.
        if($request['task']->status != K::TASK_ONGOING)
           throw new AGInvalidStateException('Cannot post messages for this task.');

        // This message can only be posted by providers
        if(!$request['sender']->is_provider())
           throw new AGAuthorizationException;
        
        // The task cannot be ended if there are unanswered cancellation requests.
        if($request['task']->get_pending_cancel_request())
           throw new AGInvalidStateException(trans('exceptions.task-end-cancel-request-pending'));
        
        // The recipient is the customer
        $recipient_id = $request['task']->id_users_customer;

        $msg_body = trans('messages.chat-deliverables', [
             'message'  =>  $request['message']['body']
        ]);
        
        $this->ref = Message::make_ref($this);
        $this->body = $msg_body;
        $this->is_deliverable = false;
        $this->id_users_sender = $request['sender']->id;
        $this->id_users_recipient = $recipient_id;

        $this->validate();

        $attachments = [];

        try {
            DB::beginTransaction();

            // Flag the task as being ended so that we can stop any further posting.
            // TODO This should probably be a new state in the task's FSM.
            $request['task']->is_ending = true;
            
            // Create the end request message that will be sent to the customer.
            $request['message']['type'] = K::MESSAGE_TASK_END_REQUEST;
            $end_message = App::make('message')->from($request);

            $attachments = $request['task']->store_files($request['message']['attachments']);

            $request['task']->messages()->save($this);
            $this->attachments()->saveMany($attachments);
            $request['task']->save();

            $this->add('attachments', $attachments);

            DB::commit();
        }
        catch (Exception $exc) {
            DB::rollBack();
            $request['task']->rollback_archive($attachments);
            throw $exc;
        }
        catch (Throwable $exc) {
            DB::rollBack();
            $request['task']->rollback_archive($attachments);
            throw $exc;
        }
    }
    
}
