<?php

namespace App\Messages;

use App\Models\Message;
use App\Models\Service;
use App\K;
use App\Hey;
use App\Exceptions\AGException;
use App\Exceptions\AGValidationException;
use App\Exceptions\AGInvalidStateException;
use App\Events\EventTaskEndRequest;
use DB,Event;

/**
 * Transactional message.
 * 
 * This class models a transactional message that creates a request from a provider
 * to end a task.
 * 
 * @author Albert
 *
 */
class TaskEndRequestMessage extends TransactionalMessage {

	
    public function __construct(array $request = null) {
    	parent::__construct(K::MESSAGE_TASK_END_REQUEST);
    	
    	if($request) {
    	   $this->process($request);
    	}
    }
    
    /*
     *   Request data:
     *   
     *   [
     *      message => [
     *           type => <message_type>,
     *           body => <message_body>,
     *      ],
     *      
     *      sender => <message_sender>,
     *      task   => <task_instance>
     *   ]
     *   
     *   Available actions:
     *   
     *   accept  | POST /message/{msg_ref}/action/accept => []
     *   dispute | POST /message/{msg_ref}/action/dispute => []
     */
    public function process(array $request) {
    	
    	assert($request['task']);
    	
        // This message type can only be posted to ongoing tasks.
        if($request['task']->status != K::TASK_ONGOING)
           throw new AGInvalidStateException('Cannot post messages for this task.');
        
        // This message can only be created by providers
        if(!$request['sender']->is_provider())
           throw new AGAuthorizationException;
        
        // The recipient is the customer
        $recipient_id = $request['task']->id_users_customer;
        
    	$msg_ref = Message::make_ref($this);
    	$msg_action = '/message/'.$msg_ref.'/action/';
    	$msg_body = trans('messages.chat-end-request');
    	
    	$this->ref = $msg_ref; 
    	$this->body = $msg_body;
    	$this->action_names = 'accept|dispute';
    	$this->action_method = $msg_action;
    	$this->id_users_sender = $request['sender']->id;
    	$this->id_users_recipient = $recipient_id;
    	
    	$this->validate();
    	
        // NOTE: Currently end request messages are already wrapped in a transaction
        // in TaskDeliverablesMessage::process(), so we don't need one here.
        // 
    	//DB::transaction(function() use ($request) {
            $request['task']->messages()->save($this);
            Event::fire(new EventTaskEndRequest($this->id_tasks, 
                                                $this->id));
    	//});
        
    }
    
}
