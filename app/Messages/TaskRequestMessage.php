<?php

namespace App\Messages;

use App\Models\Message;
use App\Models\Service;
use App\Models\Task;
use App\Models\User;
use App\Exceptions\AGException;
use App\Exceptions\AGValidationException;
use App\Exceptions\AGInvalidStateException;
use App\K;
use App\Hey;
use App\Utils\Validate;
use App,DB,URL,Event;

/**
 * Transactional message.
 * 
 * This class models a transactional message that creates and schedules
 * a new task in the system upon customer request.
 * 
 * @author Albert
 *
 */
class TaskRequestMessage extends TransactionalMessage {
	
    
    public function __construct(array $request = null) {
    	parent::__construct(K::MESSAGE_TASK_REQUEST);
    	 
    	if($request) {
           $this->process($request);
    	}
    }
    

    /*
     *   Request data:
     *   
     *   [
     *      message => [
     *           type => <message_type>,
     *           body => <task description>,
     *           data => [
     *               type      => <task service type>,
     *               catags    => <task service category tags>,
     *               subject   => <task subject>
     *               assignee  => <task assignee provider>
     *           ],
     *           attachments => <task document files>
     *      ],
     *      
     *      task   => null (if new) | Task (if rescheduled),
     *      sender => <message_sender>,
     *   ]
     *   
     *   Available actions:
     *
     *   None
     */
    public function process(array $request) {
    	
    	// This message can only be posted by customers
    	if(!$request['sender']->is_customer())
    		throw new AGAuthorizationException;
            	
    	// Message data must be present.
    	if(!isset($request['message']['data']))
    	   throw new AGValidationException('Field message.data is not set.');
    	       	
        $this->ref = Message::make_ref($this);
        $this->body = $request['message']['body'];
        $this->data = json_encode($request['message']['data']);
        $this->id_users_sender = $request['sender']->id;
        $this->id_users_recipient = User::SYSTEM;
       	$this->is_delivered = true;
       	$this->is_seen = true;
       	
        $this->validate();

        // Schedule the task
        $task_request = [
            
            'task'  => [
                'type'      => $request['message']['data']['type'],
                'catags'    => $request['message']['data']['catags'],
                'subject'   => $request['message']['data']['subject'],
                'assignee'  => $request['message']['data']['assignee'],
                'attachs'   => $request['message']['attachments'],
            ],
            'sender' => $request['sender'],
            'rescheduled' => $request['task'],
            'message' => $this,
        ];

        App::make('task.manager')->schedule($task_request);
		
    }

}
