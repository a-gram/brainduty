<?php

namespace App\Messages;

use App\Models\Message;


/**
 * A transactional message is a chat/conversational message that gives rise
 * to business transactions, such as creating resources, authorizing charges,
 * consuming services, etc. A transactional message may be associated with
 * some action(s) that must be taken by the other party in order to execute
 * the transactions, however this is not required and the transactions may
 * be triggered as a result of just posting the message.
 * It is an extension of the basic chat message, which is merely an exchange
 * of text between the parties and is the heart of the Conversational Transaction
 * System.
 * 
 * @author Albert
 *
 */
abstract class TransactionalMessage extends Message {

	//protected $message;
	
	
    public function __construct($type) {
    	$this->/*message->*/type = $type;
    }
    
    /**
     * This method processes the request data in order to create the message
     * and execute the relative transactions, if any. Must be reimplemented
     * by concrete classes for their specific processing needs.
     * 
     * @param array $request The array of request parameters and other data
     * necessary to process the message and perform the transactions. It has
     * the following structure:
     *   
     *     [ 
     *        message => <message request parameters>,
     *        task    => <instance of the task to which the message belongs>,
     *        sender  => <instance of the user sending the request>
     *     ]
     */
    abstract public function process(array $request);
        
}
