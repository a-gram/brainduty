<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Exceptions\AGException;
use App\Hey;

/**
 * The Godfather of all app's entities.
 * 
 * @author Albert
 *
 */
abstract class AGEntity extends Model {

    /**
     * Data attached to this entity that are not part of the model.
     * 
     * @var array
     */
    protected $m_data = [];
    
    /**
	 *  This method checks the current entity's instance making sure its fields
	 *  are valid and consistent according to the app's business rules. All
	 *  entities must implement and provide one.
	 */
	abstract public function validate();
        
        
        public static function get_table_name() {
            return with(new static)->getTable();
        }
        
        
        /**
         * Get the specified attached data from this entity.
         * 
         * @param string $name
         * @return mixed|null
         */
        public function get_data($name) {
            return array_key_exists($name, $this->m_data) ? 
                   $this->m_data[$name] : null;
        }
        
        
        /**
         * Attach data to this entity.
         * 
         * @param type $name
         * @param type $value
         * @return this
         */
        public function set_data($name, $value) {
            $this->m_data[$name] = $value;
            return $this;
        }
        

        /**
	 * Add model(s) to a one-to-one or one-to-many relationship.
	 *
	 * @param string $relation The relation to which to add the model(s).
	 * @param boolean $icheck Flag indicating whether to perform integrity checks.
	 * @param Model $models The model(s) to be added.
	 */
	public function add($relation, $models, $icheck = true) {
		// Get the relation info
        $r = $this->$relation();
        $f_key = $r->getPlainForeignKey();
        $l_key = $this->getKeyName();
		$check_models = is_array($models) ? $models : [$models];
		// Add the model(s)
		foreach ($check_models as $model){
           // Integrity check
           if($icheck) {
			  if(!$model->__isset($f_key)) {
			   	 // The attached model has no set foreign key.
			   	 // Check the parent's (this) local key.
			   	 if(!$this->__isset($l_key))
			   	  	// Not even the parent's local key is set. Horror!
			   	  	throw new AGException
			   	    ('Integrity check faild for relation : '.$relation);
			   	 // Ok, the parent has a local key. Assign it.
			   	 $model->setAttribute($f_key, $this->getAttribute($l_key));
			  }
           }
		   // If it's a 1-1 relation and we got many models to add then
		   // it's ambiguous so just throw up.
		   if ($r instanceof HasOne) {
		   	  if (count($check_models)>1)
		   	  	throw new AGExcpetion('Trying to set many models on a 1-1 relation.');
		   	  $this->setRelation($relation, $model);
		   }
		   // If it's a 1-n relation create a collection if there is none,
		   // then add the model(s) to the collection.
		   elseif ($r instanceof HasMany) {
		   	  if(!$this->relationLoaded($relation))
		   	  	  $this->setRelation($relation, new Collection);
		   	  $this->$relation->add($model);
		   // Other relations not supported. Sorry.
		   } else {
		   	  throw new AGExcpetion('Trying to set an unsupported relation.');
		   }
		}
	}
	
	/**
	 *  Remove fields from this entity's instance. May be useful if there are
	 *  unwanted fields in the currently loaded entity.
	 *
	 *  @param array|string $attributes The fields to be removed. If this parameter
	 *  is the wildcard '*' then all attributes are removed.
	 *  @param array $but_these An optional exclude list to be used when removing all
	 *  attributes (only used when '*' is passed).
	 */
	public function forget($attributes, array $but_these = null) {
	
		if(empty($attributes)) return;
	
		if(!is_array($attributes))
			$attributes = [$attributes];
	
			// If we get the wildcard then remove all attributes, possibly
			// using an exclude list, else remove the specified attributes.
			if($attributes[0] === '*') {
				$attr_keys = array_keys($this->attributes);
				$remove_keys = $but_these ? array_diff($attr_keys, $but_these) :
				$attr_keys;
				foreach ($remove_keys as $key)
					$this->__unset($key);
			} else {
				foreach ($attributes as $attribute){
					if(array_key_exists($attribute, $this->attributes))
						$this->__unset($attribute);
				}
			}
	}
		
	
	/**
	 *  Checks whether this entity has all of the specified array of attributes set.
	 *  Attributes are considered as 'set' if they contain a value (that is they are
	 *  not null and not an empty string '').
	 *  
	 *  @param string|array $attributes The attribute names to be checked.
	 *  @return boolean True if all of the specified attributes are present, false
	 *  otherwise.
	 */
	public function has_set($attributes) {
		$attributes = is_array($attributes) ? $attributes : [$attributes];
		//return count(array_intersect($attributes,
		//		     array_keys(array_filter($this->attributes)))) ===
		//       count($attributes);
		$has = array_filter($attributes, function($a){
			return $this->__isset($a) &&
			       !is_null($this->attributes[$a]) &&
			       $this->attributes[$a] !== "";
		});
		return count($has) === count($attributes);
	}
	
	
	/**
	 *  Removes all attributes that are null or empty strings ('') from this entity
	 *  and all of the loaded related entities, if any.
	 *  
	 *  @return AGEntity This entity instance.
	 */
	public function cleanup() {
		foreach ($this->attributes as $attribute=>$value) {
			if(is_null($value) || $value === "")
				$this->__unset($attribute);
		}
		foreach ($this->relations as $models) {
			$models = $models instanceof Collection	? $models->all() : [$models];
			foreach (array_filter($models) as $model) {
				$model->cleanup();
			}
		}
		return $this;
	}
		
	
	/**
	 * Generates a unique reference string (id) for this entity.
         * 
         * TODO Use integer numbers (see https://github.com/jenssegers/optimus )
	 * 
	 * @return string A unique reference.
	 */
	protected static function generate_ref(AGEntity $entity) {
		return Hey::generate_uuid();
	}
}
