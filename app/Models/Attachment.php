<?php

namespace App\Models;

use App\Utils\Validate;

class Attachment extends AGEntity {

	protected $fillable = [
//		'ref',
		'filename',
		'path',
		'size',
		'mime',
                'is_encrypted',
                'is_compressed',
		'filename_orig',
		//'ref_ext1',
		//'ref_ext2',
		//'id_messages'
	];

	protected $hidden = [
//		'id',
		'id_messages',
		'filename',
		'path',
		'ref_ext1',
		'ref_ext2',
		'created_at',
		'updated_at',
	];
        
        
        public function __construct(array $attributes = []) {
            parent::__construct($attributes);
        }

        ///////////////////////////// RELATIONSHIPS /////////////////////////////

	public function message(){
		return $this->belongsTo('App\Models\Message', 'id_messages');
	}

	/////////////////////////////////////////////////////////////////////////


	public function validate() {
		// TODO
	}
	
}
