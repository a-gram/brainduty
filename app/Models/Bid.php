<?php

namespace App\Models;

class Bid extends AGEntity {

    protected $table = 'tasks_bids';

    protected $fillable = [

    ];

    protected $hidden = [
        'id_tasks',
        'bidder',
        //'bid_datetime',
    ];

    public $timestamps = false;

    
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }

    ///////////////////////////// RELATIONSHIPS /////////////////////////////

    public function task(){
         return $this->belongsTo('App\Models\Task', 'id_tasks');
    }

    /////////////////////////////////////////////////////////////////////////


    public function validate() {
    }
	
}
