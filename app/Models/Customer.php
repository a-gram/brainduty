<?php

namespace App\Models;

use App\Utils\Validate;
use App\Utilas\Hey;
use App\K;
use App\Models\Service;
use App\Exceptions\AGException;
use App\Exceptions\AGValidationException;
use DB;

class Customer extends User
{
    protected $table = 'users';

    protected $m_default_payment_method = null;


    public function  __construct(array $attributes = []) {
            parent::__construct($attributes);
            $this->role = K::ROLE_CUSTOMER;
    }

    ///////////////////////////// RELATIONSHIPS /////////////////////////////

    public function settings() {
        return $this->hasOne('App\Models\CustomerSettings', 'id_users');
    }

    public function payment() {
        return $this->hasMany('App\Models\Payment', 'id_users_customer');
    }

    public function tasks() {
        return $this->hasMany('App\Models\Task', 'id_users_customer');
    }

    ////////////////////////////////////////////////////////////////////////

    /**
     *  Validates customer's data. Always check the instance by calling the parent's
     *  validate() method first (User::validate()) to validate derived attributes, then
     *  implement here any customer specific validation bits.
     */
    public function validate() {
        parent::validate();
    }


    /**
     *  Gets the default payment method for this customer.
     *  
     *  @return Payment|null The default payment method or null if the customer has
     *  no registered payment methods.
     */
    public function default_payment_method() {
        
        if(is_null($this->m_default_payment_method)) {
            $default = $this->settings()->first()->default_payment_method;
            $this->m_default_payment_method =
            $this->payment()->where('method', $default)
                            ->where('is_primary', true)
                            ->where('is_open', true)
                            ->first();
        }
        return $this->m_default_payment_method;
    }

}


