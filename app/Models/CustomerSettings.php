<?php

namespace App\Models;

class CustomerSettings extends AGEntity {

    protected $table = 'users_customers_settings';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'default_payment_method',
    	'ta_method',
        'ta_criteria',
        'ta_rating',
        'ta_pricing',
    ];
    
    /**
     * The attributes that should be hidden for arrays returned from
     * methods of the model.
     *
     * @var array
     */
    protected $hidden = [
    	'id',
    	'id_users',
    	'created_at',
    	'updated_at',
    ];
    

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }

	///////////////////////////// RELATIONSHIPS /////////////////////////////
    
    public function customer(){
    	return $this->belongsTo('App\Models\Customer', 'id_users');
    }
    
    /////////////////////////////////////////////////////////////////////////
    

    public function validate() {
    	// TODO
    }
    
    
}
