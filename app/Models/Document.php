<?php

namespace App\Models;

use App\Utils\Validate;

class Document extends AGEntity {
    
    protected $table = 'documents';
    
    protected $fillable = [
//            'id_users',
//		'ref',
            'category',
            'type',
            'filename',
            'path',
            'size',
            'mime',
            'is_encrypted',
            'is_compressed',
            'is_hosted',
            'filename_orig',
            'ref_ext',
            'data',
    ];

    protected $hidden = [
//		'id',
            'id_users',
            'filename',
            'path',
            'ref_ext',
            'created_at',
            'updated_at',
    ];


    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }

    ///////////////////////////// RELATIONSHIPS /////////////////////////////


    /////////////////////////////////////////////////////////////////////////


    public function validate() {
            // TODO
    }
	
}
