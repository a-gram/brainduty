<?php

namespace App\Models;

use App\Utils\Validate;
use App\Hey;
use App\K;


class Message extends AGEntity {

	protected $table = 'messages';
	
	protected $fillable = [
		//'ref',
		//'delivery_datetime',
		//'seen_datetime',
		//'type',
		'body',
		'is_delivered',
		//'is_deliverable',
		'is_seen',
		//'is_txn_completed',
		//'action_names',
		//'action_method',
		//'action_params',
		//'action_user',
                //'action_datetime',
		//'ref_ext1',
		//'ref_ext2',
	];

	protected $hidden = [
		'id',
		'id_tasks',
		'id_users_sender',
		'id_users_recipient',
		'is_deliverable',
//		'token',
		'ref_ext1',
		'ref_ext2',
		'data',
//		'created_at',
		'updated_at',
	];

        
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }

		
	///////////////////////////// RELATIONSHIPS /////////////////////////////

	public function attachments() {
		return $this->hasMany('App\Models\Attachment', 'id_messages');
	}
	
	public function task(){
		return $this->belongsTo('App\Models\Task', 'id_tasks');
	}
	
	public function sender(){
		return $this->belongsTo('App\Models\User', 'id_users_sender');
	}
	
	public function recipient(){
		return $this->belongsTo('App\Models\User', 'id_users_recipient');
	}

	////////////////////////// ACCESSORS/MUTATORS ///////////////////////////
	
	
 	public function setBodyAttribute($value) {
 		// The message body can be injected by clients with malicious html code
 		// so sanitize it by removing any html tags.
 		$this->attributes['body'] = Hey::strip_html_tags($value);
 	}
	
// 	public function getBodyAttribute($value) {
//            // Upon reading the body, replace all newline character sequences
//            // with html <br> so the message's original layout is preserved.
//            // Also, check for http URLs and turn them into html links.
//            return Hey::linkfy( preg_replace('/(\r?\n)/', '<br>', $value) );
// 	}
 	
	/////////////////////////////////////////////////////////////////////////
	
	
	public function validate() {

		Validate::message_body($this->body);
	}
	
	public static function make_ref(Message $msg) {
		$ref = parent::generate_ref($msg);
		return 'msg-'.$ref;
	}
	
	
	/**
	 * Convenience function to check whether a user is a legit actor of this
	 * message. A 'legit actor' is any entities that has rights to act on a message,
	 * such as reading its contents, updating its status, etc.
	 *
	 * @param unknown $user_id The id of the user to be verified.
	 * @return boolean True if the user is enabled to act on the task, false if not.
	 */
	public function has_actor($user_id) {
		return $user_id === $this->id_users_sender ||
		       $user_id === $this->id_users_recipient ||
		       User::is_admin_id($user_id);
	}
	
	
	/**
	 * Check whether this message is a transactional one.
	 */
	public function is_transactional() {
		return $this->type != K::MESSAGE_TASK_CONVERSATION;
	}
	
        
        /**
         * Get the role of this message's sender. Messages can be sent by the
         * customer, the provider or the system.
         * 
         * @param Task $task
         * @return string|null
         */
        public function get_sender_role($task = null) {
            
            // Check in the given task or in this msg's task.
            $task = $task && $task->id === $this->id_tasks ?
                    $task : $this->task;
            
            $role = $this->id_users_sender === $task->id_users_customer ?
                    User::role_name(K::ROLE_CUSTOMER) :
                   ($this->id_users_sender === $task->id_users_provider ?
                    User::role_name(K::ROLE_PROVIDER) : null );
            
            return $role ?: ($this->id_users_sender === User::SYSTEM ? 
                             User::role_name(K::ROLE_SYSTEM) : null);
        }
        

        /**
         * If this message is a transactional request (cancel, end, etc.) check 
         * whether this request has expired. An expired transactional request is a message 
         * to which the other party has not responded within a predefined period of 
         * time, which depends on the type of request.
         * 
         * @return boolean
         */
        public function is_request_expired() {
            
            if($this->type === K::MESSAGE_TASK_CANCEL_REQUEST) {
               $expire_interval = Hey::get_app('intervals.task.cancel_request_expired');
            } else
            if($this->type === K::MESSAGE_TASK_END_REQUEST) {
               $expire_interval = Hey::get_app('intervals.task.end_request_expired');
            } else {
               Hey::bug_here($this->type);
            }
            $now = new \DateTime();
            $request_time =  new \DateTime($this->created_at);
            $request_expire_period = new \DateInterval($expire_interval);
            return $now >= $request_time->add($request_expire_period);
        }
		
}
