<?php

namespace App\Models;

class Notification extends AGEntity {

	protected $table = 'notifications';
	
	protected $fillable = [
		'type',
		'event',
		'message',
		'is_delivered',
		'is_seen',
		'subject_id',
                'url',
		'data',
		'id_users_recipient',
	];

	protected $hidden = [
		'id',
		'id_users_recipient',
		//'created_at',
		'updated_at',
	];

        
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }

	
	///////////////////////////// RELATIONSHIPS /////////////////////////////

	public function user(){
		return $this->belongsTo('App\Models\User', 'id_users_recipient');
	}

	/////////////////////////////////////////////////////////////////////////


	public function validate() {
		assert(strlen($this->url)<=150);
                assert(strlen($this->data)<=500);
	}
	
	
	public function to($recipient) {
		$this->id_users_recipient = $recipient;
	}

}
