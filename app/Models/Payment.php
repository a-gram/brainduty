<?php

namespace App\Models;

use App\Utils\Validate;

/**
 *  This entity represents a source of funds for payments made by customers
 *  on the platform. For example, it may represent a credit/debit card, a
 *  virtual wallet (Paypal, etc.) ...
 */
class Payment extends AGEntity {

	protected $table = 'users_payment_settings';
	
	protected $fillable = [
		'method',
		'cycle',
		'source_id',
		'source_name',
		'source_number',
		'source_expiry',
		'source_type',
		'is_primary',
		'is_open',
		'is_verified',
	];

	protected $hidden = [
		'id',
		'id_users_customer',
		'created_at',
		'updated_at',
	];
        
        
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }


	///////////////////////////// RELATIONSHIPS /////////////////////////////

	public function customer(){
		return $this->belongsTo('App\Models\Customer', 'id_users_customer');
	}

	/////////////////////////////////////////////////////////////////////////


	public function validate() {
		Validate::payment_method($this->method);
		Validate::payment_cycle($this->cycle);
		Validate::payment_source_id($this->source_id);
		Validate::payment_source_name($this->source_name);
	}

	/**
	 * Get this payment method's holder full name
	 */
	public function get_account_holder() {
		return $this->source_name;
	}
	
}
