<?php

namespace App\Models;

use App\Utils\Validate;


class Payout extends AGEntity {

    protected $table = 'users_payout_settings';

    protected $fillable = [
   		'method',
   		'account_name',
   		'account_id',
   		'account_number',
   		'account_host',
   		'is_primary',
   		'is_open',
   		'is_verified',
    ];
    
    protected $hidden = [
   		'id',
    	'id_users_provider',
   		'created_at',
    	'updated_at',
    ];
    
    
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }

	///////////////////////////// RELATIONSHIPS /////////////////////////////
    
    public function provider(){
    	return $this->belongsTo('App\Models\Provider', 'id_users_provider');
    }
    
    /////////////////////////////////////////////////////////////////////////
    

    public function validate() {
    	Validate::payout_method($this->method);
    	Validate::payout_account_id($this->account_id);
    	Validate::payment_account_name($this->account_name);
    	
    }
    
    /**
     * Get this payment method's holder full name
     */
    public function get_account_holder() {
    	return $this->account_name;
    }
    
}
