<?php

namespace App\Models;

use App\Utils\Validate;
use App\Hey;
use App\K;
use App\Models\Service;
use App\Models\ServiceCategory;
use App\Exceptions\AGException;
use App\Exceptions\AGValidationException;
use App\IdentityManager;
use DB;

class Provider extends User
{
    protected $table = 'users';

    protected $m_default_payout_method = null;
    protected $id_manager;


    public function  __construct(array $attributes = []) {
            parent::__construct($attributes);
            $this->role = K::ROLE_PROVIDER;
            $this->id_manager = new IdentityManager($this);
    }

    ///////////////////////////// RELATIONSHIPS /////////////////////////////

    public function settings() {
            return $this->hasOne('App\Models\ProviderSettings', 'id_users');
    }

    public function services() {
            return $this->belongsToMany('App\Models\Service',
                                'providers_services', 'id_users', 'id_services')
            ->withPivot('categories', 'rate');
    }

    public function identity() {
            return $this->hasOne('App\Models\ProviderIdentity', 'id_users_provider');
    }

    public function payout() {
            return $this->hasMany('App\Models\Payout', 'id_users_provider');
    }

    public function tasks() {
            return $this->hasMany('App\Models\Task', 'id_users_provider');
    }

    ////////////////////////////////////////////////////////////////////////


    /**
     *  Validates provider's data. Always check the instance by calling the parent's
     *  validate() method first (User::validate()) to validate derived attributes, then
     *  implement here any provider's specific validation bits.
     */
    public function validate() {
            parent::validate();
            //
    }


    /**
     * Get the identity verification manager for this provider.
     * 
     * @return IdentityManager
     */
    public function get_identity_manager() {
        return $this->id_manager;
    }

    
    /**
     *  Check whether this provider's identity verification has started.
     *  
     * @return boolean
     */
    public function has_verification() {
    	return $this->identity && $this->identity->status !== null;
    }

    
    /**
     *  Check whether this provider's identity is being verified.
     *
     * @return boolean
     */
    public function is_verifying() {
        return $this->identity &&
              ($this->identity->status == K::PROVIDER_IDENTITY_SUBMITTED ||
               $this->identity->status == K::PROVIDER_IDENTITY_VERIFYING);
    }
	
	    
    /**
     *  Check whether this provider's identity is verified.
     *  
     * @return boolean
     */
    public function is_verified() {
    	return $this->identity &&
    	       $this->identity->status == K::PROVIDER_IDENTITY_VERIFIED;
    }

    
    /**
     *  Check whether this provider's identity failed verification.
     *
     * @return boolean
     */
    public function is_unverified() {
    	return $this->identity &&
    	       $this->identity->status == K::PROVIDER_IDENTITY_UNVERIFIED;
    }
     
    
    /**
     *  Check whether this provider is being inquired for further identity
     *  verification.
     *
     * @return boolean
     */
    public function is_inquired() {
    	return $this->identity &&
    	       $this->identity->status == K::PROVIDER_IDENTITY_INQUIRED;
    }
    
        
    /**
     *  Gets the default payout method for this provider.
     *
     *  @return Payout|null The default payout method or null if the provider has
     *  no registered payout accounts.
     */
    public function default_payout_method() {
    	
    	if(is_null($this->m_default_payout_method)) {
    	   $default = $this->settings()->first()->default_payout_method;
    	   $this->m_default_payout_method =
    	   $this->payout()->where('method', $default)
    	                  ->where('is_primary', true)
    	                  ->where('is_open', true)
    	                  ->first();
    	}
    	return $this->m_default_payout_method;
    }
    
    
    /**
     * Get the services rendered by this provider.
     * Unlike the 'services' relationship, which returns the Service models, this
     * method directly returns the Pivot models in the joining/intermediate table.
     *
     * @return Collection A collection of pivot service models for this provider.
     */
    public function get_services() {
        $services = array_map(function($service){
                return $service->pivot;
        }, $this->services->all());
        return collect($services);
    }
    

    /**
     *  This method gets the list of services rendered by this provider (records
     *  from the joining/intermediate table) as a list of record arrays indexed by
     *  service id, as follows
     *  
     *  [ 
     *     <service id> => or
     *     ([0]         =>)
     *     [
     *            (id => <service id>),
     *             categories =>
     *             [
     *                     <catag> => or
     *                     ([0]    =>)
     *                     [
     *                          (catag => <catag>),
     *                           name  => <name>,
     *                           price => <price>
     *                     ],
     *                     
     *                     <catag> => or
     *                     ([1]    =>) [...]
     *                     ...
     *             ],
     * 
     *             rate => <service rate>
     *             ...
     *     ],
     * 
     *     <service id> => or
     *     ([1]         =>) [...]
     *     ...
     *  ]
     *
     *  @param boolean $id_keys If this flag is true (default) then the returned
     *  services and categories (if any) are indexed by their service id and 
     *  category id (catag) respectively. If it is false then the returned records
     *  are sequentially indexed as standard and the service and category ids are
     *  included as fields.
     *  @return array An array of services that this provider provides, indexed
     *  by service id or sequentially.
     */
    public function services_list($id_keys = true) {
        
        $categs = ServiceCategory::all()->keyBy('tag')->all();
        
        $s = $this->get_services();
        
        if($id_keys)
           $s = $s->keyBy('id_services');
        
        return $s->map(function ($val, $key) use ($categs, $id_keys) {
                          $val = $val->toArray();
                          $cat = $val['categories'] ? explode(',', $val['categories']) : [];
                          $val['categories'] = [];
                          foreach($cat as $i=>$tag) {
                              $val['categories'][$id_keys?$tag:$i] = [
                                  'name' => $categs[$tag]['name'],
                                  'price' => $categs[$tag]['price'],
                              ];
                              if(!$id_keys)
                                 $val['categories'][$i]['tag'] = $tag;
                          }
                          if(!$id_keys)
                             $val['id'] = $val['id_services'];
                          unset ($val['id_users']);
                          unset ($val['id_services']);
                          return $val;
                    })
                    ->all();
    }
    
    
    /**
     * This method is equivalent to services_list().
     * 
     * TODO Is it more efficient ?
     * 
     * @return type
     */
    public function services_list_2($id_keys = true) {
        
        $categs = ServiceCategory::all()->keyBy('tag')->all();
        
        $s = Provider::where('id', $this->id)
                     ->join('providers_services',
                            'providers_services.id_users', '=', 'users.id')
                     ->select('providers_services.id_services',
                              'providers_services.categories',
                              'providers_services.rate')
                     ->get();
        
        if($id_keys)
           $s = $s->keyBy('id_services');
        
        return $s->map(function ($val, $key) use ($categs, $id_keys) {
                          $val = $val->toArray();
                          $cat = $val['categories'] ? explode(',', $val['categories']) : [];
                          $val['categories'] = [];
                          foreach($cat as $i=>$tag) {
                              $val['categories'][$id_keys?$tag:$i] = [
                                  'name' => $categs[$tag]['name'],
                                  'price' => $categs[$tag]['price'],
                              ];
                              if(!$id_keys)
                                 $val['categories'][$i]['tag'] = $tag;
                          }
                          if(!$id_keys)
                             $val['id'] = $val['id_services'];
                          unset ($val['id_services']);
                          return $val;
                    })
                    ->all();
    }
    
    
    /**
     * Check whether this provider provides the specified service, optionally in
     * specified categories.
     * 
     * @param mixed|array|Service $service The id of the service to be checked,
     * a Service model or a service record array.
     * @param string|array|null $category A string or array of categories tag/id.
     * @return boolean
     */
    public function provides($service, $category = null) {
        
        if($service instanceof Service) $id = $service->id;
        elseif (is_array($service))     $id = Hey::getval($service,'id');
        else                            $id = $service;
        // NOTE: This method is called within loops and will make few hundreds
        // database calls so we just cache the provider's services data.
        $services = $this->get_data('cache.services') ?:
                    $this->set_data('cache.services', $this->services_list())
                         ->get_data('cache.services');
        if($category && !is_array($category))
           $category = [$category];
    	return array_key_exists($id, $services) &&
    	       (is_null($category) ? true :
                count($category) === count(array_intersect($category, 
                                           array_keys($services[$id]['categories'])))
               );
    }
    
}


