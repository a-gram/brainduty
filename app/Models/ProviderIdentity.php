<?php

namespace App\Models;

use App\K;
use App\Hey;
use App\Exceptions\AGException;
use App\Exceptions\AGValidationException;


class ProviderIdentity extends AGEntity {

	protected $table = 'providers_identities';
        protected $primaryKey = 'id_users_provider';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
//		'id_users_provider',
//		'submission_datetime',
//		'results_datetime',
//		'status',
//		'info_required',
//		'int_verified',
//		'ext_verified',
//		'notes',
	];
        
	/**
	 * The attributes that should be hidden for arrays returned from
	 * methods of the model.
	 *
	 * @var array
	 */
	protected $hidden = [
		'id_users_provider',
		'created_at',
		'updated_at',
	];

        
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }

	
	///////////////////////////// RELATIONSHIPS /////////////////////////////

	public function provider(){
		return $this->belongsTo('App\Models\Provider', 'id_users_provider');
	}
        
	public function documents() {
		return $this->hasMany('App\Models\Document', 'id_users');
	}

	/////////////////////////////////////////////////////////////////////////


	public function validate() {
		//
	}
	
        
        /**
         * Check whether this identity verification has been closed.
         * 
         * @return boolean
         */
        public function is_closed() {
            return $this->status === K::PROVIDER_IDENTITY_VERIFIED ||
                   $this->status === K::PROVIDER_IDENTITY_UNVERIFIED;
        }
	
	/*
	 * Get a human-readable string representing the current identity status.
	 */
	public function status_to_string() {
		return static::status_name($this->status);
	}
	
	
	/**
	 * Map identity status ids to human-readable strings.
	 *
	 * @param unknown $status
	 * @return string
	 */
	public static function status_name($status) {
            
		switch ($status) {
			case K::PROVIDER_IDENTITY_SUBMITTED:   return 'submitted';
			case K::PROVIDER_IDENTITY_VERIFYING:   return 'verifying';
                        case K::PROVIDER_IDENTITY_INQUIRED:    return 'inquirying';
			case K::PROVIDER_IDENTITY_VERIFIED:    return 'verified';
			case K::PROVIDER_IDENTITY_UNVERIFIED:  return 'unverified';
			default:                               return 'unknown';
		}
	}
        
        
        /**
         * Add identity info required for this provider.
         * 
         * @param array $info An array of identity info types.
         */
        public function add_required_info(array $info) {
            
            $required = $this->info_required ? explode(',', $this->info_required) : [];
            $new = array_unique(array_merge($required, $info));
            $this->info_required = implode(',', $new) ?: null;
        }
        
        
        /**
         * Remove identity info from this provider's required info.
         * 
         * @param array $info
         */
        public function remove_required_info(array $info) {
            
            $required = $this->info_required ? explode(',', $this->info_required) : [];
            $new = array_diff($required, $info);
            $this->info_required = implode(',', $new) ?: null;
        }
        

        /**
         * Set the identity info required for this provider.
         * 
         * @param array $info An array of identity info types.
         */
        public function set_required_info(array $info) {
            
            $this->info_required = implode(',', $info) ?: null;
        }
        	
        
        /**
         * Check whether the given info is required by this provider.
         * 
         * @param string $info
         * @return boolean
         */
        public function is_info_required($info) {
            
            if(!$this->info_required)
               return false;
            $required = explode(',', $this->info_required);
            return in_array($info, $required);
        }
        
        /**
         * Check whether a given list of identity info matches the ones required
         * from this provider.
         * 
         * @param string|array $info
         * @return boolean
         * @throws AGValidationException
         */
        public function has_required_info($info) {
            
            $info = is_array($info) ? $info : [$info];
            
            $required = $this->info_required ? explode(',', $this->info_required) : [];
            
            $has = array_intersect($required, $info);
            return count($has) === count($required);
        }
        
	
        /**
         * Check whether identity documents are required for this provider.
         * 
         * @return boolean
         */
        public function requires_documents() {
            
            if(!$this->info_required) return false;
            $app_documents = Hey::get_app('identity.documents');
            $required = explode(',', $this->info_required);
            $requires = false;
            foreach($app_documents as $document) {
                $requires |= in_array($document['type'], $required);
            }
            return $requires;
        }
        

        /**
         * Get the list of required identity documents for this provider.
         * 
         * @return array List of identity document records (see app-settings.php).
         */
        public function get_required_documents() {
            
            if(!$this->info_required) return [];
            $app_documents = Hey::get_app('identity.documents');
            $required = explode(',', $this->info_required);
            $documents = [];
            foreach($app_documents as $document) {
                if(in_array($document['type'], $required)) {
                   $documents[] = $document;
                }
            }
            return $documents;
        }
                
        
        /**
         * Check whether a given list of identity documents matches the ones
         * required from this provider.
         * 
         * @param array $documents An array of string identity document types or
         * of identity document records (see app-settings.php).
         * 
         * @return boolean
         */
        public function has_required_documents(array $documents) {
            
            $required = $this->info_required ? explode(',', $this->info_required) : [];
            $app_documents = array_column (Hey::get_app('identity.documents'), 'type');
            $required_documents = array_intersect($required, $app_documents);
            
            // If the first element is a string we assume it's a list of
            // document types, if it has a "type" string field we assume it's
            // a list of identity document records and the check is done on
            // the "type" column.
            if(empty($documents) || is_string($documents[0])) {
               $docs = $documents;
            }
            else if(is_array($documents) &&
                    array_key_exists('type', $documents[0]) &&
                    is_string($documents[0]['type'])) {
               
               $docs = array_column ($documents, 'type');
            }
            else {
               assert(false);
            }
            $has = array_intersect($required_documents, $docs);
            return count($has) === count($required_documents);
        }
        

        /**
         * Check whether a given document type must be verified by the platform.
         * 
         * @param string $doc_type
         * @return bool
         * @throws AGException
         */
        public static function is_platform_verified($doc_type) {
            return self::get_document_verifier($doc_type) === K::IDENTITY_DOC_VERIFIER_PLATFORM;
        }

        /**
         * Check whether a given document type must be verified by the platform.
         * 
         * @param string $doc_type
         * @return bool
         * @throws AGException
         */
        public static function is_external_verified($doc_type) {
            return self::get_document_verifier($doc_type) === K::IDENTITY_DOC_VERIFIER_EXTERNAL;
        }
        
        /**
         * Get the verifier for a given document type.
         * 
         * @param string $doc_type
         * @return string
         * @throws AGException
         */
        protected static function get_document_verifier($doc_type) {
            $app_id_documents = Hey::get_app('identity.documents');
            foreach($app_id_documents as $document) {
                if($document['type'] == $doc_type) {
                   return $document['verification'];
                }
            }
            Hey::bug_here($doc_type);
        }
        
        
        /**
         * Add identity notes.
         * 
         * @param string $notes
         */
        public function add_notes($notes) {
            $this->notes = $this->notes ? $this->notes.' '.$notes : $notes;
        }
        
}
