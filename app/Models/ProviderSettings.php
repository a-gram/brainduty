<?php

namespace App\Models;

use App\Utils\Validate;
use App\Hey;
use App\K;


class ProviderSettings extends AGEntity {

    protected $table = 'users_providers_settings';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'default_payout_method',
    	'markup',
        'task_cancel_policy',
    ];
    
    /**
     * The attributes that should be hidden for arrays returned from
     * methods of the model.
     *
     * @var array
     */
    protected $hidden = [
    	'id',
    	'id_users',
    	'created_at',
    	'updated_at',
    ];
    
    
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }


	///////////////////////////// RELATIONSHIPS /////////////////////////////
    
    public function provider(){
    	return $this->belongsTo('App\Models\Provider', 'id_users');
    }
    
    /////////////////////////////////////////////////////////////////////////
    

    public function validate() {
    	
        Validate::payout_method($this->default_payout_method);
        Validate::in_range($this->markup, [1,100]);
        Validate::task_cancellation_policy($this->task_cancel_policy);
    }
    
    
}
