<?php

namespace App\Models;

use App\Hey;
use DB;


class Service extends AGEntity {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	'type',
    	'name',
    	'description',
    	'duration',
    	'price',
    	'is_active',
    ];
    
    /**
     * The attributes that should be hidden for arrays returned from
     * methods of the model.
     *
     * @var array
     */
    protected $hidden = [
        'pivot',
        'created_at',
    	'updated_at',
    ];
	
    
    // Service ids. These are currently invariant, once chosen.
    const GENERIC = 1;
    const EXPERT = 2;
    const EXTRA = 3;
    
    
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }

    
    ///////////////////////////// RELATIONSHIPS /////////////////////////////

    public function providers() {
        return $this->belongsToMany('App\Models\User', 'providers_services',
                                    'id_services', 'id_users');
    }
    
    public function categories() {
        return $this->hasMany('App\Models\ServiceCategory', 'id_services');
    }

    /////////////////////////////////////////////////////////////////////////


    public function validate() {
    }
    
    
    /**
     * Get all services and related categories (if any) available on the platform
     * and returns them in the following array format:
     * 
     * [
     *     <service id> => [
     *           <field> => <value>,
     *           ...
     *           'categories' => [
     *                 <tag> => [
     *                       name  => <name>,
     *                       price => <price>
     *                 ]
     *                 ...
     *           ]
     *     ]
     *    ...
     * ]
     * 
     * NOTE: This method makes database queries everytime it is called.
     *       Use the cached version App\Services::get() instead.
     * 
     * @return array An array of Service records with related categories in the
     * format described above.
     */
    public static function get_all() {
        
        return
        Service::with('categories')->get()
               ->keyBy('id')
               ->map(function($service, $id){
                     $categories = [];
                     foreach ($service->categories as $category) {
                         $categories[$category->tag] = [
                             'name'  => $category->name,
                             'price' => $category->price,
                         ];
                     }
                     $service = $service->attributesToArray();
                     $service['categories'] = $categories;
                     return $service;
                 })
               ->all();
    }
    

    /**
     * This method may be used if providers are allowed to modify the services
     * base rates to give customers an estimate of the possible cost. How rates
     * are estimated depend on how providers are allowed to modify the base rates.
     * 
     * The following procedure estimates min and max rates for each active service
     * based on providers markups.
     * 
     * @param array $services The list of services whose rates must be estimated.
     * @return array The given services array with the modified rates.
     */
    public static function estimate_rates(&$services) {
        
        $providers_markup_table = \App\Models\ProviderSettings::get_table_name();
                
        $markup  = DB::table($providers_markup_table)->
                       select(DB::raw(
                           'MIN(markup) as min,
                            MAX(markup) as max'))->
                       get();
        
        if($markup[0]->min || $markup[0]->max) {
           foreach ($services as &$service) {
              if($service['is_active']) {
                 $min = &$service['price_estimate']['min'];
                 $max = &$service['price_estimate']['max'];
                 $min = $service['price'] * (1 + $markup[0]->min / 100);
                 $max = $service['price'] * (1 + $markup[0]->max / 100);
              }
           }
        }
    }
    
}
