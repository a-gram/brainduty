<?php

namespace App\Models;

use App\Hey;


class ServiceCategory extends AGEntity {

    protected $table = 'services_categories';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	'tag',
    	'name',
        'price',
    ];
    
    /**
     * The attributes that should be hidden for arrays returned from
     * methods of the model.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'id_services',
    ];

    
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }

    public function validate() {
    }
	
}
