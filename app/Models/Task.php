<?php

namespace App\Models;

use App;
use App\K;
use App\Hey;
use App\Utils\Validate;
use App\Models\Attachment;
use App\Models\Service;
use App\TaskManager;


class Task extends AGEntity {

    protected $fillable = [
        //'ref',
        //'schedule_datetime',
        //'start_datetime',
        //'end_datetime',
        //'complete_datetime',
        'type',
        'subject',
        'catags',
        //'location',
        //'status',
        'priority',
        'is_funded',
        'is_rescheduled',
        'is_managed',
        'notes',
        'assignee',
//	'ref_ext1',
//	'ref_ext2',
    ];

    protected $hidden = [
        'id',
        'id_users_provider',
        'id_users_customer',
        'created_at',
        'updated_at',
        'ref_ext1',
        'ref_ext2',
        'assignee',
    ];

    /**
     * The archive used by this task to store related files.
     * 
     * @var TaskArchive
     */
    protected $archive;

    /**
     * The task life cycle manager instance.
     * This is often used where a task instance is involved to change the
     * task current state, so we have it injected here.
     * 
     * @var TaskManager
     */
    protected $manager;


    public function __construct(array $attributes = [])	{
        parent::__construct($attributes);
        $this->archive = app('archive')->of($this);
        $this->manager = new TaskManager($this);
    }


    ///////////////////////////// RELATIONSHIPS /////////////////////////////

    public function provider(){
        return $this->belongsTo('App\Models\Provider', 'id_users_provider');
    }

    public function customer(){
        return $this->belongsTo('App\Models\Customer', 'id_users_customer');
    }

    public function messages() {
        return $this->hasMany('App\Models\Message', 'id_tasks');
    }

    public function transactions() {
        return $this->hasMany('App\Models\Transaction', 'id_tasks');
    }

    public function services() {
        return $this->belongsToMany('App\Models\Service', 
                                    'tasks_services', 'id_tasks', 'id_services')
        ->withPivot('id_messages', 'amount', 'description');
    }

    public function bidders() {
        return $this->belongsToMany('App\Models\Provider',
                                    'tasks_bids', 'id_tasks', 'bidder');
    }

    public function bids() {
        return $this->hasMany('App\Models\Bid', 'id_tasks');
    }

    public function ratings() {
        return $this->hasMany('App\Models\UserRatings', 'id_tasks');
    }
    
    public function dispute() {
        return $this->hasOne('App\Models\TaskDispute', 'id_tasks');
    }

    /////////////////////////////////////////////////////////////////////////


    public function validate() {
        Validate::text($this->subject, 100);
        //Validate::alphanumeric($this->category, 256);
    }

    
    /**
     * Generate a unique reference for the given task.
     * 
     * @param Task $task
     * @return string
     */
    public static function make_ref(Task $task) {
        $ref = parent::generate_ref($task);
        return 'tsk-'.$ref;
    }


    /**
     * Get the services associated with this task.
     * Unlike the 'services' relationship, this method directly returns the records
     * in the joining/intermediate table (tasks_services) not the Service models.
     *
     * @return Collection A collection of task services record (pivot) models.
     */
    public function get_services() {
        $services = array_map(function($service){
                return $service->pivot;
        }, $this->services->all());
        return collect($services);
    }


    public function get_manager() {
        return $this->manager;
    }


    /**
     * Map task status ids to human-readable strings.
     * 
     * @param unknown $status
     * @return string
     */
    public static function status_name($status) {
        switch ($status) {
                case K::TASK_SCHEDULED: return 'scheduled';
                case K::TASK_ONGOING:   return 'ongoing';
                case K::TASK_ENDED:     return 'ended';
                case K::TASK_CANCELLED: return 'cancelled';
                case K::TASK_COMPLETED: return 'completed';
                case K::TASK_DISPUTED:  return 'disputed';
                default:                return 'unknown';
        }
    }


    /**
     * Convenience function to check whether a user is a legit actor of this
     * task. A 'legit actor' is any entity that has rights to act on a task,
     * such as updating its status, posting messages, etc.
     * 
     * @param unknown $user_id The id of the user to be verified.
     * @return boolean True if the user is enabled to act on the task, false if not.
     */
    public function has_actor($user_id) {
        return $user_id === $this->id_users_provider ||
               $user_id === $this->id_users_customer ||
               User::is_admin_id($user_id);
    }


    /**
     * Check whether this task has been assigned to a provider. Tasks that are
     * not yet assigned are temporarily assigned to the system user.
     */
    public function is_assigned() {
        return $this->id_users_provider !== User::SYSTEM;
    }


    /**
     * Check whether this task is open. Tasks are considered as 'open' if they
     * are not cancelled or completed.
     */
    public function is_open() {
        return $this->status != K::TASK_CANCELLED &&
               $this->status != K::TASK_COMPLETED;
    }


    /**
     * Check whether this task is closed. Tasks are considered as 'closed' if they
     * are either cancelled or completed.
     */
    public function is_closed() {
        return $this->status == K::TASK_CANCELLED ||
               $this->status == K::TASK_COMPLETED;
    }


    /**
     * Check whether this task is expired. Tasks are considered 'expired' if they
     * are scheduled and not assigned within the expire period.
     */
    public function is_expired() {
        $now = new \DateTime();
        $task_schedule_time =  new \DateTime($this->schedule_datetime);
        $task_expire_period = new \DateInterval(Hey::get_app('intervals.task.expired'));
        return $now >= $task_schedule_time->add($task_expire_period);
    }


    /**
     * Check whether this task is disputable. Tasks are considered 'disputable'
     * if the dispute period after they're ended has not elapsed.
     */
    public function is_disputable() {
        $now = new \DateTime();
        $task_end_time =  new \DateTime($this->end_datetime);
        $task_disputable_period = new \DateInterval(Hey::get_app('intervals.task.disputable'));
        return $now < $task_end_time->add($task_disputable_period);
    }


    /**
     * Check whether this task accepts bids. Tasks can accept bids if they are
     * still scheduled.
     */
    public function accepts_bids() {
        return $this->status == K::TASK_SCHEDULED;
    }


    /**
     * Get a human-readable string representing the current task status.
     */
    public function status_to_string() {
        return static::status_name($this->status);
    }


    /**
     * Get this task's archive.
     * 
     * @return TaskArchive
     */
    public function archive() {
        return $this->archive;
    }


    /**
     * Store the given files in this task's archive and returns a set of objects
     * representing the stored files.
     *
     * @param array|File $files An array of File objects, or a File object.
     * @param string $context A context in which to store the files.
     * @param string $type Specifies how the stored files are returned. By default
     * as an array of Attachment models, otherwise an array of Archive file info records.
     * @return array An array of Attachment models or Archive file info records
     * representing the stored files, if any.
     */
    public function store_files($files, $context = '', $type = 'attachments') {

        if(!is_array($files))
           $files = [$files];
        
        if(empty($files)) return [];

        $records = $this->archive->
                          with_encryption()->
                          with_compression()->
                          store($files, $context);

        if($type === 'attachments') {

           $attachments = array_map(function($a){
              return new Attachment ($a);
           }, $records);

           return $attachments;
        }
        else {
           return $records;
        }
    }


    /**
     * Roll back the given task's files or the whole archive.
     * 
     * @param unknown $files The file(s) to be rolled back. If null, the whole
     * archive is rolled back (removed).
     * @return array An array of the rolled-back files.
     */
    public function rollback_archive($files = null) {
        
        // Handle the case where we get an Attachment model or an array
        // of Attachment models to convert them to arrays.
        if($files instanceof Attachment) {
           $files = [$files->makeVisible($files->getHidden())->attributesToArray()];
        } else
        if(is_array($files)) {
           array_walk($files, function (&$file, $i) {
                if($file instanceof Attachment)
                   $file = $file->makeVisible($file->getHidden())->attributesToArray();
           });
        }
        return $this->archive->remove($files);
    }


    /**
     * Copy this task archive or part of it to a new task archive.
     * 
     * @param Task $to The new task archive where the files will be copied to.
     * @param Attachment $attachments An array of Attachment models. If given,
     * only the specified files will be copied to the new archive (partial copy).
     * @return array An array of Attachment models for the new files.
     */
    public function copy_archive($to, $attachments = null) {
        
        if($attachments) {
           $new_attachments = array_map(function ($a) use ($to) {
                $attach = $a->makeVisible($a->getHidden())->attributesToArray();
                $new = $this->archive()->clone_to($to->id, $attach);
                return (new Attachment ($attach))->fill($new[0]);
           }, $attachments);
        } else {
           $new = $this->archive()->clone_to($to->id);
           $new_attachments = array_map(function ($a) {
                return new Attachment ($a);
           }, $new);
        }
        return $new_attachments;
    }
    
    
    /**
     * This method maps request task services to the appropriate database records.
     * 
     * @param array $task An array of task info.
     * @return array An array of task service records
     */
    public function make_services($task) {
        
        // Map task service/type to internal format (see Service::get_all()) so
        // we can easily check it. Currently, only one service type can be selected
        // (Generic or Expert).
        $task_services = [
            $task['type'] => $task['catags']
        ];

        Validate::services($task_services);

        // Lookup the base services to be attached to the task.
        $services = app('services')->get();

        // Map the requested services to database records.
        array_walk($task_services, function (&$val, $id) use ($services, $task) {
            $val = [
                'amount'      => $id === Service::GENERIC ?
                                 $services[$id]['price'] :
                                 $services[$id]['categories'][$task['catags']]['price'],
                'description' => $services[$id]['duration'].'min slot for '.
                                 $services[$id]['name'].' duty.',
            ];
        });
        
       return $task_services;
    }
    
    
    /**
     * Get pending cancel requests for this task, if any. Pending cancel requests 
     * are transactional messages for mutual cancellation of the task to which the
     * counterpart has not taken any action.
     * 
     * @param boolean $expired If true, only the expired pending cancel request is returned.
     * @return Message|null
     */
    public function get_pending_cancel_request($expired = false) {
        
        $query = $this->messages()->where('type', K::MESSAGE_TASK_CANCEL_REQUEST)->
                                    where('is_txn_completed', false);
        if($expired) {
           $expiry_period = new \DateInterval(
                Hey::get_app('intervals.task.cancel_request_expired')
           );
           $dt = (new \DateTime)->sub($expiry_period)->format(K::DB_DATETIME_FORMAT);
           $query->where('created_at','>=',$dt);
        }
        return $query->first();
    }
    
    
    /**
     * Get pending charge requests for this task, if any. Pending charge requests 
     * are transactional messages for service charges to which the counterpart has 
     * not taken any action.
     * 
     * @return Message|null
     */
    public function get_pending_charge_request() {
        
        return $this->messages()->where('type', K::MESSAGE_TASK_CHARGE_REQUEST)->
                                  where('is_txn_completed', false)->
                                  first();
    }

}
