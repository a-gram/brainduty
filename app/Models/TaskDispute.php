<?php

namespace App\Models;

use App\Utils\Validate;
use App\Hey;
use App\K;


class TaskDispute extends AGEntity {

	protected $table = 'tasks_disputes';
	protected $primaryKey = 'id_tasks';
	public    $incrementing = false;

	protected $fillable = [
		//'id_tasks',
		'reason',
		'details',
		//'status',
                //'winner',
		//'notes',
	];

	protected $hidden = [
		'id_tasks',
		//'reason',
		//'details',
		//'status',
                'winner',
                //'notes',
		'created_at',
		'updated_at',
	];

        
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }

	///////////////////////////// RELATIONSHIPS /////////////////////////////

	public function task(){
		return $this->belongsTo('App\Models\Task', 'id_tasks');
	}

	/////////////////////////////////////////////////////////////////////////


	public function validate() {
		Validate::dispute_reason($this->reason);
		Validate::text($this->details, 300);
	}
        
        
    /**
     * Map dispute status ids to human-readable strings.
     * 
     * @param unknown $status
     * @return string
     */
    public static function status_name($status) {
        switch ($status) {
                case K::TASK_DISPUTE_SUBMITTED: return 'submitted';
                case K::TASK_DISPUTE_PROCESSING:   return 'processing';
                case K::TASK_DISPUTE_RESOLVED:     return 'resolved';
                default:                return 'unknown';
        }
    }
    

    /**
     * Get a human-readable string representing this dispute status.
     */
    public function status_to_string() {
        return static::status_name($this->status);
    }

    
    /**
     * Get a human-readable string representing this dispute reason.
     * 
     * @return string
     */
    public function reason_to_string() {
        if($this->reason === 0) {
           return null;
        }
        return Hey::get_app('task_dispute_reasons')[$this->reason];
    }

}
