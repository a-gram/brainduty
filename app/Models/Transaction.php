<?php

namespace App\Models;

use App\Exceptions\AGException;
use App\Hey;
use App\K;
use Carbon\Carbon;

/**
 * A transaction represents the movement of funds from a sender, who is debited, to
 * a recipient, who is credited. Transactions affect the user balance depending on
 * the flow of funds. Transactions may produce 'balance entries' to explicitly explain
 * the impact of the transaction on the user balance. For example, a 'charge' would
 * produce a negative balance entry (debit) in the sender's account and a positive
 * balance entry (credit) in the recipient's account. The gross amount represents 
 * the amount debited to the sender. Fees and taxes are amounts that are deducted 
 * along the way from the gross amount by middlemen (in this case by the platform).
 * The amount balanced is the amount affecting the user's balance, depending on
 * whether it is the sender or the recipient. In this application it refers to the
 * provider's balance (the flow of funds, that is the signs of the amounts, do refer 
 * to providers as well). So, if the provider is the recipient in the transaction
 * (e.g. for a payment) then the amount balanced is the net amount 
 * bal = gross - fees - taxes, 
 * while if it's the sender the amount balanced would be the gross amount
 * bal = (-)gross
 */
class Transaction extends AGEntity {
	
	protected $table = 'transactions';

	protected $fillable = [
		//'ref',
		'amount_gross',
		'amount_fee',
		'amount_tax',
                'amount_bal',
		'currency',
		'status',
		'description',
		'type',
		'is_captured',
		'settle_method',
                'settle_status',
		'request_id',
		'request_auth',
		'ref_ext',
                'ref_ext_source',
		'clear_datetime',
	];

	protected $hidden = [
		'id',
		'id_sender',
		'id_recipient',
		'id_tasks',
		//'ref_ext',
                //'ref_ext_source',
		//'created_at',
		'updated_at',
	];

        
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }

	
	///////////////////////////// RELATIONSHIPS /////////////////////////////

	public function sender(){
		return $this->belongsTo('App\Models\User', 'id_sender');
	}
	
	public function recipient(){
		return $this->belongsTo('App\Models\User', 'id_recipient');
	}
	
 	public function task(){
 		return $this->belongsTo('App\Models\Task', 'id_tasks');
 	}

	/////////////////////////////////////////////////////////////////////////


	public function validate() {
		// TODO
	}
	
	
    /**
     * Generate a unique reference for the given transaction.
     * 
     * @param Transaction $txn
     * @return string
     */
    public static function make_ref(Transaction $txn) {
        $ref = parent::generate_ref($txn);
        return 'txn-'.$ref;
    }

    
    /**
     * Convenience function to check whether a user is a legit actor of this
     * transaction.
     *
     * @param unknown $user_id
     * @return boolean
     */
    public function has_actor($user_id) {
    	return $user_id === $this->id_sender ||
    	       $user_id === $this->id_recipient || 
               User::is_admin_id($user_id);
    }
    
    
    /**
     * Check whether this transaction is pending.
     * 
     * @return boolean
     */
    public function is_pending() {
        return $this->status === K::TXN_PENDING;
    }
    
    
    /**
     * Check whether this transaction is completed.
     * 
     * @return boolean
     */
    public function is_completed() {
        return $this->status === K::TXN_COMPLETED;
    }
    
    
    /**
     * Check whether this transaction is cancelled.
     * 
     * @return boolean
     */
    public function is_cancelled() {
        // NOTE: See Transaction::get_status().
        return $this->get_status() === K::TXN_CANCELLED;
    }
    
    
    /**
     * Check whether this transaction is a credit.
     * 
     * @return boolean
     */
    public function is_credit() {
        return $this->amount_bal >= 0;
    }
    
    
    /**
     * Check whether this transaction is a dbit.
     * 
     * @return boolean
     */
    public function is_debit() {
        return $this->amount_bal < 0;
    }
    
    
    /**
     * Determine the status of this transaction.
     * 
     * Currently the 'cancelled' status for a transaction is never set, so we
     * determine whether the transaction has been cancelled (failed) by checking
     * for failures in the settlement method. This method should not be necessary
     * if the marketplace provider sets this status or if we determine whether a 
     * transaction failed when it is created or by receiving specific events.
     * 
     * @return string
     */
    public function get_status() {
        if(!$this->status || !$this->settle_status) Hey::bug_here();
        return K::$TXN_STATUS_MAP[$this->settle_status] === K::TXN_CANCELLED ? 
                                                            K::TXN_CANCELLED : 
                                                            $this->status;
    }

    
    /**
     * Set the date-time on which the funds associated with this transaction are
     * cleared, that is available for the recipient to use.
     * 
     * Funds would normally be available immediately after they're received. However, 
     * there may be 'hold periods' before funds can be used. For example, after services
     * have been paid there may be a 'dispute' period to allow customers to claim and
     * get refunds straight away, or other 'security' periods put in place by the
     * payment processors before releasing funds, etc.
     * 
     * @param number $time The date-time at which this transaction will be cleared
     * (available for withdraw) as a Unix timestamp. If zero, the current date-time
     * will be set. If null, the payout delay setting for the app will be added to
     * the current date-time.
     */
    public function set_clear_datetime($time = null) {
    	if(!is_null($time)) {
           $this->clear_datetime = $time === 0 ? Hey::now_to_db_datetime() :
                                                 Hey::timestamp_to_db_datetime($time);
    	} else {
           $delay = Hey::get_app('payout.delay');
           //$clear_on = (new \DateTime)->add(\DateInterval::createFromDateString($delay.' days'));
    	   //$this->clear_datetime = Hey::to_db_datetime($clear_on->setTime(0,0,0));
           $this->clear_datetime = Hey::to_db_datetime( Carbon::now()->addDays($delay)->setTime(0,0,0) );
    	}
    }
    
    
    /**
     * Check whether this transaction's funds are cleared, that is available for the
     * recipient to use.
     * 
     * @return boolean
     */
    public function is_clear() {
    	if(!isset($this->clear_datetime))
            throw new AGException('The clear date-time is not set.');
    	$now = new \DateTime();
    	$clear_datetime = new \DateTime($this->clear_datetime);
    	return $now >= $clear_datetime;
    }
    
    
}
