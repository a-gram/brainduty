<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

use App\Utils\Validate;
use App\Hey;
use App\K;
use App\Models\Provider;
use App\Models\Customer;
use App\Models\Payment;
use App\Models\Payout;
use App\Models\IdentityDocument;
use App\Exceptions\AGException;
use App\Exceptions\AGValidationException;
use App\Exceptions\AGInvalidStateException;
use URL, Auth, File;


class User extends AGEntity implements AuthenticatableContract,
                                       AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable. These are usually all
     * the model's attributes that are exposable to clients and that
     * can be set by clients.
     *
     * @var array
     */
    protected $fillable = [
            'username',
            /*'password',*/
            'first_name',
            'last_name',
            'dob',
            'mobile',
            'email',
            'address',
            'city',
            'state',
            'zip_code',
            'country',
            'entity_type',
            'business_name',
            'business_number',
            'about',
            //'status',
            //'role',
            //'rank',
            //'rating',
            //'fee',
            //'email_verified',
            //'ref',
            //'ref_ext',

    ];
    
    /**
     * The attributes that should be hidden for arrays returned from
     * methods of the model. These are usually sensitive info that we
     * dont want to risk leaking.
     *
     * @var array
     */
    protected $hidden = [
            'password',
            'remember_token',
            'id',
            //'role',
            'rank',
            //'email_verified',
            'created_at',
            'updated_at',
            //'ref_ext',
    ];
        
    /**
     * The archive used by this user to store related files.
     * 
     * NOTE: This property is not initialized on construction as it would not be
     *       a specific user archive if the instance is a User. Always use the 
     *       $this->archive() method.
     * 
     * @var TaskArchive
     */
    protected $archive;


    // The system user's id.
    const SYSTEM = 1006975;
    
    const DOCUMENTS_DIR = 'docs';
    const LOGS_DIR = 'logs';


    /**
     *   Construct a new user.
     */
    public function  __construct(array $attributes = []) {
            parent::__construct($attributes);
    }

    ///////////////////////////// RELATIONSHIPS /////////////////////////////


    public function notifications() {
            return $this->hasMany('App\Models\Notification', 'id_users_recipient');
    }

    public function ratings_given() {
            return $this->hasMany('App\Models\UserRating', 'id_sender');
    }

    public function ratings_received() {
            return $this->hasMany('App\Models\UserRating', 'id_recipient');
    }

    public function verification() {
            return $this->hasOne('App\Models\UserVerification', 'id_users');
    }

    public function favourited() {
            return $this->belongsToMany('App\Models\User',
                                        'users_fav_list', 'id_users', 'id_users_fav');
    }

    public function ignored() {
            return $this->belongsToMany('App\Models\User',
                                        'users_ignore_list', 'id_users', 'id_users_ignored');
    }


    ////////////////////////// ACCESSORS/MUTATORS ///////////////////////////


    // Convert dates between the database native format and the app's format.
    public function getDobAttribute($value) {
            return Hey::db_date_to_dmy($value);
    }
    public function setDobAttribute($value) {
            Validate::dob( $value );
            $this->attributes['dob'] = Hey::dmy_to_db_date($value);
    }


    /////////////////////////////////////////////////////////////////////////

    
    /**
     * Generate a unique reference for the given user.
     * 
     * @param User $user
     * @return string
     */
    public static function make_ref(User $user) {
        $ref = parent::generate_ref($user);
        return 'usr-'.$ref;
    }
    
    
    /**
     *  This method validates the current user's instance making sure its fields
     *  are valid and consistent according to the app's business rules.
     *  
     *  @param boolean $cleanup If set to 'true' then this model's instance is cleaned
     *  from all null and empty string attributes.
     *  @throws AGValidationException on errors.
     */
    public function validate($cleanup = true) {

        Validate::username( $this->username );
        Validate::password( $this->password );

        Validate::name( $this->first_name );
        Validate::name( $this->last_name );
        Validate::dob( $this->dob );
        Validate::age( $this->age() );
        Validate::email( $this->email );
        Validate::phone( $this->mobile );
        Validate::full_address( [
                        'address'  => $this->address,
                        'city'     => $this->city,
                        'state'    => $this->state,
                        'zip_code' => $this->zip_code,
                        'country'  => $this->country
        ] );

        // Validate business info
        Validate::business_info( $this->entity_type,
                                 $this->business_name,
                                 $this->business_number );

        // Check whether the user is already registered (only do this for new entities)
        if(!$this->exists && User::where('username', $this->username)->exists()){
           throw new AGInvalidStateException
           ('The specified username cannot be registered.');
        }

        if($cleanup)
           $this->cleanup();
    }


    /**
     *  Returns the current user as an instance of the specified role.
     *  
     *  @param unknown $role The new user's role.
     */
    public function as_a($role) {

        $role = strtoupper ($role);
        $new_role = null;

        switch($role){
                case 'PROVIDER':
                        if($this->role != K::ROLE_PROVIDER)
                           throw new AGException('Cannot morph to Provider.');
                        $new_role = new Provider;
                        $new_role->role = K::ROLE_PROVIDER;
                        break;
                case 'CUSTOMER':
                        if($this->role != K::ROLE_CUSTOMER)
           throw new AGException('Cannot morph to Customer.');
                        $new_role = new Customer;
                        $new_role->role = K::ROLE_CUSTOMER;
                        break;
            default:
                throw new AGException('Bad user role : '.$role);
        }
        $new_role->attributes = $this->attributes;
        $new_role->relations = $this->relations;
        $new_role->original = $this->original;
        $new_role->exists = $this->exists;

        return $new_role;
    }
    
    
    /**
     * Get this user's archive.
     * 
     * @return IArchive
     */
    public function archive() {
        if(!$this->archive)
            $this->archive = app('archive')->of($this);
        return $this->archive;
    }


    /**
     *  Convenience function to check whether this user is a customer.
     */
    public function is_customer() {
            return $this->role == K::ROLE_CUSTOMER;
    }

    /**
     *  Convenience function to check whether this user is a provider.
     */
    public function is_provider() {
            return $this->role == K::ROLE_PROVIDER;
    }

    /**
     *  Convenience function to check whether this user is an admin,
     *  including the system user/admin.
     */
    public function is_admin() {
            return $this->role == K::ROLE_ADMIN ||
                   $this->role == K::ROLE_SYSTEM;
    }


    /**
     *  Check whether this user is registered.
     *
     * @return boolean
     */
    public function is_registered() {
            return $this->status == K::USER_REGISTERED;
    }

    /**
     *  Check whether this user is active.
     *
     * @return boolean
     */
    public function is_active() {
            return $this->status == K::USER_ACTIVE;
    }

    /**
     *  Check whether this user is onboarding.
     *
     * @return boolean
     */
    public function is_onboarding() {
            return $this->status == K::USER_ONBOARDING;
    }

    /**
     *  Check whether this user is inactive.
     *
     * @return boolean
     */
    public function is_inactive() {
            return $this->status == K::USER_INACTIVE;
    }

    /**
     *  Check whether this user is suspended.
     *
     * @return boolean
     */
    public function is_suspended() {
            return $this->status == K::USER_SUSPENDED;
    }

    /**
     *  Check whether this user is cancelled.
     *
     * @return boolean
     */
    public function is_cancelled() {
            return $this->status == K::USER_CANCELLED;
    }

    /**
     *  Convenience function to check whether this is a business entity.
     *
     * @return boolean
     */
    public function is_business() {
        return static::is_business_entity($this->entity_type);
    }

    /*
     * Get a human-readable string representing the current user role.
     */
    public function role_to_string() {
            return static::role_name($this->role);
    }


    /**
     *  Convenience function to get a short version of this user's name.
     */
    public function short_name() {
            return $this->first_name && $this->last_name ?
           $this->first_name.' '.$this->last_name[0].'.' :
           'N/A';
    }
    

    /**
     *  Convenience function to get the full name of this user.
     */
    public function full_name() {
            return $this->first_name && $this->last_name ?
           $this->first_name.' '.$this->last_name :
           'N/A';
    }
    
    
    /**
     * Get this user's age.
     * 
     * @return int|null This user's age (in years) or null if the date of birth 
     * is not set.
     */
    public function age() {
        return isset($this->dob) ?
               \DateTime::createFromFormat('d/m/Y', $this->dob)->diff(new \DateTime)->y :
                null;
    }
    
    
    /**
     * Returns the system user.
     */
    public static function sys() {
            // TODO Currently the sys user has id=1. This must obviously change...
            return User::find(User::SYSTEM);
    }


    /**
     *  Convenience function to check whether a user id is an admin.
     */
    public static function is_admin_id($user_id) {
        // Before making a db call, check whether there is an authenticated user 
        // with the given id. If so, then we already have the user record loaded.
        $user = Auth::check() && Auth::user()->id === $user_id ?
                Auth::user() :
                User::find($user_id);

        return $user ?
               $user->role === K::ROLE_ADMIN || $user->role === K::ROLE_SYSTEM : 
               false;
    }


    /**
     * Map role ids to human-readable strings.
     *
     * @param int $role
     * @return string
     */
    public static function role_name($role) {
        
            switch ($role) {
                    case K::ROLE_SYSTEM:   return 'system';
                    case K::ROLE_ADMIN:    return 'admin';
                    case K::ROLE_PROVIDER: return 'provider';
                    case K::ROLE_CUSTOMER: return 'customer';
                    case K::ROLE_GUEST:    return 'guest';
                    default:               return 'unknown';
            }
    }
    
    /**
     * Convenience method to check whether an entity type is business entity.
     * 
     * @param string $type
     * @return boolean
     */
    public static function is_business_entity($type) {
        
        return $type === K::USER_TYPE_SOLETRADER ||
               $type === K::USER_TYPE_COMPANY;
    }

    /**
     * This method verifies that this user's full name matches the given payment
     * method holder's full name. If the user is a business entity then the business
     * name is considered. The matching is done regardless of case, order and spacing.
     *
     * @param User $this
     * @param Payment|Payout $payment
     * @throws AGValidationException
     */
    public function match_payment_names($payment) {
        
        assert($payment instanceof Payment || $payment instanceof Payout);
        $user_name = $this->first_name.' '.$this->last_name;
        $user_business_name = $this->business_name;
        $payment_name = $payment->get_account_holder();
        $match = true;

        $b = $user_business_name ?
             array_filter( explode(' ', trim(strtoupper($user_business_name))) ) : [];
        $u = array_filter( explode(' ', trim(strtoupper($user_name))) );
        $p = array_filter( explode(' ', trim(strtoupper($payment_name))) );
        
        // For non business users, only match the user names
        if(!$user_business_name) {
            if(array_count_values($u) != array_count_values($p))
               $match = false;
        // For business users, match either the user names or the business name
        } else {
            if(array_count_values($u) != array_count_values($p) &&
               array_count_values($b) != array_count_values($p))
               $match = false;
        }
        
        if(!$match) throw new AGValidationException
        ('The provided account holder name does not match your registered names.');
    }
    
    
    /**
     * Get this user's home page URL.
     * 
     * @return string
     */
    public function home() {
        
        if($this->is_provider())
          return URL::to('provider/home');
        else if($this->is_customer())
          return URL::to('customer/home');
        else
          return URL::to('/');
    }
    
    
    /**
     * Get the location of a shared/public resource for this user. The name of a 
     * shared user resource depends on the type. The location has the following format:
     * 
     * <public app files dir>/<user reference>/<resource name>
     * 
     * @param string $type The type of resource.
     * @param string $format The format into which the resource location should 
     * be returned (url, path, name).
     * @param boolean $alt Flag indicating whether an alternative resource location
     * should be returned if the requested one cannot be found.
     * @return string
     */
    public function get_shared_file($type, $format = 'path', $alt = true) {
        
        assert(!empty($this->ref));
        
        $type = strtolower($type);
        
        $app_pub_dir  = \App\Archive::get_local_files_dir(false, true);
        $user_pub_dir = $this->archive()->pub()->get_context();
        $pub_dir      = $app_pub_dir.$user_pub_dir.DIRECTORY_SEPARATOR;
        $root_url     = URL::to('/shared').'/'.$user_pub_dir.'/';
        
        switch($type) {
            
            case 'avatar' :
                $res_name = 'avatar.jpg';
                if($format === 'url' && $alt && !File::exists($pub_dir.$res_name)) {
                   return asset(K::DEFAULT_AVATAR_IMAGE_URI);
                }
                break;
        }
        
        // This archive is switched to public above. Switch it back to private.
        $this->archive()->pvt();
        
        if($format === 'path') return $pub_dir.$res_name; else 
        if($format === 'url') return $root_url.$res_name; else
        if($format === 'name') return $res_name; else
        Hey::bug_here ($format);
    }
    

    /**
     * Store the given files in this user's archive and returns a set of objects
     * representing the stored files.
     *
     * @param array|File $files An array of File objects, or a File object.
     * @param string $context A context in which to store the files.
     * @param string $type Specifies the type of files to be stored and determines
     * the type of returned objects, as follows: 
     * 
     * attachments => Attachment models,
     * documents   => Document models,
     * avatar      => Archive file info record
     * 
     * @param boolean $compress Flag indicating whether the files should be compressed.
     * @param boolean $encrypt Flag indicating whether the files should be encrypted.
     * @return array An array of Attachment or Document models or Archive file info
     * records representing the stored files, if any.
     */
    public function store_files($files, $context = '', $type = 'attachments',
                                $compress = true, $encrypt = true) {

        if(!is_array($files))
           $files = [$files];
        
        if(empty($files)) return [];
        
        $archive = $this->archive();
        
        if($encrypt)  $archive->with_encryption();
        if($compress) $archive->with_compression();

        $records = $archive->store($files, $context);

        if($type === 'attachments') {

           $attachments = array_map(function($a){
              return new Attachment ($a);
           }, $records);

           return $attachments;
        }
        else if($type === 'documents') {

           $docs = array_map(function($d){
              return new Document ($d);
           }, $records);

           return $docs;
        }
        else if($type === 'avatar') {
            
            try {
                $pub_filename = $this->get_shared_file('avatar', 'path', false);
                
                if(!File::exists(File::dirname($pub_filename)))
                    File::makeDirectory(File::dirname($pub_filename),493,true);
                
                $path = $records[0]['path'];

                $type = exif_imagetype($path);

                if($type === IMAGETYPE_PNG)  $img = imageCreateFromPng($path);
                else if($type === IMAGETYPE_JPEG) $img = imageCreateFromJpeg($path);
                else if($type === IMAGETYPE_BMP)  $img = imageCreateFromBmp($path);
                else if($type === IMAGETYPE_GIF)  $img = imageCreateFromGif($path);
                else
                    throw new AGValidationException('Unsupported image file type.');
                
                $min_dim = min(imagesx($img), imagesy($img));
                
                $crop_rect = ['x' => 0.5*(imagesx($img) - $min_dim), 
                              'y' => 0.5*(imagesy($img) - $min_dim),
                              'width' => $min_dim,
                              'height' => $min_dim];
                
                $img_cropped = imagecrop($img, $crop_rect);
                $img_scaled = imagescale($img_cropped, K::MAX_THUMBNAIL_WIDTH);
                imagejpeg($img_scaled, $pub_filename);

                $records[0]['filename'] = File::basename($pub_filename);
                $records[0]['path'] = $pub_filename;
                $records[0]['size'] = File::size($pub_filename);
                $records[0]['mime'] = 'image/jpeg';
                
                return $records;
            }
            finally {
                $archive->remove_contexts('avatar');
            }
        }
        else {
           return $records;
        }
    }

}
