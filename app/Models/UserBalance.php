<?php

namespace App\Models;

class UserBalance extends AGEntity {

	protected $table = 'users_balances';
	protected $primaryKey = 'id_users';
	public    $incrementing = false;
	
	protected $fillable = [
        'id_users',
		'total',
		'available',
            'pending',
	];

	protected $hidden = [
		'id_users',
	];

	public $timestamps = false;
        
        
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }


	///////////////////////////// RELATIONSHIPS /////////////////////////////

	public function user(){
		return $this->belongsTo('App\Models\User', 'id_users');
	}

	/////////////////////////////////////////////////////////////////////////


	public function validate() {
	}

		
}
