<?php

namespace App\Models;

use App\Utils\Validate;


class UserRating extends AGEntity {

	protected $table = 'users_ratings';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		//'id',
		//'id_sender',
		//'id_recipient',
		//'id_tasks',
		'description',
		'score',
	];

	/**
	 * The attributes that should be hidden for arrays returned from
	 * methods of the model.
	 *
	 * @var array
	 */
	protected $hidden = [
		'id',
		'id_sender',
		'id_recipient',
		'id_tasks',
		//'description',
		//'score',
	];


    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }

	///////////////////////////// RELATIONSHIPS /////////////////////////////

	public function sender(){
		return $this->belongsTo('App\Models\User', 'id_sender');
	}
	
	public function recipient(){
		return $this->belongsTo('App\Models\User', 'id_recipient');
	}

	/////////////////////////////////////////////////////////////////////////


	public function validate() {
		Validate::rating_score($this->score);
		Validate::text($this->description, 300);
	}


}
