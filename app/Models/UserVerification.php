<?php

namespace App\Models;

class UserVerification extends AGEntity {

	protected $table = 'users_verifications';
	protected $primaryKey = 'id_users';
	public    $incrementing = false;
	
	protected $fillable = [
            'id_users',
            'ev_token',
            'ev_token_ts',
            'pr_token',
            'pr_token_ts',
	];

	protected $hidden = [
            'id_users',
	];

	public $timestamps = false;
        
        
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }


	///////////////////////////// RELATIONSHIPS /////////////////////////////

	public function user(){
		return $this->belongsTo('App\Models\User', 'id_users');
	}

	/////////////////////////////////////////////////////////////////////////


	public function validate() {
	}

		
}
