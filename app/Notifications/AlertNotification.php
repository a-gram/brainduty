<?php

namespace App\Notifications;

use App\Models\Notification;
use App\K;


class AlertNotification extends Notification {

	/**
	 * Construct a new alert notification with a default
	 * message and no actions.
	 */
	public function __construct($txn = null) {
		 
		$this->type = K::NOTIFICATION_ALERT_EVENT;
		$this->message = 'Some issues occurred in your account.';
	}

}
