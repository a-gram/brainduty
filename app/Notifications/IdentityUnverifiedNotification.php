<?php

namespace App\Notifications;

use App\Models\Notification;
use App\K;


class IdentityUnverifiedNotification extends Notification {

	/**
	 * Construct a new identity unverified notification with a default
	 * message and no actions.
	 *
	 * @param User $user The instance of the subject user.
	 */
	public function __construct($user = null) {
			
		$this->type = K::NOTIFICATION_ACCOUNT_EVENT;
		$this->event = K::IDENTITY_UNVERIFIED_EVENT;
		
                $this->message = trans('messages.identity-unverified');

		if($user) {
			$this->subject_id = $user->username;
			$this->id_users_recipient = $user->id;
		}
	}

}
