<?php

namespace App\Notifications;

use App\Models\Notification;
use App\K;
use App\Hey;


class TaskChargeRefusedNotification extends Notification {

	/**
	 * Construct a new task charge refused notification with a default
	 * message and no actions.
	 * 
	 * @param Task $task The instance of the subject task.
	 */
	public function __construct($task = null) {
		 
		$this->type = K::NOTIFICATION_TASK_EVENT;
		
		if($task) {
                   $this->message = trans('messages.task-charge-refused', [
                         'task-subject' => Hey::truncate_text( $task->subject, 30 )
                   ]);
		   $this->subject_id = $task->ref;
		}
	}

}
