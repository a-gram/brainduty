<?php

namespace App\Notifications;

use App\Models\Notification;
use App\K;
use App\Hey;


class TaskDisputeResolvedNotification extends Notification {

	/**
	 * Construct a new task dispute resolved notification with a default
	 * message and no actions.
	 *
	 * @param Task $task The instance of the subject task.
	 */
	public function __construct($task = null) {
		 
		$this->type = K::NOTIFICATION_TASK_EVENT;
		$this->event = K::TASK_DISPUTE_RESOLVED_EVENT;
		 
		if($task) {
                   $this->message = trans('messages.task-dispute-resolved', [
                         'task-subject' => Hey::truncate_text( $task->subject, 30 )
                   ]);
                   $this->subject_id = $task->ref;
		}
	}

}
