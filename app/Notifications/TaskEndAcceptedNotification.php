<?php

namespace App\Notifications;

use App\Models\Notification;
use App\K;
use App\Hey;


class TaskEndAcceptedNotification extends Notification {

	/**
	 * Construct a new task end accepted notification with a default
	 * message and no actions.
	 *
	 * @param Task $task The instance of the subject task.
	 */
	public function __construct($task = null) {
			
		$this->type = K::NOTIFICATION_TASK_EVENT;
                $this->event = K::TASK_END_REQUEST_ACCEPTED_EVENT;
			
		if($task) {
                        $this->message = trans('messages.task-end-request-accepted', [
                              'task-subject' => Hey::truncate_text( $task->subject, 30 )
                        ]);

			$this->subject_id = $task->ref;
		}
	}

}
