<?php

namespace App\Notifications;

use App\Models\Notification;
use App\K;
use App\Hey;


class TaskRescheduledNotification extends Notification {

	/**
	 * Construct a new task rescheduled notification with a default
	 * message and no actions.
	 *
	 * @param Task $task The instance of the subject task.
	 */
	public function __construct($task = null) {
		 
		$this->type = K::NOTIFICATION_TASK_EVENT;
		$this->event = K::TASK_RESCHEDULED_EVENT;
		 
		if($task) {
                        $this->message = trans('messages.task-rescheduled', [
                              'task-subject' => Hey::truncate_text( $task->subject, 30 )
                        ]);
			$this->subject_id = $task->ref;
		}
	}

}
