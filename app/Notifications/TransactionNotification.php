<?php

namespace App\Notifications;

use App\Models\Notification;
use App\K;


class TransactionNotification extends Notification {

	/**
	 * Construct a new transaction notification with a default
	 * message and no actions.
	 *
	 * @param Transaction $txn The instance of the subject transaction.
	 */
    public function __construct($txn = null) {
    	
    	$this->type = K::NOTIFICATION_TRANSACTION_EVENT;
    	
    	if($txn) {
           $this->message = trans('messages.txn-created', [
                 'txn-type' => $txn->subject
           ]);
    	   $this->subject_id = $txn->ref;
    	}
    }
    
}
