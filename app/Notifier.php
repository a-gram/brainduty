<?php

namespace App;

use App\Models\Notification;
use App\Hey;


/**
 * This class represents the app's notifier. It provides all services
 * related to sending notification messages to users. 
 */
class Notifier {
	
    public function __construct() {
    }

    /**
     * Pushes the given notification to the recipient.
     * Currently, this function simply saves the notification into a
     * database table where it will be retrieved by the user through
     * polling, but something better will be implemented in the future,
     * for example pushing onto websockets or 'push services'.
     * 
     * @param Notification|array $notifications A Notification instance
     * or an array of Notification instances.
     * @param unknown $recipient (optional) The recipient of the notifications.
     * If specified, all the notifications are sent to this recipient.
     */
    public function push($notifications, $recipient = null) {

        $notifications = $notifications ? (is_array($notifications) ?
                         $notifications : [$notifications]) : null;

        if(empty($notifications)) return;

        $records = array_map(function($notification) use ($recipient) {
            $notification->created_at = Hey::now_to_db_datetime();
            $notification->validate();
            if($recipient)
               $notification->to($recipient);
            return $notification->withHidden($notification->getHidden())->toArray();
        }, $notifications);

        Notification::insert($records);
    }
	
}

