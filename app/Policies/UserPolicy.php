<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\User;

/**
 * This class provides the policy to authorize users access to resources.
 * 
 * @author Albert
 *
 */
class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct() {
    }
    
    
    /**
     * The admin users are all authorized for any policy.
     * 
     * @param unknown $user
     * @param unknown $ability
     * @return boolean
     */
    public function before($user, $ability)
    {
    	if ($user->is_admin()) {
    		return true;
    	}
    }
    
    
    /**
     * Authorize access to all admins.
     *
     * @param User $user
     * @return bool
     */
    public function admin(User $user) {
    	return $user->is_admin();
    }
    
    
    /**
     * Authorize access to all customers.
     * 
     * @param User $user
     * @return bool
     */
    public function customer(User $user) {
   	    return $user->is_customer();
    }

    
    /**
     * Authorize access to all providers.
     *
     * @param User $user
     * @return bool
     */
    public function provider(User $user) {
    	return $user->is_provider();
    }
    
    
    /**
     * Authorize access to all providers and customers.
     *
     * @param User $user
     * @return bool
     */
    public function provider_or_customer(User $user) {
    	return $user->is_provider() || $user->is_customer();
    }
    
}
