<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Queue\Events\JobProcessed;
use App\AccountManager;
use App\Account;
use App\Archive;
use App\Notifier;
use App\Marketplace;
use App\Services;
use App\TaskManager;
use App\IdentityManager;
use App\Messages\MessageFactory;
use App\Messages\Actions\MessageActionMap;
use Queue;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Queue::after(function (JobProcessed $job) {
            //$event = unserialize($job->data['data']['data'])[0];
            //$event->on_after_event();
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    	$this->app->singleton('account.manager', function () {
            return new AccountManager;
    	});

    	$this->app->bind('account', function () {
            return new Account;
    	});
    	
        $this->app->bind('archive', function () {
            return new Archive;
        });
    	
    	$this->app->singleton('notifier', function () {
            return new Notifier;
    	});
    	
        $this->app->singleton('message', function () {
            return new MessageFactory;
        });

        $this->app->singleton('message.action', function () {
            return new MessageActionMap;
        });

        $this->app->singleton('marketplace', function () {
            return new Marketplace;
        });

        $this->app->bind('task.manager', function () {
            return new TaskManager;
        });
        
        $this->app->bind('identity.manager', function () {
            return new IdentityManager;
        });
        
        $this->app->singleton('services', function () {
            return new Services;
        });

    }
}
