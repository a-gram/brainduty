<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\User;
use App\Policies\UserPolicy;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     * 
     * NOTE: When checking for a policy (a set of policy methods defined in
     *       a xxxPolicy class) the mapping is performed by checking the second
     *       argument of the 'check methods' (allows(), can(), authorize(), etc.)
     *       against the below map. For example, in Gate::allows('delete', $task)
     *       the $task instance class is used to lookup the corresponding Policy
     *       class with a policy method delete().
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        //
    }
}
