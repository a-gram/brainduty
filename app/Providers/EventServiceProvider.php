<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
    	
        
        /********************************************************
         *                    ACCOUNT EVENTS
         ********************************************************/
        

    	/*
    	 * Event User Signup
    	 * 
    	 * This event is fired when a user signs up to the platform.
    	 * 
    	 */
        'App\Events\EventUserSignup' => [
            'App\Listeners\OnUserSignup',
        ],
        
        
    	/*
    	 * Event User Rating
    	 * 
    	 * This event is fired when a user gives a rating/feedback to another
         * user of the platform.
    	 * 
    	 */
        'App\Events\EventUserRating' => [
            'App\Listeners\OnUserRating',
        ],
        
        
    	/*
    	 * Event User Identity Verification
    	 * 
    	 * This event is fired when a user starts the identity verification process.
    	 * 
    	 */
        'App\Events\EventUserIdentityVerification' => [
            'App\Listeners\OnUserIdentityVerification',
        ],


    	/*
    	 * Event User Cancelled
    	 * 
    	 * This event is fired when a user cancels its account.
    	 * 
    	 */
        'App\Events\EventUserCancelled' => [
            'App\Listeners\OnUserCancelled',
        ],

        
        /********************************************************
         *                     TASK EVENTS
         ********************************************************/


    	/*
    	 * Event Task Scheduled
    	 * 
    	 * This event is fired when a task is requested and scheduled
    	 * in the system. The associated handler will assign the task
    	 * to a matched provider according to the specified criterion.
    	 * 
    	 */
        'App\Events\EventTaskScheduled' => [
            'App\Listeners\OnTaskScheduled',
        ],
    	
    		
    	/*
    	 * Event Task Ended
    	 * 
    	 * This event is fired when a task is ended. The associated
    	 * handler may charge the customer for the services used
    	 * for the task.
    	 */
    	'App\Events\EventTaskEnded' => [
            'App\Listeners\OnTaskEnded',
    	],
    	
    	
        /*
         * Event Task Cancelled
         *
         * This event is fired when a task is cancelled. The associated
         * handler will charge cancellation fees, if any, and close the task.
         */
        'App\Events\EventTaskCancelled' => [
            'App\Listeners\OnTaskCancelled',
        ],
        
    	
    	/*
    	 * Event Task Completed
    	 * 
    	 * This event is fired when a task is successfully completed. The associated
    	 * handler may charge the customer for the services used, issue a receipt,
         * release provider funds, etc.
    	 */
    	'App\Events\EventTaskCompleted' => [
            'App\Listeners\OnTaskCompleted',
    	],
        
    	
        /*
         * Event Task Cancel Request
         *
         * This event is fired when a mutual cancellation request is sent
         * to a task.
         */
        'App\Events\EventTaskCancelRequest' => [
            'App\Listeners\OnTaskCancelRequest',
        ],
        
        
        /*
         * Event Task End Request
         *
         * This event is fired when a provider sends an end request to a task.
         */
        'App\Events\EventTaskEndRequest' => [
            'App\Listeners\OnTaskEndRequest',
        ],

        
        /********************************************************
         *             EXTERNAL MARKETPLACE EVENTS
         ********************************************************/

        
        /*
         * Event Account Updated.
         *
         * This event is fired when the platform receives a webhook call
         * from the marketplace provider for a provider's account update.
         */
        'App\Events\EventMarketplaceAccountUpdated' => [
            'App\Listeners\OnMarketplaceAccountUpdated',
        ],
        
        /*
         * Event Balance Available.
         *
         * This event is fired when the platform receives a webhook call
         * from the marketplace provider upon clearance of pending payment
         * transactions that become available in the balance for withdrawal.
         */
        'App\Events\EventMarketplaceBalanceAvailable' => [
            'App\Listeners\OnMarketplaceBalanceAvailable',
        ],
        
        /*
         * Event Withdrawal Created.
         *
         * This event is fired when the platform receives a webhook call
         * from the marketplace provider upon creation of a transfer to the
         * provider's bank account (withdrawal).
         */
        'App\Events\EventMarketplaceWithdrawalCreated' => [
            'App\Listeners\OnMarketplaceWithdrawalCreated',
        ],

        /*
         * Event Withdrawal Deposited.
         *
         * This event is fired when the platform receives a webhook call
         * from the marketplace provider upon deposit of a withdrawn amount
         * in the destination bank account.
         */
        'App\Events\EventMarketplaceWithdrawalDeposited' => [
            'App\Listeners\OnMarketplaceWithdrawalDeposited',
        ],

        /*
         * Event Withdrawal Failed.
         *
         * This event is fired when the platform receives a webhook call
         * from the marketplace provider upon failure of a withdrawal.
         */
        'App\Events\EventMarketplaceWithdrawalFailed' => [
            'App\Listeners\OnMarketplaceWithdrawalFailed',
        ],

    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
