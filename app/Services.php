<?php

namespace App;

use App\Models\Service;


/**
 * This class is just a container for all of the services provided by the app
 * (as in services provided to customers). Since services are pretty much invariant
 * or sort of, they're cached here and bound to the service container as a singleton.
 * We can then use it to get them rather than make database calls everytime.
 */
class Services {
    
    protected $cached;
    
    public function __construct() {
        $this->cached = Service::get_all();
    }
    
    public function get() {
        return $this->cached;
    }
}
