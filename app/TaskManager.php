<?php

namespace App;

use App\Interfaces\ITaskLifecycle;
use App\Models\Task;
use App\Models\Service;
use App\Models\User;
use App\Models\Attachment;
use App\Events\EventTaskCancelled;
use App\Events\EventTaskEnded;
use App\Events\EventTaskScheduled;
use App\Events\EventTaskCompleted;
use App\Notifications\TaskCancelledNotification;
use App\Notifications\TaskEndedNotification;
use App\Notifications\TaskDisputedNotification;
use App\Notifications\TaskCompletedNotification;
use App\Exceptions\AGException;
use App\Exceptions\AGAuthorizationException;
use App\Exceptions\AGInvalidStateException;
use App\Exceptions\AGValidationException;
use App\Hey;
use App\Utils\Validate;
use App\K;
use App,Event,DB;


/**
 * An implementation of the ITaskLifecycle interface.
 * 
 * @author Albert
 *
 */
class TaskManager implements ITaskLifecycle {
	
	
    /**
     * The instance of the task.
     * 
     * IMPORTANT! THE TASK RECORD MUST BE ALREADY LOCKED WITHIN A
     *            DATABASE TRANSACTION BEFORE USING THIS CLASS METHODS.
     * 
     * @var Task (optional)
     */
    protected $task;

    /**
     * The user who initiated the action.
     *  
     * @var User
     */
    protected $requestor = null;

    /**
     * The event to be fired when changing state.
     * 
     * @var Event (optional)
     */
    protected $event = null;

    /**
     * The notification to be pushed when changing state.
     * 
     * @var Notification (optional)
     */
    protected $notification = null;

    /**
     * The notifier instance
     * 
     * @var Notifier
     */
    protected $notifier;



    public function __construct($task = null) {
        $this->task = $task;
        $this->notifier = app('notifier');
    }

    // SETTERS

    public function set_task($task) {
        $this->task = $task;
        return $this;
    }

    public function set_requestor($user) {
        $this->requestor = $user;
        return $this;
    }

    public function set_event($event) {
        $this->event = $event;
        return $this;
    }

    public function set_notification($notification) {
        $this->notification = $notification;
        return $this;
    }
    
    // GETTERS
    
    public function get_task() {
        return $this->task;
    }
    
    public function get_requestor() {
        return $this->requestor;
    }
    
    public function get_event() {
        return $this->event;
    }
    
    public function get_notification() {
        return $this->notification;
    }

    /**
     * Get a managed task.
     * 
     * @param Task|string|int $task
     */
    public function of($task) {
        
        $this->task = $task instanceof Task ? $task : 
                      $this->get_task_record($task);
        return $this;
    }

    
    // INTERFACE


    /**
     * Schedule a task from a client request.
     * 
     * @param array $request An array with the request data needed to schedule
     * the task having the following format:
     * [
     *    task => [
     *        type     =>
     *        catags   =>
     *        subject  =>
     *        assignee =>
     *        attachs  =>
     *    ],
     *    sender => <the customer sending the request>,
     *    rescheduled => <the reschedule task, if applicable>,
     *    message => <the transactional message for the task request>
     * ]
     * 
     * @return Task
     * @throws AGValidationException
     * @throws Exception
     * @throws Throwable
     */
    public function schedule(&$request) {

        $message = $request['message'];
        $rescheduled = Hey::getval($request, 'rescheduled');

        // Validate message data
        if(!isset($request['task']['type'],
                  $request['task']['catags'],
                  $request['task']['subject']))
                  throw new AGValidationException('Missing fields in message data');

        // If the task is rescheduled check that we get a Task and there is only
        // the task request message loaded (the MESSAGE_TASK_REQUEST type).
        if($rescheduled) {
           assert($rescheduled instanceof Task &&
                  $rescheduled->relationLoaded("messages") &&
                  $rescheduled->messages->count() === 1);
        }
        
        
        // ---------- Deal with the customer and payment ------------

        $customer = $request['sender']->as_a('customer');
        
        // Only active customers can request tasks
        if(!$customer->is_active())
            throw new AGAuthorizationException
            ('Your account must be activated in order to use this feature.');

        $payment = $customer->default_payment_method();

        if(!$payment) {
            // TODO Handle here the case where customer has no payment method
            // on file. Can still request a task ?
        }

        // If the customer has specified a specific provider to be assigned
        // to the task and this is not a reschedule then the request should contain 
        // the user reference (for reschedules we already have the provider id).
        if(!empty($request['task']['assignee']) && !$rescheduled) {
            // Lookup the provider's id from the reference.
            $assignee = User::where('ref', $request['task']['assignee'])->
                              select(['id','role'])->
                              first();
            // TODO Check whether the favourite provider is actually in the
            //      customer's favourites list ?
            if(!$assignee || !$assignee->is_provider())
                throw new AGException;
            $request['task']['assignee'] = $assignee->id;
        }

        // ---------------- Deal with the task then -----------------

        Hey::trim_record($request['task']);

        $task = new Task ($request['task']);
        $task->ref = Task::make_ref($task);
        $task->status = K::TASK_SCHEDULED;
        $task->schedule_datetime = Hey::now_to_db_datetime();
        $task->is_funded = $payment !== null;
        $task->is_managed = $customer->settings->ta_method === K::TASK_ASSIGNMENT_MANAGED;
        
        $base_services = $task->make_services($request['task']);

        // NOTE: we need to assign the task to a provider in order to keep
        // the relationship integrity, so we initially assign it to the system
        // user. This would still mean it is not yet assigned to a provider.
        $task->id_users_provider = User::sys()->id;

        $task->validate();

        try {
            DB::beginTransaction();

            $customer->tasks()->save($task);
            $task->messages()->save($message);
            $task->services()->attach($base_services);
            
            // If we are creating a rescheduled task then there will be no posted
            // attachments in the request so we have to copy the original ones.
            if( $rescheduled ) {
                $attachments = $rescheduled->messages->first()->attachments->all();
                $attachments = $rescheduled->copy_archive($task, $attachments);
                $rescheduled->is_rescheduled = true;
                $rescheduled->save();
            } else {
                $attachments = $task->store_files($request['task']['attachs']);
            }
            
            $message->attachments()->saveMany($attachments);
            $message->add('attachments', $attachments);
            
            // Fire an event to queue a job that will take care of
            // following up on this task and assign it to a provider.
            Event::fire(new EventTaskScheduled($task->id));

            DB::commit();
        }
        catch (Exception $exc) {
            DB::rollBack();
            $task->rollback_archive();
            throw $exc;
        }
        catch (Throwable $exc) {
            DB::rollBack();
            $task->rollback_archive();
            throw $exc;
        }
        
        return $task;
    }


    /**
     * Start the specified task by assigning it to a service provider.
     * 
     * IMPORTANT! This method must be called within a database transaction.
     * 
     * @param unknown $task_ref The reference of the task to be ended.
     * If not supplied, then the task instance in the $task property will
     * be used.
     * @return Task
     */
    public function start($task_ref = null) {
        // Tasks are currently started by the system when assigned.
    }


    /**
     * End the specified task and take/schedule any consequent actions.
     * Note that any action associated with the ending may be taken
     * either synchronously in this method or scheduled as an event/job.
     * This method is called by customers (if the accept the task end request)
     * or by the system (if the task end request expires).
     *  
     * IMPORTANT! This method must be called within a database transaction.
     *  
     * @param unknown $task_ref The reference of the task to be ended.
     * If not supplied, then the task instance in the $task property will
     * be used.
     * @return Task
     * 
     * @throws AGResourceNotFoundException
     * @throws AGAuthorizationException
     * @throws AGInvalidStateException
     */
    public function end($task_ref = null) {

        $task = $this->get_task_record($task_ref);

        // The requesting user must always be present
        if(!$this->requestor)
            throw new AGException('No requestor set.');

        if(!$task->has_actor($this->requestor->id))
            throw new AGAuthorizationException;

        if($task->status == K::TASK_ONGOING) {
            // Actions for ending the task will occur in the OnTaskEnded handler.
            if(!$this->event)
                $this->event = new EventTaskEnded($task->id);
        }
        else {
            throw new AGInvalidStateException('Cannot end the task.');
        }

        // End the task
        $task->status = K::TASK_ENDED;
        $task->end_datetime = Hey::now_to_db_datetime();
        $task->save();

        if($this->event)
           Event::fire($this->event);

        // Notify the other party.
        if(!$this->notification) {
            $this->notification = new TaskEndedNotification($task);
            $this->notification->to($task->id_users_provider);
        }

        $this->notifier->push($this->notification);

        return $task;
    }


    /**
     * Cancel the specified task and take/schedule any consequent actions.
     * Note that any action associated with the cancellation may be taken
     * either synchronously in this method or scheduled as an event/job.
     * This method is called by providers, customers and the system user.
     *  
     * IMPORTANT! This method must be called within a database transaction.
     *  
     * @param unknown $task_ref The reference of the task to be cancelled.
     * If not supplied, then the task instance in the $task property will
     * be used.
     * @return Task
     * 
     * @throws AGResourceNotFoundException
     * @throws AGAuthorizationException
     * @throws AGInvalidStateException
     */
    public function cancel($task_ref = null) {

        $task = $this->get_task_record($task_ref);

        // The requesting user must always be present
        if(!$this->requestor)
            throw new AGException('No requestor set.');

        if(!$task->has_actor($this->requestor->id))
            throw new AGAuthorizationException;
        

        // Scheduled tasks can be cancelled straight away by customers only.
        if($task->status == K::TASK_SCHEDULED) {
            if(!$this->requestor->is_customer())
                throw new AGAuthorizationException;

            $task->notes = 'Cancelled by customer on'.Hey::now_to_db_datetime();
        }
        // Ongoing tasks can be cancelled by both customers and providers
        // according to the cancellation policy.
        else if($task->status == K::TASK_ONGOING) {
            // Tasks that are being ended cannot be cancelled
            if($task->is_ending)
                throw new AGInvalidStateException(trans('exceptions.task-ending'));
            // Actions for cancelling the task will occur in the OnTaskCancelled handler.
            if(!$this->event)
                $this->event = new EventTaskCancelled($task->id,
                                                      $task->status,
                                                      $this->requestor->id);			    		 
        }
        // Disputed tasks can only be cancelled by an admin user.
        else if($task->status == K::TASK_DISPUTED) {
            if(!$this->requestor->is_admin())
                throw new AGAuthorizationException;
                    // TODO:
            $task->notes = 'Cancelled by admin on '.Hey::now_to_db_datetime();
        }
        else {
            throw new AGInvalidStateException('Cannot cancel the task.');
        }

        // Cancel the task
        $task->status = K::TASK_CANCELLED;
        $task->end_datetime = Hey::now_to_db_datetime();
        $task->save();

        if($this->event)
           Event::fire($this->event);

        // Notify the other party.
        if(!$this->notification) {
            $this->notification = new TaskCancelledNotification($task);
            $this->notification->to( $this->requestor->id == $task->id_users_customer ?
                                                     $task->id_users_provider :
                                                     $task->id_users_customer );
        }

        $this->notifier->push($this->notification);

        return $task;
    }


    /**
     * Dispute the specified task.
     * This method is called by customers.
     *  
     * IMPORTANT! This method must be called within a database transaction.
     *  
     * @param unknown $task_ref The reference of the task to be disputed.
     * If not supplied, then the task instance in the $task property will
     * be used.
     * @return Task
     * 
     * @throws AGResourceNotFoundException
     * @throws AGException
     * @throws AGAuthorizationException
     * @throws AGInvalidStateException
     */
    public function dispute($task_ref = null) {

        $task = $this->get_task_record($task_ref);

        // The requesting user must always be present
        if(!$this->requestor)
            throw new AGException('No requestor set.');

        if(!$task->has_actor($this->requestor->id))
            throw new AGAuthorizationException;
        
        if(!$task->is_disputable())
            throw new AGInvalidStateException(trans('exceptions.task-undisputable'));

        // Tasks can only be disputed by customers when ended.
        if($task->status == K::TASK_ENDED) {
            if(!$this->requestor->is_customer())
                throw new AGAuthorizationException;
        }
        else {
            throw new AGInvalidStateException(trans('exceptions.task-undisputable'));
        }

        $task->status = K::TASK_DISPUTED;
        $task->save();

        if($this->event)
           Event::fire($this->event);

        // Notify the other party.
        if(!$this->notification) {
            $this->notification = new TaskDisputedNotification($task);
            $this->notification->to($task->id_users_provider);
        }

        $this->notifier->push($this->notification);

        return $task;
    }


    /**
 * This method checks whether the given task can be set as completed (closed)
 * by verifying that the dispute period has elapsed and completing the payment,
 * if necessary (e.g. capturing uncaptured funds).
 * 
     * @param unknown $task_ref The reference of the task to be closed.
     * If not supplied, then the task instance in the $task property will
     * be used.
     * @return Task|null
     * 
     * @throws AGResourceNotFoundException
     * @throws AGException
     * @throws AGAuthorizationException
     * @throws AGInvalidStateException
     */
    public function close($task_ref = null) {

        $task = $this->get_task_record($task_ref);

        // The requesting user must always be present
        if(!$this->requestor)
            throw new AGException('No requestor set.');

        // Tasks can only be closed by the system user/admin when ended or disputed.
        if(!$this->requestor->is_admin())
            throw new AGAuthorizationException;

        if($task->status == K::TASK_ENDED) {
            // If the task is still disputable it cannot be closed and we need to
            // signal this to the caller by returning null (or throwing something)
            // else the task is completed and can be closed.
            if($task->is_disputable())
               return null;
            
            if(!$this->event)
                $this->event = new EventTaskCompleted($task->id);
        }
        else if($task->status == K::TASK_DISPUTED) {
            // If the task is disputed then it will be closed by an admin if
            // resolved to the provider.
        }
        else {
            throw new AGInvalidStateException('Cannot close the task.');
        }

        $task->status = K::TASK_COMPLETED;
        $task->complete_datetime = Hey::now_to_db_datetime();
        $task->save();

        if($this->event)
           Event::fire($this->event);

        // Notify the parties about the task completion.
        if(!$this->notification) {
            $customer_notification = new TaskCompletedNotification($task);
            $provider_notification = new TaskCompletedNotification($task);
            $customer_notification->to($task->id_users_customer);
            $provider_notification->to($task->id_users_provider);
            $this->notification = [$customer_notification, $provider_notification];
        }

        $this->notifier->push($this->notification);

        return $task;
    }
    
    
    // HELPERS
    
    
    /**
     * Get the task record.
     * 
     * @param string $task_ref
     * @return Task
     * @throws AGResourceNotFoundException
     */
    protected function get_task_record($task_ref) {
        
        $task = isset($task_ref) ?
                Task::where('ref', $task_ref)->lockForUpdate()->first() :
                $this->task;

        if(!$task)
            throw new AGException('Task not found', [$task_ref]);
        
        return $task;
    }
    

}
