<?php

namespace App\Utils;

use App\Exceptions\AGException;
use App\Exceptions\AGValidationException;
use App\K;
use App\Hey;

// TODO Validator::validate($field)->as('rule');

abstract class Validate {

    /**
     * Validate an email address.
     *
     * @param string $email The email address to be validated.
     * @throws Exception if the email address is invalid.
     */
    public static function email($email) {
            if(!isset($email)) return;
            $email = filter_var($email, FILTER_SANITIZE_EMAIL);
            if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    throw new AGValidationException('Invalid email address provided : '.$email);
            }
    }

    /**
     * Validate a date of birth according to the format DD/MM/YYYY.
     *
     * @param string $dob The date to be validated.
     * @throws Exception if the date is invalid.
     */
    public static function dob($dob) {
            if(!isset($dob)) return;
            $d = \DateTime::createFromFormat('d/m/Y', $dob);
            if($d && $d->format('d/m/Y') == $dob) {}
            else throw new AGValidationException('Invalid date of birth '.$dob.
                            '. Must be a valid date in the format DD/MM/YYYY.');
    }

    /**
     * Validate a generic name (first name, last name, street name, etc.).
     *
     * @param string $name The name to be validated.
     * @throws Exception if the name is invalid.
     */
    public static function name($name) {
            if(!isset($name)) return;
            if(!preg_match("/^[a-zA-Z0-9 \/'.\-&]{1,64}$/", $name)) {
                    throw new AGValidationException("Invalid name : ".$name." - ".
                                    "Names can only contain letters, numbers and /'.-&");
            }
    }

    /**
     * Validate a username. Usernames must be valid email addresses.
     *
     * @param string $username The username to be validated.
     * @throws Exception if the username is invalid.
     */
    public static function username($username) {
            if(!isset($username)) return;
            try {
                    self::email($username);
            } catch (AGValidationException $exc) {
                    throw new AGValidationException('The username must be a valid email address.');
            }
    }

    /**
     * Validate a password.
     *
     * @param string $password The password to be validated.
     * @throws Exception if the password is invalid.
     */
    public static function password($password) {
            if(!isset($password)) return;
            if(!preg_match('/^[a-zA-Z0-9!?@#$%&(){}]{'.K::MIN_PASSWORD_LENGTH.',64}$/', $password)) {
                    throw new AGValidationException('Passwords must be minimum '.K::MIN_PASSWORD_LENGTH.' characters. '.
                                    'Allowed characters are letters, numbers and the following symbols  !?@#$%&(){}.');
            }
    }

    /**
     * Validate a phone number.
     *
     * @param string $number The number to be validated.
     * @throws Exception if the number is invalid.
     */
    public static function phone($number) {
            if(!isset($number)) return;
            if(!preg_match('/^\d{10,12}$/', $number)) {
                    throw new AGValidationException('Invalid phone number.');
            }
    }


    /**
     * Validate a postal code.
     *
     * @param string $code The code to be validated.
     * @throws Exception if the code is invalid.
     */
    public static function postal_code($code) {
            if(!isset($code)) return;
            if(!preg_match('/^\d{4}$/', $code)) {
                    throw new AGValidationException('Invalid postal code.');
            }
    }

    /**
     * Validate user entity type (individual|soletrader|company).
     * 
     * @param string $type
     */
    public static function entity_type($type) {
        if(!in_array($type, K::$USER_TYPE))
           throw new AGException('Invalid entity type : '.$type);
    }
    
    /**
     * This method checks that the provided business info are consistent
     * and valid.
     * 
     * @param string $type The entity type
     * @param string $name The business name, if entity is business
     * @param string $number The business number, if entity is business
     */
    public static function business_info($type, $name, $number) {

        if(!isset($type) && !isset($name) && !isset($number)) return;

        self::entity_type($type);
        
        // Business entity
        if(\App\Models\User::is_business_entity($type)) {
           if(empty($name) || empty($number))
              throw new AGValidationException
              ('Business name and number are required for business entities.');
           self::name($name);
           self::business_number($number);
        }
    }
    
    /**
     * Validate a business number.
     * 
     * @param type $number
     * @return type
     * @throws AGValidationException
     */
    public static function business_number($number) {
            if(!isset($number)) return;
            if(!preg_match('/^\d{9}$|^\d{11}$/', $number)) {
                    throw new AGValidationException('Invalid business number.');
            }
    }

    /**
     * Validate a full address that includes address, city, zip code, state and country.
     *
     * @param array $address An array containing the full address fields.
     * @throws Exception if any fields of the address are invalid.
     */
    public static function full_address(array $address) {
            if(!isset($address)) return;
            $address_fields = ['address', 'city', 'zip_code', 'state', 'country'];
            if(!Hey::has_field($address_fields, $address, 'all', false))
                    throw new AGException('Missing fields in the address.');
            self::name($address['address']);
            self::name($address['city']);
            self::postal_code($address['zip_code']);
            self::name($address['state']);
            self::name($address['country']);
    }
    
    /**
     * Validate user's age.
     * 
     * @param int $age The age in years.
     * @throws AGValidationException
     */
    public static function age($age) {
        if(!isset($age)) return;
        if($age < Hey::get_app('legal_age'))
            throw new AGValidationException(trans('exceptions.user-not-of-legal-age',[
                'legal-age' => Hey::get_app('legal_age')
            ]));
    }

    /**
     * Validate a provider's payout settings.
     *
     * @param array $settings An array containing the payment settings.
     * @throws Exception if any fields of the settings are invalid.
     */
    public static function payout($settings) {
            if(!isset($settings)) return;
            if(!is_array($settings))
                    throw new AGException('Invalid parameters : $settings is not an array.');
                    self::name($settings['account_name']);
                    self::payout_account_id($settings['account_id']);
                    //self::alphanumeric($settings['account_number']);
                    //self::alphanumeric($settings['account_host']);
    }

    /**
     * Validate payout method type.
     */
    public static function payout_method($type) {
            if(!isset($type)) return;
            if (!in_array($type, Hey::get_app('payout.methods')))
                    throw new AGException('Invalid payout method.');
    }

    /**
     * Validate payout account id/number. This is usually a bank account
     * number, but since we use tokenized payment info this string depends
     * on the token format and is already validated by the payment processor
     * so just do a loose verification that it's at least 5 characters.
     */
    public static function payout_account_id($id) {
            if(!isset($id)) return;
            if (strlen($id)<5) {
                    throw new AGValidationException('Invalid payout account id.');
            }
    }

    /**
     * Validate a payout method holder name.
     */
    public static function payment_account_name($name) {
            self::name($name);
    }

    /**
     * Validate payment method type.
     */
    public static function payment_method($type) {
            if(!isset($type)) return;
            if (!in_array($type, Hey::get_app('payment.methods')))
                    throw new AGException('Invalid payment method.');
    }

    /**
     * Validate payment cycle types (one-time and recurring)
     */
    public static function payment_cycle($cycle) {
            if(!isset($cycle)) return;
            if (!in_array($cycle, K::PAYMENT_CYCLE))
                    throw new AGException('Invalid payment cycle type.');
    }

    /**
     * Validate payment source id/number. This is usually a card/account
     * number, but since we use tokenized payment info this string depends
     * on the token format and is already validated by the payment processor
     * so just do a loose verification that it's at least 5 characters.
     */
    public static function payment_source_id($id) {
            if(!isset($id)) return;
            if (strlen($id)<5) {
                    throw new AGValidationException('Invalid payment source id.');
            }
    }

    /**
     * Validate a payment method holder name. 
     */
    public static function payment_source_name($name) {
            self::name($name);
    }

    /**
     * Validate a generic alphanumeric string.
     */
    public static function alphanumeric($string, $maxlen=100) {
            if(!isset($string)) return;
            if (!preg_match('/^[\w ]{1,'.$maxlen.'}$/', $string)) {
                    throw new AGValidationException('Invalid alphanumeric string.');
            }
    }

    /**
     * Validate a text with only alphanumeric and common punctuation characters.
     */
    public static function text($text, $maxlen=100) {
            if(!isset($text)) return;
            if(!preg_match('/^[\w ,.:;!?\'()%&$]{1,'.$maxlen.'}$/', $text)) {
                    throw new AGValidationException('Invalid text.');
            }
    }

    /**
     *  Validate the provider's account activation info.
     *  
     *  @param array $info An array of info as follows
     *
     * [
     *     entity    => <provider entity type>
     *     dob       => <provider date of birth>
     *     names     => [
     *           first_name => <provider first name>
     *           last_name  => <provider last name>
     *     ]
     *     business  => [
     *           business_name => <provider business name>
     *           business_number => <provider business number>
     *     ]
     *     bank  => [
     *           method     => <payout account method>
     *           acc_holder => <payout account holder>
     *           acc_id     => <payout account id/number>
     *     ]
     *     address  => [
     *           address    => <provider address>
     *           city       => <provider city>
     *           state      => <provider state>
     *           zip_code   => <provider post code>
     *           country    => <provider country>
     *     ]
     *     documents  => [
     *           0 => [
     *                  type     => <type of document>
     *                  filename => <original file name>
     *                  mime     => <mme type>
     *                  (token    => <document file token>)
     *           ]
     *           1 => [...]
     *           2 => [...]
     *               ...
     *     ]
     *     docs => [
     *          files => [...]
     *     ]
     * ]
     *  @throws AGValidationException
     */
    public static function activation_info(array $info) {
        
        if(empty($info)) return;
        
        if(isset($info['entity'])) {
           self::entity_type($info['entity']);
        }
        if(isset($info['dob'])) {
           self::dob($info['dob']);
        }
        if(isset($info['names'])) {
           self::name($info['names']['first_name']);
           self::name($info['names']['last_name']);
        }
        if(isset($info['business'])) {
           self::name($info['business']['business_name']);
           self::name($info['business']['business_number']);
        }
        if(isset($info['bank'])) {
           self::name($info['bank']['account_name']);
           self::payout_account_id($info['bank']['account_id']);
           self::payout_method($info['bank']['method']);
        }
        if(isset($info['address'])) {
           self::full_address($info['address']);
        }
        if(isset($info['documents'])) {
           self::identity_documents($info['documents']);
        }
        self::attachments(count($info['docs']) ? $info['docs']['files'] : []);
    }

    /**
     * Validate an identity document record.
     * 
     * @param array|null $documents
     * @throws AGException
     */
    public static function identity_documents($documents) {
        if(!isset($documents)) return;
        if(!is_array($documents))
           throw new AGException('Invalid argument: documents');
        foreach ($documents as $document) {
            if(!is_array($document))
                throw new AGException('Invalid element in: documents[]');
            if(!isset($document['type']))
                throw new AGException('Missing field "type" in documents[n]');
        }
    }

    /**
     * Validate a provider's services (and categories, if any).
     * 
     * @param string $list An array of services and related categories, if any,
     * [ <service id> => <categories list> ]
     */
    public static function services($list) {
        
        if(!isset($list))
            throw new AGException('No services provided.');
        
        // Get the platform's services and categories.
        $services = app('services')->get();

        foreach ($list as $service => $categories) {
            // Check that service is valid first.
            if(!array_key_exists($service, $services))
               throw new AGException('Invalid service : '.$service);
            // Get the categories ids associated with this service (these are the
            // keys of the categories array).
            $service_categories = array_keys($services[$service]['categories']);
            // Service categories may be provided as a comma separated list string,
            // so convert it to an array if that's the case.
            if(is_string($categories)) {
               $categories = $categories ? explode(',', $categories) : [];
            }
            // If the service has categories then at least one must be included.
            if(count($service_categories) && !count($categories))
               throw new AGValidationException('At least one category must be selected.');
            // Is there a max allowed?
            if(count($service_categories) &&
               count($categories) > K::MAX_PROVIDER_SERVICE_CATEGORIES)
               throw new AGValidationException('Max number of categories exceeded.');
            // Now check the categories ids validity.
            foreach ($categories as $category) {
                if(!in_array($category, $service_categories, true))
                    throw new AGException('Invalid category : '.$category);
            }
        }
    }


    /**
     * Validate the given attachment files by verifying that they are in the
     * allowed number, size, format, etc.
     *
     * @param array $files An array of File objects.
     * @param string $context (Optional) The context in which to make the verification.
     * Aside from rules that apply to attachments globally, some may apply
     * depending on where they are used. If no context is given, then only the global
     * rules are verified.
     */
    public static function attachments(array $files, $context = null) {
        
        if(empty($files)) return;
        // Global rules verifications. These apply to all attachments
        // regardless of context.
        $total_size = 0;
        foreach ($files as $file) {
//            if(!$file->isValid())
//                throw new AGValidationException
//                ('Invalid file : '.$file->getClientOriginalName().'. '.$file->getErrorMessage());
            if(!in_array($file->getMimeType(), Hey::get_app('file_mime_types')))
                throw new AGValidationException('File type not allowed.');
            $total_size += $file->getSize() / 1E6;
        }
        if($total_size > K::MAX_ATTACHS_SIZE_IN_MB)
           throw new AGValidationException
           ('Max upload size exceeded ('.K::MAX_ATTACHS_SIZE_IN_MB.' MB)');

        if(!is_null($context)) {
           switch ($context) {
                // Validate Message attachments
                case 'message':
                    // Verify number of attachments
                    if(count($files) > K::MAX_ATTACHS_PER_MESSAGE)
                        throw new AGValidationException
                        ('Maximum allowed number of attachments exceeded ('.
                          K::MAX_ATTACHS_PER_MESSAGE.')');
                    break;
                case 'avatar':
                    if(!in_array($files[0]->getMimeType(), ['image/png',
                                                            'image/jpeg',
                                                            'image/jpg']))
                        throw new AGValidationException
                        ('Avatar must be a png or jpeg file.');
                    break;
                default:
                    throw AGException('Unknown context : '.$context);
           }
        }
    }


    /**
     * Validate a message body.
     */
    public static function message_body($body) {
            if(!isset($body)) return;
            if (strlen($body) > K::MAX_CHAT_MESSAGE_LENGTH) {
                    throw new AGValidationException('Message too long.');
            }
    }

    /**
     * Validate a dispute reason.
     */
    public static function dispute_reason($reason) {
            if(!isset($reason)) return;
            $reasons = array_keys(Hey::get_app('task_dispute_reasons'));
            if(!in_array($reason, $reasons))
                    throw new AGValidationException('Invalid dispute reason.');
    }

    /**
     * Validate a rating score.
     */
    public static function rating_score($score) {
            if(!isset($score)) return;
            $scores = array_keys(Hey::get_app('task_rating_scores'));
            if(!in_array($score, $scores))
                    throw new AGValidationException('Invalid rating score.');
    }
    
    /**
     * Validate task cancellation policy.
     */
    public static function task_cancellation_policy($policy) {
            if(!isset($policy)) return;
            if (!in_array($policy, K::$TASK_CANCEL_POLICY))
                    throw new AGException('Invalid task cancellation policy.');
    }
    
    /**
     * Validate a ranged value.
     * 
     * @param type $val
     * @param array $range
     * @return type
     * @throws AGValidationException
     */
    public static function in_range($val, array $range) {
        if(!isset($val)) return;
        assert(count($range) === 2);
        assert($range[0] <= $range[1]);
        if($val < $range[0] || $val > $range[1])
            throw new AGValidationException('Invalid value : out of range.');
    }
    
}

