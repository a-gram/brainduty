<?php

return [
		
	/*
	|--------------------------------------------------------------------------
	| Company Display Name 
	|--------------------------------------------------------------------------	
	*/
	
	'company_name' => 'Brainduty',
    
		
	/*
	|--------------------------------------------------------------------------
	| Application Currency
	|--------------------------------------------------------------------------
	*/
	
	'currency' => 'AUD',

    
	/*
	|--------------------------------------------------------------------------
	| Application Payment Settings
	|--------------------------------------------------------------------------
	|
	| 
	
	*/
	
	'payment' => [
	    'methods' => [
		'card',
            ],
	],
	
	
	/*
	|--------------------------------------------------------------------------
	| Application Payout Settings
	|--------------------------------------------------------------------------
	|
	| 
	
	*/
	
	'payout' => [
	    'methods' => [
		'bank',
            ],
            'delay'    => 3,
            'interval' => 'daily', // daily/manual
	],
	    
    
	/*
	|--------------------------------------------------------------------------
	| Application Email
	|--------------------------------------------------------------------------
	|
	
	*/
    
        'email' => [
            
            'from' => 'no-reply@brainduty.com',
        ],
    

	/*
	 |--------------------------------------------------------------------------
	 | Application Fees
	 |--------------------------------------------------------------------------
	 |
	 | This is the list of the fees applied on the platform. It includes variable
         | fees (calculated on the applicable amount) and fixed fees (independent of
         | the applicable amount). Variable fees are expressed as a percentage, while
         | fixed fees are expressed in base currency units.
	 |
	 | - Service  : these are fees applicable to service providers (commissions,
         |             payment processing fees, handling fees, etc.).
         | 
         | - Withdraw : these are fees applicable to funds transferred to external accounts
         *              (i.e. withdrawals).
         | 
	 | - Task     : these are fees applicable to tasks (cancellations, disputes, etc).
	 |
	
	 */
		
	'fees' => [
            'service' => [
		'var' => [ 
                    'commission' => 0.13,
                    'payment'    => 0.0175, // 0.029,
                    'connect'    => 0.005,
                ],
                'fix' => [
                    'payment'    => 30 /*cents*/,
                ],
            ],
            'withdraw' => [
                'var' => [0],
                'fix' => [0],
            ],
            'task' => [
                'var' => [0],
                'fix' => [
                    'cancellation' => 500 /*cents*/,
                ],
            ],
	],

	/*
	 |--------------------------------------------------------------------------
	 | Application Taxes
	 |--------------------------------------------------------------------------
         |
         | Include sales taxes and taxes that must be withheld from payments to
         * third party service providers, if any.
         |

         */
        'taxes' => [
            'sales' => [
                'var' => [
                    'gst' => 0.10,
                ],
                'fix' => [0],
            ],
            'payments' => [
                'var' => [0],
                'fix' => [0],
            ],
            'payouts' => [
                'var' => [0],
                'fix' => [0],
            ],
        ],
    
    
	/*
	 |--------------------------------------------------------------------------
	 | Application User Identity
	 |--------------------------------------------------------------------------
	 |
	 | These are settings related to the identity verification of users.
	
	 */
    
        'identity' => [
            'documents' => [
                0 => [
                    'type' => 'photoid',
                    'name' => 'ID with photograph',
                    'description' => 'ID with photograph (driver license, passport, id card, etc.)',
                    'verification' => 'external',
                ],
                1 => [
                    'type' => 'qualifications',
                    'name' => 'Proof of qualifications',
                    'description' => 'Certificate or link to record of professional qualifications.',
                    'verification' => 'platform',
                ],
            ],
        ],
    
    
	/*
	 |--------------------------------------------------------------------------
	 | Application Time Intervals
	 |--------------------------------------------------------------------------
	 |
	 | These are time intervals used in various contexts (currently in ISO 8601 format)
	
	 */
    
        'intervals' => [

            'task'    => [
                'disputable' => env('APP_INTERVAL_TASK_DISPUTABLE'),
                'expired'    => env('APP_INTERVAL_TASK_EXPIRED'),
                'cancel_request_expired' => env('APP_INTERVAL_TASK_CANCEL_REQUEST_EXPIRED'),
                'end_request_expired' => env('APP_INTERVAL_TASK_END_REQUEST_EXPIRED'),
                'bid_request_expired' => env('APP_INTERVAL_TASK_BID_REQUEST_EXPIRED'),
            ],

            'account' => [
                //'withdraw'   => env('APP_INTERVAL_ACCOUNT_WITHDRAW'),
                'ev_token_expired' => env('APP_INTERVAL_ACCOUNT_EV_TOKEN_EXPIRED'),
                'pr_token_expired' => env('APP_INTERVAL_ACCOUNT_PR_TOKEN_EXPIRED'),
            ],
            
            'provider' => [
                'offline' => env('APP_INTERVAL_PROVIDER_OFFLINE'),
            ],
        ],
    
	/*
	 |--------------------------------------------------------------------------
	 | Application Time Periods
	 |--------------------------------------------------------------------------
	 |
	 | These are time periods used by the app between processings (in sec).
	
	 */

        'periods' => [
            
            'task' => [
                'reassign' => env('APP_PERIOD_TASK_REASSIGN'),
                'close_reattempts' => env('APP_PERIOD_TASK_CLOSE_REATTEMPTS'),
            ],

            'txn' => [
                'charge_reattempts' => env('APP_PERIOD_TXN_CHARGE_REATTEMPTS'),
                'transfer_reattempts' => env('APP_PERIOD_TXN_TRANSFER_REATTEMPTS'),
            ],
            
            'identity' => [
            ],
            
            '1m'  => 60,
            '10m' => 600,
            '30m' => 1800,
            '1h'  => 3600,
            '3h'  => 10800,
        ],

	/*
	 |--------------------------------------------------------------------------
	 | Task Dispute Reasons
	 |--------------------------------------------------------------------------
	 |
	 | This is the list of the possible reasons for task disputes.
	 |
		
	 */
		
	'task_dispute_reasons' => [
					
		1 /*'task_not_fullfilled'*/        =>  'Task not fulfilled as per description',
		2 /*'task_service_not_supplied'*/  =>  'Services charged but not provided',
		3 /*'task_not_as_expected'*/       =>  'Task not completed as expected',
                4 /*'provider_abusive'*/           =>  'Abusive behaviour from assistant',
	],
	
	
	/*
	 |--------------------------------------------------------------------------
	 | Task Ratings Scores
	 |--------------------------------------------------------------------------
	 |
	 | This is the list of the possible scores for task ratings.
	 |
	
	 */
	
	'task_rating_scores' => [
				
		5   =>  '<i class="fa fa-thumbs-o-up fa-fw"></i> Excellent. I\'m very happy',
		4   =>  '<i class="fa fa-smile-o fa-fw"></i> Good. I\'m satisfied',
		3   =>  '<i class="fa fa-meh-o fa-fw"></i> Fair. I think could have been better',
		2   =>  '<i class="fa fa-frown-o fa-fw"></i> Sloppy. I\'m quite disappointed',
		1   =>  '<i class="fa fa-thumbs-o-down fa-fw"></i> Disastrous. I\'m totally dissatisfied.',
	],

	
	/*
	 |--------------------------------------------------------------------------
	 | Application Supported MIME types for file uploads
	 |--------------------------------------------------------------------------
	 |
	 | All of the supported MIME types for file uploads.
	
	 */
	
	'file_mime_types' => [
	
		'image/png',
		'image/jpg',
		'image/jpeg',
		'text/plain',
		'application/pdf',
		'application/msword',
		'application/vnd.ms-powerpoint',
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'application/vnd.openxmlformats-officedocument.presentationml.presentation',
		'application/vnd.oasis.opendocument.text',
		'application/vnd.oasis.opendocument.presentation',
	],
    
    
	/*
	|--------------------------------------------------------------------------
	| Supported Countries
	|--------------------------------------------------------------------------	
	*/
	
	'countries' => [
            'Australia' => 'AU',
        ],
    

        'legal_age' => 18,
    
	'hash_algo' => 'sha1' /*'md5'*/,
    
    
];
