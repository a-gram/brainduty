<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
 *    php artisan migrate         (create db)
 *    php artisan migrate:reset   (reset db)
 *    php artisan migrate:refresh (reset&create db)
 *    
 *    Not working? 'composer dump-autoload' and try again.
 */

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            
            $table->engine = 'InnoDB';
        	
            $table->bigIncrements('id')->unsigned();
            $table->string('username')->unique();
            $table->string('password', 256);
            $table->string('first_name', 64)->nullable();
            $table->string('last_name', 64)->nullable();
            // Date of birth.
            $table->date('dob')->nullable();
            $table->string('mobile', 64)->nullable();
            $table->string('email', 128)->nullable();
            $table->string('address', 128)->nullable();
            $table->string('city', 64)->nullable();
            $table->string('state', 64)->nullable();
            $table->string('zip_code', 32)->nullable();
            $table->string('country', 64)->nullable();
            // The type of entity registered on the platform (individual|business).
            $table->string('entity_type', 32)->nullable();
            // The unique tax identifier of the entity (TFN, SSN, etc.).
            $table->string('tax_id', 64)->nullable();
            $table->string('business_name', 128)->nullable();
            $table->string('business_number', 128)->nullable();
            $table->string('about', 200)->nullable();
            
            $table->smallInteger('status')->nullable();
            $table->smallInteger('role')->default(0);
            // A ranking value used to evaluate the user in the context of the platform.
            // This value is an aggregation/composition of different scores given
            // to the user in relation to its behaviour within the system.
            $table->integer('rank')->default(0);
            // A rating value representing the 'average' feedback received by the users
            // in relation to other users (for example received by providers from
            // customers). This value can be used as part of the overall ranking.
            $table->decimal('rating', 3,2)->default(0);
            // For providers, this is the individual commission/service fee.
            $table->decimal('fee', 2,2)->default(0);
            // This flag indicated whether the user's email has been verified.
            $table->boolean('email_verified')->default(false);
            
            $table->string('aiid', 64)->nullable();
            $table->integer('aiid_ts')->nullable();
            
            $table->rememberToken();
            // User reference number/code for clients.
            $table->string('ref')->unique();
            // User reference on external services. Currently stores the user 
            // account id on Stripe (Account id and Customer id).
            $table->string('ref_ext')->unique()->nullable();
            // The time at which the user was last seen online.
            $table->dateTime('lastseen_at')->nullable();
            
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
