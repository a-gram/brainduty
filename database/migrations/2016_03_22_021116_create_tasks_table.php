<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateTasksTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks', function (Blueprint $table) {
			 
			$table->engine = 'InnoDB';
			 
			$table->bigIncrements('id')->unsigned();
			$table->bigInteger('id_users_provider')->unsigned();
			$table->bigInteger('id_users_customer')->unsigned();
			// The type of services required for the task (Generic|Expert).
			$table->smallInteger('type')->nullable();
			// The subject of the task, serving as the title/short description.
			$table->string('subject', 100)->nullable();
			// The location at which the task must be performed (Remote|Onsite).
			//$table->string('location', 256)->nullable();
			// The geospatial point of the location
			// Apparently Laravel does not yet support POINT.
			// $table->point('location_point', [0,0])->nullable();
			// Flag indicating whether the task is backed by a source of funds
			// such as a credit/debit card, virtual wallet, voucher, etc.
			$table->boolean('is_funded')->default(false);
                        // Flag indicating whether this task has been rescheduled.
			$table->boolean('is_rescheduled')->default(false);
                        // Flag indicating whether this task is managed or not.
			$table->boolean('is_managed')->default(true);
                        // Flag indicating whether a task end request has been posted
                        // and the task is in the process of being ended.
                        // TODO This should probably be a new state in the task's FSM.
                        $table->boolean('is_ending')->default(false);
			// One of the statuses in the task life-cycle (see state diagram).
			$table->smallInteger('status')->default(0);
			// Priority for this task.
			$table->smallInteger('priority')->default(0);
			// The category tags used to classify the task, if any.
			$table->string('catags', 200)->nullable();
                        // If set, this field contains the id of a specific provider 
                        // to be assigned to the task.
                        $table->bigInteger('assignee')->unsigned()->nullable();
			// Anything relevant to the task.
			$table->text('notes')->nullable();
			// Task reference number/code for clients.
			$table->string('ref')->unique();
			// Reference to external resources associated with the task.
			$table->string('ref_ext', 256)->nullable();
			$table->dateTime('schedule_datetime')->nullable();
			$table->dateTime('start_datetime')->nullable();
			$table->dateTime('end_datetime')->nullable();
			$table->dateTime('complete_datetime')->nullable();
						
			$table->timestamps();
			
			$table->index('id_users_provider');
			$table->index('id_users_customer');
			
			$table->foreign('id_users_provider')
			      ->references('id')->on('users')
			// TODO: Not sure about cascade-deleting all tasks for a user in
			//       case a user account is deleted is appropriate. Probably
			//       we still want to keep these (and all associated) info.
			      ->onDelete('cascade');
			
			$table->foreign('id_users_customer')
			      ->references('id')->on('users')
			// TODO See NOTE above.
			      ->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tasks');
	}
}
