<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\K;

class CreateUsersCustomersSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_customers_settings', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('id_users')->unsigned();
            // The default payment method used to charge the customer.
            $table->string('default_payment_method', 32)->nullable();
            // The task assignment method indicates how tasks for this customer are
            // assigned to providers.
            $table->tinyInteger('ta_method')->unsigned()->default(K::TASK_ASSIGNMENT_MANAGED);
            // The task assignment criterion for managed assigments. This
            // preference indicates how to match/choose a provider when the task
            // assignment is managed by the platform.
            $table->tinyInteger('ta_criteria')->unsigned()->default(K::TASK_ASSIGN_FIRST_BIDDER);
            // The task assignment preference for providers rating. This indicates
            // the minimum rating score that provider must have.
            $table->integer('ta_rating')->unsigned()->default(0);
            // The task assignment preference for providers pricing.
            $table->integer('ta_pricing')->unsigned()->default(0);
            
            $table->timestamps();

            $table->index('id_users');

            $table->foreign('id_users')->references('id')->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_customers_settings');
    }
}
