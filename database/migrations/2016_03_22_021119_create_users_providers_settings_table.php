<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\K;

class CreateUsersProvidersSettingsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_providers_settings', function (Blueprint $table) {
			
			$table->engine = 'InnoDB';
			
			$table->bigIncrements('id')->unsigned();
			$table->bigInteger('id_users')->unsigned();
			// The default payout method used to pay the provider.
			$table->string('default_payout_method', 32)->nullable();
			// The markup on services rates [0,100]%.
			$table->smallInteger('markup')->default(0);
			// The task cancellation policy;
                        $table->tinyInteger('task_cancel_policy')->unsigned()
                                          ->default(K::TASK_CANCEL_POLICY_NO_CHARGE);

			$table->timestamps();
			
			$table->index('id_users');
			
			$table->foreign('id_users')
			      ->references('id')->on('users')
			      ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_providers_settings');
	}
}
