<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersVerificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_verifications', function (Blueprint $table) {
            
            $table->engine = 'InnoDB';
            
            $table->bigInteger('id_users')->unsigned();
            // The account email verification token.
            $table->string('ev_token', 128)->nullable();
            // The account email verification token timestamp.
            $table->datetime('ev_token_ts')->nullable();
            // The password reset verification token.
            $table->string('pr_token', 128)->nullable();
            // The password reset verification token timestamp.
            $table->datetime('pr_token_ts')->nullable();

            $table->index('id_users');
            //$table->index('ev_token');
            //$table->index('pr_token');

            $table->foreign('id_users')->references('id')->on('users')
                  ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_verifications');
    }
}
