<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersFavListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_fav_list', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->bigInteger('id_users')->unsigned();
            // The favoured user
            $table->bigInteger('id_users_fav')->unsigned();

            $table->primary(['id_users', 'id_users_fav']);

            $table->foreign('id_users')->references('id')->on('users')->
                    onDelete('cascade');
            
            $table->foreign('id_users_fav')->references('id')->on('users')->
                    onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_fav_list');
    }
}
