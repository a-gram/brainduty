<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersIgnoreListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_ignore_list', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->bigInteger('id_users')->unsigned();
            // The ignored user
            $table->bigInteger('id_users_ignored')->unsigned();

            $table->primary(['id_users', 'id_users_ignored']);

            $table->foreign('id_users')->references('id')->on('users')->
                    onDelete('cascade');
            
            $table->foreign('id_users_ignored')->references('id')->on('users')->
                    onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_ignore_list');
    }
}
