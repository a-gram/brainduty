<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services_categories', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('id_services')->unsigned();
            // This is the identifier of the categories which is generally
            // formatted like a path (e.g. engineering/electronics) for sub-categories.
            $table->string('tag', 256)->nullable();
            // The category's screen name that appears on clients.
            $table->string('name', 256)->nullable();
            $table->integer('price')->default(0);
            
            $table->index('id_services');

            $table->foreign('id_services')->references('id')->on('services')
                  ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('services_categories');
    }
}
