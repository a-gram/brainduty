<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvidersServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers_services', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->bigInteger('id_users')->unsigned();
            $table->bigInteger('id_services')->unsigned();
            // Comma-separated list of categories for the provided service.
            $table->string('categories', 256)->nullable();
            // The provider's rate for the service.
            $table->integer('rate')->unsigned()->default(0);

            $table->primary(['id_users', 'id_services']);
            $table->index('id_services');

            $table->foreign('id_users')->references('id')->on('users')->
                    onDelete('cascade');
            
            $table->foreign('id_services')->references('id')->on('services')->
                    onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('providers_services');
    }
}
