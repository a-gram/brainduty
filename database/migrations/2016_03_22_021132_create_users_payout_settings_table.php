<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersPayoutSettingsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_payout_settings', function (Blueprint $table) {
				
			$table->engine = 'InnoDB';
			
			$table->bigIncrements('id')->unsigned();
			$table->bigInteger('id_users_provider')->unsigned();
			// The payout method type (bank|paypal, etc.)
			$table->string('method', 32)->nullable();
			// The payout method id. This should be unique. This is currently
			// a bank account token returned by the marketplace provider.
			$table->string('account_id')->unique()->nullable();
			// The account holder's name
			$table->string('account_name', 64)->nullable();
			// The account original number (masked).
			$table->string('account_number', 64)->nullable();
			// Currently stores the bank's name.
			$table->string('account_host', 64)->nullable();
                        // Account secret key
                        $table->string('account_sk', 256)->nullable();
                        // Account public key
                        $table->string('account_pk', 256)->nullable();
			// Flag indicating if this is the primary payout account to be used.
			$table->boolean('is_primary')->default(false);
			// Flag indicating whether this payout account can be considered ready.
			$table->boolean('is_open')->default(false);
			// Flag indicating whether this payment method has been verified.
			$table->boolean('is_verified')->default(false);
			
                        $table->ipAddress('requestor')->nullable();
                        
			$table->timestamps();
				
			$table->index('id_users_provider');
			
			$table->foreign('id_users_provider')
			->references('id')->on('users')
			->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_payout_settings');
	}
}
