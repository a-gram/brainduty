<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersPaymentSettingsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_payment_settings', function (Blueprint $table) {
				
			$table->engine = 'InnoDB';
			
			$table->bigIncrements('id')->unsigned();
			$table->bigInteger('id_users_customer')->unsigned();
			// The payment method (card|paypal|bank|etc.)
			$table->string('method', 32)->nullable();
			// The payment cycle (one-time|recurring)
			$table->string('cycle', 32)->nullable();
			// The payment method's id. This should be unique (e.g. card number,
			// bank account number, PayPal account id, etc.). This is currently
			// a card token returned by the marketplace provider.
			$table->string('source_id')->unique()->nullable();
			// The name of the payment method's holder
			$table->string('source_name', 64)->nullable();
			// The type of source. Actually, for cards this is the issuer (Visa, etc.).
			$table->string('source_type', 32)->nullable();
			$table->string('source_number', 256)->nullable();
			$table->string('source_expiry', 32)->nullable();
			// Flag indicating if this is the primary payment method of all payment
			// methods of this type (e.g. the primary card of all registered cards).
			$table->boolean('is_primary')->default(false);
			// Flag indicating whether this payment method can be considered ready.
			$table->boolean('is_open')->default(false);
			// Flag indicating whether this payment method has been verified.
			$table->boolean('is_verified')->default(false);
			
                        $table->ipAddress('requestor')->nullable();
                        
			$table->timestamps();
				
			$table->index('id_users_customer');
			
			$table->foreign('id_users_customer')
			->references('id')->on('users')
			->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_payment_settings');
	}
}
