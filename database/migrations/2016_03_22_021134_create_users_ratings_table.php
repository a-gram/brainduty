<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersRatingsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_ratings', function (Blueprint $table) {

			$table->engine = 'InnoDB';

			$table->bigIncrements('id')->unsigned();
			
			// The user giving the rating.
			$table->bigInteger('id_sender')->unsigned();
			// The user receiving the rating.
			$table->bigInteger('id_recipient')->unsigned();
			// A rating is always given in the context of a task.
			// Although a many-to-many relationship, there are only 2 ratings
			// per task at most (from customer and provider).
			$table->bigInteger('id_tasks')->unsigned()->unique();
			// The description/feedback/review/whatever.
			$table->string('description', 300)->nullable();
			// The rating score.
			$table->integer('score')->nullable();
			
			$table->timestamps();
			
			$table->index('id_sender');
			$table->index('id_recipient');
			$table->index('id_tasks');
			
			$table->foreign('id_sender')
			      ->references('id')->on('users')
			      ->onDelete('cascade');

			$table->foreign('id_recipient')
			      ->references('id')->on('users')
			      ->onDelete('cascade');
				
			$table->foreign('id_tasks')
		          ->references('id')->on('tasks')
			      ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_ratings');
	}
}
