<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvidersIdentitiesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('providers_identities', function (Blueprint $table) {

			$table->engine = 'InnoDB';
				
			$table->bigInteger('id_users_provider')->unique()->unsigned();
			$table->smallInteger('status')->nullable();
                        // Field indicating whether and which identity info are 
                        // required to be submitted as the verification progresses.
                        $table->string('info_required',128)->nullable();
                        // Flag indicating whether the provider identity has been
                        // internally verified (by the platform).
                        $table->boolean('int_verified')->nullable();
                        // Flag indicating whether the provider identity has been
                        // verified by an external verification service.
                        $table->boolean('ext_verified')->nullable();
                        // Notes on the current state of the identification.
			$table->string('notes', 200)->nullable();
			$table->dateTime('submission_datetime')->nullable();
			$table->dateTime('results_datetime')->nullable();
			
			$table->timestamps();

			$table->foreign('id_users_provider')
			      ->references('id')->on('users')
			      ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('providers_identities');
	}
}
