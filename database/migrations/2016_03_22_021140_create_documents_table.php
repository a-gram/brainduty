<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('id_users')->unsigned();
            // The category of document
            $table->string('category', 64)->nullable();
            // The type of document
            $table->string('type', 64)->nullable();
            // The name of the file (without the path).
            $table->string('filename', 512)->nullable();
            // The full, absolute path to the file, or its URL.
            $table->string('path', 1024)->nullable();
            // The file size
            $table->integer('size')->unsigned();
            // The original name of the file on the client before posting it.
            $table->string('filename_orig', 512)->nullable();
            // The file mime type.
            $table->string('mime', 128)->nullable();
            // Flag indicating whether the document is encrypted.
            $table->boolean('is_encrypted')->default(false);
            // Flag indicating whether the document is compressed.
            $table->boolean('is_compressed')->default(false);
            // Flag indicating whether the document is hosted by the platform or
            // by an external storage service.
            $table->boolean('is_hosted')->default(true);
//			$table->string('ref')->unique();
            $table->string('ref_ext')->nullable();
            // Arbitrary data associated with the document.
            $table->text('data')->nullable();

            $table->timestamps();

            $table->index('id_users');

            $table->foreign('id_users')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('documents');
    }
}
