<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateMessagesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('messages', function (Blueprint $table) {

			$table->engine = 'InnoDB';

			$table->bigIncrements('id')->unsigned();
			$table->bigInteger('id_tasks')->unsigned();
			$table->bigInteger('id_users_sender')->unsigned();
			$table->bigInteger('id_users_recipient')->unsigned();
			$table->dateTime('delivery_datetime')->nullable();
			$table->dateTime('seen_datetime')->nullable();
			$table->integer('type');
			$table->text('body')->nullable();
			$table->string('token', 128)->nullable();
			// Flag indicating whether the message can be delivered to the recipient
			// (some messages may need to wait for further operations before being delivered).
			$table->boolean('is_deliverable')->default(true);
			// Flag indicating whether the message has been delivered (received at the
			// client side) by the recipient.
			$table->boolean('is_delivered')->default(false);
			// Flag indicating whether the message has been seen (or an optimistic guess)
			// by the recipient.
			$table->boolean('is_seen')->default(false);
                        // If the message is transactional, then this flag indicated whether the
                        // transaction associated with the message has been completed.
			$table->boolean('is_txn_completed')->default(false);
			// The names of possible actions for transactional messages, separated
			// by a pipe '|' character. The action taken by the user will be appended to
			// the action method field's URL, representing its first mandatory parameter.
			$table->string('action_names', 256)->nullable();
			// The method that will respond to the user action (URL endpoint).
			$table->string('action_method', 256)->nullable();
			// Optional parameters for the method call (these are actually appended
			// as the query string of the action method's URL, so no big data here).
			$table->string('action_params', 256)->nullable();
			// The action taken by the user.
			$table->string('action_user', 32)->nullable();
                        // The date/time the action was taken on.
                        $table->dateTime('action_datetime')->nullable();
			// For transactional messages, this flag indicates whether the associated
			// transaction has been completed.
			$table->string('ref')->unique();
			$table->string('ref_ext', 256)->nullable();
			// Arbitrary data associated with the message.
			$table->text('data')->nullable();
				
			$table->timestamps();
				
			$table->index('id_tasks');
			$table->index('id_users_sender');
			$table->index('id_users_recipient');
				
			$table->foreign('id_tasks')
			->references('id')->on('tasks')
			->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('messages');
	}
}
