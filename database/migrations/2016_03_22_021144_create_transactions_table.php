<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateTransactionsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transactions', function (Blueprint $table) {

			$table->engine = 'InnoDB';

			$table->bigIncrements('id')->unsigned();
			// The user from which the funds originate (the debited user).
			$table->bigInteger('id_sender')->unsigned();
			// The user to which the funds clear (the credited user).
			$table->bigInteger('id_recipient')->unsigned();
			// A transaction may or may not be created in the context of a task
			// so this field must be nullable.
			$table->bigInteger('id_tasks')->unsigned()->nullable();
                        // The total amount of ther transaction.
			$table->integer('amount_gross')->default(0);
                        // Fees calculated on the total amount.
			$table->integer('amount_fee')->default(0);
                        // Taxes calculated on the total amount.
			$table->integer('amount_tax')->default(0);
                        // The amount affecting the balance. This may be the net amount
                        // or the total amount, depending on the user.
                        $table->integer('amount_bal')->default(0);
			$table->string('currency', 8)->nullable();
                        // This is the status of the transaction with respect to the balance.
                        // A transaction can be initiated but has not yet affected the balance
                        // (pending, outstanding, ongoing, etc.), initiated and affected the 
                        // balance (completed, available, etc.) aborted and will never affect
                        // the balance (cancelled, failed, etc.). The terminology used to mark
                        // these states may vary. Note that the transaction status may be
                        // different than the status of the method used to make payments or
                        // payouts (see 'settle_status'). For example, a charge may be completed
                        // as funds have been debited to the sender but the transaction may be 
                        // still pending because the funds are not yet available (for some reason) 
                        // in the balance, or a transfer may be still in transit to the destination 
                        // but the transaction is already completed cause the funds have been taken 
                        // from the balance.
			$table->string('status', 32)->nullable();
			$table->string('description', 256)->nullable();
                        // The type of transaction indicates the action that caused
                        // the movement of funds (charge, refund, payment, transfer, etc.).
			$table->string('type', 32)->nullable();
                        // Flag indicating whether funds for this transaction have been
                        // already moved or just 'secured' (put on hold, authorized, etc.).
                        // If not applicable to the transaction, this field is null.
			$table->boolean('is_captured')->nullable();
                        // The method used to settle the transaction, that is to source funds 
                        // from the sender if it's a payment/charge (card, e-wallet, etc.) and 
                        // to deposit funds to the recipient if it's a transfer/withdrawal 
                        // (bank account, debit card, e-wallet, etc.).
			$table->string('settle_method', 64)->nullable();
                        // This is the status of the method used to settle the transaction.
                        // For example, in a payment/charge transaction made by card this 
                        // indicates the status of the charge (e.g. succeeded, failed, etc.),
                        // while in a withdrawal it indicates the status of the transfer, etc.
                        $table->string('settle_status', 32)->nullable();
                        // The (possibly unique) identifier of the requestor initiating
                        // the transaction.
			$table->string('request_id', 64)->nullable();
                        // Flag indicating whether the requestor was authenticated when
                        // initiating the transaction.
			$table->boolean('request_auth')->default(false);
                        // If the transaction is a refund, this field contains the id of
                        // the transaction that has been refunded. This is generally not unique
                        // as there may be several partial refunds for a given transaction.
                        $table->bigInteger('refunded_txn')->unsigned()->nullable();
                        // The reference/id of the transaction.
			$table->string('ref')->unique();
                        // The reference/id of the transaction on the marketplace provider.
			$table->string('ref_ext')->nullable();
                        // The reference/id of the action that generated the transaction.
                        $table->string('ref_ext_source')->nullable();
                        // The time at which the funds associated with the transaction will
                        // be available to the recipient.
			$table->dateTime('clear_datetime')->nullable();
				
			$table->timestamps();

			$table->index('id_sender');
			$table->index('id_recipient');
			$table->index('id_tasks');

			$table->foreign('id_sender')
			      ->references('id')->on('users')
			      ->onDelete('cascade');

			$table->foreign('id_recipient')
			      ->references('id')->on('users')
			      ->onDelete('cascade');
				
			$table->foreign('id_tasks')
		          ->references('id')->on('tasks')
			      ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transactions');
	}
}
