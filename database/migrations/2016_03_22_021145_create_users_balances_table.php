<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateUsersBalancesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_balances', function (Blueprint $table) {

			$table->engine = 'InnoDB';

			$table->bigInteger('id_users')->unsigned();
			// The account total balance.
			$table->integer('total')->default(0);
			// The amount available for withdrawal.
			$table->integer('available')->default(0);
                        // The amount not yet available for withdrawal.
                        $table->integer('pending')->default(0);
                        
			$table->timestamps();
            $table->primary(['id_users']);

			$table->foreign('id_users')
			      ->references('id')->on('users')
			      ->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_balances');
	}
}
