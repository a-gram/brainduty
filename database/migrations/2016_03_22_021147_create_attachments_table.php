<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateAttachmentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('attachments', function (Blueprint $table) {

			$table->engine = 'InnoDB';

			$table->bigIncrements('id')->unsigned();
			$table->bigInteger('id_messages')->unsigned();
			// The name of the file (without the path).
			$table->string('filename', 512)->nullable();
			// The full, absolute path to the file, or its URL.
			$table->string('path', 1024)->nullable();
			// The file size
			$table->integer('size')->unsigned();
			// The original name of the file on the client before posting it.
			$table->string('filename_orig', 512)->nullable();
			// The attachment mime type.
			$table->string('mime', 128)->nullable();
                        // Flag indicating whether the file is encrypted.
                        $table->boolean('is_encrypted')->default(false);
                        // Flag indicating whether the file is compressed.
                        $table->boolean('is_compressed')->default(false);
//			$table->string('ref')->unique();
			$table->string('ref_ext1', 256)->nullable();
			$table->string('ref_ext2', 256)->nullable();

			$table->timestamps();

			$table->index('id_messages');

			$table->foreign('id_messages')
			->references('id')->on('messages')
			->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('attachments');
	}
}
