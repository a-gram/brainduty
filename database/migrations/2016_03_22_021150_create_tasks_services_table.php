<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksServicesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks_services', function (Blueprint $table) {
				
			$table->engine = 'InnoDB';
			
			$table->bigInteger('id_tasks')->unsigned();
			$table->bigInteger('id_services')->unsigned();
			$table->bigInteger('id_messages')->unsigned();
			$table->integer('amount')->default(0);
			$table->string('description', 100)->nullable();
			
			$table->primary(['id_tasks', 'id_services', 'id_messages']);
			
			$table->index('id_tasks');
			$table->index('id_services');
			$table->index('id_messages');
				
			$table->foreign('id_tasks')->references('id')->on('tasks')
			->onDelete('cascade');
			//$table->foreign('id_services')->references('id')->on('services')
			//->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tasks_services');
	}
}
