<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksBidsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks_bids', function (Blueprint $table) {
				
			$table->engine = 'InnoDB';
				
			$table->bigInteger('id_tasks')->unsigned();
			$table->bigInteger('bidder')->unsigned();
                        $table->boolean('is_excluded')->default(false);
			$table->dateTime('bid_datetime')->nullable();
			
			$table->primary(['id_tasks', 'bidder']);
			
			$table->index('id_tasks');
			$table->index('bidder');
				
			$table->foreign('id_tasks')->references('id')->on('tasks')
			->onDelete('cascade');
			$table->foreign('bidder')->references('id')->on('users')
			->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tasks_bids');
	}
}
