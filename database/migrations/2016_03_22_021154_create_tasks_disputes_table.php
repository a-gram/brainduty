<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksDisputesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks_disputes', function (Blueprint $table) {

			$table->engine = 'InnoDB';

			$table->bigInteger('id_tasks')->unsigned();
			$table->integer('reason')->nullable();
			$table->string('details', 300)->nullable();
			$table->integer('status')->nullable();
                        $table->bigInteger('winner')->unsigned()->nullable();
			$table->string('results', 100)->nullable();
			$table->dateTime('results_datetime')->nullable();
			
			$table->timestamps();
			
			$table->primary(['id_tasks']);

			$table->foreign('id_tasks')
			->references('id')->on('tasks')
			->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tasks_disputes');
	}
}
