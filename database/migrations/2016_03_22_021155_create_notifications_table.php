<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateNotificationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notifications', function (Blueprint $table) {

			$table->engine = 'InnoDB';

			$table->bigIncrements('id')->unsigned();
			$table->bigInteger('id_users_recipient')->unsigned();
			$table->integer('type')->nullable();
			$table->integer('event')->nullable();
			$table->string('message', 256)->nullable();
			$table->boolean('is_delivered')->default(false);
			$table->boolean('is_seen')->default(false);
			$table->string('subject_id', 64)->nullable();
                        // This is the url that will be loaded when the notification
                        // is clicked.
                        $table->string('url', 150)->nullable();
                        // This field may be used to attach small data to the
                        // notification, such as the result of some operation.
			$table->string('data', 500)->nullable();
                        
			$table->timestamps();
				
			$table->index('id_users_recipient');
			$table->index('subject_id');
				
			$table->foreign('id_users_recipient')
			      ->references('id')->on('users')
			      ->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notifications');
	}
}
