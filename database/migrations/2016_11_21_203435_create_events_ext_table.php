<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * This table is created to prevent possible issues with duplicate events received
 * by Stripe on webhooks (see https://stripe.com/docs/webhooks#best-practices) )
 */
class CreateEventsExtTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('x_events', function (Blueprint $table) {
            
            $table->engine = 'InnoDB';
            
            $table->string('id')->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('x_events');
    }
}
