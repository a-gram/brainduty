<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesToNotificationAndTaskbids extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Index notifications.type, notifications.event
        Schema::table('notifications', function($table) {
            $table->index('type');
            $table->index('event');
        });
        
        // Index tasks_bids.bid_datetime
        Schema::table('tasks_bids', function($table) {
            $table->index('bid_datetime');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notifications', function($table) {
            $table->dropIndex('type');
            $table->dropIndex('event');
        });
        
        Schema::table('tasks_bids', function($table) {
            $table->dropIndex('bid_datetime');
        });
    }
}
