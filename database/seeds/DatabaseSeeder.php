<?php

use Illuminate\Database\Seeder;
use App\K;
use App\Models\User;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * php artisan migrate:refresh --seed
     * 
     * @return void
     */
    public function run()
    {
        DB::statement('ALTER TABLE users AUTO_INCREMENT = '.(User::SYSTEM).'');
        
        // Create system users
    	DB::table('users')->insert([
            [
                'username'   => 'sys@brainduty.com',
                'password'   => '$2y$10$ZHpFeEX3QcHJkSC7SAIPFeNppsyuVqRJ4qYKHLDyvz161UpDyfEuy',
                'role'       => K::ROLE_SYSTEM,
                'first_name' => null,
                'last_name'  => null,
                'status'     => K::USER_ACTIVE,
                'ref'        => 'usr-ldk948GKSDL0zfFP2cG5zW2jp00',
            ],
            [
                'username'   => 'admin1@brainduty.com',
                'password'   => '$2y$10$ZHpFeEX3QcHJkSC7SAIPFeNppsyuVqRJ4qYKHLDyvz161UpDyfEuy',
                'role'       => K::ROLE_ADMIN,
                'first_name' => 'Alberto',
                'last_name'  => 'Gramaglia',
                'status'     => K::USER_ACTIVE,
                'ref'        => 'usr-ioMD30GQA3J61lo3XdP08DJ5ud5',
            ],    			 
    	]);

        DB::statement('ALTER TABLE users AUTO_INCREMENT = '.(User::SYSTEM+K::RESERVED_USER_IDS).'');

    	// Create base services
    	DB::table('services')->insert([
            [
                'name' => 'Generic',
                'description' => 'Generic assistance does not require specific professional qualifications '.
                                 'and can embrace a broad range of duties such as administration, scheduling, '.
                                 'planning, organizing, moderating, researching, etc.',
                'duration' => 20,
                'price' => 750,
                'is_active' => true,
            ],
            [
                'name' => 'Expert',
                'description' => 'Expert assistance requires certified professional qualifications and include '.
                                 'duties such as consultations, advisory, reviews, training, designing, etc.',
                'duration' => 20,
                'price' => 0,
                'is_active' => true,
            ],
            [
                'name' => 'Extra',
                'description' => 'Any extra services that can be charged for duties while they are being performed.',
                'duration' => 0,
                'price' => 0,
                'is_active' => false,
            ],
    	]);
        
    	// Create base services categories
    	DB::table('services_categories')->insert([
            
            [ 'id_services' => 2, 'tag' => 'acc_taxes',      'name' => 'Accounting - Taxes',             'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'acc_payroll',    'name' => 'Accounting - Payroll',           'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'acc_book',       'name' => 'Accounting - Bookkeeping',       'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'acc_analysis',   'name' => 'Accounting - Financial Analysis','price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'acc_budget',     'name' => 'Accounting - Budgeting',         'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'acc_startup',    'name' => 'Accounting - Business Start-up', 'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'acc_planning',   'name' => 'Accounting - Business Planning', 'price' => 1990, ],
            
            [ 'id_services' => 2, 'tag' => 'arch_concept',   'name' => 'Architecture - Concept',         'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'arch_design',    'name' => 'Architecture - Design & Development','price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'arch_regulatory','name' => 'Architecture - Regulatory',      'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'arch_fisibility','name' => 'Architecture - Fisibility',      'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'arch_master',    'name' => 'Architecture - Master Planning', 'price' => 1990, ],

            [ 'id_services' => 2, 'tag' => 'tech_troubles',  'name' => 'Computers - Troubleshooting',    'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'tech_install',   'name' => 'Computers - Installations',      'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'tech_virus',     'name' => 'Computers - Virus Removal',      'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'tech_recovery',  'name' => 'Computers - Data Recovery',      'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'tech_training',  'name' => 'Computers - Training',           'price' => 1990, ],
            
            [ 'id_services' => 2, 'tag' => 'fin_loans',      'name' => 'Finance - Loans & Mortgages',    'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'fin_stocks',     'name' => 'Finance - Stocks & Bonds',       'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'fin_hedge',      'name' => 'Finance - Hedge Funds',          'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'fin_forex',      'name' => 'Finance - Forex',                'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'fin_capital',    'name' => 'Finance - Capital Investment',   'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'fin_insurance',  'name' => 'Finance - Insurance',            'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'fin_super',      'name' => 'Finance - Superannuation',       'price' => 1990, ],
            
            [ 'id_services' => 2, 'tag' => 'eng_civil',      'name' => 'Engineering - Civil',      'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'eng_electrical', 'name' => 'Engineering - Electrical', 'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'eng_mechanical', 'name' => 'Engineering - Mechanical', 'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'eng_mechanical', 'name' => 'Engineering - Chemical',   'price' => 1990, ],
            
            [ 'id_services' => 2, 'tag' => 'fitness',        'name' => 'Fitness & Wellbeing',      'price' => 1990, ],
            
            [ 'id_services' => 2, 'tag' => 'food',           'name' => 'Food & Culinary',          'price' => 1990, ],
            
            [ 'id_services' => 2, 'tag' => 'law_banking',    'name' => 'Legal - Banking & Finance',     'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'law_rights',     'name' => 'Legal - Civil & Human Rights',  'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'law_commercial', 'name' => 'Legal - Commercial',            'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'law_construct',  'name' => 'Legal - Construction',          'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'law_contract',   'name' => 'Legal - Contract',              'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'law_criminal',   'name' => 'Legal - Criminal',              'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'law_corporate',  'name' => 'Legal - Corporate & Securities','price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'law_labor',      'name' => 'Legal - Employment & Labor',    'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'law_environ',    'name' => 'Legal - Environment',           'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'law_family',     'name' => 'Legal - Family & Juvenile',     'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'law_immi',       'name' => 'Legal - Immigration',           'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'law_ip',         'name' => 'Legal - Intellectual Property', 'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'law_insurance',  'name' => 'Legal - Insurance',             'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'law_litigation', 'name' => 'Legal - Litigation',            'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'law_medical',    'name' => 'Legal - Medical',               'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'law_injury',     'name' => 'Legal - Personal Injury',       'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'law_property',   'name' => 'Legal - Property',              'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'law_tax',        'name' => 'Legal - Tax',                   'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'law_wills',      'name' => 'Legal - Wills & Deceased',      'price' => 1990, ],
            
            [ 'id_services' => 2, 'tag' => 'med_allergo',    'name' => 'Medical - Allergology',         'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'med_cardio',     'name' => 'Medical - Cardiology',          'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'med_dentist',    'name' => 'Medical - Dentistry',           'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'med_derma',      'name' => 'Medical - Dermatology',         'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'med_endocrino',  'name' => 'Medical - Endocrinology',       'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'med_gynecology', 'name' => 'Medical - Gynecology',          'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'med_geriatrics', 'name' => 'Medical - Geriatrics',          'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'med_neuro',      'name' => 'Medical - Neurology',           'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'med_nephro',     'name' => 'Medical - Nephrology',          'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'med_onco',       'name' => 'Medical - Oncology',            'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'med_eye',        'name' => 'Medical - Ophthalmology ',      'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'med_ortho',      'name' => 'Medical - Orthopedics',         'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'med_pediatrics', 'name' => 'Medical - Pediatrics',          'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'med_plastic',    'name' => 'Medical - Plastic & Aesthetic', 'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'med_psyco',      'name' => 'Medical - Psychiatry',          'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'med_pulmo',      'name' => 'Medical - Pulmonology',         'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'med_urology',    'name' => 'Medical - Urology',             'price' => 1990, ],
            
            [ 'id_services' => 2, 'tag' => 'dieting',        'name' => 'Nutrition & Dieting',           'price' => 1990, ],
            
            [ 'id_services' => 2, 'tag' => 'pharmacy',       'name' => 'Pharmacy',                      'price' => 1990, ],
            
            [ 'id_services' => 2, 'tag' => 'counselling',    'name' => 'Psychology and Counselling',    'price' => 1990, ],
            
            [ 'id_services' => 2, 'tag' => 'estate',         'name' => 'Real Estate & Housing',         'price' => 1990, ],
            
            [ 'id_services' => 2, 'tag' => 'soft_web',       'name' => 'Software & Programming - Web',  'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'soft_mobile',    'name' => 'Software & Programming - Mobile', 'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'soft_database',  'name' => 'Software & Programming - Database', 'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'soft_embedded',  'name' => 'Software & Programming - Embedded', 'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'soft_bigdata',   'name' => 'Software & Programming - Big Data', 'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'soft_systems',   'name' => 'Software & Programming - Systems',  'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'soft_c',         'name' => 'Software & Programming - C',        'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'soft_cpp',       'name' => 'Software & Programming - C++',      'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'soft_csharp',    'name' => 'Software & Programming - C#',       'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'soft_css',       'name' => 'Software & Programming - CSS',      'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'soft_html5',     'name' => 'Software & Programming - HTML5',    'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'soft_java',      'name' => 'Software & Programming - Java',     'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'soft_jscript',   'name' => 'Software & Programming - JavaScript', 'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'soft_objc',      'name' => 'Software & Programming - Objective-C', 'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'soft_php',       'name' => 'Software & Programming - PHP',         'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'soft_python',    'name' => 'Software & Programming - Python',      'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'soft_ruby',      'name' => 'Software & Programming - Ruby',        'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'soft_sql',       'name' => 'Software & Programming - SQL',         'price' => 1990, ],
            
            [ 'id_services' => 2, 'tag' => 'edu_algebra',    'name' => 'Tutoring - Algebra',             'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_arts',       'name' => 'Tutoring - Arts',                'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_biology',    'name' => 'Tutoring - Biology',             'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_business',   'name' => 'Tutoring - Business',            'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_calculus',   'name' => 'Tutoring - Calculus',            'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_chemistry',  'name' => 'Tutoring - Chemistry',           'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_computer',   'name' => 'Tutoring - Computer Science',    'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_chinese',    'name' => 'Tutoring - Chinese',             'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_economics',  'name' => 'Tutoring - Economics',           'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_electro',    'name' => 'Tutoring - Electronics',         'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_english',    'name' => 'Tutoring - English',             'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_french',     'name' => 'Tutoring - French',              'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_geography',  'name' => 'Tutoring - Geography',           'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_geology',    'name' => 'Tutoring - Geology',             'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_geometry',   'name' => 'Tutoring - Geometry',            'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_german',     'name' => 'Tutoring - German',              'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_greek',      'name' => 'Tutoring - Greek',               'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_history',    'name' => 'Tutoring - History',             'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_italian',    'name' => 'Tutoring - Italian',             'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_management', 'name' => 'Tutoring - Management',          'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_maths',      'name' => 'Tutoring - Mathematics',         'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_music',      'name' => 'Tutoring - Music',               'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_physics',    'name' => 'Tutoring - Physics',             'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_pro_basic',  'name' => 'Tutoring - Programming - Basic', 'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_pro_c',      'name' => 'Tutoring - Programming - C',     'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_pro_cpp',    'name' => 'Tutoring - Programming - C++',   'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_pro_csharp', 'name' => 'Tutoring - Programming - C#',    'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_pro_css',    'name' => 'Tutoring - Programming - CSS',   'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_pro_html',   'name' => 'Tutoring - Programming - HTML',  'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_pro_java',   'name' => 'Tutoring - Programming - Java',  'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_pro_jscript','name' => 'Tutoring - Programming - Javascript','price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_pro_objc',   'name' => 'Tutoring - Programming - Objective-C','price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_pro_php',    'name' => 'Tutoring - Programming - PHP',   'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_pro_python', 'name' => 'Tutoring - Programming - Python','price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_pro_ruby',   'name' => 'Tutoring - Programming - Ruby',  'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_pro_sql',    'name' => 'Tutoring - Programming - SQL',   'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_science',    'name' => 'Tutoring - Science',             'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_spanish',    'name' => 'Tutoring - Spanish',             'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_statistics', 'name' => 'Tutoring - Statistics',          'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'edu_web',        'name' => 'Tutoring - Web & Internet',      'price' => 1990, ],
            
            [ 'id_services' => 2, 'tag' => 'trans_arabic',   'name' => 'Translation - Arabic',           'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'trans_chinese',  'name' => 'Translation - Chinese',          'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'trans_filipino', 'name' => 'Translation - Filipino',         'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'trans_french',   'name' => 'Translation - French',           'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'trans_german',   'name' => 'Translation - German',           'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'trans_greek',    'name' => 'Translation - Greek',            'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'trans_hindi',    'name' => 'Translation - Hindi',            'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'trans_italian',  'name' => 'Translation - Italian',          'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'trans_japanese', 'name' => 'Translation - Japanese',         'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'trans_filipino', 'name' => 'Translation - Filipino',         'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'trans_portug',   'name' => 'Translation - Portuguese',       'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'trans_russian',  'name' => 'Translation - Russian',          'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'trans_spanish',  'name' => 'Translation - Spanish',          'price' => 1990, ],
            [ 'id_services' => 2, 'tag' => 'trans_turkish',  'name' => 'Translation - Turkish',          'price' => 1990, ],
            
            [ 'id_services' => 2, 'tag' => 'veterinary',     'name' => 'Veterinary',                     'price' => 1990, ],
            
        ]);

    	    	
    }
    
}
