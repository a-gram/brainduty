"use strict";

/**
 * This namespace contains facilities that are used by the admin's home page.
 */
function HomePage() {
	
	UserPage.call(this);
	
    
    this.initialize = function() {
    	
    };


    /////////////////////////////////////////////////////////////////////////////
    ///                          EVENT HANDLERS                               ///
    /////////////////////////////////////////////////////////////////////////////
    
    /////////////////////////////////////////////////////////////////////////////
    
	
    this.initialize();
    
};


// AUGMENTED PROTOTYPES.


Dashboard.prototype.create_page = function() {
	return new HomePage;
};


