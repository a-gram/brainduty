"use strict";

/**
 * This namespace contains facilities that are used by the admin's Provider
 * Onboarding page.
 */
function ProviderOnboardingPage() {
	
	UserPage.call(this);
	
    this.initialize = function() {
    	
    	// Setup views.
    	this.provider_list_view.setup(this);
    	this.provider_details_view.setup(this);
    	
    };
    
    /**
     * Handlers for user account commands/actions. All of them are bound to this page.
     * 
     * @param data Object containing the server response and context data, as follows:
     * { response: <server response>, ctx : <context data> }
     * @param ctx Object Contains command contextual data.
     */
    this.command_handlers = {
    	
        // This handler is called before the 'accept' command is executed (posted).
        // The accept command can be posted straight away so this handler does nothimg.
        'before_execute_accept' : function (ctx) {
            return true;
        },

        // This handler is called when a user's identity (typically a provider) has been
        // successfully verified and the user accepted on the platform.
        'on_accept_success' : function (data) {
            var accepted_user = data.ctx.user_id;
            this.provider_details_view.set_status({
                    status   :            App.USER_ACTIVE,
                    identity : { status : App.PROVIDER_IDENTITY_VERIFIED }
            });
            this.provider_list_view.remove(accepted_user);
            App.page.on_success('The provider has been accepeted.');
        },

        // This handler is called when a user's identity accept request fails.
        'on_accept_error' : function (data) {
            App.page.on_error(data.response);
        },

        // This handler is called before the 'reject' command is executed (posted).
        // The reject command has a parameter indicating the reason for rejection
        // (which may be empty).
        'before_execute_reject' : function (ctx) {
            ctx.post.reason = this.get_reject_reason();
            return true;
        },

        // This handler is called when a user's identity (typically a provider) cannot
        // be successfully verified.
        'on_reject_success' : function (data) {
            var rejectd_user = data.ctx.user_id;
            this.provider_details_view.set_status({
                    status   :            App.USER_ONBOARDING,
                    identity : { status : App.USER_IDENTITY_UNVERIFIED }
            });
            this.provider_list_view.set_item_status(rejectd_user,
                                        App.PROVIDER_IDENTITY_UNVERIFIED);
            App.page.on_success('The provider has been rejected.');
        },

        // This handler is called when a user's identity reject request fails.
        'on_reject_error' : function (data) {
            App.page.on_error(data.response);
        },

        // This handler is called before the 'inquiry' command is executed (posted).
        // The inquiry command has a parameter indicating the information subject
        // of the inquiry and the reason for inquirying which is mandatory.
        'before_execute_inquiry' : function (ctx) {
            var reason = this.get_inquiry_reason();
            var info = ctx.command.data('info');
            if(!reason) {
                App.page.on_error('The inquiry reason must be provided.');
                return false;
            }
            ctx.post.reason = reason;
            ctx.post.info = info;
            return true;
        },

        // This handler is called when a user's identity (typically a provider) cannot
        // be successfully verified and the user is being asked for more info.
        'on_inquiry_success' : function (data) {
            var inquired_user = data.ctx.user_id;
            this.provider_details_view.set_status({
                    status   :            App.USER_ONBOARDING,
                    identity : { status : App.PROVIDER_IDENTITY_INQUIRED }
            });
            this.provider_list_view.set_item_status(inquired_user,
                                                            App.PROVIDER_IDENTITY_INQUIRED);
            App.page.on_success('The provider has been inquired.');
        },

        // This handler is called when a user's identity reject request fails.
        'on_inquiry_error' : function (data) {
            App.page.on_error(data.response);
        }
    };

    
    /* ***********************************************************************
     *                        Providers list view
     * ***********************************************************************/
    

    this.provider_list_view = {
    		
        page  :  null,
        view  :  $('.user-list-view'),
        list  :  $('.user-list-view .list-group.providers'),


        setup : function(page) {
            this.page = page;
            this.list.find('.list-group-item').click(this.on_item_click.bind(this));
        },


        on_item_click : function(event) {
           var user_id = $(event.currentTarget).data('user-id');
           this.page.provider_details_view.open(user_id);
        },


        add : function(item) {
                this.list.prepend(item);
        },


        remove : function(item) {
                this.list.find('#'+item).remove();
        },


        item : function(id) {
                return this.list.find('#'+id);
        },


        exists : function(item) {
                return this.list.find('#'+item).length > 0;
        },


        set_item_status : function(item, status) {
                var item = this.list.find('#'+item);
                status = App.identity_status_to_string(status);
        var classes = 'label label-identity-status label-identity-'+status+' pull-right';
        var label = item.find('.label-identity-status');
                label.prop('className', classes);
                label.text(status);
        }

    };
    
    
    /* ***********************************************************************
     *                        User details view
     * ***********************************************************************/


    this.provider_details_view = {

    	page             :  null,
    	view             :  $('.user-details-view'),
    	commands         :  $('.user-details-view .user-commands'),
    	
    	
    	setup : function(page) {
    		this.page = page;
        	this.view.find('.btn-close-view').click(this.on_btn_close_click.bind(this));
    	},
    	
    	
    	show : function() {
    		$('.task-view').removeClass('task-loading');
    		$('.task-view').addClass('task-open');
    	},
    	
    	
    	close : function() {
    		$('.task-view').removeClass('task-open');
    		$('.task-view').removeClass('task-loading');
    	},
    	
    	
    	open : function(user_id) {

    		var _this = this;
    		
            _this.loading();
            
    	    _this.page.fetch_user_account(
                user_id,
                function(user) {
                    ag_assert(user.role == App.ROLE_PROVIDER || new Error);
                    ag_assert(user.identity != null || new Error);
                    _this.page.provider_list_view.set_item_status
                    (user_id, user.identity.status);
                    _this.fill(user);
                    _this.show();
                },
                function() {
                   _this.close();
                },
                null,
                null
      	    );
    	},
    	
    	
    	loading : function() {
    		$('.task-view').addClass('task-loading');
    	},
    	    	
    	
    	is_closable : function(closable) {
    		if(!closable) this.view.find('.btn-close-view').hide();
    		else          this.view.find('.btn-close-view').show();
    	},

    	
    	on_btn_close_click : function() {
    		this.close();
    		this.clear();
    	},

    	
    	/**
    	 * This method is the handler that is attached to the "Actions" command button(s)
    	 * which appears in the header of the provider details view. The binding between
         * the command button(s) and this handler is done in the set_commands() method.
    	 */
    	on_btn_command_click : function(event) {
    		
            var _this = App.page.loaded.provider_details_view;
            var command = $(this).data('command');
            var command_type = $(this).data('command-type');
            var user_id = $(this).data('user-id');
            var url = _this.page.URL_ADMIN_PROVIDER_IDENTITY
                                .replace('{admin_root}', App.env.admin_root)
                                .replace('{user_id}', user_id)
                                .replace('{action}', command);

            var command_on_success = _this.page.command_handlers['on_'+command+'_success'];
            var command_on_error = _this.page.command_handlers['on_'+command+'_error'];
            var command_ctx = { 
                user_id : user_id,
                post    : {},
                command : $(this)
            };

            // Call the pre-posting handler to do any preprocessing or get additional post data.
            var before_execute = _this.page.command_handlers['before_execute_'+command];
                before_execute = before_execute.bind(_this.page);

            var can_proceed = before_execute(command_ctx);

            if(can_proceed) {
                // For asynch commands we make an ajax call else we make a page GET.
                if(command_type === 'asynch')
                   App.page.post_command(url, command_ctx.post, 
                                         command_on_success.bind(_this.page),
                                         command_on_error.bind(_this.page),
                                         command_ctx);
                else
                   App.page.change(url);
            }
    	},
    	
    	
    	fill : function(user) {
        	
    	    ag_assert(user != null || new Error); 
    	    
    	    var _this = this;
    	    
    	    this.view.find('.title').text(user.first_name+' '+user.last_name);
    	    this.view.find('.first-name').text(user.first_name);
    	    this.view.find('.last-name').text(user.last_name);
    	    this.view.find('.dob').text(user.dob);
 	        this.view.find('.entity-type').text(user.entity_type);
    	    
    	    if(App.is_user_business(user)) {
    	       this.view.find('.business-name').text(user.business_name);
    	       this.view.find('.business-number').text(user.business_number);
    	    } else {
     	       this.view.find('.business-name').text('N/A');
    	       this.view.find('.business-number').text('N/A');
    	    }
    	    
            this.view.find('.address').text(user.address);
            this.view.find('.city').text(user.city);
            this.view.find('.zip-code').text(user.zip_code);
            this.view.find('.state').text(user.state);
            this.view.find('.country').text(user.country);

    	    this.set_status(user);
    	    this.set_commands(user);
    	    this.set_identity_docs(user);
    	    this.set_identity_info(user);
    	    this.set_available_actions(user);
            this.set_services(user);
        },

        
        clear : function() {
            this.set_identity_docs(null);
            this.set_identity_info(null);
            this.set_services(null);
        },
        

        // SETTERS
        
        set_status : function(provider) {
            var user_status = App.user_status_to_string(provider);
            var identity_status = App.identity_status_to_string(provider.identity.status);
            var user_status_classes = 'label label-user-status label-user-'+user_status;
            var identity_status_classes = 'label label-identity-status label-identity-'+identity_status;
            var user_label = this.view.find('.label-user-status');
            var identity_label = this.view.find('.label-identity-status');
            user_label.prop('className', user_status_classes);
            user_label.text(user_status);
            identity_label.prop('className', identity_status_classes);
            identity_label.text(identity_status+' identity');
        },

        
        set_identity_docs : function(provider) {
            if(provider &&
               provider.identity.documents &&
               provider.identity.documents.length)
            {
                var fileURL = this.page.URL_ADMIN_PROVIDER_IDENTITY_DOC
                                  .replace('{admin_root}', App.env.admin_root)
                                  .replace('{user_id}', provider.id);
                var docs = App.page.factory.make_identity_attachment(provider.identity.documents, fileURL);
                    docs = App.page.factory.make_identity_attachment(docs, null, App.LIST);
                this.view.find('.activation-docs').append(docs);
            } else {
               this.view.find('.activation-docs').empty();
            }
        },

        
        set_identity_info : function(provider) {
            if(provider && provider.identity) {
               var icon_ext = 'fa fa-'+(provider.identity.ext_verified ? 'check' : 'times');
               var icon_int = 'fa fa-'+(provider.identity.int_verified ? 'check' : 'times');
               this.view.find('.identity .external i').prop('className', icon_ext);
               this.view.find('.identity .internal i').prop('className', icon_int);
            } else {
               this.view.find('.identity i').prop('className', '');
            }
        },
        
        
        set_services: function(provider) {
            var svc_label_map = { };
                svc_label_map[App.SERVICE_GENERIC] = 'info';
                svc_label_map[App.SERVICE_EXPERT] = 'secondary';
            if(provider && provider.services && provider.services.length) {
               var services = '';
               var categories = '';
               $.each(provider.services, function(i,service){
                   var svc_label = svc_label_map[service.id];
                   services += '<span class="label label-'+svc_label+' sm">'+App.service_to_string(service.id)+'</span>';
                   $.each(service.categories, function(j,category){
                       categories += '<span class="label label-'+svc_label+'">'+category.name+'</span>';
                   });
               });
               this.view.find('.services').append(services);
               this.view.find('.services-categories').append(categories);
            } else {
               this.view.find('.services').empty();
               this.view.find('.services-categories').empty();
            }
        },


        // The following two functions set the commands and actions available
        // for the currently loaded provider, which depend on identity status and type
        // of admin user (admins may have different privileges and be allowed different
        // actions on a user account).
        
        set_commands : function(provider) {
            if(!provider) return;
            this.commands.empty();
            var commands = null;
            switch(provider.identity.status) {
               case App.PROVIDER_IDENTITY_SUBMITTED:
               case App.PROVIDER_IDENTITY_VERIFYING:
                    commands = $( App.page.factory.make_user_command_button([
                     {
                         label : 'Accept',
                         command : 'accept'
                     },
                     {
                         label : 'Reject',
                         command : 'reject'
                     },
                     {
                         label : 'Inquiry - Qualifications',
                         command : 'inquiry',
                         data: [
                             { name: 'info', value: 'qualifications' }
                         ]
                     }
                    ]) );
                    break;
            }
            if(commands) {
               commands.find('a').data('user-id', provider.id);
               commands.find('a').click(this.on_btn_command_click);
               this.commands.append(commands);
            }
        },
        
        
        set_available_actions : function() {},
        
        
        has_notes_input : function(has) {
            if(has) this.view.find('.notes textarea').show();
            else    this.view.find('.notes textarea').hide();
        },
        
        
        get_notes : function() {
            return this.view.find('.notes textarea').val();
        }
                
    };
    
    
    // When rejecting a provider's identity a reason may be given and
    // it is input in the 'notes' text box of the user details view.
    this.get_reject_reason = function() {
    	return this.provider_details_view.get_notes();
    };
    
    // When inquirying a provider's identity a reason must be given and
    // it is input in the 'notes' text box of the user details view.
    this.get_inquiry_reason = function() {
    	return this.provider_details_view.get_notes();
    };

    
    this.initialize();
    
};


// AUGMENTED PROTOTYPES.


Dashboard.prototype.create_page = function() {
	return new ProviderOnboardingPage;
};


