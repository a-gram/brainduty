"use strict";

/**
 * This namespace contains facilities that are used by the admin's Task
 * Dsiputes page.
 */
function TaskDisputesPage() {
	
    UserPage.call(this);

    this.open_task         = null;
    this.task_list_view    = new TaskListView;
    this.task_details_view = new TaskDetailsView;

	
    this.initialize = function() {
    	
    	// Setup views.
    	this.task_list_view.setup(this);
    	this.task_details_view.setup(this);

    	this.task_details_view.set_commands = this.set_commands.bind(this.task_details_view);
    };


    /**
     * Handlers for dispute commands/actions. All of them are bound to this page.
     * 
     * @param data Object containing the server response and context data, as follows:
     * { response: <server response>, ctx : <context data> }
     * @param ctx Object Contains command contextual data.
     */
    this.command_handlers = {
    	
	// This handler is called before the 'award' command is executed (posted).
    	// The award command can be posted straight away so this handler does nothimg.
	'before_execute_resolve' : function (ctx) {
            return true;
	},

    	// This handler is called when a dispute has been successfully awarded
        'on_resolve_success' : function (data) {
            var dispute = data.ctx.dispute;
            this.task_list_view.remove(dispute);
            this.task_details_view.close();
            App.page.on_success('The dispute has been resolved.');
        },
		
        // This handler is called when a dispute award fails.
        'on_resolve_error' : function (data) {
            App.page.on_error(data.response);
        }
    },
		

    
    /////////////////////////////////////////////////////////////////////////////
    ///                          EVENT HANDLERS                               ///
    /////////////////////////////////////////////////////////////////////////////
    
    /**
     * This method is the handler that is attached to the "Actions" command button(s)
     * which appears in the header of the task details view. The binding between
     * the command button(s) and this handler is done in the set_commands() method.
     */
    this.on_btn_command_click = function(event) {

        var _this = App.page.loaded;
        var command = $(this).data('command');
        var command_type = $(this).data('command-type');
        var user_ref = $(this).data('user-ref');
        var task_ref = $(this).data('task-ref');
        var url = App.page.loaded.URL_ADMIN_DISPUTE_AWARD
                                 .replace('{admin_root}', App.env.admin_root)
                                 .replace('{task_ref}', task_ref)
                                 .replace('{user_ref}', user_ref)
                                 .replace('{action}', command);

        var command_on_success = _this.command_handlers['on_'+command+'_success'];
        var command_on_error = _this.command_handlers['on_'+command+'_error'];
        var command_ctx = { 
            dispute : task_ref,
            post    : {},
            button  : $(this)
        };

        // Call the pre-posting handler to do any preprocessing or get additional post data.
        var before_execute = _this.command_handlers['before_execute_'+command];
            before_execute = before_execute.bind(_this);

        var can_proceed = before_execute(command_ctx);

        if(can_proceed) {
           // For asynch commands we make an ajax call else we make a page GET.
           if(command_type === 'asynch')
              App.page.post_command(url, command_ctx.post, 
                                    command_on_success.bind(_this),
                                    command_on_error.bind(_this),
                                    command_ctx);
           else
              App.page.change(url);
        }
    };
    

    /////////////////////////////////////////////////////////////////////////////
    
    
    // The following two functions set the commands and actions available
    // for the currently loaded dispute, which depend on dispute status and type
    // of admin user (admins may have different privileges and be allowed different
    // actions).

    this.set_commands = function() {

        if (!this.page.open_task) return;
        this.commands.empty();
        var commands = null;
        switch(this.page.open_task.dispute.status) {
           case App.TASK_DISPUTE_SUBMITTED:
           case App.TASK_DISPUTE_PROCESSING:
                commands = $( App.page.factory.make_user_command_button([
                 {
                     label : 'Resolve to customer',
                     command : 'resolve',
                     data : [{
                         name: 'user-ref',
                         value: this.page.open_task.customer.ref
                     }]
                 },
                 {
                     label : 'Resolve to provider',
                     command : 'resolve',
                     data : [{
                         name: 'user-ref',
                         value: this.page.open_task.provider.ref
                     }]
                 }
                ]) );
                break;
        }
        if(commands) {
           commands.find('a').data('task-ref', this.page.open_task.ref);
           commands.find('a').click(this.page.on_btn_command_click);
           this.commands.append(commands);
        }
    };


    this.set_available_actions = function() {},
        

    this.initialize();
};


// AUGMENTED PROTOTYPES.


Dashboard.prototype.create_page = function() {
	return new TaskDisputesPage;
};


