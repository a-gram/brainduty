"use strict";

/**
 *  Base class for admin user pages.
 */
function UserPage() {
    
    this.URL_ADMIN_USER_ACCOUNT_GET = App.URL_BASE + '/{admin_root}/user/{user_id}/account';
    this.URL_ADMIN_PROVIDER_IDENTITY = App.URL_BASE + '/{admin_root}/provider/{user_id}/identity/{action}';
    this.URL_ADMIN_PROVIDER_IDENTITY_DOC = App.URL_BASE + '/{admin_root}/provider/{user_id}/identity/doc/{doc_id}';
    this.URL_ADMIN_DISPUTE_AWARD = App.URL_BASE + '/{admin_root}/dispute/{task_ref}/{action}/{user_ref}';

    /**
     * Get this user's role
     * 
     * @returns {String|undefined}
     */
    this.user_role = function() { 
        return 'admin'; 
    };

    /**
     * Check if this user has the specified role.
     * 
     * @param {String} role
     * @returns {Boolean}
     */
    this.user_is = function(role) { 
        return role === this.user_role(); 
    }

    this.is_customer = function() { return false; };
    this.is_provider = function() { return false; };
        
    /**
     * Fetch user accout info
     */
    this.fetch_user_account = function(user_id, on_fetch, on_error, url, params) {
    	
        ag_assert(user_id != null || new Error);
	ag_assert(on_fetch || new Error);

    	var getUrl = url ? url : this.URL_ADMIN_USER_ACCOUNT_GET
    	                         .replace('{admin_root}', App.env.admin_root)
    	                         .replace('{user_id}', user_id);
    	
    	getUrl = params ? getUrl + params : getUrl;
		
        $.ajax({
            type: "GET",
            url: getUrl,
            dataType: "json",

            success: function (response) {
                var user = response.body;
                ag_assert(user != null || new Error);
                on_fetch(user);
            },
            error: function (response) {
                if(on_error)
                   on_error();
                App.page.on_error(response);
            }
        });
    };


    // User account namespace groups stuffs related to user account administration.
    this.user = {
        
    };


    this.task = {
        
    };

};

