

/**
 *  This class manages a bi-directional communication with the backend for real-time 
 *  interaction. Data handled by this class include chat messages, notifications and
 *  other updates.
 *  
 */
function AGComm() {
    
    // The URL used to 'listen' for events (messages, updates, notifications, etc.)
    this.url_updates = App.env.URL_BASE + '/updates/push';
    // The URL used to ack delivered and seen messages.
    this.url_ack_message = App.env.URL_BASE + '/message/ack';
    // The URL used to post new messages. Messages are posted to tasks, which can be
    // seen as 'chatrooms' identified by the task reference.
    this.url_chat_message_post = App.env.URL_BASE + '/task/{ref}/message';
    // Event fired when a message is posted and accepted by the server.
    this.event_chat_message_posted = null;
    // Event fired when new messages are received.
    this.event_chat_messages_received = null;
    // Event fired when unseen messages are received.
    this.event_chat_unseen_messages_received = null;
    // Event fired when the latest delivered and seen messages are received.
    this.event_chat_latest_messages_received = null;
    // Event fired when a message post fails.
    this.event_chat_message_post_failed = null;
    // Event fired when a message post progress update is received.
    this.event_chat_message_post_progress = null;
    // Event fired when documents/files are attached to a message.
    this.event_chat_message_attachments_added = null;
    // Event fired when new notifications are received.
    this.event_notifications_received = null;
    // Event fired when unseen notifications are received.
    this.event_unseen_notifications_received = null;
    // Event fired when the latest notifications are received.
    this.event_latest_notifications_received = null;
    
    this.msg_pool = {};
    
    // Flag indicating whether to consume (ack) the messages. In some cases
    // we may not want to consume the received notifications/messages but only
    // be alerted (for example in the portal) and receive them again later.
    // NOTE: IF THIS FLAG IS TRUE, MESSAGES SHOULD NEVER BE PROCESSED.
    this.ack = true;
    
    this.connected = false;
    
    this.synched = false;
    
    this.init();
}
//-----------------------------------------------------------------------------

AGComm.prototype.init = function () {
    
    // Initialize the fileupload form, if present.
    if ($('#fileupload').length) {
        $('#fileupload').fileupload({
                dataType: 'json',
                singleFileUploads: false,
                add: this.on_chat_message_attachments_added_fu.bind(this),
                done: this.on_chat_message_send_success_fu.bind(this),
                fail: this.on_chat_message_send_fail_fu.bind(this),
                progressall: this.on_chat_message_send_progress_fu.bind(this)
            }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    }
    // Bind event handlers
};

// -------------------------- Fileupload handlers -----------------------------

AGComm.prototype.on_chat_message_attachments_added_fu = function (e, data) {
    if (this.event_chat_message_attachments_added)
        this.event_chat_message_attachments_added(data.files);
};

AGComm.prototype.on_chat_message_send_success_fu = function (e, data) {
    if (this.event_chat_message_posted)
        this.event_chat_message_posted(data.result.body);
};

AGComm.prototype.on_chat_message_send_fail_fu = function (e, data) {
    if (this.event_chat_message_post_failed)
        this.event_chat_message_post_failed(data._response.jqXHR);
};

AGComm.prototype.on_chat_message_send_progress_fu = function (e, data) {
    var progress = parseInt(data.loaded / data.total * 100, 10);
    if (this.event_chat_message_post_progress)
        this.event_chat_message_post_progress(progress);
};

// ----------------------------------------------------------------------------

AGComm.prototype.fire_chat_messages_received = function (messages) {
    
    if (messages.length === 0) return;
    
    var _this = this;
    
    var unacked = [];
    
    if (this.ack) {
        $.each(messages, function (i, message) {
            message.is_delivered = true;
            if (_this.is_pooled(message)) {
                message.is_seen = _this.pool_get(message).is_seen;
                unacked.push(message);
                ag_log('Unacked message received. Re-acking...', message);
            } else {
                _this.pool(message);
            }
        });
    }
    
    var filtered = messages.filter(function (msg) {
        return !unacked.has_msg(msg);
    });

    try {
        if (this.event_chat_messages_received)
            this.event_chat_messages_received(filtered);
    } catch (error) {
        App.page.on_error(error);
    }
    
    if (messages.length && this.ack)
        this.ack_messages(messages);
};

//----------------------------------------------------------------------------

AGComm.prototype.fire_chat_unseen_messages_received = function (messages) {
    if (messages.length === 0) return;
    if (this.event_chat_unseen_messages_received)
        this.event_chat_unseen_messages_received(messages);
};

//----------------------------------------------------------------------------

AGComm.prototype.fire_chat_latest_messages_received = function (messages) {
    if (messages.length === 0) return;
    if (this.event_chat_latest_messages_received)
        this.event_chat_latest_messages_received(messages);
};

//----------------------------------------------------------------------------

AGComm.prototype.fire_notifications_received = function (notifications) {
    if (notifications.length === 0) return;
    // TODO Ack notifications ?
    if (this.event_notifications_received)
        this.event_notifications_received(notifications);
};

//----------------------------------------------------------------------------

AGComm.prototype.fire_unseen_notifications_received = function (notifications) {
    if (notifications.length === 0) return;
    // TODO Ack notifications ?
    if (this.event_unseen_notifications_received)
        this.event_unseen_notifications_received(notifications);
};

//----------------------------------------------------------------------------

AGComm.prototype.fire_latest_notifications_received = function (notifications) {
    if (notifications.length === 0) return;
    // TODO Ack notifications ?
    if (this.event_latest_notifications_received)
        this.event_latest_notifications_received(notifications);
};

//----------------------------------------------------------------------------

/**
 * Post a chat message to the server.
 */
AGComm.prototype.post_chat_message = function (message, task_ref /*, form*/ ) {
    
    var _this = this;
    
    var postUrl = this.url_chat_message_post.replace('{ref}', task_ref);
    
    // Apparently 'fileupload' does not submit the data if there
    // are no files selected even though there are form fields attached,
    // so in such case we send the form fields with an ajax post.
    if (message.files.length) {
        //form.attr('action', postUrl);
        $('#fileupload').fileupload('option', {
            url: postUrl
        });
        $('#fileupload').fileupload('send', message);
    } else {
        var postData = {};
        $.each(message.formData, function (i, field) {
            postData[field.name] = field.value;
        });
        $.ajax({
            url: postUrl,
            type: "POST",
            dataType: "json",
            data: postData,
            success: function (response) {
                if (_this.event_chat_message_posted)
                    _this.event_chat_message_posted(response.body);
            },
            error: function (response) {
                if (_this.event_chat_message_post_failed)
                    _this.event_chat_message_post_failed(response);
            }
        });
    }
};

//-----------------------------------------------------------------------------

AGComm.prototype.ack_messages = function (messages) {
    
    var _this = this;
    
    var putURL = this.url_ack_message;
    
    var putData = {
        messages: []
    };
    
    $.each(messages, function (i, message) {
        putData.messages.push({
            ref: message.ref,
            is_delivered: true,
            is_seen: message.is_seen
        })
    });
    
    $.ajax({
        type: "PUT",
        url: putURL,
        dataType: "json",
        data: putData,
        success: function (response) {
            var delivered = response.body.delivered;
            var seen = response.body.seen;
            // Messages acked successfully. Nothing to do here.
        },
        error: function (response) {
            // Messages acks fail. We may retry ack-ing again but all unacked
            // messages are acked anyways upon polling or task fetching.
        }
    });
};

//-----------------------------------------------------------------------------

AGComm.prototype.pool = function (message) {
    this.msg_pool[message.ref] = message;
}

//-----------------------------------------------------------------------------

AGComm.prototype.is_pooled = function (message) {
    return this.msg_pool.hasOwnProperty(message.ref)
}

//-----------------------------------------------------------------------------

AGComm.prototype.pool_get = function (message) {
    this.msg_pool[message.ref];
}

//-----------------------------------------------------------------------------

AGComm.prototype.is_open = function () {
    return this.connected;
}

//-----------------------------------------------------------------------------

AGComm.prototype.open = function () {
    
    var _this = this;
    
    var get_url = this.url_updates + '?ch[]=messages/new&ch[]=notifications/new';
    
    if (!this.synched) {
        get_url += '&ch[]=messages/latest&ch[]=messages/unseen&ch[]=notifications/latest';
    }
    
    if (!this.ack) {
        get_url += '&noack=1';
    }
    
    $.ajax({
        type: "GET",
        url: get_url,
        dataType: "json",
        cache: false,
        //timeout: 50000,
        
        success: function (response) {
            
            var channels = response.body;
            // Check latest messages channel and process messages, if any.
            if (channels.messages.latest) {
                _this.fire_chat_latest_messages_received(channels.messages.latest);
            }
            // Check unseen messages channel and process messages, if any.
            if (channels.messages.unseen) {
                _this.fire_chat_unseen_messages_received(channels.messages.unseen);
            }
            // Check new messages channel and process messages, if any.
            if (channels.messages.new) {
                _this.fire_chat_messages_received(channels.messages.new);
            }
            // Check latest notifications channel and process notifications, if any.
            if (channels.notifications.latest) {
                _this.fire_latest_notifications_received(channels.notifications.latest);
            }
            // Check unseen notifications channel and process notifications, if any.
            if (channels.notifications.unseen) {
                _this.fire_unseen_notifications_received(channels.notifications.unseen);
            }
            // Check new notifications channel and process notifications, if any.
            if (channels.notifications.new) {
                _this.fire_notifications_received(channels.notifications.new);
            }
            
            if (!_this.synched)
                _this.synched = true;
            
            setTimeout(_this.open.bind(_this), 10000);
        },
        
        error: function (response) {
            setTimeout(_this.open.bind(_this), 15000);
        }
    });
    this.connected = true;
};

//-----------------------------------------------------------------------------
//
// AUGMENTED PROTOTYPES.
Array.prototype.has_msg = function (message) {
    for (var i = 0; i < this.length; i++) {
        if (this[i].ref === message.ref) {
            return true;
        }
    }
    return false;
}

