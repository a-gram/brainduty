"use strict"

/**
 * Main javascript code for the app's frontend. Must be included by all front end code.
 */


////////////////// DEBUG ONLY. TO BE REMOVED \\\\\\\\\\\\\\\\\\\\\  
// Usage: ag_assert( condition || new Error )
function ag_assert(event) {
    if (typeof event === 'boolean') {
        if (event)
            return true;
    } else if (typeof event === 'number') {
        if (event !== 0)
            return true;
    } else if (typeof event === 'string') {
        if (event !== '')
            return true;
    } else if (typeof event === 'object') {
        if (!(event instanceof Error)) {
            if (event.constructor === Array && event.length > 0)
                return true;
            if (event != null)
                return true;
        }
    } else if (typeof event === 'function') {
        return true;
    } else if (typeof event === 'undefined') {
    } else if (typeof event === null) {
    } else {
        throw new Error('Unrecognized assertion event.')
    }
    var msg = 'ASSERTION FAILED: ' + (event.message ? event.message : '');
    var linen = ('lineNumber' in event ? event.lineNumber : ('number' in event ? event.number : 'Unknown'));
    var file = ('fileName' in event ? event.fileName : ('file' in event ? event.file : 'Unknown'));
    ag_log(msg);
    ag_log('File: ' + file);
    ag_log('Line: ' + linen);
    throw event;
}

function ag_log(msg, obj) {
    console.log(msg);
    if (obj != null)
        console.log(obj);
}
//////////////////////////////////////////////////////////////////


var App = {

    /*
     *  The app's global page environment.
     */
    env: null,

    /*
     *  The app's com object.
     */
    com: null,

    /**
     *  An objects cache, if needed.
     */
    cache: null,

    /**
     *  Tha app's payment processor object, if needed. 
     */
    payment: null,

    /*
     *  Tha app's loaded main page. This is the top-level page
     *  which may contain (and tipically does contain) several
     *  'page views' or 'sub-pages'. For example in the user's
     *  dashboard the Dashboard object is the top-level page,
     *  while the Home, Settings, Booking, etc. are views or
     *  sub-pages of the dashboard.
     */
    page: null,

    /*
     * Utility functions. 
     */
    utils: null,

    /*
     * App's consts.
     */
    URL_BASE: null,
    URL_USER_LOGIN: null,
    URL_USER_FORGOT_PASSWORD: null,
    URL_USER_FAVOURITE: null,
    URL_USER_IGNORE: null,
    URL_USER_DELETE: null,
    URL_USER_DEFAULT_AVATAR: null,
    URL_TASK_GET: null,
    URL_TASKS_GET: null,
    URL_TASK_GET_WITH_RELATED: null,
    URL_TASK_FILE_GET: null,
    URL_TASK_PARTY_GET: null,
    URL_TASK_REQUEST: null,
    URL_TASK_COMMAND: null,
    URL_TASK_VIEW: null,
    URL_IMG: null,
    URL_IMG_NOTIFICATION: null,
    URL_CUSTOMER_HOME: null,
    URL_CUSTOMER_SIGNUP: null,
    URL_CUSTOMER_PAYMENT_REGISTRATION: null,
    URL_CUSTOMER_PAYMENT_UPDATE: null,
    URL_CUSTOMER_UPDATE: null,
    URL_CUSTOMER_SETTINGS: null,
    URL_CUSTOMER_CLOSED_TASKS: null,
    URL_PROVIDER_HOME: null,
    URL_PROVIDER_SIGNUP: null,
    URL_PROVIDER_UPDATE: null,
    URL_PROVIDER_IDENTITY: null,
    URL_PROVIDER_WITHDRAW: null,
    URL_PROVIDER_PAYOUT_UPDATE: null,
    URL_PROVIDER_SETTINGS: null,
    URL_PROVIDER_ACTIVITY: null,
    URL_PROVIDER_CLOSED_TASKS: null,
    URL_TXN_GET: null,
    URL_TXNS_GET: null,
    
    ROLE_SYSTEM: 1,
    ROLE_PROVIDER: 4,
    ROLE_CUSTOMER: 8,
    ROLE_GUEST: 16,

    TASK_SCHEDULED: 1,
    TASK_ONGOING: 2,
    TASK_ENDED: 3,
    TASK_CANCELLED: 4,
    TASK_COMPLETED: 5,
    TASK_DISPUTED: 6,

    MESSAGE_TASK_REQUEST: 1,
    MESSAGE_TASK_CONVERSATION: 2,
    MESSAGE_TASK_CHARGE_REQUEST: 3,
    MESSAGE_TASK_DELIVERABLES: 4,
    MESSAGE_TASK_CANCEL_REQUEST: 5,
    MESSAGE_TASK_END_REQUEST: 6,

    USER_REGISTERED: 1,
    USER_ONBOARDING: 2,
    USER_ACTIVE: 3,
    USER_INACTIVE: 4,
    USER_CANCELLED: 5,
    USER_SUSPENDED: 6,
    
    USER_TYPE_INDIVIDUAL : 'individual',
    USER_TYPE_SOLETRADER : 'soletrader',
    USER_TYPE_COMPANY : 'company',

    PROVIDER_IDENTITY_SUBMITTED: 1,
    PROVIDER_IDENTITY_VERIFYING: 2,
    PROVIDER_IDENTITY_INQUIRED: 3,
    PROVIDER_IDENTITY_VERIFIED: 4,
    PROVIDER_IDENTITY_UNVERIFIED: 5,

    TXN_PENDING: 'pending',
    TXN_COMPLETED: 'available',
    TXN_CANCELLED: 'cancelled',
    
    TASK_DISPUTE_SUBMITTED : 1,
    TASK_DISPUTE_PROCESSING : 2,
    TASK_DISPUTE_RESOLVED : 4,


    NOTIFICATION_PLATFORM_EVENT: 100,

    NOTIFICATION_TASK_EVENT: 200,
    TASK_SCHEDULED_EVENT: 201,
    TASK_STARTED_EVENT: 202,
    TASK_ENDED_EVENT: 203,
    TASK_CANCELLED_EVENT: 204,
    TASK_COMPLETED_EVENT: 205,
    TASK_DISPUTED_EVENT: 206,
    TASK_BID_REQUEST_EVENT: 207,
    TASK_RESCHEDULED_EVENT: 208,
    TASK_CHARGE_ADDED_EVENT: 209,
    TASK_CANCEL_REQUEST_ACCEPTED_EVENT: 210,
    TASK_CANCEL_REQUEST_REFUSED_EVENT: 211,
    TASK_CANCEL_REQUEST_EXPIRED_EVENT: 212,
    TASK_DISPUTE_RESOLVED_EVENT: 213,

    NOTIFICATION_TRANSACTION_EVENT: 300,

    NOTIFICATION_ACCOUNT_EVENT: 400,
    IDENTITY_INQUIRY_EVENT: 401,
    IDENTITY_VERIFIED_EVENT: 402,
    IDENTITY_UNVERIFIED_EVENT: 403,

    NOTIFICATION_ALERT_EVENT: 500,

    SERVICE_GENERIC: 1,
    SERVICE_EXPERT: 2,
    SERVICE_EXTRA: 3,

    LIST: true,

    ATTACH_FILE: 1,
    ATTACH_URL: 2,

    MASK_PLACEHOLDER: '*',
    MARKETPLACE_URL_FILE_UPLOAD: 'https://uploads.stripe.com/v1/files',
    SUPPORT_EMAIL: 'support@brainduty.com',
    
    
    /**
     * Initialize the frontend application.
     * 
     * This method must be called before using the frontend, usually
     * when the page is finished loading. It takes care of initializing
     * the app's environment and all the plugins used throughout.
     */
    init: function () {

        if (!this.env) {
            throw new Error('App.env not set.');
        }

        if (!this.env.URL_BASE) {
            throw new Error('App.env.URL_BASE not set.');
        }

        this.utils = new AGUtils;

        this.URL_BASE = this.env.URL_BASE;
        this.URL_USER_LOGIN = this.env.URL_BASE + '/user/login';
        this.URL_USER_FORGOT_PASSWORD = this.env.URL_BASE + '/user/password/reset';
        this.URL_USER_FAVOURITE = this.env.URL_BASE + '/user/{user_ref}/favourite';
        this.URL_USER_IGNORE = this.env.URL_BASE + '/user/{user_ref}/ignore';
        this.URL_USER_DELETE = this.env.URL_BASE + '/user/delete';
        this.URL_USER_DEFAULT_AVATAR = this.env.URL_BASE + '/img/def-avatar.png';
        this.URL_TASK_GET = this.URL_BASE + '/task/{task_ref}';
        this.URL_TASKS_GET = this.URL_BASE + '/task';
        //this.URL_TASK_GET_WITH_RELATED = this.URL_BASE + '/task/{task_ref}?with[]=services&with[]=messages.attachments&where[]=messages.deliverable&with[]=provider&where[]=provider.summary';
        this.URL_TASK_FILE_GET = this.URL_BASE + '/task/{task_ref}/file/{attachment_ref}';
        this.URL_TASK_PARTY_GET = this.URL_BASE + '/task/{task_ref}/party/{user_ref}';
        this.URL_TASK_REQUEST = this.URL_BASE + '/task/new/message'; //'/task';
        this.URL_TASK_COMMAND = this.URL_BASE + '/task/{task_ref}/{command}';
        this.URL_TASK_VIEW = this.URL_BASE + '/task/{task_ref}/view';
        this.URL_IMG = this.URL_BASE + '/img';
        this.URL_IMG_NOTIFICATION = this.URL_IMG + '/notification-{notification_type}.png';
        this.URL_CUSTOMER_HOME = this.URL_BASE + '/customer/home';
        this.URL_CUSTOMER_SIGNUP = this.URL_BASE + '/customer';
        this.URL_CUSTOMER_PAYMENT_REGISTRATION = this.URL_BASE + '/customer/payment';
        this.URL_CUSTOMER_PAYMENT_UPDATE = this.URL_BASE + '/customer/payment/{action}';
        this.URL_CUSTOMER_UPDATE = this.URL_BASE + '/customer';
        this.URL_CUSTOMER_SETTINGS = this.URL_BASE + '/customer/settings';
        this.URL_CUSTOMER_CLOSED_TASKS = this.URL_BASE + '/customer/tasks/closed';
        this.URL_PROVIDER_HOME = this.URL_BASE + '/provider/home';
        this.URL_PROVIDER_SIGNUP = this.URL_BASE + '/provider';
        this.URL_PROVIDER_UPDATE = this.URL_PROVIDER_SIGNUP;
        this.URL_PROVIDER_IDENTITY = this.URL_BASE + '/provider/identity';
        this.URL_PROVIDER_WITHDRAW = this.URL_BASE + '/transaction/withdraw';
        this.URL_PROVIDER_PAYOUT_UPDATE = this.URL_BASE + '/provider/payout/{action}';
        this.URL_PROVIDER_SETTINGS = this.URL_BASE + '/provider/settings';
        this.URL_PROVIDER_ACTIVITY = this.URL_BASE + '/provider/activity';
        this.URL_PROVIDER_CLOSED_TASKS = this.URL_BASE + '/provider/tasks/closed';
        this.URL_TXN_GET = this.URL_BASE + '/transaction/{txn_ref}';
        this.URL_TXNS_GET = this.URL_BASE + '/transaction';


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': this.get_csrf_token()
            }
        });

        window.ParsleyConfig = {
            excluded: 'input[type=button], input[type=submit], input[type=reset]',
            inputs: 'input, textarea, select, input[type=hidden], :hidden',
        };

        return this;
    },


    /**
     * Set and start the app's main top-level page (i.e. Dashboard, Portal, etc.)
     */
    run: function (page) {
        this.page = page;
        this.page.start();
    },


    /**
     * Get the request csrf token.
     */
    get_csrf_token: function () {
        return $('meta[name="csrf-token"]').attr('content');
    },


    /*
     *  Setup a cache for the app.
     */
    create_cache: function () {
        this.cache = new HomeMadeCache;
    },

    /**
     * Process an ajax/api failure by extracting the error response from a given object,
     * which is generally a jqXHR but maybe be something else. All API error responses
     * have the following format:
     * 
     *  {
     *      "status":   "error",
     *      "error_code": n,
     *      "error_description": "A short description of the error",
     *      "error_details":
     *       [ 
     *             {  
     *                 "message" : "A detailed specific error message",
     *                 "culprit" : "The name of the entity/field that caused the error"
     *             }
     *             
     *             ...
     *                 
     *       ] | null
     *  }
     *  
     */
    get_api_error: function (data) {
        var msg = null;

        if (data.hasOwnProperty('error_description') &&
            data.hasOwnProperty('error_details')) {
            return data;
        } else if (data.hasOwnProperty('responseText') &&
            data.hasOwnProperty('responseJSON') &&
            data.responseJSON) {
            return data.responseJSON;
        } else if (typeof data === 'string') {
            msg = data;
        } else {
            // TODO scan the data for an error response.
        }
        // Return a generic error response if none is found.
        return {
            "status": "error",
            "error_code": 500,
            "error_description": (msg ? msg : "An error occurred."),
            "error_details": null
        };
    },


    /**
     * Same as get_api_error() but for successful API calls that return
     * an informational message. API success responses have the following
     * format
     * 
     *  {
     *     "status" : "ok"
     *     "body" : {
     *          "message" : "Some message here" (optional)
     *          ...
     *     }
     *  }
     *  
     */
    get_api_message: function (data) {
        var msg = null;

        if (data.hasOwnProperty('body') &&
            data.body &&
            data.body.hasOwnProperty('message')) {
            return data.body.message;
        } else if (data.hasOwnProperty('responseText') &&
            data.hasOwnProperty('responseJSON') &&
            data.responseJSON &&
            data.responseJSON.body) {
            return data.responseJSON.body.message;
        } else if (typeof data === 'string') {
            msg = data;
        } else {
            // TODO scan the data for a message response.
        }
        // Return a generic success message if none is found.
        return msg ? msg : 'The action completed successfully.';
    },


    /*
     * Convenience function to check whether a message is transactional.
     */
    is_transactional: function (message) {
        return message.type != App.MESSAGE_TASK_CONVERSATION;
    },

    /*
     * Convenience function to check whether a notification is a task event.
     */
    is_task_event: function (notification) {
        return notification.type == App.NOTIFICATION_TASK_EVENT;
    },

    /*
     * Convenience function to check whether a notification is a task event.
     */
    is_account_event: function (notification) {
        return notification.type == App.NOTIFICATION_ACCOUNT_EVENT;
    },

    /*
     * Convenience function to check whether a user is a customer.
     */
    is_user_customer: function (user) {
        return user.role == App.ROLE_CUSTOMER;
    },

    /*
     * Convenience function to check whether a user is a provider.
     */
    is_user_provider: function (user) {
        return user.role == App.ROLE_PROVIDER;
    },
    
    /*
     * Convenience function to check whether a user is a provider.
     */
    is_user_business: function (user) {
        return user.entity_type == App.USER_TYPE_SOLETRADER ||
               user.entity_type == App.USER_TYPE_COMPANY;
    },

    /**
     * Check whether this task is open (not cancelled or completed).
     */
    is_task_open : function(task) {
        return task.status != App.TASK_CANCELLED &&
               task.status != App.TASK_COMPLETED;
    },
    
    /**
     * Check whether this task is closed (either cancelled or completed).
     */
    is_task_closed : function(task) {
        return task.status == App.TASK_CANCELLED ||
               task.status == App.TASK_COMPLETED;
    },
    
    /**
     * Determine the status of a transaction.
     */
    get_txn_status: function(txn) {
        return this.txn_status_map(txn.settle_status) == App.TXN_CANCELLED ?
                                                         App.TXN_CANCELLED :
                                                         App.txn_status_to_string(txn.status);
    },
    
    /*
     * Convenience function to convert task states into human-readable stuff.
     */
    task_status_to_string: function (status) {
        switch (status) {
            case App.TASK_SCHEDULED:  return 'scheduled';  break;
            case App.TASK_ONGOING:    return 'ongoing';    break;
            case App.TASK_ENDED:      return 'ended';      break;
            case App.TASK_CANCELLED:  return 'cancelled';  break;
            case App.TASK_COMPLETED:  return 'completed';  break;
            case App.TASK_DISPUTED:   return 'disputed';   break;
            default: return undefined;
        }
    },

    /*
     * Convenience function to convert user identity states into human-readable stuff.
     */
    identity_status_to_string: function (status) {
        switch (status) {
            case App.PROVIDER_IDENTITY_SUBMITTED:   return 'submitted'; break;
            case App.PROVIDER_IDENTITY_VERIFYING:   return 'verifying'; break;
            case App.PROVIDER_IDENTITY_INQUIRED:    return 'inquirying'; break;
            case App.PROVIDER_IDENTITY_VERIFIED:    return 'verified';   break;
            case App.PROVIDER_IDENTITY_UNVERIFIED:  return 'unverified'; break;
            default: return undefined;
        }
    },

    /*
     * Convenience function to convert user states into human-readable stuff.
     */
    user_status_to_string: function (user) {
        switch (user.status) {
            case App.USER_REGISTERED: return 'registered'; break;
            case App.USER_ONBOARDING: return 'onboarding'; break;
            case App.USER_SUSPENDED:  return 'suspended';  break;
            case App.USER_ACTIVE:     return 'active';     break;
            case App.USER_INACTIVE:   return 'inactive';   break;
            case App.USER_CANCELLED:  return 'cancelled';  break;
            default: status = undefined;
        }
    },

    /*
     * Convenience function to convert transaction states into human-readable stuff.
     */
    txn_status_to_string: function (status) {
        switch (status) {
            case App.TXN_PENDING:   return 'pending';  break;
            case App.TXN_COMPLETED: return 'available'; break;
            case App.TXN_CANCELLED:  return 'cancelled'; break;
            default: return undefined;
        }
    },
    
    
    /*
     * Convenience function to convert user roles into human-readable names.
     */
    user_role_to_string: function (role) {
        switch(role) {
            case App.ROLE_SYSTEM:   return 'system'; break;
            case App.ROLE_PROVIDER: return 'provider'; break;
            case App.ROLE_CUSTOMER: return 'customer'; break;
            default: return undefined;
        }
    },

    
    /*
     * Convenience function to convert provided services into human-readable names.
     */
    service_to_string: function (service) {
        switch(service) {
            case App.SERVICE_GENERIC: return 'generic'; break;
            case App.SERVICE_EXPERT:  return 'expert'; break;
            case App.SERVICE_EXTRA:   return 'extra'; break;
            default: return undefined;
        }
    },
    
    /**
     * Map a generic transaction status identifier into the app identifier.
     */
    txn_status_map: function(status) {
        switch(status) {
            case 'completed': return App.TXN_COMPLETED; break;
            case 'succeeded': return App.TXN_COMPLETED; break;
            case 'available': return App.TXN_COMPLETED; break;
            case 'paid':      return App.TXN_COMPLETED; break;
        
            case 'cancelled': return App.TXN_CANCELLED; break;
            case 'failed':    return App.TXN_CANCELLED; break;
            case 'aborted':   return App.TXN_CANCELLED; break;
        
            case 'pending':    return App.TXN_PENDING; break;
            case 'ongoing':    return App.TXN_PENDING; break;
            case 'in_transit': return App.TXN_PENDING; break;
            case 'processing': return App.TXN_PENDING; break;
            default: return undefined;
        }
    },


    /*
     * The app's currency.
     */
    currency: function () {
        return App.env.currency || 'AUD';
    },

    /*
     * Compute the net amount of a transaction.
     */
    net_amount_of: function (txn) {
        return (parseInt(txn.amount_gross) +
            parseInt(txn.amount_fee) +
            parseInt(txn.amount_tax));

    },


    /******************************************************************
     *                Frontend initialization stuffs. 
     **************************************************************** */


    core: function () {

        var getLayoutColors = function (theme) {

            var theme_choice = (theme === undefined || theme === '') ? 'slate' : theme

            var color_object = {
                slate: ['#D74B4B', '#475F77', '#BCBCBC', '#777777', '#6685a4', '#E68E8E'],
                belize: ['#2980b9', '#7CB268', '#A9A9A9', '#888888', '#74B5E0', '#B3D1A7'],
                square: ['#6B5C93', '#444444', '#569BAA', '#AFB7C2', '#A89EC2', '#A9CCD3'],
                pom: ['#e74c3c', '#444444', '#569BAA', '#AFB7C2', '#F2A299', '#A9CBD3'],
                royal: ['#3498DB', '#2c3e50', '#569BAA', '#AFB7C2', '#ACCDD5', '#6487AA'],
                carrot: ['#E5723F', '#67B0DE', '#373737', '#BCBCBC', '#F2BAA2', '#267BAE']
            }

            return color_object[theme_choice]
        }

        var isLayoutCollapsed = function () {
            return $('.navbar-toggle').css('display') == 'block'
        }

        var initLayoutToggles = function () {
            $('.navbar-toggle, .mainnav-toggle').click(function (e) {
                $(this).toggleClass('is-open')
            })
        }

        var initBackToTop = function () {
            var backToTop = $('<a>', {
                    id: 'back-to-top',
                    href: '#top'
                }),
                icon = $('<i>', {
                    'class': 'fa fa-chevron-up'
                })

            backToTop.appendTo('body')
            icon.appendTo(backToTop)

            backToTop.hide()

            $(window).scroll(function () {
                if ($(this).scrollTop() > 150) {
                    backToTop.fadeIn()
                } else {
                    backToTop.fadeOut()
                }
            })

            backToTop.click(function (e) {
                e.preventDefault()

                $('body, html').animate({
                    scrollTop: 0
                }, 600)
            })
        }

        var navEnhancedInit = function () {
            $('.mainnav-menu').find('> .active').addClass('is-open')

            $('.mainnav-menu > .dropdown').on('show.bs.dropdown', function () {
                $(this).addClass('is-open')
                $(this).siblings().removeClass('is-open')
            })
        }

        var navHoverInit = function (config) {
            $('[data-hover="dropdown"]').each(function () {
                var $this = $(this),
                    defaults = {
                        delay: {
                            show: 1000,
                            hide: 1000
                        }
                    },
                    $parent = $this.parent(),
                    settings = $.extend(defaults, config),
                    timeout

                if (!('ontouchstart' in document.documentElement)) {
                    $parent.find('.dropdown-toggle').click(function (e) {
                        if (!isLayoutCollapsed()) {
                            e.preventDefault()
                            e.stopPropagation()
                        }
                    })
                }

                $parent.mouseenter(function () {
                    if (isLayoutCollapsed()) {
                        return false
                    }

                    timeout = setTimeout(function () {
                        $parent.addClass('open')
                        $parent.trigger('show.bs.dropdown')
                    }, settings.delay.show)
                })

                $parent.mouseleave(function () {
                    if (isLayoutCollapsed()) {
                        return false
                    }

                    clearTimeout(timeout)

                    timeout = setTimeout(function () {
                        $parent.removeClass('open keep-open')
                        $parent.trigger('hide.bs.dropdown')
                    }, settings.delay.hide)
                })
            })
        }

        var initNavbarNotifications = function () {
            var notifications = $('.navbar-notification')

            notifications.find('> .dropdown-toggle').click(function (e) {
                //	      if (App.core.isLayoutCollapsed ()) {
                //	        window.location = $(this).prop ('href')
                //	      }
            })

            notifications.find('.notification-list').slimScroll({
                height: 225,
                railVisible: true
            })
        }

        var initMastheadCarousel = function () {
            if (!$.fn.carousel) {
                return false
            }

            var currItem = $('.masthead-carousel .item.active')

            $('.masthead-carousel')
                .carousel({
                    interval: false
                })
                .on('slide.bs.carousel', function (e) {
                    var next = $(e.relatedTarget),
                        nextH = next.height(),
                        active = $(this).find('.active.item')

                    if (currItem.height() > nextH) {
                        next.height(active.height())
                    }

                    active.parent().animate({
                        height: nextH
                    }, 500, function () {
                        next.height(nextH)
                        currItem = $(e.relatedTarget)
                    })
                })

            $(window).resize(function () {
                var item = $('.active.item'),
                    curH = item.height()

                item.parent().height(curH)
            })
        }


        return {
            navEnhancedInit: navEnhancedInit,
            navHoverInit: navHoverInit

            ,
            initBackToTop: initBackToTop,
            isLayoutCollapsed: isLayoutCollapsed,
            initLayoutToggles: initLayoutToggles

            ,
            layoutColors: getLayoutColors('slate'),
            initNavbarNotifications: initNavbarNotifications,
            initMastheadCarousel: initMastheadCarousel
        }

    }(),


    components: function () {

        var initFormValidation = function () {
            if ($.fn.parsley) {
                $('.parsley-form').each(function () {
                    $(this).parsley({
                        trigger: 'change',
                        errorsContainer: function (el) {
                            if (el.$element.parents('form').is('.form-horizontal')) {
                                return el.$element.parent("*[class^='col-']")
                            }

                            return el.$element.parents('.form-group')
                        },
                        errorsWrapper: '<ul class="parsley-errors-list"></ul>'
                    })
                })
            }
        }

        var initAccordions = function () {
            $('.accordion-simple, .accordion-panel').each(function (i) {
                var accordion = $(this),
                    toggle = accordion.find('.accordion-toggle'),
                    activePanel = accordion.find('.panel-collapse.in').parent()

                activePanel.addClass('is-open')

                toggle.prepend('<i class="fa accordion-caret"></i>')

                toggle.on('click', function (e) {
                    var panel = $(this).parents('.panel')

                    panel.toggleClass('is-open')
                    panel.siblings().removeClass('is-open')
                })
            })
        }

        var initTooltips = function () {
            if ($.fn.tooltip) {
                $('.ui-tooltip').tooltip({
                    container: 'body'
                })
            }
            if ($.fn.popover) {
                $('.ui-popover').popover({
                    container: 'body'
                })
            }
        }

        var initLightbox = function () {
            if ($.fn.magnificPopup) {
                $('.ui-lightbox').magnificPopup({
                    type: 'image',
                    closeOnContentClick: false,
                    closeBtnInside: true,
                    fixedContentPos: true,
                    mainClass: 'mfp-no-margins mfp-with-zoom',
                    image: {
                        verticalFit: true,
                        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
                    }
                })

                $('.ui-lightbox-video, .ui-lightbox-iframe').magnificPopup({
                    disableOn: 700,
                    type: 'iframe',
                    mainClass: 'mfp-fade',
                    removalDelay: 160,
                    preloader: false,
                    fixedContentPos: false
                })

                $('.ui-lightbox-gallery').magnificPopup({
                    delegate: 'a',
                    type: 'image',
                    tLoading: 'Loading image #%curr%...',
                    mainClass: 'mfp-img-mobile',
                    gallery: {
                        enabled: true,
                        navigateByImgClick: true,
                        preload: [0, 1]
                    },
                    image: {
                        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                        titleSrc: function (item) {
                            return item.el.attr('title')
                        }
                    }
                })
            }
        }

        var initSelect = function () {
            if ($.fn.select2) {
                $('.ui-select').select2({
                    allowClear: true,
                    placeholder: "Select...",
                    theme: 'classic'
                })
            }
        }

        var initIcheck = function () {
            if ($.fn.iCheck) {
                $('.ui-check').iCheck({
                    checkboxClass: 'ui-icheck icheckbox_minimal-grey',
                    radioClass: 'ui-icheck iradio_minimal-grey',
                    inheritClass: true
                }).on('ifChanged', function (e) {
                    $(e.currentTarget).trigger('change')
                })
            }
        }

        var initDataTableHelper = function () {
            if ($.fn.dataTableHelper) {
                $('.ui-datatable').dataTableHelper()
            }
        }

        var initiTimePicker = function () {
            if ($.fn.timepicker) {
                $('.ui-timepicker').timepicker()
                $('.ui-timepicker-modal').timepicker({
                    template: 'modal'
                })
            }
        }

        var initDatePicker = function () {
            if ($.fn.datepicker) {
                $('.ui-datepicker').datepicker({
                    autoclose: true,
                    todayHighlight: true
                })
            }
        }

        var initPlaceholder = function () {
            $.support.placeholder = false
            var test = document.createElement('input')
            if ('placeholder' in test) $.support.placeholder = true

            if (!$.support.placeholder) {
                $('.placeholder-hidden').show()
            }
        }
        
        
        var initAlerts = function() {
            Messenger.options = {
               extraClasses: 'messenger-fixed messenger-on-bottom',
               theme: 'flat'
            };
        }

        return {
            initAccordions: initAccordions,
            initFormValidation: initFormValidation,
            initTooltips: initTooltips,
            initLightbox: initLightbox,
            initSelect: initSelect,
            initIcheck: initIcheck,
            initDataTableHelper: initDataTableHelper,
            initiTimePicker: initiTimePicker,
            initDatePicker: initDatePicker,
            initPlaceholder: initPlaceholder,
            initAlerts: initAlerts
        }

    }(),
    
    // -------------------------------------------------------------------

    
    /**
     * Set the given button in the busy/loading state.
     * 
     * @param {type} button
     */
    busy: function(button) {
        if(!button) $.error('No button given.');
        button.prop('disabled', true);
        button.addClass('busy');
    },
    
    /**
     * Unset the given button from the busy/loading state.
     * 
     * @param {type} button
     */
    done: function(button) {
        if(!button) $.error('No button given.');
        button.prop('disabled', false);
        button.removeClass('busy');
    }

    
} // App


/**
 *  A very rough implementation of a very common and useful data structure
 *  that Javascript doesn't support adequately.
 *  IMPORTANT NOTE: MAKE SURE THAT 'KEYS' USED TO ASSOCIATE WITH VALUES
 *  ARE UNIQUE STRINGS!
 *  (Would be nice to use Map but doesn't seem to be decently supported yet
 *   across all browsers and platforms, expecially on mobile ...)
 */

function HomeMadeCache() {
    this.cache = {};
    this.get = function (key) {
        return this.cache[key];
    };
    this.set = function (key, value) {
        this.cache[key] = value;
        return this.cache;
    }
    this.remove = function (key) {
        delete this.cache[key];
    }
}


/**
 *  This class encapsulates the functionality to deal with tokenizing
 *  card details, bank account details and other sensitive data.
 */

function PaymentProcessor() {
    
    this.init = function() {
        if(!App.env.market_pkey)
            App.page.abort('No pub key found');
        Stripe.setPublishableKey(App.env.market_pkey);
    };

    this.tokenizeCard = function (card, callback) {
        
        Stripe.card.createToken({
           number          : card.number,
           cvc             : card.cvc,
           exp_month       : card.expMonth,
           exp_year        : card.expYear,
           name            : card.holderName,
           address_line1   : card.holderAddress,
           address_zip     : card.holderZip,
           address_country : card.holderCountry
        }, 
        function(status, response) {
            
           if(status == 200) {
              ag_log('Stripe: Got card token');
              var data = {
                  token   : response.id,
                  details : response.card
              }
              callback(null, data);
           } else {
              ag_log('Stripe: Could not get card token');
              callback(response.error.message, null);
           }
        });
    };

    this.tokenizeBankInfo = function (bank, callback) {
        // Split the bank number into its components (bsb, account num)
        var bcomp = bank.number.split(" ");

        if (bcomp.length != 2) {
            callback('Incorrect bank account number format.', null);
            return;
        }
        
        Stripe.bankAccount.createToken({
           account_holder_name : bank.holder,
           //account_holder_type : bank.entity_type,
           country             : App.utils.country_to_iso3166(bank.billing.country),
           currency            : App.currency(),
           routing_number      : bcomp[0], // bsb
           account_number      : bcomp[1]
        }, 
        function(status, response) {
            
           if(status == 200) {
              ag_log('Stripe: Got bank token');
              var data = {
                  token   : response.id,
                  details : response.bank_account
              }
              callback(null, data);
           } else {
              ag_log('Stripe: Could not get bank token');
              callback(response.error.message, null);
           }
        });
    };
    
    this.init();
};


/**
 * Format a numeric value into a currency string.
 * (Adapted from http://stackoverflow.com/questions/149055/how-can-i-format-numbers-as-money-in-javascript/14428340#comment34151293_14428340)
 * 
 * @param string  m: currency symbol
 * @param integer n: length of decimal
 * @param integer x: length of whole part
 * @param mixed   s: sections delimiter
 * @param mixed   c: decimal delimiter
 */
Number.prototype.to_currency = function (m, n, x, s, c) {

    m = m != undefined ? m : App.currency();
    n = n != undefined ? n : 2;

    var num = this / 100;
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = num.toFixed(Math.max(0, ~~n));

    return (m ? m + ' ' : '') + (c ? num.replace('.', c) : num).
    replace(new RegExp(re, 'g'), '$&' + (s || ','));
};