"use strict";

/**
 * This namespace handles the task request process.
 */

function BookingPage() {
    
    UserPage.call(this);
    
    this.request_form = { files: [], formData: [] };

    /**
     *  Whether the customer has a registered payment method.
     *  This is indicated by the presence of the customer's
     *  payment details form: if it's sent by the server then
     *  the customer has no registered payment method, and viceversa.
     */
    this.customer_has_payment = false;

	
    this.initialize = function() {
    		    
    	// Setup views.
    	this.wizard_view.setup(this);    	

    	this.customer_has_payment = !this.wizard_view.has_customer_payment_form();
    };

    
    /////////////////////////////////////////////////////////////////////////////
    ///                          EVENT HANDLERS                               ///
    /////////////////////////////////////////////////////////////////////////////

    
    /////////////////////////////////////////////////////////////////////////////
    
    
    
    /* ***********************************************************************
     *                          Booking wizard view
     * ***********************************************************************/
    
    
    this.wizard_view = {

       	page               : null,
       	view               : $('#process-wizard'),
       	validate           : [0],
       	steps              : [   0, // Steps are 1-based.
       	                         $('#wizard-step-1'),
       	                         $('#wizard-step-2'),
       	                         $('#wizard-step-3')
       	                     ],
       	form               : $('#task-request-form').parsley(),
        button_submit      : $('#task-request-form button.submit'),
        service_list       : new AGList,
       	
       	
       	setup : function(page) {
       		
            this.page = page;

            this.set_step_validators ();
            this.service_list.setup(this.page, 'services', 'single');
        	
            App.com.event_chat_message_attachments_added = this.on_message_attachments_added.bind(this);
            App.com.event_chat_message_posted = this.on_message_post_success.bind(this);
            App.com.event_chat_message_post_failed = this.on_message_post_fail.bind(this);
            App.com.event_chat_message_post_progress = this.on_message_post_progress.bind(this);

            this.view.find('button.next').click(this.on_btn_next_click.bind(this));
            this.view.find('button.back').click(this.on_btn_back_click.bind(this));
            this.view.find('button.submit').click(this.on_btn_submit_click.bind(this));
            this.view.find('select.categories').change(this.on_service_selected);

            this.view.find('#task-message').val(''); // workaround for textareas not empty
       	},
       	
       	
       	set_step_validators : function () {
       		
            var _this = this;

            // Step 1 (Task type selection) validator.
            this.validate.push(function(){

                var task_type = _this.get_task_type();
                var task_type_field = _this.steps[1].find('input[name="task_type"]');
                task_type_field.val( task_type );
                task_type_field.parsley().validate();

                if(!task_type_field.parsley().isValid())
                   return false;
               
                if(_this.has_categories(task_type)) {
                   var categories = _this.view.find('#service-'+task_type+'-categories > select');
                   categories.parsley().validate();
                   if(!categories.parsley().isValid())
                      return false;
                }

                return true;
            });


            // Step 2 (Task details) validator.
            this.validate.push(function(){

                if(!_this.form.validate({group:'step-2'}))
                   return false;

                // Remove the payment form if not needed and show the
                // registered payment method that will be used.
                if (_this.page.customer_has_payment) {
                    _this.page.wizard_view.remove_customer_payment_form();
                }
                return true;
            });


            // Step 3 (Payment details) validator.
            this.validate.push(function(){

                if(_this.has_customer_payment_form() &&
                  !_this.form.validate({group:'step-3'})) {
                   return false;
                }
                return true;
            });

       	},
       	
       	has_categories : function(service) {
            return this.steps[1].find('#service-'+service+'-categories').length>0;
       	},
       	

       	get_task_type : function() {
            var selected = this.service_list.get_selected();
            return selected.length ? selected[0] : '';
       	},
       	
       	
        // Get the selected categories ids for the specified service as a list string
       	get_task_categories_as_id_string : function(service) {
            var field = this.view.find('#service-'+service+'-categories > select');
            return field.length ? field.val() : '';
       	},
       	
        
       	get_task_info : function() {
            return {
                type       : this.get_task_type(),
                categories : this.get_task_categories_as_id_string(this.get_task_type()),
                assignee   : this.view.find('#task-favourite').length ? // may not be present
                             this.view.find('#task-favourite').val() : '',
                subject    : this.view.find('#task-subject').val(),
                message    : this.view.find('#task-message').val(),
            };
       	},
       	
       	
       	get_customer_info : function() {
       		
            var info = this.view.find('#customer-payment-details');
            return {
                first_name  : info.find('#first-name').val(),
                last_name   : info.find('#last-name').val(),
                address     : info.find('#address').val(),
                zip_code    : info.find('#zip-code').val(),
                city        : info.find('#city').val(),
                state       : info.find('#state').val(),
                country     : info.find('#country').val()
            };
       	},
        
        
        get_customer_payment_method : function() {
            return this.view.find('input[name=payment-method]:checked').val();
        },
        
       	
       	get_customer_card_info : function() {
       		
            var card = this.view.find('#customer-payment-details');
            return {
                number   : card.find('#card-number').val(),
                expMonth : card.find('#card-expiry-month').val(),
                expYear  : card.find('#card-expiry-year').val(),
                cvc      : card.find('#card-cvv').val(),
            };
       	},

       	
       	has_customer_payment_form : function() {
            return this.view.find('#customer-payment-details').length !== 0;
       	},
       	
       	
       	remove_customer_payment_form : function(){
            this.view.find('#customer-payment-details').remove();
            this.view.find('#customer-default-payment').show();
       	},
       	
       	
       	get_attachments : function() {
            return this.view.find('.message-box-attachments');
       	},

       	
       	set_customer_default_payment_view : function(payment) {
            var payment_view = this.view.find('#customer-default-payment');
            payment_view.find('#method').text(payment.method);
            payment_view.find('#type').text(payment.source_type);
            payment_view.find('#number').text(payment.source_number);
            payment_view.find('#expiry').text(payment.source_expiry || 'N/A');
       	},
       	
       	
        on_service_selected : function() {
            var selected = $(this).find('option:selected');
            var price = selected.data('price');
            var amount = $(this).parents('.list-group-item').find('.item-price .amount');
            amount.text(price ? price.to_currency() : 'Select category');
        },
        
        
       	on_message_attachments_added : function(files) {
    	    this.page.request_form.files = files;
    	    
    	    var items = '';
            $.each(files, function(i,file){
                    items += App.page.factory.make_attachment_nolink(file.name);
            });
            items = App.page.factory.make_attachment_nolink(items, App.LIST);

            var attachments = this.get_attachments();
            attachments.empty();
            attachments.append(items);
    	},

    	on_message_post_success : function(message) {
            App.page.dialog.show('info', message.body, '', [{
                type:'ok',
                label:'Ok',
                callback:function(){ App.page.change(App.URL_CUSTOMER_HOME); }
            }]);
    	},
    	
    	on_message_post_fail : function(response) {
            App.done(this.button_submit);
            App.page.on_error(response);
    	},
    	
    	on_message_post_progress : function(progress) {
            // TODO Add a nice progress bar :-)
    	},

    	
       	on_btn_next_click : function(event) {
       		
            var _this = this;
            var curr_step = $(event.currentTarget).attr('data-step');

            if(!_this.validate[curr_step]())
               return;
        	
            var next_step = parseInt(curr_step) + 1;

            _this.steps[curr_step].fadeOut('slow', function() {
                _this.steps[next_step].fadeIn('slow');
            });
       	},
       	
       	
       	on_btn_back_click : function(event) {
       		
            var _this = this;
            var curr_step = $(event.currentTarget).attr('data-step');
            var prev_step = parseInt(curr_step) - 1;

            _this.steps[curr_step].fadeOut('slow', function() {
                _this.steps[prev_step].fadeIn('slow');
            });
       	},
       	
       	
       	on_btn_submit_click : function(event) {
       		
            var _this = this;

            App.busy(_this.button_submit);

            var task = _this.get_task_info();

            // If customer has no payment method, we need to collect the method
            // details and register it before submitting the task, else submit the
            // task straight away.
            if (!_this.page.customer_has_payment) {

                if(!_this.validate[ _this.steps.length-1 ]()) {
                   App.done(_this.button_submit);
                   return;
                }

                var payment_method = _this.get_customer_payment_method();
                
                var card = _this.get_customer_card_info();

                // Card holder info required by some payment processors.
                var customer = _this.get_customer_info();

                card.holderName    = customer.first_name+' '+customer.last_name;
                card.holderAddress = customer.address;
                card.holderCity    = customer.city;
                card.holderCountry = customer.country;
                card.holderZip     = customer.zip_code;

                if(!App.payment)
                    App.payment = new PaymentProcessor();

                // Get the tokenized card or an error.
                App.payment.tokenizeCard (card, function(error, tcard) {

                    if (error) {
                        App.done(_this.button_submit);
                        App.page.on_error('Couldn\'t process your card: '+error);
                    } else {
                       // We got a card token. Now gather all customer and payment
                       // info and start the registration.
                       var customer_payment = {
                           customer : customer,
                           payment  : {
                               method        : payment_method,
                               source_id     : tcard.token,
                               source_number : '*******'+tcard.details.last4,
                               source_type   : tcard.details.brand,
                               source_name   : tcard.details.name,
                               source_expiry : tcard.details.exp_month + '/' + tcard.details.exp_year
                           }
                       };
                       // Register the customer's payment method
                       _this.page.register_payment_method(customer_payment);
                    }
                });
            }
            else {
                _this.page.book(task);
            }
       	}
       	
       	
    };

    
    /**
     *  Register a customer payment method, if needed.
     *  @param data Payment and Customer (optional) details.
     */
    this.register_payment_method = function(data) {
    	
       var _this = this;
	   
       // These info cannot be changed, so no point in posting them.
       delete data.customer.first_name;
       delete data.customer.last_name;
       
        $.ajax({
            url: App.URL_CUSTOMER_PAYMENT_REGISTRATION,
            type: "POST",
            dataType: "json",
            data: data,

            success: function (response) {
                // Customer payment registration successful. At this point we may
                // remove the customer/payment form and, show the submit form.
                _this.customer_has_payment = true;
                _this.wizard_view.set_customer_default_payment_view(data.payment);
                _this.wizard_view.remove_customer_payment_form();
                App.done(_this.wizard_view.button_submit);
            },
            error: function (response) {
                App.done(_this.wizard_view.button_submit);
                App.page.on_error(response);
            }
        });
    };

    
    /**
     * Submit the task.
     */
    this.book = function(task) {

        var _this = this;

         // Add the task fields to the multipart form.
        this.request_form.formData = [
           { name  : 'message[type]', value : App.MESSAGE_TASK_REQUEST },
           { name  : 'message[body]', value : task.message },
           { name  : 'message[data][type]', value : task.type },
           { name  : 'message[data][catags]', value : task.categories },
           { name  : 'message[data][subject]', value : task.subject },
           { name  : 'message[data][assignee]', value : task.assignee }
        ];

        App.com.post_chat_message(this.request_form, 'new');
    };
	    

    this.initialize();
    
};


// AUGMENTED PROTOTYPES.


Dashboard.prototype.create_page = function() {
    return new BookingPage;
};


