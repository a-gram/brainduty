"use strict";

/**
 * This namespace handles a task dispute process.
 */

function DisputePage() {
	
    UserPage.call(this);

    this.request_form = { files: [], formData: [] };
	
	
    this.initialize = function() {
    		    
    	// Setup views.
    	this.wizard_view.setup(this);    	
    };

    
    this.wizard_view = {

       	page               : null,
       	view               : $('#process-wizard'),
       	validate           : [0],
       	steps              : [   0, // Steps are 1-based.
       	                         $('#wizard-step-1')
       	                     ],
       	form               : $('#task-dispute-form'),
       	dispute_docs_files :   '#dispute-message-box input[name="files[]"]',
        button_submit      : $('#task-dispute-form button.submit'),
       	
       	
       	setup : function(page) {

            this.page = page;

            this.set_step_validators ();

                 // Initialize the request form
                $(this.dispute_docs_files).fileupload({
                         dataType    : 'json',
                         singleFileUploads: false,
                         add         : this.on_dispute_documents_attached_fu.bind(this),
                         done        : this.on_dispute_post_success_fu.bind(this),
                         fail        : this.on_dispute_post_fail_fu.bind(this),
                         progressall : this.on_dispute_post_progress_fu.bind(this)
                }).prop('disabled', !$.support.fileInput)
                  .parent().addClass($.support.fileInput ? undefined : 'disabled');

            this.view.find('button.submit').click(this.on_btn_submit_click.bind(this));

            this.view.find('#dispute-message').val(''); // workaround for textareas not empty
       	},
       	
       	
       	set_step_validators : function () {
       		
            var _this = this;

            this.validate.push(function(){

                var dispute_reason = _this.get_dispute_reason();
                var reason = _this.steps[1].find('input[name="reason"]');
                    reason.val(dispute_reason);

                    reason.parsley().validate();

                if(!reason.parsley().isValid())
                   return false;

                if(!_this.form.parsley().validate({group:'step-1'}))
                   return false;

                return true;
            });
       	},
       	
       	
       	get_dispute_reason : function() {
            return this.view.find('select').val();
       	},
       	
        
       	get_dispute_message : function() {
            return this.view.find('#dispute-message').val();
       	},

        
       	get_attachments : function() {
            return this.view.find('.message-box-attachments');
       	},

       	       	
        // ------------- Fileupload handlers -------------
       	
    	on_dispute_documents_attached_fu : function(e,data) {
            var files = data.files;
    	    this.page.request_form.files = files;
    	    
    	    var items = '';
            $.each(files, function(i,file){
                    items += App.page.factory.make_attachment_nolink(file.name);
            });
            items = App.page.factory.make_attachment_nolink(items, App.LIST);

            var attachments = this.get_attachments();
            attachments.empty();
            attachments.append(items);
    	},

    	on_dispute_post_success_fu : function(e,data) {
            this.submit_succeeded(data.result);
    	},
    	
    	on_dispute_post_fail_fu : function(e,data) {
            this.submit_failed(data._response.jqXHR);
    	},
    	
    	on_dispute_post_progress_fu : function(e,data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            // TODO Add a nice progress bar :-)
    	},
    	
    	// -----------------------------------------------

       	
       	on_btn_submit_click : function(event) {
       		
            var _this = this;

            if(!_this.validate[ _this.steps.length-1 ]()) {
               return;
            }
            
    	    // Add the fields to the dispute multipart form.
            _this.page.request_form.formData = [
                   { name  : 'dispute[reason]', value : _this.get_dispute_reason() },
                   { name  : 'dispute[details]', value : _this.get_dispute_message() }
            ];

            App.busy(_this.button_submit);
     		
            // Apparently 'fileupload' does not submit the data if there
            // are no files selected even though there are form fields attached,
            // so in such case we send the form fields with an ajax post.
    	    if(_this.page.request_form.files.length){
    	       $(_this.dispute_docs_files).fileupload('send', _this.page.request_form);
    	    } else {
                var postData = {};
                var postUrl = _this.form.attr('action');
                $.each(_this.page.request_form.formData, function(i,field){
                         postData[field.name] = field.value;
                });
                     $.ajax({
                        url: postUrl,
                        type: "POST",
                        dataType: "json",
                        data: postData,

                        success: function (response) {
                           _this.submit_succeeded(response);
                        },
                        error: function (response) {
                           _this.submit_failed(response);
                        }
                     });
    	    }
       	},
        
        submit_succeeded : function(response) {
            App.done(this.button_submit);
            App.page.dialog.show('info', response.body.message, '', [{
                type:'ok',
                label:'Ok',
                callback:function(){ App.page.change(response.body.redirect); }
            }]);
        },

        submit_failed : function(response) {
            App.done(this.button_submit);
            App.page.on_error(response);
        }
       	
    };
    
    this.initialize();
    
};


// AUGMENTED PROTOTYPES.


Dashboard.prototype.create_page = function() {
    return new DisputePage;
};


