

"use strict";

/**
 *  Base class for customer pages.
 */
function UserPage() {

    /**
     * Get this user's role
     * 
     * @returns {String|undefined}
     */
    this.user_role = function () {
        return App.user_role_to_string(App.ROLE_CUSTOMER);
    };

    /**
     * Check if this user has the specified role.
     * 
     * @param {String} role
     * @returns {Boolean}
     */
    this.user_is = function (role) {
        return role === this.user_role();
    }

    this.is_customer = function () {
        return true;
    };
    this.is_provider = function () {
        return false;
    };

    this.task = {

        /**
         * Handlers for customer task commands. These are default/base handlers that
         * may be overridden by specific page managers.
         * 
         * @param data Object containing the server response and context data, as follows:
         * { response: <server response>, ctx : <command context data> }
         * @param ctx Object containing command context data, such as post data, post function,
         * task info, etc. that may be used to pre-process the request. The minimal form is:
         * { task_ref: <subject task>, post: <post data>, do_post: <post function> }
         */
        command_handlers: {

            // This handler is called before the 'cancel' command is executed (posted).
            'before_execute_cancel': function (ctx) {
                var cancelled_task = ctx.task_ref;
                var task = App.cache.get(cancelled_task);
                // If the task is ongoing warn the customer about possible cancellation
                // penalties, otherwise just cancel the task straight away.
                if (task && task.status == App.TASK_ONGOING) {
                    var msg = 'You may be charged a cancellation fee up to the amount of the duty '+
                              '(depending on the assistant\'s cancellation policy). '+
                              'Consider sending a Cancellation Request to the other party to '+
                              'cancel the task in mutual agreement and avoid any cancellation fees.';
                    App.page.dialog.show('warning', msg, 'Warning', [{
                        type    :'ok',
                        label   :'Proceed',
                        callback:ctx.do_post
                    },{
                        type    :'cancel',
                        label   :'Abort'
                    }]);
                } else {
                    return true;
                }
                return false;
            },

            // This handler is called when a task has been successfully cancelled.
            // We call the page on_task_update() listener to update the page views.
            'on_cancel_success': function (data) {
                var cancelled_task = data.ctx.task_ref;
                App.page.loaded.on_task_update(cancelled_task, App.TASK_CANCELLED_EVENT);
                App.page.on_success('The duty has been cancelled.');
            },

            // This handler is called when a task cancellation request fails.
            'on_cancel_error': function (data) {
                App.page.on_error('The duty cannot be cancelled.');
            },

            // This handler is called before the 'rate' command is executed (posted).
            'before_execute_rate': function (ctx) {
                return true;
            },

            // This handler is called when a task has been successfully rated.
            'on_rate_success': function (data) {
                App.page.on_success('Your rating has been received. Thanks for giving your feedback.');
            },

            // This handler is called when a task rating request fails.
            'on_rate_error': function (data) {
                App.page.on_error('There was a problem with your request.');
            },

            // This handler is called before the 'reschedule' command is executed (posted).
            'before_execute_reschedule': function (ctx) {
                return true;
            },

            // This handler is called when a task has been successfully rescheduled.
            'on_reschedule_success': function (data) {
                App.page.on_success('The duty has been rescheduled.');
                App.page.change(App.URL_CUSTOMER_HOME);
            },

            // This handler is called when a task reschedule request fails.
            'on_reschedule_error': function (data) {
                App.page.on_error(data.response);
            },

            // This handler is called before the 'dispute' command is executed.
            'before_execute_dispute': function (ctx) {
                return true;
            },

            // This handler is called when a task has been successfully reschedule.
            'on_dispute_success': function (data) {
                App.page.on_success('Your request has been received and will be processed by our team shortly.');
            },

            // This handler is called when a task reschedule request fails.
            'on_dispute_error': function (data) {
                App.page.on_error('The dispute request failed.');
            }

        },


        // Available task commands for customers.
        // See widget/task.js => TaskListView.set_commands
        set_commands: function () {
            if (!this.page.open_task) return;
            this.commands.empty();
            var command = null;
            switch (this.page.open_task.status) {
                case App.TASK_SCHEDULED:
                case App.TASK_ONGOING:
                    command = $(App.page.factory.make_task_command_button('Cancel', 'cancel'));
                    break;
                case App.TASK_ENDED:
                    command = $(App.page.factory.make_task_command_button('Dispute', 'dispute', 'get'));
                    break;
                case App.TASK_CANCELLED:
                    command = $(App.page.factory.make_task_command_button('Resubmit', 'reschedule'));
                    break;
                case App.TASK_COMPLETED:
                    command = $(App.page.factory.make_task_command_button('Rate', 'rate', 'get'));
                    break;
            }
            if (command) {
                command.data('task-ref', this.page.open_task.ref);
                command.click(this.on_btn_command_click);
                this.commands.append(command);
            }
        },


        // Available task actions for customers.
        // See widget/task.js => TaskListView.set_available_actions
        set_available_actions: function () {
            if (!this.page.open_task) return;
            switch (this.page.open_task.status) {
                case App.TASK_SCHEDULED:
                    this.commands.show();
                    if (this.page.task_chat_view)
                        this.page.task_chat_view.hide();
                    break;
                case App.TASK_ONGOING:
                    this.commands.show();
                    if (this.page.task_chat_view) {
                        this.page.task_chat_view.show();
                        this.page.task_chat_view.message_box.show();
                    }
                    break;
                case App.TASK_CANCELLED:
                case App.TASK_ENDED:
                    this.commands.show();
                    if (this.page.task_chat_view) {
                        this.page.task_chat_view.show();
                        this.page.task_chat_view.message_box.hide();
                    }
                    break;
                case App.TASK_COMPLETED:
                    this.commands.show();
                    if (this.page.task_chat_view) {
                        this.page.task_chat_view.show();
                        this.page.task_chat_view.message_box.hide();
                    }
                    break;
                case App.TASK_DISPUTED:
                    this.commands.hide();
                    if (this.page.task_chat_view) {
                        this.page.task_chat_view.show();
                        this.page.task_chat_view.message_box.hide();
                    }
                    break;
            }
        }
    };


    this.message = {

        /**
         * Handlers for customer transactional message actions. These are default/base handlers
         * that may be overridden by specific page managers.
         * 
         * @param action Object containing data about the action, as follows:
         * { response: <server response>, ctx : <action context data> }
         */
        action_handlers: {

            // This handler is called when a charge request action is successfull.
            'on_charge_request_success': function (action) {
                if (action.ctx.user_action == 'approve') {
                    App.page.loaded.on_task_update(action.ctx.task_ref,
                        App.TASK_CHARGE_ADDED_EVENT,
                        action.response.body);
                    App.page.on_success('The charge has been approved.');
                } else {
                    App.page.on_success('The charge has been refused.');
                }
            },

            // This handler is called when a charge request action fails.
            'on_charge_request_error': function (action) {
                App.page.on_error(action.response);
            },

            // This handler is called when a task cancel request action is successfull.
            'on_task_cancel_request_success': function (action) {
                if (action.ctx.user_action == 'accept') {
                    App.page.loaded.on_task_update(action.ctx.task_ref,
                        App.TASK_CANCELLED_EVENT,
                        action.response.body);
                    App.page.on_success('The duty has been cancelled.');
                } else {
                    App.page.on_success('The cancel request has been refused.');
                }
            },

            // This handler is called when a task cancel request action fails.
            'on_task_cancel_request_error': function (action) {
                App.page.on_error(action.response);
            },
            
            // This handler is called when a task end request action is successfull.
            'on_task_end_request_success': function (action) {
                if (action.ctx.user_action == 'accept') {
                    App.page.loaded.on_task_update(action.ctx.task_ref,
                                                   App.TASK_ENDED_EVENT);
                    App.page.on_success('The duty has been ended.');
                } else {
                    App.page.change(App.URL_TASK_COMMAND
                                       .replace('{task_ref}', action.ctx.task_ref)
                                       .replace('{command}', 'dispute'));
                }
            },

            // This handler is called when a task end request action fails.
            'on_task_end_request_error': function (action) {
                App.page.on_error(action.response);
            }

        }
    };

};

