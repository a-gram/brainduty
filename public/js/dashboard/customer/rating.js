"use strict";

/**
 * This namespace handles a task rating process.
 */

function RatingPage() {
	
	UserPage.call(this);
	
	this.request_form = { files: [], formData: [] };
	
	
    this.initialize = function() {
    		    
    	// Setup views.
    	this.wizard_view.setup(this);    	
    };

    
    this.wizard_view = {

       	page               : null,
       	view               : $('#process-wizard'),
       	validate           : [0],
       	steps              : [   0, // Steps are 1-based.
       	                         $('#wizard-step-1')
       	                     ],
       	form               : $('#task-rating-form'),
       	score              : $('#task-rating-form #rating-score ul > li > a'),
       	score_selected     : $('#task-rating-form #rating-score .rating-score-selected'),
       	score_input        : $('#task-rating-form #rating-score input'),
        button_submit      : $('#task-rating-form button.submit'),
       	
       	
       	setup : function(page) {
       		
       		this.page = page;
        	this.set_step_validators ();
        	this.score.click(this.on_rating_score_selected.bind(this));
        	this.view.find('button.submit').click(this.on_btn_submit_click.bind(this));
        	this.view.find('#rating-message').val(''); // workaround for textareas not empty
        	App.done(this.button_submit);
       	},
       	
       	
       	set_step_validators : function () {
       		
       		var _this = this;
       		
       		this.validate.push(function(){
       			
   			    _this.score_input.parsley().validate();
   			    
                if(!_this.score_input.parsley().isValid())
                   return false;
       			
       			if(!_this.form.parsley().validate({group:'step-1'}))
       			   return false;

   	            return true;
       		});

       	},
       	
       	get_rating_score : function() {
       		return this.score_input.val();
       	},
       	
       	get_rating_message : function() {
    		return this.view.find('#rating-message').val();
       	},
       	
       	on_rating_score_selected : function(event) {
       		var score = $(event.currentTarget).data('score');
       		var rating = $(event.currentTarget).text();
       		this.score_selected.text(rating);
       		this.score_input.val(score);
       	},
       	       	
       	on_btn_submit_click : function(event) {
       		
       		var _this = this;
       		
            if(!_this.validate[ _this.steps.length-1 ]()) {
               return;
            }
            
            var postData = {
            	rating : {
            	    score        : _this.get_rating_score(),
            	    description  : _this.get_rating_message()
            	}
            };
            
		    var postUrl = _this.form.attr('action');
                    
                    App.busy(_this.button_submit);

			$.ajax({
				   url: postUrl,
				   type: "POST",
				   dataType: "json",
				   data: postData,
				   
				   success: function (response) {
                                       App.done(_this.button_submit);
				       App.page.on_success(response);
				       App.page.change(response.body.redirect);
				   },
				   error: function (response) {
					  App.done(_this.button_submit);
					  App.page.on_error(response);
				   }
			});
       	}
       	
    };
	
    
    this.initialize();
    
};


// AUGMENTED PROTOTYPES.


Dashboard.prototype.create_page = function() {
	return new RatingPage;
};


