"use strict";

/**
 * This namespace contains facilities that are used by the customer's account
 * Settings page.
 */
function SettingsPage() {

   UserPage.call(this);

   this.initialize = function () {

      // Setup views.
      this.profile_settings_view.setup(this);
      this.security_settings_view.setup(this);
      this.payment_settings_view.setup(this);
      
      this.show_tab_on_startup();
   };


   /////////////////////////////////////////////////////////////////////////////
   ///                          EVENT HANDLERS                               ///
   /////////////////////////////////////////////////////////////////////////////


   /////////////////////////////////////////////////////////////////////////////


   this.do_update = function (info, url, on_success, on_error, button) {

      $.ajax({
         url: url || App.URL_CUSTOMER_UPDATE,
         type: "PUT",
         dataType: "json",
         data: info,

         success: function (response) {
            if(button)
               App.done(button);
            if(on_success)
               on_success(response.body);
            App.page.on_success(response);
         },
         error: function (response) {
            if(button)
               App.done(button);
            if(on_error)
               on_error();
            App.page.on_error(response);
         }
      });
   };



   /* ***********************************************************************
    *                         Profile settings view
    * ***********************************************************************/


   this.profile_settings_view = {

      page: null,
      view: $('#profile-settings'),
      form: $('#profile-settings form').parsley(),
      button_save : $('#profile-settings button.save'),
      
      avatar                   : $('#profile-settings .avatar'),
      avatar_images            : $('#profile-settings .avatar .images'),
      avatar_image             : '#profile-settings form input[name="files[]"]',
      avatar_btn_change        : $('#profile-settings .avatar .btn.change'),
      avatar_btn_remove        : $('#profile-settings .avatar .btn.remove'),
      
            
      setup: function (page) {
         this.page = page;
         this.view.find('button.save').click(this.on_btn_save_click.bind(this));
         
         // Avatar
         $(this.avatar_image).fileupload({
            type: 'POST',
            url: App.URL_CUSTOMER_UPDATE,
            dataType: 'json',
            autoUpload: false,
            singleFileUploads: false,
            acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
            maxFileSize: 500000,
            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            previewMaxWidth: 125,
            previewMaxHeight: 125,
            previewCrop: true
         }).on('fileuploadadd', this.on_avatar_added_fu.bind(this))
           .on('fileuploadprocessalways', this.on_avatar_process_fu.bind(this))
           .on('fileuploaddone', this.on_avatar_upload_success_fu.bind(this))
           .on('fileuploadfail', this.on_avatar_upload_fail_fu.bind(this))
           .prop('disabled', !$.support.fileInput)
           .parent().addClass($.support.fileInput ? undefined : 'disabled');
   
         this.avatar_btn_remove.click(this.on_btn_avatar_remove_click.bind(this));

      },
      
      // ------------- Fileupload handlers -------------

      on_avatar_added_fu: function (e, data) {
          this.avatar.data('upload',data);
      },

      on_avatar_process_fu: function (e, data) {
          var file = data.files[data.index];
          if(file.preview) {
             this.avatar_images.find('.thumbnail').hide();
             this.avatar_images.append($(file.preview).addClass('img thumbnail preview'));
             this.avatar_btn_change.hide();
             this.avatar_btn_remove.show();
          }
          if(file.error) {
             App.page.on_error(file.error);
          }
      },
      
      on_avatar_upload_success_fu:function(e,data) {
          App.done(this.button_save);
          App.page.on_success(data);
          if(this.avatar.data('upload')) {
             App.page.change(App.URL_CUSTOMER_SETTINGS);
          }
      },

      on_avatar_upload_fail_fu:function(e,data) {
          App.done(this.button_save);
          App.page.on_error(data._response.jqXHR)
      },

      // -----------------------------------------------
      
      on_btn_avatar_remove_click: function() {
          this.avatar_images.find('.preview').remove();
          this.avatar_images.find('.thumbnail').show();
          this.avatar_btn_change.show();
          this.avatar_btn_remove.hide();
          this.avatar.data('upload',null);
      },
      
      
      on_btn_save_click: function (event) {
          
         if (!this.validate())
            return;
         var user = this.get_profile_info();
         var profile = {
            customer: {
               //first_name : user.first_name,
               //last_name  : user.last_name,
               mobile: user.mobile
            }
         };
         
         App.busy(this.button_save);
         
         var upload = this.avatar.data('upload');
         
         // If there are files to upload use the multipart form, else do a 
         // standard ajax form post.
         if(upload) {
            upload.formData = [
                { name:'_method', value:'PUT' },
                { name:'customer[mobile]', value:profile.customer.mobile },
            ];
            upload.submit();
         }
         else {
            this.page.do_update(profile, null, null, null, this.button_save);
         }
      },


      validate: function () {
         if (!this.form.validate({
               group: 'profile-info'
            }))
            return false;
         return true;
      },


      get_profile_info: function () {
         var info = {
            username: this.view.find('#user-name').val(),
            first_name: this.view.find('#first-name').val(),
            last_name: this.view.find('#last-name').val(),
            mobile: this.view.find('#mobile-number').val()
         };
         return info;
      }

   };


   /* ***********************************************************************
    *                         Security settings view
    * ***********************************************************************/


   this.security_settings_view = {

      page: null,
      view: $('#security-settings'),
      form: $('#security-settings form').parsley(),
      button_save : $('#security-settings button.save'),

      setup: function (page) {
         this.page = page;
         this.view.find('button.save').click(this.on_btn_save_click.bind(this));
      },


      on_btn_save_click: function () {
         if (!this.validate())
            return;
         var security = {
            customer: this.get_security_info()
         }
         App.busy(this.button_save);
         this.page.do_update(security, null, null, null, this.button_save);
      },


      validate: function () {
         return this.form.validate() ? true : false;
      },


      get_security_info: function () {
         return {
            password: this.view.find('#password').val(),
            old_password: this.view.find('#old-password').val()
         };
      },

   };


   /* ***********************************************************************
    *                        Payment settings view
    * ***********************************************************************/


   this.payment_settings_view = {

      page                       : null,
      view                       : $('#payment-settings'),
      form                       : $('#payment-settings form').parsley(),
      customer_has_payment       : false,
      btn_submit_payment_details : $('#payment-settings .payment-details button.submit'),
      btn_save_billing_details   : $('#payment-settings .billing-details button.save'),


      setup: function (page) {
         this.page = page;
         this.customer_has_payment = this.has_default_payment();
         if (this.customer_has_payment) {
            this.default_payment_view_visible(true);
            this.payment_details_view_visible(false);
         } else {
            this.default_payment_view_visible(false);
            this.payment_details_view_visible(true);
         }
         App.done(this.btn_save_billing_details);
         this.view.find('.billing-details button.save')
            .click(this.on_btn_save_billing_info_click.bind(this));
         this.view.find('.default-payment button.change')
            .click(this.on_btn_change_payment_info_click.bind(this));
         this.view.find('.payment-details button.submit')
            .click(this.on_btn_submit_payment_info_click.bind(this));
      },


      get_billing_info: function () {
          
         var billing = this.view.find('.billing-details');
         return {
            address: billing.find('#address').val(),
            city: billing.find('#city').val(),
            state: billing.find('#state').val(),
            zip_code: billing.find('#zip-code').val(),
            country: billing.find('#country').val()
         };
      },


      get_card_info: function () {
          
         var card = this.view.find('.payment-details');
         return {
            number: card.find('#card-number').val(),
            expMonth: card.find('#card-expiry-month').val(),
            expYear: card.find('#card-expiry-year').val(),
            cvc: card.find('#card-cvv').val(),
         };
      },


      get_selected_payment_method: function () {
         // Currently only cards supported.
         return 'card';
      },


      on_btn_save_billing_info_click: function () {

         if (!this.validate_billing_info())
            return;
         var info = {
            customer: this.get_billing_info()
         }
         App.busy(this.btn_save_billing_details);
         this.page.do_update(info,null,null,null,this.btn_save_billing_details);
      },


      on_btn_submit_payment_info_click: function () {
          
         var _this = this;

         var selected_payment_method = this.get_selected_payment_method();

         if (selected_payment_method == 'card') {
            
            if (!this.validate_billing_info())
                return;

            if (!this.validate_card_info() ||
                !this.validate_billing_info())
                return;
            
            App.busy(this.btn_submit_payment_details);
            
            var card = this.get_card_info();

            var profile = this.page.profile_settings_view.get_profile_info();

            // Card holder info required by some payment processors.
            var customer = this.get_billing_info();
            //customer.first_name = profile.first_name;
            //customer.last_name  = profile.last_name;

            card.holderName = profile.first_name + ' ' + profile.last_name;
            card.holderAddress = customer.address;
            card.holderCity = customer.city;
            card.holderCountry = customer.country;
            card.holderZip = customer.zip_code;

            if (!App.payment)
                 App.payment = new PaymentProcessor();

            // Get the tokenized card or an error.
            App.payment.tokenizeCard(card, (function (error, tcard) {
                
                if (error) {
                   App.done(_this.btn_submit_payment_details);
                   App.page.on_error('Couldn\'t process your card: ' + error);
                } else {
                   // We got a card token. Now gather all customer and payment
                   // info and start the registration.
                   var customer_payment = {
                      customer: customer,
                      payment: {
                         method: 'card',
                         source_id: tcard.token,
                         source_number: '*******'+tcard.details.last4,
                         source_type: tcard.details.brand,
                         source_name: tcard.details.name,
                         source_expiry: tcard.details.exp_month + '/' + tcard.details.exp_year
                      }
                   };
                   // Register or update the customer's payment method.
                   // Currently, only replacing a method is supported.
                   if (!this.customer_has_payment)
                      this.register_payment_method(customer_payment);
                   else
                      this.update_payment_method(customer_payment, 'replace');
                }
            })
            .bind(this));
         }
      },


      register_payment_method: function (data) {
         var _this = this;
         $.ajax({
            url: App.URL_CUSTOMER_PAYMENT_REGISTRATION,
            type: "POST",
            dataType: "json",
            data: data,
            success: function (response) {
               ag_log('Payment registration ok:', response);
               _this.customer_has_payment = true;
               _this.on_payment_method_submit_success(response.body);
            },
            error: function (response) {
               _this.on_payment_method_submit_error(response);
            }
         });
      },


      update_payment_method: function (data, action) {
         var _this = this;
         delete data.customer;
         $.ajax({
            url: App.URL_CUSTOMER_PAYMENT_UPDATE
               .replace('{action}', action),
            type: "PUT",
            dataType: "json",
            data: data,
            success: function (response) {
               ag_log('Payment update ok:', response);
               _this.on_payment_method_submit_success(response.body);
            },
            error: function (response) {
               _this.on_payment_method_submit_error(response);
            }
         });
      },


      // Called whenever payment method submits are successfull to
      // update the page views.
      on_payment_method_submit_success: function (payment) {
         this.set_customer_default_payment_view(payment);
         this.default_payment_view_visible(true);
         this.payment_details_view_visible(false);
         App.done(this.btn_submit_payment_details);
         App.env.default_payment_method = payment.method;
         App.page.on_success('The payment method has been updated.');
      },


      on_payment_method_submit_error: function (response) {
         App.done(this.btn_submit_payment_details);
         App.page.on_error(response);
      },


      on_btn_change_payment_info_click: function () {
         var payment_details_view = this.view.find('.payment-details');
         if (App.env.default_payment_method == 'card') {
            var card = payment_details_view;
            card.find('#card-holder').val(
               this.view.find('.default-payment #holder').text()
            );
            card.find('#card-number').val('');
            card.find('#card-expiry-month').val('');
            card.find('#card-expiry-year').val('');
            card.find('#card-cvv').val('');
            card.slideToggle();
         }
      },


      validate_billing_info: function () {
         return this.form.validate({
            group: 'billing-details'
         }) ? true : false;
      },


      validate_card_info: function () {
         return this.form.validate({
            group: 'card-details'
         }) ? true : false;
      },

      has_default_payment: function () {
         return App.env.default_payment_method != null;
      },

      set_customer_default_payment_view: function (payment) {
         var payment_view = this.view.find('.default-payment');
         payment_view.find('#method').text(payment.method);
         payment_view.find('#type').text(payment.source_type);
         payment_view.find('#number').text(payment.source_number);
         payment_view.find('#expiry').text(payment.source_expiry || 'N/A');
      },

      default_payment_view_visible: function (visible) {
         if (visible) this.view.find('.default-payment').show();
         else this.view.find('.default-payment').hide();
      },

      payment_details_view_visible: function (visible) {
         if (visible) this.view.find('.payment-details').show();
         else this.view.find('.payment-details').hide();
      }

   };

    
   /**
    *  Show a specified tab after the page has loaded.
    */
   this.show_tab_on_startup = function() {
       var url_params = App.utils.get_url_params();
       if(url_params.has('view')) {
          $('#settings-tabs a[href="#'+url_params['view']+'"]').tab('show');
       }
   };
    

   this.initialize();

};


// AUGMENTED PROTOTYPES.


Dashboard.prototype.create_page = function () {
   return new SettingsPage;
};
