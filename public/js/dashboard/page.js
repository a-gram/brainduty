'use strict';

/**
 * This namespace contains functions that are used in the dashboard section of
 * the application and that are provided to all of its sub pages. Specific
 * functionality are provided by its sub pages.
 */
function Dashboard() {

    /**
     * The currently loaded (sub) page.
     */
    this.loaded = null;

    /**
     * Event fired whenever a chat message is received. Page managers must register
     * handlers with this event if they need to respond to chat message events.
     * @param message
     * @return {boolean} True if the message was handled, false otherwise.
     */
    this.event_chat_message = null;

    /**
     * Event fired whenever a task is updated. Page managers must register
     * handlers with this event if they need to respond to task update events.
     * @param subject
     * @param event
     * @param data
     */
    this.event_task_update = null;

    /**
     * Event fired whenever an account is updated. Page managers must register
     * handlers with this event if they need to respond to account update events.
     * @param subject
     * @param event
     * @param data
     */
    this.event_account_update = null;

    /**
     * Event fired whenever a message notification is clicked. Page managers
     * must register handlers with this event in order to respond with the
     * appropriate actions.
     * @param task_ref
     */
    this.event_message_notification_click = null;

    /**
     * Event fired whenever a update notification is clicked. Page managers
     * must register handlers with this event in order to respond with the
     * appropriate actions.
     * @param update
     */
    this.event_update_notification_click = null;


    /**
     * The message notifications view.
     */
    this.message_notifications_view = new NotificationsView(this, 'messages');
    
    /**
     * The update notifications view.
     */
    this.update_notifications_view = new NotificationsView(this, 'updates');


    //////////////////////////// EVENT HANDLERS ////////////////////////////

    /**
     *  This method handles displaying backend or third party services success
     *  responses.
     *  
     *  @param response A server response object or string message.
     */
    this.on_success = function (response) {
        var message = App.get_api_message(response);
        this.show_alert('success', message);
    };

    /**
     *  This method handles displaying backend or third party services error
     *  responses.
     *  
     *  @param response A server response object or string message.
     */
    this.on_error = function (response) {
        var message = App.get_api_error(response).error_description;
        this.show_alert('error', message);
    };

    /**
     *  This method handles displaying backend or third party services info
     *  messages.
     *  
     *  @param response A server response object or string message.
     */
    this.on_info = function (response) {
        var message = App.get_api_error(response).error_description;
        this.show_alert('info', message);
    };


    /**
     * Handler called whenever chat messages are received from the backend.
     * Must be registered/attached to the app's com object.
     * 
     * @param messages Array
     */
    this.on_chat_messages = function (messages) {

        var _this = this;

        $.each(messages, function (i, message) {

            ag_assert(message.task != null || new Error);

            // Check if the task for this message has been loaded.
            var task = App.cache.get(message.task);

            if (task != null) {
                // Add the new message to the loaded task
                ag_assert(!task.messages.has_msg(message) || new Error('Duplicate message' + message.ref + ' found in loaded task ' + task.ref));
                task.messages.push(message);

                // Dispatch the message to the loaded page manager for handling.
                // If there is no listener or the message is not handled then
                // push it to the message notifications view.
                if (_this.event_chat_message && _this.event_chat_message(message)) {
                    // The message was handled
                } else {
                    _this.message_notifications_view.push([message]);
                }
            } else {
                _this.message_notifications_view.push([message]);
            }
        });
    };


    /**
     * Handler called whenever unseen messages are received from the backend.
     * Must be registered/attached to the app's com object.
     * 
     * @param messages Array
     */
    this.on_unseen_chat_messages = function (messages) {
        this.message_notifications_view.push(messages);
    };


    /**
     * Handler called whenever latest messages are received from the backend.
     * Must be registered/attached to the app's com object.
     * 
     * @param messages Array
     */
    this.on_latest_chat_messages = function (messages) {
        this.message_notifications_view.push(messages, false);
    };


    /**
     * Handler called whenever notifications (updates) are received from the backend.
     * Must be registered/attached to the app's com object.
     * Here the updates are filtered based on the type of event and passed on to the
     * registered handler of the currently loaded page before being pushed to the view.
     * 
     * @param updates Array
     */
    this.on_notifications = function (updates) {
        var _this = this;
        $.each(updates, function (i, update) {
            if (App.is_task_event(update)) {
                if (_this.event_task_update)
                    _this.event_task_update(update.subject_id, update.event, update.data);
            }
            if (App.is_account_event(update)) {
                if (_this.event_account_update)
                    _this.event_account_update(update.subject_id, update.event, update.data);
            }
        });
        _this.update_notifications_view.push(updates);
    };


    /**
     * Handler called whenever unseen updates are received from the backend.
     * Must be registered/attached to the app's com object.
     * 
     * @param updates Array
     */
    this.on_unseen_notifications = function (updates) {
        this.update_notifications_view.push(updates);
    };


    /**
     * Handler called whenever latest updates are received from the backend.
     * Must be registered/attached to the app's com object.
     * 
     * @param updates Array
     */
    this.on_latest_notifications = function (updates) {
        this.update_notifications_view.push(updates, false);
    };


    /**
     * Reimplemented method (see notifications.js->NotificationsView).
     */
    this.on_message_notification_click = function () {

        var _this = App.page;
        
        var msg_ref = $(this).data('msg-ref'),
            task_ref = $(this).data('task-ref');
    
        App.page.fetch_task (task_ref, function (task) {
            if (_this.event_message_notification_click &&
                _this.event_message_notification_click(task_ref, msg_ref)) {
                // the event was handled
            } else {
                _this.go_to_task(task, msg_ref);
            }
        });
    };


    /**
     * Reimplemented method (see notifications.js->NotificationsView).
     */
    this.on_update_notification_click = function () {

        var _this = App.page;
        var update = {
            type: $(this).data('type'),
            event: $(this).data('event'),
            subject: $(this).data('subject'),
            url: $(this).data('url')
        }
        
        if (App.is_task_event(update)) {
            App.page.fetch_task (update.subject, function (task) {
                if (_this.event_update_notification_click &&
                    _this.event_update_notification_click(update)) {
                    // the event was handled
                } else {
                    _this.go_to_task(task);
                }
            });
        }
        if (App.is_account_event(update)) {
            if (_this.event_update_notification_click &&
                _this.event_update_notification_click(update)) {
            } else {
                // Unhandled?
                if(update.url)
                   App.page.change(update.url);
            }
        }
    };


    ////////////////////////////////////////////////////////////////////////


    /**
     * This method is called in response to a user click on a notification
     * to take the user to a view where she can check the task event (message, 
     * update, etc.).
     * 
     * @param {type} task
     * @param {type} msg_ref [optional]
     */
    this.go_to_task = function(task, msg_ref) {
        
        msg_ref = msg_ref || null;
        var to_view;
        // If the task is open we need to pass the message to the page
        // that handles the open tasks so that the user can interact with it, 
        // otherwise pass the message to the page that handles the closed tasks 
        // so the user can review it.
        if(App.is_task_open(task)) {
           to_view = this.loaded.is_provider() ? App.URL_PROVIDER_HOME :
                    (this.loaded.is_customer() ? App.URL_CUSTOMER_HOME : null);
        } else {
           to_view = this.loaded.is_provider() ? App.URL_PROVIDER_CLOSED_TASKS :
                    (this.loaded.is_customer() ? App.URL_CUSTOMER_CLOSED_TASKS : null);
        }
        to_view += '?task='+task.ref+(msg_ref ? '&msg='+msg_ref : '');

        this.change(to_view);
    };
    

    /**
     * Fetches the specified task either from the server or from the cache
     * and sets it as the current task.
     * 
     * @param {task_ref} The reference of the task to be fetched,
     * @param {on_fetch} A callback to be invoked if the task is fetched successfully.
     * @param {on_error} (optional) A callback to be invoked if the task fetch fails.
     * @param {url} (optional) The URL at which to get the task.
     * @param {cached} (optional) Whether to cache the fetched task or not.
     */
    this.fetch_task = function (task_ref, on_fetch, on_error, url, cached) {

        on_error = on_error === undefined ? null : cached;
        url      = url === undefined      ? null : url;
        cached   = cached === undefined   ? true : cached;

        ag_assert(task_ref != null || new Error);

        // If there is no callback on success and the task is not cached
        // the fetched data will be lost, so signal this.
        ag_assert(cached || on_fetch || new Error);

        // Check if the task is in the cache before making a network call.
        var task = App.cache.get(task_ref);

        if (!task) {

            var getUrl = url || App.URL_TASK_GET.replace('{task_ref}', task_ref);

            $.ajax({
                type: "GET",
                url: getUrl,
                dataType: "json",

                success: function (response) {
                    task = response.body;
                    ag_assert(task.ref === task_ref || new Error);
                    ag_assert((task.messages && task.messages.length) || new Error);
                    if (cached)
                        App.cache.set(task.ref, task);
                    if (on_fetch)
                        on_fetch(task);
                },
                error: function (response) {
                    if (on_error)
                        on_error();
                    App.page.on_error(response);
                }
            });
        } else {
            on_fetch(task);
        }
    };


    /**
     *  Post task commands and actions.
     *  
     *  @param url The URL of the endpoint to post to.
     *  @param data The data to be posted.
     *  @param on_success Success response handler.
     *  @param on_error Error response handler.
     *  @param ctx Contextual data to be passed to the response handlers.
     */
    this.post_command = function (url, data, on_success, on_error, ctx) {

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: "json",

            success: function (response) {
                if(ctx && ctx.button)
                   App.done(ctx.button);
                if (on_success)
                    on_success({
                        response: response,
                        ctx: ctx
                    });
                else
                    App.page.on_success(response);
            },
            error: function (response) {
                if(ctx && ctx.button)
                   App.done(ctx.button);
                if (on_error)
                    on_error({
                        response: response,
                        ctx: ctx
                    });
                else
                    App.page.on_error(response);
            }
        });
    };
    
    
    /**
     * Show app alerts (messages from the server/backend).
     * 
     * @param {String} type
     * @param {String} message
     * @returns {undefined}
     */
    this.show_alert = function(type, message) {
        
        var alert = this.factory.make_alert(type, message);
        // The alert is currently shown when created by the Messenger library.
        alert.show();
        return alert;
    };
    
    
    /**
     * This method should be called to handle critical situations, such as
     * unrecoverable errors, and abort the running of the page.
     * @param {String} message
     * @returns {undefined}
     */
    this.abort = function(message) {
        
        // TODO Do a POST to our servers with details about the error instead.
        var apology = 'If you are seeing this message then a critical error occurred\
                       (maybe a bug). Please contact our <a href="mailto:'+App.SUPPORT_EMAIL+
                       '">support team</a> and report the following message so that\
                       we can fix the issue:<br><br><i>'+message+'</i>';
        
        this.dialog.show('error', apology, 'Critical Error');
    }


    /* ***********************************************************************
     *                           Message Box
     * ***********************************************************************/

    // TODO Move this to the App namespace ?
    this.dialog = {

        box: $('#ag-modal'),
        icon: $('#ag-modal .modal-header > i'),
        title: $('#ag-modal .modal-title'),
        body: $('#ag-modal .modal-body'),
        btn_ok: $('#ag-modal .btn.ok'),
        btn_cancel: $('#ag-modal .btn.cancel'),

        titles: {
            error: 'Something went wrong',
            info: 'Success',
            warning: 'Warning'
        },

        icons: {
            error: 'fa-times-circle', // fa-frown-o
            info: 'fa-info-circle', // fa-smile-o
            warning: 'fa-exclamation-circle' // fa-meh-o
        },

        setup: function () {
            // On box closed, clean up any attached event handlers
            // to the OK/CANCEL buttons. Event handlers attached to
            // the on_close (hidden) event are always removed using
            // .one() since the box must always be closed to continue.
            this.box.on('hidden.bs.modal', function () {
                this.btn_ok.off('click');
                this.btn_cancel.off('click');
            }.bind(this));
        },

        on_close: function (handler) {
            this.box.one('hidden.bs.modal', handler);
        },


        /**
         * Display the dialog.
         * 
         * @param type The dialog type (error,info,warning).
         * @param message The message to be displayed.
         * @param title The dialog title.
         * @param buttons Array of buttons to display in the dialog. Each button
         * is an object in the following format:
         *    {
         *       type  : 'ok' | 'cancel',
         *       label : 'Label of the button',
         *       callback : Closure to execute if the button is pressed
         *    }
         */
        show: function (type, message, title, buttons) {
            var _this = this;
            ag_assert(type && message || new Error);
            type = type.toLowerCase();
            title = title ? title : this.titles[type];
            buttons = buttons || [];
            
            this.btn_ok.hide();
            this.btn_cancel.hide();
            
            $.each(buttons, function(i,button) {
                var has = button.type == 'ok' ? _this.btn_ok : 
                         (button.type == 'cancel' ? _this.btn_cancel : 
                          null);
                if(has) {
                   has.text(button.label);
                   if(button.callback !== 'undefined')
                      has.one('click', button.callback)
                   has.show();
                }
            });
            
            this.box.removeClass('error')
                .removeClass('info')
                .removeClass('warning');
            this.box.addClass(type);
            this.icon.attr('class', 'fa ' + this.icons[type]);
            this.title.text(title);
            this.body.empty().html('<p>' + message + '<p>');
            this.box.modal('show');
        }
    };


    /*
     *  Page components factory. Takes care of building dynamically created components,
     *  such as messages, notifications, command buttons, list items, etc.
     */
    this.factory = {
        
        // TODO Hardcoding is mean. Use templating.
        
        templates: {},

        // Notification type => icon map.
        notification_update_icon: {},
        // Transaction status => icon map.
        txn_status_icon: {},


        initialize: function () {

            this.notification_update_icon[App.NOTIFICATION_PLATFORM_EVENT] = 'fa-bullhorn';
            this.notification_update_icon[App.NOTIFICATION_TASK_EVENT] = 'fa-calendar';
            this.notification_update_icon[App.NOTIFICATION_TRANSACTION_EVENT] = 'fa-money';
            this.notification_update_icon[App.NOTIFICATION_ACCOUNT_EVENT] = 'fa-home';
            this.notification_update_icon[App.NOTIFICATION_ALERT_EVENT] = 'fa-exclamation-triangle';

            this.txn_status_icon[App.TXN_PENDING] = 'fa-minus-circle';
            this.txn_status_icon[App.TXN_COMPLETED] = 'fa-check-circle';
            this.txn_status_icon[App.TXN_CANCELLED] = 'fa-times-circle';

        },


        make_task_list_item: function (task) {

            var task_status = App.task_status_to_string(task.status);
            var item = '<a id="' + task.ref + '" href="javascript:;" class="list-group-item" data-task-ref="' + task.ref + '">' +
                '<span class="label label-task-status label-task-' + task_status + ' pull-right">' + task_status + '</span>' +
                '<h4 class="list-group-item-heading">' + task.subject + '</h4>' +
                '<small class="list-group-item-details"><i class="fa fa-clock-o"></i>&nbsp;' + task.schedule_datetime + '</small>' +
                '</a>';
            return item;
        },


        make_txn_list_item: function (txn) {

            var txn_status = App.get_txn_status(txn);
            var txn_amount_bal = txn.amount_bal.to_currency();
            var txn_amount_type = txn_status!==App.TXN_CANCELLED ? 
                                 (txn.amount_bal >= 0 ? 'credit' : 'debit') :
                                  'cancelled';
            var icon = App.page.factory.txn_status_icon[ txn_status ];

            var item =
                '<div class="item-container" id="' + txn.ref + '">' +
                '<a class="list-group-item" href="javascript:;" data-txn-ref="' + txn.ref + '" >' +
                '<h4 class="item-amount ' + txn_amount_type + ' pull-right">' + txn_amount_bal + '</h4>' +
                '<h4 class="list-group-item-heading">' + txn.description + '</h4>' +
                '<i class="fa ' + icon + ' status ' + txn_status + '"></i>' +
                '<small class="item-datetime"><i class="fa fa-clock-o"></i>&nbsp;' + txn.created_at + '</small>' +
                '</a>' +
                '<div class="item-details collapse" aria-expanded="false">' +
                '<dl class="dl-horizontal">' +
                '<dt>Reference</dt><dd class="txn-ref"></dd>' +
                '<dt>Duty</dt><dd class="txn-task"><a href="javascript:;"></a></dd>' +
                '<dt>Date/time</dt><dd class="txn-created-at"></dd>' +
                '<dt>Status</dt><dd class="txn-status"><span class="label label-txn-status"></span></dd>' +
                '<dt>&nbsp;</dt><dd>&nbsp;</dd>' +
                '<dt>Type</dt><dd class="txn-type"></dd>' +
                '<dt>Paid by</dt><dd class="txn-settle"><span class="method"></span><span class="status"></span></dd>' +
                '<dt>Description</dt><dd class="txn-description"></dd>' +
                '<dt>Amount</dt><dd class="txn-gross"></dd>' +
                '<dt>Fees</dt><dd class="txn-fee"></dd>' +
                '<dt>Tax</dt><dd class="txn-tax"></dd>' +
                '<dt>Amount Net</dt><dd class="txn-net"></dd>' +
                '</dl>' +
                '</div>' +
                '<div class="item-loading">Loading...</div>' +
                '</div>';
            return item;
        },


        make_attachment: function (attachment, task_ref, list) {

            if (!list) {
                var fileURL = App.env.URL_BASE + '/task/' + task_ref + '/file/' + attachment.id;
                var item = '<li><i class="icon-li fa fa-paperclip"></i>' +
                    '<a href="' + fileURL + '">' + attachment.filename_orig + '</a>' +
                    '</li>';
                return item;
            } else {
                var items_list = '<ul class="icons-list">' + attachment + '</ul>';
                return items_list;
            }
        },


        make_attachment_nolink: function (attachment, list) {

            if (!list) {
                var item = '<li><i class="icon-li fa fa-file-o"></i>' + attachment + '</li>';
                return item;
            } else {
                var items_list = '<ul class="icons-list-2">' + attachment + '</ul>';
                return items_list;
            }
        },


        make_attachment_id_doc: function (attachment, list) {

            if (!list) {
                var e = this.templates['attachment_id_doc'].clone();
                var icon = attachment.mime ? (/^image/.test(attachment.mime) ?
                                             'fa-picture-o' : 'fa-file-text') :
                                             'fa-file-text';
                e.find('.doc-icon > i').addClass(icon);
                e.find('.doc-filename').text(attachment.filename);
                return e;
            } else {}
        },


        make_chat_message: function (message, list) {

            // Make chat message list
            if (list) {
                $('.task-message-list').empty();
                $('.task-message-list').append('<ul></ul>');
                return $('.task-message-list > ul');
            }

            // Make chat message

            var msg_position = '',
                msg_author   = '',
                msg_type     = ' type-' + message.type,
                msg_attachs  = '',
                msg_actions  = '',
                msg_avatar   = App.URL_USER_DEFAULT_AVATAR,
                task         = App.cache.get(message.task);

            // By convention, if this user has a role in the task then its messages
            // are positioned on the right side, whereas the other party's messages
            // are positioned on the left hand side. If the user is not a party in
            // the task (external observer) then customers messages go on the right
            // hand side and providers' messages on the left hand side. Systems
            // messages are always centered.
            
            if(message.sender === App.user_role_to_string(App.ROLE_SYSTEM)) {
               msg_position = 'centre';
            }
            else if(App.page.loaded.user_role() in task) {
               msg_position = App.page.loaded.user_is(message.sender) ? 'right' : 'left';
            }
            else {
               msg_position = message.sender === App.user_role_to_string(App.ROLE_CUSTOMER) ?
                          'right' : 'left';
            }
            
            // Messages authors are either one of the task's parties or the system.
            
            if(message.sender === App.user_role_to_string(App.ROLE_SYSTEM)) {
               msg_author = 'System message';
            }
            else {
               msg_author = App.page.loaded.user_is(message.sender) ? 'Me' : 
                            App.utils.shortname( task[message.sender] );
               msg_avatar = task[message.sender].avatar;
            }
                    
            // Format the message (restore line breaks, linkfy URLs, etc.).
            var msg_body = App.utils.format_message(message.body);
            

            // If it's a transactional message, add action buttons if any
            if (App.is_transactional(message) && message.action_names) {
                var actions = message.action_names.split("|");
                $.each(actions, function (i, action) {
                    msg_actions +=
                        '<button class="btn btn-success btn-action" href="javascript:;" data-action="' +
                        action.toLowerCase() + '">' + action +
                        '<i class="fa fa-spinner fa-pulse loading"></i>' +
                        '</button>';
                });
            }

            // Include message attachments, if any.
            if (message.attachments && message.attachments.length) {
                $.each(message.attachments, function (i, attachment) {
                    msg_attachs += App.page.factory.make_attachment(attachment, message.task);
                });
                msg_attachs = '<ul>' + msg_attachs + '</ul>';
            }

            var item =
                '<li>' +
                '<div id="' + message.ref + '" class="message ' + msg_position + msg_type + '">' +
                '<div class="message-avatar">' +
                '<img alt="" src="'+msg_avatar+'" class="avatar">' +
                '</div>' +
                '<div class="message-meta">' +
                '<span class="message-author">' + msg_author + '</span>' +
                '<small class="message-timestamp">' + message.created_at + '</small>' +
                '</div>' +
                '<div class="message-body">' +
                '<p>' + msg_body + '</p>' +
                (msg_attachs ?
                    '<div class="message-attachments">' + msg_attachs + '</div>' : '') +
                (msg_actions ?
                    '<div class="message-actions">' + msg_actions + '</div>' : '') +
                '</div>' +
                '</div>' +
                '</li>';
            return item;
        },


        make_notification_message: function (message) {

            var message_body_preview = App.utils.truncate_text(message.body);
            var classes = (!message.is_delivered || !message.is_seen) ? 'new' : '';
            var item =
                '<a class="notification ' + classes + '" href="javascript:;" '+
                   'data-task-ref="' + message.task + '" data-msg-ref="'+message.ref+'">' +
                '<div class="notification-icon"><i class="fa fa-comments"></i></div>' +
                '<div class="notification-title">Message for duty ' + /*message.task +*/ '</div>' +
                '<div class="notification-description">' + message_body_preview + '</div>' +
                '<div class="notification-time">' + message.created_at + '</div>' +
                '</a>';
            return item;
        },


        make_notification_update: function (update) {

            var classes = !update.is_delivered ? 'new' : '';
            var icon = App.page.factory.notification_update_icon[update.type];
            var item =
                '<a class="notification ' + classes + '" href="javascript:;"'+
                ' data-type="' + (update.type || '') + '"' +
                ' data-event="' + (update.event || '') + '"' +
                ' data-subject="' + (update.subject_id || '') + '"' +
                ' data-url="' + (update.url || '') + '">' +
                '<span class="notification-icon"><i class="fa ' + icon + '"></i></span>' +
                '<span class="notification-title"></span>' +
                '<span class="notification-description">' + update.message + '</span>' +
                '<span class="notification-time">' + update.created_at + '</span>' +
                '</a>';
            return item;
        },


        make_task_service_table: function (services) {

            var items = '';
            $.each(services, function (i, service) {
                items +=
                    '<tr><td class="description">' + service.description + '</td>' +
                    '<td class="amount">' + parseInt(service.amount).to_currency() + '</td>' +
                    '</tr>';
            });
            return items;
        },


        /**
         * @param label The button's label
         * @param command The command associated with the button. This will be
         * included into task command base URL.
         * @param type The type of command. Can be 'asynch' for asynchronous calls
         * or anything else for synchronous calls (i.e. resulting in pages change).
         * @param classes Optional CSS classes to append to the button.
         */
        make_task_command_button: function (label, command, type, classes) {

            type = type ? type : 'asynch';
            classes = classes ? classes : '';

            var item = '<button type="button"' +
                ' class="btn btn-primary ' + classes + '"' +
                ' data-command="' + command + '"' +
                ' data-command-type="' + type + '">' + label +
                '<i class="fa fa-spinner fa-pulse loading"></i>' +
                '</button>';
            return item;
        },
        
        
        /**
         * 
         * @param {String} note
         * @returns {String}
         */
        make_task_note: function (note) {
            
            if(!note) return '';
            return '<p>'+note+'</p>';
        },
        
        
        /**
         * 
         * @param {Object} dispute
         * @returns {String}
         */
        make_task_dispute: function (dispute) {
            return '<p>'+
                     'This duty has been disputed:<br>'+
                     '<b>Reason:</b> '+dispute.reason+'<br>'+
                     '<b>Report:</b> "'+dispute.details+'"<br>'+
                     '<b>Results:</b> '+(dispute.results || '')+
                   '</p>';
        },
        
        
        /**
         * Make a representation of a user rating (currently a 5-star system).
         * 
         * @param {Number} rating
         * @returns {String}
         */
        make_rating : function(rating) {
            
            if(rating<0.5) rating = 0;
            if(rating>5.0) rating = 5;
            var star = '<i class="fa fa-star"></i>';
            var pstar = '<i class="fa fa-star-half-o"></i>';
            var estar = '<i class="fa fa-star-o"></i>';
            var full_stars = Math.floor(rating);
            var part_stars = rating - full_stars >= 0.5 ? 1 : 0;
            var empty_stars = 5 - full_stars - part_stars;
            
            if(full_stars || part_stars) {
               return (new Array(full_stars + 1)).join(star) +
                      (part_stars ? pstar : '') +
                      (new Array(empty_stars + 1)).join(estar);
            } else {
               return '';
            }
        },
        
        
        
        make_identity_attachment : function(attachments, file_url, list) {

            if (!list) {
                var items = '';
                $.each(attachments, function (i, attachment) {
                    var url = file_url.replace('{doc_id}', attachment.id);
                    items += '<tr>'+
                               '<td><i class="icon-li fa fa-paperclip"></td>'+
                               (attachment.is_hosted ?
                               '<td><a href="'+ url + '">' + attachment.filename_orig + '</a></td>' :
                               '<td>' + attachment.filename_orig + '</td>') +
                               '<td>'+attachment.type+'</td>'+
                             '</tr>';
                });
                return items;
            } else {
                var items_list = '<table class="table table-condensed styled">'+
                                 '<caption>Provided documents</caption>'+
                                 '<tbody>' + attachments + '</tbody></table>';
                return items_list;
            }
        },


        make_payout_table : function(payouts) {

                var items = '';
                $.each(payouts, function(i,payout){
           items += '<tr>'+
                  '<td>'+payout.account_name+'</td>'+
                  '<td>'+payout.account_number+'</td>'+
                  (payout.is_primary?'<td><i class="fa fa-check"></i></td>':'')+
                '</tr>';
                });
                return items;
        },


        /**
         * @param commands Array A list of commands to be made. Each command is an
         * object with the following properties:
         * 
         *   - label: The button's label
         *   - command: The command associated with the button. This will be
         *     included in the command post URL.
         *   - type: The type of command. Can be 'asynch' for asynchronous calls
         *     or anything else for synchronous calls (i.e. resulting in pages change).
         *   - classes: Optional CSS classes to append to the button.
         *   - data: Optional data to be attached to the command. This is an array of
         *     objects in the following format:
         *
         *     {
         *        name: "name of the data field",
         *        value: <value of the data field>
         *     }
         */
        make_user_command_button : function(commands) {

            var items = '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action<span class="caret"></span></button>';
                items += '<ul class="dropdown-menu" role="menu">';

            $.each(commands, function(i,command){
                    command.type = command.type || 'asynch';
                    command.classes = command.classes || '';
                    var command_data = '';
                    if(command.data) {
                       $.each(command.data, function(i,data){
                              command_data += ' data-'+data.name+'="'+data.value+'"';
                       });
                    }
                    items += '<li>'+
                               '<a href="javascript:;" '+
                                 ' class="'+command.classes+'"'+
                                 ' data-command="'+command.command+'"'+
                                 ' data-command-type="'+command.type+'"'+
                                 command_data+'>'+
                                 command.label+
                               '</a></li>';
            });
            items += '</ul>';
            return items;
        },
        
        
        /**
         * Make an alert to be shown on the main page.
         * Currently using the Messenger library (https://github.com/HubSpot/messenger)
         * 
         * @param {String} type
         * @param {String} message
         * @returns {undefined}
         */
        make_alert : function(type, message) {
            
            var alert = Messenger().post({
                message : message,
                type    : type,
                showCloseButton: true
            });
            return alert;
        }

    };


    this.change = function (url) {
        window.location.replace(url);
    };


    this.initialize();

};


// ----------------------------  PROTOTYPES -------------------------------


Dashboard.prototype.initialize = function () {

    // Layouts
    App.core.navEnhancedInit();
    App.core.navHoverInit({
        delay: {
            show: 250,
            hide: 350
        }
    });
    App.core.initNavbarNotifications();
    App.core.initLayoutToggles();
    App.core.initBackToTop();

    // Components
    App.components.initAccordions();
    App.components.initFormValidation();
    App.components.initTooltips();
    App.components.initLightbox();
    App.components.initSelect();
    App.components.initIcheck();
    App.components.initDataTableHelper();
    App.components.initiTimePicker();
    App.components.initDatePicker();
    App.components.initPlaceholder();
    App.components.initAlerts();

    App.create_cache();

    // Comms
    App.com = new AGComm;
    App.com.event_chat_messages_received = this.on_chat_messages.bind(this);
    App.com.event_chat_unseen_messages_received = this.on_unseen_chat_messages.bind(this);
    App.com.event_chat_latest_messages_received = this.on_latest_chat_messages.bind(this);
    App.com.event_notifications_received = this.on_notifications.bind(this);
    App.com.event_unseen_notifications_received = this.on_unseen_notifications.bind(this);
    App.com.event_latest_notifications_received = this.on_latest_notifications.bind(this);

    // Views setup and event/listener bindings
    this.message_notifications_view.on_notification_click = this.on_message_notification_click;
    this.message_notifications_view.item_factory = this.factory.make_notification_message;
    this.update_notifications_view.on_notification_click = this.on_update_notification_click;
    this.update_notifications_view.item_factory = this.factory.make_notification_update;

    this.factory.initialize();

    this.dialog.setup();

};


/**
 * Start the dashboard by creating and running the (sub) page manager.
 */
Dashboard.prototype.start = function () {

    // Start the com with the server.
    if(!App.env.no_com)
        App.com.open();

    // Check if the dashboard page factory method has been attached and call it,
    // otherwise just create the base user page manager.
    if ('create_page' in this) {
        this.loaded = Dashboard.prototype.create_page();
    } else {
        this.loaded = new UserPage;
    }
};
