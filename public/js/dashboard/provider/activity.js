"use strict";

/**
 * This namespace contains facilities that are used by the provider's account
 * Activity page.
 */
function ActivityPage() {

    UserPage.call(this);

    this.open_task = null;
	
	
    this.initialize = function() {
    	
    	// Setup views.
    	this.balance_view.setup(this);
    	this.txn_list_view.setup(this);
    	this.task_details_view.setup(this);
    };
    
    
    /* ***********************************************************************
     *                            Balance view
     * ***********************************************************************/
    

    this.balance_view = {

       	page              : null,
       	view              : $('.txn-view .balance'),
       	//btn_withdraw_pane : $('.txn-view .balance .withdraw-pane'),
       	//btn_withdraw      : $('.txn-view .balance form button.withdraw'),
       	form              : $('.txn-view .balance form'),
       	balance           : {
       		                   available : $('.txn-view .balance .available'),
       		                   pending   : $('.txn-view .balance .pending')
       	                    },

       	
       	setup : function(page) {
            var _this = this;
            this.page = page;
//            this.btn_withdraw.click(this.on_btn_withdraw_click.bind(this));
//            this.btn_withdraw_pane.click(this.on_btn_withdraw_pane_click.bind(this));
//            this.btn_withdraw_pane.popover({
//                    placement :'bottom',
//                    html      : true,
//                    content   : function(){
//                            return _this.form.removeClass('hidden').detach();
//                    }
//            });
       	},
       	
       	/*
       	on_btn_withdraw_pane_click : function(){
            this.btn_withdraw_pane.popover('toggle');
       	},
       	
       	
       	on_btn_withdraw_click : function(event) {
            var _this = this;
            if(!_this.form.parsley().validate())
                return;
            App.busy(_this.btn_withdraw);
            var post_data = {
                provider : {
                        password : _this.form.find('#password').val()
                }	
            };
            $.ajax({
                type: "POST",
                url: App.URL_PROVIDER_WITHDRAW,
                dataType: "json",
                data : post_data,

                success: function (response) {
                    _this.btn_withdraw_pane.popover('hide');
                    App.done(_this.btn_withdraw);
                    App.page.change(App.URL_PROVIDER_ACTIVITY);
                },
                error: function (response) {
                    _this.btn_withdraw_pane.popover('hide');
                    App.done(_this.btn_withdraw);
                    App.page.on_error(response);
                }
            });
       	},
       	*/
       	
       	set_balance : function(balance) {
            this.balance.available.text(balance.available.to_currency());
            this.balance.pending.text(balance.pending.to_currency());
       	}
        
    };

    
    /* ***********************************************************************
     *                        Transactions list view
     * ***********************************************************************/
    

    this.txn_list_view = {

       	page    : null,
       	view    :  $('.txn-view'),
       	list    :  $('.txn-view .list-group.transactions'),
       	no_list :  $('.txn-view .panel-empty.transactions'),
        button_load_more : $('.txn-view .btn.load-more'),

       	
       	setup : function(page) {
            this.page = page;
            this.list.find('.list-group-item').click(this.on_item_click.bind(this));
            this.button_load_more.click(this.on_btn_load_more_click.bind(this));
            this.hide_if_empty();
       	},
       	
       	
       	on_item_click : function(event) {
       		
            var _this = this;
            var txn_ref = $(event.currentTarget).data('txn-ref');
            var txn_item = $(event.currentTarget).parent();

            // Check whether this txn has already been fetched from the server.
            if(txn_item.hasClass('loaded')) {
                _this.show_details(txn_ref);
            }
            else {
                _this.loading(txn_ref, true);
                $.ajax({
                    type: "GET",
                    url: App.URL_TXN_GET.replace('{txn_ref}', txn_ref),
                    dataType: "json",

                    success: function (response) {
                        var txn = response.body;
                        _this.fill(txn);
                    },
                    error: function (response) {
                        _this.loading(txn_ref, false);
                        App.page.on_error(response);
                    }
                });
            }		    
       	},
       	
       	
       	on_task_open_click : function(event) {
            var task_ref = $(event.target).prop('id');
            if(task_ref)
           this.page.task_details_view.open(task_ref);
       	},
       	
        
        on_btn_load_more_click : function(e) {

            var _this = this;
            var curr_page = _this.button_load_more.data('page');
            var load_page = curr_page + 1;
            var query = '?page='+(load_page)+'&where=closed';
            App.busy(_this.button_load_more);
            $.ajax({
                type: "GET",
                url: App.URL_TXNS_GET+query,
                data: {},
                dataType: "json",

                success: function (response) {
                   App.done(_this.button_load_more);
                   $.each(response.body.transactions, function(i,txn){
                       var item = $( App.page.factory.make_txn_list_item(txn) );
                           item.find('.list-group-item').click(_this.on_item_click.bind(_this));
                       _this.list.append(item);
                   });
                   $(e.target).data('page', load_page)
                   if(load_page >= response.body.pages) {
                      _this.button_load_more.remove();
                   }
                },
                error: function (response) {
                   App.page.on_error(response);
                }
            });
        },

       	
       	fill : function(txn) {
       		
            this.loading(txn.ref, false);

            var item = this.list.find('#'+txn.ref);
            var item_details = item.find('.item-details');

            item.addClass('loaded');

            item_details.find('.txn-ref').text(txn.ref);
            item_details.find('.txn-created-at').text(txn.created_at);
            item_details.find('.txn-type').text(txn.type);
            item_details.find('.txn-settle .method').text(txn.settle_method);
            item_details.find('.txn-settle .status').text(txn.settle_status)
                                                    .addClass(txn.settle_status);
            item_details.find('.txn-description').text(txn.description);
            item_details.find('.txn-ref').text(txn.ref);
            item_details.find('.txn-gross').text(txn.amount_gross.to_currency());
            item_details.find('.txn-fee').text(txn.amount_fee.to_currency());
            item_details.find('.txn-tax').text(txn.amount_tax.to_currency());

            var task_link = item_details.find('.txn-task > a');
                task_link.text(txn.task ? 'View' : 'N/A');
                task_link.prop('id', txn.task || '');
                task_link.click(this.on_task_open_click.bind(this));

            this.set_item_details_status(item_details, txn.status);

            var net = App.net_amount_of(txn).to_currency();

            item_details.find('.txn-net').text(net);

            this.show_details(txn.ref);
       	},
       	
       	
       	loading : function(item_id, is_loading) {
            if(is_loading)
               this.list.find('#'+item_id+' > .item-loading').show();
            else
               this.list.find('#'+item_id+' > .item-loading').hide();
       	},
       	
       	
       	show_details : function(txn_ref) {
            var details = this.list.find('#'+txn_ref+' > .item-details');
            details.collapse('toggle');
       	},
       	
       	add : function(item) {
       	    return this.list.prepend(item);
       	},
       	
       	remove : function(item) {
            this.list.find('#'+item).remove();
            this.hide_if_empty();
       	},
       	
       	item : function(id) {
            return this.list.find('#'+id);
       	},
       	
       	exists : function(item) {
            return this.list.find('#'+item).length > 0;
       	},
       	
       	set_item_status : function(item, status) {
            var item = this.list.find('#'+item);
            status = App.txn_status_to_string(status);
            var classes = App.page.factory.txn_status_icon + ' status ' + status;
            var label = item.find('i.status');
                label.prop('className', classes);
       	},
       	
       	set_item_details_status : function(item_details, status) {
            status = App.txn_status_to_string(status);
            var classes = 'label label-txn-status label-txn-'+status;
            var label = item_details.find('.txn-status .label');
            label.prop('className', classes);
            label.text(status);
       	},

       	
       	hide_if_empty : function() {
            if(this.list.children().length === 0) {
               this.list.parent().hide();
               this.no_list.show();
            }
       	}
       	
    };

    this.task_details_view = new TaskDetailsView;
    
    this.initialize();
    
};


// AUGMENTED PROTOTYPES.


Dashboard.prototype.create_page = function() {
    return new ActivityPage;
};


