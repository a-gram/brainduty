"use strict";

/**
 * This namespace contains facilities that are used by the provider's account
 * Closed Tasks page.
 */
function ClosedTasksPage() {
	
	UserPage.call(this);
	
	this.open_task         = null;
    this.task_list_view    = new TaskListView;
    this.task_details_view = new TaskDetailsView;
	
	
    this.initialize = function() {
    	
    	// Setup views.
    	this.task_list_view.setup(this);
    	this.task_details_view.setup(this);
        
   	App.page.event_message_notification_click = this.on_message_notification_click.bind(this);
    	App.page.event_update_notification_click = this.on_update_notification_click.bind(this);

    	this.task_details_view.set_commands = this.task.set_commands.bind(this.task_details_view);
        
        // Should we open a specific task after the page has loaded ?
        this.show_task_on_startup();
    };

    
    /////////////////////////////////////////////////////////////////////////////
    ///                          EVENT HANDLERS                               ///
    /////////////////////////////////////////////////////////////////////////////

    
    /**
     * This handler is called when the user clicks on a task message notification.
     * If the task is closed it is displayed, otherwise it is ignored since this page 
     * only manages closed tasks.
     * 
     * @param {type} task_ref
     * @param {type} msg_ref
     * @return {type} Return true if the event has been handled, otherwise return
     * false so the caller can perform a default handling.
     */
    this.on_message_notification_click = function(task_ref, msg_ref) {
        var task = App.cache.get(task_ref);
        if(App.is_task_closed(task)) {
    	   this.task_details_view.open(task_ref, msg_ref);
           return true;
        }
        return false;
    };
    
    
    /**
     * This handler is called when the user clicks on an update notification.
     * If the update is related to a task and the task is closed, then it is 
     * displayed, otherwise it is ignored since this page only manages closed tasks.
     * 
     * @param {type} update
     */
    this.on_update_notification_click = function(update) {
    	if(App.is_task_event(update)) {
           var task = App.cache.get(update.subject);
           if(App.is_task_closed(task)) {
              this.task_details_view.open(update.subject);
              return true;
           }
    	}
        return false;
    };


    /////////////////////////////////////////////////////////////////////////////
    
    
    /**
     *  Show a specified task after the page has loaded, optionally jumping
     *  to a specific message. The task (and message) to show are indicated
     *  in the url's query string as parameters 'task' and 'msg' respectively.
     */
    this.show_task_on_startup = function() {
        var url_params = App.utils.get_url_params();
        if(url_params.has('task')) {
           this.task_details_view.open(url_params['task'],
                                       url_params.has('msg') ?
                                       url_params['msg'] : null);
        }
    };

    
    this.initialize();
    
};


// AUGMENTED PROTOTYPES.


Dashboard.prototype.create_page = function() {
	return new ClosedTasksPage;
};


