"use strict";

/**
 * This namespace contains facilities that are used by the provider's account
 * Home page.
 */
function HomePage() {
	
	UserPage.call(this);
	
	this.open_task         = null;
    this.task_list_view    = new TaskListView;
    this.task_details_view = new TaskDetailsView;
    this.task_chat_view    = new AGChat;    

	
    this.initialize = function() {
    	
    	// Setup views.
    	this.task_details_view.setup(this);
    	this.task_list_view.setup(this);
    	this.task_chat_view.setup(this);
    	
    	// Setup listeners
        App.page.event_chat_message = this.on_task_chat_message.bind(this);
    	App.page.event_task_update = this.on_task_update.bind(this);
   	App.page.event_message_notification_click = this.on_message_notification_click.bind(this);
    	App.page.event_update_notification_click = this.on_update_notification_click.bind(this);

    	// Reimplement/override methods and objects for task views.
    	this.task_details_view.chat = this.task_chat_view;
    	
    	this.task_details_view.set_commands = this.task.set_commands.
                                              bind(this.task_details_view);

        this.task_details_view.set_available_actions = this.task.set_available_actions.
                                                       bind(this.task_details_view);
        
        // Should we open a specific task after the page has loaded ?
        this.show_task_on_startup();
    };

    
    /////////////////////////////////////////////////////////////////////////////
    ///                          EVENT HANDLERS                               ///
    /////////////////////////////////////////////////////////////////////////////

    
    /**
     * This handler is called whenever a task chat message is received.
     * 
     * @param {type} message
     * @returns {boolean}
     */
    this.on_task_chat_message = function(message) {
        
        // If the message belongs in the currently open chat, append it.
        if (this.open_task && this.open_task.ref === message.task) {
            this.task_chat_view.append(message);
            return true;
        }
        return false;
    };
    
    
    /**
     * This handler is called whenever a task update occurs. It takes care of
     * updating the task views in the page based on the new status and provider rules.
     * 
     * @param task_ref Reference to the task subject of the event.
     * @param event The event.
     * @param data (optional) Additional contextual data.
     */
    this.on_task_update = function(task_ref, event, data) {
    	
    	var _this = this;
    	
    	var task = App.cache.get(task_ref);
    	
		switch(event) {
		    /**
		     * Started tasks are available in the open tasks section of the home page and
		     * the available commands/actions associated with the new status must be reset.
		     */
		   case App.TASK_STARTED_EVENT:
			  var status = App.TASK_ONGOING;
              if(task) {
                 task.status = status;
 	             if(this.open_task && this.open_task.ref == task_ref) {
                    this.task_details_view.set_status(status);
 					this.task_details_view.set_commands();
 					this.task_details_view.set_available_actions();
 	             }
              } else {
            	 App.page.fetch_task(task_ref, function(){
            		 _this.on_task_update(task_ref,event);
            	 });
            	 return;
              }
              // For newly started tasks there should be no item in the open tasks list. Create it.
              if(!this.task_list_view.exists(task_ref)) {
                 var item = $( App.page.factory.make_task_list_item(task) );
                 item.click(this.task_list_view.on_item_click.bind(this.task_list_view));
			     this.task_list_view.add(item);
              } else {
            	 this.task_list_view.set_item_status(task_ref, status);
              }
			  break;
		    /**
		     * Closed tasks (completed, cancelled) are no longer available in the open tasks
		     * section of the home page, so we just remove it from the cache, close the task
		     * details view if it's currently open and remove it from the open tasks list view.
		     */
		   case App.TASK_COMPLETED_EVENT:
		   case App.TASK_CANCELLED_EVENT:
			  App.cache.remove(task_ref);
			  if(this.open_task && this.open_task.ref == task_ref) {
                              App.page.on_info('The duty "'+this.open_task.subject+'" has been closed.');
                 this.task_details_view.close();
                 this.task_details_view.clear();
			  }
              this.task_list_view.remove(task_ref);
			  break;
		    /**
		     * Disputed and ended tasks are still available in the open tasks section of
		     * the home page but the available commands/actions associated with the new status
		     * must be reset.
		     */
		   case App.TASK_ENDED_EVENT:
		   case App.TASK_DISPUTED_EVENT:
			  var status = 0;
                  status = event == App.TASK_ENDED_EVENT ? App.TASK_ENDED : status;
                  status = event == App.TASK_DISPUTED_EVENT ? App.TASK_DISPUTED : status;
              if(task) {
                 task.status = status;
	             if(this.open_task && this.open_task.ref == task_ref) {
	                this.task_details_view.set_status(status);
	                this.task_details_view.set_commands();
	                this.task_details_view.set_available_actions();
	             }
              }
              this.task_list_view.set_item_status(task_ref, status);
			  break;
		   /**
		    * Update task services.
		    */
		   case App.TASK_CHARGE_ADDED_EVENT:
			  if(task && data) {
				 var service = JSON.parse(data);
				 task.services.push(service);
                 if(this.open_task && this.open_task.ref == task_ref) {
			        this.task_details_view.set_services([service]);
			        this.task_details_view.set_price(task);
                 }
			  }
			  break;

		}
    };
    
    
    /**
     * This handler is called when the user clicks on a task message notification.
     * If the task is open it is displayed, otherwise it is ignored since this page 
     * only manages open tasks.
     * 
     * @param {type} task_ref
     * @param {type} msg_ref
     * @return {type} Return true if the event has been handled, otherwise return
     * false so the caller can perform a default handling.
     */
    this.on_message_notification_click = function(task_ref, msg_ref) {
        var task = App.cache.get(task_ref);
        if(App.is_task_open(task)) {
    	   this.task_details_view.open(task_ref, msg_ref);
           return true;
        }
        return false;
    };
    
    
    /**
     * This handler is called when the user clicks on an update notification.
     * If the update is related to a task and the task is open, then it is 
     * displayed, otherwise it is ignored since this page only manages open tasks.
     * 
     * @param {type} update
     */
    this.on_update_notification_click = function(update) {
    	if(App.is_task_event(update)) {
           var task = App.cache.get(update.subject);
           if(App.is_task_open(task)) {
              this.task_details_view.open(update.subject);
              return true;
           }
    	}
        return false;
    };

        
    /////////////////////////////////////////////////////////////////////////////

    
    /**
     *  Show a specified task after the page has loaded, optionally jumping
     *  to a specific message. The task (and message) to show are indicated
     *  in the url's query string as parameters 'task' and 'msg' respectively.
     */
    this.show_task_on_startup = function() {
        var url_params = App.utils.get_url_params();
        if(url_params.has('task')) {
           this.task_details_view.open(url_params['task'],
                                       url_params.has('msg') ?
                                       url_params['msg'] : null);
        }
    };
    
    
    this.initialize();
    
};


// AUGMENTED PROTOTYPES.


Dashboard.prototype.create_page = function() {
	return new HomePage;
};


