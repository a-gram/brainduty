"use strict";

/**
 *  Base class for provider pages.
 */
function UserPage() {

    /**
     * Get this user's role
     * 
     * @returns {String|undefined}
     */
    this.user_role = function() { 
        return App.user_role_to_string(App.ROLE_PROVIDER); 
    };

    /**
     * Check if this user has the specified role.
     * 
     * @param {String} role
     * @returns {Boolean}
     */
    this.user_is = function(role) { 
        return role === this.user_role(); 
    }

    this.is_customer = function() { return false; };
    this.is_provider = function() { return true; };

    this.task = {

        /**
         * Handlers for provider task commands/actions. These are default/base handlers that
         * may be overridden by specific page managers.
         * 
         * @param data Object containing the server response and context data, as follows:
         * { response: <server response>, ctx : <command context data> }
         * @param ctx Object containing command context data, such as post data, post function,
         * task info, etc. that may be used to pre-process the request. The minimal form is:
         * { task_ref: <subject task>, post: <post data>, do_post: <post function> }
         */
        command_handlers: {

            // This handler is called before the 'cancel' command is executed (posted).
            'before_execute_cancel': function (ctx) {
                var msg = 'Consider sending a Cancellation Request to the other party to ' +
                          'cancel the task in mutual agreement and avoid any cancellation fees.';
                App.page.dialog.show('warning', msg, 'Warning', [{
                    type    :'ok',
                    label   :'Proceed',
                    callback:ctx.do_post
                },{
                    type    :'cancel',
                    label   :'Abort'
                }]);
                return false;
            },

            // This handler is called when a task has been successfully cancelled.
            // We call this.on_task_update() listener to update this page views.
            'on_cancel_success': function (data) {
                var cancelled_task = data.ctx.task_ref;
                App.page.loaded.on_task_update(cancelled_task, App.TASK_CANCELLED_EVENT);
                App.page.on_success('The duty has been cancelled.');
            },

            // This handler is called when a task cancellation request fails.
            'on_cancel_error': function (data) {
                App.page.on_error('The duty cannot be cancelled.');
            },

            // This handler is called before the 'end' command is executed (posted).
            'before_execute_end': function (ctx) {
                var msg = 'Posting this message will send the customer a request\
                           to end the duty. Proceed?';
                App.page.dialog.show('warning', msg, 'Warning', [{
                    type    :'ok',
                    label   :'Proceed',
                    callback:ctx.do_post
                },{
                    type    :'cancel',
                    label   :'Abort'
                }]);
                return false;
            },

            // This handler is called when a task has been successfully ended.
            // We call this.on_task_update() listener to update this page views.
            'on_end_success': function (data) {
                var ended_task = data.ctx.task_ref;
                App.page.loaded.on_task_update(ended_task, App.TASK_ENDED_EVENT);
                App.page.on_success('The duty has ended.');
            },

            // This handler is called when a task end request fails.
            'on_end_error': function (data) {
                App.page.on_error('The duty end request failed.');
            },

            // This handler is called before the 'bid' command is executed (posted).
            'before_execute_bid': function (ctx) {
                return true;
            },

            // This handler is called when a task bid has been successfully submitted.
            'on_bid_success': function (data) {
                App.page.on_success(data.response);
            },

            // This handler is called when a task bid request fails.
            'on_bid_error': function (data) {
                App.page.on_error(data.response);
            }

        },


        // Available task commands for providers.
        // See widget/task.js => TaskListView.set_commands
        set_commands: function () {
            if (!this.page.open_task) return;
            this.commands.empty();
            var command = null;
            switch (this.page.open_task.status) {
                case App.TASK_SCHEDULED:
                    command = $(App.page.factory.make_task_command_button('Confirm', 'bid'));
                    break;
                case App.TASK_ONGOING:
                    command = $(App.page.factory.make_task_command_button('Cancel', 'cancel'));
                    break;
            }
            if (command) {
                command.data('task-ref', this.page.open_task.ref);
                command.click(this.on_btn_command_click);
                this.commands.append(command);
            }
        },


        // Available task actions for providers.
        // See widget/task.js => TaskListView.set_available_actions
        set_available_actions: function () {
            if (!this.page.open_task) return;
            switch (this.page.open_task.status) {
                case App.TASK_SCHEDULED:
                    this.commands.show();
                    if (this.page.task_chat_view)
                        this.page.task_chat_view.hide();
                    break;
                case App.TASK_ONGOING:
                    this.commands.show();
                    if (this.page.task_chat_view) {
                        this.page.task_chat_view.show();
                        this.page.task_chat_view.message_box.show();
                    }
                    break;
                case App.TASK_ENDED:
                case App.TASK_COMPLETED:
                case App.TASK_CANCELLED:
                case App.TASK_DISPUTED:
                    this.commands.hide();
                    if (this.page.task_chat_view) {
                        this.page.task_chat_view.show();
                        this.page.task_chat_view.message_box.hide();
                    }
                    break;
            }
        }
    };


    this.message = {

        /**
         * Handlers for provider transactional message actions. These are default/base handlers
         * that may be overridden by specific page managers.
         * 
         * @param action Object containing data about the action, as follows:
         * { response: <server response>, ctx : <action context data> }
         */
        action_handlers: {
            
            // This handler is called when a task cancel request action is successfull.
            'on_task_cancel_request_success' : function (action) {
                if(action.ctx.user_action == 'accept') {
                       App.page.loaded.on_task_update(action.ctx.task_ref,
                                          App.TASK_CANCELLED_EVENT,
                                          action.response.body);
                       App.page.on_success('The duty has been cancelled.');
                } else {
                       App.page.on_success('The cancel request has been refused.');
                }
            },

            // This handler is called when a task cancel request action fails.
            'on_task_cancel_request_error' : function (action) {
                App.page.on_error(action.response);
            }

        }
    };
    
};
