"use strict";

/**
 * This namespace contains facilities that are used by the provider's account
 * Settings page.
 */
function SettingsPage() {

   UserPage.call(this);

   this.initialize = function () {

      // Setup views.
      this.profile_settings_view.setup(this);
      this.security_settings_view.setup(this);
      this.payment_settings_view.setup(this);
      this.account_settings_view.setup(this);
      
      this.show_tab_on_startup();
   };


   /////////////////////////////////////////////////////////////////////////////
   ///                          EVENT HANDLERS                               ///
   /////////////////////////////////////////////////////////////////////////////


   /////////////////////////////////////////////////////////////////////////////


   this.do_update = function (info, url, on_success, on_error, button) {

      $.ajax({
         url: url || App.URL_PROVIDER_UPDATE,
         type: "PUT",
         dataType: "json",
         data: info,

         success: function (response) {
            if(button)
               App.done(button);
            if(on_success)
               on_success(response.body);
            App.page.on_success(response);
         },
         error: function (response) {
            if(button)
               App.done(button);
            if(on_error)
               on_error();
            App.page.on_error(response);
         }
      });
   };


   /* ***********************************************************************
    *                         Profile settings view
    * ***********************************************************************/


   this.profile_settings_view = {

      // The parent containing page.
      page                     : null,
      view                     : $('#profile-settings'),
      form                     : $('#profile-settings form').parsley(),
      input_service_categories : $('#profile-settings form input[name=service_categories]'),
      button_save              : $('#profile-settings button.save'),
      service_list             : new AGList,
      
      avatar                   : $('#profile-settings .avatar'),
      avatar_images            : $('#profile-settings .avatar .images'),
      avatar_image             : '#profile-settings form input[name="files[]"]',
      avatar_btn_change        : $('#profile-settings .avatar .btn.change'),
      avatar_btn_remove        : $('#profile-settings .avatar .btn.remove'),


      setup: function (page) {
         this.page = page;
         this.service_list.setup(this.page, 'services', 'multi');
         this.button_save.click(this.on_btn_save_click.bind(this));
         
         // Avatar
         $(this.avatar_image).fileupload({
            type: 'POST',
            url: App.URL_PROVIDER_UPDATE,
            dataType: 'json',
            autoUpload: false,
            singleFileUploads: false,
            acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
            maxFileSize: 500000,
            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            previewMaxWidth: 125,
            previewMaxHeight: 125,
            previewCrop: true
         }).on('fileuploadadd', this.on_avatar_added_fu.bind(this))
           .on('fileuploadprocessalways', this.on_avatar_process_fu.bind(this))
           .on('fileuploaddone', this.on_avatar_upload_success_fu.bind(this))
           .on('fileuploadfail', this.on_avatar_upload_fail_fu.bind(this))
           .prop('disabled', !$.support.fileInput)
           .parent().addClass($.support.fileInput ? undefined : 'disabled');
   
         this.avatar_btn_remove.click(this.on_btn_avatar_remove_click.bind(this));

         // Hack. See AGList
         this.view.find('.select2').addClass('no-bubble');
      },
      
      
      // ------------- Fileupload handlers -------------

      on_avatar_added_fu: function (e, data) {
          this.avatar.data('upload',data);
      },

      on_avatar_process_fu: function (e, data) {
          var file = data.files[data.index];
          if(file.preview) {
             this.avatar_images.find('.thumbnail').hide();
             this.avatar_images.append($(file.preview).addClass('img thumbnail preview'));
             this.avatar_btn_change.hide();
             this.avatar_btn_remove.show();
          }
          if(file.error) {
             App.page.on_error(file.error);
          }
      },
      
      on_avatar_upload_success_fu:function(e,data) {
          App.done(this.button_save);
          App.page.on_success(data)
          if(this.avatar.data('upload')) {
             App.page.change(App.URL_PROVIDER_SETTINGS);
          }
      },

      on_avatar_upload_fail_fu:function(e,data) {
          App.done(this.button_save);
          App.page.on_error(data._response.jqXHR)
      },

      // -----------------------------------------------
      
      on_btn_avatar_remove_click: function() {
          this.avatar_images.find('.preview').remove();
          this.avatar_images.find('.thumbnail').show();
          this.avatar_btn_change.show();
          this.avatar_btn_remove.hide();
          this.avatar.data('upload',null);
      },


      on_btn_save_click: function () {

         if (!this.validate())
            return;

         var profile = {
            provider: this.get_profile_info()
         };

         profile.provider.services = this.get_selected_services();
         
         App.busy(this.button_save);
         
         var upload = this.avatar.data('upload');
         
         // If there are files to upload use the multipart form, else do a 
         // standard ajax form post.
         if(upload) {
            upload.formData = [
                { name:'_method', value:'PUT' },
                //{ name:'provider[username]', value:profile.provider.username },
                //{ name:'provider[first_name]', value:profile.provider.first_name },
                //{ name:'provider[last_name]', value:profile.provider.last_name },
                //{ name:'provider[dob]', value:profile.provider.dob },
                { name:'provider[mobile]', value:profile.provider.mobile },
                { name:'provider[about]', value:profile.provider.about }
            ];
            $.each(profile.provider.services, function(i,service){
                upload.formData.push({name:'provider[services]['+i+']',value:service});
            });
            upload.submit();
         }
         else {
            this.page.do_update(profile, null, null, null, this.button_save);
         }
      },


      validate: function () {

         if (!this.form.validate({ group: 'profile-info' }))
            return false;

         var services = this.get_services_as_id_array(),
             service_categories = '';

         // Validate service selections and build service-categories JSON object.
         $.each(services, (function (i, service) {
               var categories = this.get_service_categories_as_id_string(service);
               if (service == App.SERVICE_EXPERT && !categories) {
                  service_categories = '';
                  return false;
               }
               service_categories += '"' + service + '":"' + (categories || '')
                                         + (i < services.length-1?'",':'"');
            })
            .bind(this));

         if (service_categories)
            service_categories = '{' + service_categories + '}';

         this.input_service_categories.val(service_categories);
         this.input_service_categories.parsley().validate();
         
         if (!this.input_service_categories.parsley().isValid())
             return false;

         return true;
      },


      // Get the selected services ids as an array
      get_services_as_id_array: function () {
         return this.service_list.get_selected_as('id_array');
      },

      // Get the selected services ids as a comma-separated list string
//      get_services_as_id_string: function () {
//         return this.service_list.get_selected_as('id_string');
//      },

      // Get the selected categories ids for the specified service as an array
//      get_service_categories_as_id_array: function (service) {
//         var field = this.view.find('#service-' + service + '-categories');
//         return field.length ? this.service_list.get_selected_as('id_array', field.select2('data')) : null;
//      },

      // Get the selected categories ids for the specified service as a list string
      get_service_categories_as_id_string: function (service) {
         var field = this.view.find('#service-' + service + '-categories');
         return field.length ? this.service_list.get_selected_as('id_string', field.select2('data')) : null;
      },

      get_selected_services: function () {
         return JSON.parse(this.input_service_categories.val());
      },

      get_profile_info: function () {
         var info = {
            //username    : this.view.find('#user-name').val(),
            //first_name  : this.view.find('#first-name').val(),
            //last_name   : this.view.find('#last-name').val(),
            //dob         : this.view.find('#dob').val(),
            mobile        : this.view.find('#mobile-number').val(),
            about         : this.view.find('#about').val()
         };
         if (this.is_business_entity()) {
            //info.business_name = this.view.find('#business-name').val();
            //info.business_number = this.view.find('#business-number').val();           
         }
         return info;
      },


      is_business_entity: function () {
         return this.view.find('#business-name').length > 0 &&
                this.view.find('#business-number').length > 0;
      },


      has_expert_service: function () {
         var services = this.get_services_as_id_array();
         return $.inArray(App.SERVICE_EXPERT, services) > -1;
      }

   };


   /* ***********************************************************************
    *                         Security settings view
    * ***********************************************************************/


   this.security_settings_view = {

      // The parent containing page.
      page         : null,
      view         : $('#security-settings'),
      form         : $('#security-settings form').parsley(),
      button_save  : $('#security-settings button.save'),


      setup: function (page) {
         this.page = page;
         this.view.find('button.save').click(this.on_btn_save_click.bind(this));
      },


      on_btn_save_click: function () {
         if (!this.validate())
            return;
         var security = {
            provider: this.get_security_info()
         };
         App.busy(this.button_save);
         this.page.do_update(security, null, null, null, this.button_save);
      },


      validate: function () {
         return this.form.validate() ? true : false;
      },


      get_security_info: function () {
         return {
            password: this.view.find('#password').val(),
            old_password: this.view.find('#old-password').val()
         };
      }

   };


   /* ***********************************************************************
    *                        Payment settings view
    * ***********************************************************************/


   this.payment_settings_view = {

      page           : null,
      view           : $('#payment-settings'),
      form           : $('#payment-settings form').parsley(),
      default_payout : {
                         view: $('#payment-settings .default-payout'),
                         method: $('#payment-settings .default-payout #method'),
                         host: $('#payment-settings .default-payout #host'),
                         holder: $('#payment-settings .default-payout #holder'),
                         id: $('#payment-settings .default-payout #id'),
                         btn_change: $('#payment-settings .default-payout button.change')
                       },
      bank_details  :  {
                         view: $('#payment-settings .bank-details'),
                         btn_submit: $('#payment-settings .bank-details button.submit')
                       },
      billing_details: {
                         view: $('#payment-settings .billing-details'),
                         btn_save: $('#payment-settings .billing-details button.save')
                       },


      setup: function (page) {

         this.page = page;
         
         //this.bank_details.view.hide();
            
         this.bank_details.btn_submit.click(this.on_btn_save_bank_info_click.bind(this));
         this.default_payout.btn_change.click(this.on_btn_change_bank_info_click.bind(this));
         this.billing_details.btn_save.click(this.on_btn_save_billing_info_click.bind(this));
         
         this.view.find('form')[0].reset();
      },
      
      
      on_btn_save_billing_info_click: function () {

         if (!this.validate_billing_info())
            return;
         var info = {
            provider: this.get_billing_info()
         }
         App.busy(this.billing_details.btn_save);
         this.page.do_update(info,null,null,null,this.billing_details.btn_save);
      },


      on_btn_save_bank_info_click: function () {
          
         var _this = this;
         var bank_info = this.get_bank_info();
             bank_info.billing = this.get_billing_info();
         
         if (!this.validate_bank_info()) {
            return;
         }

         if (!App.payment) {
              App.payment = new PaymentProcessor();
         }
         
         App.busy(this.bank_details.btn_submit);

         App.payment.tokenizeBankInfo(bank_info, function (error, bank) {
             
            if (!error) {
                 bank_info.number = bank.token;
                 _this.bank_details.view.find('#bank-number').val(
                       bank.details.routing_number + ' ' + 
                       Array(6).join(App.MASK_PLACEHOLDER) + 
                       bank.details.last4
                 );
                 var url = App.URL_PROVIDER_PAYOUT_UPDATE
                           .replace('{action}', 'replace');

                 _this.page.do_update({
                       payout_acc_holder: bank_info.holder,
                       payout_acc_id: bank_info.number,
                       payout_method: 'bank'
                    },
                    url,
                    function (payout) {
                       _this.set_default_payout(payout);
                       _this.bank_details.view.slideToggle();
                       App.done(_this.bank_details.btn_submit);
                    },
                    function () {
                       App.done(_this.bank_details.btn_submit);
                    }
                 );
            }
            else {
                App.done(_this.bank_details.btn_submit);
                App.page.on_error(error);
            }
         });
      },


      on_btn_change_bank_info_click: function () {
         this.bank_details.view.find('#bank-holder').val(
            this.default_payout.holder.text()
         );
         this.bank_details.view.find('#bank-number').val('');
         this.bank_details.view.slideToggle();
      },

      get_billing_info: function () {
         return {
            address:  this.billing_details.view.find('#address').val(),
            city:     this.billing_details.view.find('#city').val(),
            state:    this.billing_details.view.find('#state').val(),
            zip_code: this.billing_details.view.find('#zip-code').val(),
            country:  this.billing_details.view.find('#country').val()
         };
      },


      get_bank_info: function () {
         return {
            holder: this.bank_details.view.find('#bank-holder').val(),
            number: this.bank_details.view.find('#bank-number').val()
         };
      },
      
      validate_billing_info: function () {
         return this.form.validate({
            group: 'billing-details'
         }) ? true : false;
      },


      validate_bank_info: function () {
         return this.form.validate({
            group: 'bank-details'
         }) ? true : false;
      },


      set_default_payout: function (payout) {
         this.default_payout.method.text(payout.method);
         this.default_payout.host.text(payout.host);
         this.default_payout.holder.text(payout.holder);
         this.default_payout.id.text(payout.number);
      }

   };
   
   
   /* ***********************************************************************
    *                        Account settings view
    * ***********************************************************************/
   

    this.account_settings_view = {

        // The parent containing page.
        page         : null,
        view         : $('#account-settings'),
        form         : $('#account-settings form').parsley(),
        input_markup : $('#account-settings form input#markup'),
        generic_rate_label : $('#account-settings form b#generic-rate'),
        expert_rate_label : $('#account-settings form b#expert-rate'),
        input_cancel_policy : $('#account-settings .cancel-policy'),
        button_save : $('#account-settings button.save'),
        button_delete : $('#account-settings button.delete'),


        setup: function (page) {
           this.page = page;
           this.view.find('button.save').click(this.on_btn_save_click.bind(this));
           this.view.find('button.delete').click(this.on_btn_delete_click.bind(this));
           this.input_markup.on('input', this.on_markup_change.bind(this));
           this.activation.setup(page);
           this.on_markup_change();
        },


        on_btn_save_click: function () {
           if (!this.validate())
              return;
           var data = {
              settings: this.get_account_settings()
           };
           App.busy(this.button_save);
           this.page.do_update(data, App.URL_PROVIDER_SETTINGS, null, null, this.button_save);
        },

        on_btn_delete_click: function () {
          var _this = this;
          if(!window.confirm('Closing your account will delete all of your data '+
                             'and you will no longer have access to it.'))
              return;
          App.busy(this.button_delete);
          $.ajax({
             url: App.URL_USER_DELETE,
             type: "DELETE",
             dataType: "json",
             success: function (response) {
                  App.page.dialog.show('info', response.body.message, '', [{
                      type:'ok', label:'Ok',
                      callback:function(){ App.page.change(App.URL_BASE); }
                  }]);
             },
             error: function (response) {
                  App.done(_this.button_delete);
                  App.page.on_error(response);
             }
          });
        },

        on_markup_change : function(e) {
            var generic_rate = 1500 * (1 + this.input_markup.val() / 100);
            var expert_rate =  3000 * (1 + this.input_markup.val() / 100);
            this.generic_rate_label.text(generic_rate.to_currency());
            this.expert_rate_label.text(expert_rate.to_currency());
        },

        validate: function () {
            return this.form.validate() ? true : false;
        },

        get_cancel_policy: function() {
            return this.input_cancel_policy.find('input:checked').val() || null;
        },

        get_account_settings: function () {
           return {
              //markup           : this.input_markup.val(),
              task_cancel_policy : this.get_cancel_policy()
           };
        },
      
        // ---------------------------------------------------------------------

        activation : {

            page           : null,
            view           : $('.activation'),
            form           : $('#account-settings form').parsley(),
            bank_details  :  {
                               view: $('.activation .bank-details'),
                             },
            billing_details: {
                               view: $('.activation .billing-details'),
                             },
            input_documents: '.activation input[name=document]',
            activation_docs: {
                               view: $('.activation .documents'),
                               files: '.activation .documents input[name="files[]"]',
                               files_ext: '.activation .documents input[name="file"]',
                               list: $('.activation .documents table > tbody'),
                             },
            activation_btn_submit: $('.activation button.submit'),


            setup : function(page) {

                this.page = page;

                if (!this.view.length) {
                    return
                }

                this.activation_enabled(true);

                // Initialize the uploaders for the platform and the marketplace.
                $(this.activation_docs.files).fileupload({
                     url: App.URL_PROVIDER_IDENTITY,
                     dataType: 'json',
                     singleFileUploads: false,
                     //progressall: this.on_activation_send_progress_fu.bind(this)
                })
                .prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');

                $(this.activation_docs.files_ext).fileupload({
                     url: App.MARKETPLACE_URL_FILE_UPLOAD,
                     headers: { Authorization: 'Bearer '+App.env.market_pkey },
                     paramName: 'file',
                })
                .prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');

                // Bind the activation buttons
                this.activation_btn_submit.click(this.on_btn_submit_activation_click.bind(this));
                $(this.input_documents).change(this.on_document_added);
            },

            activation_enabled: function (enabled) {
               if(enabled) App.done(this.activation_btn_submit);
               else        App.busy(this.activation_btn_submit)
               this.view.find('.form-control:not(#country)').prop('disabled', !enabled);
            },

            on_document_added: function(e) {
               var doc = $(this).prop('files')[0];
               var doc_item = $(this).parents('.document');
               doc_item.find('.doc-filename').text(doc.name);
               doc_item.find('.doc-icon i').addClass('fa-paperclip');
            },

            /**
             * Start the submission of the identity verification info. The submission
             * process is composed of 3 parts:
             * 1) Tokenization of bank data and/or other sensitive info (optional)
             * 2) Submission of identity documents to the marketplace provider (optional)
             * 3) Submission of all identity info to the platform.
             * 
             * @returns {undefined}
             */
            on_btn_submit_activation_click: function () {

               var _this = this;

               var post_data = {};

               this.activation_enabled(false);

               if(this.billing_details.view.length) {

                  if(!this.validate_billing_info()) {
                      this.activation_enabled(true);
                      return;
                  }
                  post_data.billing_info = this.get_billing_info();
               }

               if(this.bank_details.view.length) {

                  if(!this.validate_bank_info()) {
                      this.activation_enabled(true);
                      return;
                  }

                  post_data.bank_info = this.get_bank_info();
                  
                  if(!post_data.billing_info)
                      throw new Error('Billing info required with bank info');
                  
                  var params = post_data.bank_info;
                      params.billing = post_data.billing_info;

                  if (!App.payment)
                       App.payment = new PaymentProcessor();

                  App.payment.tokenizeBankInfo(params, (function (error, bank) {

                        if (!error) {
                            post_data.bank_info.number = bank.token;
                            _this.bank_details.view.find('#bank-number').val(
                               bank.details.routing_number + ' ' + 
                               Array(6).join(App.MASK_PLACEHOLDER) + 
                               bank.details.last4
                            );
                            _this.submit_activation(post_data);
                        }
                        else {
                            _this.activation_enabled(true);
                            App.page.on_error('Bank account info error: ' + error);
                        }
                  })
                  .bind(this));
               }
               else {
                  this.submit_activation(post_data);
               }
            },

            /**
             * Start the submission of activation info to external services and to the
             * platform.
             * 
             * @param {object} data
             * @returns {undefined}
             */
            submit_activation: function (data) {

               var _this = this;

               // The form to be submitted to the platform.
               var form = { files: [], formData: [] };

               // Include all required info to the submit form, if any.

               if (data && data.bank_info) {
                  form.formData.push({
                     name: 'payout[account_name]',
                     value: data.bank_info.holder
                  });
                  form.formData.push({
                     name: 'payout[account_id]',
                     value: data.bank_info.number
                  });
                  form.formData.push({
                     name: 'payout[method]',
                     value: 'bank'
                  });
               }

               if (data && data.billing_info) {
                  form.formData.push({
                     name: 'billing[address]',
                     value: data.billing_info.address
                  });
                  form.formData.push({
                     name: 'billing[city]',
                     value: data.billing_info.city
                  });
                  form.formData.push({
                     name: 'billing[state]',
                     value: data.billing_info.state
                  });
                  form.formData.push({
                     name: 'billing[zip_code]',
                     value: data.billing_info.zip_code
                  });
                  form.formData.push({
                     name: 'billing[country]',
                     value: data.billing_info.country
                  });
               }

               // If there are attached document files then separate them into those
               // that need external verification and those that need be verified by
               // the platform.
               try {
                  var docs_platform = [],
                      docs_external = [];

                  var documents = $(this.input_documents);

                  $.each(documents, function(i,doc) {
                      var doc_files = $(doc).prop('files');
                      if (!doc_files || !doc_files.length)
                          throw new Error('Please provide all the required documents.');
                      var document = {
                          idx          : i,
                          type         : $(doc).data('type'),
                          verification : $(doc).data('verification'),
                          file         : doc_files[0]
                      };
                      if(document.verification === 'platform') {
                         docs_platform.push(document);
                      }
                      else if(document.verification === 'external') {
                         docs_external.push(document);
                      }
                  });

                  // Send documents that need verification from the marketplace provider first
                  // if any, then collect the response and send everything to the platform.
                  this.upload_to_marketplace(docs_external, function() {
                      _this.upload_to_platform(docs_platform, form);
                  }, form);
               }
               catch(error) {
                  this.activation_enabled(true);
                  this.bank_details.view.find('#bank-number').val('');
                  App.page.on_error(error.message);
               }
            },

            upload_to_marketplace: function(documents, next, form) {

              var _this = this;

              if(documents.length === 0) {
                 next(); return;
              }

              $.each(documents, function(i,document) {
                  $(_this.activation_docs.files_ext).fileupload('send', { 
                      files    : [ document.file ], 
                      formData : [{ name: 'purpose', value: 'identity_document' }]
                  })
                  .success(function (result, textStatus, jqXHR) {
                      ag_log(result);
                      ag_log('Document '+i+' token.');
                      ag_assert(result.size === document.file.size);
                      form.formData.push({
                          name  : 'documents['+document.idx+'][type]',
                          value : document.type
                      },{
                          name  : 'documents['+document.idx+'][filename]',
                          value : document.file.name
                      },{
                          name  : 'documents['+document.idx+'][mime]',
                          value : document.file.type
                      },{
                          name  : 'documents['+document.idx+'][token]',
                          value : result.id
                      });
                      if(i+1 === documents.length) {
                         next();
                      }
                   })
                   .error(function (response, textStatus, errorThrown) {
                      _this.submission_fail(response.responseJSON.error.message);
                   });
              });
            },

            upload_to_platform: function(documents, form) {

              var _this = this;

              // NOTE: If there are no files to be submitted, fileupload will not post
              // the form (?), so in that case just do a standard ajax post.
              if(documents.length) {
                 $.each(documents, function(i,document){
                     form.files.push(document.file);
                     form.formData.push({
                          name  : 'documents['+document.idx+'][type]',
                          value : document.type
                     },{
                          name  : 'documents['+document.idx+'][filename]',
                          value : document.file.name
                     },{
                          name  : 'documents['+document.idx+'][mime]',
                          value : document.file.type
                     });
                 });
                 $(this.activation_docs.files).fileupload('send', form)

                 .success(function (response, textStatus, jqXHR) {
                     _this.submission_success(response);
                 })
                 .error(function (response, textStatus, errorThrown) {
                     _this.submission_fail(response);
                 });
              } else {
                 this.post_form_data(form);
              }
            },

            post_form_data: function(form) {

              var _this = this;

              var postData = {};
              $.each(form.formData, function (i, field) {
                  postData[field.name] = field.value;
              });
              $.ajax({
                 url: App.URL_PROVIDER_IDENTITY,
                 type: "POST",
                 dataType: "json",
                 data: postData,

                 success: function (response) {
                    _this.submission_success(response);
                 },
                 error: function (response) {
                    _this.submission_fail(response);
                 }
              });
            },

            submission_fail: function(response) {
                this.activation_enabled(true);
                this.bank_details.view.find('#bank-number').val('');
                App.page.on_error(response);
            },

            submission_success: function(response) {
                //this.activation_enabled(true);
                App.page.on_success(response);
                //location.reload(true);
                App.page.change(App.URL_PROVIDER_HOME);
            },

            get_billing_info: function () {
               return {
                  address:  this.billing_details.view.find('#address').val(),
                  city:     this.billing_details.view.find('#city').val(),
                  state:    this.billing_details.view.find('#state').val(),
                  zip_code: this.billing_details.view.find('#zip-code').val(),
                  country:  this.billing_details.view.find('#country').val()
               };
            },

            get_bank_info: function () {
               return {
                  holder: this.bank_details.view.find('#bank-holder').val(),
                  number: this.bank_details.view.find('#bank-number').val()
               };
            },

            validate_billing_info: function () {
               return this.form.validate({
                  group: 'billing-details'
               }) ? true : false;
            },

            validate_bank_info: function () {
               return this.form.validate({
                  group: 'bank-details'
               }) ? true : false;
            }

        }

        // ---------------------------------------------------------------------
      
    };
   
    
    /**
     *  Show a specified tab after the page has loaded.
     */
    this.show_tab_on_startup = function() {
        var url_params = App.utils.get_url_params();
        if(url_params.has('view')) {
           $('#settings-tabs a[href="#'+url_params['view']+'"]').tab('show');
        }
    };
    

   this.initialize();

};


// AUGMENTED PROTOTYPES.


Dashboard.prototype.create_page = function () {
   return new SettingsPage;
};
