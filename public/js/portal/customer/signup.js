"use strict";

function CustomerSignupPage() {
		
    this.initialize = function() {
    	
    	// Setup views.
    	this.signup_view.setup(this);
    	
    };

    
    /* ***********************************************************************
     *                             Signup view
     * ***********************************************************************/
    

    this.signup_view = {

       	page   : null,
       	view   : $('#signup-form'),
       	form   : $('#signup-form').parsley(),
        button_submit : $('#signup-form button.submit'),
       	       	
       	setup : function(page) {
       		this.page = page;
        	this.view.find('button.submit').click(this.on_btn_submit_click.bind(this));
       	},
       	
       	
       	on_btn_submit_click : function(event) {
       		
       		var _this = this;
       		
        	if(!_this.form.validate())
        		return;
        	App.busy(_this.button_submit);
                
    		$.ajax({
 			   url: App.URL_CUSTOMER_SIGNUP,
 			   type: "POST",
 			   dataType: "json",
 			   data: {
 				   customer : _this.get_signup_info()
 			   },
 			   success: function (response) {
                                  //App.done(_this.button_submit);
 				  App.page.change(response.body.redirect);
 			   },
 			   error: function (response) {
                                  App.done(_this.button_submit);
 				  App.page.on_error(response);
 			   }
 		    });
    		event.preventDefault();
     	},

       	
       	get_signup_info : function() {
    		return {
    			first_name : this.view.find('#first-name').val(),
    			last_name  : this.view.find('#last-name').val(),
    	        username   : this.view.find('#username').val(),
    			password   : this.view.find('#password').val()
    		};
       	}
       	       	       	
    };

    this.initialize();
    
};


// AUGMENTED PROTOTYPES.


Portal.prototype.create_page = function() {
	return new CustomerSignupPage;
};
