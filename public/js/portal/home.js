"use strict";

/**
 * This namespace handles the portal home page.
 */

function HomePage() {
	

	
    this.initialize = function() {
    };

    
    /////////////////////////////////////////////////////////////////////////////
    ///                          EVENT HANDLERS                               ///
    /////////////////////////////////////////////////////////////////////////////


    
    /////////////////////////////////////////////////////////////////////////////
    
    this.initialize();
};


// AUGMENTED PROTOTYPES.

Portal.prototype.create_page = function() {
	return new HomePage;
};


