"use strict";

/**
 * This namespace handles the portal login page.
 */

function LoginPage() {
		
    this.initialize = function() {
    	
    	// Setup views.
    	this.login_view.setup(this);
    	
    };

    
    /* ***********************************************************************
     *                             Login view
     * ***********************************************************************/
    

    this.login_view = {

       	page      : null,
       	view      : $('.account-body'),
       	form      : {
                       data     : $('#form-login'),
                       input    : $('#form-login').parsley(),
                       username : $('input[name="username"]'),
                       password : $('input[name="password"]')
       	            },
        button_signin : $('#form-login button.signin'),
        forgot    : {
                       button_reset : $('#form-login #forgot-password'),
                       busy : function() {
                           this.button_reset.find('a').hide();
                           this.button_reset.find('.loading').removeClass('hidden');
                       },
                       done : function() {
                           this.button_reset.find('a').show();
                           this.button_reset.find('.loading').addClass('hidden');
                       }
                    },
       	
       	
       	setup : function(page) {
            this.page = page;
            this.form.data.submit(this.on_form_submit.bind(this));
            this.forgot.button_reset.find('a').click(this.on_forgot_password_click.bind(this));
       	},
       	
       	
       	on_form_submit : function(event) {
       		
            var _this = this;
            if(!_this.form.input.validate())
                return;
            App.busy(_this.button_signin);
            
            $.ajax({
                url: _this.form.data.attr('action'),
                type: "POST",
                dataType: "json",
                data: {
                    username: _this.form.username.val(),
                    password: _this.form.password.val()
                },
                success: function (response) {
                   App.page.change(response.body.redirect);
                },
                error: function (response) {
                   App.done(_this.button_signin);
                   App.page.on_error(response);
                }
            });
            event.preventDefault();
     	},
        
        
        on_forgot_password_click : function(e) {
            
            var _this = this;
            this.form.input.reset();
            this.form.username.parsley().validate();
            if(!this.form.username.parsley().isValid())
                return false;
            _this.forgot.busy();
            
            $.ajax({
                url: App.URL_USER_FORGOT_PASSWORD,
                type: "POST",
                dataType: "json",
                data: {
                    username: this.form.username.val(),
                },
                success: function (response) {
                    _this.forgot.done();
                   App.page.on_success(response);
                },
                error: function (response) {
                   _this.forgot.done();
                   App.page.on_error(response);
                }
            });
            e.preventDefault();
        }
               	       	       	
    };

    this.initialize();
    
};

// AUGMENTED PROTOTYPES.

Portal.prototype.create_page = function() {
	return new LoginPage;
};


