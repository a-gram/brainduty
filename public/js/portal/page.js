'use strict';

/**
 * This namespace contains functions that are used in the portal section of
 * the application and provided to all of its (sub) pages. Specific functionality
 * are provided by its (sub) pages.
 */
function Portal() {
	
    /**
     * The currently loaded (sub) page.
     */
    this.loaded = null;

    this.notifications_alert = $('header .navbar-header .alert');
    
    this.notifications_account = $('header .mainnav-menu .btn-profile .badge');

    //////////////////////////// EVENT HANDLERS ////////////////////////////

    /**
     *  This method handles displaying backend or third party services success
     *  responses.
     *  
     *  @param response A server response object or string message.
     */
    this.on_success = function (response) {
        var message = App.get_api_message(response);
        this.dialog.show('info', message, '', [{ type:'ok', label:'Ok' }]);
    };

    /**
     *  This method handles displaying backend or third party services error
     *  responses.
     *  
     *  @param response A server response object or string message.
     */
    this.on_error = function (response) {
        var message = App.get_api_error(response).error_description;
        this.dialog.show('error', message, '', [{ type:'ok', label:'Ok' }]);
    };

    /**
     *  This method handles displaying backend or third party services info
     *  messages.
     *  
     *  @param response A server response object or string message.
     */
    this.on_info = function (response) {
        var message = App.get_api_error(response).error_description;
        this.dialog.show('info', message, '', [{ type:'ok', label:'Ok' }]);
    };

    /**
     * Handler called whenever chat messages are received from the backend.
     * Must be registered/attached to the app's com object.
     * 
     * @param messages Array
     */
    this.on_chat_messages = function (messages) {
        this.update_notification_badges();
    };


    /**
     * Handler called whenever unseen messages are received from the backend.
     * Must be registered/attached to the app's com object.
     * 
     * @param messages Array
     */
    this.on_unseen_chat_messages = function (messages) {
        this.update_notification_badges();
    };


    /**
     * Handler called whenever latest messages are received from the backend.
     * Must be registered/attached to the app's com object.
     * 
     * @param messages Array
     */
    this.on_latest_chat_messages = function (messages) {
    };


    /**
     * Handler called whenever notifications (updates) are received from the backend.
     * Must be registered/attached to the app's com object.
     * Here the updates are filtered based on the type of event and passed on to the
     * registered handler of the currently loaded page before being pushed to the view.
     * 
     * @param updates Array
     */
    this.on_notifications = function (updates) {
        this.update_notification_badges();
    };


    /**
     * Handler called whenever unseen updates are received from the backend.
     * Must be registered/attached to the app's com object.
     * 
     * @param updates Array
     */
    this.on_unseen_notifications = function (updates) {
        this.update_notification_badges();
    };


    /**
     * Handler called whenever latest updates are received from the backend.
     * Must be registered/attached to the app's com object.
     * 
     * @param updates Array
     */
    this.on_latest_notifications = function (updates) {
    };


    ////////////////////////////////////////////////////////////////////////


    /* ***********************************************************************
     *                           Message Box
     * ***********************************************************************/
    

    // TODO Move this to the App namespace ?
    this.dialog = {

        box: $('#ag-modal'),
        icon: $('#ag-modal .modal-header > i'),
        title: $('#ag-modal .modal-title'),
        body: $('#ag-modal .modal-body'),
        btn_ok: $('#ag-modal .btn.ok'),
        btn_cancel: $('#ag-modal .btn.cancel'),

        titles: {
            error: 'Something went wrong',
            info: 'Success',
            warning: 'Warning'
        },

        icons: {
            error: 'fa-times-circle', // fa-frown-o
            info: 'fa-info-circle', // fa-smile-o
            warning: 'fa-exclamation-circle' // fa-meh-o
        },

        setup: function () {
            // On box closed, clean up any attached event handlers
            // to the OK/CANCEL buttons. Event handlers attached to
            // the on_close (hidden) event are always removed using
            // .one() since the box must always be closed to continue.
            this.box.on('hidden.bs.modal', function () {
                this.btn_ok.off('click');
                this.btn_cancel.off('click');
            }.bind(this));
        },

        on_close: function (handler) {
            this.box.one('hidden.bs.modal', handler);
        },


        /**
         * Display the dialog.
         * 
         * @param type The message box type (error,info,warning).
         * @param message The message to be displayed.
         * @param title The box title.
         * @param buttons Array of buttons to display in the dialog. Each button
         * is an object in the following format:
         *    {
         *       type  : 'ok' | 'cancel',
         *       label : 'Label of the button',
         *       callback : Closure to execute if the button is pressed
         *    }
         */
        show: function (type, message, title, buttons) {
            var _this = this;
            ag_assert(type && message || new Error);
            type = type.toLowerCase();
            title = title ? title : this.titles[type];
            buttons = buttons || [];
            
            this.btn_ok.hide();
            this.btn_cancel.hide();
            
            $.each(buttons, function(i,button) {
                var has = button.type == 'ok' ? _this.btn_ok : 
                         (button.type == 'cancel' ? _this.btn_cancel : 
                          null);
                if(has) {
                   has.text(button.label)
                      .one('click', button.callback)
                      .show();
                }
            });
            
            this.box.removeClass('error')
                .removeClass('info')
                .removeClass('warning');
            this.box.addClass(type);
            this.icon.attr('class', 'fa ' + this.icons[type]);
            this.title.text(title);
            this.body.empty().html('<p>' + message + '<p>');
            this.box.modal('show');
        }
    };

    /*
     *  Page components factory.
     */
    this.factory = {
    		
    };
    
    /**
     * Change the page to the specified url.
     * 
     * @param {string} url
     */
    this.change = function(url) {
    	window.location.replace(url);
    };
    

    /**
     * Update notification badges.
     */
    this.update_notification_badges = function() {
        this.notifications_alert.show();
        this.notifications_account.text('1+');
    };
    
    
    this.initialize();

};


// ----------------------------  PROTOTYPES -------------------------------


Portal.prototype.initialize = function() {
	
    // Layouts
    //App.core.navEnhancedInit ()
    App.core.navHoverInit ({ delay: { show: 250, hide: 350 } });
    App.core.initMastheadCarousel ();
    App.core.initBackToTop ();

    // Components
    App.components.initAccordions ();
    App.components.initFormValidation ();
    App.components.initTooltips ();
    App.components.initLightbox ();
    App.components.initSelect ();
    App.components.initAlerts();
    
    App.create_cache();
    
    // Comms
    App.com = new AGComm;
    App.com.ack = false;
    App.com.event_chat_messages_received = this.on_chat_messages.bind(this);
    App.com.event_chat_unseen_messages_received = this.on_unseen_chat_messages.bind(this);
    App.com.event_chat_latest_messages_received = this.on_latest_chat_messages.bind(this);
    App.com.event_notifications_received = this.on_notifications.bind(this);
    App.com.event_unseen_notifications_received = this.on_unseen_notifications.bind(this);
    
};


/**
 * Start the portal by creating and running the (sub) page manager.
 */
Portal.prototype.start = function() {
	
    // In the portal pages users may not be logged in, in which case the
    // RT communication with the server is not needed, so we check a flag in
    // the portal page environment to see whether to start the com.
    if(App.env.is_logged)
       App.com.open();
	
    // Check if the portal page factory method has been attached and call it.
    // NOTE: Portal pages may not need a page manager, so it's ok to just
    //       ignore it should there be none.
    if('create_page' in this) {
       this.loaded = Portal.prototype.create_page();
    } 

};

