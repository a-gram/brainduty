"use strict";

/**
 * This namespace handles the provider onboarding process.
 */

function OnboardingPage() {
	

	
    this.initialize = function() {
    	
    	// Setup views.
    	this.wizard_view.setup(this);
    	
    };

    
    /////////////////////////////////////////////////////////////////////////////
    ///                          EVENT HANDLERS                               ///
    /////////////////////////////////////////////////////////////////////////////


    
    /////////////////////////////////////////////////////////////////////////////
    
    
    
    /* ***********************************************************************
     *                          Booking wizard view
     * ***********************************************************************/
    
    
    this.wizard_view = {

       	page             : null,
       	view             : $('#process-wizard'),
       	validate         : [0],
       	steps            : [   0, // Steps are 1-based.
                               $('#wizard-step-1'),
       	                       $('#wizard-step-2'),
       	                       $('#wizard-step-3'),
       	                       $('#wizard-step-4')
       	                   ],
       	form             : $('#onboarding-form').parsley(),
        input_service_categories : $('#onboarding-form input[name=service_categories]'),
        button_submit    : $('#onboarding-form button.submit'),
        entity_type_list : new AGList,
        service_list     : new AGList,

        
       	setup : function(page) {
       		
            this.page = page;

            this.service_list.setup(this.page, 'services', 'multi');
            this.entity_type_list.setup(this.page, 'entity-type', 'single');
            this.set_step_validators ();

            this.view.find('button.next').click(this.on_btn_next_click);
            this.view.find('button.back').click(this.on_btn_back_click);
            this.view.find('button.submit').click(this.on_btn_submit_click);
            
            // Hack. See AGList
            this.steps[3].find('.select2').addClass('no-bubble');
       	},
       	
       	
       	set_step_validators : function () {
       		
            var _this = this;
            
            // Page 1 Welcome (no validation required).
            this.validate.push(function(){
                return true;
            });
            

            // Page 2 (Entity type selection) validator.
            this.validate.push(function(){

                var selected = _this.get_entity_type();
                var field = _this.steps[2].find('input[name=entity_type]');
                field.val( selected || '' );
                field.parsley().validate();
                return field.parsley().isValid();
            });


            // Page 3 (Provider services selection) validator.
            this.validate.push(function(){

                var services = _this.get_services_as_id_array(),
                service_categories = '';

                // Validate service selections and build service-categories JSON object.
                $.each(services, (function(i, service) {
                    var categories = _this.get_service_categories_as_id_string(service);
                    if(service == App.SERVICE_EXPERT && !categories) {
                       service_categories = '';
                       return false;
                    }
                    service_categories += '"' + service + '":"' + (categories||'')
                                              + (i<services.length-1?'",':'"');
                })
                .bind(_this));

                if(service_categories)
                   service_categories = '{'+service_categories+'}';

                _this.input_service_categories.val(service_categories);
                _this.input_service_categories.parsley().validate();

                if (!_this.input_service_categories.parsley().isValid())
                    return false;
                
                _this.business_details_form_visible( _this.is_business_entity() );

                return true;
            });


            // Page 4 (Provider and business details) validator.
            this.validate.push(function(){

                if(_this.is_business_entity() &&
                  !_this.form.validate({group:'step-3-business-details'}))
                  return false;

                if(!_this.form.validate({group:'step-3'}))
                  return false;
                  return true;
            });

       	},
       	
       	
       	get_entity_type : function() {
            var selected = this.entity_type_list.get_selected();
            return selected.length ? selected[0] : null;
       	},
       	
       	
       	// Get the selected services ids as an array
       	get_services_as_id_array : function() {
            return this.service_list.get_selected_as('id_array');
       	},
       	
        // Get the selected services ids as a comma-separated list string
//        get_services_as_id_string : function() {
//            return this.service_list.get_selected_as('id_string');
//        },
       	
       	// Get the selected categories ids for the specified service as an array
//        get_service_categories_as_id_array : function(service) {
//            var field = this.view.find('#service-'+service+'-categories');
//            return field.length ? this.service_list.get_selected_as('id_array',field.select2('data')) : null;
//        },

        // Get the selected categories ids for the specified service as a list string
       	get_service_categories_as_id_string : function(service) {
            var field = this.view.find('#service-'+service+'-categories');
            return field.length ? this.service_list.get_selected_as('id_string',field.select2('data')) : null;
       	},
       	
        get_selected_services : function() {
            return JSON.parse( this.view.find('input[name=service_categories]').val() );
        },

       	
       	get_entity_info : function() {
       		
            return {
                username    : this.view.find('#username').val(),
                password    : this.view.find('#password').val(),
                first_name  : this.view.find('#first-name').val(),
                last_name   : this.view.find('#last-name').val(),
                dob         : this.view.find('#dob').val(),
                mobile      : this.view.find('#mobile-number').val(),
                address     : this.view.find('#address').val(),
                city        : this.view.find('#city').val(),
                state       : this.view.find('#state').val(),
                zip_code    : this.view.find('#zip-code').val(),
                country     : this.view.find('#country').val()
            };
       	},
       	
       	
       	get_entity_business_info : function() {
       		
            return {
                entity_type     : this.get_entity_type(),
                business_name   : this.view.find('#business-name').val(),
                business_number : this.view.find('#business-number').val()
            };
       	},

       	
       	is_business_entity : function() {
            return App.is_user_business({ entity_type: this.get_entity_type() });
       	},
       	
       	has_expert_service : function() {
                var services = this.get_services_as_id_array();
                return $.inArray(App.SERVICE_EXPERT, services)>-1;
       	},
       	
       	
       	business_details_form_visible : function(visible) {
            if(visible)
               this.view.find('.business-details').show();
            else
               this.view.find('.business-details').hide();
       	},
       	
       	
       	on_btn_next_click : function(event) {
       		
            var _this = App.page.loaded.wizard_view;
            var curr_step = $(this).attr('data-step');

            if(!_this.validate[curr_step]())
                return;
        	
            var next_step = parseInt(curr_step) + 1;

            _this.steps[curr_step].fadeOut('slow', function() {
                _this.steps[next_step].fadeIn('slow');
            });
       	},
       	
       	
       	on_btn_back_click : function(event) {
       		
            var _this = App.page.loaded.wizard_view;
            var curr_step = $(this).attr('data-step');
            var prev_step = parseInt(curr_step) - 1;

            _this.steps[curr_step].fadeOut('slow', function() {
                _this.steps[prev_step].fadeIn('slow');
            });
       	},
       	
       	
       	on_btn_submit_click : function(event) {
       		
            var _this = App.page.loaded.wizard_view;

            var curr_step = $(this).attr('data-step');

            if(!_this.validate[curr_step]())
                    return;

            App.busy(_this.button_submit);

            var provider = _this.get_entity_info();

            provider.entity_type = _this.get_entity_type();

            if(_this.is_business_entity()){
               provider.business_name = _this.get_entity_business_info().business_name;
               provider.business_number = _this.get_entity_business_info().business_number;           
            }

            provider.services = _this.get_selected_services();

            $.ajax({
                url: App.URL_PROVIDER_SIGNUP,
                type: "POST",
                dataType: "json",
                data: {
                    provider: provider
                },

                success: function (response) {
                    //App.done(_this.button_submit);
                    App.page.change(response.body.redirect);
                },
                error: function (response) {
                     App.done(_this.button_submit);
                     App.page.on_error(response);
                }
            });
       	}
       	
       	
    };
    
    this.initialize();
    
};


// AUGMENTED PROTOTYPES.

Portal.prototype.create_page = function() {
    return new OnboardingPage;
};


