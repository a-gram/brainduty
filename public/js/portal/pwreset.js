

"use strict";

/**
 * This namespace handles the portal password reset page.
 */

function PasswordResetPage() {

    this.initialize = function () {

        // Setup views.
        this.reset_view.setup(this);

    };


    /* ***********************************************************************
     *                             Reset view
     * ***********************************************************************/


    this.reset_view = {

        page: null,
        view: $('.account-body'),
        form: {
            data:             $('#form-password-reset'),
            input:            $('#form-password-reset').parsley(),
            password:         $('input[name="password"]'),
            password_confirm: $('input[name="password-confirm"]'),
            token:            $('input[name="token"]'),
            button_submit:    $('#form-password-reset button.submit')
        },


        setup: function (page) {
            this.page = page;
            this.form.data.submit(this.on_form_submit.bind(this));
        },


        on_form_submit: function (event) {

            var _this = this;

            if (!_this.form.input.validate())
                return;

            App.busy(_this.form.button_submit);
            
            $.ajax({
                url: _this.form.data.attr('action'),
                type: "POST",
                dataType: "json",
                data: {
                    password: _this.form.password.val(),
                    token:    _this.form.token.val()
                },

                success: function (response) {
                    App.page.dialog.show('info', response.body.message, '', [{
                        type    :'ok',
                        label   :'Ok',
                        callback:function(){ App.page.change(App.URL_USER_LOGIN); }
                    }]);
                },
                error: function (response) {
                    App.done(_this.form.button_submit);
                    App.page.on_error(response);
                }
            });

            event.preventDefault();
        }

    };

    this.initialize();

};

// AUGMENTED PROTOTYPES.

Portal.prototype.create_page = function () {
    return new PasswordResetPage;
};

