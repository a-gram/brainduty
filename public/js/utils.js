/**
 * This file contains utility functions used throughout the frontend.
 *
 * @namespace Utils
 */
function AGUtils() {
    
    /**
     * Truncate the given text into a short version of maximum N characters.
     */
    this.truncate_text = function(text, N) {
        N = N || 50;
        var ttext = text.trim();
        var stext = ttext.substr(0,N);
    	return stext + (stext.length < ttext.length ? '...' : '');
    };
    

    /**
     * Get the parameters from the query string in the current page's url.
     * 
     * Adapted from http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
     * 
     * @returns {AGUtils.get_url_params.urlParams}
     */
    this.get_url_params = function() {
        
        var match,
            pl     = /\+/g,
            search = /([^&=]+)=?([^&]*)/g,
            decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
            query  = window.location.search.substring(1),
            urlParams = {
                has : function(param) { return param in this; },
                has_fragment : function() { return this.has('#'); }
            };
        while (match = search.exec(query))
              urlParams[decode(match[1])] = decode(match[2]);
        if(window.location.hash)
           urlParams['#'] = window.location.hash.substring(1);
        return urlParams;
    }
    
    /**
     * Get the short name of an entity (e.g. John D.).
     *
     * @param {object} entyty Must have a "first_name" and "last_name" property.
     * @returns {string}
     */
    this.shortname = function(entity) {
        if(!entity) return '';
        return entity.first_name && entity.last_name ?
               entity.first_name + ' ' + entity.last_name.charAt(0) + '.' :
               'Not Available';
    };
    
    
    /**
     * Get the full name of an entity.
     * 
     * @param {object} entity
     * @returns {String}
     */
    this.fullname = function(entity) {
        if(!entity) return '';
        return entity.first_name && entity.last_name ?
               entity.first_name +' '+entity.last_name : 'Not Available';
    };
    
    
    /**
     * Turn URLs in a text into html links.
     * 
     * @param {string} text
     * @returns {string}
     */
    this.linkfy = function(text) {
        return text.replace(/(https?)\:\/\/(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])/ig,
                            '<a href="$1://$2" target="_blank">$1://$2</a>');

    };
    
    
    /**
     * Format the given message by restoring paragraphs, linkfying URLs, etc.
     * 
     * @param {String} text
     * @returns {String}
     */
    this.format_message = function(text) {
        var linkfied = this.linkfy(text);
        var paragraphed = linkfied.replace(/\r?\n/g, '<br>');
        return paragraphed;
    };
    
    
    /**
     * Assign an object to another object's property.
     * 
     * @param {object} source
     * @param {object} target
     * @param {string} property
     * @returns {object}
     */
    this.assign = function(source, target, property) {
        var target_has_property = target.hasOwnProperty(property);
        if(!target_has_property || (target_has_property && typeof
                                    target[property] !== "object")) {
            target[property] = {};
        }
        for(var prop in source) {
            if (source.hasOwnProperty(prop))
                target[property][prop] = source[prop];
        }
        return target[property];
    };
    
    
    /**
     * Get the ISO 3166 code of a supported country.
     * 
     * @param {string} country
     * @returns {string} The ISO 3166 code of the given country.
     */
    this.country_to_iso3166 = function(country) {
        var map = {
            Australia  :  'AU'
        };
        return map[country];
    };

};
