

"use strict";


function AGChat() {

    this.page                = null;
    this.view                = $('.task-chat');
    this.message_template    = $('.task-chat .message-template li');
    this.message_box         = $('.task-chat .message-box');
    this.message_text        = $('.task-chat .message-box .message-box-body');
    this.message_attachments = $('.task-chat .message-box .message-box-attachments');
    this.message_progress    = $('.task-chat .hloader');
    this.message_form        = { files: [], formData: [] };
    this.message_parser      = {};


    this.setup = function (page) {

        this.page = page;
        this.set_message_parsers();

        // Event handlers bindings

        App.com.event_chat_message_attachments_added = this.on_message_attachments_added.bind(this);
        App.com.event_chat_message_posted = this.on_message_post_success.bind(this);
        App.com.event_chat_message_post_failed = this.on_message_post_fail.bind(this);
        App.com.event_chat_message_post_progress = this.on_message_post_progress.bind(this);

        this.message_box.find('.btn-post').click(this.on_btn_post_message_click.bind(this));

        this.message_text.val(''); // workaround for textareas not empty
    };


    /**
     * Transactional messages tipically (not always) require some parameters/info
     * to be collected and sent to the server (and/or the other party). These
     * additional info are stored in the message.data field and collected from the
     * user chat message itself using a simple field=value pairs list (called header)
     * to be inserted at the very beginning of the chat message. These parser methods
     * take care of processing the various message types and, for transactional types,
     * get the required info and include them in the message.
     * 
     * @return All of them return the message augmented with extra fields included in
     * the message.data payload and an array of these extra fields to be added to the
     * multipart form, if any. On error null is returned.
     */
    this.set_message_parsers = function () {

        var _this = this;

        /*
         * Standard Conversational messages don't require any extra parameters.
         */
        this.message_parser[App.MESSAGE_TASK_CONVERSATION] = function (message) {
            return {
                message: message,
                form_fields: []
            };
        };


        /*
         * Task Charge Request messages require the following header:
         * 
         *       amount = <currency> <charge amount>
         *       description = <charge description>
         */
        this.message_parser[App.MESSAGE_TASK_CHARGE_REQUEST] = function (message) {

            var cur = App.currency();
            var header = 'amount = ' + cur + ' \ndescription = \n\n';
            var header_pattern = new RegExp(
                '^amount *= *' + cur + ' *(?:\\d+|\\d+\.\\d{2}) *\\r?\\n' +
                'description *= *[A-Za-z0-9_ ,.()]{5,}'
            );
            var header_error_msg =
                'To send a charge request message, specify the charge amount and the description ' +
                'by including 2 lines at the beginning as in the following example:<br><br><i>' +
                'amount = ' + cur + ' 10<br>' +
                'description = A short description for the charge here.<br><br>' +
                'An optional message can be appended here.</i><br><br>' +
                'and post it using the <b>Charge request</b> option.';

            var lines = message.body.trim().split(/\r?\n/);
            // If the user hasn't written the header in the message then 
            // insert a blank one and invite her to fill it out.
            if (lines.length < 2) {
                _this.message_text.val(header + message.body);
                App.page.dialog.show('info', header_error_msg, 'Charge Request', [{
                    type : 'ok',  label : 'Ok'
                }]);
                return null;
            }

            if (!header_pattern.test(message.body)) {
                App.page.dialog.show('info', header_error_msg, 'Charge Request', [{
                    type : 'ok',  label : 'Ok'
                }]);
                return null;
            }

            message.data = {};

            // NOTE: Amounts are in cents, so we need to convert it here before posting.
            message.data.amount = lines[0].split('=')[1].replace(cur, '').trim();
            message.data.amount = parseInt(message.data.amount * 100);
            message.data.description = lines[1].split('=')[1].trim();
            message.data.service = App.SERVICE_EXTRA;

            // The extra fields to be added to the multipart form.
            var form_fields = [{
                name: 'message[data][service]',
                value: message.data.service
            }, {
                name: 'message[data][amount]',
                value: message.data.amount
            }, {
                name: 'message[data][description]',
                value: message.data.description
            }];

            // We need to remove the header from the message as it's no longer needed.
            lines.splice(0, 2);
            message.body = lines.join('\n');

            // Attachments not allowed for this message type.
            _this.clear_attachments();

            return {
                message: message,
                form_fields: form_fields
            };
        };

        /*
         * Task Cancel Request messages don't require any extra parameters.
         */
        this.message_parser[App.MESSAGE_TASK_CANCEL_REQUEST] = function (message) {
            _this.clear_attachments();
            return {
                message: message,
                form_fields: []
            };
        };

        /*
         * Task Deliverable messages don't require any extra parameters.
         */
        this.message_parser[App.MESSAGE_TASK_DELIVERABLES] = function (message) {
            return {
                message: message,
                form_fields: []
            };
        };

    };


    this.on_btn_post_message_click = function (event) {

        var message,
            message_type = $(event.target).data('message-type');
        
        // If the request is for a slot we build and send the message straight
        // away, otherwise parse the user message.
        if($(event.target).hasClass('slot')) {
            
           // The base slot should be the first charged service.
           var slot = this.page.open_task.services[0];
           
           ag_assert(slot.ref === App.SERVICE_GENERIC || slot.ref === App.SERVICE_EXPERT);
           
           message = {
               type: message_type,
               body: "amount="+slot.amount.to_currency()+"\n"+
                     "description="+slot.description+"\n"+
                     "The duty requires an extra slot to be continued."
           };
        } else {
           message = {
               type: message_type,
               body: this.message_text.val()
           };
        }
        
        this.send(message);
    };
    

    this.on_message_post_success = function (message) {
        
        this.message_progress.hide();
        this.clear_form();

        if (this.page.open_task && this.page.open_task.ref == message.task) {
            this.page.open_task.messages.push(message);
            this.append(message);
        } else {
            var task = App.cache.get(message.task);
            // The following line should never fail
            ag_assert(task != null || new Error('Task not found in the cache.'));
            task.messages.push(message);
        }
        // Process txn messages success    	
        this.txn_msg_on_success(message);
    };


    this.on_message_post_fail = function (response) {
        this.message_progress.hide();
        this.clear_form();
        App.page.on_error(response);
    };


    this.on_message_attachments_added = function (files) {

        this.message_form.files = files;

        var items = '';
        $.each(files, function (i, file) {
            items += App.page.factory.make_attachment_nolink(file.name);
        });
        items = App.page.factory.make_attachment_nolink(items, App.LIST);

        this.message_attachments.empty();
        this.message_attachments.append(items);
    };


    this.on_message_post_progress = function (progress) {
        // TODO
    };


    this.send = function (message) {
        
        var _this = this;

        if (this.page.open_task == null)
            throw new Error('No active conversation set.');

        // For transactional messages we need to 'augment' the message with more info,
        // so parse the message and get any required extra parameters.
        var message_x = this.message_parser[message.type](message);

        if (message_x === null)
            return;

        message.body = message.body.trim();

        // If a message body is not given for transactional messages then a
        // default one will be used since the system always requires one. The
        // system will anyways set the body when the message is received and
        // will append the one given by the user, if any.
        // For non-transactional messages, a body must be provided by the user.
        if (message.body == '') {
            if (App.is_transactional(message)) {
                message.body = 'No message from user.';
            } else {
                App.page.on_error('The message is empty. Please write something.');
                return;
            }
        }

        // Add the standard fields to the multipart form.
        this.message_form.formData = [{
            name: 'message[body]',
            value: message.body
        }, {
            name: 'message[type]',
            value: message.type
        }, ];

        // Add extra fields, if any.
        message_x.form_fields.forEach(function (field) {
            this.message_form.formData.push(field);
        }, this);

        var do_post = function () {
            App.com.post_chat_message(_this.message_form, _this.page.open_task.ref);
            _this.message_text.val('');
            _this.message_progress.show();
        };

        // If the message is transactional then process it before posting it
        // and return if the execution is aborted, otherwise do the post.
        if (App.is_transactional(message) &&
            !this.txn_msg_before_execute(do_post, message, this.page.open_task.ref))
            return;

        do_post();
    };

    // ------------------------------------------------------------------------
    // Some task commands are sent thru transactional messages (for example ending
    // and cancelling) and these methods handle such messages events.

    this.txn_msg_before_execute = function (do_post, message, task_ref) {
        
        if (message.type == App.MESSAGE_TASK_DELIVERABLES) {
            var ctx = {
                task_ref: task_ref,
                post: {},
                do_post: do_post
            };
            return this.page.task.command_handlers['before_execute_end'](ctx);
        }
        // Put here other messages before execute ...
        else {
            return true;
        }
    };

    this.txn_msg_on_success = function (message, data) {
    };

    this.txn_msg_on_error = function (message, data) {
    };

    // ------------------------------------------------------------------------

    this.append = function (message) {

        var _this = this;

        // Make sure the message belongs to the active conversation.
        if (message.task != this.page.open_task.ref)
            throw new Error('Message does not match the active conversation.')

        var message_list = this.view.find('.task-message-list > ul');

        if (message_list.length === 0) {
            message_list = App.page.factory.make_chat_message(null, App.LIST);
        }

        var msg = App.page.factory.make_chat_message(message);

        var $msg = $(msg);

        // When a transactional message is added, attach event handlers that
        // will respond to the taken action and any contextual params, if any.
        if (App.is_transactional(message)) {

            // The sender of the message does not take actions, so remove the
            // action buttons if this user is the sender.
            if (App.page.loaded.user_is(message.sender)) {
                $msg.find('.message-actions').empty();
            } else {
                var action_url = App.URL_BASE + message.action_method + '{action}' +
                    (message.action_params || '');

                // The handlers to be invoked on successfull and failed actions and
                // contextual parameters to be returned in these handlers, if any. 
                // These depends on the type of message and action.
                var action_on_success = null,
                    action_on_error = null,
                    action_ctx = null;

                if (message.type == App.MESSAGE_TASK_CANCEL_REQUEST) {
                    action_on_success = _this.page.message.action_handlers['on_task_cancel_request_success'];
                    action_on_error = _this.page.message.action_handlers['on_task_cancel_request_error'];
                    action_ctx = {
                        task_ref: message.task
                    };
                }

                if (message.type == App.MESSAGE_TASK_CHARGE_REQUEST) {
                    action_on_success = _this.page.message.action_handlers['on_charge_request_success'];
                    action_on_error = _this.page.message.action_handlers['on_charge_request_error'];
                    action_ctx = {
                        task_ref: message.task
                    };
                }
                
                if (message.type == App.MESSAGE_TASK_END_REQUEST) {
                    action_on_success = _this.page.message.action_handlers['on_task_end_request_success'];
                    action_on_error = _this.page.message.action_handlers['on_task_end_request_error'];
                    action_ctx = {
                        task_ref: message.task
                    };
                }
                
                // Attach the handlers to the message's action buttons.
                $msg.find('.btn-action').click(function (event) {
                    var action_button = $(this);
                    action_url = action_url.replace('{action}', action_button.data('action'));
                    action_ctx.user_action = action_button.data('action');
                    action_ctx.button = action_button;
                    App.busy(action_button);
                    App.page.post_command(action_url, {},
                                          action_on_success.bind(_this.page),
                                          action_on_error.bind(_this.page),
                                          action_ctx);
                    event.preventDefault();
                });
            }
        }

        message_list.append($msg);

        // Assume the message has been seen
        message.is_seen = true;
    };


    this.clear = function () {
        this.view.find('.task-message-list').empty();
    };


    this.clear_attachments = function () {
        this.message_form.files = [];
        this.message_attachments.empty();
    };

    this.clear_form = function() {
        this.clear_attachments();
        this.message_form.formData= [];
    };

    this.hide = function () {
        this.view.hide();
    };
    this.show = function () {
        this.view.show();
    };

};

