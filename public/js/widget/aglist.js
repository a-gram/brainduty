
/**
 *  A multi-purpose list view used across the app.
 *  This list widget works by associating an array of objects to it so that each
 *  item of the list represents an object in the associated array.
 *  
 */

function AGList() {

    // The parent containing page.
    this.page = null;

    // The task details view object.
    this.view = null;

    // The name of the array of entities associated with this list.
    this.entity = null;

    this.on_list_item_click = {

        single: function (e) {
            // This is a small hack to prevent click events on elements embedded
            // inside the list item from bubbling and toggling the item status.
            // Just add the 'no-bubble' class to embedded elements that are not
            // supposed to propagate click events.
            if ($(e.target).hasClass('no-bubble') || 
                $(e.target).parents('.no-bubble').length) {
                return;
            }
            $(this).siblings('.list-group-item').each(function () {
                $(this).removeClass('active');
            });
            $(this).addClass('active');
        },

        multi: function (e) {
            if ($(e.target).hasClass('no-bubble') || 
                $(e.target).parents('.no-bubble').length) {
                return;
            }
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            } else {
                $(this).addClass('active');
            }
        }
    };

    this.setup = function (page, entity, type) {

        this.page = page;
        this.entity = entity;
        var list = '.' + entity + '.ag-list.' + type + '-select.list-group';
        this.view = $(list);
        if (this.view.length === 0)
            throw new Error('AGList "' + list + '" not found.')
        this.view.find('.list-group-item').click(this.on_list_item_click[type]);
    };

    /**
     * Return the entities selected in the list from the linked entity collection.
     * The linked entity collection must be an array of objects with an 'id' property
     * linked to the list items by a 'data-{entity}-id attribute.
     * 
     * @return {array} An array of the selected objects in the linked entity collection.
     * If the entity collection is not found, the returned array contains the data-{entity}-id
     * attribute values of the selected items.
     */
    this.get_selected = function () {

        if (this.entity == null)
            throw new Error('ERROR: AGList.get_selected(): entity is not defined');

        var _this = this;
        var selected_entities = [];

        if (this.entity in App.env && App.env[this.entity] instanceof Array) {
            $.each(App.env[this.entity], function (idx, e) {
                var selected = _this.view.children('.active[data-' + _this.entity + '-id="' + e.id + '"]');
                if (selected.length) {
                    if (selected.length === 1)
                        selected_entities.push(e);
                    else throw new Error('ERROR: AGList.get_selected(): ' +
                        _this.entity + ' list has multiple equal ids.');
                }
            });
        } else {
            _this.view.children('.active').each(function (i) {
                selected_entities.push($(this).data(_this.entity + '-id'));
            });
        }
        return selected_entities;
    };


    /**
     * @param {format} String The format into which to return the entities.
     * @return The selected entities (or the given entities) in the specified
     * format, or null if there are no entities.
     */
    this.get_selected_as = function (format, entities) {

        format = format || '';
        entities = entities || this.get_selected();

        if (entities.length === 0) return null;

        // Returns the selected entities as an array of their ids.
        if (format === 'id_array') {
            return entities.map(function (entity) {
                return typeof entity === 'object' ? entity.id : entity;
            });
        } else
        // Returns the selected entities as a (comma-separated) id list string.
        if (format === 'id_string' || format === 'id_string_quoted') {
            var list = '',
                q = format === 'id_string_quoted' ? '"' : '';
            entities.forEach(function (entity, i) {
                list += q + (typeof entity === 'object' ? entity.id : entity) +
                        q + (i === entities.length - 1 ? '' : ',');
            });
            return list;
        }
        // Returns the selected services as is (array of objects).
        else {
            return entities;
        }
    };
    

    /**
     * @return All the entities in the list, or null if there are no entities.
     */
    this.get_all = function () {
        
        if (this.entity == null)
            throw new Error('ERROR: AGList.get_selected(): entity is not defined');

        var _this = this;
        var entities = [];

        if (this.entity in App.env && App.env[this.entity] instanceof Array) {
            entities = App.env[this.entity];
        } else {
            _this.view.children().each(function (i) {
                entities.push($(this).data(_this.entity + '-id'));
            });
        }
        return entities;
    };


};

