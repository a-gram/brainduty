"use strict";

/**
 * This file groups widgets/views that are related to notifications. 
 */


/* ***********************************************************************
 *                        Notifications view
 * ***********************************************************************/


function NotificationsView(page, type) {
	    
    this.page   = page;
    this.type   = type;
    this.view   = $('header .navbar-notification.'+type);
    this.list   = $('header .navbar-notification.'+type+' .notification-list');
    this.badge  = $('header .navbar-notification.'+type+' .badge');
    this.global = $('header .navbar-header .alert');


    this.setup = function() {

        this.view.on('shown.bs.dropdown', this.on_show.bind(this));
        this.view.on('hidden.bs.dropdown', this.on_hide.bind(this));

    };


    this.on_show = function() {
    };


    this.on_hide = function() {
        this.list.children().removeClass('new');
        this.clear_badge();
    };


    /**
     * This method must be ovverriden/reimplemented by page managers in order
     * to respond accordingly to clicks on notification items.
     */
    this.on_notification_click = function() {};


    /**
     * This method creates the notification items to be inserted in the
     * notification list. Must be reimplemented by page managers depending on the
     * type of notification.
     * @param notification The notification object from which to build the item.
     */
    this.item_factory = function(notification) {};


    /**
     * Push notifications in the list.
     * 
     * @param {Object} notifications
     * @param {bool} alert If true, the user is alerted by updating the badges.
     */
    this.push = function(notifications, alert) {
        alert = alert === undefined ? true : alert;
        if(notifications.length===0) return;
        var _this = this;
        var items = [];
        $.each(notifications, function(i,notification){
            var item = $( _this.item_factory(notification) );
            item.click(_this.on_notification_click);
            items.push(item);
        });
        this.list.prepend(items);
        if(alert)
           this.update_badge();
    };


    this.update_badge = function() {
        //var count = this.list.children().length;
        var nnew = this.list.children('.new').length;
        this.badge.text(nnew || '');
        this.global.show();
    };


    this.clear_badge = function() {
        this.badge.text('');
        this.global.hide();
    };


    this.clear = function() {
        this.list.empty();
        this.update_badge();
    };


    this.setup();
};
