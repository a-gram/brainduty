"use strict";

/**
 * This file groups widgets/views that are related to tasks. 
 */


/* ***********************************************************************
 *                            Task list view
 * ***********************************************************************/


function TaskListView() {

    this.page          = null;
    this.view          = $('.task-list-view');
    this.list          = $('.task-list-view .panel .list-group.tasks');
    this.no_list       = $('.task-list-view .panel-empty');
    this.button_load_more = $('.task-list-view .btn.load-more');
    this.tasks_type    = '';


    this.setup = function (page) {
        
        if(this.list.hasClass('open')) {
           this.tasks_type = 'open';
        }
        else if(this.list.hasClass('closed')) {
           this.tasks_type = 'closed';
        }
        else if(this.list.hasClass('disputed')) {
           this.tasks_type = 'disputed';
        }
        else {
           App.page.abort('Undefined task_type in TaskListView');
        }
        this.page = page;
        this.list.find('.list-group-item').click(this.on_item_click.bind(this));
        this.button_load_more.click(this.on_btn_load_more_click.bind(this));
        this.show();
    };


    this.on_item_click = function (event) {
        var task_ref = $(event.currentTarget).data('task-ref');
        this.page.task_details_view.open(task_ref);
    };
    
    
    this.on_btn_load_more_click = function(e) {
        
        var _this = this;
        var curr_page = _this.button_load_more.data('page');
        var load_page = curr_page + 1;
        var query = '?page='+(load_page)+'&where='+this.tasks_type;
        App.busy(_this.button_load_more);
        $.ajax({
            type: "GET",
            url: App.URL_TASKS_GET+query,
            data: {},
            dataType: "json",

            success: function (response) {
               App.done(_this.button_load_more);
               $.each(response.body.tasks, function(i,task){
                   var item = $( App.page.factory.make_task_list_item(task) );
                       item.click(_this.on_item_click.bind(_this));
                   _this.list.append(item);
               });
               $(e.target).data('page', load_page)
               if(load_page >= response.body.pages) {
                  _this.button_load_more.remove();
               }
            },
            error: function (response) {
               App.done(_this.button_load_more);
               App.page.on_error(response);
            }
        });
    };


    this.add = function (item) {
        var added = this.list.prepend(item);
        if (this.list.children().length === 1)
            this.show();
        return added;
    };


    this.remove = function (item) {
        this.list.find('#' + item).remove();
        if (this.list.children().length === 0)
            this.show();
    };


    this.item = function (id) {
        return this.list.find('#' + id);
    };


    this.exists = function (item) {
        return this.list.find('#' + item).length > 0;
    };


    this.set_item_status = function (item, status) {
        var item = this.list.find('#' + item);
        status = App.task_status_to_string(status);
        var classes = 'label label-task-status label-task-' + status + ' pull-right';
        var label = item.find('.label-task-status');
        label.prop('className', classes);
        label.text(status);
    };


    /**
     * Show the list if not empty, otherwise show the empty list panel.
     */
    this.show = function () {
        if (this.list.children().length > 0) {
            this.list.parent().show();
            this.no_list.hide();
        } else {
            this.list.parent().hide();
            this.no_list.show();
        }
    };

};


/* ***********************************************************************
 *                          Task details view
 * ***********************************************************************/


function TaskDetailsView() {

    this.page = null;
    this.view = $('.task-details-view');
    this.commands = $('.task-details-view .task-commands');
    this.party_button = $('.task-details-view .task-party'),
    this.party_info = null;
    

    this.setup = function (page) {
        var _this = this;
        this.page = page;
        this.view.find('.btn-close-view').click(this.on_btn_close_click.bind(this));
        this.party_info = this.view.find('.task-party-info').detach();
        this.party_info.find('.btn.favourite').click(this.on_btn_favourite_click.bind(this));
        this.party_info.find('.btn.ignore').click(this.on_btn_ignore_click.bind(this));
        this.party_button.popover({
             placement : 'right',
             html      : true,
             content   : function() {
                 return _this.party_info.removeClass('hidden');
             }
        });
    };


    this.show = function () {
        $('.task-view').removeClass('task-loading');
        $('.task-view').addClass('task-open');
    };


    this.close = function () {
        $('.task-view').removeClass('task-open');
        $('.task-view').removeClass('task-loading');
    };
    
    
    /**
     * 
     * @param {type} task_ref
     * @param {type} msg_ref
     */
    this.open = function (task_ref, msg_ref) {

        var _this = this;

        _this.loading();

        App.page.fetch_task(
            task_ref,
            function (task) {
                _this.clear();
                _this.fill(task);
                _this.show();
                _this.goto(msg_ref || null);
            },
            function () {
                _this.close();
            }
        );
    };


    this.loading = function () {
        $('.task-view').addClass('task-loading');
    };


    this.is_closable = function (closable) {
        if (!closable) this.view.find('.btn-close-view').hide();
        else this.view.find('.btn-close-view').show();
    };


    this.on_btn_close_click = function () {
        this.close();
        this.clear();
    };
    
    
    this.on_btn_favourite_click = function (e) {
        var user_ref = this.party_info.data('user-ref');
        App.page.post_command(
            App.URL_USER_FAVOURITE.replace('{user_ref}',user_ref),
            null, null, null, {
                button: $(e.currentTarget)
            }
        );
    }
    

    this.on_btn_ignore_click = function (e) {
        var user_ref = this.party_info.data('user-ref');
        App.page.post_command(
            App.URL_USER_IGNORE.replace('{user_ref}',user_ref),
            null, null, null, {
                button: $(e.currentTarget)
            }
        );
    }
    
    
    /**
     * This method is an handler that is attached to the task command button(s)
     * which appears in the header of the task details view. These commands
     * give raise to different actions depending on task status and type of
     * user. The binding between the command button(s) and this handler is done
     * in the TaskListView.set_commands() method, that must be overridden by page
     * managers.
     */
    this.on_btn_command_click = function (event) {

        var _this = App.page.loaded.task_details_view;
        var command = $(this).data('command');
        var command_type = $(this).data('command-type');
        var task_ref = $(this).data('task-ref');
        var url = App.URL_TASK_COMMAND.replace('{task_ref}', task_ref)
                                      .replace('{command}', command);

        var command_on_success = _this.page.task.command_handlers['on_' + command + '_success'];
        var command_on_error = _this.page.task.command_handlers['on_' + command + '_error'];

        var command_ctx = {
            task_ref: task_ref,
            post: {},
            button: $(this)
        };

        command_ctx.do_post = function () {
            App.busy(command_ctx.button);
            App.page.post_command(url, command_ctx.post,
                command_on_success.bind(_this.page),
                command_on_error.bind(_this.page),
                command_ctx);
        }

        // Call the pre-posting handler to give a chance to do any preprocessing,
        // get additional post data or aborting the execution of the command.
        var before_execute = _this.page.task.command_handlers['before_execute_' + command];
        before_execute = before_execute.bind(_this.page);

        var can_proceed = before_execute(command_ctx);

        if (can_proceed) {
            // For asynch commands we make an ajax call else we make a page GET.
            if (command_type === 'asynch')
                command_ctx.do_post();
            else
                App.page.change(url);
        }
    };


    this.fill = function (task) {

        ag_assert(task != null || new Error);
        ag_assert(task.messages || new Error);

        var _this = this;

        var unacked = [];

        this.page.open_task = task;

        this.set_commands();
        this.set_available_actions();
        this.set_status(task.status);
        this.set_subject(task.subject);
        this.set_created_at(task.schedule_datetime);
        this.set_funded(task.is_funded);
        this.set_services(task.services);
        this.set_price(task);
        this.set_notes(task);
        this.set_party(task);

        $.each(task.messages, function (i, message) {
            message.task = task.ref;
            // Task request message is the task description
            if (message.type == App.MESSAGE_TASK_REQUEST) {

                _this.set_description(message.body);

                if (message.attachments && message.attachments.length) {
                    var items = '';
                    $.each(message.attachments, function (i, attachment) {
                        items += App.page.factory.make_attachment(attachment, task.ref);
                    });
                    _this.set_attachments(items);
                }
            }
            // Any other message type should be appeneded to the chat
            else {
                // Verify if the message needs an ack.
                if (!message.is_delivered || !message.is_seen) {
                    // Remember the message as delivered
                    if (!message.is_delivered)
                        App.com.pool(message);

                    message.is_delivered = true;
                    unacked.push(message);
                }
                _this.chat.append(message);
            }
        });

        if (unacked.length > 0)
            App.com.ack_messages(unacked);
    };


    this.clear = function () {
        this.set_subject('');
        this.set_description('');
        this.set_created_at('');
        this.set_price(null);
        this.set_attachments(null);
        this.set_services(null);
        this.set_notes(null);
        this.set_party(null);
        this.chat.clear();
        this.page.open_task = null;
    };
    
    
    /**
     * Scroll to the specified message in the chat window.
     * 
     * @param {type} msg_ref
     */
    this.goto = function(msg_ref) {
        if(!msg_ref) return;
        var msg = $('#'+msg_ref);
        if(msg.length)
           $('html, body').scrollTop(msg.offset().top);
    };


    // SETTERS

    this.set_status = function (status) {
        status = App.task_status_to_string(status);
        var classes = 'label label-task-status label-task-' + status;
        var label = this.view.find('.label-task-status');
        label.prop('className', classes);
        label.text(status);
    };

    this.set_subject = function (subject) {
        this.view.find('.task-subject').text(subject);
    };

    this.set_description = function (description) {
        this.view.find('.task-description').html(
            App.utils.format_message(description)
        );
    };

    this.set_created_at = function (date) {
        this.view.find('.task-created-at').text(date);
    };

    this.set_price = function (task) {
        if (!task) {
            this.view.find('.task-price').text('');
            return;
        }
        var price = 0;
        $.each(task.services, function (i, service) {
            price += parseInt(service.amount);
        });
        price = price.to_currency();
        this.view.find('.task-price').text(price);
    };

    this.set_funded = function (funded) {
        if (!funded) {
            this.view.find('.task-funded').hide();
        } else {
            this.view.find('.task-funded').show();
        }
    };

    this.set_attachments = function (items) {
        if (items) {
            items = App.page.factory.make_attachment(items, null, App.LIST);
            this.view.find('.task-attachments').append(items);
        } else {
            this.view.find('.task-attachments').empty();
        }
    };

    this.set_services = function (services) {
        if (services && services.length) {
            var items = App.page.factory.make_task_service_table(services);
            this.view.find('.task-services table tbody').append(items);
        } else {
            this.view.find('.task-services table tbody').empty();
        }
    };

    this.set_notes = function (task) {
        var notes = '';
        if(task) {
           notes += task.notes ? App.page.factory.make_task_note(task.notes) : '';
           notes += task.dispute ? App.page.factory.make_task_dispute(task.dispute) : '';
        }
        else {
            this.view.find('.task-notes-content').empty();
        }
        if (notes) {
            this.view.find('.task-notes-content').append(notes);
            this.view.find('.task-notes').show();
        } else {
            this.view.find('.task-notes').hide();
        }
    };

    this.set_party = function (task) {
        if(task) {
           var party=null, role=null;
           if(App.page.loaded.is_customer() && task.provider) {
              party = task.provider;
              role = 'Assistant';
              this.party_info.data('user-ref', party.ref);
           }
           if(App.page.loaded.is_provider() && task.customer) {
              party = task.customer;
              role = 'Customer';
              this.party_info.find('.btn').remove();
           }
           
           if(party) {
              this.party_button.text(App.utils.shortname(party));
              this.party_info.find('.role').text(role);
              this.party_info.find('.name').text(App.utils.fullname(party));
              this.party_info.find('.about').text(party.about);
              this.party_info.find('.rating').html(App.page.factory.make_rating(party.rating));

              if(party.ref)  this.party_info.find('.btn').show();
              else           this.party_info.find('.btn').hide();
           } else {
              this.party_info.find('.name').text('N/A');
           }
        }
        else {
           this.party_button.text('N/A');
           this.party_info.find('.role').text('');
           this.party_info.find('.about').text('');
           this.party_info.find('.rating').html('');
           this.party_info.data('user-ref', '');
           this.party_info.find('.btn').hide();
        }
    };

    // The following two functions set the commands and actions available
    // for the currently loaded task, which depend on task status and type
    // of user (customers and providers may be allowed different actions
    // on a task for the same task status). These functions are not defined
    // here and must be reimplemented and attached by page managers.
    this.set_commands = function () {};
    this.set_available_actions = function () {};


    // This object implements a basic/default chat object to manage chat
    // messages related to a task. Its only functionality is that of adding
    // messages to the chat and clearing. If a fully fledged chat object is
    // needed then it must be attached by page managers.
    this.chat = {

        view: $('.task-chat'),
        msg_box: $('.task-chat .message-box'),

        append: function (message) {

            if (this.msg_box.length)
                this.msg_box.remove();

            var message_list = this.view.find('.task-message-list > ul');

            if (message_list.length === 0) {
                message_list = App.page.factory.make_chat_message(null, App.LIST);
            }

            var msg = App.page.factory.make_chat_message(message);

            var $msg = $(msg);

            // For transactional messages, remove the action buttons.
            if (App.is_transactional(message)) {
                $msg.find('.message-actions').empty();
            }

            message_list.append($msg);

            // Assume the message has been seen
            message.is_seen = true;
        },

        clear: function () {
            this.view.find('.task-message-list').empty();
        }
    };

};
