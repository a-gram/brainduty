# Brainduty

Brainduty is a web platform implementing a 2-sided marketplace for on-demand personal assistance. It connects service providers, such as consultants, specialists, assistants, etc. to people who need advices or some task performed.

An example website (no longer in production) can be found [here](https://brainduty.albertogramaglia.com).

## Techs

The platform is based on the following tech stack:

- PHP 5.6+
- Laravel 5.2+
- MySQL 5+
- jQuery 2+

and makes use of external REST APIs and web services, such as Stripe and AWS.

## Documentation

Not available

## Contributing

Contributions are always welcome. Just fork and clone the repo. Make your changes, test them and submit a pull request.

## License

The Brainduty platform is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
