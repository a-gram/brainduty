<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Exceptions Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for all system exceptions.
    |
    */

    'app-offline'                 => 'Our services are temporarily offline for maintenance. We apologize for any '.
                                    'inconvenience and will be back as sson as possible.',
    
    'account-email-unverified'    => 'Your email is unverified. Please verify your email by clicking on the verification '.
                                     'link in the welcome email sent to your registered address.',
    
    'account-evlink-expired'      => 'The verification link has expired. If you want it to be resent click on the '.
                                    'following link <br><br> :link-resend',
    
    'account-evlink-not-expired'  => 'The verification link we sent has not yet expired. New verification links '.
                                    'can only be resent after the previous one has expired.',
    
    'account-evlink-invalid'      => 'The verification link is invalid.',
    
    'account-prlink-expired'      => 'The password reset link has expired. You can have it resent by going to '.
                                    'the login page and clicking on Forgot Password <br><br> :link-resend',
    
    'account-prlink-not-expired'  => 'We have already sent a password reset email to the specified address. '.
                                    'Please check your inbox and follow the instructions in the email. If you '.
                                    'did not find the email, check whether it ended up in the junk/spam folder. '.
                                    'If you still can\'t find it, you can request a new password reset email '.
                                    'after a few hours.',
    
    'account-prlink-invalid'      => 'The password reset link is invalid.',
    
    'account-no-withdraw'         =>  'You are currently not allowed to withdraw funds. Please check your account '.
                                      'and identity verification status.',
    
    'account-delete-has-open-tasks' => 'The account cannot be delete as there are open duties. You can delete your '.
                                       'account once all the duties have been closed.',
    
    'account-delete-is-verifying' => 'Your account is currently under verification. You can delete your account '.
                                     'once the verification process is completed.',
    
    'user-already-favourite'      => 'This user is already in your favourites list',
    
    'user-already-ignored'        => 'This user is already in your ignore list',
    
    'user-not-of-legal-age'       => 'Sorry, you must be :legal-age or older to register with the platform.',
        
    'session-invalid'             => 'This session is no longer valid. Please login again or reload the page.',
    
    'task-cancel-request-pending' => 'There is already a pending cancellation request for this duty. If you have '.
                                     'sent the request, then wait for the other party to respond or for the request '.
                                     'to expire, after which you can cancel the duty without incurring any penalty. '.
                                     'If you have received the request, then you can just accept it to cancel the duty.',
    
    'task-cancel-request-expired' => 'The other party has not responded to your cancellation request within the allowed timeframe. '.
                                     'You can cancel the duty without incurring any penalty so long as the other party does not respond.',
    
    'task-end-cancel-request-pending' => 'There is a pending cancellation request for this duty. Please respond to the '.
                                         'request before ending the duty.',
    
    'task-charge-request-pending' => 'There is already a pending charge request for this duty. The customer must '.
                                     'respond to the request before further requests can be made.',
    
    'task-undisputable'           => 'This duty can no longer be disputed.',
    
    'task-bids-closed'            => 'This duty is no longer available.',
    
    'task-bid-already'            => 'You have already confirmed your availability for this duty.',
    
    'task-bid-expired'            => 'The offer for this duty has expired.',
    
    'task-ending'                 => 'Messages for this duty can no longer be posted as it is being ended.',
    
    

];
