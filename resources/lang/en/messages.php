<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by messages in various contexts.
    |
    */

    // RESPONSE MESSAGES
    
    'identity-info-received' => 'The information have been received. We will review it shortly '.
                                'and notify you of the outcome.',
    
    'account-password-resetting' => 'We have sent an email to the specified address with instructions on how '.
                                    'to reset the password. Shouldn\'t you find it, make sure it did not end up '.
                                    'in the spam/junk folder.',
    
    'account-password-reset' =>  'The password has been reset. You can now login using the new password.',
    
    'account-deleted'        => 'Your account has been closed.',
    
    'task-scheduled'         => 'Your request has been received and will be dispatched to an available assistant shortly. '.
                                '',
    
    'task-feedback-received' => 'Your feedback has been received. Thanks.',
    
    'task-bid-received'      => 'Your availability has been confirmed.',// and you are currently at position :position in the list.',
    
    'task-dispute-received'  => 'The dispute has been filed. Our team will examine the case and will get in touch '.
                                'with you if further info are needed.',
    
    'task-unavailable'       => 'This duty is no longer available.',

    'user-ignored' =>            'The user has been added to your ignores list',
    
    'user-favourited' =>         'The user has been added to your favourites list',
    
    'user-ignored' =>            'The user has been added to your ignore list',
    
    'user-favourited-was-ignored' => 'The user was in your ignore list. It has been removed from there and is now a favourite.',
    
    'user-ignored-was-favourited' => 'The user was in your favourites list. It has been removed from there and is now ignored.',
    

    // NOTIFICATION MESSAGES
    
    'task-bid'               => 'A <b>new duty</b> has been requested. Click to view and confirm your availability.',
    
    'task-bid-expired'       => 'The offer for duty <i>:task-subject</i> has expired.',
    
    'task-expired'           => 'There are currently no available assistants for the request <i>:task-subject</i>',
    
    'task-started'           => 'The duty <i>:task-subject</i> has been assigned and is now ongoing.',
    
    'task-cancelled'         => 'The duty <i>:task-subject</i> has been cancelled.',
    
    'task-completed'         => 'The duty <i>:task-subject</i> has been completed.',
    
    'task-ended'             => 'The duty <i>:task-subject</i> has ended.',
    
    'task-disputed'          => 'The duty <i>:task-subject</i> has been disputed.',
    
    'task-rescheduled'       => 'The duty <i>:task-subject</i> has been rescheduled.',
    
    'task-cancel-request-expired' => 'The request to cancel the duty <i>:task-subject</i> has not been responded to. '.
                                     'You can freely cancel the duty so long as the other party does not respond.',
    
    'task-cancel-request-accepted' => 'The request to cancel the duty <i>:task-subject</i> has been accepted.',
    
    'task-cancel-request-refused' => 'The request to cancel the duty <i>:task-subject</i> has been refused.',
    
    'task-end-request-accepted' => 'The request to end the duty <i>:task-subject</i> has been accepted.',
    
    'task-end-request-refused' => 'The request to end the duty <i>:task-subject</i> has been refused.',

    'task-end-request-expired' => 'The request to end the duty <i>:task-subject</i> has not been responded to '.
                                  'and the duty has been ended.',
    
    'task-charge-approved'   => 'The charge request for the duty <i>:task-subject</i> has been approved.',
    
    'task-charge-refused'    => 'The charge request for the duty <i>:task-subject</i> has been refused.',
    
    'task-dispute-resolved'  => 'The dispute for the duty <i>:task-subject</i> has been resolved.',
    
    'txn-created'            => 'A new :txn-type transaction has been created in your account.',

    'txn-charge-made'        => 'A charge of <b>:charge-amount</b> has been made for the duty <i>:task-subject</i>.',
    
    'txn-charge-fail-reattempt'  => 'The charge for <i>:subject</i> failed. We will try again later. '.
                                'In the meanwhile please try to fix the issue with your card or use a different payment '.
                                'method to avoid limitations on your account.',
    
    'txn-charge-fail'        => 'Several charge attempts for <i>:subject</i> failed. Please try to fix the issue or  '.
                                'contact us to avoid limitations on your account.',
    
    'txn-payment-received'   => 'A payment of <b>:charge-amount</b> has been received for the duty <i>:task-subject</i>.',
    
    'txn-withdrawal-deposited'  => 'The withdrawal of :txn-amount has been completed and founds shoould be in your bank account.',
    
    'txn-withdrawal-failed'  => 'The withdrawal of :txn-amount failed for the following reason: <i>:fail-reason</i>',
    
    'txn-refund-made'        => 'A refund of <b>:refund-amount</b> has been made for the duty <i>:task-subject</i>.',
    
    'identity-inquiry'       => 'The verification of your account requires some action.',
    
    'identity-unverified'    => 'Your identity could not be verified.',
    
    'identity-verified'      => 'Your identity has been verified.',
    
    'account-transfers-disabled' => 'Bank transfers are disabled. Information may be needed.',
    
    'account-charges-disabled' => 'Payments are disabled and your account has been temporarily suspended. '.
                                  '',

    // ALERT MESSAGES
    
    'email-unverified'        => 'Please verify your email address by clicking on the '.
                                 'activation link in the welcome email sent to :user-email. If you did not receive it, '.
                                 'click :link-email-resend to have it resent.',
    
    'email-verified'          => 'Your email is verified!',
    
    'email-verification-sent' => 'An activation email has been sent to :user-email.',
    
    'provider-verifying'      => 'Your account is under verification.',
    
    'provider-inquired'       => 'The verification of your account requires some action. Click :link-provider-settings '.
                                 'to reply.',
    
    'provider-unverified'     => 'Your identity could not be verified. Please contact us to resolve this issue.',
    
    'provider-not-active'     => 'Your account is not yet activated. Click :link-provider-settings to verify your account.',
    
    'customer-active'         => 'Your account is fully active!',
    
    
    // OTHER
    
    'chat-cancel-request'     => "This is a request for the cancellation of the duty in mutual agreement.\n\n:message",
    
    'chat-charge-request'     => "This is a charge request for the following services:\n\n".
                                 "amount: :amount\ndescription: :description\n\n:message",
    
    'chat-end-request'        => "This is a request to end the duty.",
    
    'chat-deliverables'       => "The duty has ended. You can download any deliverables attached to this message, ".
                                 "if any.\n\n:message",
    
    'task-dispute-on-end'     => 'The customer refused to end the duty.',
    
    'task-dispute-no-reason'  => 'Please provide details for disputing the duty <a href=":url">here</a>',

];
