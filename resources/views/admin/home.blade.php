@extends('layouts.admin')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent
    <script src="{{ URL::asset('js/admin/dashboard/home.js') }}"></script>
    
    <script>
        $(document).ready(function() {
        	App.init().run(new Dashboard);
        });
    </script>
@endsection


@section('content')
    <div class="container">
	
      <div class="portlet portlet-default">

        <div class="portlet-header">
          <h4 class="portlet-title">
            Monthly Stats
          </h4>
        </div> <!-- /.portlet-header -->

        <div class="portlet-body">

          <div class="row">

            <div class="col-sm-6 col-md-3">
              <div class="row-stat">
                <p class="row-stat-label">Revenue Today</p>
                <h3 class="row-stat-value">$890.00</h3>
                <span class="label label-success row-stat-badge">+43%</span>
              </div> <!-- /.row-stat -->
            </div> <!-- /.col -->

            <div class="col-sm-6 col-md-3">
              <div class="row-stat">
                <p class="row-stat-label">Revenue This Month</p>
                <h3 class="row-stat-value">$8290.00</h3>
                <span class="label label-success row-stat-badge">+17%</span>
              </div> <!-- /.row-stat -->
            </div> <!-- /.col -->

            <div class="col-sm-6 col-md-3">
              <div class="row-stat">
                <p class="row-stat-label">Total Users</p>
                <h3 class="row-stat-value">98,290</h3>
                <span class="label label-success row-stat-badge">+26%</span>
              </div> <!-- /.row-stat -->
            </div> <!-- /.col -->

            <div class="col-sm-6 col-md-3">
              <div class="row-stat">
                <p class="row-stat-label">Onboarding providers</p>
                <h3 class="row-stat-value">19</h3>
                <span class="label label-danger row-stat-badge">+5%</span>
              </div> <!-- /.row-stat -->
            </div> <!-- /.col -->
            
          </div> <!-- /.row -->

        </div> <!-- /.portlet-body -->

      </div>

    </div> <!-- /.container -->
@endsection
