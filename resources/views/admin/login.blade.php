@extends('layouts.login')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent
    <script src="{{ URL::asset('js/portal/login.js') }}"></script>
    <script src="{{ URL::asset('ext/parsley/js/parsley.min.js') }}"></script>
    
    <script type="text/javascript">
	    $(document).ready(function() {
	    	App.init().run(new Portal);
	    });
    </script>
@endsection


@section('content')
<div class="account-wrapper">

    <div class="account-body">

      <h3>Admin login.</h3>

      <form id="form-login"
            class="form account-form parsley-form"
            method="POST"
            action="{{ url($vw_admin_root.'/login') }}">
      
        <div class="form-group">
          <label for="login-username" class="placeholder-hidden">Username</label>
          <input type="email" class="form-control" id="login-username" placeholder="Username" name="username" tabindex="1"
                 data-parsley-required
                 data-parsley-required-message="Please provide your username."
                 data-parsley-type="email">
        </div> <!-- /.form-group -->

        <div class="form-group">
          <label for="login-password" class="placeholder-hidden">Password</label>
          <input type="password" class="form-control" id="login-password" placeholder="Password" name="password" tabindex="2"
                 data-parsley-required
                 data-parsley-required-message="Please provide the password.">
        </div> <!-- /.form-group -->

        <div class="form-group clearfix">
          <div class="pull-left">					
            <label class="checkbox-inline">
            <input type="checkbox" name="remember" tabindex="3">
            <small>Remember me</small>
            </label>
          </div>

          <div class="pull-right">
            <small><a href="#">Forgot Password?</a></small>
          </div>
        </div> <!-- /.form-group -->

        <div class="form-group">
          <button type="submit" class="btn btn-primary btn-block btn-lg" tabindex="4">Signin &nbsp; <i class="fa fa-play-circle"></i></button>
        </div> <!-- /.form-group -->

      </form>

    </div> <!-- /.account-body -->

    <div class="account-footer">
      <p>
      &copy; 2016 Tasko.com.
      </p>
    </div> <!-- /.account-footer -->

</div> <!-- /.account-wrapper -->
@endsection
