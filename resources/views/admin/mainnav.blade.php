  
  <div class="mainnav ">

    <div class="container">

      <a class="mainnav-toggle" data-toggle="collapse" data-target=".mainnav-collapse">
        <span class="sr-only">Toggle navigation</span>
        <i class="fa fa-bars"></i>
      </a>

      <nav class="collapse mainnav-collapse" role="navigation">

        <form class="mainnav-form" role="search">
          <input class="form-control input-md mainnav-search-query" placeholder="Search" type="text">
          <button class="btn btn-sm mainnav-form-btn"><i class="fa fa-search"></i></button>
        </form>

        <ul class="mainnav-menu">
          <li class="dropdown active is-open">
          	<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
          	  Provider
          	  <i class="mainnav-caret"></i>
          	</a>
          	<ul class="dropdown-menu" role="menu">
                    <li>
                      <a href="{{ URL::to('/') }}/{{ $vw_admin_root }}/provider/onboarding">
                        <i class="fa fa-dashboard dropdown-icon "></i> 
                        Onboarding
                      </a>
                    </li>
          	</ul>
          </li>
          <li class="dropdown active is-open">
          	<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
          	  Task
          	  <i class="mainnav-caret"></i>
          	</a>
          	<ul class="dropdown-menu" role="menu">
                    <li>
                      <a href="{{ URL::to('/') }}/{{ $vw_admin_root }}/disputes">
                        <i class="fa fa-legal dropdown-icon "></i> 
                        Disputes
                      </a>
                    </li>
          	</ul>
          </li>
        </ul>

      </nav>

    </div> <!-- /.container -->

  </div>
  