		     <?php
		            // Main admin account menu.
		     ?>
     
            <ul class="dropdown-menu" role="menu">
              <li>
                <a href="{{ URL::to('/') }}/{{ $vw_admin_root }}/settings">
                  <i class="fa fa-cogs"></i> 
                  &nbsp;&nbsp;Settings
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="{{ URL::to('/') }}/{{ $vw_admin_root }}/logout">
                  <i class="fa fa-sign-out"></i> 
                  &nbsp;&nbsp;Logout
                </a>
              </li>
            </ul>
