@extends('layouts.admin')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent
    <script src="{{ URL::asset('js/'.$vw_admin_root.'/dashboard/provider-onboarding.js') }}"></script>
    
    <script>
        $(document).ready(function() {
        	App.init().run(new Dashboard);
        });
    </script>
@endsection


@section('content')
    <div class="container">

	   <div class="row">
	      <div class="col-md-6 col-md-offset-3 task-view">
		  
            <!-- ONBOARDING PROVIDERS LIST VIEW -->

		    <div class="user-list-view">
			                              			    
                <div class="heading-block-2">
	               <i class="fa fa-user"></i><h5>Onboarding providers</h5>
				</div>

                @if ($vw_providers)
                <div class="list-group providers">
                  @foreach ($vw_providers as $provider)
					 <a id="{{ $provider['id'] }}" href="javascript:;" class="list-group-item" data-user-id="{{ $provider['id'] }}">
						<span class="label label-identity-status label-identity-{{ $provider['identity']['status'] }} pull-right">{{ $provider['identity']['status'] }}</span>
						<h4 class="list-group-item-heading">{{ $provider['first_name'].' '.$provider['last_name'] }}</h4>
						<small class="list-group-item-details"><i class="fa fa-clock-o"></i>&nbsp;{{ $provider['identity']['submission_datetime'] }}</small>
					 </a>
				  @endforeach
				</div>
				<a data-toggle="modal" href="#" class="btn btn-default demo-element">Load more</a>
				@else
				<div>No providers.</div>
				@endif

			</div>
			
			<!-- PROVIDER DETAILS VIEW -->
			
			@include('widgets.user-details')
						
			<div class="task-progress-view">
			   <img src="{{ URL::asset('img/loading.gif') }}" alt="Loading ...">
			</div>
			
		  </div>
	   </div>

    </div> <!-- /.container -->
@endsection
