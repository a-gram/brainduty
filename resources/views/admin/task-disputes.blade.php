@extends('layouts.admin')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent
    <script src="{{ URL::asset('js/widget/task.js') }}"></script>
    <script src="{{ URL::asset('js/'.$vw_admin_root.'/dashboard/task-disputes.js') }}"></script>
    
    <script>
        $(document).ready(function() {
          App.init().run(new Dashboard);
        });
    </script>
@endsection


@section('content')
    <div class="container">

        <div class="row">
           <div class="col-md-6 col-md-offset-3">
               
              <div class="task-view">
                  
                 <!-- TASKS LIST VIEW -->
                 <div class="content-pane task-list-view">
                    <div class="panel panel-default">
                       <div class="panel-heading">DISPUTED TASKS</div>
                       <!-- tasks list -->
                       <div class="list-group disputed tasks">
                          @foreach ($vw_disputes as $dispute)
                          <a id="{{ $dispute->task->ref }}" href="javascript:;" class="list-group-item" data-task-ref="{{ $dispute->task->ref }}">
                             <span class="label label-dispute-status label-dispute-{{ $dispute->status_to_string() }} pull-right">{{ $dispute->status_to_string() }}</span>
                             <h4 class="list-group-item-heading">{{ $dispute->reason_to_string() }}</h4>
                             <small class="list-group-item-details"><i class="fa fa-clock-o"></i>&nbsp;{{ $dispute->created_at }}</small>
                          </a>
                          @endforeach
                       </div>
                       <div class="panel-footer"></div>
                    </div>
                    <!-- No disputes pane -->
                    <div class="panel-empty tasks" style="display: none;">
                       <p>No disputes.</p>
                    </div>
                    <br>
                 </div>
                 
                 <!-- TASK DETAILS VIEW -->
                 @include('widgets.task-details')
                 
                 <div class="task-progress-view"><i class="fa fa-spinner fa-spin"></i></div>
                 
              </div>
               
           </div>
        </div>

    </div> <!-- /.container -->
@endsection
