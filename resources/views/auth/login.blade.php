@extends('layouts.login')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent
    <script src="{{ URL::asset('js/portal/login.js') }}"></script>
    <script src="{{ URL::asset('ext/parsley/js/parsley.min.js') }}"></script>
    
    <script type="text/javascript">
	    $(document).ready(function() {
	    	App.init().run(new Portal);
	    });
    </script>
    
@endsection


@section('content')
    <div class="account-body">
<?php /*
      <div class="row">		
        <div class="col-sm-6">
          <a href="javascript:;" class="btn btn-twitter btn-block">
          <i class="fa fa-twitter"></i>
          &nbsp;&nbsp;Login with Twitter
          </a>
        </div> <!-- /.col -->
        <div class="col-sm-6">
          <a href="javascript:;" class="btn btn-facebook btn-block">
          <i class="fa fa-facebook"></i>
          &nbsp;&nbsp;Login with Facebook
          </a>
        </div> <!-- /.col -->
      </div> <!-- /.row -->

      <span class="account-or-social text-muted"></span>
*/ ?>
      <h4>Log in to manage your duties</h4>
      
      <form id="form-login"
            class="form account-form parsley-form"
            method="POST"
            action="{{ url('user/login') }}">
      
        <div class="form-group">
          <label for="login-username" class="placeholder-hidden">Username</label>
          <div class="input-group">
              <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user fa-fw"></i></span>
              <input type="email" class="form-control" id="login-username" placeholder="Email" name="username" tabindex="1"
                     data-parsley-required
                     data-parsley-required-message="Please provide your username."
                     data-parsley-type="email">
          </div>
        </div> <!-- /.form-group -->

        <div class="form-group">
          <label for="login-password" class="placeholder-hidden">Password</label>
          <div class="input-group">
              <span class="input-group-addon" id="basic-addon2"><i class="fa fa-lock fa-fw"></i></span>
              <input type="password" class="form-control" id="login-password" placeholder="Password" name="password" tabindex="2"
                     data-parsley-required
                     data-parsley-required-message="Please provide the password.">
          </div>
        </div> <!-- /.form-group -->

        <div class="form-group clearfix">
          <div class="pull-left">					
            <label class="checkbox-inline">
            <input type="checkbox" name="remember" tabindex="3"> <small>Remember me</small>
            </label>
          </div>

          <div class="pull-right">
            <small id="forgot-password"><a href="javascript:;">FORGOT PASSWORD&nbsp;</a><i class="fa fa-spinner fa-pulse loading hidden"></i></small>
          </div>
        </div> <!-- /.form-group -->

        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block btn-lg signin" tabindex="4">Signin<i class="fa fa-spinner fa-pulse loading"></i></button>
        </div> <!-- /.form-group -->

      </form>

    </div> <!-- /.account-body -->
@endsection
