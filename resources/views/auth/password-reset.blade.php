@extends('layouts.login')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent
    <script src="{{ URL::asset('js/portal/pwreset.js') }}"></script>
    <script src="{{ URL::asset('ext/parsley/js/parsley.min.js') }}"></script>
    
    <script type="text/javascript">
	    $(document).ready(function() {
	    	App.init().run(new Portal);
	    });
    </script>
    
@endsection


@section('content')
    <div class="account-body">
      
      <h4>Password reset</h4>
      
      <form id="form-password-reset"
            class="form account-form parsley-form"
            method="POST"
            action="{{ url('user/password/reset') }}">
      
        <div class="form-group">
          <label for="reset-password" class="placeholder-hidden">New Password</label>
          <input type="password" class="form-control" id="reset-password" placeholder="New password" name="password" tabindex="1"
                 data-parsley-required
                 data-parsley-pattern="[a-zA-Z0-9!?@#$%&(){}]{8,64}"
                 data-parsley-pattern-message="Invalid password.">
        </div> <!-- /.form-group -->

        <div class="form-group">
          <label for="reset-password-confirm" class="placeholder-hidden">Confirm New Password</label>
          <input type="password" class="form-control" id="reset-password-confirm" placeholder="Confirm new password" name="password-confirm" tabindex="2"
                 data-parsley-required
                 data-parsley-equalto="#reset-password"
                 data-parsley-equalto-message="This does not match the new password.">
        </div> <!-- /.form-group -->
        
        <input type="hidden" name="token" value="{{ $vw_pr_token }}">

        <div class="form-group">
            <button type="submit" class="btn btn-success btn-block btn-lg submit" tabindex="4">Reset<i class="fa fa-spinner fa-pulse loading"></i></button>
        </div> <!-- /.form-group -->

      </form>

    </div> <!-- /.account-body -->
@endsection
