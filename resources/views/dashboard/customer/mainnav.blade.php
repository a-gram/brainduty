
  <div class="mainnav ">
    <div class="container">
      <a class="mainnav-toggle" data-toggle="collapse" data-target=".mainnav-collapse">
        <span class="sr-only">Toggle navigation</span>
        <i class="fa fa-bars"></i>
      </a>
      <nav class="collapse mainnav-collapse" role="navigation">
        <ul class="mainnav-menu">
          <li class="dropdown ">
            <a href="{{ URL::to('customer/home') }}" class="">
               <i class="fa fa-home fa-fw"></i>
               <span class="visible-xs-inline">&nbsp;Home</span>
            </a>
          </li>
          <li class="dropdown ">
            <a href="{{ URL::to('customer/tasks/closed') }}" class="">
               <i class="fa fa-folder-open fa-fw"></i>
               <span class="visible-xs-inline">&nbsp;Closed duties</span>
            </a>
          </li>
          <li class="dropdown ">
            <a href="{{ URL::to('task/request') }}" class="highlight">
               <i class="fa fa-calendar fa-fw"></i>
               <span class="visible-xs-inline">&nbsp;Request a duty</span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
