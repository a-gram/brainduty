@extends('layouts.dashboard')

@section('styles')
    @parent
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('javascripts')
    @parent
    <script src="{{ URL::asset('js/dashboard/customer/settings.js') }}"></script>
    <script src="{{ URL::asset('ext/parsley/js/parsley.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://js.stripe.com/v2/"></script>
    @include('widgets.agchat-include')

    <script type="text/javascript">
        App.env.@if ($vw_customer->default_payment_method())
                default_payment_method = '{{ $vw_customer->default_payment_method()->method }}';
                @else
                default_payment_method = null;
                @endif
                
        App.env.market_pkey = '{{ $vw_market_pkey }}';
        
        $(document).ready(function() {
            App.init().run(new Dashboard);
        });
    </script>
@endsection

@section('content')
<div class="container">
   <div class="layout layout-main-right layout-stack-sm">
      <div class="col-md-3 col-sm-4 layout-sidebar">
         <div class="nav-layout-sidebar-skip">
            <strong>Tab Navigation</strong> / <a href="#settings-content">Skip to Content</a>	
         </div>
         <ul id="settings-tabs" class="nav nav-layout-sidebar nav-stacked">
            <li class="active">
               <a href="#profile-settings" data-toggle="tab">
               <i class="fa fa-user"></i> 
               &nbsp;&nbsp;Profile
               </a>
            </li>
            <li>
               <a href="#security-settings" data-toggle="tab">
               <i class="fa fa-lock"></i> 
               &nbsp;&nbsp;Password
               </a>
            </li>
            <li>
               <a href="#payment-settings" data-toggle="tab">
               <i class="fa fa-dollar"></i> 
               &nbsp;&nbsp;Payment
               </a>
            </li>
            <li>
               <a href="#account-settings" data-toggle="tab">
               <i class="fa fa-cogs"></i> 
               &nbsp;&nbsp;Account
               </a>
            </li>
         </ul>
      </div>
      <div class="col-md-9 col-sm-8 layout-main">
         <div id="settings-content" class="tab-content stacked-content">
             
            <!-- PROFILE SETTINGS -->
            
            <div class="tab-pane fade in active" id="profile-settings">
               <div class="heading-block">
                  <h3>Edit Profile</h3>
               </div>
               <form class="form-horizontal parsley-form">
                   
                  <div class="form-group">
                     <label class="col-md-3 control-label"></label>
                     <div class="col-md-7">
                        <div class="avatar">
                          <div class="images">
                            <img class="img thumbnail" src="{{ $vw_customer->get_shared_file('avatar','url') }}" alt="Avatar">
                          </div>
                          <div class="command-buttons">
                               <span class="btn btn-default fileinput-button change">
                                   <i class="fa fa-pencil fa-fw"></i>
                                   <input name="files[]" type="file" multiple>
                               </span>
                               <span class="btn btn-primary remove" style="display:none;">
                                   <i class="fa fa-times fa-fw"></i>
                               </span>
                          </div>
                        </div>
                     </div>
                  </div>
              

                  <div class="form-group">
                     <label class="col-md-3 control-label">Username</label>
                     <div class="col-md-7">
                        <input type="text" name="user-name" value="{{ $vw_customer->username }}"
                           class="form-control"
                           disabled>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label">First Name</label>
                     <div class="col-md-7">
                        <input type="text" id="first-name" value="{{ $vw_customer->first_name }}"
                           class="form-control"
                           disabled>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label">Last Name</label>
                     <div class="col-md-7">
                        <input type="text" id="last-name" value="{{ $vw_customer->last_name }}"
                           class="form-control" 
                           disabled>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label">Mobile number</label>
                     <div class="col-md-7">
                        <input type="text" id="mobile-number" value="{{ $vw_customer->mobile }}"
                           class="form-control"
                           data-parsley-pattern="\d{10,12}"
                           data-parsley-pattern-message="Please check the phone number."
                           data-parsley-group="profile-info">
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="col-md-7 col-md-push-3">
                        <button type="button" class="btn btn-primary save">Save Changes<i class="fa fa-spinner fa-pulse loading"></i></button>
                     </div>
                  </div>
               </form>
            </div>
            
            <!-- SECURITY SETTINGS -->
            
            <div class="tab-pane fade" id="security-settings">
               <div class="heading-block">
                  <h3>Change Password</h3>
               </div>
               <form class="form-horizontal parsley-form">
                  <div class="form-group">
                     <label class="col-md-3 control-label">Old Password</label>
                     <div class="col-md-7">
                        <input type="password" id="old-password"
                           class="form-control"
                           data-parsley-required>
                     </div>
                  </div>
                  <hr>
                  <div class="form-group">
                     <label class="col-md-3 control-label">New Password</label>
                     <div class="col-md-7">
                        <input type="password" id="password"
                           class="form-control"
                           data-parsley-required
                           data-parsley-pattern="[a-zA-Z0-9!?@#$%&(){}]{8,64}"
                           data-parsley-pattern-message="Invalid password.">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label">New Password Confirm</label>
                     <div class="col-md-7">
                        <input type="password" id="password-confirm"
                           class="form-control"
                           data-parsley-required
                           data-parsley-equalto="#password"
                           data-parsley-equalto-message="This does not match the new password.">
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="col-md-7 col-md-push-3">
                        <button type="button" class="btn btn-primary save">Save Changes<i class="fa fa-spinner fa-pulse loading"></i></button>
                     </div>
                  </div>
               </form>
            </div>
            
            <!-- PAYMENT SETTINGS -->
            
            <div class="tab-pane fade" id="payment-settings">
               <div class="heading-block">
                  <h3>Payment Settings</h3>
               </div>
               <form class="form-horizontal parsley-form">
                   
                  <!-- Billing details -->
                  
                  <div class="billing-details">
                     <div class="form-group">
                        <label class="col-md-3 control-label">Address</label>
                        <div class="col-md-7">
                           <input type="text" id="address" value="{{ $vw_customer->address }}"
                                  class="form-control"
                                  data-parsley-required
                                  data-parsley-pattern="[a-zA-Z0-9 \/'.\-&]{1,64}"
                                  data-parsley-pattern-message="Addresses can only contain letters, numbers and the symbols /'.-&"
                                  data-parsley-group="billing-details">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-3 control-label">Zip code</label>
                        <div class="col-md-7">
                           <input type="text" id="zip-code" value="{{ $vw_customer->zip_code }}"
                                  class="form-control"
                                  data-parsley-required
                                  data-parsley-pattern="\d{4}"
                                  data-parsley-pattern-message="Invalid zip code."
                                  data-parsley-group="billing-details">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-3 control-label">City</label>
                        <div class="col-md-7">
                           <input type="text" id="city" value="{{ $vw_customer->city }}"
                                  class="form-control"
                                  data-parsley-required
                                  data-parsley-pattern="[a-zA-Z0-9 \/'.\-&]{1,64}"
                                  data-parsley-pattern-message="Names can only contain letters, numbers and the symbols /'.-&"
                                  data-parsley-group="billing-details">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-3 control-label">State</label>
                        <div class="col-md-7">
                           <select id="state" class="form-control"
                                   data-parsley-required
                                   data-parsley-group="billing-details">
                           <option value="NSW" {{ $vw_customer->state == 'NSW' ? 'selected' : '' }}>New South Wales</option>
                           <option value="NT" {{ $vw_customer->state == 'NT' ? 'selected' : '' }}>Northern Territory</option>
                           <option value="QLD" {{ $vw_customer->state == 'QLD' ? 'selected' : '' }}>Queensland</option>
                           <option value="WA" {{ $vw_customer->state == 'WA' ? 'selected' : '' }}>Western Australia</option>
                           <option value="SA" {{ $vw_customer->state == 'SA' ? 'selected' : '' }}>South Australia</option>
                           <option value="TAS" {{ $vw_customer->state == 'TAS' ? 'selected' : '' }}>Tasmania</option>
                           <option value="VIC" {{ $vw_customer->state == 'VIC' ? 'selected' : '' }}>Victoria</option>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-3 control-label">Country</label>
                        <div class="col-md-7">
                           <input type="text" id="country" value="{{ $vw_customer->country }}"
                                  class="form-control"
                                  data-parsley-group="billing-details"
                                  disabled>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-7 col-md-push-3">
                           <button type="button" class="btn btn-primary save">Save Changes<i class="fa fa-spinner fa-pulse loading"></i></button>
                        </div>
                     </div>
                  </div>
                  
                  <hr>
                  
                  <div class="reassurance">
                     <i class="fa fa-lock"></i>
                     <p>
                        Your details are transmitted over a secure connection. Card info are
                        sent to our payment processor and not stored in our records. To learn more, see our 
                        <a href="{{ url('privacy') }}">privacy policy</a>.
                     </p>
                  </div>
                  
                  <!-- Default payment details -->
                  
                  <div class="default-payment" style="display:none;">
                     <div class="form-group">
                        <div class="col-md-7 col-md-push-3">
                           <div class="well">
                              <h4>Your default payment method is set to
                                 <span id="method" class="text-primary">
                                 {{ $vw_customer->default_payment_method() ?
                                 $vw_customer->default_payment_method()->method : 'N/A' }}
                                 </span>
                              </h4>
                              <p>The following source of funds will be charged when payments are made.</p>
                              <table class="table">
                                 <tbody>
                                    <tr>
                                       <th>Type</th>
                                       <td id="type">{{ $vw_customer->default_payment_method() ?
                                          $vw_customer->default_payment_method()->source_type : 'N/A' }}
                                       </td>
                                    </tr>
                                    <tr>
                                       <th>Number</th>
                                       <td id="number">{{ $vw_customer->default_payment_method() ?
                                          $vw_customer->default_payment_method()->source_number : 'N/A' }}
                                       </td>
                                    </tr>
                                    <tr>
                                       <th>Expires</th>
                                       <td id="expiry">{{ $vw_customer->default_payment_method() ?
                                          $vw_customer->default_payment_method()->source_expiry : 'N/A' }}
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                              <div><button type="button" class="btn btn-secondary btn-sm change">Change</button></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  
                  <!-- Card details -->
                  
                  <div class="payment-details" style="display:none;">
                    <div class="col-md-7 col-md-push-3">
                       <div class="row">
                          <div class="col-md-12">
                             <div class="form-group">
                                <label for="card-number">Card number</label>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-credit-card"></i></span>
                                    <input type="text" id="card-number" class="form-control"
                                           maxlength="25"
                                           data-parsley-required
                                           data-parsley-required-message="Card number missing. You can use spaces to separate digit blocks (eg.: 1111 2222 3333 4444)."
                                           data-parsley-group="card-details">
                                </div>
                             </div>
                          </div>
                       </div>
                       <div class="row">
                          <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                             <div class="form-group month">
                                <label for="card-expiry-month">Expiry month</label>
                                <select id="card-expiry-month" class="form-control"
                                        data-parsley-required
                                        data-parsley-group="card-details">
                                    @for ($i = 1; $i <= 12; $i++)
                                      <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                             </div>
                          </div>
                          <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                             <div class="form-group year">
                                <label for="card-expiry-year">Expiry year</label>
                                <select id="card-expiry-year" class="form-control"
                                        data-parsley-required
                                        data-parsley-group="card-details">
                                    @for ($i = date('Y'); $i <= date('Y') + 20; $i++)
                                      <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                             </div>
                          </div>
                          <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                             <div class="form-group">
                                <label for="card-cvv">CVV</label>
                                <input type="text" id="card-cvv" class="form-control"
                                   maxlength="4"
                                   data-parsley-required
                                   data-parsley-group="card-details">
                             </div>
                          </div>
                       </div>
                       <div class="row">
                          <div class="col-sm-5 col-md-5 col-lg-5">
                            <div class="form-group">
                              <button type="button" class="btn btn-success submit">Submit<i class="fa fa-spinner fa-pulse loading"></i></button>
                            </div>                               
                          </div>
                       </div>
                    </div>
                  </div>
                  
               </form>
            </div>
            
            <!-- ACCOUNT SETTINGS -->
            
            <div class="tab-pane fade" id="account-settings">
               <div class="heading-block">
                  <h3>Account settings</h3>
               </div>
               <p>No settings available.</p>
               <form class="form-horizontal parsley-form">
               <?php /*    
                <div class="account-details">
                  <div class="form-group">
                    <label class="col-md-3 control-label">Duty assignment criteria</label>
                    <div class="col-md-7">
                        <p>When assigning my duties, choose the assistant that:</p>
                        <fieldset class="radiogroup assignment-policy">
                        <ul>
                          <li><input type="radio" id="ap-first" name="assign_policy" value="{{ \App\K::TASK_ASSIGN_FIRST_BIDDER }}"><label for="ap-first">Is first available</label></li>
                          <li><input type="radio" id="ap-best" name="assign_policy" value="{{ \App\K::TASK_ASSIGN_BEST_BIDDER }}"><label for="ap-first">Has more than 3-star feedback</label></li>
                        </ul>
                        </fieldset>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-7 col-md-push-3">
                      <button type="button" class="btn btn-primary save">Save Changes<i class="fa fa-spinner fa-pulse loading"></i></button>
                    </div>
                  </div>
                </div>
                */ ?>
               </form>
            </div>
            
         </div>
         <!-- /.tab-content -->
      </div>
      <!-- /.col -->
   </div>
   <!-- /.row -->
</div>
<!-- /.container -->
@endsection
