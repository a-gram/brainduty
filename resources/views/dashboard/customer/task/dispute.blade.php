@extends('layouts.dashboard')

@section('styles')
    @parent
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('javascripts')
    @parent
    <script src="{{ URL::asset('js/dashboard/customer/dispute.js') }}"></script>
    <script src="{{ URL::asset('ext/parsley/js/parsley.min.js') }}"></script>
    @include('widgets.agchat-include')
    
    <script type="text/javascript">
        $(document).ready(function() {
        	App.init().run(new Dashboard);
        });
    </script>
@endsection


@section('content')
    <div class="container">
	
	   <div class="row">
	      <div class="col-md-6 col-md-offset-3">
	      
	      <!-- START WIZARD -->
	      
	      <div id="process-wizard">
	      
		    <form id="task-dispute-form"
		          action="{{ URL::to('/task').'/'.$vw_task->ref.'/dispute' }}"
		          method="post"
		          enctype="multipart/form-data"
		          class="form parsley-form">
			
			<!-- TASK DISPUTE DETAILS FRAME -->
			
			<div id="wizard-step-1" class="content-pane wizard-page">
				<div class="wizard-page-content">
					<p>
                                            Tell us why you want to dispute the duty <strong>{{ $vw_task->subject }}</strong>.
                                            Please provide a detailed description including, if necessary, any
                                            document supporting your claims in order to facilitate the investigation
                                            of the case.
                                        </p>
                                        <br class="xs-20">
					
					<div class="frame-content">
					    <div class="form-group">
							<select class="reasons form-control" autocomplete="off">
                                                            <option value="" selected>Select a reason ...</option>
                                                            @foreach ($vw_dispute_reasons as $reason => $name)
                                                            <option value="{{ $reason }}">{{ $name }}</option>
                                                            @endforeach
							</select>
                            <input type="hidden" name="reason" value=""
						           data-parsley-required
							       data-parsley-required-message="Please select a dispute reason."
							       data-parsley-group="step-1" />
						</div>
						<div class="form-group" id="dispute-message-box">
							<div class="message-box clearfix">
							
							    <textarea id="dispute-message"
							              class="form-control message-box-textarea message-box-body"
							              rows="10"
							              placeholder="Type your message here ..."
							              tabindex="2"
							              data-parsley-required
							              data-parsley-required-message="The task dispute description is required."
							              data-parsley-maxlength="500"
							              data-parsley-maxlength-message="The task dispute description can be 500 characters long maximum."
							              data-parsley-errors-container="#dispute-message-box"
							              data-parsley-group="step-1">
							    </textarea>
							 <div class="message-box-attachments"></div>
							 <div class="message-box-actions">
								<div class="message-box-types pull-left">
								  <a data-original-title="Attach a file" class="fa fa-paperclip ui-tooltip fileinput-button btn-upload" title="">
									 <input type="file" name="files[]" multiple>
								  </a>
								</div>
								<div class="btn-group pull-right"></div>					
							 </div>
							</div>
						</div>
					</div>
					
				</div>
				<div class="command-buttons">
                                        <button type="button" class="btn btn-success submit" data-step="1">
                                        Submit <i class="fa fa-spinner fa-pulse loading"></i>
                                        </button>
				</div>
			</div>
			
            </form>
          </div>
          
          <!-- END WIZARD -->
          
          <div id="wizard-step-2" class="content-pane wizard-page" style="display:none;">
              <div class="wizard-page-content">
               Dispute submitted.
              </div>
          </div>
          
		  </div>
	   </div>

    </div> <!-- /.container -->
@endsection
