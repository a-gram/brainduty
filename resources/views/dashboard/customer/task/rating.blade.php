@extends('layouts.dashboard')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent
    <script src="{{ URL::asset('js/dashboard/customer/rating.js') }}"></script>
    <script src="{{ URL::asset('ext/parsley/js/parsley.min.js') }}"></script>
    
    <script type="text/javascript">
        $(document).ready(function() {
        	App.init().run(new Dashboard);
        });
    </script>
@endsection


@section('content')
    <div class="container">
	
	   <div class="row">
	      <div class="col-md-6 col-md-offset-3">
	      
	      <!-- START WIZARD -->
	      
	      <div id="process-wizard">
	      
		    <form id="task-rating-form"
		          action="{{ URL::to('/task').'/'.$vw_task->ref.'/rate' }}"
		          method="post"
		          enctype="multipart/form-data"
		          class="form parsley-form">
			
			<!-- TASK RATING DETAILS FRAME -->
			
			<div id="wizard-step-1" class="content-pane wizard-page">
				<div class="wizard-page-content">
					<p>
                                            How was your experience with the duty <strong>{{ $vw_task->subject }}</strong> ?
                                        </p>
                                        <br class="xs-20">
					<div class="frame-content">
					    <div class="form-group" id="rating-score">
					        <div class="btn-group">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        The experience was ...&nbsp;<span class="caret"></span>
                                                    </button>
						       <ul class="dropdown-menu" role="menu">
						          @foreach ($vw_rating_scores as $score => $rating)
							      <li><a href="javascript:;" data-score="{{ $score }}">{!! $rating !!}</a></li>
							      @endforeach
						       </ul>
						    </div>
						    <div class="rating-score-selected"></div>
                            <input type="hidden" name="score" value=""
						           data-parsley-required
							       data-parsley-required-message="Please give a rating."
							       data-parsley-group="step-1" />
						</div>
						<div class="form-group" id="rating-message-box">
						    <textarea id="rating-message"
						              class="form-control message-box-textarea message-box-body"
						              rows="10"
						              placeholder="Type your message here ..."
						              tabindex="2"
						              data-parsley-required
						              data-parsley-required-message="The rating description is required."
						              data-parsley-maxlength="300"
						              data-parsley-maxlength-message="The rating description can be 300 characters long maximum."
						              data-parsley-errors-container="#rating-message-box"
						              data-parsley-group="step-1">
						    </textarea>
						</div>
					</div>
					
				</div>
				<div class="command-buttons">
                                        <button type="button" class="btn btn-success submit" data-step="1">
                                        Submit <i class="fa fa-spinner fa-pulse loading"></i>
                                        </button>
				</div>
			</div>
			
            </form>
          </div>
          
          <!-- END WIZARD -->
          
          <div id="wizard-step-2" class="content-pane wizard-page" style="display:none;">
              <div class="wizard-page-content">
               Rating submitted.
              </div>
          </div>
          
		  </div>
	   </div>

    </div> <!-- /.container -->
@endsection
