@extends('layouts.dashboard')

@section('styles')
    @parent
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('javascripts')
    @parent
    <script src="{{ URL::asset('js/widget/aglist.js') }}"></script>
    <script src="{{ URL::asset('js/dashboard/customer/booking.js') }}"></script>
    <script src="{{ URL::asset('ext/parsley/js/parsley.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://js.stripe.com/v2/"></script>
    @include('widgets.agchat-include')
    
    <script type="text/javascript">
        
        App.env.market_pkey = '{{ $vw_market_pkey }}';
        
        $(document).ready(function() {
            App.init().run(new Dashboard);
        });
    </script>
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
        
      <!-- START WIZARD -->
      
      <div id="process-wizard">
        <form id="task-request-form"
          action="{{ URL::to('/task') }}"
          method="post"
          enctype="multipart/form-data"
          class="form parsley-form">
            
          <!-- TASK SERVICES FRAME -->
          
          <div id="wizard-step-1" class="wizard-page">
            <div class="wizard-page-content">
              <p>
                  What kind of assistance do you need ?
              </p>
              <br class="xs-20">
              <div class="list-group ag-list single-select services">
                  @foreach ($vw_services as $service)
                  @if ($service['is_active'])
                  <div class="list-group-item" data-services-id="{{ $service['id'] }}">
                    <div class="item-icon pull-left">
                      <i class="fa {{ $service['id'] == 1 ? 'fa-file-text' : 'fa-briefcase' }}"></i>
                    </div>
                    <div class="item-body">
                      <div class="item-heading">
                        <h3 class="item-name pull-left">{{ $service['name'] }}</h3>
                        <div class="item-price pull-right">
                          @if ($service['id'] == 1)
                            @if (isset($service['price_estimate']))
                            <h4 class="amount">{{ App\Hey::to_currency($service['price_estimate']['min']) }}-
                                               {{ App\Hey::to_currency($service['price_estimate']['max']) }}</h4>
                            @else
                            <h4 class="amount">{{ App\Hey::to_currency($service['price']) }}</h4>
                            @endif
                          @else
                            <h4 class="amount-old">AUD 25.00</h4>
                            <h4 class="amount">Select category</h4>
                          @endif
                          <h5 class="duration">{{ $service['duration'] }} min slot</h5>
                        </div>
                      </div>
                      <p>{{ $service['description'] }}</p>
                      @if (count($service['categories']))
                      <div class="form-group" id="service-{{ $service['id'] }}-categories">
                        <select class="ui-select form-control categories"
                                placeholder="Select a category..."
                                data-parsley-required
                                data-parsley-required-message="Please select a category.">
                          <option value="" data-price="" selected="selected">Select a category...</option>
                          @foreach ($service['categories'] as $id => $category)
                          <option value="{{ $id }}" data-price="{{ $category['price'] }}">{{ $category['name'] }}</option>
                          @endforeach
                        </select>
                      </div>
                      @endif
                    </div>
                  </div>
                  @endif
                  @endforeach
              </div>
              <div class="form-group">
                  <input type="hidden" name="task_type" value=""
                    data-parsley-required
                    data-parsley-required-message="Please select the type of duty."
                    data-parsley-group="step-1">
              </div>
            </div>
            <div class="command-buttons">
               <button type="button" class="btn next btn-primary" data-step="1">
               Next <i class="fa fa-chevron-right"></i>
               </button>
            </div>
          </div>
          
          <!-- TASK DETAILS FRAME -->
          
          <div id="wizard-step-2" class="wizard-page" style="display:none;">
            <div class="wizard-page-content">
              <br class="xs-20">
              
              
              <div class="form-group">
                  <label>
                      Provide a short title and a detailed description for the duty. 
                      Documents and files can be attached to the message by clicking on the <i class="fa fa-paperclip"></i> icon.
                  </label>
                  <input id="task-subject" type="text" class="form-control"
                    placeholder="Type a short title here ..."
                    tabindex="1"
                    data-parsley-required-message="The task title is required."
                    data-parsley-required
                    data-parsley-maxlength="100"
                    data-parsley-pattern="[\w ,.:;!?'()%&$]{1,100}"
                    data-parsley-pattern-message="A title is required. It can only contain text with punctuation and be max 100 characters long."
                    data-parsley-group="step-2">
              </div>
              <div class="form-group" id="task-message-box">
                  <div class="message-box clearfix">
                    <textarea id="task-message" class="form-control message-box-textarea message-box-body"
                      rows="10"
                      placeholder="Type a detailed description here ..."
                      tabindex="2"
                      data-parsley-required
                      data-parsley-required-message="The description is required."
                      data-parsley-maxlength="1000"
                      data-parsley-maxlength-message="The description can be 1000 characters long maximum."
                      data-parsley-errors-container="#task-message-box"
                      data-parsley-group="step-2">
                    </textarea>
                    <div class="message-box-attachments"></div>
                    <div class="message-box-actions">
                      <div class="message-box-types pull-left">
                        <a class="fa fa-paperclip fileinput-button btn-upload" title="">
                           <input id="fileupload" type="file" name="files[]" multiple>
                        </a>
                      </div>
                    </div>
                  </div>
              </div>
              
              @if (!empty($vw_favourites))
              <div class="form-group">
                  <label>
                      You can indicate to assign the duty to one of the assistants in your favourites
                      list. However, keep in mind that we cannot guarantee that the asistant will be able to take on the
                      duty.
                  </label>
                  <select id="task-favourite" class="form-control">
                    <option value="" selected="selected">Select a name...</option>
                    @foreach ($vw_favourites as $provider)
                    <option value="{{ $provider['ref'] }}">{{ $provider['first_name'].' '.$provider['last_name'] }}</option>
                    @endforeach
                  </select>
              </div>
              @endif
              
            </div>
            <div class="command-buttons">
               <button type="button" class="btn back btn-default" data-step="2">
               <i class="fa fa-chevron-left"></i> Back
               </button>
               <button type="button" class="btn next btn-primary" data-step="2">
               Next <i class="fa fa-chevron-right"></i>
               </button>
            </div>
          </div>
          
          <!-- PAYMENT DETAILS AND SUBMIT FRAME -->
          
          <?php /* If the customer has no payment method registered then include the
            registration form in the page, otherwise it is not needed.*/ ?>
          
          <div id="wizard-step-3" class="wizard-page" style="display:none;">
              
            @if (empty($vw_customer->default_payment_method()))
            
            <div id="customer-payment-details" class="wizard-page-content">
              <p>
                  In order to submit a request you need to provide a valid 
                  payment method and your billing info. You will only be charged
                  if your duty is assigned and completed by an assistant.
              </p>
                              
              <!-- Payment method details -->
              
              <div class="heading-block-style underlined">
                 <i class="fa fa-dollar"></i>
                 <h5>Payment method</h5>
              </div>

              <div class="row payment-method-details">
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                      <fieldset class="radiogroup">
                        <input type="radio" id="payment-method" name="payment-method" value="card"
                               data-parsley-required 
                               data-parsley-required-message="Please select a payment method"
                               data-parsley-group="step-3">
                        <label for="payment-method">Credit/Debit Card</label>
                      </fieldset>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="row card-details">
                      <div class="col-md-12">
                         <div class="row">
                            <div class="col-md-12">
                               <div class="form-group">
                                  <label for="card-number">Card number</label>
                                  <div class="input-group">
                                     <span class="input-group-addon" id="basic-addon1"><i class="fa fa-credit-card"></i></span>
                                     <input type="text" id="card-number" class="form-control"
                                            maxlength="25"
                                            data-parsley-required
                                            data-parsley-required-message="Card number missing. You can use spaces to separate digit blocks (eg.: 4111 2222 3333 4444)."
                                            data-parsley-group="step-3">
                                  </div>
                               </div>
                            </div>
                         </div>
                         <div class="row">
                            <div class="col-sm-4 col-md-4 col-lg-4">
                               <div class="form-group month">
                                  <label for="card-expiry-month">Expiry (M)</label>
                                  <select id="card-expiry-month" class="form-control"
                                          data-parsley-required
                                          data-parsley-group="step-3">
                                      @for ($i = 1; $i <= 12; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                      @endfor
                                  </select>
                               </div>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4">
                               <div class="form-group year">
                                  <label for="card-expiry-year">Expiry (Y)</label>
                                  <select id="card-expiry-year" class="form-control"
                                          data-parsley-required
                                          data-parsley-group="step-3">
                                      @for ($i = date('Y'); $i <= date('Y') + 20; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                      @endfor
                                  </select>
                               </div>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4">
                               <div class="form-group">
                                  <label for="card-cvv">CVV</label>
                                  <input type="text" id="card-cvv" class="form-control"
                                     maxlength="4"
                                     data-parsley-required
                                     data-parsley-group="step-3">
                               </div>
                            </div>
                         </div>
                      </div>
                    </div>
                    
                    <?php /*
                    <div class="row paypal-details" style="display:none;">
                        // Here go the paypal payment details, if any
                    </div>
                     */ ?>
                    
                </div>
              </div> <!-- Payment method details -->
			  
              <br class="xs-50">

              <!-- Customer details -->
              
              <div class="heading-block-style underlined">
                <i class="fa fa-info"></i>
                <h5>Billing info</h5>
              </div>
              
              <div class="row customer-details">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                      <div class="form-group">
                        <label for="first-name">First Name</label>
                        <input type="text" id="first-name" class="form-control"
                          value="{{ $vw_customer['first_name'] }}"
                          disabled>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-6">
                      <div class="form-group">
                        <label for="last-name">Last Name</label>
                        <input type="text" id="last-name" class="form-control"
                          value="{{ $vw_customer['last_name'] }}"
                          disabled>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" id="address" class="form-control"
                          value="{{ $vw_customer['address'] }}"
                          data-parsley-required
                          data-parsley-group="step-3">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                      <div class="form-group">
                        <label for="zip-code">Zip code</label>
                        <input type="text" id="zip-code" class="form-control"
                          value="{{ $vw_customer['zip_code'] }}"
                          data-parsley-required
                          data-parsley-group="step-3">
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-6">
                      <div class="form-group">
                        <label for="city">City</label>
                        <input type="text" id="city" class="form-control"
                          value="{{ $vw_customer['city'] }}"
                          data-parsley-required
                          data-parsley-group="step-3">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="state">State</label>
                        <select id="state" class="form-control"
                          data-parsley-required
                          data-parsley-group="step-3">
                          <option value="NSW" {{ $vw_customer['state'] == 'NSW' ? 'selected' : '' }}>New South Wales</option>
                          <option value="NT" {{ $vw_customer['state'] == 'NT' ? 'selected' : '' }}>Northern Territory</option>
                          <option value="QLD" {{ $vw_customer['state'] == 'QLD' ? 'selected' : '' }}>Queensland</option>
                          <option value="WA" {{ $vw_customer['state'] == 'WA' ? 'selected' : '' }}>Western Australia</option>
                          <option value="SA" {{ $vw_customer['state'] == 'SA' ? 'selected' : '' }}>South Australia</option>
                          <option value="TAS" {{ $vw_customer['state'] == 'TAS' ? 'selected' : '' }}>Tasmania</option>
                          <option value="VIC" {{ $vw_customer['state'] == 'VIC' ? 'selected' : '' }}>Victoria</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="country">Country</label>
                        <input type="text" id="country" class="form-control" 
                          value="{{ $vw_customer['country'] }}" disabled>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                            
            </div>
            @endif
            
            <div id="customer-default-payment" class="wizard-page-content" style="display:none;">
                <div class="well">
                  <h4>Your default payment method is set to
                    <span id="method" class="text-primary">
                    {{ $vw_customer->default_payment_method() ?
                    $vw_customer->default_payment_method()->method : '' }}
                    </span>
                  </h4>
                  <p>The following source of funds will be charged for this request.</p>
                  <br>
                  <table class="table">
                    <tbody>
                      <tr>
                        <th>Type</th>
                        <td id="type">{{ $vw_customer->default_payment_method() ?
                          $vw_customer->default_payment_method()->source_type : '' }}
                        </td>
                      </tr>
                      <tr>
                        <th>Number</th>
                        <td id="number">{{ $vw_customer->default_payment_method() ?
                          $vw_customer->default_payment_method()->source_number : '' }}
                        </td>
                      </tr>
                      <tr>
                        <th>Expires on</th>
                        <td id="expiry">{{ $vw_customer->default_payment_method() ?
                          $vw_customer->default_payment_method()->source_expiry : 'N/A' }}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
            </div>
            <div class="command-buttons">
               <button type="button" class="btn back btn-default" data-step="3">
               <i class="fa fa-chevron-left"></i> Back
               </button>
               <button type="button" class="btn btn-success submit" data-step="3">
               Submit <i class="fa fa-spinner fa-pulse loading"></i>
               </button>
            </div>
          </div>
          
        </form>
      </div>
      <!-- END WIZARD -->
    </div>
  </div>
</div>
<!-- /.container -->
@endsection
