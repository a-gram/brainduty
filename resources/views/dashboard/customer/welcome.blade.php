@extends('layouts.dashboard')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent

    <script>
        
        App.env.no_com = true;
        
        $(document).ready(function() {
            App.init().run(new Dashboard);
        });
    </script>
@endsection


@section('content')
    <div class="container">
	<div class="row">
	    <div class="col-md-6 col-md-offset-3">
                
                @if ($vw_customer->is_active())
		<div class="customer-welcome">
		  <h1>Hello, {{ $vw_customer->first_name }}</h1>
		  <p>
                    Welcome onboard. Your brand new account has just been created and you are
                    ready to request tasks. Just click the below button to start!<br><br>
		  </p>
		  <p><a class="btn btn-primary btn-lg" href="{{ url('task/request') }}" role="button">Request a duty</a></p>
		</div>
                @else
		<div class="customer-welcome">
		  <h1>Hello {{ $vw_customer->first_name }},</h1>
		  <p>
                    Welcome onboard!<br><br>
                    To fully activate your account, please verify your email address by clicking on the activation
                    link in the welcome email sent to <b>{{ $vw_customer->email }}</b>.<br>
                    Make sure it did not end up in the junk/spam folder. If you still can't find it, 
                    you can have it sent again by clicking on the following link
                    <br><br>
                    <a href="{{ url('user/email/verify/resend') }}" class="alert-link">Resend email</a>
                    <br><br>
		  </p>
		</div>
                @endif
                
	    </div>
	</div>
    </div> <!-- /.container -->
@endsection
