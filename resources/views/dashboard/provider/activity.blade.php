@extends('layouts.dashboard')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent
    <script src="{{ URL::asset('js/dashboard/provider/activity.js') }}"></script>
    <script src="{{ URL::asset('js/widget/task.js') }}"></script>
    <script src="{{ URL::asset('ext/parsley/js/parsley.min.js') }}"></script>

    <script>
        $(document).ready(function() {
        	App.init().run(new Dashboard);
        });
    </script>
@endsection

@section('content')
<div class="container">
   <div class="row">
      <div class="col-md-6 col-md-offset-3">
          
         <div class="task-view">
             
         <!-- TRANSACTIONS VIEW -->
         
         <div class="content-pane txn-view">
             
            <!-- BALANCE VIEW -->
            <div class="balance">
               <label>BALANCE</label>
               <h1 class="available">{{ App\Hey::to_currency($vw_balance['available']) }}
               <?php
               /*
                * NOTE: We are currently using automatic transfers.
                * 
               @if ($vw_balance['available'] > 0)
               <a class="btn btn-success withdraw-pane">WITHDRAW</a>
               @endif
                */
                ?>
               </h1>
               @if ($vw_balance['pending'] > 0)
               <h5 class="pending">{{ App\Hey::to_currency($vw_balance['pending']) }}<small>&nbsp;(pending)</small></h5>
               @endif
               <?php
               /*
                * See note above.
                * 
               <form class="form parsley-form hidden" action="">
                  <p>For security reasons your account password is needed to withdraw funds.</p>
                  <div class="form-group">
                     <input type="password" id="password" class="form-control" data-parsley-required>
                  </div>
                  <button type="button" class="btn btn-primary withdraw" style="width:100%;">Go<i class="fa fa-spinner fa-pulse loading"></i></button>
               </form>
                */
               ?>
            </div>
            
            <!-- TRANSACTIONS LIST VIEW -->
            <div class="panel panel-default">
               <div class="panel-heading">TRANSACTIONS</div>
               <div class="list-group transactions">
                  @foreach ($vw_transactions as $txn)
                  <div class="item-container" id="{{ $txn->ref }}">
                     <a class="list-group-item" href="javascript:;" data-txn-ref="{{ $txn->ref }}" >
                        <h4 class="item-amount {{ !$txn->is_cancelled() ? ($txn->is_credit() ? 'credit' : 'debit') : 'cancelled' }} pull-right">{{ App\Hey::to_currency($txn->amount_bal) }}</h4>
                        <h4 class="list-group-item-heading">{{ $txn->description }}</h4>
                        <i class="fa {{ App\K::$TXN_STATUS_ICON[$txn->get_status()] }} status {{ $txn->get_status() }}"></i>
                        <small class="item-datetime"><i class="fa fa-clock-o"></i>&nbsp;{{ $txn->created_at }}</small>
                     </a>
                     <div class="item-details collapse" aria-expanded="false">
                        <dl class="dl-horizontal">
                           <dt>Reference</dt>
                           <dd class="txn-ref"></dd>
                           <dt>Duty</dt>
                           <dd class="txn-task"><a href="javascript:;"></a></dd>
                           <dt>Date/time</dt>
                           <dd class="txn-created-at"></dd>
                           <dt>Status</dt>
                           <dd class="txn-status"><span class="label label-txn-status"></span></dd>
                           <dt>&nbsp;</dt>
                           <dd>&nbsp;</dd>
                           <dt>Type</dt>
                           <dd class="txn-type"></dd>
                           <dt>Paid by</dt>
                           <dd class="txn-settle"><span class="method"></span><span class="status"></span></dd>
                           <dt>Description</dt>
                           <dd class="txn-description"></dd>
                           <dt>Amount</dt>
                           <dd class="txn-gross"></dd>
                           <dt>Fees</dt>
                           <dd class="txn-fee"></dd>
                           <dt>Tax</dt>
                           <dd class="txn-tax"></dd>
                           <dt>Amount Net</dt>
                           <dd class="txn-net"></dd>
                        </dl>
                     </div>
                     <div class="item-loading">Loading...</div>
                  </div>
                  @endforeach
               </div>
               <div class="panel-footer">
                  @if (!empty($vw_transactions))
                  <button href="javascript:;" class="btn btn-tertiary load-more" data-page="1">Load more ...<i class="fa fa-spinner fa-pulse loading"></i></button>
                  @endif
               </div>
            </div>
            
            <!-- NO TRANSACTIONS VIEW -->
            <div class="panel-empty transactions" style="display: none;">
               <p class="title"><i class="fa fa-inbox"></i><span>No transactions.</span></p>
            </div>
            
         </div>
         <!-- /.txn-view -->
         
         <!-- TASK DETAILS VIEW -->
         @include('widgets.task-details')
         
         <div class="task-progress-view"><i class="fa fa-spinner fa-spin"></i></div>
         
         </div>
         
      </div>
   </div>
</div>
<!-- /.container -->
@endsection
