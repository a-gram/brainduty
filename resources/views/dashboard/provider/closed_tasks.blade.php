@extends('layouts.dashboard')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent
    <script src="{{ URL::asset('js/dashboard/provider/closed_tasks.js') }}"></script>
    <script src="{{ URL::asset('js/widget/task.js') }}"></script>
    
    <script>
        $(document).ready(function() {
        	App.init().run(new Dashboard);
        });
    </script>
@endsection


@section('content')
    <div class="container">
	
	   <div class="row">
	      <div class="col-md-6 col-md-offset-3">
                  
                  <div class="task-view">
		  
		    <!-- CLOSED TASKS LIST VIEW -->
		    
		    <div class="content-pane task-list-view">
			
				<div class="panel panel-default">
				  <div class="panel-heading">CLOSED DUTIES</div>
				  
				  <!-- Closed tasks list -->
                  <div class="list-group closed tasks">
				      @foreach ($vw_closed_tasks as $task)
					  <a id="{{ $task->ref }}" href="javascript:;" class="list-group-item" data-task-ref="{{ $task->ref }}">
						<span class="label label-task-status label-task-{{ $task->status_to_string() }} pull-right">{{ $task->status_to_string() }}</span>
						<h4 class="list-group-item-heading">{{ $task->subject }}</h4>
						<small class="list-group-item-details"><i class="fa fa-clock-o"></i>&nbsp;{{ $task->schedule_datetime }}</small>
					  </a>
				      @endforeach
                  </div>
                                  <div class="panel-footer">
                                      @if (!empty($vw_closed_tasks))
                                      <button href="javascript:;" class="btn btn-tertiary load-more" data-page="1">Load more ...<i class="fa fa-spinner fa-pulse loading"></i></button>
                                      @endif
                                  </div>
				</div>
				
				<!-- No closed tasks list -->
	            <div class="panel-empty tasks closed" style="display: none;">
	                                <p class="title"><i class="fa fa-inbox"></i><span>No closed duties.</span></p>
	            </div>

			</div>
			
			<!-- TASK DETAILS VIEW -->
			
			@include('widgets.task-details')
			
                        <div class="task-progress-view"><i class="fa fa-spinner fa-spin"></i></div>
                        
                  </div>
			
		  </div>
	   </div>

    </div> <!-- /.container -->
@endsection
