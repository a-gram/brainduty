@extends('layouts.dashboard')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent
    <script src="{{ URL::asset('js/dashboard/provider/home.js') }}"></script>
    <script src="{{ URL::asset('js/widget/task.js') }}"></script>
    @include('widgets.agchat-include')

    <script>
        $(document).ready(function() {
        	App.init().run(new Dashboard);
        });
    </script>
@endsection


@section('content')
<div class="container">
    
   <!-- ALERTS -->
   <div class="row">
      <div class="col-md-6 col-md-offset-3">
         @if (!empty($vw_alerts))
         <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="alert-container">
            @foreach ($vw_alerts as $alert)
            <div class="alert-item">
                <i class="fa fa-exclamation-circle"></i>
                <div class="alert-item-content">{!! $alert->get() !!}</div>
            </div>
            @endforeach
            </div>
         </div>
         @endif
      </div>
   </div>

   <div class="row">
      <div class="col-md-6 col-md-offset-3">
          
         <div class="task-view">
         
         <!-- OPEN TASKS LIST VIEW -->
         
         <div class="content-pane task-list-view">
            <div class="panel panel-default">
               <div class="panel-heading">OPEN DUTIES</div>
               <!-- Open tasks list -->
               <div class="list-group tasks open">
                  @foreach ($vw_open_tasks as $task)
                  <a id="{{ $task->ref }}" href="javascript:;" class="list-group-item" data-task-ref="{{ $task->ref }}">
                     <span class="label label-task-status label-task-{{ $task->status_to_string() }} pull-right">{{ $task->status_to_string() }}</span>
                     <h4 class="list-group-item-heading">{{ $task->subject }}</h4>
                     <small class="list-group-item-details"><i class="fa fa-clock-o"></i>&nbsp;{{ $task->schedule_datetime }}</small>
                  </a>
                  @endforeach
               </div>
               <div class="panel-footer">
                  @if (!empty($vw_open_tasks))
                  <button href="javascript:;" class="btn btn-tertiary load-more" data-page="1">Load more ...<i class="fa fa-spinner fa-pulse loading"></i></button>
                  @endif
               </div>
            </div>
            <!-- No open tasks list -->
            <div class="panel-empty tasks open" style="display: none;">
               <p class="title"><i class="fa fa-inbox"></i><span>No open duties.</span></p>
            </div>
         </div>
         
         <!-- TASK DETAILS VIEW -->
         @include('widgets.task-details')
         
         <div class="task-progress-view"><i class="fa fa-spinner fa-spin"></i></div>
         
         </div>
          
      </div>
   </div>
</div>
@endsection
