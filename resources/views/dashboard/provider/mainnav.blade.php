
  <div class="mainnav ">
    <div class="container">
      <a class="mainnav-toggle" data-toggle="collapse" data-target=".mainnav-collapse">
        <span class="sr-only">Toggle navigation</span>
        <i class="fa fa-bars"></i>
      </a>
      <nav class="collapse mainnav-collapse" role="navigation">
        <ul class="mainnav-menu">
          <li class="dropdown ">
            <a href="{{ URL::to('provider/home') }}" class="">
               <i class="fa fa-home fa-fw"></i>
               <span class="visible-xs-inline">&nbsp;Home</span>
            </a>
          </li>
          <li class="dropdown ">
            <a href="{{ URL::to('provider/activity') }}" class="">
               <i class="fa fa-dollar fa-fw"></i>
               <span class="visible-xs-inline">&nbsp;Activity</span>
            </a>
          </li>
          <li class="dropdown ">
            <a href="{{ URL::to('provider/tasks/closed') }}" class="">
               <i class="fa fa-folder-open fa-fw"></i>
               <span class="visible-xs-inline">&nbsp;Closed duties</span>
            </a>
          </li>
          <li class="dropdown ">
            <a href="{{ URL::to('provider/help') }}" class="">
               <i class="fa fa-question-circle fa-fw"></i>
               <span class="visible-xs-inline">&nbsp;Help</span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
