		     <?php
		            // Main provider account menu.
		     ?>
     
            <ul class="dropdown-menu" role="menu">
              <li>
                <a href="{{ URL::to('/provider/settings') }}">
                  <i class="fa fa-cogs fa-fw"></i> 
                  &nbsp;&nbsp;Settings
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="{{ URL::to('/user/logout') }}">
                  <i class="fa fa-sign-out fa-fw"></i> 
                  &nbsp;&nbsp;Logout
                </a>
              </li>
            </ul>
