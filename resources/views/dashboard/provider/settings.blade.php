@extends('layouts.dashboard')

@section('styles')
    @parent
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('javascripts')
    @parent
    <script src="{{ URL::asset('js/widget/aglist.js') }}"></script>
    <script src="{{ URL::asset('js/dashboard/provider/settings.js') }}"></script>
    <script src="{{ URL::asset('ext/parsley/js/parsley.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://js.stripe.com/v2/"></script>
    @include('widgets.agchat-include')

    <script type="text/javascript">
        
        App.env.market_pkey = '{{ $vw_market_pkey }}';

        $(document).ready(function() {
            App.init().run(new Dashboard);
        });
    </script>
@endsection

@section('content')
<div class="container">
    
  <div class="layout layout-main-right layout-stack-sm">
      
    <div class="col-md-3 col-sm-4 layout-sidebar">
      <div class="nav-layout-sidebar-skip">
        <strong>Tab Navigation</strong> / <a href="#settings-content">Skip to Content</a>	
      </div>
      <ul id="settings-tabs" class="nav nav-layout-sidebar nav-stacked">
        <li class="active">
          <a href="#profile-settings" data-toggle="tab">
          <i class="fa fa-user"></i> 
          &nbsp;&nbsp;Profile
          </a>
        </li>
        <li>
          <a href="#security-settings" data-toggle="tab">
          <i class="fa fa-lock"></i> 
          &nbsp;&nbsp;Password
          </a>
        </li>
        <li>
          <a href="#payment-settings" data-toggle="tab">
          <i class="fa fa-dollar"></i> 
          &nbsp;&nbsp;Payment
          </a>
        </li>
        <li>
          <a href="#account-settings" data-toggle="tab">
          <i class="fa fa-cogs"></i> 
          &nbsp;&nbsp;Account
          </a>
        </li>
      </ul>
    </div>
      
    <div class="col-md-9 col-sm-8 layout-main">
      <div id="settings-content" class="tab-content stacked-content">
          
        <!-- PROFILE SETTINGS -->
        
        <div class="tab-pane fade in active" id="profile-settings">
          <div class="heading-block">
            <h3>Edit Profile</h3>
          </div>
          <form class="form-horizontal parsley-form">
              
            <div class="form-group">
              <label class="col-md-3 control-label"></label>
              <div class="col-md-7">
                  
                 <div class="avatar">
                   <div class="images">
                     <img class="img thumbnail" src="{{ $vw_provider->get_shared_file('avatar','url') }}" alt="Avatar">
                   </div>
                   <div class="command-buttons">
                        <span class="btn btn-default fileinput-button change">
                            <i class="fa fa-pencil fa-fw"></i>
                            <input name="files[]" type="file" multiple>
                        </span>
                       <span class="btn btn-primary remove" style="display:none;">
                           <i class="fa fa-times fa-fw"></i>
                       </span>
                   </div>
                 </div>
                 
              </div>
            </div>
              
            <div class="form-group">
              <label class="col-md-3 control-label">Username</label>
              <div class="col-md-7">
                <input type="text" name="user-name" value="{{ $vw_provider->username }}"
                  class="form-control"
                  disabled>
              </div>
            </div>
            @if ($vw_provider->is_business())
            <div class="form-group">
              <label class="col-md-3 control-label">Business Name</label>
              <div class="col-md-7">
                <input type="text" id="business-name" value="{{ $vw_provider->business_name }}"
                  class="form-control"
                  disabled>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Business number</label>
              <div class="col-md-7">
                <input type="text" id="business-number" value="{{ $vw_provider->business_number }}"
                  class="form-control"
                  disabled>
              </div>
            </div>
            @endif
            <div class="form-group">
              <label class="col-md-3 control-label">First Name</label>
              <div class="col-md-7">
                <input type="text" id="first-name" value="{{ $vw_provider->first_name }}"
                  class="form-control"
                  disabled>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Last Name</label>
              <div class="col-md-7">
                <input type="text" id="last-name" value="{{ $vw_provider->last_name }}"
                  class="form-control" 
                  disabled>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Date of birth</label>
              <div class="col-md-7">
                <input type="text" id="dob" value="{{ $vw_provider->dob }}"
                  class="form-control"
                  disabled>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Mobile number</label>
              <div class="col-md-7">
                <input type="text" id="mobile-number" value="{{ $vw_provider->mobile }}"
                  class="form-control"
                  data-parsley-pattern="\d{10,12}"
                  data-parsley-pattern-message="Please check the phone number."
                  data-parsley-group="profile-info">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">About You</label>
              <div class="col-md-7">
                <textarea id="about" maxlength="120" rows="6" class="form-control">
                {{ $vw_provider->about }}
                </textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Provided services</label>
              <div class="col-md-7">
                
                <div class="services ag-list multi-select list-group">
                  @foreach ($vw_services as $service)
                  @if ($service['is_active'])
                  <div class="list-group-item {{ $vw_provider->provides($service) ? 'active' : '' }}" data-services-id="{{ $service['id'] }}">
                    <div class="item-icon pull-left">
                      <i class="fa {{ $service['id'] == 1 ? 'fa-file-text' : 'fa-briefcase' }}"></i>
                    </div>
                    <div class="item-body">
                      <div class="item-heading">
                        <h4 class="item-name pull-left">{{ $service['name'] }}</h4>
                      </div>
                      <p>{{ $service['description'] }}</p>
                      @if (count($service['categories']))
                      <select id="service-{{ $service['id'] }}-categories"
                        class="categories ui-select no-bubble form-control" multiple="multiple"
                        data-placeholder="Select a category"
                        style="width: 100%;">
                        <!-- <option value="none" selected="selected">Select a category...</option> -->
                        @foreach ($service['categories'] as $id => $category)
                        <option value="{{ $id }}" {{ $vw_provider->provides($service, $id) ? 'selected="selected"' : '' }}>
                        {{ $category['name'] }}
                        </option>
                        @endforeach
                      </select>
                      @endif
                    </div>
                  </div>
                  @endif
                  @endforeach
                </div>
                
                <div class="">
                  <input type="hidden" name="service_categories" value=""
                    data-parsley-required
                    data-parsley-required-message="Please select the provided services and at least one category, if any (max 10)." />
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-7 col-md-push-3">
                <button type="button" class="btn btn-primary save">Save Changes<i class="fa fa-spinner fa-pulse loading"></i></button>
              </div>
            </div>
          </form>
        </div>
        
        <!-- SECURITY SETTINGS -->
        
        <div class="tab-pane fade" id="security-settings">
          <div class="heading-block">
            <h3>Change Password</h3>
          </div>
          <form class="form-horizontal parsley-form">
            <div class="form-group">
              <label class="col-md-3 control-label">Old Password</label>
              <div class="col-md-7">
                <input type="password" id="old-password"
                  class="form-control"
                  data-parsley-required>
              </div>
            </div>
            <hr>
            <div class="form-group">
              <label class="col-md-3 control-label">New Password</label>
              <div class="col-md-7">
                <input type="password" id="password"
                  class="form-control"
                  data-parsley-required
                  data-parsley-pattern="[a-zA-Z0-9!?@#$%&(){}]{8,64}"
                  data-parsley-pattern-message="Invalid password.">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Confirm New Password</label>
              <div class="col-md-7">
                <input type="password" id="password-confirm"
                  class="form-control"
                  data-parsley-required
                  data-parsley-equalto="#password"
                  data-parsley-equalto-message="This does not match the new password.">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-7 col-md-push-3">
                <button type="button" class="btn btn-primary save">Save Changes<i class="fa fa-spinner fa-pulse loading"></i></button>
              </div>
            </div>
          </form>
        </div>
        
        <!-- PAYMENT SETTINGS -->
        
        <div class="tab-pane fade" id="payment-settings">
          <div class="heading-block">
            <h3>Payment Settings</h3>
          </div>
          
          <form class="form-horizontal parsley-form">
            
            <?php /*
              If the provider is not verified, include the activation/verification
              pane and the required inputs, based on whether the current state of
              verification allows submission of info.
              If the provider is verified include the default payout method pane
              to allow changes.
              */ 
            ?>
            @if ($vw_provider->is_verified())
            
            <div class="default-payout">
              <div class="form-group">
                <div class="col-md-7 col-md-push-3">
                  <div class="well">
                    <h4>Your default payout method is set to
                      <span id="method" class="text-primary">{{ $vw_provider->default_payout_method()->method }}</span>
                    </h4>
                    <p>Withdrawn funds will be transferred to the following account.</p>
                    <table class="table">
                      <tbody>
                        <tr>
                          <th>Account host</th>
                          <td id="host">{{ $vw_provider->default_payout_method()->account_host }}</td>
                        </tr>
                        <tr>
                          <th>Account holder</th>
                          <td id="holder">{{ $vw_provider->default_payout_method()->account_name }}</td>
                        </tr>
                        <tr>
                          <th>Account id</th>
                          <td id="id">{{ $vw_provider->default_payout_method()->account_number }}</td>
                        </tr>
                      </tbody>
                    </table>
                    <div><button type="button" class="btn btn-secondary btn-sm change">Change</button></div>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="bank-details" style="display:none;">
              <div class="form-group">
                <label class="col-md-3 control-label">Account holder</label>
                <div class="col-md-7">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user fa-fw"></i></span>
                        <input type="text" id="bank-holder" class="form-control"
                               value="{{ $vw_provider->default_payout_method() ? 
                                         $vw_provider->default_payout_method()->account_name : '' }}"
                               data-parsley-pattern="[a-zA-Z0-9 \/'.\-&]{1,64}"
                               data-parsley-pattern-message="Names can only contain letters, numbers and the symbols /'.-&"
                               data-parsley-group="bank-details">
                    </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Account number</label>
                <div class="col-md-7">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-bank fa-fw"></i></span>
                        <input type="text" id="bank-number" class="form-control"
                               value="{{ $vw_provider->default_payout_method() ? 
                                         $vw_provider->default_payout_method()->account_number : '' }}"
                               data-parsley-pattern="\d{6} \d{6,10}"
                               data-parsley-pattern-message="Invalid account number. Specify the BSB and account number separated by a space."
                               data-parsley-group="bank-details">
                    </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-7 col-md-push-3">
                  <button type="button" class="btn btn-success submit">Submit<i class="fa fa-spinner fa-pulse loading"></i></button>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-7 col-md-push-3">
                   <div class="reassurance">
                      <i class="fa fa-lock"></i>
                      <p>
                         Your details are transmitted over a secure connection. Bank info are
                         sent to our payout processor and not stored in our records. To learn more, see our 
                         <a href="{{ url('privacy') }}">privacy policy</a>.
                      </p>
                   </div>
                </div>
              </div>
            </div>
            
            <div class="heading-block-style underlined">
              <i class="fa fa-info"></i>
              <h5>Billing info</h5>
            </div>
            
            <div class="billing-details">
              <div class="form-group">
                <label class="col-md-3 control-label">Address</label>
                <div class="col-md-7">
                  <input type="text" id="address" value="{{ $vw_provider->address }}"
                    class="form-control"
                    data-parsley-required
                    data-parsley-pattern="[a-zA-Z0-9 \/'.\-&]{1,64}"
                    data-parsley-pattern-message="Addresses can only contain letters, numbers and the symbols /'.-&"
                    data-parsley-group="billing-details">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">City</label>
                <div class="col-md-7">
                  <input type="text" id="city" value="{{ $vw_provider->city }}"
                    class="form-control"
                    data-parsley-required
                    data-parsley-pattern="[a-zA-Z0-9 \/'.\-&]{1,64}"
                    data-parsley-pattern-message="Names can only contain letters, numbers and the symbols /'.-&"
                    data-parsley-group="billing-details">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">State</label>
                <div class="col-md-7">
                  <select id="state" class="form-control"
                    data-parsley-required
                    data-parsley-group="billing-details">
                  <option value="NSW" {{ $vw_provider->state === 'NSW' ? 'selected' : '' }}>New South Wales</option>
                  <option value="NT" {{ $vw_provider->state === 'NT' ? 'selected' : '' }}>Northern Territory</option>
                  <option value="QLD" {{ $vw_provider->state === 'QLD' ? 'selected' : '' }}>Queensland</option>
                  <option value="WA" {{ $vw_provider->state === 'WA' ? 'selected' : '' }}>Western Australia</option>
                  <option value="SA" {{ $vw_provider->state === 'SA' ? 'selected' : '' }}>South Australia</option>
                  <option value="TAS" {{ $vw_provider->state === 'TAS' ? 'selected' : '' }}>Tasmania</option>
                  <option value="VIC" {{ $vw_provider->state === 'VIC' ? 'selected' : '' }}>Victoria</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Zip code</label>
                <div class="col-md-7">
                  <input type="text" id="zip-code" value="{{ $vw_provider->zip_code }}"
                    class="form-control"
                    data-parsley-required
                    data-parsley-pattern="\d{4}"
                    data-parsley-pattern-message="Invalid zip code."
                    data-parsley-group="billing-details">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Country</label>
                <div class="col-md-7">
                  <input type="text" id="country" value="Australia"
                         class="form-control"
                         data-parsley-group="billing-details"
                         disabled>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-7 col-md-push-3">
                  <button type="button" class="btn btn-primary save">Save Changes<i class="fa fa-spinner fa-pulse loading"></i></button>
                </div>
              </div>
            </div>
            
            @else
            <p>
                Your account is not yet active.
            </p>
            @endif

          </form>
        </div>
        
        <!-- ACCOUNT SETTINGS -->
        
        <div class="tab-pane fade" id="account-settings">
          <div class="heading-block">
            <h3>Account settings</h3>
          </div>
          <form class="form-horizontal parsley-form">
              
            <!-- ------------------------ ACTIVATION --------------------------- -->
            
            @if (!$vw_provider->is_verified())
            
            <div class="activation">
              <div class="form-group">
                <label class="col-md-3 control-label"></label>
                <div class="col-md-7">
                    
                    <div class="frame">
                        
                    <!-- ALERTS -->
                    
                    <section class="alerts">
                        <p></p>
                        @if (!$vw_provider->has_verification())
                        <p>
                          Your account needs to be verified in order to become fully active. Submit this 
                          form to start the verification process.
                          
                          Once the verification is started, you may be able to receive customer requests.<br><br>
                          
                          <small>
                              To learn more about account verification and the kind of information you may be asked for,
                              see <a href="{{ url('provider/help#verification') }}">here</a>.
                          </small>
                        </p>
                        @elseif ($vw_provider->is_verifying())
                        <p>
                          Your account is under verification. 
                        </p>
                        @elseif ($vw_provider->is_inquired())
                        <p>
                          Your account is under verification. The following information will be required in order to
                          fully activate it.
                        </p>
                           @if (!empty($vw_provider->identity->notes))
                           <p style="color: #d74b4b;">
                               <i class="fa fa-exclamation-circle"></i><strong>&nbsp;Actions required:</strong>
                               <br>
                               {{ $vw_provider->identity->notes }}
                           </p>
                           @endif
                        @elseif ($vw_provider->is_unverified())
                        <p>
                          Your account could not be verified.
                        </p>
                        @endif
                    </section>
                                        
                    <!-- DOCUMENTS -->
                    
                    @if ($vw_id_docs_required && ($vw_provider->is_inquired() || 
                                                 !$vw_provider->has_verification()))
                    <section class="documents">
                      <div class="command-buttons">
                        <h4>Required documents
                        <a class="help" href="{{ url('provider/help#verification') }}">
                          <i class="fa fa-question-circle"></i>
                        </a>
                        </h4>
                        <input class="hidden" name="files[]" type="file">
                        <input class="hidden" name="file" type="file">
                      </div>
                      <table class="table table-condensed styled">
                        <tbody>
                            @foreach ($vw_id_docs_required as $id=>$document)
                            <tr class="document">
                                <td class="doc-filename"></td>
                                <td class="doc-name">{{ $document['name'] }}</td>
                                <td class="doc-file">
                                    <span class="btn btn-default btn-xs fileinput-button">
                                        <i class="fa fa-upload fa-fw"></i>
                                        <input name="document" type="file"
                                               class="form-control {{ $document['verification'] }}"
                                               data-type="{{ $document['type'] }}"
                                               data-verification="{{ $document['verification'] }}">
                                    </span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                    </section>
                    @endif
                    
                    <!-- BILLING INFO -->
                    
                    @if ($vw_provider->identity->is_info_required(\App\K::IDENTITY_INFO_ADDRESS))
                    <section class="billing-details">
                      <div class="heading-block-style underlined">
                        <i class="fa fa-info"></i>
                        <h5>Billing info</h5>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3 control-label">Address</label>
                        <div class="col-md-7">
                          <input type="text" id="address" value="{{ $vw_provider->address }}"
                            class="form-control"
                            data-parsley-required
                            data-parsley-pattern="[a-zA-Z0-9 \/'.\-&]{1,64}"
                            data-parsley-pattern-message="Addresses can only contain letters, numbers and the symbols /'.-&"
                            data-parsley-group="billing-details">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3 control-label">City</label>
                        <div class="col-md-7">
                          <input type="text" id="city" value="{{ $vw_provider->city }}"
                            class="form-control"
                            data-parsley-required
                            data-parsley-pattern="[a-zA-Z0-9 \/'.\-&]{1,64}"
                            data-parsley-pattern-message="Names can only contain letters, numbers and the symbols /'.-&"
                            data-parsley-group="billing-details">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3 control-label">State</label>
                        <div class="col-md-7">
                          <select id="state" class="form-control"
                            data-parsley-required
                            data-parsley-group="billing-details">
                          <option value="NSW" {{ $vw_provider->state === 'NSW' ? 'selected' : '' }}>New South Wales</option>
                          <option value="NT" {{ $vw_provider->state === 'NT' ? 'selected' : '' }}>Northern Territory</option>
                          <option value="QLD" {{ $vw_provider->state === 'QLD' ? 'selected' : '' }}>Queensland</option>
                          <option value="WA" {{ $vw_provider->state === 'WA' ? 'selected' : '' }}>Western Australia</option>
                          <option value="SA" {{ $vw_provider->state === 'SA' ? 'selected' : '' }}>South Australia</option>
                          <option value="TAS" {{ $vw_provider->state === 'TAS' ? 'selected' : '' }}>Tasmania</option>
                          <option value="VIC" {{ $vw_provider->state === 'VIC' ? 'selected' : '' }}>Victoria</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3 control-label">Zip code</label>
                        <div class="col-md-7">
                          <input type="text" id="zip-code" value="{{ $vw_provider->zip_code }}"
                            class="form-control"
                            data-parsley-required
                            data-parsley-pattern="\d{4}"
                            data-parsley-pattern-message="Invalid zip code."
                            data-parsley-group="billing-details">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3 control-label">Country</label>
                        <div class="col-md-7">
                          <input type="text" id="country" value="Australia"
                                 class="form-control"
                                 data-parsley-group="billing-details"
                                 disabled>
                        </div>
                      </div>
                    </section>
                    @endif
                    
                    <!-- BANK INFO -->
                    
                    @if ($vw_provider->identity->is_info_required(\App\K::IDENTITY_INFO_BANK))
                    <section class="bank-details" >
                      <div class="heading-block-style underlined">
                        <i class="fa fa-dollar"></i>
                        <h5>Bank account</h5>
                      </div>
                      <div class="reassurance">
                         <i class="fa fa-lock"></i>
                         <p>
                            This is the bank account where your proceeds will be deposited.
                            These details are transmitted over a secure connection and safely stored
                            by our payout processor. To learn more, see our <a href="{{ url('privacy') }}">privacy policy</a>.
                         </p>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3 control-label">Holder</label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user fa-fw"></i></span>
                                <input type="text" id="bank-holder" class="form-control"
                                       value="{{ $vw_provider->business_name ?: $vw_provider->full_name()  }}"
                                       data-parsley-required
                                       data-parsley-pattern="[a-zA-Z0-9 \/'.\-&]{1,64}"
                                       data-parsley-pattern-message="Names can only contain letters, numbers and the symbols /'.-&"
                                       data-parsley-group="bank-details">
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3 control-label">Number</label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-bank fa-fw"></i></span>
                                <input type="text" id="bank-number" class="form-control"
                                       data-parsley-required
                                       data-parsley-pattern="\d{6} \d{6,10}"
                                       data-parsley-pattern-message="Invalid account number. Specify the BSB and account number separated by a space."
                                       data-parsley-group="bank-details">
                            </div>
                        </div>
                      </div>
                    </section>
                    @endif
                    
                    @if ($vw_provider->is_inquired() || !$vw_provider->has_verification())
                    <section class="command-buttons">
                        <button type="button" class="btn btn-secondary submit">Submit<i class="fa fa-spinner fa-pulse loading"></i></button>
                    </section>
                    @endif
                    
                    </div>
                </div>
              </div>
            </div>

            @endif
            
            <!-- --------------------------------------------------------------- -->
            
            @if ($vw_provider->is_active())
            
            <?php /*
            <div class="account-markup">
              <div class="form-group">
                <label class="col-md-3 control-label">Markup</label>
                <div class="col-md-7">
                    <p>With the below markup you will earn <b id="generic-rate"></b> per hour for generic tasks and <b id="expert-rate"></b> for expert tasks (excluding fees). </p>
                    <div class="input-group" style="width:30%; text-align:right;">
                      <input id="markup" type="text" class="form-control" aria-describedby="basic-addon2" maxlength="3"
                             value="{{ $vw_provider->settings->markup }}"
                             data-parsley-required
                             data-parsley-pattern="\d+"
                             data-parsley-pattern-message="This must be a number"
                             data-parsley-group="account-details">
                      <span class="input-group-addon" id="basic-addon2">%</span>
                    </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-7 col-md-push-3">
                  <button type="button" class="btn btn-primary save">Save Changes</button>
                </div>
              </div>
            </div>
            */?>
              
            <div class="account-cancel-policy">
              <div class="form-group">
                <label class="col-md-3 control-label">Duty cancellation policy</label>
                <div class="col-md-7">
                    <fieldset class="radiogroup cancel-policy">
                    <ul>
                      <li><input type="radio" id="cp-no-charge" name="cancel_policy" value="{{ \App\K::TASK_CANCEL_POLICY_NO_CHARGE }}" {{ $vw_provider->settings->task_cancel_policy === \App\K::TASK_CANCEL_POLICY_NO_CHARGE ? 'checked' : '' }}><label for="cp-no-charge">No charge</label></li>
                      <li><input type="radio" id="cp-services" name="cancel_policy" value="{{ \App\K::TASK_CANCEL_POLICY_CHARGE_SERVICES }}" {{ $vw_provider->settings->task_cancel_policy === \App\K::TASK_CANCEL_POLICY_CHARGE_SERVICES ? 'checked' : '' }}><label for="cp-services">Full price</label></li>
                      <li><input type="radio" id="cp-fee" name="cancel_policy" value="{{ \App\K::TASK_CANCEL_POLICY_CHARGE_FEE }}" {{ $vw_provider->settings->task_cancel_policy === \App\K::TASK_CANCEL_POLICY_CHARGE_FEE ? 'checked' : '' }}><label for="cp-fee">Fixed fee</label></li>
                    </ul>
                    </fieldset>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-7 col-md-push-3">
                  <button type="button" class="btn btn-primary save">Save Changes<i class="fa fa-spinner fa-pulse loading"></i></button>
                </div>
              </div>
            </div>
            
            @endif

          </form>
          
          <br class="xs-60">
          <?php /*
          <hr>
          
          <div>
             <button type="button" class="btn btn-default btn-sm delete">Close account<i class="fa fa-spinner fa-pulse loading"></i></button>
          </div>
          */ ?>
        </div>
        
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</div>
<!-- /.container -->
@endsection
