@extends('layouts.dashboard')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent

    <script>
        
        App.env.no_com = true;
        
        $(document).ready(function() {
            App.init().run(new Dashboard);
        });
    </script>
@endsection


@section('content')
    <div class="container">
	   <div class="row">
	      <div class="col-md-6 col-md-offset-3">
                  
                @if ($vw_provider->is_active())
		<div class="provider-welcome">
		  <h1>Hello, {{ $vw_provider->first_name }}</h1>
                  <p>
                    Thanks for registering. Your account is active and you are ready to provide your services 
                    and start earning!
                  </p>
		</div>
                @else
		<div class="provider-welcome">
		  <h1>Hello {{ $vw_provider->first_name }},</h1>
		  <p>
                    Welcome onboard!<br><br>
                    We need to verify your email address.
                    Please click on the confirmation link in the welcome email that we sent to <b>{{ $vw_provider->email }}</b>
                    Make sure it did not end up in the junk/spam folder. 
                    If you can't find it, use the following link to have it resent
                    <br><br>
                    <a href="{{ url('user/email/verify/resend') }}" class="alert-link">Resend email</a>
                    <br><br>
                    After your email address is verified, you can start the identity verification process.<br><br>
		  </p>
                  
		</div>
                @endif

	      </div>
	   </div>
    </div> <!-- /.container -->
@endsection
