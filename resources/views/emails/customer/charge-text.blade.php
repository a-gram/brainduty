
Hi {{ $vw_customer_name }},

A charge of {{ $vw_charge_amount_total }} has been made for the following job

"{{ $vw_task_subject }}"

Used payment method: {{ $vw_payment_method }} {{ $vw_payment_method_number }}

If there are any issues with this payment, please use the Dispute Form in your
account, accessible by clicking on the Dispute button in the job details page,
or use the following link

{{ url('task/'.$vw_task_ref.'/dispute') }}

Do not reply to this email as it's an automatically sent message and you will not
receive a response.

Sincerely,

The Brainduty Team
