
Hi {{ $vw_customer_name }},

Thank you for your payment. Here's a receipt for your records:

REFERENCE: {{ $vw_receipt_ref }}
DATE: {{ $vw_receipt_date }}
PAID WITH: {{ $vw_receipt_pay_method }}
DESCRIPTION: {{ $vw_receipt_description }}
TOTAL: {{ $vw_receipt_amount_total }}
<?php //TAX:            {{ $vw_receipt_amount_tax }} ?>

Please take a few seconds to give your feedback by using the following link

{{ url('task/'.$vw_task_ref.'/rate') }}

If the link does not work, you can log into your account and click on the Rate
button in the jobs detail page.

Do not reply to this email as it's an automatically sent message and you will not
receive a response.

Sincerely,

The Brainduty Team

