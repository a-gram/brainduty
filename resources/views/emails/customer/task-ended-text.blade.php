
Hi {{ $vw_customer_name }},

A request to end the following duty has been sent by the assistant:

"{{ $vw_task_subject }}"

To accept or refuse the request, log in to your account. You can use the following 
link to see the request in your dashboard

{!! url('customer/home').'?task='.$vw_task_ref.'&msg='.$vw_msg_ref !!}

If you do not reply to the request within {{ $vw_request_expiry_time }} you are deemed as
having accepted the duty and will be charged the due amount.

Do not reply to this email as it's an automatically sent message and you will not
receive a response.

Sincerely,

The Brainduty Team
