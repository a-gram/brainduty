
Hi,

We have received a request to reset the password for a Brainduty account.
If it is your account and you have made the request, click on the following link 
to reset the password

{{ url('user/password/reset?t=').$vw_user_pr_token }}

If you did not make the request, then someone else used this email on our platform. 
It may be by mistake or for some other reason. Regardless, please ignore this email
or contact us if you have any concerns.

Kind Regards,
The Brainduty Team
