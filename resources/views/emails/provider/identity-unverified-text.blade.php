
Hi {{ $vw_provider_name }},

unfortunately your identity could not be verified. There may be several reasons
why this happened, including incorrect or incomplete information provided. If
so, then you can try to fix the issues and resubmit the correct information.

Note that we may not be able to provide the reason of a failed identity verification
if it is not known to us or cannot be disclosed, and you may not be able to
resubmit the application.

Do not reply to this email as it's an automatically sent message and you will not
receive a response.

Sincerely,

The Brainduty Team
