
Hi {{ $vw_provider_name }},

your identity has been verified and your account is now fully active.

For more information on how to use your account, please have a look at the Help 
section. Many answers can be found there. If you still cannot find an answer, feel 
free to contact our support team.

Do not reply to this email as it's an automatically sent message and you will not
receive a response.

Sincerely,

The Brainduty Team
