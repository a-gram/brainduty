
Hi {{ $vw_provider_name }},

you have been assigned the following duty

{{ $vw_offer_url }}

If the above link does not work, log into your account and find the ongoing duty
in the Open Duties section of your home page.

Good work!

Do not reply to this email as it's an automatically sent message and you will not
receive a response.

Sincerely,

The Brainduty Team
