
Hi {{ $vw_provider_name }},

a new duty is waiting to be assigned to you. Click on the following link to confirm 
your availability, or log into your account and reply to the notification.

{{ $vw_offer_url }}

This offer is valid for {{ $vw_offer_expiry }}, after which the job will no longer be available.

Do not reply to this email as it's an automatically sent message and you will not
receive a response.

Sincerely,

The Brainduty Team
