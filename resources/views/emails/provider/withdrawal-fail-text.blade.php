
Hi {{ $vw_provider_name }},

on {{ $vw_withdrawal_date }} a withdrawal of {{ $vw_withdrawal_amount }} was attempted but 
failed. The following reason was given to us for the failure:

"{{ $vw_withdrawal_fail_reason }}"

If you are aware of the issue, please fix it as soon as possible or contact your
bank for help.

Do not reply to this email as it's an automatically sent message and you will not
receive a response.

Sincerely,

The Brainduty Team
