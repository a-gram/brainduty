<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
  <title>Auch!</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="description" content="">
  <meta name="author" content="">

  @section('styles')
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,800,800italic">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,300,700">
  <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
  <!--  <link rel="stylesheet" href="{{URL::asset('ext/bootstrap/css/bootstrap.min.css')}}"> -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ URL::asset('css/dashboard.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/common.css') }}">
  @show
   
  <!-- Favicon -->
  <link rel="shortcut icon" href="{{ url('favicon.ico') }}">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class=" ">

<div id="wrapper">

  <header class="navbar" role="banner">

    <div class="container">

      <div class="navbar-header">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <i class="fa fa-cog"></i>
        </button>

        <a href="./" class="navbar-brand navbar-brand-img">
          <img src="{{ URL::asset('img/logo2.png') }}" alt="Company name">
        </a>
      </div> <!-- /.navbar-header -->


    </div> <!-- /.container -->

  </header>

  <div class="content">

    <div class="container">

      <div class="error-container">

        <div class="error-code">
            :-(
        </div> <!-- /.error-code -->

        <div class="error-details">

          <h4>Oops,</h4>

          <p>{!! $error_message !!}</p>
          
          <p>
             <a href="http://">Take me to some nice place</a>
          </p>

        </div> <!-- /.error-details -->

      </div> <!-- /.error -->

    </div> <!-- /.container -->
    
  </div> <!-- .content -->
  
</div> <!-- /#wrapper -->

<footer class="footer">
  <div class="container">
    <p class="pull-left">Copyright &copy; 2013-15 Company name.</p>
  </div>
</footer>

@section('javascripts')
<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<!--  <script src="{{ URL::asset('ext/jquery/js/jquery.min.js') }}"></script> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<!--  <script src="{{ URL::asset('ext/bootstrap/js/bootstrap.min.js') }}"></script> -->
@show

</body>
</html>
