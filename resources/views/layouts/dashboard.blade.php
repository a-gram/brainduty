<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
  <title>Dashboard &middot; Brainduty</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="description" content="">
  <meta name="author" content="">

  @section('styles')
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,800,800italic">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,300,700">
  <?php /* <link rel="stylesheet" href="{{URL::asset('ext/fontawesome/css/font-awesome.min.css')}}"> */ ?>
  <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <?php /* <link rel="stylesheet" href="{{URL::asset('ext/bootstrap/css/bootstrap.min.css')}}"> */ ?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ URL::asset('ext/alerts/css/messenger.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/dashboard.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/common.css') }}">
  @show
   
  <!-- Favicon -->
  <link rel="shortcut icon" href="{{ url('favicon.ico') }}">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class=" ">

<!--[if lte IE 9]>
<div class="alert alert-danger alert-dismissible" role="alert" style="margin:0;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Your browser is outdated and this site may not work properly. Please consider upgrading to a modern browser, such as Chrome, Opera, Firefox or IE 10+
</div>
<![endif]-->

<div id="wrapper">

  <header class="navbar" role="banner">

    <div class="container">

      <div class="navbar-header">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <i class="fa fa-cog"></i>
          <i class="fa fa-circle alert"></i>
        </button>

        <a href="{{ url('/') }}" class="navbar-brand navbar-brand-img">
          <img src="{{ URL::asset('img/logo3.png') }}" alt="{{ $vw_company_name }}">
        </a>
      </div> <!-- /.navbar-header -->

      <nav class="collapse navbar-collapse" role="navigation">

        <ul class="nav navbar-nav navbar-left"></ul>

        <ul class="nav navbar-nav navbar-right">    

          <!-- NOTIFICATIONS MENU -->

          <li class="dropdown navbar-notification updates">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell navbar-notification-icon"></i>
              <span class="visible-xs-inline">&nbsp;Notifications</span>
              <b class="badge badge-primary"></b>
            </a>
            <div class="dropdown-menu">
              <div class="dropdown-header">&nbsp;Notifications</div>
              <div class="notification-list"></div>
            </div>
          </li>

          <!-- MESSAGES NOTIFICATION MENU -->
          
          <li class="dropdown navbar-notification messages">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope navbar-notification-icon"></i>
              <span class="visible-xs-inline">&nbsp;Messages</span>
              <b class="badge badge-primary"></b>
            </a>
            <div class="dropdown-menu">
              <div class="dropdown-header">Messages</div>
              <div class="notification-list"></div>
            </div>
          </li>
		  
		  <!-- PROFILE MENU -->
		  
          <li class="dropdown navbar-profile">
            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
              <img src="{{ Auth::user()->get_shared_file('avatar','url') }}" class="navbar-profile-avatar" alt="">
              <span class="">Hi {{ Auth::user()->first_name }} &nbsp;</span>
              <i class="fa fa-caret-down"></i>
            </a>
            @if (Auth::check() && Auth::user()->is_provider())
               @include('dashboard.provider.menu')
            @elseif (Auth::check() && Auth::user()->is_customer())
               @include('dashboard.customer.menu')
            @endif
          </li>

        </ul>

      </nav>

    </div> <!-- /.container -->

  </header>

  @if (Auth::check() && Auth::user()->is_provider())
     @include('dashboard.provider.mainnav')
  @elseif (Auth::check() && Auth::user()->is_customer())
     @include('dashboard.customer.mainnav')
  @endif

  <div class="content">

    @yield('content')
    
  </div> <!-- .content -->
  
</div> <!-- /#wrapper -->

@include('widgets.modal')

<footer class="footer">
  <div class="container">
    <p class="pull-left"><small>Copyright &copy; 2017 Brainduty Technologies.</small></p>
  </div>
</footer>

@section('javascripts')
<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<?php /* <script src="{{ URL::asset('ext/jquery/js/jquery.min.js') }}"></script> */ ?>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<?php /* <script src="{{ URL::asset('ext/bootstrap/js/bootstrap.min.js') }}"></script> */ ?>
<script src="{{ URL::asset('ext/slimscroll/js/jquery.slimscroll.min.js') }}"></script>
<script src="{{ URL::asset('ext/alerts/js/messenger.min.js') }}"></script>
<script src="{{ URL::asset('js/app.js') }}"></script>
<script src="{{ URL::asset('js/app.com.js') }}"></script>
<script src="{{ URL::asset('js/dashboard/page.js') }}"></script>
@if (Auth::check() && Auth::user()->is_provider())
<script src="{{ URL::asset('js/dashboard/provider/page.js') }}"></script>
@elseif (Auth::check() && Auth::user()->is_customer())
<script src="{{ URL::asset('js/dashboard/customer/page.js') }}"></script>
@endif
<script src="{{ URL::asset('js/widget/notifications.js') }}"></script>
<script src="{{ URL::asset('js/utils.js') }}"></script>
<script>
   App.env = {
         URL_BASE  : "{{ url('/') }}",
         user_ref  : "{{ Auth::user() ? Auth::user()->ref : null }}"
   };
</script>
@show

</body>
</html>
