<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
  
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="description" content="Brainduty connects you to a personal assistant or advisor in real time to accomplish tasks or have professional consultations.">
  <meta name="author" content="Brainduty">

  <title>Join us and get a personal assistant for tasks and consultations</title>

  @section('styles')
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,800,800italic">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,300,700">
  <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ URL::asset('ext/alerts/css/messenger.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/login.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/common.css') }}">
  @show
   
  <!-- Favicon -->
  <link rel="shortcut icon" href="{{ url('/favicon.ico') }}">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body class="">

<div class="mask">
  <i class="fa fa-spinner fa-spin mask-loader"></i>
</div>

<div class="countdown-wrapper">

  <header>
      <a href="{{ url('/') }}">
        <img src="{{ URL::asset('img/logo2.png') }}" alt="{{ $vw_company_name }}">
      </a>
  </header>

  <main>

    <div class="content">
       @yield('content')
    </div> <!-- /.content -->
      
    <div class="content-footer">
      <p>Don't have an account? &nbsp; <a href="{{ url('customer/signup') }}" class="">Create an Account!</a></p>
    </div>

  </main>

  <footer>
     <p style="text-align:center;">Copyright &copy; 2017 Brainduty Technologies.</p>
  </footer>

</div> <!-- /.wrapper -->

@include('widgets.modal')

@section('javascripts')
<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="{{ URL::asset('ext/alerts/js/messenger.min.js') }}"></script>
<script src="{{ URL::asset('ext/vegas/jquery.vegas.min.js') }}"></script>
<script src="{{ URL::asset('js/app.js') }}"></script>
<script src="{{ URL::asset('js/app.com.js') }}"></script>
<script src="{{ URL::asset('js/portal/page.js') }}"></script>
<script src="{{ URL::asset('js/utils.js') }}"></script>
<script>
    App.env = {
        URL_BASE  : "{{ url('/') }}",
        is_logged : false
    };

    // Stuffs needed for background images management
    var page = function() {
      "use strict";
      var initVegasBg = function() {
        $.vegas({
            src:"{{ URL::asset('img/bg/bg-login.jpg') }}",
            fade: 1000
        });
        $.vegas('overlay', {
            src:"{{ URL::asset('img/bg/overlay.png') }}"
        });
      };
      return { init: function() { initVegasBg(); } };
    } ();

    $(function () { page.init(); });
    
    $(window).load (function() {
      $('.mask').fadeOut ('fast', function() { $(this).remove() } );
    });
</script>
@show

</body>
</html>
