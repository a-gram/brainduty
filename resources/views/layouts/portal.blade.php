<!DOCTYPE html>
<!--[if lt IE 7]>      
<html class="no-js lt-ie9 lt-ie8 lt-ie7">
<![endif]-->
<!--[if IE 7]>         
<html class="no-js lt-ie9 lt-ie8">
<![endif]-->
<!--[if IE 8]>         
<html class="no-js lt-ie9">
<![endif]-->
<!--[if gt IE 8]><!--> 
<html lang="en" class="no-js">
<!--<![endif]-->
    <head>
       <title>Get a personal assistant for tasks and consultations on Brainduty</title>
       <meta charset="utf-8">
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <meta name="csrf-token" content="{{ csrf_token() }}">
       <meta name="description" content="Brainduty connects you to a personal assistant or advisor in real time to accomplish tasks or have professional consultations.">
       <meta name="author" content="Brainduty">
       @section('styles')
       <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,800,800italic">
       <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,300,700">
       <?php /* <link rel="stylesheet" href="{{URL::asset('ext/fontawesome/css/font-awesome.min.css')}}"> */ ?>
       <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
       <?php /* <link rel="stylesheet" href="{{URL::asset('ext/bootstrap/css/bootstrap.min.css')}}"> --}} */ ?>
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
       <link rel="stylesheet" href="{{ URL::asset('ext/alerts/css/messenger.css') }}">
       <link rel="stylesheet" href="{{ URL::asset('css/portal.css') }}">
       <link rel="stylesheet" href="{{ URL::asset('css/common.css') }}">
       @show
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
       <!--[if lt IE 9]>
       <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
       <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
       <![endif]-->
       <link rel="shortcut icon" href="{{ url('favicon.ico') }}">
	   
	   <!-- Google Analytics -->
       <script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-66862413-3', 'auto');
		  ga('send', 'pageview');
       </script>

    </head>
    <body class="">
        
        <!--[if lte IE 9]>
        <div class="alert alert-danger alert-dismissible" role="alert" style="margin:0;">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          Your browser is outdated and this site may not work properly. Please consider upgrading to a modern browser, such as Chrome, Opera, Firefox or IE 10+
        </div>
        <![endif]-->

       <div id="wrapper">
           
          <header class="navbar" role="banner">
             <div class="container">
                <div class="navbar-header">
                   <a href="{{ url('/') }}" class="navbar-brand navbar-brand-img">
                   <img src="{{ URL::asset('img/logo.png') }}" alt="Brainduty">
                   </a>
                   <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                     <span class="sr-only">Toggle navigation</span>
                     <i class="fa fa-bars"></i>
                     <i class="fa fa-circle alert"></i>
                   </button>
                </div>
                <!-- /.navbar-header -->
                <nav class="collapse navbar-collapse" role="navigation">
                   <ul class="nav navbar-nav navbar-right mainnav-menu">
                      <li class="dropdown">
                         <a href="/">
                         Home
                         </a>
                      </li>
                      <li class="dropdown ">
                         <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                         Explore
                         <i class="fa fa-caret-down navbar-caret"></i>
                         </a>
                         <ul class="dropdown-menu" role="menu">
                            <li>
                               <a href="{{ url('learn') }}">
                               <i class="fa fa-angle-double-right dropdown-icon"></i> 
                               Brainduty
                               </a>
                            </li>
                            <li>
                               <a href="{{ url('faq') }}">
                               <i class="fa fa-angle-double-right dropdown-icon"></i> 
                               FAQ
                               </a>
                            </li>
                         </ul>
                      </li>
                      @if (Auth::check())
                      <li class="dropdown">
                         <a class="btn-profile login" href="{{ Auth::user()->is_customer() ? url('customer/home') : url('provider/home') }}">
                         <img class="navbar-profile-avatar" alt="" src="{{ Auth::user()->get_shared_file('avatar','url') }}">
                         <span>My account</span>
                         <b class="badge badge-primary"></b>
                         </a>
                      </li>
                      @else
                      <li class="dropdown ">
                         <a href="{{ url('provider/signup') }}" class="call-action">
                         Become an assistant
                         </a>
                      </li>
                      <li class="dropdown">
                         <a class="btn-profile logout" href="{{ url('user/login') }}">
                         <span>Login</span>
                         </a>
                      </li>
                      @endif
                      
                   </ul>
                </nav>
             </div>
             <!-- /.container -->
          </header>
           
          <div class="masthead">
             @yield('hero')
          </div>
           
          <div class="content">
             @yield('content')
          </div>
          <!-- .content -->
          
       </div>
       <!-- /#wrapper -->
       
       @include('widgets.modal')
       
       <footer class="footer">
          <div class="container">
             <div class="row">
                <div class="col-sm-3">
                   <p>
                      <img src="{{ URL::asset('img/logo_mono.png') }}" alt="Brainduty">
                   </p>
                   <p></p>
                   <p></p>
                </div>
                <!-- /.col -->
                <div class="col-sm-3">
                   <div class="heading-block">
                      <h4>Keep In Touch</h4>
                   </div>
                   <!-- /.heading-block -->
                   <ul class="icons-list">
                      <li>
                         <i class="icon-li fa fa-envelope"></i>
                         <a href="{{ url('about') }}">Contact Us</a>
                      </li>
                      <li>
                         <i class="icon-li fa fa-info-circle"></i>
                         <a href="mailto:info@brainduty.com">Info & Support</a>
                      </li>
                      <?php /*
                      <li>
                         <i class="icon-li fa fa-facebook"></i>
                         <a href="https://www.facebook.com/BraindutyAU">Facebook</a>
                      </li>
                       */ ?>
                   </ul>
                </div>
                <!-- /.col -->
                <div class="col-sm-3">
                   <div class="heading-block">
                      <h4>Company</h4>
                   </div>
                   <!-- /.heading-block -->
                   <ul class="w-list">
                      <li>
                         <a href="{{ url('about') }}">About Us</a>
                      </li>
                      <li>
                         <a href="{{ url('terms') }}">Terms of Service</a>
                      </li>
                      <li>
                         <a href="{{ url('privacy') }}">Privacy Policy</a>
                      </li>
                   </ul>
                </div>
                <!-- /.col -->
                <div class="col-sm-3">
                   <div class="heading-block">
                      <h4>Explore</h4>
                   </div>
                   <!-- /.heading-block -->
                   <ul class="w-list">
                      <li>
                         <a href="{{ url('learn') }}">Platform</a>
                      </li>
                      <li>
                         <a href="{{ url('faq') }}">FAQ</a>
                      </li>
                      <li>
                         <a href="{{ url('work') }}">Work with us</a>
                      </li>
                   </ul>
                </div>
                <!-- /.col -->
             </div>
             <!-- /.row -->
          </div>
          <!-- /.container -->
       </footer>
       
       <footer class="copyright">
          <div class="container">
             <div class="row">
                <div class="col-sm-12">
                    <p style="text-align:center;">
                      Copyright &copy; 2017 Brainduty Technologies.<br>
                      <small>All rights reserved. ABN 408 289 15627</small>
                    </p>
                </div>
                <!-- /.col -->
             </div>
             <!-- /.row -->
          </div>
       </footer>
       
       @section('javascripts')
       <!-- JavaScripts -->
       <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
       <?php /* <script src="{{ URL::asset('ext/jquery/js/jquery.min.js') }}"></script> */ ?>
       <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
       <?php /* <script src="{{ URL::asset('ext/bootstrap/js/bootstrap.min.js') }}"></script> */ ?>
       <script src="{{ URL::asset('ext/alerts/js/messenger.min.js') }}"></script>
       <script src="{{ URL::asset('js/app.js') }}"></script>
       <script src="{{ URL::asset('js/app.com.js') }}"></script>
       <script src="{{ URL::asset('js/portal/page.js') }}"></script>
       <script src="{{ URL::asset('js/utils.js') }}"></script>
       <script>
          App.env = {
                is_logged : {{ Auth::check() ? 'true' : 'false' }},
                URL_BASE  : "{{ url('/') }}"
          };
       </script>
       @show
       
    </body>
</html>
