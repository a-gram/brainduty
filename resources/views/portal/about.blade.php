@extends('layouts.portal')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent

    <script src="https://maps.google.com/maps/api/js?key=AIzaSyAwJcZVzJrKopoqEgF93e8IIJwLdL11zyA&sensor=false"></script>
    <script src="{{ URL::asset('ext/misc/js/jquery.gmap.js') }}"></script>
    <script>
        $(document).ready(function() {
            App.init().run(new Portal);
        });

        $(function () {
          var mapMarkers = [{
                address: "Noble Park 3174, Australia",
                html: "<strong>Melbourne Office</strong><br>Melbourne",
                icon: {
                  image: "",
                  iconsize: [26, 46],
                  iconanchor: [12, 46]
                },
                popup: true
              }];

              // Map Initial Location
              var initLatitude = -37.815133;
              var initLongitude = 144.9617003;

              // Map Extended Settings
              var mapSettings = {
                controls: {
                  draggable: true,
                  panControl: false,
                  zoomControl: false,
                  mapTypeControl: false,
                  scaleControl: false,
                  streetViewControl: false,
                  overviewMapControl: false
                },
                scrollwheel: false,
                markers: mapMarkers,
                latitude: initLatitude,
                longitude: initLongitude,
                centre: {lat: initLatitude, lng: initLongitude},
                zoom: 16
              };

              var map = $("#googlemaps").gMap(mapSettings),
                mapRef = $("#googlemaps").data('gMap.reference');

              // Create an array of styles.
              var mapColor = "#0088cc";

              var styles = [{
                stylers: [{
                  hue: mapColor
                }]
              }, {
                featureType: "road",
                elementType: "geometry",
                stylers: [{
                  lightness: 0
                }, {
                  visibility: "simplified"
                }]
              }, {
                featureType: "road",
                elementType: "labels",
                stylers: [{
                  visibility: "off"
                }]
              }];

              var styledMap = new google.maps.StyledMapType(styles, {
                name: "Styled Map"
              });

              mapRef.mapTypes.set('map_style', styledMap);
              mapRef.setMapTypeId('map_style');
        })
    </script>

@endsection

@section('hero')
    <div class="container">
       <h1 class="masthead-subtitle">
           About Us
       </h1>
    </div> <!-- /.container -->
@endsection

@section('content')
    <div class="container">        

        <div class="row">

          <div class="col-md-7">
              
            <h2 class="no-bold">A vision is a <strong>starting point!</strong></h2>

            <p class="lead">
                <i>
                At Brainduty, we believe that in the modern society, empowered by new technological 
                tools, everyone can get assistance of any kind in practically real-time. Our vision 
                is that of a world where getting support for our day to day life is no longer the 
                privilege of a few lucky ones.
                </i>
            </p>

            <p class="lead">
                By providing a range of technological services, Brainduty's platform facilitates the 
                connection between anyone who needs personalised assistance or advices and operators in the industry. 
                Delegating chores or finding the right advice when it is really needed, 
                is a starting point towards a more productive and enjoyable life.
            </p>

            <br class="xs-20">
            
          </div> <!-- /.col -->

          <div class="col-md-5">

            <br class="xs-30">

            
            <div class="heading-block">
              <h4>Contact Us</h4>
            </div>
            
            <div id="googlemaps" class="google-map chart-holder-150"></div>

            <ul class="icons-list">
			   <li></li>
               <li>
                  <i class="icon-li fa fa-phone"></i>
                  03 9005 8158
               </li>
               <li>
                  <i class="icon-li fa fa-envelope"></i>
                  <a href="mailto:info@brainduty.com">info@brainduty.com</a>
               </li>
               <li>
                  <i class="icon-li fa fa-facebook"></i>
                  <a href="https://www.facebook.com/BraindutyAU">Facebook</a>
               </li>
            </ul>
            
            <br class="xs-30">

          </div> <!-- /.col -->

        </div> <!-- /.row -->
        
        <br class="xs-30">

      </div> <!-- /.container -->
@endsection
