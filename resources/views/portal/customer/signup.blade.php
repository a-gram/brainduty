@extends('layouts.portal')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent
    <script src="{{ URL::asset('js/portal/customer/signup.js') }}"></script>
    <script src="{{ URL::asset('ext/parsley/js/parsley.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
        	App.init().run(new Portal);
        });
    </script>
	
	<!-- Google Code AdWords -->
	<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 944524794;
		var google_conversion_language = "en";
		var google_conversion_format = "3";
		var google_conversion_color = "ffffff";
		var google_conversion_label = "bDq6CJLo1HAQ-puxwgM";
		var google_remarketing_only = false;
		/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
	<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/944524794/?label=bDq6CJLo1HAQ-puxwgM&amp;guid=ON&amp;script=0"/>
		</div>
	</noscript>
@endsection

@section('hero')
    <div class="container">
       <h1 class="masthead-subtitle">
           <i class="fa fa-user-plus"></i>
           Signup
       </h1>
    </div> <!-- /.container -->
@endsection

@section('content')
		<div class="item" style="background-color: #A10202; background-image: url(../img/bg/shattered.png);">

		  <br class="xs-30 sm-60">

		  <div class="container">

			<div class="row">
			  
			  <div class="col-md-6 masthead-text animated fadeInLeftBig">
				<h4 class="masthead-title">Need a helping hand ?</h4>

				<p>
                                    Join Us today and see how Brainduty's assistants can help you.<br>
                                    It is free to join !
				</p>

				<br>

				<div class="masthead-actions">
				  <a href="{{ url('learn') }}" class="btn btn-transparent btn-jumbo">
				  Learn More
				  </a>
				</div> <!-- /.masthead-actions -->
			  </div> <!-- /.masthead-text -->

			  <br class="xs-30 md-0">

			  <div class="col-md-6 masthead-img animated fadeInRightBig">

				<form id="signup-form"
				      class="form masthead-form well parsley-form" 
				      action="{{ url('customer') }}">

				  <h3 class="text-center">Join us for free!</h3>
				  <br>

				  <div class="form-group">
					<!-- <label class="" for="full_name">Full Name</label> -->
					<input type="text" id="first-name" class="form-control"
					       placeholder="Your first name ..."
                           data-parsley-required
                           data-parsley-required-message="Please provide your first name."
                           data-parsley-pattern="[a-zA-Z0-9 \/'.\-&]{1,64}"
                           data-parsley-pattern-message="Names can only contain letters, numbers and the symbols /'.-&">
				  </div> <!-- /.form-group -->

				  <div class="form-group">
					<!-- <label class="" for="email_address">Email Address</label> -->
					<input type="text" id="last-name" class="form-control"
					       placeholder="Your last name ..."
                           data-parsley-required
                           data-parsley-required-message="Please provide your first name."
                           data-parsley-pattern="[a-zA-Z0-9 \/'.\-&]{1,64}"
                           data-parsley-pattern-message="Names can only contain letters, numbers and the symbols /'.-&">
				  </div> <!-- /.form-group -->

				  <div class="form-group">
                              <div class="input-group">
                                 <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user fa-fw"></i></span>
					<!-- <label class="" for="organization">Organization</label> -->
					<input type="text" id="username" class="form-control"
					       placeholder="Your email ..."
                           data-parsley-required
                           data-parsley-required-message="Please provide a username. This must be a valid email."
                           data-parsley-type="email">
                              </div>
				  </div> <!-- /.form-group -->

				  <div class="form-group">
                              <div class="input-group">
                                 <span class="input-group-addon" id="basic-addon2"><i class="fa fa-lock fa-fw"></i></span>

					<!-- <label class="" for="organization">Organization</label> -->
					<input type="password" id="password" class="form-control"
					       placeholder="Your password ..."
                           data-parsley-required
                           data-parsley-required-message="Please provide a password.">
                              </div>
				  </div> <!-- /.form-group -->

				  <div class="form-group">
					<button type="submit" class="btn btn-secondary btn-jumbo btn-block submit">
                                            Sign me up<i class="fa fa-spinner fa-pulse loading"></i>
                                        </button>
				  </div> <!-- /.form-group -->
                    <p class="text-center small">
                       By clicking "Sign me up" you agree to Brainduty's <a href="{{ url('terms') }}">Terms</a>
					   and <a href="{{ url('privacy') }}">Privacy Policy</a>
                    </p>

				</form>    

				<br class="xs-80">
                                
			  </div> <!-- /.masthead-img -->

			</div> <!-- /.row -->

		  </div> <!-- /.container -->

		</div> <!-- /.item -->
@endsection
