@extends('layouts.portal')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            App.init().run(new Portal);
        });
    </script>
@endsection

@section('hero')
    <div class="container">
       <h1 class="masthead-subtitle">
           <i class="fa fa-question-circle"></i>
           FAQ
       </h1>
    </div> <!-- /.container -->
@endsection

@section('content')
    <div class="container">
       <div class="layout layout-stack-sm layout-main-left">
          <div class="col-sm-8 layout-main">
             
             <!-- TOPIC DUTIES -->
             
             <div class="portlet">
                <div class="heading-block">
                   <h3>Duties</h3>
                </div>
                <div id="Q-1" class="panel-group accordion-panel">
                    
                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-1" href="#Q-1-1">
                                 What is a duty ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-1-1" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                A duty is simply a request for some task or consultation that needs to be 
								fulfilled by our assistants. We call it a "duty".
								You can request any kind of task that can be done remotely and does not
								require the physical presence of the assistant.
                            </p>
                         </div>
                      </div>
                   </div>
                   
                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-1" href="#Q-1-2">
                                Who are the assistants ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-1-2" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                Generally speaking, we call our service providers <i>assistants</i>.
                                However, specifically, on Brainduty there are generic assistants and expert assistants.
                                Generic assistants provide services that do not need specific professional qualifications and
                                can perform a broad range of generic tasks, such as administrative duties, researches, organizing,
                                writing, scheduling, etc., much like a secretary. On the other hand, expert assistants provide
                                services that can only be rendered by expert with specific professional skills, such as advisory,
                                consulting, reviewing, designing, etc. They posses relevant qualifications and/or certifications
                                that we verify when they join the platform.
                            </p>
                         </div>
                      </div>
                   </div>
                   
                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-1" href="#Q-1-3">
                                Do i have to make an appointment to request a duty ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-1-3" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                No. Duties are assigned and performed as soon as they are matched to a suitable available assistant.
                            </p>
                         </div>
                      </div>
                   </div>

                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-1" href="#Q-1-4">
                                Can i choose the assistant for my duties ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-1-4" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                Brainduty's goal is to provide on-demand services to customers quickly and seamlessly when 
                                they are requested. We do not require our customers to be hiring managers and go 
                                through a tedious and time consuming selection process. We take care of that 
                                using our technology. However, you have the ability to put assistants that you have
                                been happy with, in your favourites list and indicate that you would like the duty
                                be assigned to a specific assistant when you make a request. Keep in mind, however, that we
                                cannot guarantee that the assistant will be able to take on your duty.
                            </p>
                         </div>
                      </div>
                   </div>

                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-1" href="#Q-1-5">
                                How can i cancel a duty ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-1-5" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                Normally, cancelling a duty that has already been assigned to an assistant 
                                is discouraged if it is done unilaterally, that is
                                without the other party agreeing. This is to avoid disruption in the engagement
                                of both customers and assistants, compromising the overall user experience. If you
                                want to cancel an ongoing duty then the recommended way is to discuss it
                                with the other party and make a cancellation in mutual agreement. Please see
                                the Cancellation Request question for more info.<br><br>
                                Users who unilaterally cancel an assigned duty may incur some kind of penalty
                                as a form of compensation for the resources invested so far by the other party.
                                Customers may be charged an amount (up to the full price of the duty, depending 
                                on the assistant's cancellation policy) and assistants may not get paid and compromise their ranking.<br>
                                Duties that have been requested but not yet assigned can be cancelled anytime with no penalties.
                            </p>
                         </div>
                      </div>
                   </div>

                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-1" href="#Q-1-6">
                                What is a Cancellation Request ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-1-6" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                A Cancellation Request is a message sent to the other party
                                to request the cancellation of an assigned duty in mutual agreement.
                                Both customers and assistants can send this message, which must be accepted by the 
                                counterpart in order for the duty to be cancelled without incurring any penalties. 
                                If the other party does not respond to the request within 
                                {{ \App\Hey::dt_to_human( \App\Hey::get_app('intervals.task.cancel_request_expired') ) }}, 
                                then it is considered unresponsive and the duty can be freely cancelled.
                            </p>
                         </div>
                      </div>
                   </div>
                   
                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-1" href="#Q-1-7">
                                What is a Charge Request ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-1-7" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                A Charge Request is a request sent by the assistant to authorize additional
                                payments for extra slots and other expenses that are required in order to
                                fullfill the duty. You may approve or refuse these extra charges.
                            </p>
                         </div>
                      </div>
                   </div>
                   
                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-1" href="#Q-1-8">
                                What is an End Duty Request ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-1-8" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                An End Duty Request is a request sent by the assistant to end a duty when
                                it is considered fullfilled.
                                If you accept the request then the duty will be immediately ended and the
                                due amounts charged to your default payment method. If you do not want to
                                accept to end the duty then you can dispute it and specify the reasons why
                                you think the assistant should not get paid.<br>
                                <b>NOTE:</b> if you do not answer to the request within 
                                {{ \App\Hey::dt_to_human( \App\Hey::get_app('intervals.task.end_request_expired') ) }},
                                then you are deemed as having accepted to end the duty.
                            </p>
                         </div>
                      </div>
                   </div>
                   
                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-1" href="#Q-1-9">
                                Can i dispute a duty ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-1-9" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
							    You can dispute a duty if there is a compelling reason to do so.<br>
                                For example, if you think the assistant has committed some abuse and/or fraud 
								against you, or the service was not provided or was clearly not fullfilled as requested, 
                                then you can file a dispute and we'll give you a refund if we find your claims are valid.
                                You have 
                                {{ \App\Hey::dt_to_human( \App\Hey::get_app('intervals.task.disputable') ) }} 
                                after the duty has ended to dispute it.
                                If the assistant has fulfilled the duty as per request and you are not satisfied, 
                                you have the ability to give the assistant a low rating but may not get a refund.
                            </p>
                         </div>
                      </div>
                   </div>
                   
                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-1" href="#Q-1-10">
                                Why am I unable to dispute a duty ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-1-10" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                If you are trying to dispute a duty but are unable to do so then it means
                                the duty is no longer disputable (e.g. the dispute period has elapsed).
                            </p>
                         </div>
                      </div>
                   </div>

                </div>
             </div>
             
             <br class="xs-20">
             
             <!-- TOPIC PAYMENT -->
             
             <div class="portlet">
                <div class="heading-block">
                   <h3>Payment</h3>
                </div>
                <div id="Q-2" class="panel-group accordion-panel">
                    
                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-2" href="#Q-2-1">
                                What am I charged ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-2-1" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                Duties are charged in 20-minute <b>slots</b>.
                                A slot is the minimum amount of time (and money) for an assistant to take on the duty.
                                You are initially charged one slot and if the duty can be accomplished in one slot 
                                then that's all you pay, otherwise the assistant may determine additional slots
                                and other expenses needed in order to complete the duty, which must be approved by you.
                            </p>
                         </div>
                      </div>
                   </div>

                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-2" href="#Q-2-2">
                                How do i pay for a duty ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-2-2" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                In order to request a duty you must have a valid payment method on file.
                                To register a payment method with your account, you can do it beforehand
                                in the <strong>Settings > Payment</strong> section of your dashboard. If there is no payment
                                method on file at the time of requesting a duty, you will be asked to provide
                                one along the process in order to proceed.
                            </p>
                         </div>
                      </div>
                   </div>

                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-2" href="#Q-2-3">
                                What payment methods do you accept ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-2-3" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                Currently, only credit/debit cards are accepted.
                            </p>
                         </div>
                      </div>
                   </div>

                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-2" href="#Q-2-4">
                                Can i get a refund ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-2-4" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                Refunds can only be given on disputed duties resolved in your favour. In such
                                a case the duty will be cancelled and you will get a refund for all the services 
                                charged.
                            </p>
                         </div>
                      </div>
                   </div>

                </div>
             </div>

          </div>
          <!-- /.layout-main -->
          
          <div class="col-sm-4 layout-sidebar">
             <hr class="visible-xs">
             <br class="xs-20 sm-0">
             <div class="well text-center">
                <p><i class="fa fa-question-circle fa-5x text-muted"></i></p>
                <h4>Have a Question?</h4>
                <p>Do not esitate to ask.</p>
                <a href="mailto:info@brainduty.com" class="btn btn-secondary">Get it Answered!</a>
             </div>
             <!-- /.well -->
          </div>
          <!-- /.layout-sidebar -->
          
       </div>
       <!-- /.layout -->          
    </div>
    <!-- /.container -->
@endsection
