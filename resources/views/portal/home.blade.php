@extends('layouts.portal')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent
    <script src="{{ URL::asset('js/portal/home.js') }}"></script>

    <script>
        $(document).ready(function() {
             App.init().run(new Portal);
        });
    </script>
@endsection

@section('hero')
    <div id="masthead-carousel" class="carousel slide carousel-fade masthead-carousel">
      <div class="carousel-inner">

        <div class="item active" style="background-color: #00213e; background-image: url('img/bg/shattered.png');">

          <br class="xs-30 sm-60">

          <div class="container">

            <div class="row">

              <div class="col-md-6 masthead-text animated fadeInDownBig">
                <h1 class="masthead-title">Assistance on the go</h1>

                <p class="masthead-lead">
                Brainduty helps you meet your goals and have peace of mind
                with personalised and professional assistance.
                </p>

                <br>

                <div class="masthead-actions">
                  <a href="{{ URL::to('/learn') }}" class="btn btn-transparent btn-jumbo">
                  Learn More
                  </a>
                  <a href="{{ URL::to('/customer/signup') }}" class="btn btn-primary btn-jumbo">
                  Try it now
                  </a>
                </div> <!-- /.masthead-actions -->

              </div> <!-- /.masthead-text -->

              <br class="xs-30 md-0">

              <div class="col-md-6 masthead-img animated fadeInUpBig">
                <img src="img/masthead/mobile.png" alt="slide2" class="img-responsive" />     
              </div> <!-- /.masthead-img -->

            </div> <!-- /.row -->

          </div> <!-- /.container -->

        </div> <!-- /.item -->
        
      </div>  <!-- /.carousel-inner -->
      <!-- Carousel nav -->

    </div> <!-- /.masthead-carousel -->
@endsection

@section('content')

    <section id="section-benefits" class="home-section" style="background-color: #f3f3f3;">
        
        <br class="sm-50">

        <div class="container">

            <div class="row">

              <div class="col-sm-4">
                <div class="feature-sm">
                  <i class="fa fa-rocket feature-sm-icon text-secondary"></i>

                  <h3 class="feature-sm-label">Instant support</h3>

                  <p class="feature-sm-content">
                    Access a team of professional assistants who are ready to help
                    you with personalised support and expert advises. All it takes 
                    is the tap of a button.
                  </p>
                </div> <!-- /.feature-sm -->
              </div> <!-- /.col -->

              <div class="col-sm-4">
                <div class="feature-sm">
                  <i class="fa fa-comments-o feature-sm-icon text-secondary"></i>

                  <h3 class="feature-sm-label">Full control</h3>

                  <p class="feature-sm-content">
                    Fast and easy. Using real-time technology services such as 
                    instant messaging, notifications and cashless payments, the 
                    whole process is seamless and secure.
                  </p>
                </div> <!-- /.feature-sm -->
              </div> <!-- /.col -->

              <div class="col-sm-4">
                <div class="feature-sm">
                  <i class="fa fa-credit-card feature-sm-icon text-secondary"></i>

                  <h3 class="feature-sm-label">Secure payments</h3>

                  <p class="feature-sm-content">
                    Pay with confidence knowing upfront what you will be charged,
                    with no commitments and cashless transactions. Only pay for
                    what you really need.
                  </p>
                </div> <!-- /.feature-sm -->
              </div> <!-- /.col -->

            </div> <!-- /.row -->

        </div> <!-- /.container -->

    </section>

    <section id="section-features" class="home-section">

    <br class="sm-30">

    <div class="container">

      <div class="heading-block heading-minimal heading-center">
        <h1>
          How does it work ?
        </h1>
      </div> <!-- /.heading-block -->
      
      <div class="row how-works">
          <div class="col-md-4">
              <img class="img-circle img-responsive img-center" src="{{ URL::asset('img/homepage-features/step-1.png') }}" alt="">
              <h3>1. Make a request</h3>
              <p>
                  Getting assistance has never been so easy. Just request a <a href="{{ url('faq#Q-1') }}">duty</a> with a simple text message 
                  specifying what you need. Attach support documents if any, and submit your request. That's it.<br><br>
                  <a href="{{ url('learn') }}" class="btn btn-default">Learn More</a>
              </p>
              
          </div>
          <div class="col-md-4">
              <img class="img-circle img-responsive img-center" src="{{ URL::asset('img/homepage-features/step-2.png') }}" alt="">
              <h3>2. Get assistance</h3>
              <p>
                  Within minutes an assistant will be assigned to you and will take care of your duty.
                  Whether it's a generic task or a professional consultation, we have you covered. No need to make 
                  appointments, meetings or long waiting lists.<br><br>
                  <a href="{{ url('learn') }}" class="btn btn-default">Learn More</a>
              </p>
          </div>
          <div class="col-md-4">
              <img class="img-circle img-responsive img-center" src="{{ URL::asset('img/homepage-features/step-3.png') }}" alt="">
              <h3>3. Enjoy :)</h3>
              <p>
                 Life is better when you get the help you need. Enjoy what you love the most, keep focusing on your 
                 daily goals, or just have peace of mind with that expert advise and opinion that you were looking for. 
                 <br><br>
                 <a href="{{ url('learn') }}" class="btn btn-default">Learn More</a>
              </p>
          </div>
      </div>

      </div> <!-- /.container -->

    </section>
<?php /*
    <section id="section-benefits" class="home-section" style="background-color: #f3f3f3;">
        <div class="container">

          <div class="heading-block heading-minimal heading-center">
            <h1>
              What our customers are saying
            </h1>
          </div> <!-- /.heading-block -->

          <div class="row">

            <div class="col-sm-4">

              <div class="testimonial">

                <div class="testimonial-icon bg-secondary">
                  <i class="fa fa-quote-left"></i>
                </div> <!-- /.testimonial-icon -->

                <div class="testimonial-content">
                  Brainduty is a valuable tool in my small business for marketing campaigns and direct sales.
                  Using their services as needed saves me from hiring dedicated staff which 
                  would cost me much more.
                </div> <!-- /.testimonial-content -->

                <div class="testimonial-author">
                  <div class="testimonial-image"><img alt="" src="{{ URL::asset('img/testimonials/1.png') }}"></div>

                  <div class="testimonial-author-info">
                    <h5><a href="#">Michelle Tah</a></h5> Seller
                  </div>
                </div> <!-- /.testimonial-author -->

              </div> <!-- /.testimonial -->

              <br class="xs-30">

            </div> <!-- /.col -->
            
            <div class="col-sm-4">

              <div class="testimonial">

                <div class="testimonial-icon bg-secondary">
                  <i class="fa fa-quote-left"></i>
                </div> <!-- /.testimonial-icon -->

                <div class="testimonial-content">
                  Since becoming a full time mom i have no more time to hit the gym
                  to stay fit. I used a Brainduty expert assistant to get a fitness
                  program tailored to my needs that i can do at home. It works great!
                </div> <!-- /.testimonial-content -->

                <div class="testimonial-author">
                  <div class="testimonial-image"><img alt="" src="{{ URL::asset('img/testimonials/3.png') }}"></div>

                  <div class="testimonial-author-info">
                    <h5><a href="#">Sharon Ruth</a></h5> Mom
                  </div>
                </div> <!-- /.testimonial-author -->

              </div> <!-- /.testimonial -->

            </div> <!-- /.col -->

            <div class="col-sm-4">

              <div class="testimonial">

                <div class="testimonial-icon bg-secondary">
                  <i class="fa fa-quote-left"></i>
                </div> <!-- /.testimonial-icon -->

                <div class="testimonial-content">
                  As an import-export broker, i need to scour the internet often to find 
                  the suppliers with the best rates. Brainduty is the perfect service to 
                  assist me in the process. It's great value for money.
                </div> <!-- /.testimonial-content -->

                <div class="testimonial-author">
                  <div class="testimonial-image"><img alt="" src="{{ URL::asset('img/testimonials/2.png') }}"></div>

                  <div class="testimonial-author-info">
                    <h5><a href="#">Tom Collin</a></h5> Broker
                  </div>
                </div> <!-- /.testimonial-author -->

              </div> <!-- /.testimonial -->

              <br class="xs-30">

            </div> <!-- /.col -->

          </div> <!-- /.row -->

        </div> <!-- /.container -->
    </section>
*/ ?>
    <section id="section-contact" class="home-section" style="padding: 65px 0; background: url('img/work/banner.jpg') 0 0 /100% no-repeat #eceff4;">

        <div class="container">

          <div class="row">
            <div class="col-sm-8">
              <h2>Work with us</h2>
              <p class="lead">
                  We are onboarding new assistants, right now!
              </p>
              <a href="{{ url('provider/signup') }}" class="btn btn-primary btn-lg">&nbsp;&nbsp;&nbsp;&nbsp;Become an assistant&nbsp;&nbsp;&nbsp;&nbsp;</a>
            </div>

            <div class="col-sm-4 text-center">
              
            </div>
          </div>

        </div> <!-- /.container -->

    </section>

@endsection
