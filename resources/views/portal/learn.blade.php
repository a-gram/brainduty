@extends('layouts.portal')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            App.init().run(new Portal);
        });
    </script>
@endsection

@section('hero')
    <div class="container">
       <h1 class="masthead-subtitle">
           <i class="fa fa-users"></i>
           Community
       </h1>
    </div> <!-- /.container -->
@endsection

@section('content')
    <section class="content-feature">

      <div class="container">

        <p class="lead">
           Brainduty connects you to an assistant or advisor in real-time for on-demand tasks
           and consultations. Be more productive and focus on what really matters to you by 
           delegating work, or have peace of mind by getting that professional advise 
           you were looking for. Get support whenever you need it, and pay as you go.
        </p>
        
        <br class="xs-50 lg-70">
        
        <div class="feature-lg figure-right">
          <div class="row">
            <div class="col-sm-6 col-sm-push-6">
              <figure class="feature-figure">
                  <img src="./img/homepage-features/feature-2.png" class="img-responsive figure-shadow center-block"  alt="">
              </figure>
            </div><!-- /.col -->
            <div class="col-sm-6 col-sm-pull-6">
              <br class="xs-30 sm-0">
              <div class="feature-content">
                <h2>Generic or expert assistance</h2>
                <p class="synopsis">
                    Whether you need assistance with a casual generic duty, a consultation with a 
                    specialist, or simply an opinion from an expert's point of view, we have you covered. 
                    Just specify it in your request.
                </p>
                <a href="{{ url('customer/signup') }}" class="btn btn-default">Try it now!</a>
              </div> <!-- /.feature-content -->
            </div><!-- /.col -->
          </div> <!-- /.row -->
        </div> <!-- /.feature-lg -->

        <br class="xs-50 lg-100">

        <div class="feature-lg">
          <div class="row">
            <div class="col-sm-6">
              <figure class="feature-figure">
                  <img src="./img/homepage-features/feature-1.png" class="img-responsive figure-shadow center-block" alt="">
              </figure>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <br class="xs-30 sm-0">
              <div class="feature-content">
                <h2>Get assistance at the tap of a button</h2>
                <p class="synopsis">
                    All you have to do to get in touch with an assistant is tell us what
                    you need by typing a message and hit a button. That's it. Based on your requirements, 
                    an assistant will be assigned to your duty within minutes. Say goodbye to making 
                    appointments, meetings, hiring and all the tedious stuff.
                </p>
                <a href="{{ url('customer/signup') }}" class="btn btn-default">Try it now!</a>
              </div> <!-- /.feature-content -->
            </div><!-- /.col -->
          </div> <!-- /.row -->
        </div> <!-- /.feature-lg -->

        <br class="xs-50 lg-100">

        <div class="feature-lg figure-right">
          <div class="row">
            <div class="col-sm-6 col-sm-push-6">
              <figure class="feature-figure">
                  <img src="./img/homepage-features/feature-4.png" class="img-responsive figure-shadow center-block"  alt="">
              </figure>
            </div><!-- /.col -->
            <div class="col-sm-6 col-sm-pull-6">
              <br class="xs-30 sm-0">
              <div class="feature-content">
                <h2>Only pay for what you really need</h2>
                <p class="synopsis">
                   With Brainduty, you are in control of what you want to pay. It all starts
                   with a <a href="{{url('faq')}}">slot</a>, and that is all you pay if it's 
                   enough to accomplish your duty. If the duty requires extra time and/or expenses, then they will be added 
                   as needed. Generic duties are charged $7.50 per slot and Expert duties from $19.90 per slot.
				   See our <a href="{{url('faq')}}">FAQ</a> page for more info on what you are charged.
                </p>
                <a href="{{ url('customer/signup') }}" class="btn btn-default">Try it now!</a>
              </div> <!-- /.feature-content -->
            </div><!-- /.col -->
          </div> <!-- /.row -->
        </div> <!-- /.feature-lg -->

        <br class="xs-50 lg-100">

        <div class="feature-lg">
          <div class="row">
            <div class="col-sm-6">
              <figure class="feature-figure">
                  <img src="./img/homepage-features/feature-5.png" class="img-responsive figure-shadow center-block"  alt="">
              </figure>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <br class="xs-30 sm-0">
              <div class="feature-content">
                <h2>Real-time management from start to end</h2>
                <p class="synopsis">
                   Manage your duties from your account using our simple and easy-to-use
                   interface. The whole process is conversational and real-time, meaning
                   you can interact with your assistant through the platform's integrated 
                   chat as the job progresses, share documents, files and make transactions.
                </p>
                <a href="{{ url('customer/signup') }}" class="btn btn-default">Try it now!</a>
              </div> <!-- /.feature-content -->
            </div><!-- /.col -->
          </div> <!-- /.row -->
        </div> <!-- /.feature-lg -->

        <br class="xs-50 lg-100">

        <div class="feature-lg figure-right">
           <div class="row">
             <div class="col-sm-6 col-sm-push-6">
               <figure class="feature-figure">
                   <img src="./img/homepage-features/feature-3.png" class="img-responsive figure-shadow center-block" alt="">
               </figure>
             </div><!-- /.col -->
             <div class="col-sm-6 col-sm-pull-6">
               <br class="xs-30 sm-0">
               <div class="feature-content">
                 <h2>We are local!</h2>
                 <p class="synopsis">
                     Studies show that customer satisfaction increases when services are provided by someone
                     local. Our assistants speak your language, know the local mindset, culture, way of dealing with people
                     and doing business. Avoid cultural barriers, awkward conversations and sloppy jobs.
                 </p>
                 <a href="{{ url('customer/signup') }}" class="btn btn-default">Try it now!</a>
               </div> <!-- /.feature-content -->
             </div><!-- /.col -->
           </div> <!-- /.row -->
         </div> <!-- /.feature-lg -->

         <br class="xs-50 lg-100">
      
      </div>

    </section>
@endsection
