@extends('layouts.portal')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            App.init().run(new Portal);
        });
    </script>
@endsection

@section('hero')
    <div class="container">
       <h1 class="masthead-subtitle">
           Privacy Policy
       </h1>
    </div> <!-- /.container -->
@endsection

@section('content')
    <div class="container">
        
        <section class="privacy-policy">
            <br class="xs-20">
            <p>
Brainduty ("We", "Us", "Our", etc.) understands the concerns that may arise from submitting personal information 
to websites and in the following sections we clearly outline what kind of information 
we collect in connection with your use of our websites, applications and technology services (collectively, the "Platform")
and how we use it under the Australian Privacy Principles. This Privacy Policy applies to both users who request 
services (the "Customers") and users who provide services through the Platform (the "Assistants").
            </p>
            <br class="xs-20">
            <h3>WHAT PERSONAL INFORMATION WE COLLECT</h3>
<p>
Although some parts and features of the Platform may be accessed without collecting personal information,
in order to provide you with the products and services that we offer we may collect several types
of individual personal information as part of the normal operations of our business.<br><br>
</p>

<h4>Registration Information</h4>

<p>When you register with our Platform for a user account we may collect the following information:</p>

<ul>
  <li>Full Name</li>
  <li>Date of birth</li>
  <li>Address</li>
  <li>Email address</li>
  <li>Phone number</li>
  <li>Business name and number (for businesses only)</li>
</ul>

<p>
If you sign up using third party services (Facebook, Google+, etc.) we may additionally collect
the following information provided by such third party services:
</p>

<ul>
  <li>Picture</li>
  <li>Gender</li>
  <li>List of Friends</li>
</ul>

<p>
These information are used to enhance the customer experience and to verify your identity. Your contact 
details, such as email address and phone number, will be used to send notifications necessary to keep
your account operational (address verification, password resets, status of purchased services, etc.).
Additionally, your email address may be included in mailing lists used to send promotional material about 
our services and/or products. You will have the ability to opt-out and be unsubscribed from such mailing lists.<br><br>
</p>

<h4>Payment Information</h4>

<p>
When you register a payment method, either to pay for goods or services, or to receive payments, we may
collect the following information:
</p>

<ul>
  <li>Credit card details</li>
  <li>Bank account details</li>
</ul>

<p>
These information are used to charge you for purchases you make on the Platform or to
send funds to your bank account for services or goods sold by you on the Platform.
NOTE: We do not keep any of these payment information on file in our systems but send them
to our payment processor where they are encrypted and stored according to
current security standards.<br><br>
</p>

<h4>Assistant Identity Information</h4>

<p>
When a user registers with the Platform as an Assistant, there are certain information that 
we are required by law to collect for identity verification. In addition to the personal information listed 
above, we may also collect the following:
</p>

<ul>
  <li>Identity Document <small>(passport, driver license, birth certificate, etc.)</small></li>
  <li>Proof of residence <small>(utility bills, rental contracts, bank statements, etc.)</small></li>
  <li>Proof of qualifications/skills <small>(certificates, professional registration records, etc.)</small></li>
</ul>

<br>

<h4>Browsing information</h4>

<p>
For each visitor of our Platform we may collect a number of Non-Personal Information including, but not
limited to, the following:
</p>

<ul>
  <li>IP Address</li>
  <li>Web Browser Type</li>
  <li>Device Type</li>
  <li>Operating System</li>
  <li>Language</li> 
  <li>Pages viewed</li>
</ul>

<p>
These information are used solely internally for the purpose of gauging visitor traffic, 
trends and delivering personalized content to you while using our Platform.
</p>
        <br class="xs-20">
        <h3>HOW WE USE YOUR PERSONAL INFORMATION</h3>
<p>
We only use and disclose information collected from individuals that access and use our Platform with the
sole purpose of conducting our business. This includes but is not limited to:<br>
</p>

<ul>
  <li>Providing and administering our products and/or services</li>
  <li>Processing payments</li>
  <li>Conducting customer support activities</li>
  <li>Communications with customers and other service providers</li>
  <li>Operating competitions, promotions and events</li>
  <li>Conducting marketing activities and internal analyses</li>
  <li>Conducting background checks</li>
</ul>
        <br class="xs-20">
        <h3>DISCLOSURE OF PERSONAL INFORMATION</h3>
<p>
We may disclose your personal information to others in connection with the sale and provision of our
products and services, including:<br>
</p>

<ul>
  <li>Law enforcement and/or background check services</li>
  <li>Payment processors and financial institutions</li>
  <li>Customer support services</li>
  <li>State and/or Federal agencies</li>
  <li>Other users of the Platform</li>
</ul>

<p>
Brainduty only discloses to third parties the necessary amount of personal information
they need to deliver the service. We may disclose some information to other users of the platform
in order to provide the services that you requested. For example, some details may be disclosed to
Assistants who will fulfill your requests, or to any other user if you post information in a public
section of the Platform.
</p>
        <br class="xs-20">
        <h3>SECURITY</h3>
        <p>
Brainduty are committed to ensuring that your Personal Information is secure in order 
to prevent unauthorized access or disclosure. Your Personal Information is transmitted over
a secure connection that uses 256-bit Secure Socket Technology (SSL/TLS) and stored in secure computing environments 
provided by third party services that comply with the highest industry security standards. All
documents provided for identity verification are encrypted and access is password-protected.
        </p>
        <br class="xs-20">
        <h3>COOKIES</h3>
        <p>
As with most business platforms, Brainduty uses cookies to improve the user experience.
Cookies are small text files stored on your device which hold information about how you use a website, 
including your browsing preferences, login sessions identifiers, etc. You may decide not to allow 
cookies to be stored on your computer by selecting specific settings in your web browser. If you 
decide to refuse cookies, your use of our Platform may be limited and some parts of it may not be accessed.
        </p>
        <br class="xs-20">
        <h3>THIRD PARTY LINKS</h3>
        <p>
Our Platform may contain links to third party websites. Please be aware that we are not responsible 
for the privacy practices of such third party sites. When you go to other websites from here, our Privacy Policy
terms no longer apply, so we strongly encourage you to read their Privacy Policy.
        </p>
        <br class="xs-20">
        <h3>AVAILABILITY AND CHANGES</h3>
        <p>
Our Privacy Policy is publicly available on our website and should be read in conjunction with our
Terms and Conditions of Sale. Brainduty reserves the right to make amendments to this Privacy Policy 
at any time without notice. It is your responsibility to check this page back whenever you use our
Platform. If you do not agree to this Privacy Policy then you should not access or use our Platform.
        </p>
        <br class="xs-20">
        <h3>ACCESS</h3>
        <p>
To the extent permitted by applicable law, you have the right to access your personal information and
we will provide you with access to the information we hold about you within a reasonable period of time
and at no cost whenever possible. However, depending on the amount of resources and time required to
gather the information you need, a fee may be incurred to recover the costs of supplying this information.
All access requests must be put in writing by contacting us using the contact details in the 'Contact us'
section below.
        </p>
        <br class="xs-20">
        <h3>CONTACT US</h3>
        <p>

Should you have any questions, you can <a href="mailto:info@brainduty.com">write to us</a>
        </p>
        
        </section>
        
    </div>
    <!-- /.container -->
@endsection
