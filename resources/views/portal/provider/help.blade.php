@extends('layouts.portal')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent
    <script>
        $(document).ready(function() {
            App.init().run(new Portal);
        });
    </script>
@endsection

@section('hero')
    <div class="container">
       <h1 class="masthead-subtitle">
           Help
       </h1>
    </div> <!-- /.container -->
@endsection

@section('content')
    <div class="container">
       <div class="layout layout-stack-sm layout-main-left">
          <div class="col-sm-8 layout-main">
             
             <!-- TOPIC DUTIES -->
             
             <div class="portlet">
                <div class="heading-block">
                   <h3>Duties</h3>
                </div>
                <div id="Q-1" class="panel-group accordion-panel">
                    
                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-1" href="#Q-1-1">
                                 What is a duty ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-1-1" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                A duty is simply a request from a customer for some task or consultation 
                                that needs to be fulfilled by the assistants. We call it a "duty".
                            </p>
                         </div>
                      </div>
                   </div>
                   
                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-1" href="#Q-1-2">
                                How can i take on duties?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-1-2" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                Brainduty automatically assigns duties to available assistants based on
                                customers' preferences when requests are made. You will be notified every
                                time there is a new request and must confirm your availability by replying 
                                to the notification in order to be able to work on the duty.<br>
                                Another way you can work on duties is if a customer explicitly indicates that
                                they want to assign their request to you because they have already worked with
                                you previously.
                            </p>
                         </div>
                      </div>
                   </div>
                   
                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-1" href="#Q-1-3">
                                How can i work on expert/professional duties ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-1-3" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                To take on duties requiring professional expertise you must prove that you have
                                the relevant professional qualifications to do so. During the identity verification
                                process you will have to submit proof of your qualifications by providing us
                                with documentation and/or information that allow us to verify your claims.
                                Please see the Verification section for a list of accepted proof of expertise.
                            </p>
                         </div>
                      </div>
                   </div>

                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-1" href="#Q-1-4">
                                Why am i not being assigned new duties ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-1-4" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                There may be several reasons why you are not being assigned new duties.
                                It may be one or more of the following:<br>
                            </p>
                            <ol>
                                <li>A long period of inactivity on the platform (not logged in for a long time). 
                                    In this case the system considers you as not available.</li>
                                <li>You may already be working on other duties, which limits your availablity, and 
                                    there are other assistants free of work.</li> 
                                <li>Your ranking may be compromised by a low feedback and/or other actions that are 
                                    explicitly discouraged or prohibited.</li>
                                <li>Your account might have been subject to restrictions.</li>
                            </ol>
                         </div>
                      </div>
                   </div>

                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-1" href="#Q-1-5">
                                How can i cancel a duty ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-1-5" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                Normally, cancelling a duty that has already been assigned to you 
                                is discouraged if it is done unilaterally, that is without the customer agreeing. 
                                This is to avoid disruption in the engagement of customers with the platform. If you
                                want to cancel an ongoing duty then the recommended way is to discuss it
                                with the customer and make a cancellation in mutual agreement. Please see
                                the Cancellation Request question for more info.<br><br>
                                Assistants who unilaterally cancel an assigned duty will not get paid for any services
                                associated with that duty and may have their ranking compromised.
                            </p>
                         </div>
                      </div>
                   </div>
                   
                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-1" href="#Q-1-6">
                                How can i end a duty ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-1-6" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                You can end a duty if you deem it has been fullfilled as per customer request
                                by sending a "Deliverables message" to the customer. A Deliverables message is
                                a special message to which you can attach documents, files or anything that should
                                be given to the customer as an outcome of the duty. This message is not immediately
                                sent to the customer but is kept "on hold" until payment for the duty is received,
                                upon which it will be delivered.<br><br>
                                Posting this message will notify the customer that you deem the duty completed
                                and will ask him/her whether to accept or refuse. If the customer accepts the duty
                                is ended, the customer charged and the deliverables released. If the customer
                                refuses then the duty will be disputed.
                            </p>
                         </div>
                      </div>
                   </div>

                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-1" href="#Q-1-7">
                                What is a Cancellation Request ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-1-7" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                A Cancellation Request is a message sent to the other party
                                to request the cancellation of an assigned duty in mutual agreement.
                                Both customers and assistants can send this message, which must be accepted by the 
                                counterpart in order for the duty to be cancelled without incurring any penalties. 
                                If the other party does not respond to the request within 
                                {{ $vw_cancel_req_exp }}, 
                                then it is considered unresponsive and the duty can be freely cancelled.
                            </p>
                         </div>
                      </div>
                   </div>
                   
                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-1" href="#Q-1-8">
                                What is a Charge Request ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-1-8" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                A Charge Request is a message sent to the customer to ask for the authorization
                                of additional charges that may be needed to fullfill the duty. This feature must
                                be used only in all those cases where you have incurred or are expected to incur 
                                expenses that are relevant and necessary to the duty. Any abuse may lead to the
                                suspension and/or termination of your account.
                            </p>
                         </div>
                      </div>
                   </div>

                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-1" href="#Q-1-9">
                                What if a customer cancels an ongoing duty ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-1-9" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                If a customer unilaterally cancels an ongoing duty you have the ability to
                                charge an amount, which is one of the following: 
                            </p>
                            <ol>
                                <li>A fixed fee</li>
                                <li>The services used up to the cancellation time</li>
                            </ol>
                            <p>
                                You can also choose not to charge any amount. Note that by default customers are not charged anything.
                                You will have to set your cancellation policy in your account. The cancellation policy can be set
                                in your Settings > Account page.<br>
                                Currently, the fixed cancellation fee is set to
                                {{ $vw_task_cancel_fee }}.
                            </p>
                         </div>
                      </div>
                   </div>

                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-1" href="#Q-1-10">
                                A customer has disputed a duty. What happens now ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-1-10" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                If we deem that the dispute was made on valid grounds and it is resolved in the
                                customer's favour then the duty will be cancelled and all payments made
                                refunded to the customer.<br>
                                If we resolve the dispute in your favour then the duty will be deemed fullfilled,
                                closed and all payments released to your account.
                            </p>
                         </div>
                      </div>
                   </div>
                   
                </div>
             </div>
             
             <br class="xs-20">
             
             <!-- TOPIC PAYMENT -->
             
             <div class="portlet">
                <div class="heading-block">
                   <h3>Payment</h3>
                </div>
                <div id="Q-2" class="panel-group accordion-panel">
                    
                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-2" href="#Q-2-1">
                                What am I paid ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-2-1" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                You will receive the total amount (gross amount) paid by the customer
                                minus the Service Fee and any taxes we are required to collect 
                                by law.
                            </p>
                         </div>
                      </div>
                   </div>

                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-2" href="#Q-2-2">
                                What is the Service Fee ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-2-2" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                The Service Fee is a commission that we take on each payment for allowing
                                the assistants to use the services of our platform. It is currently set to
                                {{ $vw_service_fee_var }}% 
                                of the total amount on each payment, plus 
                                {{ $vw_service_fee_fix }} cents.
                            </p>
                         </div>
                      </div>
                   </div>

                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-2" href="#Q-2-3">
                                How will i be paid ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-2-3" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                The available funds in your account balance will be automatically
                                transferred to your nominated bank account on a {{ $vw_payout_interval }} basis.<br>
                                @if ($vw_withdraw_fee_fix > 0)
                                There is a fixed fee of {{$vw_withdraw_fee_fix}} cents on each withdrawal.
                                @endif
                            </p>
                         </div>
                      </div>
                   </div>

                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-2" href="#Q-2-4">
                                Why are payments marked as "pending" ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-2-4" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                When you receive payments for your duties the funds are not immediately available 
                                in your account balance. After a duty has been ended, there is a 
                                {{$vw_dispute_period}} 
                                hold period within which the customer can dispute the payment. If no dispute 
                                has been filed within such period, the payment will be released. However, 
                                due to security measures put in place by payment processors and/or the banking 
                                system the funds will be put on hold still for some time before becoming available.<br><br>
                                From the time payments are received you can expect the funds to become
                                available in your balance within 
                                {{ $vw_payout_delay }} 
                                days, after which they will be automatically transferred to your bank account.
                                Note that once funds are paid out it may take few days to appear in your
                                bank account and this delay is out of our control.
                            </p>
                         </div>
                      </div>
                   </div>

                </div>
             </div>
             
             <br class="xs-20">
             
             <!-- TOPIC IDENTITY -->
             
             <div class="portlet">
                <div class="heading-block">
                   <h3 id="verification">Verification</h3>
                </div>
                <div id="Q-3" class="panel-group accordion-panel">
                   
                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-3" href="#Q-3-1">
                                Why my account needs verification ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-3-1" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                The account verification is necessary in order to have it fully active. 
                                It involves checking your identity (necessary to receive funds) and other
                                details required to maintain a minimum quality standard on the platform.
                                The information that you need to provide may vary, depending on your
                                situation, and we will ask you when it's needed. If you don't provide us
                                with all the requested information your account functionality may be limited
                                or disabled altogether.
                            </p>
                         </div>
                      </div>
                   </div>

                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-3" href="#Q-3-2">
                                How long will it take for my account to become active ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-3-2" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                The activation time depends on the information you provide and whether it's correct.
                                It may take one or more days. You will be notified when your account becomes fully active.
                            </p>
                         </div>
                      </div>
                   </div>
                   
                   <!-- Question .panel -->
                   <div class="panel">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                             <a class="accordion-toggle" data-toggle="collapse" data-parent="#Q-3" href="#Q-3-3">
                                What kind of identity information should i provide ?
                             </a>
                         </h4>
                      </div>
                      <div id="Q-3-3" class="panel-collapse collapse">
                         <div class="panel-body">
                            <p>
                                The required information for identity verification may vary but most assistants
                                will need to provide the following:<br>
                            </p>
                            <ul>
                                <li>Full name</li>
                                <li>Date of birth</li>
                                <li>Full Address</li>
                                <li>Colour scan of an identity document with photo (driver's license, passport, etc.)</li>
                                <li>Bank account details</li>
                            </ul>
                            <p>
                                If you are registered as an expert assistant then you will also have
                                to provide proof of your qualifications. Following are the accepted evidence of
                                expertise:<br>
                            </p>
                            <ul>
                                <li>Records of registration with Professional Bodies/Boards (if electronic
                                records are provided must be freely accessible through the internet)</li>
                                <li>Certification from a recognized educational institution (e.g. diploma,
                                written declaration or other course certificate)</li>
                            </ul>
                         </div>
                      </div>
                   </div>

                </div>
             </div>

          </div>
          <!-- /.layout-main -->
          
          <div class="col-sm-4 layout-sidebar">
             <hr class="visible-xs">
             <br class="xs-20 sm-0">
             <div class="well text-center">
                <p><i class="fa fa-question-circle fa-5x text-muted"></i></p>
                <h4>Have a Question?</h4>
                <p>Do not esitate to ask.</p>
                <a href="mailto:info@brainduty.com" class="btn btn-secondary">Get it Answered!</a>
             </div>
             <!-- /.well -->
          </div>
          <!-- /.layout-sidebar -->
          
       </div>
       <!-- /.layout -->          
    </div>
    <!-- /.container -->
@endsection
