@extends('layouts.portal')

@section('styles')
    @parent
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('javascripts')
    @parent
    <script src="{{ URL::asset('js/widget/aglist.js') }}"></script>
    <script src="{{ URL::asset('js/portal/provider/onboarding.js') }}"></script>
    <script src="{{ URL::asset('ext/parsley/js/parsley.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
             App.init().run(new Portal);
        });
    </script>
@endsection

@section('hero')
    <div class="container">
       <h1 class="masthead-subtitle">
           <i class="fa fa-user-circle"></i>
           Onboarding
       </h1>
    </div> <!-- /.container -->
@endsection

@section('content')
<div class="container">
   <div class="row">
      <div class="col-md-6 col-md-offset-3">
          
         <!-- START WIZARD -->
         <div id="process-wizard">
            <form id="onboarding-form"
               action="{{ URL::to('provider') }}"
               method="post"
               class="form parsley-form">
               
               <!-- WELCOME FRAME -->
               
               <div id="wizard-step-1" class="wizard-page">
                  <div class="wizard-page-content">
                     <div class="heading-block-style underlined">
                        <i class="fa fa-file"></i>
                        <h5>Getting ready</h5>
                     </div>
                     <p>
                        Thanks for your interest in becoming an assistant on Brainduty.<br><br>
                        To get you onboard you will be required to submit some documentation
                        in order to verify your identity. The required documents depend on your
                        situation but in most cases you will have to provide the following:
                     </p>
                     <ul class="icons-list">
                         <li>
                             <i class="icon-li fa fa-drivers-license"></i>
                             Identity document with photo (driver license, passport, id card). This may
							 be required to verify your identity.
                         </li>
                         <li>
                             <i class="icon-li fa fa-mortar-board"></i>
                             Proof of professional qualifications. This is only required if you are
                             registering as an expert assistant (consultant/advisor).<br><br>
                         </li>
                     </ul>
                     <p>
                        More information can be found in your assistant's account upon registration.
                     </p>
                     <p><br class="md-30"></p>
                  </div>
                  <div class="command-buttons">
                     <button type="button" class="btn next btn-primary" data-step="1">
                     Next <i class="fa fa-chevron-right"></i>
                     </button>
                  </div>
               </div>
               
               <!-- ENTITY TYPE FRAME -->
               
               <div id="wizard-step-2" class="wizard-page" style="display:none;">
                  <div class="wizard-page-content">
                     <div class="heading-block-style underlined">
                        <i class="fa fa-user"></i>
                        <h5>Your identity</h5>
                     </div>
                     <p>
                        Let's start by telling us what kind of entity you are ...
                     </p>
                     <div class="entity-type ag-list single-select list-group">
                       <div class="list-group-item" data-entity-type-id="individual">
                          <div class="item-icon pull-left"><i class="fa fa-user"></i></div>
                          <div class="item-body">
                             <div class="item-heading">
                                <h4 class="item-name pull-left">Individual</h4>
                             </div>
                             <p>
                                Click this option if you are not a business entity.
                             </p>
                          </div>
                       </div>
                       <div class="list-group-item" data-entity-type-id="soletrader">
                          <div class="item-icon pull-left"><i class="fa fa-briefcase"></i></div>
                          <div class="item-body">
                             <div class="item-heading">
                                <h4 class="item-name pull-left">Sole trader</h4>
                             </div>
                             <p>
                                Click this option if you are a sole trader (ABN holder).
                             </p>
                          </div>
                       </div>
                       <div class="list-group-item" data-entity-type-id="company">
                          <div class="item-icon pull-left"><i class="fa fa-institution"></i></div>
                          <div class="item-body">
                             <div class="item-heading">
                                <h4 class="item-name pull-left">Company</h4>
                             </div>
                             <p>
                                Click this option if you are a company (ACN holder).
                             </p>
                          </div>
                       </div>
                     </div>
                     <div class="form-group">
                        <input type="hidden" name="entity_type" value=""
                           data-parsley-required
                           data-parsley-required-message="Please select the type of entity."
                           data-parsley-group="step-1">
                     </div>
                  </div>
                  <div class="command-buttons">
                     <button type="button" class="btn back btn-default" data-step="2">
                     <i class="fa fa-chevron-left"></i> Back
                     </button>
                     <button type="button" class="btn next btn-primary" data-step="2">
                     Next <i class="fa fa-chevron-right"></i>
                     </button>
                  </div>
               </div>
               
               <!-- SERVICES FRAME -->
               
               <div id="wizard-step-3" class="wizard-page" style="display:none;">
                  <div class="wizard-page-content">
                     <div class="heading-block-style underlined">
                        <i class="fa fa-industry"></i>
                        <h5>Your services</h5>
                     </div>
                     <p>
                         Specify what kind of assistance you want to provide. You can select more than one.
                     </p>
                     <div class="services ag-list multi-select list-group">
                       @foreach ($vw_services as $service)
                       @if ($service['is_active'])
                       <div class="list-group-item" data-services-id="{{ $service['id'] }}">
                          <div class="item-icon pull-left">
                             <i class="fa {{ $service['id'] == 1 ? 'fa-file-text' : 'fa-briefcase' }}"></i>
                          </div>
                          <div class="item-body">
                             <div class="item-heading">
                                <h4 class="item-name pull-left">{{ $service['name'] }}</h4>
                             </div>
                             <p>{{ $service['description'] }}</p>
                             @if (count($service['categories']))
                             <select id="service-{{ $service['id'] }}-categories"
                                class="categories ui-select no-bubble form-control" multiple="multiple"
                                data-placeholder="Select a category"
                                style="width: 100%;">
                                <!-- <option value="none" selected="selected">Select a category...</option> -->
                                @foreach ($service['categories'] as $id => $category)
                                <option value="{{ $id }}">{{ $category['name'] }}</option>
                                @endforeach
                             </select>
                             @endif
                          </div>
                       </div>
                       @endif
                       @endforeach
                     </div>
                     <div class="form-group">
                        <input type="hidden" name="service_categories" value=""
                           data-parsley-required
                           data-parsley-required-message="Please select the type of services and at least one category, if any (max 10)."
                           data-parsley-group="step-2">
                     </div>
                  </div>
                  <div class="command-buttons">
                     <button type="button" class="btn back btn-default" data-step="3">
                     <i class="fa fa-chevron-left"></i> Back
                     </button>
                     <button type="button" class="btn next btn-primary" data-step="3">
                     Next <i class="fa fa-chevron-right"></i>
                     </button>
                  </div>
               </div>
               
               <!-- ENTITY DETAILS FRAME -->
               
               <div id="wizard-step-4" class="wizard-page" style="display:none;">
                  <div class="wizard-page-content">
                     <div class="heading-block-style underlined">
                        <i class="fa fa-id-card"></i>
                        <h5>Your details</h5>
                     </div>
                     <p>
                         Provide us with some information about you and/or your business, choose your
                         account credentials and you're signed up.
                     </p>
                     <div class="business-details row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="business-name">Business name</label>
                              <input type="text" id="business-name" class="form-control"
                                 data-parsley-required
                                 data-parsley-required-message="Please provide your business name. Use your full name if you do not have one."
                                 data-parsley-pattern="[a-zA-Z0-9 \/'.\-&]{1,64}"
                                 data-parsley-pattern-message="Business Names can only contain letters, numbers and the symbols /'.-&"
                                 data-parsley-group="step-3-business-details">
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="business-number">Business number</label>
                              <input type="text" id="business-number" class="form-control"
                                 data-parsley-required
                                 data-parsley-required-message="Please provide your business number."
                                 data-parsley-pattern="^\d{9}$|^\d{11}$"
                                 data-parsley-pattern-message="Invalid business number."
                                 data-parsley-group="step-3-business-details">
                           </div>
                        </div>
                     </div>
                     <div class="entity-details row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <label for="first-name">First Name</label>
                              <input type="text" id="first-name" class="form-control"
                                 data-parsley-required
                                 data-parsley-required-message="Please provide your first name."
                                 data-parsley-pattern="[a-zA-Z0-9 \/'.\-&]{1,64}"
                                 data-parsley-pattern-message="Names can only contain letters, numbers and the symbols /'.-&"
                                 data-parsley-group="step-3">
                           </div>
                           <div class="form-group">
                              <label for="last-name">Last Name</label>
                              <input type="text" id="last-name" class="form-control"
                                 data-parsley-required
                                 data-parsley-required-message="Please provide your last name."
                                 data-parsley-pattern="[a-zA-Z0-9 \/'.\-&]{1,64}"
                                 data-parsley-pattern-message="Names can only contain letters, numbers and the symbols /'.-&"
                                 data-parsley-group="step-3">
                           </div>
                           <div class="form-group">
                              <label for="dob">Date of Birth (DD/MM/YYYY)</label>
                              <input type="text" id="dob" class="form-control"
                                 data-parsley-required
                                 data-parsley-required-message="Please provide your date of birth."
                                 data-parsley-pattern="(?:0[1-9]|(?:1|2)[0-9]|3(?:0|1))/(?:0[1-9]|1[0-2])/(?:19\d\d|20(?:0\d|1[0-6]))"
                                 data-parsley-pattern-message="Invalid date of birth. Must be in the format DD/MM/YYYY."
                                 data-parsley-group="step-3">
                           </div>
                           <div class="form-group">
                              <label for="mobile-number">Mobile Number</label>
                              <input type="text" id="mobile-number" class="form-control"
                                 data-parsley-required
                                 data-parsley-required-message="Please provide your mobile number."
                                 data-parsley-pattern="\d{10,12}"
                                 data-parsley-pattern-message="Please check the phone number."
                                 data-parsley-group="step-3">
                           </div>
                        </div>
                     </div>
                     
                     <div class="entity-credentials row">
                        <div class="col-sm-6 col-md-6">
                           <div class="form-group">
                              <label for="username">Username</label>
                              <div class="input-group">
                                 <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user fa-fw"></i></span>
                                 <input type="text" id="username" class="form-control"
                                        data-parsley-required
                                        data-parsley-required-message="The username must be a valid email address."
                                        data-parsley-type="email"
                                        data-parsley-group="step-3">
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="password">Password</label>
                              <div class="input-group">
                                 <span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock fa-fw"></i></span>
                                 <input type="password" id="password" class="form-control"
                                        data-parsley-required
                                        data-parsley-required-message="Please provide a password for your account."
                                        data-parsley-pattern="[a-zA-Z0-9!?@#$%&(){}]{8,64}"
                                        data-parsley-pattern-message="Invalid password."
                                        data-parsley-group="step-3">
                              </div>
                           </div>
                            <?php /*
                           <div class="form-group">
                              <label for="password-confirm">Retype Password</label>
                              <div class="input-group">
                                 <span class="input-group-addon" id="basic-addon1"><i class="fa fa-check fa-fw"></i></span>
                                 <input type="password" id="password-confirm" class="form-control"
                                        data-parsley-required
                                        data-parsley-required-message="Please confirm your password."
                                        data-parsley-equalto="#password"
                                        data-parsley-equalto-message="This does not match the chosen password."
                                        data-parsley-group="step-3">
                              </div>
                           </div>
                           */?>
                        </div>
                        <div class="col-sm-6 col-md-6">
                        </div>
                     </div>

                  </div>
                  <div class="command-buttons">
                     <button type="button" class="btn btn-default back" data-step="4">
                         <i class="fa fa-chevron-left"></i> Back
                     </button>
                     <button type="button" class="btn btn-success submit" data-step="4">
                         Submit <i class="fa fa-spinner fa-pulse loading"></i>
                     </button>
                  </div>
                   <p class="small">
                       By clicking "Submit" you agree to Brainduty's <a href="{{ url('terms') }}">Terms</a>
					   and <a href="{{ url('privacy') }}">Privacy Policy</a>
                   </p>
               </div>
            </form>
         </div>
         <!-- END WIZARD -->
         
      </div>
   </div>
</div>
<!-- /.container -->
@endsection
