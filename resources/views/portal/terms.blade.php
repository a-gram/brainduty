@extends('layouts.portal')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            App.init().run(new Portal);
        });
    </script>
@endsection

@section('hero')
    <div class="container">
       <h1 class="masthead-subtitle">
           Terms Of Service
       </h1>
    </div> <!-- /.container -->
@endsection

@section('content')
    <div class="container">
        
        <section class="terms-of-service">
            
            <p>
This Agreement (the "Agreement") constitutes a legally binding contract between
Brainduty Technologies, ABN 40828915627 ("Brainduty", "We", "Us", "Our") a duly registered 
business in Australia, and any legal entity or person from within any country ("You, Your") using
the applications, website and technology services owned by Brainduty (collectively 
the "Platform"). By accessing and using the Platform, You acknowledge that You have read, understood
and agreed to be bound by the terms and conditions of this Agreement. To use the Platform You must 
be able to form legally binding contracts under applicable law. These terms and
conditions are subject to change without notice. We will notify of amendments to
these terms and conditions by posting them to this web location. The modified Agreement
shall be effective upon Brainduty's posting of the amendments. If You do not
accept these terms and conditions then You are not allowed to access and use the
Platform.
            </p>
            <br class="xs-20">
            <h3>1.&nbsp;&nbsp;The Platform</h3>
            <p>
The Platform provides technology services for automated allocation and management
of on-demand tasks (the "Duties") posted by requesting entities (the "Customers") in order to be 
fullfilled by independent third party service providers (the "Assistants"). Duties posted
to the platform include but are not limited to administrative tasks, virtual assistance and
consultations. The ultimate goal of the Platform is that of facilitating the connection between 
Customers and Assistants who render their services (the "Services") through the Platform.
The acts of posting and fulfilling the Duties are at the sole discretion of the 
Customers and the Assistants respectively (collectively the "Users").
In no case shall Brainduty be deemed to control or direct the activities of the Users 
under the terms of this Agreement, including requesting and provisioning of Services. 
This Agreement solely governs the access to, and use of the Platform by the Users, and in no way sets any terms, rights,
restrictions and obligations on each Duty, which shall be governed by a separate contractual relationship between Customer and Assistant.
You understand and agree that Brainduty does not provide services for the fulfillment
of Duties but acts as a facilitator to connect Customers with Assistants, and that Assistants 
registered with the Platform are independent contractors and not employed by Brainduty
or any of its affiliates. Brainduty and You agree that this Agreement does not consitute
an employment agreement and that You do not have any authority to represent yourself
as an employee, agent, authorized representative or officer of Brainduty.
            </p>
            <br class="xs-20">
            <h3>2.&nbsp;&nbsp;User accounts</h3>
            <p>
Access and use of the Platform is only possible upon registration for a user account 
(the "Account").
The registration process may differ for Customers and Assistants, depending on the 
identity verification requirements as per applicable law. Customers must provide
a current and valid payment method (credit/debit card, e-wallet, etc., hereinafter the
"Payment Method(s)"), whereas Assistants must undergo an identity verification process by 
providing Brainduty, or any authorized third party entity, with specific information, as outlined in Section 3.2.
You are solely and fully responsible for any action done by any person using Your name, Account 
or password. You should take all necessary steps to ensure that the password is kept
confidential and secure and should inform Us immediately if You have any reason to 
believe that Your Account has been compromised. User Accounts are personal, non-transferable and unique
for each user. Multiple Accounts per User are not allowed. The information provided in Your 
Account must be up to date, accurate and comply with the terms set forth in Section 7.
Brainduty is not responsible for any damage or loss resulting from the provision of incorrect
or incomplete information. 
You acknowledge and accept that Your use of the Account may be subject to restrictions imposed 
by Us for many reasons, including but not limited to insufficient proof of identity, 
unpaid dues and breach of any of the terms and conditions of this Agreement.
We are under no obligation to monitor or record the activity
of any User and We are not responsible for any damage or loss You may incur as the
result of inappropriate behaviour of other users. Should You have any complaint or claim,
You can submit a detailed report to Us so that We can investigate the case and take
appropriate measures. To operate your Account We may need to send messages and 
communications (the "Notifications") to one or more of your contact details including
but not limited to email address and phone number, which may require action from You
in order to keep the account operational. You agree to receive such Notifications and
acknowledge that refusing them may compromise your Account functionality.
Brainduty reserves the right to suspend or terminate user Accounts at any time for any reason, 
with or without notice.
            </p>
            <br class="xs-20">
            <h3>3.&nbsp;&nbsp;Obligations</h3>
            <p>
You represent, warrant and agree that:<br>
(1) You have read, understood and accepted to be bound by this Agreement before registering 
with the Platform for an Account;<br>
(2) You will provide Brainduty with all the requested information for the provisioning of the 
Account and may access and use the Platform solely upon Brainduty's approval, which We may grant or deny at Our sole discretion;<br>
(3) You will only use the Platform for personal, non-commercial use, unless otherwise
specified in a separate Agreement;<br>
(4) You will not try to persuade Users to fulfill the Duties and/or make payments outside of the Platform. 
Any violation of this covenant shall be deemed an attempt to circumvent the Platform's fees and will result
in the immediate termination of Your Account and in the recovery of any loss that We may have incurred, either from the
available balance of Your Account or by other debt collection means; and<br>
(5) You will not try to defraud the Users or Brainduty in connection with Your use of the Platform. Should We find any
evidence of fraudulent activity in Your Account, You will be reported to the relevant authority (Police, Investigation
Agencies, etc.) and restrictive and indemnifying measures will be taken, including but not limited to termination of the Account and recovering
of funds from Your Account balance and/or registered Payment Methods to compensate for any losses or damages
that We or any other third party may have incurred.
            </p>
            <br class="xs-20">
            <h4>3.1.&nbsp;&nbsp;Obligations of Customers</h4>
            <p>
You, as a Customer, represent, warrant and agree that:<br>
(1) You will provide Brainduty with, and continue to maintain, valid and current 
Payment Methods;<br>
(2) You will pay any due amount for Your use of the Platform and/or services rendered to You through
the Platform in full and on time;<br>
(3) You will make sure that Your registered Payment Methods are backed by sufficient available funds 
at all time and agree that Brainduty is not responsible for overdraft fees or any other bank fees 
You may incur for dishonored transactions in case the amount of a charge exceeds the available funds in Your bank account;<br>
(4) You understand and accept that restrictions including but not limited to suspension 
or termination may be put on Your Account if, for any reason, a payment for a due amount fails;<br>
(5) You shall contact Us and seek assistance from Us for any disputes related to services rendered to You 
through the Platform prior to resorting to chargebacks or any other means of charge reversal through Your Payment
Method issuer to try to get a refund;<br>
(6) You understand and accept that You may incur penalties if You cancel an ongoing Duty, including 
but not limited to a Cancellation Fee and/or a negative score;<br>
(7) You will not compromise Our feedback system by falsifying Your feedback, threatening an Assistant with negative feedback or 
offering any kind of compensation in exchange for feedback; and<br>
(8) You will not post Duties that are hazardous, offensive or harmful to the dignity of the Assistants;<br>

            </p>
            <br class="xs-20">
            <h4>3.2.&nbsp;&nbsp;Obligations of Assistants</h4>
            <p>
You, as an Assistant, represent, warrant and agree that:<br>
(1) You will undergo an identity verification process as part of Your registration with the Platform and 
You may not be able to provide Services and/or withdraw funds from Your Account until We determine that Your identity is verified;<br>
(2) You will supply Brainduty with all the information and documentation (collectively "Identity 
Information") We reasonably require in order to verify Your identity, including any necessary authorizations to access such Identity
Information, and warrant that the Identity Information You provide to Us is true, accurate and up to date;<br>
(3) You possess the required skills, qualifications and necessary equipment to provide the Services;<br>
(4) You will, as an Assistant claiming professional qualifications ("Expert Assistant"), 
supply Us upon request with proof of Your qualifications, included but not limited to a copy of the official 
certifications, or any other documentation and/or material supporting Your claims;<br>
(5) You will be able to fulfill the Duties in a timely, professional and efficient manner, and 
perform assigned Duties with reliability, quality, honesty, and discretion;<br>
(6) You will communicate with the Customers effectively and in a timely manner for any matter 
related to Duties assigned to You by using the Platform's instant messaging systems;<br>
(7) You will keep any information shared with You by Customers during Your provisioning of Services,
confidential at all time, not disclosing it to third parties, unless Customers give You express consent to do so;<br>
(8) You will provide Brainduty with further Identity Information at any time during Your provisioning
of Services, even if You have successfully passed the identity verification
process, and agree to provide Us with all the necessary authorizations to access such
Identity Information during the terms of this Agreement;<br>
(9) You will be responsible for paying all taxes and duties in connection with the provisioning of Services, 
including but not limited to income tax, superannuation and insurance, as required by applicable law; and<br>
(10) You will promptly inform Brainduty of any changes to Your circumstances that may impact
the provisioning of Services.
            </p>
            <br class="xs-20">
            <h3>4.&nbsp;&nbsp;Payments and Refunds</h3>
            <p>
As a Customer, payments (the "Charges") are due at the time of Duty completion, in
full and in the amount specified in the Duty details page. Charges include fares,
applicable fees and taxes, as set forth in the fares schedule on Brainduty's website.
Upon completion of a Duty, one of Your registered Payment Methods (indicated by You as 
the default one) is charged. If the Charge fails, We will attempt to charge other
Payment Methods on file with Your Account, if any, until a successfull Charge is made.
Should all of Your Payment Methods fail, You may incur some penalties, as set forth in Section
3.1. You are given two (2) days from the ending date of the Duty (the "Dispute Period") to dispute it. 
If the dispute is resolved in Your favour, You will be issued with a refund, net of any non-refundable amounts, 
in the form of a credit to Your Account or a return of funds via the charged Payment Method. 
Which form of refund will be used is at Our sole discretion.
If the dispute is not resolved in Your favour, the payment will be released to the Assistant's
Account, with no possibility of appeal.
You acknowledge and agree that if a Duty has been completed and no dispute has been filed by You within the
Dispute Period, the Duty will be deemed as successfully fullfilled 
and You are not, in any case and by any means whatsoever, be entitled to a refund. 
In Your payment method's statement You may notice funds put on hold in connection with the
use of the Platform. These are not extra Charges but "authorizations" (funds that are just
put on hold but not actually cleared) used to verify Your registered Payment Methods
or as a security measure to make sure that there are sufficient funds to compensate
the Assistants. Brainduty is not responsible for any fees that Your Payment
Method issuer may charge You as a consequence of overdrafts. 
As specified in Section 3.1, it is Your obligation to make sure that Your registered Payment
Methods are backed by sufficient available funds at all time.<br><br>

As an Assistant, You agree to elect Brainduty as Your limited payment collection agent.
As Your limited payment collection agent, We will process all payments for Services, 
in the amount specified in the Duty details page and inclusive of any applicable taxes.
All of the payments are processed using a third party payment processor, chosen by Us
at Our sole discretion. All proceeds from Services 
will be held in Your Account, with no yield interest, net of fees, taxes and any other
amount that We are required to withhold by applicable law. Payments held in Your Account
shall only be deemed cleared at the time the relative Duties are successfully completed.
Should Customer file a dispute that We in Our sole discretion resolve in the Customer's favour,
all of the payments relative to the disputed Duty will be refunded to Customer.
Proceeds for Services will be paid out to your nominated payout account (bank account, e-wallet, 
etc., hereinafter the "Payout Account") {{ \App\Hey::get_app('payout.delay') }} days after
being received on a {{ \App\Hey::get_app('payout.interval') }} basis. 
You acknowledge and agree that you may not be able to withdraw funds if Your Account has been 
subject to restrictions. Once withdrawals are processed,
there may be waiting times involved, which are imposed by the payment processor 
and/or banking system and that are out of Our control. You acknowledge and agree that
any time delay in receiving Your funds in the nominated Payout Account is not
caused by Us, and that You are not entitled to any form of indemnification from Brainduty 
or any of Our affiliates for losses or damages caused by such delays.
            </p>
            <p>
   Payment processing services for Assistants on Brainduty are provided by Stripe and are subject 
   to the <a href="https://stripe.com/connect-account/legal">Stripe Connected Account Agreement</a>, 
   which includes the <a href="https://stripe.com/legal">Stripe Terms of Service</a> (collectively, 
   the “Stripe Services Agreement”). By agreeing to this Agreement or continuing to operate as an Assistant 
   on Brainduty, You agree to be bound by the Stripe Services Agreement, as the same may be modified 
   by Stripe from time to time. As a condition of Brainduty enabling payment processing services 
   through Stripe, You agree to provide Brainduty accurate and complete information about You and 
   Your business, and You authorize Brainduty to share it and transaction information related to 
   Your use of the payment processing services provided by Stripe.
            </p>
            <br class="xs-20">
            <h3>5.&nbsp;&nbsp;Pricing and Fees</h3>
            <p>
The rates of Services (the "Prices) are shown in the fares schedule and/or in the Duty request pages
and are inclusive of any taxes and duties that We are required to collect by applicable law.

As a Customer, You acknowledge and agree that Prices may only be estimates and that the charged
amounts may be different from what was shown in the fares schedule and/or in the Duty request
pages. Wherever possible, Brainduty will make it clear that the shown amount is an estimate by
either providing a price range or explicitly stating so.

As an Assistant, You acknowledge and agree that Brainduty may control, partially or in full, 
the Prices at any time and without prior notice.
You also acknowledge and agree that We, as Your limited payment collection agent
and for allowing You to use Our Platform, charge a commission (the "Service Fee") on each 
payment transaction for Services. The amount of the Service Fee may change at any time 
depending on factors including but not limited to the
type of services rendered, market demand, performance and revenue generated
on the Platform. The applicable Service Fee is communicated to You in the
Assistant's account pages and its maximum amount is currently 
{{ ceil( array_sum( \App\Hey::get_app('fees.service.var') )*100 ) }}% plus 
{{ array_sum( \App\Hey::get_app('fees.service.fix') ) }} cents on each payment.
In addition to the Service Fee, a withdrawal fee (the "Withdrawal Fee") may also be charged 
on each funds transfer from Your Account to Your nominated Payout Account.

            </p>
            <br class="xs-20">
            <h3>6.&nbsp;&nbsp;Cancelling and disputing a Duty</h3>
            <p>
Brainduty strongly discourage cancelling a Duty that has already been assigned to an Assistant
(ongoing Duty) in order to avoid disruptive behaviours that may negatively impact the User's experience 
on the Platform. Instead, We encourage the parties to communicate and try to come to a mutual agreement 
in order to cancel the Duty without incurring any penalties.
Should You unilaterally cancel an ongoing Duty, You acknowledge and agree that doing so may cause penalties
that will affect Your status and ability to access and use the Platform. Customers may
be charged an amount, depending on the Assistant's cancellation policy, whereas Assistants may incur a negative
score that affects their overall ranking.
Cancellation Fees are not applicable if Customers cancel a scheduled Duty that has not
yet been assigned or if a cancellation request has been sent to the other party and
the other party is unresponsive.
            </p>
            <br class="xs-20">
            <h3>7.&nbsp;&nbsp;Restricted activities</h3>
            <p>
You shall not perform nor authorize others to perform any of the following activities:<br>
(1) use the Platform for commercial purposes;<br>
(2) make works that use the Platform directly or indirectly ("Derivative Works") without the express written consent of Brainduty;<br>
(3) export and/or market, or sell, offer for sale, rent or lease, distribute, transmit,
broadcast, directly or indirectly, portions of the Platform to any third party;<br>
(4) assign or transfer the rights and duties assigned to You under this Agreement
without the express written consent of Brainduty;<br>
(5) reverse engineer, decompile, disassemble, modify, or translate portions of the Platform, unless permitted
by applicable law;<br>
(6) attempt to gain unauthorized access to restricted resources;<br>
(7) interfere with or disrupt the operations of the Platform, or other systems
connected to the Platform;<br>
(8) post content that is unlawful, harmful, threatening, abusive, harassing, defamatory, racist, 
vulgar or infringing any third party's right, included but not limited to intellectual property 
rights, copyright, patent, trademark, trade secret or other proprietary rights;<br>
(9) use the Platform to post or otherwise transmit unsolicited advertisement materials, spam emails 
and messages, chain letters, pyramid schemes or any other form of junk materials; and<br>
(10) use the Platform, directly or indirectly, to conduct or support any illegal activites;
            </p>
            <br class="xs-20">
            <h3>8.&nbsp;&nbsp;License Grant</h3>
            <p>
Subject to the terms and conditions of this Agreement, Brainduty hereby grants to You
a limited, revocable, non-transferable, non-sublicensable, non-exclusive license to:<br>
(1) install applications downloaded from Brainduty's website, or from approved third party
repositories;<br>
(2) use the applications to access contents, materials and services on the 
Platform or on authorized third party platforms; and<br>
(3) create accounts subject to the terms and conditions set forth herein.
            </p>
            <br class="xs-20">
            <h3>9.&nbsp;&nbsp;Ownership</h3>
            <p>
Brainduty is the owner of title, copyright and other Intellectual Property provided
on the Brainduty's website. The Platform and all related contents and materials, including
all copies thereof, remain proprietary to Brainduty at all time. All rights not expressly
granted to You in this Agreement are reserved by Brainduty and not implicitly transferred
and/or assigned to You.
            </p>
            <br class="xs-20">
            <h3>10.&nbsp;&nbsp;Third party contents and materials</h3>
            <p>
The Platform may include third party software, technology, contents or materials
("Third Party Contents") that may be subject to restrictions, or covered by copyright or other form of Intellectual
Property protection and may require authorization. You agree that is Your
sole responsibility to determine what restrictions apply, to comply with all applicable
restrictions and to obtain all necessary authorizations. Where applicable, the Agreement grants to You
any assignable, transferable right as specified by the licenses accompanying such
Third Party Contents.
We make no representations or warranties, express or implied, as to the integrity,
accuracy or completeness of Third Party Contents and You agree that access and use
of all contents provided by this site and any external site linked to from this site is at Your own risk. 
            </p>
            <br class="xs-20">
            <h3>11.&nbsp;&nbsp;Intellectual Property</h3>
            <p>
Brainduty is the full and exclusive owner of Intellectual Property rights in the Platform and We have the necessary
authority to grant all licenses and permissions related to the subject matter.
The contents provided by the Platform are © Brainduty, except where noted, and are protected by laws
and international treaty provisions. Copying, display, reproduction and distribution of such contents,
in whole or in part, is prohibited without written permission of Brainduty. All third party brands, trademarks,
logos or material mentioned, displayed or provided are the property of their respective copyright holders.

We may allow Users to post or otherwise make available contents to Brainduty as part of the normal operations of the Platform,
including but not limited to images, documents, comments, audio/visual materials (collectively
the "User Content"). You retain any property rights on the User Content but provide
Brainduty with a worldwide, sublicensable, perpetual, irrevocable, transferable, 
royalty-free license, to use, modify, copy, publish, transmit, create derivative works of,
and exploit such User Content in any manner and in any media now known or hereinafter 
invented for any purpose without requiring payment to You or any other third party.

You represent and warrant that the User Content You may post or otherwise make available to Brainduty
is Your exclusive and sole property, or You have obtained the rights, licenses and consents
that allow You to grant to Brainduty the aforementioned license to the User Content.
You also warrant that the User Content You post or otherwise make available to Brainduty 
does not infringe on the copyright, trade secrets or patent of any third party.
You assume full liability should We be sued for alleged infringment of any property
rights to the User Content posted or otherwise made available to Us by You.
            </p>
            <br class="xs-20">
            <h3>12.&nbsp;&nbsp;Unsolicited Content</h3>
            <p>
Brainduty is always pleased to hear feedback from its users in order to continually improve
the overall experience on the Platform. However, to avoid any future misunderstandings, You
must not submit ideas, concepts, prototypes or any other creative material (the "Unsolicited Content") 
for future services and/or products to Brainduty. If You choose to do so, You agree that: (1) all Unsolicited Content that You
submit or otherwise make available to Brainduty is not confidential, regardless of what You 
state in such Unsolicited Content or in any accompanying message; and 
(2) You grant to Brainduty a perpetual, worldwide, irrevocable, 
transferable, sublicensable, fully paid, royalty free license to use, modify, 
copy, publish, transmit, create derivative works of, and exploit such Unsolicited Content in 
any manner and in any media now known or hereinafter invented for any purpose without requiring 
payment to You or any other third party.
            </p>
            <br class="xs-20">
            <h3>13.&nbsp;&nbsp;Termination</h3>
            <p>
This Agreement shall commence upon creation of a User Account and shall continue until
one of the following happens:<br>
(1) either party is in breach of any of the terms and conditions of this Agreement; or<br>
(2) a User explicitly makes a request to terminate his/her Account by written notice or other 
automated means provided by the Platform.<br>
Brainduty may terminate a User account with or without notice at any time if: (i) We observe activities
that are fraudulent or potentially fraudulent or that We, in good faith, believe to be a danger to other Users;
(ii) a User's performance indicator falls below a certain threshold that We, at our sole
discretion, deem to be below the expected quality standards for the Platform; or (iii) for any other
reason.
            </p>
            <br class="xs-20">
            <h3>14.&nbsp;&nbsp;No Agency</h3>
            <p>
Nothing contained herein shall be construed as creating any agency, employment relationship, 
partnership, principal-agent or other form of joint enterprise between the parties. 
No party has any authority to make any representation or commitment, or to incur 
any liability, on behalf of the others.
            </p>
            <br class="xs-20">
            <h3>15.&nbsp;&nbsp;Disclaimer</h3>
            <p>
THE SERVICES ARE PROVIDED "AS IS" AND "AS AVAILABLE." BRAINDUTY DISCLAIMS ALL REPRESENTATIONS 
AND WARRANTIES, EXPRESS, IMPLIED OR STATUTORY, NOT EXPRESSLY SET OUT IN THESE TERMS, 
INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NON-INFRINGEMENT. BRAINDUTY MAKES NO REPRESENTATION, WARRANTY, OR GUARANTEE REGARDING THE 
RELIABILITY, TIMELINESS, QUALITY, SUITABILITY OR AVAILABILITY OF THE PLATFORM OR ANY SERVICES 
OR GOODS REQUESTED THROUGH THE USE OF THE PLATFORM, OR THAT THE PLATFORM WILL BE UNINTERRUPTED 
OR ERROR-FREE. BRAINDUTY DOES NOT GUARANTEE THAT YOU, AS AN ASSISTANT, WILL BE ASSIGNED ANY DUTY OR WILL 
BE MAKING ANY INCOME OR EARNINGS. BRAINDUTY DOES NOT GUARANTEE THE QUALITY, SUITABILITY, SAFETY OR ABILITY OF 
THE ASSISTANTS. YOU AGREE THAT THE ENTIRE RISK ARISING OUT OF YOUR USE OF THE PLATFORM, 
AND ANY SERVICE OR GOOD REQUESTED IN CONNECTION THEREWITH, REMAINS SOLELY WITH YOU, 
TO THE MAXIMUM EXTENT PERMITTED UNDER APPLICABLE LAW.
            </p>
            <br class="xs-20">
            <h3>16.&nbsp;&nbsp;Liabilities</h3>
            <p>
BRAINDUTY SHALL NOT BE LIABLE FOR INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR 
CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, LOST DATA, PERSONAL INJURY OR PROPERTY DAMAGE 
RELATED TO, IN CONNECTION WITH, OR OTHERWISE RESULTING FROM ANY USE OF THE PLATFORM, EVEN IF 
BRAINDUTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. BRAINDUTY SHALL NOT BE LIABLE 
FOR ANY DAMAGES, LIABILITY OR LOSSES ARISING OUT OF: (1) YOUR USE OF OR RELIANCE ON THE PLATFORM 
OR YOUR INABILITY TO ACCESS OR USE THE PLATFORM; OR (2) ANY TRANSACTION OR RELATIONSHIP BETWEEN 
YOU AND ANY ASSISTANT, EVEN IF BRAINDUTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 
BRAINDUTY SHALL NOT BE LIABLE FOR DELAY OR FAILURE IN PERFORMANCE RESULTING FROM CAUSES BEYOND 
BRAINDUTY’S REASONABLE CONTROL. WE CANNOT GUARANTEE THAT EACH ASSISTANT IS WHO HE OR SHE CLAIMS 
TO BE, AND THAT EACH ASSISTANT IS ABLE TO FULFILL THE ASSIGNED JOBS. YOU AGREE THAT BRAINDUTY HAS NO 
RESPONSIBILITY OR LIABILITY TO YOU RELATED TO ANY GOODS OR SERVICES PROVIDED TO YOU BY THE ASSISTANTS. 
IN NO EVENT SHALL BRAINDUTY'S TOTAL LIABILITY TO YOU FOR ALL DAMAGES, LOSSES, AND CAUSES OF ACTION WHETHER 
IN CONTRACT, TORT (INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE), OR OTHERWISE EXCEED THE AMOUNT PAID BY YOU, 
IF ANY, FOR ACCESSING THE PLATFORM. APPLICABLE LAW MAY NOT ALLOW THE LIMITATION OR EXCLUSION OF LIABILITY OR 
INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY NOT APPLY TO YOU. IN SUCH CASES, 
BRAINDUTY'S LIABILITY WILL BE LIMITED TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW.
            </p>
            <br class="xs-20">
            <h3>17.&nbsp;&nbsp;Indemnity</h3>
            <p>
You agree to defend, indemnify and hold Brainduty and its officers, directors, employees and agents, 
including its affiliates, parents and subsidiaries, harmless from any and all claims, demands, losses, 
liabilities, and expenses (including attorneys’ fees) arising out of or in connection with: 
(1) Your use of the Platform or services or goods obtained through Your use of the Platform; 
(2) Your breach or violation of any of the terms set forth in this Agreement; 
(3) Brainduty’s use of Your User Content; (4) Your violation of the rights of any third party, 
including Assistants; or (5) any other activities in connection with the Platform.
            </p>
            <br class="xs-20">
            <h3>18.&nbsp;&nbsp;Severability</h3>
            <p>
If any term of this Agreement is found to be unenforceable or contrary to law, it will
be modified to the least extent necessary to make it enforceable, and the remaining
portions of this Agreement will remain in full force and effect.
            </p>
            <br class="xs-20">
            <h3>19.&nbsp;&nbsp;Governing Law</h3>
            <p>
This Agreement shall be governed by the laws of Australia, without regard to principles
of conflict of laws. In the event that either party initiates an action
in regards to this Agreement or other dispute between the parties, the
exclusive location and jurisdiction of such action shall be in the state and courts of
Victoria, Australia.
            </p>
            <br class="xs-20">
            <h3>20.&nbsp;&nbsp;Entire Agreement</h3>
            <p>
This Agreement, together with any attachments referred to herein, constitutes the entire
Agreement between the parties with respect to its subject matter, and supersedes all
prior agreements, proposals, negotiations, representations or communications relating
to the subject matter.
            </p>
            <br class="xs-30">
            <p><small>Last update: April 19, 2017</small></p>
        </section>
        
    </div>
    <!-- /.container -->
@endsection
