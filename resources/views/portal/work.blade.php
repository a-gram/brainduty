@extends('layouts.portal')

@section('styles')
    @parent
@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            App.init().run(new Portal);
        });
    </script>
@endsection

@section('hero')
    <div class="container">
       <h1 class="masthead-subtitle">
           <i class="fa fa-handshake-o"></i>
           Work with Us
       </h1>
    </div> <!-- /.container -->
@endsection

@section('content')
    <section class="content-feature">

      <div class="container">

        <p class="lead">
            Are you attentive, organized with good communication skills ? Are you a specialist in your field ?
            Then you can become an expert or generic assistant on Brainduty and work with all the freedom
			you need to make your life more enjoyable.
        </p>
        
        <br class="xs-50 lg-70">
        
        <div class="feature-lg figure-right">
          <div class="row">
            <div class="col-sm-6 col-sm-push-6">
              <figure class="feature-figure">
                  <img src="./img/work/feature-2.png" class="img-responsive figure-shadow center-block"  alt="">
              </figure>
            </div><!-- /.col -->
            <div class="col-sm-6 col-sm-pull-6">
              <br class="xs-30 sm-0">
              <div class="feature-content">
                <h2>Work on the go</h2>
                <p class="synopsis">
                    Work from wherever you like. From home, from a cafe, or anywhere you see fit without the hassle of a daily commute. With Brainduty
                    you can work on the go from anywhere. All you need is an internet connection and your passion, skills and
                    commitment.
                </p>
              </div> <!-- /.feature-content -->
            </div><!-- /.col -->
          </div> <!-- /.row -->
        </div> <!-- /.feature-lg -->

        <br class="xs-50 lg-100">

        <div class="feature-lg">
          <div class="row">
            <div class="col-sm-6">
              <figure class="feature-figure">
                  <img src="./img/work/feature-1.png" class="img-responsive figure-shadow center-block" alt="">
              </figure>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <br class="xs-30 sm-0">
              <div class="feature-content">
                <h2>Earn as you want</h2>
                <p class="synopsis">
                    Work on your own schedule and earn from $22 to $70 per hour with money deposited
					straight into your bank account. Whether you want to make a living, expand your business or just earn extra cash, 
                    Brainduty gives you all the flexibility you need to meet your goals.
                </p>
              </div> <!-- /.feature-content -->
            </div><!-- /.col -->
          </div> <!-- /.row -->
        </div> <!-- /.feature-lg -->

        <br class="xs-50 lg-100">
        
        <div class="feature-lg figure-right">
          <div class="row">
            <div class="col-sm-6 col-sm-push-6">
              <figure class="feature-figure">
                  <img src="./img/work/feature-3.png" class="img-responsive figure-shadow center-block"  alt="">
              </figure>
            </div><!-- /.col -->
            <div class="col-sm-6 col-sm-pull-6">
              <br class="xs-30 sm-0">
              <div class="feature-content">
                <h2>Do what you like</h2>
                <p class="synopsis">
                    If you feel you are doing something that doesn't really suit you, then it's time for a change.
                    Be your own boss. The freedom to work only on what you like is priceless. 
                    Use your passion and expertise to improve your and other people's lives, not your
                    boss' paycheck.
                </p>
              </div> <!-- /.feature-content -->
            </div><!-- /.col -->
          </div> <!-- /.row -->
        </div> <!-- /.feature-lg -->

        <br class="xs-50 lg-100">
        
        <p style="text-align: center;">
            <a href="{{ url('provider/signup') }}" class="btn btn-primary btn-jumbo">Join us today!</a>
        </p>
        
      </div>

    </section>
@endsection
