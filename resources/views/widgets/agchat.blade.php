

<!-- Task chat panel -->

<div class="task-chat">
   <!-- MESSAGES LIST -->
   <div class="task-message-list"></div>
   <!-- MESSAGE TEMPLATE -->
   <div class="message-template" style="display:none;">
      <ul>
         <li>
            <div class="message">
               <div class="message-avatar">
                  <img alt="" src="" class="avatar">
               </div>
               <div class="message-meta">
                  <span class="message-author"></span>
                  <small class="message-timestamp"></small>
               </div>
               <div class="message-body">
                  <p></p>
                  <div class="message-attachments"></div>
               </div>
               <div class="message-actions"></div>
            </div>
         </li>
      </ul>
   </div>
   <!-- MESSAGE PROGRESS -->
   <div class="hloader" style="display:none;"></div>
   <!-- MESSAGE BOX -->
   @if (isset($vw_chat_enabled))
   <div class="message-box clearfix">
      <form action=""
         method="post"
         enctype="multipart/form-data">
         <textarea class="form-control message-box-textarea message-box-body"
            rows="3"
            placeholder="Type your message here ..."
            tabindex="1">
	   </textarea>
         <div class="message-box-attachments"></div>
         <div class="message-box-actions">
            <div class="message-box-types pull-left">
               <a class="fa fa-paperclip fileinput-button btn-upload" title="">
                  <input id="fileupload" type="file" name="files[]" multiple>
               </a>
            </div>
            <div class="btn-group dropup pull-right">
               <button class="btn btn-secondary btn-sm btn-post" type="button" data-message-type="2">Post</button>
               @if (Auth::user()->is_provider())
               <button class="btn btn-secondary btn-sm dropdown-toggle" aria-expanded="false" type="button" data-toggle="dropdown"><span class="caret"></span></button>
               <ul class="dropdown-menu" role="menu">
                  <li><a class="btn-post txn" href="javascript:;" data-message-type="4"><i class="fa fa-file fa-fw"></i>Deliverables (end duty)</a></li>
                  <li><a class="btn-post txn slot" href="javascript:;" data-message-type="3"><i class="fa fa-clock-o fa-fw"></i>Slot request</a></li>
                  <li><a class="btn-post txn" href="javascript:;" data-message-type="3"><i class="fa fa-money fa-fw"></i>Charge request</a></li>
                  <li><a class="btn-post txn" href="javascript:;" data-message-type="5"><i class="fa fa-handshake-o fa-fw"></i>Cancellation request</a></li>
               </ul>
               @elseif (Auth::user()->is_customer())
               <button class="btn btn-secondary btn-sm dropdown-toggle" aria-expanded="false" type="button" data-toggle="dropdown"><span class="caret"></span></button>
               <ul class="dropdown-menu" role="menu">
                  <li><a class="btn-post txn" href="javascript:;" data-message-type="5"><i class="fa fa-handshake-o fa-fw"></i>Cancellation request</a></li>
               </ul>
               @endif
            </div>
         </div>
      </form>
   </div>
   @endif
</div>

<!-- End Task chat Panel -->

