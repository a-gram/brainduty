
      <!-- MODAL MESSAGE -->
      
      <div id="ag-modal" class="modal modal-styled fade in" data-backdrop="static">

        <div class="modal-dialog">

          <div class="modal-content">

            <div class="modal-header">
              <i class="fa"></i>
              <?php /* <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> */ ?>
              <h3 class="modal-title"></h3>
            </div> <!-- /.modal-header -->

            <div class="modal-body">
              <p></p>
            </div> <!-- /.modal-body -->

            <div class="modal-footer">
              <button type="button" class="btn btn-default cancel" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-default ok" data-dismiss="modal">Ok</button>
            </div> <!-- /.modal-footer -->

          </div> <!-- /.modal-content -->

        </div><!-- /.modal-dialog -->

      </div>

