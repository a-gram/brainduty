

        <div class="task-details-view">
           <div class="task-header">
              <a class="btn-close-view" href="#"><i class="fa fa-reply-all"></i></a>
              <div class="btn-group pull-right task-commands"></div>
           </div>
           <div class="content-pane task-details">
              <div class="task-notes" style="display:none;">
                 <div class="pull-left"><i class="fa fa-info-circle"></i></div>
                 <div class="task-notes-content"></div>
              </div>
              <h3 class="task-subject"></h3>
              <ul class="icons-list-2 task-info">
                 <li class="clearfix">
                    <div class="pull-left"><i class="icon-li fa fa-clock-o"></i><span class="task-created-at"></span></div>
                    <div class="pull-right"><i class="icon-li fa fa-credit-card task-funded"></i><span class="task-price"></span></div>
                 </li>
                 <li class="clearfix">
                    <div class="pull-left">
                        <i class="icon-li fa fa-user"></i><a class="task-party" data-toggle="popover" data-placement="bottom" data-trigger="focus" role="button" tabindex="0"></a>
                        <div class="task-party-info hidden">
                            <h4 class="name"></h4>
                            <h5 class="rating"></h5>
                            <p class="about"></p>
                            <button class="btn btn-default btn-sm favourite">Favourite<i class="fa fa-spinner fa-pulse loading"></i></button>
                            <button class="btn btn-default btn-sm ignore">Ignore<i class="fa fa-spinner fa-pulse loading"></i></button>
                        </div>
                    </div>
                    <div class="pull-right"><span class="label label-task-status label-task-scheduled"></span></div>
                 </li>
              </ul>
              <p class="task-description"></p>
              <div class="task-attachments"></div>
              <br><br>
              <div class="task-services">
                 <table class="table table-condensed styled">
                    <caption>Services</caption>
                    <tbody></tbody>
                 </table>
              </div>
           </div>
            
            <br class="xs-30">
            
           <!-- /.task-details -->
           
           @if (!isset($vw_no_chat))
             @include('widgets.agchat')
           @endif
           
        </div>
        <!-- /.task-details-view -->

