
		<div class="user-details-view">

		    <p><a class="btn-close-view" href="#">x</a></p>
		  
		    <h3 class="title"></h3>

		    <div class="header">
			  <span class="label label-user-status label-user-active"></span>
			  <span class="label label-identity-status label-identity-verified"></span>
			  <div class="btn-group pull-right user-commands"></div>
		    </div>

		    <hr>

		    <div class="row">
		       <div class="col-md-3 avatar">
		           <img src="{{ URL::asset('img/avatar-2.png') }}" alt="">
		       </div>
		       <div class="col-md-9 bio">
                            <dl class="dl-horizontal">
                                   <dt>Name</dt>
                                   <dd class="first-name"></dd>
                                   <dt>Last name</dt>
                                   <dd class="last-name"></dd>
                                   <dt>Date of birth</dt>
                                   <dd class="dob"></dd>
                                   <dt>Entity type</dt>
                                   <dd class="entity-type"></dd>
                                   <dt>Business name</dt>
                                   <dd class="business-name"></dd>
                                   <dt>Business number</dt>
                                   <dd class="business-number"></dd>
                                   <dt>Services</dt>
                                   <dd class="services"></dd>
                                   <dt>Services type</dt>
                                   <dd class="services-categories"></dd>
                            </dl>
		       </div>
		       <div class="col-md-3">
		           Billing info
		       </div>
		       <div class="col-md-9 billing-info">
                            <dl class="dl-horizontal">
                                   <dt>Address</dt>
                                   <dd class="address"></dd>
                                   <dt>City</dt>
                                   <dd class="city"></dd>
                                   <dt>Zip</dt>
                                   <dd class="zip-code"></dd>
                                   <dt>State</dt>
                                   <dd class="state"></dd>
                                   <dt>Country</dt>
                                   <dd class="country"></dd>
                            </dl>
		       </div>
		       <div class="col-md-3">
		           Identity
		       </div>
		       <div class="col-md-9 identity">
                            <dl class="dl-horizontal">
                                   <dt>External</dt>
                                   <dd class="external"><i class=""></i></dd>
                                   <dt>Platform</dt>
                                   <dd class="internal"><i class=""></i></dd>
                            </dl>
		       </div>
		    </div>
		    <div class="row">
		       <div class="col-md-12">
		          <div class="activation-docs"></div>
		       </div>
		    </div>
		    <div class="row">
		       <div class="col-md-12 notes">
				   <textarea class="form-control" rows="3" placeholder="Type your notes here ..."></textarea>
		       </div>
		    </div>
		  
		</div> <!-- /.user-details-view -->
